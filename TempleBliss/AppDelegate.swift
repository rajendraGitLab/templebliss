//
//  AppDelegate.swift
//  ApiStructureModule
//
//  Created by EWW071 on 13/03/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

var dataForDeviceToken = String()

var PlayerForRequestAccept: AVAudioPlayer?
var AccessTokenForVOIP = ""
var pushTokenForVOIP = Data()
protocol PushKitEventDelegate: AnyObject {
    func credentialsUpdated(credentials: PKPushCredentials) -> Void
    func credentialsInvalidated() -> Void
    func incomingPushReceived(payload: PKPushPayload) -> Void
    func incomingPushReceived(payload: PKPushPayload, completion: @escaping () -> Void) -> Void
}


import UIKit
import IQKeyboardManagerSwift
import Firebase
import UserNotifications
import SwiftyJSON
import GoogleSignIn
import FBSDKCoreKit
import Photos
import TwilioVoice
import PushKit
import CallKit

import TwilioVideo
import AVFoundation
import SystemConfiguration.CaptiveNetwork
import FirebaseAnalytics

let kGoogle_Client_ID : String = "38837830053-red81ca0j1h5mm56faiehcnfdp6usu7q.apps.googleusercontent.com"
var fcmDvcToken = String()
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {
    
    //MARK:- Variables for Video Call
    var StartTimeOfIncomingCall = Date()
    var EndTimeOfIncomingCall = Date()
    var isLauched = false
    
    var strRoomName : String?
    var room: Room?
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil
    var VideoCallScreenOpen : Bool = false
    var DataForVideoCall : [String:String] = [:]
    var RequestFromChat : Bool = false
    var DataForChat : [String:Any] = [:]
    
    //MARK:- Variables for Audio Call
    var CallToCustomerName = ""
    var AdvisorActiveCall: Call?
    var is_rejected: Int?
    var isPickedUp: Int?
    let twimlParamTo = "To"
    let kRegistrationTTLInDays = 365
    let kCachedDeviceToken = "CachedDeviceToken"
    let kCachedBindingDate = "CachedBindingDate"
    var incomingPushCompletionCallback: (() -> Void)?
    var isSpinning: Bool = false
    var callKitCompletionCallback: ((Bool) -> Void)? = nil
    var activeCallInvites: [String: CallInvite]! = [:]
    var activeCalls: [String: Call]! = [:]
    var activeCall: Call? = nil
    var callKitProvider: CXProvider?
    var callKitCallController : CXCallController?
    var userInitiatedDisconnect: Bool = false
    var playCustomRingback = false
    var ringtonePlayer: AVAudioPlayer? = nil
    //MARK: - Other variables
    var DeviceVersion = Float()
    var pushKitEventDelegate: PushKitEventDelegate?
    var voipRegistry: PKPushRegistry!
    var userInforForNotification : [AnyHashable : Any] = [:]
    var window: UIWindow?
    
    
    //    let voipQueue = DispatchQueue(label: "voipQueue", attributes: .concurrent)
    
    static var shared: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    //MARK: - Application lifecycle
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.applicationIconBadgeNumber = 0
        webservice_Init()
        
        initializePushKit()
        
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions:
                launchOptions
        )
        //  callObserver.setDelegate(self, queue: nil)
        DeviceVersion = Float(UIDevice.current.systemVersion) ?? 0.0
        // UITextField.appearance().autocorrectionType = .no
        askPermissionIfNeeded()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        if launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] != nil {
            print("SOme notification is here")
            // Do your task here
        }
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            
            let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
            
            
            let nav = UINavigationController(rootViewController: controller)
            nav.navigationBar.isHidden = true
            self.window?.rootViewController = nav
            
            
            //self.setUpNavigation()
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            
            let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
            
            let nav = UINavigationController(rootViewController: controller)
            nav.navigationBar.isHidden = true
            self.window?.rootViewController = nav
            
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
            
            let nav = UINavigationController(rootViewController: controller)
            nav.navigationBar.isHidden = true
            self.window?.rootViewController = nav
        }
        
        self.navigateToLogin()
        
        FirebaseApp.configure()
        
        registerForPushNotifications()
        // SocketIOManager.shared.establishConnection()
        TwilioVideoSDK.setLogLevel(.debug)
        
        //        initializePushKit()
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        isLauched = true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        isLauched = true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // self.endBackgroundUpdateTask()
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            //            if !SocketIOManager.shared.isSocketOn {
            //                SocketIOManager.shared.establishConnection()
            //            }
            
            if SingletonClass.sharedInstance.WorkingChat {
                if let provider = appDel.callKitProvider {
                    //                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    //                        appDel.callKitProvider?.invalidate()
                    //                    })
                    
                }
            }
            
            
            if userDefault.getUserData() != nil{
                let userdata = userDefault.getUserData()
                SingletonClass.sharedInstance.UserId = userdata?.profile.id ?? ""
                SingletonClass.sharedInstance.Api_Key = userDefault.value(forKey: UserDefaultsKey.X_API_KEY.rawValue) as! String
                SingletonClass.sharedInstance.LoginRegisterUpdateData = userdata
                SingletonClass.sharedInstance.usertype = userDefault.value(forKey: UserDefaultsKey.selectedUserType.rawValue) as! String
                SingletonClass.sharedInstance.DeviceToken = userDefault.value(forKey: UserDefaultsKey.DeviceToken.rawValue) as! String
                
                if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
                    if userdata?.profile.categoryId == "" {
                        self.navigateToEnterCategoryData()
                    }
                    else {
                        let adviserReqModel = getAdviserData()
                        adviserReqModel.user_id = SingletonClass.sharedInstance.UserId
                        //   Utilities.showHud()
                        WebServiceSubClass.AdviserDetails(CategoryModel: adviserReqModel, completion: {(json, status, response) in
                            //   Utilities.hideHud()
                            if(status) {
                                
                                let loginModel = UserInfo.init(fromJson: json)
                                let loginModelDetails = loginModel
                                
                                SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
                                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                                userDefault.setUserData(objProfile: loginModelDetails)
                                
                                let AvailabiltyDataModel = AvailabiltyData()
                                AvailabiltyDataModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
                                
                                self.WebserviceForGetAvailability(reqModel: AvailabiltyDataModel)
                                
                                //
                            }else {
                                if json["message"] == nil {
                                    Utilities.displayErrorAlert(response as? String ?? "")
                                } else {
                                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                                }
                            }
                        })
                    }
                }
            }
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            
            //            if !SocketIOManager.shared.isSocketOn {
            //                SocketIOManager.shared.establishConnection()
            //            }
        }
        
        
    }
    func applicationWillTerminate(_ application: UIApplication) {
        AppDelegate.shared.callKitProvider = nil
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: MessageViewController.self) {
                let vc = topVC as! MessageViewController
                if vc.sessionType != "session" {
                    vc.GetHistoryOfChat()
                }
            } else if topVC.isKind(of: AdvisorMessageChatViewController.self) {
                let vc = topVC as! AdvisorMessageChatViewController
                if vc.sessionType != "session" {
                    vc.getHistoryOfChat()
                }
            }
        }
        //        let getAdviserDataReqModel = getAdviserData()
        //        getAdviserDataReqModel.user_id = SingletonClass.sharedInstance.UserId
        //
        //        WebServiceSubClass.AdviserDetails(CategoryModel: getAdviserDataReqModel, completion: {(json, status, response) in
        //            Utilities.hideHud()
        //            if(status) {
        //
        //                let loginModel = UserInfo.init(fromJson: json)
        //                let loginModelDetails = loginModel
        //                SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
        //                userDefault.setUserData(objProfile: loginModelDetails)
        //            }else {
        //                Utilities.displayErrorAlert(json["message"].string ?? "")
        //            }
        //        })
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        var handled: Bool
        
        handled = GIDSignIn.sharedInstance.handle(url)
        if handled {
            return true
        } else if url.pathComponents.contains("facebook") {
            
            return ApplicationDelegate.shared.application(
                app,
                open: url,
                options: options
            )
        }
        return false
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
    }
    // MARK: - PushKit
    //0x282042490
    func initializePushKit() {
        voipRegistry = PKPushRegistry.init(queue: nil)
        voipRegistry.delegate = self
        self.pushKitEventDelegate = self
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }
    
    
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        debugPrint("handleEventsForBackgroundURLSession: \(identifier)")
    }
    
    
    
    
    
    func navigateToUserLogin(){
        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: LoginViewController.storyboardID) as! LoginViewController
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    func navigateToAdvisorLogin(){
        let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserLoginViewController.storyboardID) as! AdviserLoginViewController
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    
    func navigateToLogin(){
        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: selectUserTypeViewController.storyboardID) as! selectUserTypeViewController
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    func navigateToHomeCustomer() {
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: MessageViewController.self) {
                    
                } else {
                    
                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomTabBarVC.storyboardID) as! CustomTabBarVC
                    controller.selectedIndex = 1
                    let nav = UINavigationController(rootViewController: controller)
                    nav.navigationBar.isHidden = true
                    self.window?.rootViewController = nav
                }
            }
            
        } else {
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                
                if userDefault.getUserDataForCustomer() != nil {
                    
                    sleep(1)
                    let userdata = userDefault.getUserDataForCustomer()
                    SingletonClass.sharedInstance.loginForCustomer = userdata
                    //  userdata?.profile.QuickTour = "1"
                    if userdata?.profile.QuickTour == "1" {
                        userdata?.profile.QuickTour = "2"
                        SingletonClass.sharedInstance.loginForCustomer = userdata
                        userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)
                        userDefault.synchronize()
                        userDefault.set(true, forKey: UserDefaultsKey.isUserFirstTime.rawValue)
                        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: IntroScreenViewController.storyboardID) as! IntroScreenViewController
                        let nav = UINavigationController(rootViewController: controller)
                        
                        self.window?.rootViewController = nav
                        
                    } else {
                        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomTabBarVC.storyboardID) as! CustomTabBarVC
                        controller.selectedIndex = 1
                        let nav = UINavigationController(rootViewController: controller)
                        nav.navigationBar.isHidden = true
                        self.window?.rootViewController = nav
                    }
                    
                }
                
            }
            
        }
        
    }
    func askPermissionIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                
            })
        case .denied:
            break
        case .granted:
            break
            
        @unknown default:
            break
        }
        
        
        AVCaptureDevice.requestAccess(for: .video) { success in
            
        }
    }
    func navigateToHomeAdviser() {
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AdvisorMessageChatViewController.self) {
                
            } else {
                
                let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: CustomTabBarVC.storyboardID) as! CustomTabBarVC
                let nav = UINavigationController(rootViewController: controller)
                nav.navigationBar.isHidden = true
                self.window?.rootViewController = nav
            }
        } else {
            logMessage(messageText: "Voice Chat: navigateToHomeAdviser else case")
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: CustomTabBarVC.storyboardID) as! CustomTabBarVC
            let nav = UINavigationController(rootViewController: controller)
            nav.navigationBar.isHidden = true
            self.window?.rootViewController = nav
        }
        
    }
    func navigateToEnterCategoryData() {
        
        let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserAddCategoryViewController.storyboardID) as! AdviserAddCategoryViewController
        
        let nav = UINavigationController(rootViewController: controller)
        nav.navigationBar.isHidden = true
        self.window?.rootViewController = nav
    }
    
    func setUpNavigation() {
        logMessage(messageText: "Voice Chat: setUpNavigation")
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            logMessage(messageText: "Voice Chat: setUpNavigation 1")
            if userDefault.getUserData() != nil{
                logMessage(messageText: "Voice Chat: setUpNavigation 2")
                let userdata = userDefault.getUserData()
                SingletonClass.sharedInstance.UserId = userdata?.profile.id ?? ""
                SingletonClass.sharedInstance.Api_Key = userDefault.value(forKey: UserDefaultsKey.X_API_KEY.rawValue) as! String
                SingletonClass.sharedInstance.LoginRegisterUpdateData = userdata
                SingletonClass.sharedInstance.usertype = userDefault.value(forKey: UserDefaultsKey.selectedUserType.rawValue) as! String
                SingletonClass.sharedInstance.DeviceToken = userDefault.value(forKey: UserDefaultsKey.DeviceToken.rawValue) as! String
                
                if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
                    if userdata?.profile.categoryId == "" {
                        self.navigateToEnterCategoryData()
                    }
                    else {
                        let adviserReqModel = getAdviserData()
                        
                        
                        adviserReqModel.user_id = SingletonClass.sharedInstance.UserId
                        //Utilities.showHud()
                        WebServiceSubClass.AdviserDetails(CategoryModel: adviserReqModel, completion: {(json, status, response) in
                            //Utilities.hideHud()
                            if(status) {
                                self.logMessage(messageText: "Voice Chat: main queue")
                                DispatchQueue.main.async {
                                    self.navigateToHomeAdviser()
                                }
                            }else {
                                if json["message"] == nil {
                                    Utilities.displayErrorAlert(response as? String ?? "")
                                } else {
                                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                                }
                            }
                        })
                    }
                }
            } else {
                logMessage(messageText: "Voice Chat: setUpNavigation 3")
            }
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            if userDefault.getUserDataForCustomer() != nil{
                let userdata = userDefault.getUserDataForCustomer()
                SingletonClass.sharedInstance.UserId = userdata?.profile.id ?? ""
                SingletonClass.sharedInstance.Api_Key = userDefault.value(forKey: UserDefaultsKey.X_API_KEY.rawValue) as! String
                SingletonClass.sharedInstance.loginForCustomer = userdata
                SingletonClass.sharedInstance.usertype = userDefault.value(forKey: UserDefaultsKey.selectedUserType.rawValue) as! String
                SingletonClass.sharedInstance.DeviceToken = userDefault.value(forKey: UserDefaultsKey.DeviceToken.rawValue) as! String
                
                if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) {
                    self.navigateToHomeCustomer()
                }
            }
        } else {
            //self.navigateToLogin()
        }
        
        
    }
    
    func clearData()
    {
        for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
            
            if key != UserDefaultsKey.DeviceToken.rawValue && key  != "language"  {
                print("\(key) = \(value) \n")
                UserDefaults.standard.removeObject(forKey: key)
            }
        }
        UserDefaults.standard.set(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
        
        SingletonClass.sharedInstance.clearSingletonClass()
        
    }
    func performLogout(){
        let logout = LogoutReqModel()
        logout.user_id = SingletonClass.sharedInstance.UserId
        Utilities.showHud()
        
        WebServiceSubClass.LogOut(logOutModel: logout, showHud: false, completion: { (response, status, error) in
            
            if status{
                appDel.SetLogout()
            }else{
                Utilities.hideHud()
            }
        })
    }
    
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_ , _ in })
            // For iOS 10 data message (sent via FCM)
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
            UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        print(#function, notification)
        
        let content = notification.request.content
        let userInfo = notification.request.content.userInfo
        print(appDel.window?.rootViewController?.navigationController?.children.first as Any)
        
        let key = (userInfo as NSDictionary).object(forKey: "gcm.notification.type")
        if key as? String ?? "" == PushNotifications.chat.Name  {
            if let topVC = UIApplication.topViewController() {
                
                if topVC.isKind(of: MessageViewController.self)  {
                    
                    let VC = topVC as! MessageViewController
                    if VC.sessionType != "session" {
                        if let SenderData = userInfo["gcm.notification.response"] as? String {
                            let data = SenderData.data(using: .utf8)!
                            do
                            {
                                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                {
                                    
                                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                                        
                                        if VC.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                            
                                        } else {
                                            NotificationCenter.default.post(name: NotificationBadges, object: content)
                                            completionHandler([.alert, .sound])
                                        }
                                    }
                                }
                            }
                            catch let error as NSError {
                                print(error)
                            }
                            
                        }
                    }
                    
                } else if topVC.isKind(of: AdvisorMessageChatViewController.self) {
                    
                    let VC = topVC as! AdvisorMessageChatViewController
                    if VC.sessionType != "session" {
                        if let SenderData = userInfo["gcm.notification.response"] as? String {
                            let data = SenderData.data(using: .utf8)!
                            do
                            {
                                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                {
                                    
                                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                                        
                                        if VC.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                            
                                        } else {
                                            NotificationCenter.default.post(name: NotificationBadges, object: content)
                                            completionHandler([.alert, .sound])
                                        }
                                    }
                                }
                            }
                            catch let error as NSError {
                                print(error)
                            }
                            
                        }
                    }
                    
                    
                } else if topVC.isKind(of: CallControllerViewController.self) || topVC.isKind(of: CustomerVideoCallViewController.self) || topVC.isKind(of: AudioCallViewController.self) || topVC.isKind(of: AdvisorAudioCallViewController.self) {
                    
                    
                }  else if key as? String ?? "" == "call" {
                    
                } else {
                    if topVC.isModal {
                        if let newPresendtedVC = (topVC.navigationController?.presentingViewController as? UINavigationController) {
                            let allController = newPresendtedVC.viewControllers
                            if allController.contains(where: {return $0 is AdvisorMessageChatViewController}) || allController.contains(where: {return $0 is MessageViewController})
                            {
                                for controller in allController
                                {
                                    if controller is AdvisorMessageChatViewController
                                    {
                                        let vc =  controller as! AdvisorMessageChatViewController
                                        if vc.sessionType != "session" {
                                            if let SenderData = userInfo["gcm.notification.response"] as? String {
                                                let data = SenderData.data(using: .utf8)!
                                                do
                                                {
                                                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                                    {
                                                        
                                                        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                                                            
                                                            if vc.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                                                
                                                            } else {
                                                                NotificationCenter.default.post(name: NotificationBadges, object: content)
                                                                completionHandler([.alert, .sound])
                                                            }
                                                        }
                                                    }
                                                }
                                                catch let error as NSError {
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    } else if controller is MessageViewController {
                                        let vc = controller as! MessageViewController
                                        if vc.sessionType != "session" {
                                            if let SenderData = userInfo["gcm.notification.response"] as? String {
                                                let data = SenderData.data(using: .utf8)!
                                                do
                                                {
                                                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                                    {
                                                        
                                                        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                                                            
                                                            if vc.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                                                
                                                            } else {
                                                                NotificationCenter.default.post(name: NotificationBadges, object: content)
                                                                completionHandler([.alert, .sound])
                                                            }
                                                        }
                                                    }
                                                }
                                                catch let error as NSError {
                                                    print(error)
                                                }
                                            }
                                        }
                                        
                                    }
                                }
                            } else if allController.contains(where: {return $0 is CallControllerViewController}) || allController.contains(where: {return $0 is CustomerVideoCallViewController}) || allController.contains(where: {return $0 is AudioCallViewController}) || allController.contains(where: {return $0 is AdvisorAudioCallViewController}){
                                
                                
                            }  else {
                                NotificationCenter.default.post(name: NotificationBadges, object: content)
                                completionHandler([.alert, .sound])
                            }
                            
                        }
                    } else {
                        if let newPresendtedVC = (topVC.navigationController) {
                            let allController = newPresendtedVC.viewControllers
                            if allController.contains(where: {return $0 is AdvisorMessageChatViewController}) || allController.contains(where: {return $0 is MessageViewController})
                            {
                                for controller in allController
                                {
                                    if controller is AdvisorMessageChatViewController
                                    {
                                        let vc =  controller as! AdvisorMessageChatViewController
                                        
                                        if let SenderData = userInfo["gcm.notification.response"] as? String {
                                            let data = SenderData.data(using: .utf8)!
                                            do
                                            {
                                                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                                {
                                                    
                                                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                                                        
                                                        if vc.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                                            
                                                        } else {
                                                            NotificationCenter.default.post(name: NotificationBadges, object: content)
                                                            completionHandler([.alert, .sound])
                                                        }
                                                    }
                                                }
                                            }
                                            catch let error as NSError {
                                                print(error)
                                            }
                                        }
                                        
                                        
                                    } else if controller is MessageViewController {
                                        let vc = controller as! MessageViewController
                                        
                                        if let SenderData = userInfo["gcm.notification.response"] as? String {
                                            let data = SenderData.data(using: .utf8)!
                                            do
                                            {
                                                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                                                {
                                                    
                                                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                                                        
                                                        if vc.bookingID == "\(jsonResponse["booking_id"] as? Int ?? 0)" {
                                                            
                                                        } else {
                                                            NotificationCenter.default.post(name: NotificationBadges, object: content)
                                                            completionHandler([.alert, .sound])
                                                        }
                                                    }
                                                }
                                            }
                                            catch let error as NSError {
                                                print(error)
                                            }
                                        }
                                    }
                                }
                            } else if allController.contains(where: {return $0 is CallControllerViewController}) || allController.contains(where: {return $0 is CustomerVideoCallViewController}) || allController.contains(where: {return $0 is AudioCallViewController}) || allController.contains(where: {return $0 is AdvisorAudioCallViewController}){
                                
                            } else {
                                NotificationCenter.default.post(name: NotificationBadges, object: content)
                                completionHandler([.alert, .sound])
                            }
                            
                        }
                    }
                    
                }
                
                
            }
        } else if key as? String ?? "" ==  PushNotifications.notify_me.Name {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdviserDetailsViewController.self) {
                    let DetailVC = topVC as! AdviserDetailsViewController
                    
                    if let SenderData = userInfo["gcm.notification.response"] as? String {
                        let data = SenderData.data(using: .utf8)!
                        do
                        {
                            if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                            {
                                if DetailVC.selectedAdviserDetails?.id == jsonResponse["advisor_id"] as? String ?? "" {
                                    
                                    
                                    GetAdvisorDetailsForUpdate(userId: jsonResponse["user_id"] as? String ?? "", advisorID: jsonResponse["advisor_id"] as? String ?? "")
                                    
                                    //   self.handlePushnotifications(NotificationType: key as? String ?? "", userData: userInfo)
                                } else {
                                    NotificationCenter.default.post(name: NotificationBadges, object: content)
                                    completionHandler([.alert, .sound])
                                }
                            }
                        }
                        catch let error as NSError {
                            print(error)
                        }
                    }
                    
                } else {
                    NotificationCenter.default.post(name: NotificationBadges, object: content)
                    completionHandler([.alert, .sound])
                }
            }
            
            
        } else if key as? String ?? "" == PushNotifications.call_reject_by_advisor.Name || key as? String ?? "" == PushNotifications.call_reject_by_user.Name || key as? String ?? "" == PushNotifications.pick_up_call.Name  {
            if key as? String ?? "" == PushNotifications.call_reject_by_user.Name {
                
            }
        } else if key as? String ?? "" == PushNotifications.BookingRequest.Name {
            if let SenderData = userInfo["gcm.notification.response"] as? String {
                let data = SenderData.data(using: .utf8)!
                do
                {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                    {
                        //                        HandleBookingRequest(response: )
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute:  {
                            self.HandleBookingRequest(response: JSON(rawValue: jsonResponse) ?? JSON())
                        })
                        
                    }
                }
                catch let error as NSError {
                    print(error)
                }
            }
            
            
        } else if key as? String ?? "" == PushNotifications.BookingRequest1.Name {
            if let SenderData = userInfo["gcm.notification.response"] as? String {
                let data = SenderData.data(using: .utf8)!
                do
                {
                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                    {
                        //                        HandleBookingRequest(response: )
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute:  {
                            self.HandleBookingRequest(response: JSON(rawValue: jsonResponse) ?? JSON())
                        })
                        
                    }
                }
                catch let error as NSError {
                    print(error)
                }
            }
        }  else {
            NotificationCenter.default.post(name: NotificationBadges, object: content)
            completionHandler([.alert, .sound])
        }
        
        print(#function)
        
        if key as? String ?? "" == PushNotifications.chat.Name || key as? String ?? "" == PushNotifications.call_reject_by_advisor.Name || key as? String ?? "" == PushNotifications.call_reject_by_user.Name || key as? String ?? "" == PushNotifications.pick_up_call.Name || key as? String ?? "" ==  PushNotifications.notify_me.Name || key as? String ?? "" == PushNotifications.BookingRequest.Name  {
            
        } else {
            self.handlePushnotifications(NotificationType: key as? String ?? "", userData: userInfo)
        }
    }
    
    //    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    //
    //        print(#function, notification)
    //
    //        let content = notification.request.content
    //        let userInfo = notification.request.content.userInfo
    //        print(userInfo)
    //        print(appDel.window?.rootViewController?.navigationController?.children.first as Any)
    //
    //
    //        let key = (userInfo as NSDictionary).object(forKey: "gcm.notification.type")
    //        if let topVC = UIApplication.topViewController() {
    //
    //            if topVC.isKind(of: MessageViewController.self) || topVC.isKind(of: AdvisorMessageChatViewController.self) {
    //
    //            } else if key as? String ?? "" == "call" {
    //
    //            } else {
    //                NotificationCenter.default.post(name: NotificationBadges, object: content)
    //                completionHandler([.alert, .sound])
    //            }
    //        }
    ////        let topVC = UIApplication.topViewController()
    ////        if ((topVC?.isKind(of: MessageViewController.self)) != nil) {
    ////
    ////        } else {
    ////            NotificationCenter.default.post(name: NotificationBadges, object: content)
    ////            completionHandler([.alert, .sound])
    ////        }
    //
    //
    //
    //        print(#function)
    //        if key as? String ?? "" == "chat" || key as? String ?? "" == "call_reject_by_advisor" || key as? String ?? "" == "call_reject_by_user" || key as? String ?? "" == "pick_up_call" {
    //
    //
    //        } else {
    //            self.handlePushnotifications(NotificationType: key as? String ?? "", userData: userInfo)
    //        }
    //
    //
    //    }
    
    func playSound() {
        
        guard let url = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") else { return }
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(.playback, mode: .default)
            try audioSession.setActive(true)
            //            NotificationCenter.default.removeObserver(self, name: AVAudioSession.interruptionNotification, object: nil)
            //            NotificationCenter.default.addObserver(self, selector: #selector(handleInterruption(notification:)), name: AVAudioSession.interruptionNotification, object: nil)
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            PlayerForRequestAccept = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = PlayerForRequestAccept else { return }
            player.numberOfLoops = -1
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    @objc func handleInterruption(notification: Notification) {
        guard let userInfo = notification.userInfo,
              let typeInt = userInfo[AVAudioSessionInterruptionTypeKey] as? UInt,
              let type = AVAudioSession.InterruptionType(rawValue: typeInt) else {
            return
        }
        
        switch type {
        case .began:
            // Pause your player
            guard let player = PlayerForRequestAccept else { return }
            player.pause()
        case .ended:
            if let optionInt = userInfo[AVAudioSessionInterruptionOptionKey] as? UInt {
                let options = AVAudioSession.InterruptionOptions(rawValue: optionInt)
                if options.contains(.shouldResume) {
                    // Resume your player
                    guard let player = PlayerForRequestAccept else { return }
                    //player.play()
                    self.playSound()
                }
            }
        @unknown default:
            break
        }
    }
    
    
    
    func HandleBookingRequest(response:JSON, isActive: Bool = false) {
        logMessage(messageText: "Voice Chat: HandleBookingRequest")
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AdvisorRequestAcceptViewController.self) {
                currentRequestArrived = true
                
            } else {
                currentRequestArrived = false
            }
        }
        if currentRequestArrived {
            logMessage(messageText: "Voice Chat: currentRequestArrived")
            let singleResponse = response
            if SocketIOManager.shared.socket.status == .connected {
                logMessage(messageText: "Voice Chat: HandleBookingRequest request_reject event fired")
                let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
            } else {
                logMessage(messageText: "Voice Chat: HandleBookingRequest not connected")
            }
            
        } else {
            logMessage(messageText: "Voice Chat: currentRequestArrived else")
            currentRequestArrived = true
            if let topVC = UIApplication.topViewController() {
                logMessage(messageText: "Voice Chat: topVC found")
                
                //                    let systemSoundID: SystemSoundID = 1315
                //                    AudioServicesPlaySystemSound (systemSoundID)
                if topVC.isModal {
                    logMessage(messageText: "Voice Chat: topVC is modal")
                    topVC.dismiss(animated: true, completion: {
                        let singleResponse = response
                        
                        if singleResponse["type"].string?.lowercased() ?? "" == "audio" || singleResponse["type"].string?.lowercased() ?? "" == "video" {
                            if SocketIOManager.shared.socket.status == .connected {
                                let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                currentRequestArrived = false
                                self.logMessage(messageText: "Voice Chat: socketEmit event fired")
                            } else {
                                self.logMessage(messageText: "Voice Chat: modal connection not found")
                            }
                        } else {
                            self.logMessage(messageText: "Voice Chat: advisor UI show")
                            self.playSound()
                            let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                            controller.closourForBtnAccept = {
                                self.logMessage(messageText: "Voice Chat: closourForBtnAccept")
                                AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                PlayerForRequestAccept?.stop()
                                controller.dismiss(animated: true, completion: nil)
                                if SocketIOManager.shared.socket.status == .connected {
                                    let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                    SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                    currentRequestArrived = false
                                    
                                } else {
                                    print("Else case 1")
                                }
                            }
                            controller.closourForBtnReject = {
                                AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                PlayerForRequestAccept?.stop()
                                controller.dismiss(animated: true, completion: nil)
                                if SocketIOManager.shared.socket.status == .connected {
                                    let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                    SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                    currentRequestArrived = false
                                }
                            }
                            
                            controller.customerName = singleResponse["username"].string ?? ""
                            controller.totalMinutes = "\(singleResponse["minute"])"
                            controller.communicationType = singleResponse["type"].string ?? ""
                            
                            controller.modalPresentationStyle = .overCurrentContext
                            controller.modalTransitionStyle = .crossDissolve
                            appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                        }
                        
                    })
                } else {
                    logMessage(messageText: "Voice Chat: modal not found")
                    let singleResponse = response
                    if singleResponse["type"].string?.lowercased() ?? "" == "audio" || singleResponse["type"].string?.lowercased() ?? "" == "video" {
                        if SocketIOManager.shared.socket.status == .connected {
                            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                            
                            currentRequestArrived = false
                            logMessage(messageText: "Voice Chat: request_accept event fired")
                            
                        } else {
                            logMessage(messageText: "Voice Chat: socket not connected 2")
                        }
                        
                    } else {
                        logMessage(messageText: "Voice Chat: show UI Request 2")
                        
                        self.playSound()
                        let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                        controller.closourForBtnAccept = {
                            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                            PlayerForRequestAccept?.stop()
                            controller.dismiss(animated: true) {
                                self.chatAcceptActionHandler(singleResponse: singleResponse, isActive: isActive)
                            }
                            
                            //                            if SocketIOManager.shared.socket.status == .connected && state == .active {
                            //                                let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                            //                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                            //
                            //                                currentRequestArrived = false
                            //                                self.logMessage(messageText: "Voice Chat: request_accept event fired 5")
                            //                            } else {
                            //                                SocketIOManager.shared.closeConnection()
                            //                                SocketIOManager.shared.socketStateChanged = { [weak self] isConnected in
                            //                                    if isConnected {
                            //                                        let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                            //                                        SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                            //
                            //                                        currentRequestArrived = false
                            //                                        self?.logMessage(messageText: "Voice Chat: request_accept event fired 6")
                            //                                        SocketIOManager.shared.socketStateChanged = nil
                            //                                    }
                            //                                }
                            //                                self.logMessage(messageText: "Voice Chat: socket not connected 5")
                            //                                SocketIOManager.shared.reconnect()
                            ////                                SocketIOManager.shared.establishConnection()
                            //                            }
                        }
                        controller.closourForBtnReject = {
                            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                            PlayerForRequestAccept?.stop()
                            controller.dismiss(animated: true, completion: nil)
                            
                            if SocketIOManager.shared.socket.status == .connected {
                                let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                currentRequestArrived = false
                            }
                        }
                        controller.customerName = singleResponse["username"].string ?? ""
                        controller.totalMinutes = "\(singleResponse["minute"])"
                        controller.communicationType = singleResponse["type"].string ?? ""
                        
                        controller.modalPresentationStyle = .overCurrentContext
                        controller.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                    }
                    
                }
            } else {
                logMessage(messageText: "Voice Chat: topVC not found")
            }
        }
    }
    
    func chatAcceptActionHandler(singleResponse: JSON, isActive: Bool) {
        if SocketIOManager.shared.socket.status == .connected {
            Utilities.hideHud()
            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
            currentRequestArrived = false
        }
        return
        if isActive {
            self.logMessage(messageText: "Voice chat: active state")
        } else {
            self.logMessage(messageText: "Voice chat: inactive state")
        }
        if SocketIOManager.shared.socket.status == .connected  {
            self.logMessage(messageText: "Voice chat: connected state")
        } else {
            self.logMessage(messageText: "Voice chat: not connected state")
        }
        let isOpened = SocketIOManager.shared.socket.status == .connected && isActive
        print("App State: ", isActive, SocketIOManager.shared.socket.status)
//        if isOpened {
//            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
//            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
//            currentRequestArrived = false
//            self.logMessage(messageText: "Voice Chat: request_accept event fired 5")
//        } else {
            if !isOpened {
                //SocketIOManager.shared.reconnect()
            }
            Utilities.showHud()
            let reqModel = AdvisorChatAcceptRequestModel()
            reqModel.user_id = Int(singleResponse["user_id"].int ?? 0)
            reqModel.booking_id = Int(singleResponse["booking_id"].int ?? 0)
            reqModel.advisor_id = Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "0") ?? 0
            reqModel.type = singleResponse["type"].string ?? ""
            WebServiceSubClass.advisorChatRequestAccept(addCategory: reqModel) { (json, status,response ) in
                print(json, status, response)
                if status{
                    let window = UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first
                    var homeVC: AdviserHomeViewController?
                    if let firstWindow = window, let root = firstWindow.rootViewController as? UINavigationController, let tab = root.viewControllers.first as? CustomTabBarVC, let nav = tab.viewControllers?.first as? UINavigationController, let vc = nav.viewControllers.first as? AdviserHomeViewController {
                        homeVC = vc
                    }
                    
                    if let vc = UIApplication.topViewController() as? AdviserHomeViewController ?? homeVC {
                        self.logMessage(messageText: "Voice chat: Logic 1")
                        SocketIOManager.shared.socketStateChanged = {isConnected in
                            if isConnected {
                                self.logMessage(messageText: "Voice chat: Logic 2")
                                vc.acceptRequestHandler(json["data"])
                                currentRequestArrived = false
                                Utilities.hideHud()
                                let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                SocketIOManager.shared.socketStateChanged = nil
                            }
                        }
                        if isOpened {
                            SocketIOManager.shared.socketStateChanged?(true)
                        } else if SocketIOManager.shared.socket.status != .connected {
                            SocketIOManager.shared.socket.connect()
                        }
                        self.logMessage(messageText: "Voice Chat: socket not connected 5")
                    } else {
                        Utilities.hideHud()
                    }
                }else{
                    Utilities.hideHud()
                    // show network request fail error
                }
            }
//        }
    }
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        //        if let refreshedToken = InstanceID.instanceID().token() {
        //            print("InstanceID token: \(refreshedToken)")
        //        }
        //dataForDeviceToken = deviceToken.hexString
        Messaging.messaging().apnsToken = deviceToken
        print("Device Token123: \(deviceToken.hexString)")
        
        
        let devicetok = tokenString(deviceToken)
        fcmDvcToken = devicetok
        print(deviceToken)
        
        print("=========================")
        print("Ravi123",devicetok)
        //Ankur's Change
        //         self.navigateToTwilioVoice()
        
        //Ankur's Change
    }
    func tokenString(_ deviceToken:Data) -> String{
        let bytes = [UInt8](deviceToken)
        var token = ""
        for byte in bytes{
            token += String(format: "%02x",byte)
        }
        //MARK: APNS - this token will be passed to your backend that can be written in php, js, .net etc.
        return token
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        print("Firebase registration token: \(fcmToken)")
        // Note: This callback is fired at each app startup and whenever a new token is generated.
        let savedFCMToken = UserDefaults.standard.object(forKey: UserDefaultsKey.DeviceToken.rawValue) as? String
        SingletonClass.sharedInstance.DeviceToken = savedFCMToken ?? ""
        if savedFCMToken != fcmToken {
            UserDefaults.standard.set(fcmToken, forKey: UserDefaultsKey.DeviceToken.rawValue)
            SingletonClass.sharedInstance.DeviceToken = fcmToken ?? ""
            UserDefaults.standard.synchronize()
            // Update FCMToken to server by doing API call...
        }
        print(SingletonClass.sharedInstance.DeviceToken)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token (Updated): \(fcmToken)")
        
        let savedFCMToken = UserDefaults.standard.object(forKey: UserDefaultsKey.DeviceToken.rawValue) as? String
        SingletonClass.sharedInstance.DeviceToken = savedFCMToken ?? ""
        if savedFCMToken != fcmToken {
            UserDefaults.standard.set(fcmToken, forKey: UserDefaultsKey.DeviceToken.rawValue)
            SingletonClass.sharedInstance.DeviceToken = fcmToken
            UserDefaults.standard.synchronize()
            // Update FCMToken to server by doing API call...
        }
        print(SingletonClass.sharedInstance.DeviceToken)
        
    }
    
    func webservice_Init(){
        var UserID = ""
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            
            if userDefault.getUserData() != nil{
                let userdata = userDefault.getUserData()
                UserID = userdata?.profile.id ?? ""
            }
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            if userDefault.getUserDataForCustomer() != nil {
                let userdata = userDefault.getUserDataForCustomer()
                UserID = userdata?.profile.id ?? ""
            }
        }
        
        
        WebServiceSubClass.initApi(strParams: UserID, showHud: false, completion: { (json, status, error) in
            
        })
        
        WebServiceSubClass.SettingsApi(strParams: "", completion: { (response, status, error) in
            
            let initData = settingsResModel.init(fromJson: response)
            
            SingletonClass.sharedInstance.settingsModel = initData
            
        })
        
        WebServiceSubClass.initForAdviser(strParams: UserID, showHud: false, completion: { (json, status, error) in
            
            if status {
                
                let initData = InitResponseForAdviser.init(fromJson: json)
                
                SingletonClass.sharedInstance.initDataForAdviser = initData
                
                
                //let string = "2021-03-22 00:45:06"
                let string = initData.current_time
                
                // Create Date Formatter
                let dateFormatter = DateFormatter()
                
                // Set Date Format
                dateFormatter.dateFormat = DateFormatterString.timeWithDate.rawValue
                
                // Convert String to Date
                
                SingletonClass.sharedInstance.TodayDate = dateFormatter.date(from: string ?? "") ?? Date()
                
                if let isUpdateAvailble = json["update"].bool, !isUpdateAvailble, let msg = json["message"].string {
                    // Show an alert, Optional update is available :
                    WebServiceSubClass.CategoryForAdviser(strParams: "", completion: {(json, status, response) in
                        if status {
                            let categoryModel = CategoryResponseForAdviser.init(fromJson: json)
                            
                            SingletonClass.sharedInstance.categoryDataForAdviser = categoryModel
                            
                            
                            self.setUpNavigation()
                            if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                                userDefault.setValue(true, forKey: UserDefaultsKey.isUserSkip.rawValue)
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                                SingletonClass.sharedInstance.usertype = ""
                                userDefault.setValue("", forKey: UserDefaultsKey.selectedUserType.rawValue)
                                userDefault.synchronize()
                                appDel.navigateToHomeCustomer()
                                //self.setUpNavigation()
                            } else {
                                self.setUpNavigation()
                                
                            }
                        }
                        
                        
                    })
                    let alert = UIAlertController(title: AppName,
                                                  message: msg,
                                                  preferredStyle: UIAlertController.Style.alert)
                    
                    // let okAction = UIAlertAction(title: "OK",style: .cancel, handle)
                    
                    let okAction = UIAlertAction(title: "Update", style: .default, handler: { (action) in
                        
                        if let url = URL(string: AppURL) {
                            UIApplication.shared.open(url)
                            Utilities.topMostController()?.present(alert, animated: true, completion: nil)
                        }
                    })
                    
                    let LaterAction = UIAlertAction(title: "Later", style: .default, handler: { (action) in
                        
                    })
                    
                    alert.addAction(okAction)
                    alert.addAction(LaterAction)
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
                        //Utilities.topMostController()?.present(alert, animated: true, completion: nil)
                    }
                    
                    
                }
                else {
                    WebServiceSubClass.CategoryForAdviser(strParams: "", completion: {(json, status, response) in
                        if status {
                            let categoryModel = CategoryResponseForAdviser.init(fromJson: json)
                            
                            SingletonClass.sharedInstance.categoryDataForAdviser = categoryModel
                            
                            if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                                userDefault.setValue(true, forKey: UserDefaultsKey.isUserSkip.rawValue)
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                                SingletonClass.sharedInstance.usertype = ""
                                userDefault.setValue("", forKey: UserDefaultsKey.selectedUserType.rawValue)
                                userDefault.synchronize()
                                appDel.navigateToHomeCustomer()
                                //self.setUpNavigation()
                            } else {
                                self.setUpNavigation()
                                
                            }
                            
                        }
                        
                        
                    })
                }
                
                
            } else {
                
                
                // Maintainance Flow :
                
                //                let update = json["update"].string
                
                if let update = json["update"].bool, update == false {
                    
                    if let maintenance = json["maintenance"].bool, maintenance == true {
                        
                        let msg = json["message"].stringValue
                        
                        // stop user here :
                        let alert = UIAlertController(title: AppName,
                                                      message: msg,
                                                      preferredStyle: UIAlertController.Style.alert)
                        
                        Utilities.topMostController()?.present(alert, animated: true, completion: nil)
                        //  Utilities.displayAlertForMainantance(msg)
                    }
                } else if let update = json["update"].bool, update == true {
                    // Force update :
                    
                    if let msg = json["message"].string {
                        let alert = UIAlertController(title: AppName,
                                                      message: msg,
                                                      preferredStyle: UIAlertController.Style.alert)
                        
                        let okAction = UIAlertAction(title: "Update", style: .default, handler: { (action) in
                            
                            if let url = URL(string: AppURL) {
                                UIApplication.shared.open(url)
                                Utilities.topMostController()?.present(alert, animated: true, completion: nil)
                            }
                        })
                        
                        alert.addAction(okAction)
                        Utilities.topMostController()?.present(alert, animated: true, completion: nil)
                        
                    }
                }
                
                if let strMessage = json["message"].string {
                    Utilities.ShowAlert(OfMessage: strMessage)
                } else {
                    Utilities.ShowAlert(OfMessage: "Something went wrong")
                    
                }
            }
            
        })
        
    }
    func ForceLogout() {
        SingletonClass.sharedInstance.StopCustomerTimer()
        SingletonClass.sharedInstance.StopAdvisorTimer()
        if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
            Utilities.showHud()
            self.UnregisterVOIP()
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
            
            SingletonClass.sharedInstance.clearSingletonClass()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation() {
                //            print("\(key) = \(value) \n")
                print(key)
                if key == UserDefaultsKey.DeviceToken.rawValue || key == UserDefaultsKey.isUserFirstTime.rawValue {
                    
                }
                else {
                    UserDefaults.standard.removeObject(forKey: key)
                }
            }
            //            SocketIOManager.shared.socket.removeAllHandlers()
            SocketIOManager.shared.closeConnection()
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                
                
                Utilities.hideHud()
                self.navigateToAdvisorLogin()
            })
            
            
        } else if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) {
            
            Utilities.showHud()
            let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
            controller.UnregisterVOIP()
            controller.internalTimer?.invalidate()
            
            
            let DetailVC:AdviserDetailsViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
            DetailVC.internalTimer?.invalidate()
            
            let VideoVC:CustomerVideoCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomerVideoCallViewController.storyboardID) as! CustomerVideoCallViewController
            VideoVC.internalTimer?.invalidate()
            
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
            
            SingletonClass.sharedInstance.clearSingletonClass()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation() {
                //            print("\(key) = \(value) \n")
                print(key)
                if key == UserDefaultsKey.DeviceToken.rawValue || key == UserDefaultsKey.isUserFirstTime.rawValue {
                    
                }
                else {
                    UserDefaults.standard.removeObject(forKey: key)
                }
            }
            //            SocketIOManager.shared.socket.removeAllHandlers()
            SocketIOManager.shared.closeConnection()
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                Utilities.hideHud()
                self.navigateToUserLogin()
            })
        }
    }
    func SetLogout() {
        
        // Reset UserDefaults
        //        SocketIOManager.shared.closeConnection()
        
        if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
            let controller:AdvisorAudioCallViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorAudioCallViewController.storyboardID) as! AdvisorAudioCallViewController
            self.pushKitEventDelegate = nil
            controller.UnregisterVOIP()
            let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
            print("Device VOIP Token: ", deviceVOIPToken)
            TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
                if let error = error {
                    NSLog("An error occurred while unregistering: \(error.localizedDescription)")
                } else {
                    NSLog("Successfully unregistered from VoIP push notifications.")
                }
            })
            
            
            UserDefaults.standard.removeObject(forKey: "CachedDeviceToken")
            
            // Remove the cached binding as credentials are invalidated
            UserDefaults.standard.removeObject(forKey: "CachedBindingDate")
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
            
            SingletonClass.sharedInstance.clearSingletonClass()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation() {
                //            print("\(key) = \(value) \n")
                print(key)
                if key == UserDefaultsKey.DeviceToken.rawValue || key == UserDefaultsKey.isUserFirstTime.rawValue {
                    
                }
                else {
                    UserDefaults.standard.removeObject(forKey: key)
                }
            }
            //            SocketIOManager.shared.socket.removeAllHandlers()
            SocketIOManager.shared.closeConnection()
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                Utilities.hideHud()
                self.navigateToAdvisorLogin()
            })
            
            
        } else if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) {
            let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
            controller.UnregisterVOIP()
            
            
            let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
            print("Device VOIP Token: ", deviceVOIPToken)
            TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
                if let error = error {
                    print("An error occurred while unregistering: \(error.localizedDescription)")
                } else {
                    print("Successfully unregistered from VoIP push notifications.")
                }
            })
            self.pushKitEventDelegate = nil
            UserDefaults.standard.removeObject(forKey: "CachedDeviceToken")
            
            // Remove the cached binding as credentials are invalidated
            UserDefaults.standard.removeObject(forKey: "CachedBindingDate")
            
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
            userDefault.set(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
            
            SingletonClass.sharedInstance.clearSingletonClass()
            
            for (key, _) in UserDefaults.standard.dictionaryRepresentation() {
                //            print("\(key) = \(value) \n")
                print(key)
                if key == UserDefaultsKey.DeviceToken.rawValue || key == UserDefaultsKey.isUserFirstTime.rawValue {
                    
                }
                else {
                    UserDefaults.standard.removeObject(forKey: key)
                }
            }
            //            SocketIOManager.shared.socket.removeAllHandlers()
            SocketIOManager.shared.closeConnection()
            DispatchQueue.main.asyncAfter(deadline: .now() + 10.0, execute: {
                Utilities.hideHud()
                self.navigateToUserLogin()
            })
            
            
        }
    }
    
    class func firebaseLogEvent(name: String, parameters: [String:Any]? = nil) {
        FirebaseAnalytics.Analytics.logEvent(name, parameters: parameters)
    }
    
}
extension AppDelegate {
    func handlePushnotifications(NotificationType:String , userData : [AnyHashable : Any]) {
        print(userData)
        
        switch NotificationType {
        case PushNotifications.logout.Name:
            Utilities.hideHud()
            appDel.ForceLogout()
        case PushNotifications.profile_under_approval.Name:
            appDel.ForceLogout()
        case PushNotifications.block_by_admin.Name:
            appDel.ForceLogout()
        case PushNotifications.notify_me.Name:
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                
                self.SendToAdvisorDetails(userDataForChat: userData)
                
            }
        case PushNotifications.profile_approved.Name:
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                
                if userDefault.getUserData() != nil{
                    let userdata = userDefault.getUserData()
                    SingletonClass.sharedInstance.UserId = userdata?.profile.id ?? ""
                    SingletonClass.sharedInstance.Api_Key = userDefault.value(forKey: UserDefaultsKey.X_API_KEY.rawValue) as! String
                    SingletonClass.sharedInstance.LoginRegisterUpdateData = userdata
                    SingletonClass.sharedInstance.usertype = userDefault.value(forKey: UserDefaultsKey.selectedUserType.rawValue) as! String
                    SingletonClass.sharedInstance.DeviceToken = userDefault.value(forKey: UserDefaultsKey.DeviceToken.rawValue) as! String
                    
                    if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
                        if userdata?.profile.categoryId == "" {
                            self.navigateToEnterCategoryData()
                        }
                        else {
                            let adviserReqModel = getAdviserData()
                            adviserReqModel.user_id = SingletonClass.sharedInstance.UserId
                            Utilities.showHud()
                            WebServiceSubClass.AdviserDetails(CategoryModel: adviserReqModel, completion: {(json, status, response) in
                                Utilities.hideHud()
                                if(status) {
                                    
                                    let loginModel = UserInfo.init(fromJson: json)
                                    let loginModelDetails = loginModel
                                    
                                    SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
                                    userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                                    userDefault.setUserData(objProfile: loginModelDetails)
                                    
                                    if ((appDel.window?.rootViewController?.children) != nil){
                                        let ProfileVC = (appDel.window?.rootViewController?.children.first?.children[3] as! UINavigationController).topViewController
                                        
                                        if ((ProfileVC as? AdviserMyAccountViewController) != nil) {
                                            let AdviserMyController : AdviserMyAccountViewController = ProfileVC as! AdviserMyAccountViewController
                                            AdviserMyController.setValue()
                                        }
                                    }
                                }else {
                                    if json["message"] == nil {
                                        Utilities.displayErrorAlert(response as? String ?? "")
                                    } else {
                                        Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                                    }
                                }
                            })
                        }
                    }
                }
            }
            break
            
        case "chat":
            // Sandeep Suthar (ToDo Check)
            //            if let SenderData = userData["gcm.notification.response"] as? String {
            //                let data = SenderData.data(using: .utf8)!
            //                do
            //                {
            //                    if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
            //                    {
            //                        self.HandleBookingRequest(response: JSON(rawValue: jsonResponse) ?? JSON())
            //                    }
            //                }
            //                catch {
            //
            //                }
            //            }
            
            //            if let MainPage = self.window?.rootViewController as? UINavigationController , let NavController = MainPage.children[0] as? CustomTabBarVC, let HomeVCPage = NavController.children[0] as? HomeViewController {
            //                print(MainPage)
            //                print(NavController)
            //                print(HomeVCPage)
            //
            //            }
            //            if !SocketIOManager.shared.isSocketOn {
            //
            //                self.perform(#selector(self.SocketConnectionProcess), with: nil, afterDelay: 0.0)
            //                print("ATDebug : AppDel establishConnection")
            //                SocketIOManager.shared.socket.on(clientEvent: .connect) {data, ack in
            //                    print("ATDebug :: Appdelgate socket Now connected")
            //                    SocketIOManager.shared.isSocketOn = true
            //                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            //                        self.sendToChatScreen(userDataForChat: userData)
            //                    })
            //                }
            //
            //                SocketIOManager.shared.socket.on(clientEvent: .disconnect) {data, ack in
            //                    print("ATDebug :: Appdelgate socket Now Disconnected")
            //
            //                }
            //
            //                SocketIOManager.shared.socket.on(clientEvent: .reconnect) {data, ack in
            //                    print("ATDebug :: Appdelgate socket Now Reconnected")
            //                }
            //                SocketIOManager.shared.socket.on(clientEvent: .error) {data, ack in
            //                    print("ATDebug :: Appdelgate socket error \(data)")
            //                }
            //
            //                // On Methods
            //            } else {
            //                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            //                    self.sendToChatScreen(userDataForChat: userData)
            //                })
            //            }
            break
        default:
            break
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
        
        let currentDate = Date()
        print("currentDate : \(currentDate)")
        
    }
    
    @objc func MessageScreenNotification() {
        self.sendToChatScreen(userDataForChat: userInforForNotification)
    }
    @objc func NotifyMeNotification() {
        self.SendToAdvisorDetails(userDataForChat: userInforForNotification)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let key = (userInfo as NSDictionary).object(forKey: "gcm.notification.type")
        if(application.applicationState == .inactive)
        {
            if key as? String == PushNotifications.chat.Name {
                //                if let SenderData = userInfo["gcm.notification.response"] as? String {
                //                    let data = SenderData.data(using: .utf8)!
                //                    do
                //                    {
                //                        if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                //                        {
                //                            DispatchQueue.main.asyncAfter(deadline: .now() + (isLauched ? 2 : 2)) {
                //                                self.HandleBookingRequest(response: JSON(rawValue: jsonResponse) ?? JSON())
                //                            }
                //                        }
                //                    }
                //                    catch {
                //
                //                    }
                //                }
                //                self.userInforForNotification = userInfo
                //                NotificationCenter.default.addObserver(self, selector: #selector(MessageScreenNotification), name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil)
            } else if key as? String == PushNotifications.notify_me.Name {
                self.userInforForNotification = userInfo
                NotificationCenter.default.addObserver(self, selector: #selector(NotifyMeNotification), name: NSNotification.Name(rawValue: "NotifyMeNotification"), object: nil)
            }
            
            
            /*
             # App is transitioning from background to foreground (user taps notification), do what you need when user taps here!
             */
        }
        else
        if(application.applicationState == .active)
        {
            self.handlePushnotifications(NotificationType: key as? String ?? "", userData: userInfo)
            /*
             # App is currently active, can update badges count here
             */
        }
        else if(application.applicationState == .background)
        {
            print("Check")
            self.handlePushnotifications(NotificationType: key as? String ?? "", userData: userInfo)
            /* # App is in background, if content-available key of your notification is set to 1, poll to your backend to retrieve data and update your interface here */
        }
        
        //        print(userInfo)
        
        
        
        
        
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
        
        // Print message ID.
        //        if let messageID = userInfo[gcmMessageIDKey] {
        //            print("Message ID: \(messageID)")
        //        }
        
        // Print full message.
        print(#function)
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
        
    }
    
    
    
}
struct InitObject {
    
    var serviceCharge : Double!
    var message : String!
    var status : Bool!
    var support : String!
    var update: String!
    var isMaintenance : String!
    
    init(fromJson json: JSON!) {
        if json.isEmpty {
            return
        }
        serviceCharge = json["service_charge"].doubleValue
        message = json["message"].stringValue
        status = json["status"].boolValue
        support = json["support"].stringValue
        update = json["update"].stringValue
        isMaintenance = json["maintenance"].stringValue
    }
}


// MARK: PKPushRegistryDelegate
extension AppDelegate : PKPushRegistryDelegate {
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate credentials: PKPushCredentials, for type: PKPushType) {
        
        //        if type == PKPushType.voIP {
        //            let tokenData = credentials.token
        //            let voipPushToken = tokenData.reduce("") { $0 + String(format: "%02X", $1) }
        //            print("VoIP Push Token: \n\(voipPushToken)")
        //
        //        }
        NSLog("VoIP Token: \(credentials)")
        let deviceToken = credentials.token.map { String(format: "%02x", $0) }.joined()
        //UserDefaults.standard.set(credentials.token, forKey: kCachedDeviceToken)
        // let deviceTokenString = credentials.token.reduce("") { $0 + String(format: "%02X", $1) }
        print("VoIP Token updated: \(deviceToken)")
        dataForDeviceToken = deviceToken
        NSLog("pushRegistry:didUpdatePushCredentials:forType:")
        
        
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            AccessTokenForVOIP = fetchAccessToken(myIdentity: "\((SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "")") ?? ""
            
            
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            AccessTokenForVOIP = fetchAccessToken(myIdentity: "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")") ?? ""
            
        }
        
        if let delegate = self.pushKitEventDelegate {
            delegate.credentialsUpdated(credentials: credentials)
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        NSLog("pushRegistry:didInvalidatePushTokenForType:")
        var IdentityForUser = ""
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            
            print("\((SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "")")
            IdentityForUser = "\((SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "")"
            
        } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
            
            IdentityForUser = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"
            
        }
        if let delegate = self.pushKitEventDelegate {
            delegate.credentialsInvalidated()
        }
    }
    
    /**
     * Try using the `pushRegistry:didReceiveIncomingPushWithPayload:forType:withCompletionHandler:` method if
     * your application is targeting iOS 11. According to the docs, this delegate method is deprecated by Apple.
     */
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        print("Sandeep Suthar1: ", payload.dictionaryPayload as NSDictionary)
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:1")
        
        let configuration = CXProviderConfiguration(localizedName: "Templebliss audio")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.includesCallsInRecents = false
        if let callKitIcon = UIImage(named: CallKitIconName) {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }
        self.callKitProvider = CXProvider(configuration: configuration)
        if let provider = self.callKitProvider {
            provider.setDelegate(self, queue: nil)
        }
        
        /*
         * The important thing to remember when providing a TVOAudioDevice is that the device must be set
         * before performing any other actions with the SDK (such as connecting a Call, or accepting an incoming Call).
         * In this case we've already initialized our own `TVODefaultAudioDevice` instance which we will now set.
         */
        TwilioVoiceSDK.audioDevice = AdvisorAudioCall.sharedInstance.audioDevice
        
        if let delegate = self.pushKitEventDelegate {
            delegate.incomingPushReceived(payload: payload)
        }
        
    }
    /*
     {
     "aps":{
     "alert":"Test",
     "sound":"default",
     "badge":1
     }
     }
     
     {
     "aps" : {
     "alert":"fdsffxg",
     "content-available" : 1,
     "sound" : ""
     }
     }
     */
    /**
     * This delegate method is available on iOS 11 and above. Call the completion handler once the
     * notification payload is passed to the `TwilioVoiceSDK.handleNotification()` method.
     */
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType, completion: @escaping () -> Void) {
        logMessage(messageText: "Voice Chat: didReceiveIncomingPushWith")
        print("Sandeep Suthar2: ", payload.dictionaryPayload as NSDictionary)
        NSLog("pushRegistry:didReceiveIncomingPushWithPayload:forType:completion:2")
        
        if !SocketIOManager.shared.isSocketOn && isLauched {
            SocketIOManager.shared.establishConnection()
        }
        
        let msgType = payload.dictionaryPayload["twi_message_type"] as? String
        if let messageType = msgType{
            if messageType == "twilio.voice.call"{
                RequestFromChat = false
                //                let configuration = CXProviderConfiguration(localizedName: AppName)
                //                configuration.maximumCallGroups = 1
                //                configuration.maximumCallsPerCallGroup = 1
                //                configuration.includesCallsInRecents = false
                //                configuration.supportsVideo = false
                //                configuration.ringtoneSound = "\(RingtoneSoundName).mp3"
                //                if let callKitIcon = UIImage(named: CallKitIconName) {
                //                    configuration.iconTemplateImageData = callKitIcon.pngData()
                //                }
                //                self.callKitProvider = CXProvider(configuration: configuration)
                //                if let provider = self.callKitProvider {
                //                    provider.setDelegate(self, queue: nil)
                //                }
                //                if let delegate = self.pushKitEventDelegate {
                //                    delegate.incomingPushReceived(payload: payload, completion: completion)
                //                }
                //
                //                if let version = Float(UIDevice.current.systemVersion), version >= 13.0 {
                //                    /**
                //                     * The Voice SDK processes the call notification and returns the call invite synchronously. Report the incoming call to
                //                     * CallKit and fulfill the completion before exiting this callback method.
                //                     */
                //                    completion()
                //                }
                
                //                 By Komal Goyani
                //                let config = CXProviderConfiguration(localizedName: AppName)
                //                config.maximumCallGroups = 1
                //                config.maximumCallsPerCallGroup = 1
                //                config.includesCallsInRecents = false
                //                config.supportsVideo = false
                //                config.ringtoneSound = "\(RingtoneSoundName).mp3"
                //                if let callKitIcon = UIImage(named: CallKitIconName) {
                //                    config.iconTemplateImageData = callKitIcon.pngData()
                //                }
                //
                //                self.callKitProvider = CXProvider(configuration: config)
                //
                //                if let provider = self.callKitProvider {
                //                    provider.setDelegate(self, queue: nil)
                //                }
                //
                //
                //                let update = CXCallUpdate()
                //                if (Int(payload.dictionaryPayload["minute"] as? String ?? "") ?? 0) == 1 {
                //                    print("ATDebug :: ","\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                //                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                //                    update.remoteHandle = CXHandle(type: .emailAddress, value: "0000000000")
                //                    //update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                //                } else {
                //                    print("ATDebug :: ","\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                //                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                //                    update.remoteHandle = CXHandle(type: .emailAddress, value: "0000000000")
                //                    //update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                //                }
                //                let callId = UUID()
                //
                //                self.callKitProvider?.reportNewIncomingCall(with: callId, update: update, completion: { error in
                //
                //                    print(error)
                //
                //                    completion()
                //
                //                })
                TwilioVoiceSDK.audioDevice = AdvisorAudioCall.sharedInstance.audioDevice
                self.pushKitEventDelegate = self
                if let delegate = self.pushKitEventDelegate {
                    delegate.incomingPushReceived(payload: payload)
                }
                
            }else if messageType == "twilio.video.call"{
                
                let config = CXProviderConfiguration(localizedName: AppName)
                config.maximumCallGroups = 1
                config.maximumCallsPerCallGroup = 1
                config.supportsVideo = true
                config.includesCallsInRecents = false
                config.ringtoneSound = "\(RingtoneSoundName).mp3"
                if let callKitIcon = UIImage(named: CallKitIconName) {
                    config.iconTemplateImageData = callKitIcon.pngData()
                }
                DataForVideoCall = ["user_id":"\(payload.dictionaryPayload["user_id"] ?? "")",
                                    "user_name":"\(payload.dictionaryPayload["user_name"] as? String ?? "")",
                                    "advisor_id":"\(payload.dictionaryPayload["advisor_id"] ?? "")",
                                    "booking_id":"\(payload.dictionaryPayload["booking_id"] ?? "")","room":"\(payload.dictionaryPayload["twi_bridge_token"] as? String ?? "")","token":"\(payload.dictionaryPayload["receiver_token"] as? String ?? "")","name":"\(payload.dictionaryPayload["user_name"] as? String ?? "")","receiver_id":"\(payload.dictionaryPayload["advisor_id"] ?? "")"]
                print("XXXX: ", DataForVideoCall)
                
                self.callKitProvider = CXProvider(configuration: config)
                
                if let provider = self.callKitProvider {
                    provider.setDelegate(self, queue: nil)
                }
                
                OnSocketRejectByCustomerForVideoCall()
                let update = CXCallUpdate()
                if (Int(payload.dictionaryPayload["minute"] as? String ?? "") ?? 0) == 1 {
                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                    update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                } else {
                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                    update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                }
                
                update.hasVideo = true
                
                update.supportsDTMF = false
                update.supportsHolding = true
                update.supportsGrouping = false
                update.supportsUngrouping = false
                
                //        strRoomName = payload.dictionaryPayload["roomName"] as? String ?? ""
                //        strRoomId = payload.dictionaryPayload["room_id"] as? String ?? ""
                //        strReceiverId = payload.dictionaryPayload["receiver_id"] as? String ?? ""
                //        strSenderName = payload.dictionaryPayload["sender_name"] as? String ?? ""
                //        strSenderId = payload.dictionaryPayload["sender_id"] as? String ?? ""
                self.callKitProvider?.reportNewIncomingCall(with: UUID(), update: update, completion: { error in
                    
                    DispatchQueue.main.async {
                        completion()
                    }
                })
                
            } else if messageType == "twilio.chat.call"  {
//                if SocketIOManager.shared.socket.status != .connected {
//                    logMessage(messageText: "Voice Chat: Manual Socket Connection Started")
//                    SocketIOManager.shared.establishConnection()
//                }
                RequestFromChat = true
                let config = CXProviderConfiguration(localizedName: AppName)
                config.maximumCallGroups = 1
                config.maximumCallsPerCallGroup = 1
                config.includesCallsInRecents = false
                config.supportsVideo = false
                config.ringtoneSound = "\(RingtoneSoundName).mp3"
                if let callKitIcon = UIImage(named: CallKitIconName) {
                    config.iconTemplateImageData = callKitIcon.pngData()
                }
                DataForChat = ["user_id":payload.dictionaryPayload["user_id"] as? Int ?? 0,
                               "type":"\(payload.dictionaryPayload["type"] as? String ?? "")",
                               "start_time":"\(payload.dictionaryPayload["start_time"] as? String ?? "")",
                               "customer_profile_picture":"\(payload.dictionaryPayload["customer_profile_picture"] as? String ?? "")",
                               "message":"\(payload.dictionaryPayload["message"] as? String ?? "")",
                               "default_message":"\(payload.dictionaryPayload["default_message"] as? String ?? "")",
                               "advisor_profile_picture":"\(payload.dictionaryPayload["advisor_profile_picture"] as? String ?? "")",
                               "booking_id":payload.dictionaryPayload["booking_id"] as? Int ?? 0,
                               "customer_name":"\(payload.dictionaryPayload["customer_name"] as? String ?? "")",
                               "minute":"\(payload.dictionaryPayload["minute"] as? String ?? "")"]
                print("Chat Response From PUSH: ", DataForChat)
                self.callKitProvider = CXProvider(configuration: config)
                
                if let provider = self.callKitProvider {
                    provider.setDelegate(self, queue: nil)
                }
                
                
                let update = CXCallUpdate()
                if (Int(payload.dictionaryPayload["minute"] as? String ?? "") ?? 0) == 1 {
                    print("ATDebug :: ","\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                    update.remoteHandle = CXHandle(type: .emailAddress, value: "0000000000")
                    //update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minute Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                } else {
                    print("ATDebug :: ","\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                    update.localizedCallerName = "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")"
                    update.remoteHandle = CXHandle(type: .emailAddress, value: "0000000000")
                    //update.remoteHandle = CXHandle(type: .generic, value: "\(payload.dictionaryPayload["minute"] as? String ?? "") Minutes Chat: \(payload.dictionaryPayload["twi_from"] as? String ?? "")")
                }
                
                update.supportsDTMF = false
                update.supportsHolding = true
                update.supportsGrouping = false
                update.supportsUngrouping = false
                
                let callId = UUID()
                
                self.callKitProvider?.reportNewIncomingCall(with: callId, update: update, completion: { error in
                    
                    print(error)
                    
                    //                    DispatchQueue.main.async {
                    completion()
                    //                    }
                })
                if UIApplication.shared.applicationState == .active  {
                    self.HandleBookingRequest(response: JSON(rawValue: self.DataForChat) ?? JSON(), isActive: true)
                    self.callKitProvider?.reportCall(with: callId, endedAt: Date(), reason: .answeredElsewhere)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        if let _ = UIApplication.topViewController() as? AdvisorRequestAcceptViewController  {
                            self.playSound()
                        }
                    }
                }
                //(with: UUID(), updated: update)
                //self.callKitProvider?.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
                
            }
        } else {
            completion()
        }
    }
    
}
extension AppDelegate {
    func SendToAdvisorDetails(userDataForChat : [AnyHashable : Any]) {
        if let SenderData = userDataForChat["gcm.notification.response"] as? String {
            let data = SenderData.data(using: .utf8)!
            do
            {
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                {
                    
                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                        
                        GetAdvisorDetails(userId: jsonResponse["user_id"] as? String ?? "", advisorID: jsonResponse["advisor_id"] as? String ?? "")
                        
                        
                    }
                }
            }
            catch let error as NSError {
                print(error)
            }
        }
    }
    func sendToChatScreen(userDataForChat : [AnyHashable : Any]) {
        logMessage(messageText: "Voice Chat: Appdelegate sendToChatScreen")
        if let SenderData = userDataForChat["gcm.notification.response"] as? String {
            let data = SenderData.data(using: .utf8)!
            do
            {
                if let jsonResponse = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? Dictionary<String,Any>
                {
                    
                    if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                        
                        print((jsonResponse["user"] as? NSDictionary)?["sender_id"] as? Int ?? 0)
                        let dataForShare : [String:Any] = ["advisor_profile_picture":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture ?? "",
                                                           "customer_profile_picture":(jsonResponse["user"] as? NSDictionary)?["profile_picture"] as? String ?? "",
                                                           "type":jsonResponse["type"] as? String ?? "",
                                                           "user_id":jsonResponse["sender_id"] as? Int ?? 0,
                                                           "booking_id":jsonResponse["booking_id"] as? Int ?? 0]
                        
                        if let notes = jsonResponse["note"] {
                            
                            if notes as? String ?? "" != ""
                            {
                                SingletonClass.sharedInstance.isNoteAdded = true
                                SingletonClass.sharedInstance.noteText = notes as? String ?? ""
                            } else {
                                SingletonClass.sharedInstance.isNoteAdded = false
                                SingletonClass.sharedInstance.noteText = ""
                            }
                            let controller : AdvisorMessageChatViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorMessageChatViewController.storyboardID) as! AdvisorMessageChatViewController
                            
                            controller.sessionType = "free"
                            controller.RequestAcceptDataOfUser = dataForShare
                            controller.bookingID = "\(jsonResponse["booking_id"] as? Int ?? 0)"
                            controller.hidesBottomBarWhenPushed = true
                            
                            if let topVc = UIApplication.topViewController() {
                                if topVc.isKind(of: UIAlertController.self) {
                                    topVc.dismiss(animated: true, completion: nil)
                                }
                                if topVc.isKind(of: AdvisorMessageChatViewController.self) {
                                    let navController = topVc.navigationController
                                    topVc.navigationController?.popViewController(animated: true)
                                    DispatchQueue.main.async {
                                        navController?.pushViewController(controller, animated: true)
                                    }
                                    
                                } else {
                                    if topVc.isModal {
                                        if let newPresendtedVC = (topVc.navigationController?.presentingViewController as? UINavigationController) {
                                            let allController = newPresendtedVC.viewControllers
                                            if allController.contains(where: {return $0 is AdvisorMessageChatViewController})
                                            {
                                                topVc.dismiss(animated: true, completion: {
                                                    
                                                    if let newTopVC = UIApplication.topViewController() {
                                                        if newTopVC.isKind(of: AdvisorMessageChatViewController.self) {
                                                            let navController = newTopVC.navigationController
                                                            newTopVC.navigationController?.popViewController(animated: true)
                                                            
                                                            DispatchQueue.main.async {
                                                                navController?.pushViewController(controller, animated: true)
                                                            }
                                                        }
                                                    }
                                                })
                                            } else {
                                                topVc.navigationController?.pushViewController(controller, animated: true)
                                            }
                                        }
                                    } else {
                                        
                                        if let newPresendtedVC = (topVc.navigationController as? UINavigationController) {
                                            let allController = newPresendtedVC.viewControllers
                                            if allController.contains(where: {return $0 is AdvisorMessageChatViewController})
                                            {
                                                topVc.dismiss(animated: true, completion: {
                                                    
                                                    if let newTopVC = UIApplication.topViewController() {
                                                        if newTopVC.isKind(of: AdvisorMessageChatViewController.self) {
                                                            let navController = newTopVC.navigationController
                                                            newTopVC.navigationController?.popViewController(animated: true)
                                                            
                                                            DispatchQueue.main.async {
                                                                navController?.pushViewController(controller, animated: true)
                                                            }
                                                        }
                                                    }
                                                })
                                            } else {
                                                topVc.navigationController?.pushViewController(controller, animated: true)
                                            }
                                        }
                                    }
                                }
                            }
                            userInforForNotification = [:]
                            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil)
                        }
                    } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
                        
                        let controller : MessageViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MessageViewController.storyboardID) as! MessageViewController
                        
                        let dataForShare : [String:Any] = ["advisor_profile_picture":(jsonResponse["user"] as? NSDictionary)?["profile_picture"] as? String ?? "",
                                                           "customer_profile_picture":SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture ?? "",
                                                           "type":jsonResponse["type"] as? String ?? "",
                                                           "advisor_id":jsonResponse["sender_id"] as? Int ?? 0,
                                                           "booking_id":jsonResponse["booking_id"] as? Int ?? 0]
                        controller.sessionType = "free"
                        controller.RequestAcceptDataOfUser = dataForShare
                        controller.bookingID = "\(jsonResponse["booking_id"] as? Int ?? 0)"
                        controller.hidesBottomBarWhenPushed = true
                        if let topVc = UIApplication.topViewController() {
                            if topVc.isKind(of: UIAlertController.self) {
                                topVc.dismiss(animated: true, completion: nil)
                            }
                            if topVc.isKind(of: MessageViewController.self) {
                                let navController = topVc.navigationController
                                topVc.navigationController?.popViewController(animated: true)
                                DispatchQueue.main.async {
                                    navController?.pushViewController(controller, animated: true)
                                }
                                
                            } else {
                                topVc.navigationController?.pushViewController(controller, animated: true)
                            }
                        }
                        
                        
                        userInforForNotification = [:]
                        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil)
                        
                    }
                }
            }
            catch let error as NSError {
                print(error)
            }
        }
    }
    
    func GetAdvisorDetails(userId:String,advisorID:String) {
        
        print(#function)
        let ReqModelData = AdvisorDetailsReqModel()
        ReqModelData.user_id = userId
        ReqModelData.advisor_id = advisorID
        webserviceForGetAdvisorDetails(reqModel: ReqModelData)
        
    }
    func webserviceForGetAdvisorDetails(reqModel:AdvisorDetailsReqModel) {
        
        print(#function)
        WebServiceSubClass.AdvisorDetails(addCategory: reqModel, completion: {(json, status, response) in
            
            if status {
                
                let AdvisorData = AdvisorDetailResModel.init(fromJson: json)
                
                let controller:AdviserDetailsViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
                
                controller.SelectedCategoryName = "All"
                controller.selectedAdviserDetails = AdvisorData.advisorDetails
                controller.hidesBottomBarWhenPushed = true
                
                if let topVc = UIApplication.topViewController() {
                    if topVc.isKind(of: UIAlertController.self) {
                        topVc.dismiss(animated: true, completion: nil)
                    }
                    if topVc.isKind(of: MessageViewController.self) {
                        let navController = topVc.navigationController
                        topVc.navigationController?.popViewController(animated: true)
                        DispatchQueue.main.async {
                            navController?.pushViewController(controller, animated: true)
                        }
                        
                    } else {
                        topVc.navigationController?.pushViewController(controller, animated: true)
                    }
                }
                
                self.userInforForNotification = [:]
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil)
                
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    func GetAdvisorDetailsForUpdate(userId:String,advisorID:String) {
        
        print(#function)
        let ReqModelData = AdvisorDetailsReqModel()
        ReqModelData.user_id = userId
        ReqModelData.advisor_id = advisorID
        webserviceForGetAdvisorDetailsForUpdate(reqModel: ReqModelData)
        
    }
    func webserviceForGetAdvisorDetailsForUpdate(reqModel:AdvisorDetailsReqModel) {
        
        print(#function)
        WebServiceSubClass.AdvisorDetails(addCategory: reqModel, completion: {(json, status, response) in
            
            if status {
                
                let AdvisorData = AdvisorDetailResModel.init(fromJson: json)
                if let topVC = UIApplication.topViewController() {
                    if topVC.isKind(of: AdviserDetailsViewController.self) {
                        let DetailsVC = topVC as! AdviserDetailsViewController
                        
                        DetailsVC.selectedAdviserDetails = AdvisorData.advisorDetails
                        DetailsVC.setValue()
                    }
                }
                //                let controller:AdviserDetailsViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
                //
                //                controller.SelectedCategoryName = "All"
                //                controller.selectedAdviserDetails = AdvisorData.advisorDetails
                //                controller.hidesBottomBarWhenPushed = true
                //
                //                if let topVc = UIApplication.topViewController() {
                //                    if topVc.isKind(of: UIAlertController.self) {
                //                        topVc.dismiss(animated: true, completion: nil)
                //                    }
                //                    if topVc.isKind(of: MessageViewController.self) {
                //                        let navController = topVc.navigationController
                //                        topVc.navigationController?.popViewController(animated: true)
                //                        DispatchQueue.main.async {
                //                            navController?.pushViewController(controller, animated: true)
                //                        }
                //
                //                    } else {
                //                        topVc.navigationController?.pushViewController(controller, animated: true)
                //                    }
                //                }
                
                self.userInforForNotification = [:]
                NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil)
                
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
}

extension AppDelegate {
    func GetBookingDetails() {
        let BookingDetailsModel = BookingDetails()
        BookingDetailsModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        BookingDetailsModel.type = "2"
        WebserviceForBookingDetails(reqModel: BookingDetailsModel)
    }
    func WebserviceForGetAvailability(reqModel:AvailabiltyData) {
        WebServiceSubClass.GetAvailabilityData(Data: reqModel, completion: {(json, status, response) in
            
            if status {
                print(json["availabile_for"].stringValue)
                let AvailibilityArray = json["availabile_for"].stringValue.split(separator: ",")
                if AvailibilityArray.contains("chat") {
                    SingletonClass.sharedInstance.adviserTextChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserTextChatOn = false
                }
                
                if AvailibilityArray.contains("audio") {
                    SingletonClass.sharedInstance.adviserAudioChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserAudioChatOn = false
                }
                
                if AvailibilityArray.contains("video") {
                    SingletonClass.sharedInstance.adviserVideoChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserVideoChatOn = false
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.setTopAvability()
                                    break
                                }
                            }
                        }
                        if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                            
                            if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                                
                                for controller in topVC as Array {
                                    if controller.isKind(of: AdviserMyAccountViewController.self) {
                                        let MyAccount = controller as! AdviserMyAccountViewController
                                        MyAccount.setTopAvability()
                                        break
                                    }
                                }
                            }
                            
                        }
                    }
                })
                
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    func WebserviceForBookingDetails(reqModel:BookingDetails) {
        WebServiceSubClass.GetBookingDetails(Data: reqModel, completion: {(json, status, response) in
            if status {
                self.logMessage(messageText: "Voice Chat: GetBookingDetails passed")
                let resData = BookingDetailsForSession.init(fromJson: json)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    let controller:AdvisorAudioCallViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorAudioCallViewController.storyboardID) as! AdvisorAudioCallViewController
                    
                    controller.hidesBottomBarWhenPushed = true
                    
                    controller.CustomerName = resData.bookingDetails.customerName
                    
                    controller.CustomerUserImageURl = resData.bookingDetails.customerProfilePicture
                    
                    
                    controller.bookingID = resData.bookingDetails.bookingId
                    
                    controller.AdvisorID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
                    controller.customerID = resData.bookingDetails.userId
                    self.isPickedUp = 1
                    if let _ = UIApplication.topViewController()?.navigationController {
                        self.logMessage(messageText: "Voice Chat: GetBookingDetails nav found")
                    } else {
                        self.logMessage(messageText: "Voice Chat: GetBookingDetails not nav found")
                    }
                    let window = UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first
                    if let tab = (window?.rootViewController as? UINavigationController)?.viewControllers.first as? CustomTabBarVC, let nav = tab.selectedViewController as? UINavigationController {
                        self.logMessage(messageText: "Voice Chat: GetBookingDetails found 1")
                        nav.pushViewController(controller, animated: true)
                    } else {
                        self.logMessage(messageText: "Voice Chat: GetBookingDetails not found 1")
                        UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                    let param = ["user_id" : Int(resData.bookingDetails.userId ?? "") ?? 0,
                                 "advisor_id": Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
                                 "advisor_name" : resData.bookingDetails.advisorName,
                                 "booking_id" : Int(resData.bookingDetails.bookingId ?? "") ?? 0] as [String: Any]
                    
                    SocketIOManager.shared.socketEmit(for: socketApiKeys.PickupCall.rawValue, with: param)
                    self.OnPickupCall()
                    //Timer start
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.totalMinutesTimer = 1
                                    
                                    HomeVC.TotalTimecounter = 0
                                    
                                    HomeVC.startTimer(withInterval: 1.0)
                                    break
                                }
                            }
                        }
                    }
                })
                
            } else {
                self.logMessage(messageText: "Voice Chat: GetBookingDetails failed")
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    func OnPickupCall()
    {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.PickupCall.rawValue) { (response) in
            
            print(#function)
            print(response)
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["status"].string?.lowercased() == "accepted" {
                        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
                    } else if singleResponse["status"].string?.lowercased() == "rejected" {
                        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
                        if let topVC = UIApplication.topViewController() {
                            
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                            }
                            
                            
                        }
                    }
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
            
            
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
        }
    }
    func navigateToCallController(withData data : [String:Any])
    {
        logMessage(messageText: "navigateToCallController")
        print(#function)
        print(data)
        let response = data// {
        
        let roomName = JSON(response["room"] as Any).stringValue // response.first?["room"] as? String ?? ""
        let senderName = JSON(response["name"] as Any).stringValue // response.first?["name"] as? String ?? ""
        let visitID = JSON(response["booking_id"] as Any).int // "\(response.first?["visit_id"] as? Int ?? 0)"
        let sender_id = JSON(response["user_id"] as Any).int
        let token = JSON(response["token"] as Any).stringValue
        let reciverID = JSON(response["receiver_id"] as Any).int
        let story = UIStoryboard(name: "AdviserMain", bundle: nil)
        guard let vc = story.instantiateViewController(withIdentifier: "CallControllerViewController") as? CallControllerViewController else { return }
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: CallControllerViewController.self) {
                return
            }
        }
        
        vc.senderID = "\(sender_id ?? 0)"
        
        vc.reciverID = "\(reciverID ?? 0)"
        vc.bookingID = "\(visitID ?? 0)"
        
        vc.senderName = senderName
        
        vc.CallingTo = senderName
        vc.visit_id = "\(visitID ?? 0)"
        self.strRoomName = roomName
        vc.vetID = "\(sender_id ?? 0)"
        vc.token = token
        vc.navigationController?.isNavigationBarHidden = true
        vc.modalPresentationStyle = .fullScreen
        
        if let topVC =  UIApplication.topViewController() as? UINavigationController
        {
            //            topVC.pushViewController(vc, animated: true)
            print("operoller")
            topVC.present(vc, animated: true, completion: nil)
        }
        else
        {
            //            UIApplication.topViewController()?.navigationController?.pushViewController(vc, animated: true)
            print("opening from normall vc")
            UIApplication.topViewController()?.present(vc, animated: true, completion: nil)
        }
        VideoCallScreenOpen = true
        
    }
}

extension UIApplication{
    class func getPresentedViewController() -> UIViewController? {
        var presentViewController = UIApplication.shared.keyWindow?.rootViewController
        while let pVC = presentViewController?.presentedViewController
        {
            presentViewController = pVC
        }
        
        return presentViewController
    }
    class func topViewController(base: UIViewController? = (UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first)?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}
class AvailabilityToggleModel {
    var type: String?
    var status: String?
    
    init(type:String,status:String) {
        self.type = type
        self.status = status
    }
    
    var dictionaryDataCart : [String : String] {
        
        var objDict : [String : String]!
        
        
        objDict["type"] = type
        
        
        objDict["status"] = status
        
        
        return objDict;
    }
    func toDictionary() -> [String:String]
    {
        var dictionary = [String:String]()
        if type != nil{
            dictionary["type"] = type
        }
        if status != nil{
            dictionary["status"] = status
        }
        
        return dictionary
    }
    class func convertDataCartArrayToProductsDictionary(arrayDataCart : [AvailabilityToggleModel]) -> [[String:String]] {
        var arrayDataCartDictionaries : [[String:String]] = []
        for objDataCart in arrayDataCart {
            print(objDataCart)
            
            arrayDataCartDictionaries.append(objDataCart.toDictionary());
        }
        return arrayDataCartDictionaries
    }
    
}
