//
//  ApiPaths.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 01/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
import Alamofire
typealias NetworkRouterCompletion = ((Data?,[String:Any]?, Bool) -> ())
var userDefault = UserDefaults.standard
let kKey = "TempleBliss123*#*" // Before Login API

let SOCKET_URL = isDevelopmentMode ? "https://www.templebliss.co:9090" : "https://www.templebliss.co:8080"

enum UserDefaultsKey : String {
    case isUserSkip = "isUserSkip"
    case isUserFirstTime = "isUserFirstTime"
    case isUserLoginAsAdviser = "isUserLoginAsAdviser"
    case userProfile = "userProfile"
    case isUserLogin = "isUserLogin"
    case isUserLoginAsCustomer = "isUserLoginAsCustomer"
    case X_API_KEY = "X_API_KEY"
    case DeviceToken = "DeviceToken"
    case selLanguage = "selLanguage"
    case selectedUserType = "usertype"
    case toggleChatOn = "toggleChatOn"
    case toggleAudioOn = "toggleAudioOn"
    case toggleVideoOn = "toggleVideoOn"
}

enum APIEnvironment : String{
    case PromoCodeBaseURL = "http://52.91.192.185/api/"
    case PromoCodeProfileBaseURL = "http://52.91.192.185/"
    
    case ProductionBaseURl = "https://www.templebliss.co/api/"
    case ProductionProfileBaseUrl = "https://www.templebliss.co/"
    
    case DevelopmentBaseURL = "https://www.templebliss.co/development/api/"
    case DevelopmentProfileBaseURL = "https://www.templebliss.co/development/"
    
    static var baseURL: String{
        return APIEnvironment.environment.rawValue
    }
    static var profileBu: String{
        return APIEnvironment.EnvironmentForProfile.rawValue
    }
    
    //PromoCode Server
    
    
    static var environment: APIEnvironment{
        //Need to change when app going to live
        return isDevelopmentMode ? .DevelopmentBaseURL : .ProductionBaseURl
    }
    static var EnvironmentForProfile: APIEnvironment{
        //Need to change when app going to live
        return isDevelopmentMode ? .DevelopmentProfileBaseURL : .ProductionProfileBaseUrl
    }
//
    //MARK: - Developers Server
//
//        static var environment: APIEnvironment{
//            //Need to change when app going to live
//            return .DBBaseURL
//
//        }
//        static var EnvironmentForProfile: APIEnvironment{
//            //Need to change when app going to live
//            return .DBProfileBaseURL
//
//        }
//    //MARK: - Development server
////
//        static var environment: APIEnvironment{
//            //Need to change when app going to live
//            return .DevelopmentBaseURL
//
//        }
//        static var EnvironmentForProfile: APIEnvironment{
//            //Need to change when app going to live
//            return .DevelopmentProfileBaseURL
//
//        }
    //MARK: - Live server
//    static var environment: APIEnvironment{
//
//        //Need to change when app going to live
//        return .ProductionBaseURl
//
//    }
//    static var EnvironmentForProfile: APIEnvironment{
//        //Need to change when app going to live
//        return .ProductionProfileBaseUrl
//
//    }
    static var headers : HTTPHeaders
    {
        
        
        if SingletonClass.sharedInstance.usertype == "2"{
            if userDefault.object(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) != nil  {
                
                if userDefault.object(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) as? Bool == true {
                    
                    if userDefault.object(forKey:  UserDefaultsKey.userProfile.rawValue) != nil {
                        
                        do {
                            if UserDefaults.standard.value(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) != nil,UserDefaults.standard.value(forKey:  UserDefaultsKey.isUserLoginAsAdviser.rawValue) as! Bool
                            {
                                
                                return ["X-API-KEY":SingletonClass.sharedInstance.Api_Key,"key": kKey]
                                
                            }
                            
                        }
                    }
                }
                
            }
        }else if SingletonClass.sharedInstance.usertype == "3"{
            if userDefault.object(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) != nil{
                
                if userDefault.object(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) as? Bool == true{
                    
                    if userDefault.object(forKey:  UserDefaultsKey.userProfile.rawValue) != nil {
                        
                        do {
                            if UserDefaults.standard.value(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) != nil,UserDefaults.standard.value(forKey:  UserDefaultsKey.isUserLoginAsCustomer.rawValue) as! Bool
                            {
                                //                                let userdata = userDefault.getUserData()
                                ////
                                //                                SingletonClass.sharedInstance.Api_Key = userdata?.profile.apiKey ?? ""
                                
                                return ["X-API-KEY":SingletonClass.sharedInstance.Api_Key,"key": kKey]
                                
                            }
                            
                        }
                    }
                }
                
            }
        }
        
        
        return ["key": kKey]
    }
    
}
enum ApiKey: String {
    
    case Init                                    = "customer/init/0.0.0/ios_customer/"
    case settings                                = "auth/settings_links"
    case ContactUs                               = "auth/contact_us_request"
    case appleLogin                              = "auth/apple_details"
    case countrycode                             = "auth/country_code"
    
    case login                                   = "auth/login"
    case editProfileCustomer                     = "customer/profile_update"
    case editProfileAdvisor                      = "advisor/profile_update"
    case socialLogin                             = "auth/social_login"
    case Register                                = "auth/register"
    case RegisterOtp                             = "auth/otp_register"
    case ForgotPassword                          = "auth/forgot_password"
    case ChangePassword                          = "customer/change_password"
    case Logout                                  = "auth/logout"
    
    
    //Customer
    
    case minutes                                = "customer/minutes"
    case categoryListForCustomer                = "customer/category_list"
    case adviserListForCustomer                 = "customer/advisor_list"
    case adviserListForCustomer1                = "customer/advisor_list1"
    case AddToFavourite                         = "customer/favourite_advisor"
    case NotifyMe                               = "customer/notify_me"
    case CustomerMySession                      = "customer/session_list"
    
    case CustomerGiveRatting                    = "customer/advisor_rating"
    case chat_history                           = "customer/chat_history"
    case delete_session                         = "customer/delete_session"
    case add_payment_card                       = "customer/add_payment_card"
    case my_payment_cards                       = "customer/my_payment_cards"
    case wallet_history                         = "customer/wallet_history"
    case card_delete                            = "customer/card_delete"
    case CustomerSeeAllRateAndReview            = "customer/rating_review"
    case add_amount                             = "customer/add_amount"
    case advisor_details                        = "customer/advisor_details"
    case first_time                             = "customer/first_time"
    case booking_details                        = "customer/booking_details"
    case account_delete                         = "customer/account_delete"

    
    
    //Adviser
    case initForAdviserData                      = "advisor/user_profile"
    case initForAdviser                          = "advisor/init/0.0.0/ios_app/"
    case CategoryForAdviser                      = "advisor/category_list"
    case training_section                        = "advisor/training_section/"
    case GetNotes                                = "advisor/notes/"
    case EndSession                              = "advisor/end_session"
    case setPostRegister                         = "advisor/set_post_register_detail"
    case changeAvailibalityStatus                = "advisor/change_availibilty"
    case changeAvailibalityStatus_1              = "advisor/change_availibilty1"
    case AdvisorMySession                        = "advisor/session_list"
    case AdvisorMyEarnings                       = "advisor/my_earning"
    case AdvisorRateAndReview                    = "advisor/rating_review"
    case save_session_note                       = "advisor/save_session_note"
    case withdraw                                = "advisor/withdraw"
    case get_availability                        = "advisor/get_availability"
    case AdvisorChatAcceptRequest                    = "commonController/accept_request"
}

enum socketApiKeys: String
{
//  case SocketBaseURL                               = "http://192.168.29.243:8080" //Mayur
//  case SocketBaseURL                               = "http://52.91.192.185:8080"
//  case SocketBaseURL                               = "http://52.45.10.207:8081"
//  case SocketBaseURL                               = "http://52.45.10.207:8080"
//    case SocketBaseURL                               = "https://www.templebliss.co:8080" //Live  (8080)
    
    
    case ConnectCustomer                             = "connect_customer"    // customer_id, lat, lng
    case connect_customer_interval                   = "connect_customer_interval"
    case connect_advisor_interval                    = "connect_advisor_interval"
    case connectAdvisor                              = "connect_advisor" // customer_id
    case advisor_inactive                            = "advisor_inactive" //
    case advisor_to_customer                         = "advisor_to_customer" //
    
    
    case booking_request                             = "booking_request"
    case request_reject                              = "request_reject" /// when advisor reject request of booking show it on customer side
    case request_accept                              = "request_accept" /// when advisor accept request of booking
    case send_message                                = "send_message" /// When advisor or customer wants to send message.
    case availibility_switch_off                     = "availibility_switch_off" /// When advisor is not response of accept request then after 30 second automatic off availibality
    case all_message_read                            = "all_message_read"
    case check_typing                                = "check_typing"
    case buy_more_minute                             = "buy_more_minute" ///For Buy more minutes when session is running
    case end_session                                 = "end_session" ///while session end emit this
    case advisor_buzy                                = "advisor_buzy"
    
    case VideoCall                                   = "video_call"
    case VideoCallRejectedByReceiver                 = "call_reject_by_advisor"
    case VideoCallRejectedBySender                   = "call_reject_by_user"
    case PickupCall                                  = "pick_up_call"
    case advisor_availability                        = "advisor_availability"
    
    
    case videoCallCustomer                           = "video_call_customer"
    case videoCallAdvisor                            = "video_call_advisor"
    
    case VoiceCall                                   = "voice_call"
    case voiceCallCustomer                           = "voice_call_customer"
    case voiceCallAdvisor                            = "voice_call_advisor"
    
    
    case stop_timer                                  = "stop_timer"
    case resume_timer                                = "resume_timer"
    
    case request_reject_disconnect                   = "request_reject_disconnect"
    
}
