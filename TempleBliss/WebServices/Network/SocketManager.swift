//
//  SocketManager.swift
//  CabRideDriver
//
//  Created by EWW-iMac Old on 29/04/19.
//  Copyright © 2019 baps. All rights reserved.
//

import Foundation
import SocketIO
import SwiftyJSON
import os.log
import UIKit

typealias CompletionBlock = ((JSON) -> ())?

class SocketIOManager: NSObject {
    static let shared = SocketIOManager()
    var socketStateChanged: ((Bool) -> ())?
    
//    let manager = SocketManager(socketURL: URL(string: socketApiKeys.SocketBaseURL.rawValue)!, config: [.log(false),.forceWebsockets(true)])

    let manager = SocketManager(socketURL: URL(string: SOCKET_URL)!, config: [.log(false),.forceWebsockets(true)])

    //,.reconnects(false)
    
    lazy var socket = manager.defaultSocket
    
    var isSocketOn = false
    var isWaitingMessageDisplayed:Bool = false
    
    override init() {
        super.init()
        print("ATDebug :: Socket init")
        
    }
    func goBackWithAll() {
        if let newTopVC = UIApplication.topViewController() {
            if newTopVC.isKind(of: AdvisorMessageChatViewController.self) {
                
                let vc = newTopVC as! AdvisorMessageChatViewController
                if vc.sessionType == "session" {
                    print(#function)
                    if newTopVC.isModal {
                        
                        newTopVC.dismiss(animated: true, completion: nil)
                        let GetSummaryReqModel = GetSessionSummary()
                        GetSummaryReqModel.booking_id = Int(vc.bookingID) ?? 0
                        GetSummaryReqModel.type = "advisor"
                        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
                    } else {
                        newTopVC.navigationController?.popViewController(animated: true)
                        let GetSummaryReqModel = GetSessionSummary()
                        GetSummaryReqModel.booking_id =  Int(vc.bookingID) ?? 0
                        GetSummaryReqModel.type = "advisor"
                        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
                    }
                } else if vc.sessionType == "free" {
                    
                }
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                        if topVC.isKind(of: AdviserHomeViewController.self) {
                            let HomeVC = topVC as! AdviserHomeViewController
                            HomeVC.stopTimer()
                        }
                    }
                }
            } else if newTopVC.isKind(of:  MessageViewController.self) {
                if let viewControllers = newTopVC.navigationController?.viewControllers
                {
                    for controller in viewControllers
                    {
                        if controller is AdviserDetailsViewController
                        {
                            let vc = controller as! AdviserDetailsViewController
                            vc.stopTimerOnEndSession()
                            vc.TimerForReminderScreen.invalidate()
                        }
                    }
                }
                let vc = newTopVC as! MessageViewController
                if vc.sessionType == "session" {
                    if newTopVC.isModal {
                        
                        newTopVC.dismiss(animated: true, completion: nil)
                        let GetSummaryReqModel = GetSessionSummary()
                        GetSummaryReqModel.booking_id = Int(vc.bookingID) ?? 0
                        GetSummaryReqModel.type = "customer"
                        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
                    } else {
                        newTopVC.navigationController?.popViewController(animated: true)
                        let GetSummaryReqModel = GetSessionSummary()
                        GetSummaryReqModel.booking_id =  Int(vc.bookingID) ?? 0
                        GetSummaryReqModel.type = "customer"
                        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
                    }
                } else if vc.sessionType == "free" {
                    
                }
            } else if newTopVC.isKind(of: CustomerVideoCallViewController.self) {
                let vc = newTopVC as! CustomerVideoCallViewController
                vc.stopTimerOnEndSession()
                vc.TimerForReminderScreen.invalidate()
                
                if newTopVC.isModal {
                    newTopVC.dismiss(animated: true, completion: nil)
                } else {
                    newTopVC.navigationController?.popViewController(animated: true)
                }
            } else if newTopVC.isKind(of: CallControllerViewController.self) {
                
                if newTopVC.isModal {
                    newTopVC.dismiss(animated: true, completion: nil)
                } else {
                    newTopVC.navigationController?.popViewController(animated: true)
                }
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                        if topVC.isKind(of: AdviserHomeViewController.self) {
                            let HomeVC = topVC as! AdviserHomeViewController
                            HomeVC.stopTimer()
                        }
                    }
                }
            } else if newTopVC.isKind(of: AudioCallViewController.self) {
                let VC = newTopVC as! AudioCallViewController
                VC.isPickedUp = nil
                VC.stopTimerOnEndSession()
             
                if newTopVC.isModal {
                    newTopVC.dismiss(animated: true, completion: nil)
                } else {
                    newTopVC.navigationController?.popViewController(animated: true)
                }
            } else if newTopVC.isKind(of: AdvisorAudioCallViewController.self) {
                
                let VC = newTopVC as! AdvisorAudioCallViewController
                appDel.GoBack()
                if newTopVC.isModal {
                    newTopVC.dismiss(animated: true, completion: nil)
                } else {
                    newTopVC.navigationController?.popViewController(animated: true)
                }
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                        if topVC.isKind(of: AdviserHomeViewController.self) {
                            let HomeVC = topVC as! AdviserHomeViewController
                            HomeVC.stopTimer()
                        }
                    }
                }
            }
        }
    }
    func goToBack() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            if let newTopVC = UIApplication.topViewController() {
                if newTopVC.isKind(of: UIAlertController.self) {
                    newTopVC.dismiss(animated: true, completion: {
                        
                        DispatchQueue.main.async {
                            if let TopVC = UIApplication.topViewController() {
                                if TopVC.isKind(of: MessageViewController.self) || TopVC.isKind(of: AdvisorMessageChatViewController.self) || TopVC.isKind(of: CallControllerViewController.self) || TopVC.isKind(of: CustomerVideoCallViewController.self) || TopVC.isKind(of: AudioCallViewController.self) || TopVC.isKind(of: AdvisorAudioCallViewController.self) {
                                    self.goBackWithAll()
                                } else {
                                    if TopVC.isKind(of: AdvisorRequestAcceptViewController.self) {
                                        let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
//                                        controller.currentRequestArrived = false
                                        currentRequestArrived = false
                                    }
                                    
                                    if TopVC.isModal {
                                        
                                        TopVC.dismiss(animated: true, completion: {
                                            self.goBackWithAll()
                                        })
                                    } else {
                                        TopVC.navigationController?.popViewController(animated: true)
                                        self.goBackWithAll()
                                    }
                                }
                            }
                        }
                        
                    })
                } else {
                    if newTopVC.isKind(of: MessageViewController.self) || newTopVC.isKind(of: AdvisorMessageChatViewController.self) || newTopVC.isKind(of: CallControllerViewController.self) || newTopVC.isKind(of: CustomerVideoCallViewController.self) || newTopVC.isKind(of: AudioCallViewController.self) || newTopVC.isKind(of: AdvisorAudioCallViewController.self) {
                        self.goBackWithAll()
                    } else {
                        if newTopVC.isKind(of: AdvisorRequestAcceptViewController.self) {
                            let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
//                            controller.currentRequestArrived = false
                            currentRequestArrived = false
                        }
                        
                        if newTopVC.isModal {
                            
                            newTopVC.dismiss(animated: true, completion: {
                                self.goBackWithAll()
                            })
                        } else {
                            newTopVC.navigationController?.popViewController(animated: true)
                            self.goBackWithAll()
                        }
                    }
                }
                
            }
        })
        
    }
    
    func goBackForAdvisor() {
        if let newTopVC = UIApplication.topViewController() {
            if newTopVC.isKind(of: AdvisorMessageChatViewController.self) || newTopVC.isKind(of: CallControllerViewController.self) {
                if newTopVC.isKind(of: AdvisorMessageChatViewController.self) {
                    
                    if newTopVC.isModal {
                        newTopVC.dismiss(animated: true, completion: nil)
                    } else {
                        newTopVC.navigationController?.popViewController(animated: true)
                    }
                    
                } else if newTopVC.isKind(of: CallControllerViewController.self) {
                    
                    if newTopVC.isModal {
                        newTopVC.dismiss(animated: true, completion: nil)
                    }else {
                        newTopVC.navigationController?.popViewController(animated: true)
                    }
                    
                }
            } else {
                if newTopVC.isModal {
                    newTopVC.dismiss(animated: true, completion: {
                        
                        if newTopVC.isModal {
                            newTopVC.dismiss(animated: true, completion: nil)
                        }else {
                            newTopVC.navigationController?.popViewController(animated: true)
                        }
                        
                    })
                }
            }
        }
    }
    
    func establishConnection() {
        if self.isSocketOn == false {
            socketMethodsOns()
            socket.connect()
        }
    }
    
    func reconnect() {
//        socketMethodsOns()
        if socket.manager?.status != .connected {
            logMessage(messageText: "Voice chat: Manual Connect")
            socket.connect()
        } else {
            logMessage(messageText: "Voice chat: Manual Reconnect")
            socket.setReconnecting(reason: "")
        }
    }
    
    func socketMethodsOns() {
        socket.on(clientEvent: .disconnect) { (data, ack) in
            logMessage(messageText: "Voice chat :: SocketManager socket is disconnected")
            print ("ATDebug :: SocketManager socket is disconnected")
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                if let provider = appDel.callKitProvider {
                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        appDel.callKitProvider?.invalidate()
                    })

                }

                let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
                controller.stopTimer()
                self.goToBack()
                
            } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true)  {
                
                let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
                controller.stopTimer()
                self.goToBack()
            }
        }
        socket.on(clientEvent: .statusChange) {data, ack in
            logMessage(messageText: "Voice chat :: SocketManager Status change")
            print("ATDebug :: SocketManager Status change: \(data)")
        }
        socket.on(clientEvent: .reconnect) { (data, ack) in
            self.socketStateChanged?(self.socket.status == .connected)
            logMessage(messageText: "Voice chat :: SocketManager socket is reconnected")
            print ("ATDebug :: SocketManager socket is reconnected")
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
//                if let provider = appDel.callKitProvider {
//                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//                        appDel.callKitProvider?.invalidate()
//                    })
//
//                }
                if appDel.activeCallInvites.keys.count == 0 && appDel.activeCalls.keys.count == 0 && !appDel.RequestFromChat {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                            if topVC.isKind(of: AdviserHomeViewController.self) {
                                let HomeVC = topVC as! AdviserHomeViewController
                                HomeVC.stopTimer()
                            }
                        }
                    }
                    self.goToBack()
                }
               
                
            } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true)  {
                
                self.goToBack()
            }
        }
        
        socket.on(clientEvent: .connect) { data, ack in
            self.socketStateChanged?(self.socket.status == .connected)
            logMessage(messageText: "Voice chat :: SocketManager socket is connected")
            print ("ATDebug :: SocketManager socket is connected")
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                SingletonClass.sharedInstance.StartTimerForAdvisor()
               
                //if appDel.activeCallInvites.keys.count == 0 {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                            if topVC.isKind(of: AdviserHomeViewController.self) {
                                let HomeVC = topVC as! AdviserHomeViewController
                                HomeVC.CallOnAllSocketToListen()
                            }
                        }
                    }
                //}
            } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true)  {
                SingletonClass.sharedInstance.StartTimerForCustomer()
                
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.topViewController {
                        if topVC.isKind(of: HomeViewController.self) {
                            let HomeVC = topVC as! HomeViewController
                            HomeVC.CallOnAllSocketToListen()
                        }
                    }
                }
            }
            
            if !self.isSocketOn {
                self.isSocketOn = true
            }
            
        }
        socket.on(clientEvent: .error) { data, ack in
            logMessage(messageText: "Voice chat :: SocketManager socket error")
            print(data)
            print ("ATDebug :: SocketManager socket error \(data)")
        }
    }
    
    func closeConnection() {
        
        socket.disconnect()
        socket.off(clientEvent: SocketClientEvent.connect)
        socket.off(clientEvent: SocketClientEvent.reconnect)
        socket.off(clientEvent: .disconnect)
        socket.off(clientEvent: .error)
        socket.off(clientEvent: .statusChange)
        
        self.isSocketOn = false
        
    }
    
    func socketCall(for key: String, completion: CompletionBlock = nil)
    {
        SocketIOManager.shared.socket.off(key)
        SocketIOManager.shared.socket.on(key, callback: { (data, ack) in
            let result = self.dataSerializationToJson(data: data)
            guard result.status else { return }
            if completion != nil { completion!(result.json) }
        })
    }
    
    func socketEmit(for key: String, with parameter: [String:Any]) {
//        print("ATDebug :: status Socket :: \(SocketIOManager.shared.socket.status)")
//        print("ATDebug :: status Newtowrk  :: \(WebService.shared.isConnectedToInternet())")
        if WebService.shared.isConnected {
            if SocketIOManager.shared.socket.status == .connected {
                socket.emit(key, parameter)
                logMessage(messageText: "Voice chat :: event emmited")
                print ("ATDebug :: SocketManager socketemit Parameter Emitted for key - \(key) :: \(parameter)")
            } else {
                print ("ATDebug :: SocketManager socketemit Parameter Emitted for key - \(key) :: \(parameter)")
                logMessage(messageText: "Voice chat :: socketEmit socket not connected 1")
            }
        } else {
            logMessage(messageText: "Voice chat :: socketEmit socket not connected 2")
        }
    }
    
    func dataSerializationToJson(data: [Any],_ description : String = "") -> (status: Bool, json: JSON){
        let json = JSON(data)
        //        print (description, ": \(json)")
        
        return (true, json)
    }
   
    
    func webserviceForGetSessionSummatyDetails(reqModel:GetSessionSummary) {
        
        print(#function)
        Utilities.showHud()
        WebServiceSubClass.SessionSummary(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                    
                    let SummaryModel = ShowSummaryResModel.init(fromJson: json)
                    let dataForShare:[String:String] = ["commission_rate": SummaryModel.data.commissionRate ?? "","session_earning": SummaryModel.data.sessionEarning ?? "","total_minute": SummaryModel.data.totalMinute ?? "","advisor_special_day_commission": SummaryModel.data.advisorSpecialDayCommission ?? "","category_name":SummaryModel.data.category_name ?? ""]
                    let alert = UIAlertController(title: AppInfo.appName, message: SummaryModel.message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                        let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                        controller.isFromCustomer = false
                        controller.dataForTable = dataForShare
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.modalPresentationStyle = .overCurrentContext
                        navigationController.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                    }))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    
                } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true)  {
                    let SummaryModel = ShowSummaryResModel.init(fromJson: json)
                    let dataForShare:[String:String] = ["total_minute":SummaryModel.data.totalMinute ?? "","free_minute":SummaryModel.data.freeMinute ?? "","booking_amount":SummaryModel.data.bookingAmount ?? "","total_chargeable_amount":SummaryModel.data.totalChargeableAmount ?? "","discount_type":SummaryModel.data.discountType ?? "","discount_value":SummaryModel.data.discountValue ?? "","category_name":SummaryModel.data.category_name ?? ""]
                    let alert = UIAlertController(title: AppInfo.appName, message: SummaryModel.message, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                        let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                        controller.isFromCustomer = true
                        controller.dataForTable = dataForShare
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.modalPresentationStyle = .overCurrentContext
                        navigationController.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                    }))
                    
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                }
                
            } else {
                
            }
        })
    }
    
}

func logMessage(messageText: String) {
    print(messageText)
    
    if #available(iOS 14.0, *) {
        let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "Komal")
        logger.log("\(messageText)")

    } else {
        // Fallback on earlier versions
    }
}
