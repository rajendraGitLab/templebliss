//
//  WebServicesSubClass.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 01/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
import UIKit
class WebServiceSubClass
{
    
    class func initApi( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        //WebService.shared.getMethodForInit(url: URL.init(string: strURL)!, httpMethod: .get, completion: completion)//requestMethod(api: .update, httpMethod: .get, parameters: strType, completion: completion)
        WebService.shared.getMethod(api: .Init, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func initForAdviser( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        //WebService.shared.getMethodForInit(url: URL.init(string: strURL)!, httpMethod: .get, completion: completion)//requestMethod(api: .update, httpMethod: .get, parameters: strType, completion: completion)
        WebService.shared.getMethod(api: .initForAdviser, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func TrainingSection( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .training_section, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func GetNotes( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .GetNotes, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func minutes( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .minutes, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func GetCountryCode( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .countrycode, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
   
    class func AdviserDetails( CategoryModel : getAdviserData ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .initForAdviserData, httpMethod: .post, parameters: params, completion: completion)
    }
   
    class func SettingsApi( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .settings, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    
    class func appleLogin( CategoryModel : appleDetailsReqModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .appleLogin, httpMethod: .post, parameters: params, completion: completion)
    }
    
    
    class func CategoryForAdviser( strParams : String ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        WebService.shared.getMethod(api: .CategoryForAdviser, parameterString: strParams, httpMethod: .get,showHud: showHud, completion: completion)
    }
    class func CategoryForCustomer( CategoryModel : CategoryReqModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .categoryListForCustomer, httpMethod: .post, parameters: params, completion: completion)
    }
    
    class func adviserList( CategoryModel : AdviserListRequestModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .adviserListForCustomer, httpMethod: .post, parameters: params, completion: completion)
    }
    class func adviserList1( CategoryModel : AdviserListRequestModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .adviserListForCustomer1, httpMethod: .post, parameters: params, completion: completion)
    }
    class func AddToFavourite( CategoryModel : FavouriteReqModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .AddToFavourite, httpMethod: .post, parameters: params, completion: completion)
    }
    class func NotifyMe( CategoryModel : NotifyMeReqModel ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .NotifyMe, httpMethod: .post, parameters: params, completion: completion)
    }
    
    class func register( registerModel : RegisterReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = registerModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .Register, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func registerOtp( registerOtpModel : RegisterReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = registerOtpModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .RegisterOtp, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func login( loginModel : LoginReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = loginModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .login, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func ForgotPassword( forgotPassword : ForgotPasswordReqModel  , showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] =  forgotPassword.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .ForgotPassword, httpMethod: .post, parameters: params, completion: completion)
    }
    class func socialLogin( socialloginModel : userSocialData  ,showHud : Bool = false,completion: @escaping CompletionResponseForLogin ) {
        let  params : [String:String] = socialloginModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethodLogin(api: .socialLogin, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func ChangePassword( changepassModel : ChangePasswordReqModel,showHud : Bool = false  ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = changepassModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .ChangePassword, httpMethod: .post, parameters: params, completion: completion)
    }
    class func UpdateProfileCustomer( editProfileModel : EditProfileReqModel  ,img : UIImage,isRemoveImage: Bool ,showHud : Bool = false , completion: @escaping CompletionResponse ) {
        let  params : [String:String] = editProfileModel.generatPostParams() as! [String : String]
        WebService.shared.postDataWithImage(api: .editProfileCustomer, isRemoveimage: isRemoveImage, showHud: false, parameter: params, image: img, imageParamName: "profile_picture", completion: completion)
    }
    class func UpdateProfileAdvisor( editProfileModel : EditprofileAdvisorReqModel  ,img : UIImage,isRemoveImage: Bool ,showHud : Bool = false , completion: @escaping CompletionResponse ) {
        
        let  params : [String:Any] = editProfileModel.generatPostParams() 
       
        WebService.shared.postDataWithImage(api: .editProfileAdvisor, isRemoveimage: isRemoveImage, showHud: false, parameter: params, image: img, imageParamName: "profile_picture", completion: completion)
    }
    class func LogOut( logOutModel : LogoutReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = logOutModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .Logout, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func setPostRegisterDetails( addCategory : setAddNewCategory  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .setPostRegister, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AdvisorDetails( addCategory : AdvisorDetailsReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .advisor_details, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func ContactUs( addCategory : contactReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .ContactUs, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
//    class func ChangeAvailibalityStatus( addCategory : changeAvailibalityStatusReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
//        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
//        WebService.shared.requestMethod(api: .changeAvailibalityStatus, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
//    }
    class func webServiceCallForToggleWithAllToggle( CategoryModel : changeAvailibalityStatusReqModelWithAllToggle ,showHud : Bool = false ,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = CategoryModel.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .changeAvailibalityStatus_1, httpMethod: .post, parameters: params, completion: completion)
    }
    class func CustomerMySession( addCategory : CustomerMySessionReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .CustomerMySession, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func SessionSummary( addCategory : GetSessionSummary  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .EndSession, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AdvisorMySession( addCategory : AdvisorMySessionReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .AdvisorMySession, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AdvisorMyEarnings( addCategory : AdvisorMyEarningsReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .AdvisorMyEarnings, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AdvisorWithDraw( addCategory : withDrawMoneyReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .withdraw, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func CustomerGiveRatting( addCategory : CustomerGiveRattingReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .CustomerGiveRatting, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetChatHistory( addCategory : getChatHistoryReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .chat_history, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func delete_session( addCategory : deleteSessionCustomerReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .delete_session, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AdvisorRateAndReview( addCategory : AdvisorRateAndReviewReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .AdvisorRateAndReview, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func SeeAllReviews( addCategory : SeeAllRateAndReviewReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .CustomerSeeAllRateAndReview, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AddCard( addCategory : AddCardReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .add_payment_card, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetMyCards( addCategory : GetMyCardsReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .my_payment_cards, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetWalletHistory( addCategory : GetMyCardsReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .wallet_history, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func DeleteCards( addCategory : DeleteCardsReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .card_delete, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AddNotes( addCategory : SessionNoteReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .save_session_note, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func AddAmount( addCategory : AddAmountReqModel  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .add_amount, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetFirstTime( Data : GetFirstTimeData  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = Data.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .first_time, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetBookingDetails( Data : BookingDetails  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = Data.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .booking_details, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    class func GetAvailabilityData( Data : AvailabiltyData  ,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = Data.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .get_availability, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    
    class func DeleteAccount( Data : DeleteAccountData, showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = Data.generatPostParams() as! [String : String]
        
        WebService.shared.requestMethod(api: .account_delete, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
    
    class func advisorChatRequestAccept( addCategory : AdvisorChatAcceptRequestModel,showHud : Bool = false,completion: @escaping CompletionResponse ) {
        let  params : [String:String] = addCategory.generatPostParams() as! [String : String]
        WebService.shared.requestMethod(api: .AdvisorChatAcceptRequest, httpMethod: .post,showHud: showHud, parameters: params, completion: completion)
    }
}


