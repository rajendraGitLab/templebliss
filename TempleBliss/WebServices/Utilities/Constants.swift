//
//  Constants.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 02/10/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
import UIKit

let Currency = "$"

let keywindow = UIApplication.shared.keyWindow

let isDevelopmentMode = false

let appDel = UIApplication.shared.delegate as! AppDelegate
let kAPPVesion = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as! String
let AppName = AppInfo.appName
let AppURL = AppInfo.appUrl // "itms-apps://itunes.apple.com/app/id1547969270"
let ReqDeviceType = "1"
let Headerkey = "TempleBliss123*#*"



// Client account - Live Key
let StripeLivePublishKey = "pk_live_51HiRQOHDsygfVRFHVQROD80xWKUbfaFnFdTMSzdoWb1h9zsdW7X04Klj2Y9JCdSTU1mYjngNR7EvHVZYIr2RKgmH00pltFsgYt"
let StripeLiveSecretKey = "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"

// Client account - Test stripe key
let StripeTestPublishKey = "pk_test_51HiRQOHDsygfVRFHHEeLwGQaT1nRUlAvt1cEzOZJCFmkAcHj5TfVwkOYicxRDOvekQ3VULQcKg3KxOpmUM5GWopZ009M8CkT0X"
let StripeTestSecretKey = "sk_test_51HiRQOHDsygfVRFH5uIWpqhWkuPjqU1StFxuZwYftBQGzIQwCtSwycRNT9abnLHFPnkdH7syiH94i1eb6xZ9ZhUQ00NVp49aYm"


let themeColor = hexStringToUIColor(hex: "2ab6b6")
let themeColorOrange = hexStringToUIColor(hex: "ff8b7b")
let themeColorOffWhite = hexStringToUIColor(hex: "efecef")

let themeStatusRed = hexStringToUIColor(hex: "DF1A49")
let themeStatusYellow = hexStringToUIColor(hex: "EFB818")
let themeStatusGreen = hexStringToUIColor(hex: "1AC62F")


let NotificationBadges = NSNotification.Name(rawValue:"NotificationBadges")

var NameTotalCount = 30

var isFreePopUpShown = false

let RingtoneSoundName = "Reflection"
let CallKitIconName = "notification_icon"

enum DateFormatterString : String{
    case timeWithDate = "yyyy-MM-dd HH:mm:ss"
    case onlyDate = "dd-MM-yyyy"
}


var RequestRejectMessage = "Request rejected"


struct DeviceType {
    
    static var hasTopNotch: Bool {
        if #available(iOS 11.0, tvOS 11.0, *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
    
    static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_SE         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_7          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_7PLUS      = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
    static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

struct ScreenSize {

    static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)

}

enum GlobalStrings : String{
    case Alert_logout = "Are you sure you want to logout ?"
}
enum ErrorMessages : String{
    case Invelid_Otp = "Invalid OTP"
}

struct AnalyticsEvents {
    static let imCustomer = "Customer_Login_Button"
    static let imAdvisor = "Advisor_Login_Button"
    static let C_skipLogin = "Skip_Login_Page"
    static let C_login = "Customer_Login"
    static let C_signUp = "Customer_SignUp_button_clicked"
    static let C_registerNow = "Customer_Successful_Signup"
    static let C_skipTour = "Customer_Skip_Tour"
    static let C_nextBtnClick = "Customer_Next_button_click"
    static let C_psychicReading = "Customer_Psychic_Reading"
    static let C_tarotReading = "Customer_Tarot_Reading"
    static let C_astrology = "Customer_Astrology"
    static let C_dreamAnalysis = "Customer_Dream_Analysis"
    static let C_reiki = "Customer_Reiki"
    static let C_lifeRelationship = "Customer_Life_Relationship"
    static let C_textChatAdvisor = "Customer_Text_Chat_To_Advisor"
    static let C_audioCall = "Customer_Audio_Call_To_Advisor"
    static let C_videoCall = "Customer_Video_Call_To_Advisor"
    static let C_addMoney = "Customer_Add_Money"
    static let C_addCard = "Customer_Add_Card"
    static let C_saveCard = "Customer_Save_Card"
}
