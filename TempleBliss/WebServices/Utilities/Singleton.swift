//
//  Singleton.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 09/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
import SwiftyJSON
class SingletonClass: NSObject
{
    var OpenFirstTimeListing = false
    var WorkingChat : Bool = false
    
    
    var DataForAudioVideo : [String:Any] = [:]
    var freeMinutesAdded : (Bool,String) = (false,"0")
    
    var AdvisorAudioCallData : JSON?
    var FreeMinutes = ""
    
    static let sharedInstance = SingletonClass()
    
    var CountryNameWithCode : [CountryCode] = []
    
    var initDataForAdviser : InitResponseForAdviser?
    
    var categoryDataForAdviser : CategoryResponseForAdviser?
    var categoryListCustomer : CategoryListForCustomer?
    var JsonForCategory = ""
    
    var adviserAudioChatOn : Bool = false
    var adviserVideoChatOn : Bool = false
    var adviserTextChatOn : Bool = false
    var usertype = ""
    
    var UserId = String()
    var LoginRegisterUpdateData : UserInfo?
    
    var loginForCustomer : userInforForCustomer?
    
    var Api_Key = String()
    var DeviceToken : String = ""
    
    
    var settingsModel : settingsResModel?
    
    
    var TodayDate = Date()
    
    var isCommingCall = false
    
    var arrFutureYears:[String] {
        get {
            let calendar = Calendar.current
            let currentYear = calendar.component(.year, from: Date())
            return (currentYear...(currentYear + 11)).map { String($0)}
        }
    }
    
    func clearSingletonClass() {
        
        SingletonClass.sharedInstance.freeMinutesAdded = (false,"0")
        SingletonClass.sharedInstance.FreeMinutes = ""
        
        SingletonClass.sharedInstance.UserId = ""
        SingletonClass.sharedInstance.LoginRegisterUpdateData = nil
        SingletonClass.sharedInstance.loginForCustomer = nil
        SingletonClass.sharedInstance.Api_Key = ""
        
        SingletonClass.sharedInstance.adviserAudioChatOn = false
        SingletonClass.sharedInstance.adviserVideoChatOn = false
        SingletonClass.sharedInstance.adviserTextChatOn = false
        
    }
    
    var dataForShareForBuyMinutes : [String:Any] = [:]
    
    var isNoteAdded = false
    var noteText = String()
    
    var minutesArray : [String] = []
     
    var selectedCategoryIndex = -1
    
   var WalletBalanceForMaintain = ""
    
    var TimerForCustomer = Timer()
    var TimerForAdvisor = Timer()
    
    func StartTimerForCustomer() {
        TimerForCustomer.invalidate()
        TimerForAdvisor.invalidate()
        
        if SocketIOManager.shared.socket.status == .connected {
            let joinAdvisor = ["customer_id": Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0] as [String : Any]
            print("ATDebug Singlton \(#function)")
            SocketIOManager.shared.socketEmit(for: socketApiKeys.ConnectCustomer.rawValue , with: joinAdvisor)
            
            TimerForCustomer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
                let JoinChatSocketParams = ["customer_id": Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0] as [String : Any]
                
                SocketIOManager.shared.socketEmit(for: socketApiKeys.connect_customer_interval.rawValue , with: JoinChatSocketParams)
            }
        }
    }
    func StartTimerForAdvisor() {
        TimerForCustomer.invalidate()
        TimerForAdvisor.invalidate()
        if SocketIOManager.shared.socket.status == .connected {
            let joinAdvisor = ["advisor_id": Int(SingletonClass.sharedInstance.UserId) ?? 0] as [String : Any]
            print("ATDebug Singlton \(#function)")
            SocketIOManager.shared.socketEmit(for: socketApiKeys.connectAdvisor.rawValue , with: joinAdvisor)
            TimerForAdvisor = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
                let JoinChatSocketParams = ["advisor_id": Int(SingletonClass.sharedInstance.UserId) ?? 0] as [String : Any]
                
                SocketIOManager.shared.socketEmit(for: socketApiKeys.connect_advisor_interval.rawValue , with: JoinChatSocketParams)
            }
        }
    }
    func StopCustomerTimer() {
        
        TimerForCustomer.invalidate()
    }
    
    func StopAdvisorTimer() {
        
        TimerForAdvisor.invalidate()
    }
}
