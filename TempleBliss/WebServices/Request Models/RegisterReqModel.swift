//
//  RegiReqModel.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 09/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
class RegisterReqModel : RequestModel {
    var full_name : String = ""
    var email : String = ""
    var phone : String = ""
    var device_token : String = ""
    var device_type : String = ReqDeviceType
    var password : String = ""
    var dob : String = ""
    var role : String = ""
    var confirm_password : String = ""
    var nick_name : String = ""
    var advisor_description : String = ""
    var social_id : String = ""
    var social_type : String = ""
}
class appleDetailsReqModel : RequestModel {
    var apple_id : String = ""
    var email : String = ""
    var first_name : String = ""
    var last_name : String = ""
}
