//
//  AdviserListRequestModel.swift
//  TempleBliss
//
//  Created by Apple on 29/01/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation

class AdviserListRequestModel : RequestModel {
    var category_id : String = ""
    var user_id : String = ""
    var type : String = ""
    var search_string : String = ""
    /// 0 for not favourite 1 for favourite
    var is_favourite : String = ""
    var page : String = ""
}
