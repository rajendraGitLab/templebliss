//
//  ProfileUpdateReqModel.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 15/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation
class EditProfileReqModel : RequestModel{
    var user_id : String = ""
    var full_name : String = ""
    var email : String = ""
    var phone : String = ""
    var profile_picture : String = ""
    var remove_image : String = ""
    var is_change : String = ""
}
class EditprofileAdvisorReqModel : RequestModel{
    var user_id : String = ""
    var full_name : String = ""
    var nick_name : String = ""
    var email : String = ""
    var phone : String = ""
    var availability : String = ""
    var advisor_description: String = ""
    var remove_image : String = ""
    var profile_picture : String = ""
    var is_change_availability : String = ""
    var is_change : String = ""
}
