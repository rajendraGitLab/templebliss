//
//  LoginReqModel.swift
//  Virtuwoof Pet
//
//  Created by EWW80 on 09/11/19.
//  Copyright © 2019 EWW80. All rights reserved.
//

import Foundation

class LoginReqModel : RequestModel {
    var user_name : String = ""
    var password : String = ""
    var device_token : String = ""
    var device_type : String = ReqDeviceType
    var role : String = "3"
}

struct UserRegistrationRequest : Encodable
{
    let FirstName, LastName, Email, Password : String

    enum CodingKeys: String, CodingKey {
        case FirstName = "First_Name"
        case LastName = "Last_Name"
        case Email, Password
    }
}

class ForgotPasswordReqModel : RequestModel
{
    var email : String = ""
    var role : String = ""
}

class ChangePasswordReqModel: RequestModel {
    var user_id : String = ""
    var old_password : String = ""
    var new_password : String = ""
    var new_confirm_password : String = ""
}
class LogoutReqModel: RequestModel{
    var user_id : String = ""
}

class contactReqModel : RequestModel {
    var user_id : String = ""
    var full_name : String = ""
    var email : String = ""
    var phone : String = ""
    var message : String = ""
    
}
class getAdviserData : RequestModel
{
    var user_id : String = ""
}
class changeAvailibalityStatusReqModel : RequestModel {
    var user_id : String = ""
    var type : String = ""
    var status : String = ""
}
class changeAvailibalityStatusReqModelWithAllToggle : RequestModel {
    var user_id : String = ""
    var status : String = ""
}

