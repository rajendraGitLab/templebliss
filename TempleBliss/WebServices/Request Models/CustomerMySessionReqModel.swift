//
//  CustomerMySessionReqModel.swift
//  TempleBliss
//
//  Created by Apple on 08/03/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation

class CustomerMySessionReqModel : RequestModel{
    var user_id = ""
    var page = ""
}
class AdvisorMySessionReqModel : RequestModel{
    var user_id = ""
    var page = ""
}
class AdvisorMyEarningsReqModel : RequestModel{
    var advisor_id = ""
    var filter = ""
    var page = ""
    
}
class withDrawMoneyReqModel : RequestModel{
    var advisor_id = ""
    var amount = ""
}

class CustomerGiveRattingReqModel : RequestModel{
    var user_id = ""
    var advisor_id = ""
    var booking_id = ""
    var rating = ""
    var note = ""
}
class getChatHistoryReqModel : RequestModel {
    var booking_id = ""
    var chat_type = ""
}

class deleteSessionCustomerReqModel : RequestModel {
    var booking_id = ""
    var user_id = ""
}
class AdvisorRateAndReviewReqModel : RequestModel {
    var user_id = ""
}
class SeeAllRateAndReviewReqModel : RequestModel {
    var advisor_id = ""
}
class AddCardReqModel : RequestModel {
  
    var user_id = ""
    var card_holder_name = ""
    var card_number = ""
    var expiry_date = ""
    var cvv = ""
    var card_type = ""
    var zipcode = ""
}
class GetMyCardsReqModel : RequestModel {
    var user_id = ""
    var page = ""
}
class DeleteCardsReqModel : RequestModel {
    var card_id = ""
}
class SessionNoteReqModel : RequestModel {
    var booking_id = ""
    var note = ""
}
class AddAmountReqModel : RequestModel {
    var card_id = ""
    var amount = ""
    var booking_id = ""
    var is_apple = 0
    var user_id = ""
    var stripe_token = ""
}
class GetSessionSummary : RequestModel {
    var booking_id = 0
    var type = ""
}
class GetFirstTimeData : RequestModel {
    var user_id = ""
}
class BookingDetails : RequestModel {
    var user_id = ""
    var type = ""
}
class AvailabiltyData : RequestModel {
    var advisor_id = ""
}

class DeleteAccountData : RequestModel {
    var user_id = 0
    var email_id = ""
}

class AdvisorChatAcceptRequestModel : RequestModel{
    var booking_id = 0
    var user_id = 0
    var advisor_id = 0
    var type = "chat"
}
