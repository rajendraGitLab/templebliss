//
//  CategoryAddRequestModel.swift
//  TempleBliss
//
//  Created by Apple on 25/01/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation

class setAddNewCategory : RequestModel {
    var user_id : String = ""
    var availability : String = ""
    var account_detail : String = ""
}

class AdvisorDetailsReqModel : RequestModel {
    var user_id : String = ""
    var advisor_id : String = ""
}

class CategoryAddRequestModel {
    var category_id = ""
    var type = ""
    var price = ""
    
    init(id:String,CategoryType:String,CategoryPrice:String) {
        self.category_id = id
        self.type = CategoryType
        self.price = CategoryPrice
    }

    
    
    var dictionaryDataCart : [String : String] {
        
        var objDict : [String : String]!
        
        
        objDict["category_id"] = category_id
        
        
        objDict["type"] = type
        objDict["price"] = price
        
        return objDict;
    }
    func toDictionary() -> [String:String]
    {
        var dictionary = [String:String]()
        if category_id != nil{
            dictionary["category_id"] = category_id
        }
        if type != nil{
            dictionary["type"] = type
        }
        if price != nil{
            dictionary["price"] = price
        }
        
        return dictionary
    }
    class func convertDataCartArrayToProductsDictionary(arrayDataCart : [CategoryAddRequestModel]) -> [[String:String]] {
        var arrayDataCartDictionaries : [[String:String]] = []
        for objDataCart in arrayDataCart {
            print(objDataCart)
            
            arrayDataCartDictionaries.append(objDataCart.toDictionary());
        }
        return arrayDataCartDictionaries
    }
  
   
}

class CategoryUpdateRequestModel {
    var current_category_id = ""
    var new_category_id = ""
    var type = ""
    var current_price = ""
    var new_price = ""
    var last_updated_date = ""
    init(idOld:String,idNew:String,CategoryType:String,CategoryPrice:String,newPrice:String,lastUpdateDate:String) {
        self.current_category_id = idOld
        self.new_category_id = idNew
        self.type = CategoryType
        self.new_price = newPrice
        self.current_price = CategoryPrice
        self.last_updated_date = lastUpdateDate
    }

    
    
    var dictionaryDataCart : [String : String] {
        
        var objDict : [String : String]!
        
        
        objDict["current_category_id"] = current_category_id
        objDict["new_category_id"] = new_category_id
        
        objDict["type"] = type
        objDict["current_price"] = current_price
        objDict["new_price"] = new_price
        objDict["last_updated_date"] = last_updated_date
        return objDict;
    }
    func toDictionary() -> [String:String]
    {
        var dictionary = [String:String]()
        if current_category_id != nil{
            dictionary["current_category_id"] = current_category_id
        }
        if new_category_id != nil{
            dictionary["new_category_id"] = new_category_id
        }
        if type != nil{
            dictionary["type"] = type
        }
        if current_price != nil{
            dictionary["current_price"] = current_price
        }
        if new_price != nil{
            dictionary["new_price"] = new_price
        }
        if new_price != nil{
            dictionary["last_updated_date"] = last_updated_date
        }
        return dictionary
    }
    class func convertDataCartArrayToProductsDictionary(arrayDataCart : [CategoryUpdateRequestModel]) -> [[String:String]] {
        var arrayDataCartDictionaries : [[String:String]] = []
        for objDataCart in arrayDataCart {
            print(objDataCart)
            
            arrayDataCartDictionaries.append(objDataCart.toDictionary());
        }
        return arrayDataCartDictionaries
    }
  
   
}

