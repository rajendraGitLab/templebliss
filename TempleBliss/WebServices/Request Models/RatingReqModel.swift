//
//  RatingReqModel.swift
//  TempleBliss
//
//  Created by Apple on 29/01/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
class RatingReqModel : RequestModel{
    var user_id = ""
    var advisor_id = ""
    /// // 0 for Not Rating & 1 for Rating
    var rating = ""
    var note = ""
}
