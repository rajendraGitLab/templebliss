//
//  SessionList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2021

//import Foundation
//import SwiftyJSON
//
//
//class SessionListForAdvisor : NSObject, NSCoding{
//
//    var bookingDate : String!
//    var bookingId : String!
//    var customerId : String!
//    var fullName : String!
//    var isSessionEnd : String!
//    var note : String!
//    var profilePicture : String!
//    var totalMinutes : String!
//    var totalPrice : String!
//    var type : String!
//
//    /**
//     * Instantiate the instance using the passed json values to set the properties values
//     */
//    init(fromJson json: JSON!){
//        if json.isEmpty{
//            return
//        }
//        bookingDate = json["booking_date"].stringValue
//        bookingId = json["booking_id"].stringValue
//        customerId = json["customer_id"].stringValue
//        fullName = json["full_name"].stringValue
//        isSessionEnd = json["is_session_end"].stringValue
//        note = json["note"].stringValue
//        profilePicture = json["profile_picture"].stringValue
//        totalMinutes = json["total_minutes"].stringValue
//        totalPrice = json["total_price"].stringValue
//        type = json["type"].stringValue
//    }
//
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
//        if bookingDate != nil{
//            dictionary["booking_date"] = bookingDate
//        }
//        if bookingId != nil{
//            dictionary["booking_id"] = bookingId
//        }
//        if customerId != nil{
//            dictionary["customer_id"] = customerId
//        }
//        if fullName != nil{
//            dictionary["full_name"] = fullName
//        }
//        if isSessionEnd != nil{
//            dictionary["is_session_end"] = isSessionEnd
//        }
//        if note != nil{
//            dictionary["note"] = note
//        }
//        if profilePicture != nil{
//            dictionary["profile_picture"] = profilePicture
//        }
//        if totalMinutes != nil{
//            dictionary["total_minutes"] = totalMinutes
//        }
//        if totalPrice != nil{
//            dictionary["total_price"] = totalPrice
//        }
//        if type != nil{
//            dictionary["type"] = type
//        }
//        return dictionary
//    }
//
//    /**
//    * NSCoding required initializer.
//    * Fills the data from the passed decoder
//    */
//    @objc required init(coder aDecoder: NSCoder)
//    {
//        bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
//        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
//        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
//        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
//        isSessionEnd = aDecoder.decodeObject(forKey: "is_session_end") as? String
//        note = aDecoder.decodeObject(forKey: "note") as? String
//        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
//        totalMinutes = aDecoder.decodeObject(forKey: "total_minutes") as? String
//        totalPrice = aDecoder.decodeObject(forKey: "total_price") as? String
//        type = aDecoder.decodeObject(forKey: "type") as? String
//    }
//
//    /**
//    * NSCoding required method.
//    * Encodes mode properties into the decoder
//    */
//    func encode(with aCoder: NSCoder)
//    {
//        if bookingDate != nil{
//            aCoder.encode(bookingDate, forKey: "booking_date")
//        }
//        if bookingId != nil{
//            aCoder.encode(bookingId, forKey: "booking_id")
//        }
//        if customerId != nil{
//            aCoder.encode(customerId, forKey: "customer_id")
//        }
//        if fullName != nil{
//            aCoder.encode(fullName, forKey: "full_name")
//        }
//        if isSessionEnd != nil{
//            aCoder.encode(isSessionEnd, forKey: "is_session_end")
//        }
//        if note != nil{
//            aCoder.encode(note, forKey: "note")
//        }
//        if profilePicture != nil{
//            aCoder.encode(profilePicture, forKey: "profile_picture")
//        }
//        if totalMinutes != nil{
//            aCoder.encode(totalMinutes, forKey: "total_minutes")
//        }
//        if totalPrice != nil{
//            aCoder.encode(totalPrice, forKey: "total_price")
//        }
//        if type != nil{
//            aCoder.encode(type, forKey: "type")
//        }
//
//    }
//
//}
//
//  SessionList.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 13, 2021
//
//import Foundation
//import SwiftyJSON
//
//
//class SessionListForAdvisor : NSObject, NSCoding{
//
//    var advisorCommission : String!
//    var bookingDate : String!
//    var bookingId : String!
//    var customerId : String!
//    var fullName : String!
//    var isSessionEnd : String!
//    var note : String!
//    var profilePicture : String!
//    var totalMinutes : String!
//    var totalPrice : String!
//    var type : String!
//
//    /**
//     * Instantiate the instance using the passed json values to set the properties values
//     */
//    init(fromJson json: JSON!){
//        if json.isEmpty{
//            return
//        }
//        advisorCommission = json["advisor_commission"].stringValue
//        bookingDate = json["booking_date"].stringValue
//        bookingId = json["booking_id"].stringValue
//        customerId = json["customer_id"].stringValue
//        fullName = json["full_name"].stringValue
//        isSessionEnd = json["is_session_end"].stringValue
//        note = json["note"].stringValue
//        profilePicture = json["profile_picture"].stringValue
//        totalMinutes = json["total_minutes"].stringValue
//        totalPrice = json["total_price"].stringValue
//        type = json["type"].stringValue
//    }
//
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
//        if advisorCommission != nil{
//            dictionary["advisor_commission"] = advisorCommission
//        }
//        if bookingDate != nil{
//            dictionary["booking_date"] = bookingDate
//        }
//        if bookingId != nil{
//            dictionary["booking_id"] = bookingId
//        }
//        if customerId != nil{
//            dictionary["customer_id"] = customerId
//        }
//        if fullName != nil{
//            dictionary["full_name"] = fullName
//        }
//        if isSessionEnd != nil{
//            dictionary["is_session_end"] = isSessionEnd
//        }
//        if note != nil{
//            dictionary["note"] = note
//        }
//        if profilePicture != nil{
//            dictionary["profile_picture"] = profilePicture
//        }
//        if totalMinutes != nil{
//            dictionary["total_minutes"] = totalMinutes
//        }
//        if totalPrice != nil{
//            dictionary["total_price"] = totalPrice
//        }
//        if type != nil{
//            dictionary["type"] = type
//        }
//        return dictionary
//    }
//
//    /**
//    * NSCoding required initializer.
//    * Fills the data from the passed decoder
//    */
//    @objc required init(coder aDecoder: NSCoder)
//    {
//        advisorCommission = aDecoder.decodeObject(forKey: "advisor_commission") as? String
//        bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
//        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
//        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
//        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
//        isSessionEnd = aDecoder.decodeObject(forKey: "is_session_end") as? String
//        note = aDecoder.decodeObject(forKey: "note") as? String
//        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
//        totalMinutes = aDecoder.decodeObject(forKey: "total_minutes") as? String
//        totalPrice = aDecoder.decodeObject(forKey: "total_price") as? String
//        type = aDecoder.decodeObject(forKey: "type") as? String
//    }
//
//    /**
//    * NSCoding required method.
//    * Encodes mode properties into the decoder
//    */
//    func encode(with aCoder: NSCoder)
//    {
//        if advisorCommission != nil{
//            aCoder.encode(advisorCommission, forKey: "advisor_commission")
//        }
//        if bookingDate != nil{
//            aCoder.encode(bookingDate, forKey: "booking_date")
//        }
//        if bookingId != nil{
//            aCoder.encode(bookingId, forKey: "booking_id")
//        }
//        if customerId != nil{
//            aCoder.encode(customerId, forKey: "customer_id")
//        }
//        if fullName != nil{
//            aCoder.encode(fullName, forKey: "full_name")
//        }
//        if isSessionEnd != nil{
//            aCoder.encode(isSessionEnd, forKey: "is_session_end")
//        }
//        if note != nil{
//            aCoder.encode(note, forKey: "note")
//        }
//        if profilePicture != nil{
//            aCoder.encode(profilePicture, forKey: "profile_picture")
//        }
//        if totalMinutes != nil{
//            aCoder.encode(totalMinutes, forKey: "total_minutes")
//        }
//        if totalPrice != nil{
//            aCoder.encode(totalPrice, forKey: "total_price")
//        }
//        if type != nil{
//            aCoder.encode(type, forKey: "type")
//        }
//
//    }
//
//}
//
//  SessionList.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on May 18, 2021

import Foundation
import SwiftyJSON


class SessionListForAdvisor : NSObject, NSCoding{

    var advisorCommission : String!
    var bookingDate : String!
    var bookingId : String!
    var customerId : String!
    var fullName : String!
    var isSessionEnd : String!
    var note : String!
    var postUnreadMessageCount : String!
    var profilePicture : String!
    var totalMinutes : String!
    var totalPrice : String!
    var type : String!
    var IsPostMessageAvailable : String!
    var isLatest : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        advisorCommission = json["advisor_commission"].stringValue
        bookingDate = json["booking_date"].stringValue
        bookingId = json["booking_id"].stringValue
        customerId = json["customer_id"].stringValue
        fullName = json["full_name"].stringValue
        isSessionEnd = json["is_session_end"].stringValue
        note = json["note"].stringValue
        postUnreadMessageCount = json["post_unread_message_count"].stringValue
        profilePicture = json["profile_picture"].stringValue
        totalMinutes = json["total_minutes"].stringValue
        totalPrice = json["total_price"].stringValue
        type = json["type"].stringValue
        isLatest = json["is_latest"].stringValue
        IsPostMessageAvailable = json["post_message_available"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if advisorCommission != nil{
            dictionary["advisor_commission"] = advisorCommission
        }
        if bookingDate != nil{
            dictionary["booking_date"] = bookingDate
        }
        if bookingId != nil{
            dictionary["booking_id"] = bookingId
        }
        if customerId != nil{
            dictionary["customer_id"] = customerId
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if isSessionEnd != nil{
            dictionary["is_session_end"] = isSessionEnd
        }
        if note != nil{
            dictionary["note"] = note
        }
        if postUnreadMessageCount != nil{
            dictionary["post_unread_message_count"] = postUnreadMessageCount
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if totalMinutes != nil{
            dictionary["total_minutes"] = totalMinutes
        }
        if totalPrice != nil{
            dictionary["total_price"] = totalPrice
        }
        if type != nil{
            dictionary["type"] = type
        }
        if IsPostMessageAvailable != nil{
            dictionary["post_message_available"] = IsPostMessageAvailable
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        advisorCommission = aDecoder.decodeObject(forKey: "advisor_commission") as? String
        bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        isSessionEnd = aDecoder.decodeObject(forKey: "is_session_end") as? String
        note = aDecoder.decodeObject(forKey: "note") as? String
        postUnreadMessageCount = aDecoder.decodeObject(forKey: "post_unread_message_count") as? String
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        totalMinutes = aDecoder.decodeObject(forKey: "total_minutes") as? String
        totalPrice = aDecoder.decodeObject(forKey: "total_price") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        IsPostMessageAvailable = aDecoder.decodeObject(forKey: "post_message_available") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if advisorCommission != nil{
            aCoder.encode(advisorCommission, forKey: "advisor_commission")
        }
        if bookingDate != nil{
            aCoder.encode(bookingDate, forKey: "booking_date")
        }
        if bookingId != nil{
            aCoder.encode(bookingId, forKey: "booking_id")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customer_id")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if isSessionEnd != nil{
            aCoder.encode(isSessionEnd, forKey: "is_session_end")
        }
        if note != nil{
            aCoder.encode(note, forKey: "note")
        }
        if postUnreadMessageCount != nil{
            aCoder.encode(postUnreadMessageCount, forKey: "post_unread_message_count")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if totalMinutes != nil{
            aCoder.encode(totalMinutes, forKey: "total_minutes")
        }
        if totalPrice != nil{
            aCoder.encode(totalPrice, forKey: "total_price")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if IsPostMessageAvailable != nil{
            aCoder.encode(IsPostMessageAvailable, forKey: "post_message_available")
        }

    }

}
