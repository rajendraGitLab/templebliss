//
//  sessionForAdvisor.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 9, 2021

import Foundation
import SwiftyJSON


class sessionForAdvisor : NSObject, NSCoding{

    var message : String!
    var sessionList : [SessionListForAdvisor]!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        message = json["message"].stringValue
        sessionList = [SessionListForAdvisor]()
        let sessionListArray = json["session_list"].arrayValue
        for sessionListJson in sessionListArray{
            let value = SessionListForAdvisor(fromJson: sessionListJson)
            sessionList.append(value)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if message != nil{
        	dictionary["message"] = message
        }
        if sessionList != nil{
        var dictionaryElements = [[String:Any]]()
        for sessionListElement in sessionList {
        	dictionaryElements.append(sessionListElement.toDictionary())
        }
        dictionary["sessionList"] = dictionaryElements
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		message = aDecoder.decodeObject(forKey: "message") as? String
		sessionList = aDecoder.decodeObject(forKey: "session_list") as? [SessionListForAdvisor]
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if sessionList != nil{
			aCoder.encode(sessionList, forKey: "session_list")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
