//
//  Training.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on February 26, 2021

import Foundation
import SwiftyJSON


class Training : NSObject, NSCoding{

    var descriptionField : String!
    var file : String!
    var id : String!
    var title : String!
    var videoUrl : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        descriptionField = json["description"].stringValue
        file = json["file"].stringValue
        id = json["id"].stringValue
        title = json["title"].stringValue
        videoUrl = json["video_url"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if file != nil{
            dictionary["file"] = file
        }
        if id != nil{
            dictionary["id"] = id
        }
        if title != nil{
            dictionary["title"] = title
        }
        if videoUrl != nil{
            dictionary["video_url"] = videoUrl
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        file = aDecoder.decodeObject(forKey: "file") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
        videoUrl = aDecoder.decodeObject(forKey: "video_url") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if file != nil{
            aCoder.encode(file, forKey: "file")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        if videoUrl != nil{
            aCoder.encode(videoUrl, forKey: "video_url")
        }

    }

}
