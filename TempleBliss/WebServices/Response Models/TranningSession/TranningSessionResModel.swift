//
//  TranningSessionResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on February 26, 2021

import Foundation
import SwiftyJSON


class TranningSessionResModel : NSObject, NSCoding{

    var message : String!
    var status : Bool!
    var training : [Training]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        message = json["message"].stringValue
        status = json["status"].boolValue
        training = [Training]()
        let trainingArray = json["training"].arrayValue
        for trainingJson in trainingArray{
            let value = Training(fromJson: trainingJson)
            training.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if message != nil{
        	dictionary["message"] = message
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if training != nil{
        var dictionaryElements = [[String:Any]]()
        for trainingElement in training {
        	dictionaryElements.append(trainingElement.toDictionary())
        }
        dictionary["training"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		message = aDecoder.decodeObject(forKey: "message") as? String
		status = aDecoder.decodeObject(forKey: "status") as? Bool
		training = aDecoder.decodeObject(forKey: "training") as? [Training]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if training != nil{
			aCoder.encode(training, forKey: "training")
		}

	}

}
