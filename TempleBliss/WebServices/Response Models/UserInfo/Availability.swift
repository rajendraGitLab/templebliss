//
//  Availability.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 26, 2021

import Foundation
import SwiftyJSON


class Availability : Codable{

    var categoryId : String!
    var lastUpdatedDate : String!
    var price : String!
    var type : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        categoryId = json["category_id"].stringValue
        lastUpdatedDate = json["last_updated_date"].stringValue
        price = json["price"].stringValue
        type = json["type"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if categoryId != nil{
        	dictionary["category_id"] = categoryId
        }
        if lastUpdatedDate != nil{
        	dictionary["last_updated_date"] = lastUpdatedDate
        }
        if price != nil{
        	dictionary["price"] = price
        }
        if type != nil{
        	dictionary["type"] = type
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
		lastUpdatedDate = aDecoder.decodeObject(forKey: "last_updated_date") as? String
		price = aDecoder.decodeObject(forKey: "price") as? String
		type = aDecoder.decodeObject(forKey: "type") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if categoryId != nil{
			aCoder.encode(categoryId, forKey: "category_id")
		}
		if lastUpdatedDate != nil{
			aCoder.encode(lastUpdatedDate, forKey: "last_updated_date")
		}
		if price != nil{
			aCoder.encode(price, forKey: "price")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}

////
////  Availability.swift
////  Model Generated using http://www.jsoncafe.com/
////  Created on March 17, 2021
//
