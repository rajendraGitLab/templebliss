//
//  Profile.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on January 26, 2021

import Foundation
import SwiftyJSON


class Profile : Codable{

    var activationCode : String!
    var activationSelector : String!
    var active : String!
    var advisorDescription : String!
    var apiKey : String!
    var approveAdvisorDetail : String!
    var categoryId : String!
    var createdAt : String!
    var createdOn : String!
    var deletedAt : String!
    var deviceToken : String!
    var deviceType : String!
    var dob : String!
    var email : String!
    var favouriteAdvisor : String!
    var firstName : String!
    var forgottenPasswordCode : String!
    var forgottenPasswordSelector : String!
    var forgottenPasswordTime : String!
    var fullName : String!
    var id : String!
    var ipAddress : String!
    var lastLogin : String!
    var lastName : String!
    var nickName : String!
    var password : String!
    var phone : String!
    var profilePicture : String!
    var rememberCode : String!
    var rememberSelector : String!
    var socialId : String!
    var socialType : String!
    var updatedAt : String!
    var username : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        activationCode = json["activation_code"].stringValue
        activationSelector = json["activation_selector"].stringValue
        active = json["active"].stringValue
        advisorDescription = json["advisor_description"].stringValue
        apiKey = json["api_key"].stringValue
        approveAdvisorDetail = json["approve_advisor_detail"].stringValue
        categoryId = json["category_id"].stringValue
        createdAt = json["created_at"].stringValue
        createdOn = json["created_on"].stringValue
        deletedAt = json["deleted_at"].stringValue
        deviceToken = json["device_token"].stringValue
        deviceType = json["device_type"].stringValue
        dob = json["dob"].stringValue
        email = json["email"].stringValue
        favouriteAdvisor = json["favourite_advisor"].stringValue
        firstName = json["first_name"].stringValue
        forgottenPasswordCode = json["forgotten_password_code"].stringValue
        forgottenPasswordSelector = json["forgotten_password_selector"].stringValue
        forgottenPasswordTime = json["forgotten_password_time"].stringValue
        fullName = json["full_name"].stringValue
        id = json["id"].stringValue
        ipAddress = json["ip_address"].stringValue
        lastLogin = json["last_login"].stringValue
        lastName = json["last_name"].stringValue
        nickName = json["nick_name"].stringValue
        password = json["password"].stringValue
        phone = json["phone"].stringValue
        profilePicture = json["profile_picture"].stringValue
        rememberCode = json["remember_code"].stringValue
        rememberSelector = json["remember_selector"].stringValue
        socialId = json["social_id"].stringValue
        socialType = json["social_type"].stringValue
        updatedAt = json["updated_at"].stringValue
        username = json["username"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if activationCode != nil{
        	dictionary["activation_code"] = activationCode
        }
        if activationSelector != nil{
        	dictionary["activation_selector"] = activationSelector
        }
        if active != nil{
        	dictionary["active"] = active
        }
        if advisorDescription != nil{
        	dictionary["advisor_description"] = advisorDescription
        }
        if apiKey != nil{
        	dictionary["api_key"] = apiKey
        }
        if approveAdvisorDetail != nil{
        	dictionary["approve_advisor_detail"] = approveAdvisorDetail
        }
        if categoryId != nil{
        	dictionary["category_id"] = categoryId
        }
        if createdAt != nil{
        	dictionary["created_at"] = createdAt
        }
        if createdOn != nil{
        	dictionary["created_on"] = createdOn
        }
        if deletedAt != nil{
        	dictionary["deleted_at"] = deletedAt
        }
        if deviceToken != nil{
        	dictionary["device_token"] = deviceToken
        }
        if deviceType != nil{
        	dictionary["device_type"] = deviceType
        }
        if dob != nil{
        	dictionary["dob"] = dob
        }
        if email != nil{
        	dictionary["email"] = email
        }
        if favouriteAdvisor != nil{
        	dictionary["favourite_advisor"] = favouriteAdvisor
        }
        if firstName != nil{
        	dictionary["first_name"] = firstName
        }
        if forgottenPasswordCode != nil{
        	dictionary["forgotten_password_code"] = forgottenPasswordCode
        }
        if forgottenPasswordSelector != nil{
        	dictionary["forgotten_password_selector"] = forgottenPasswordSelector
        }
        if forgottenPasswordTime != nil{
        	dictionary["forgotten_password_time"] = forgottenPasswordTime
        }
        if fullName != nil{
        	dictionary["full_name"] = fullName
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if ipAddress != nil{
        	dictionary["ip_address"] = ipAddress
        }
        if lastLogin != nil{
        	dictionary["last_login"] = lastLogin
        }
        if lastName != nil{
        	dictionary["last_name"] = lastName
        }
        if nickName != nil{
        	dictionary["nick_name"] = nickName
        }
        if password != nil{
        	dictionary["password"] = password
        }
        if phone != nil{
        	dictionary["phone"] = phone
        }
        if profilePicture != nil{
        	dictionary["profile_picture"] = profilePicture
        }
        if rememberCode != nil{
        	dictionary["remember_code"] = rememberCode
        }
        if rememberSelector != nil{
        	dictionary["remember_selector"] = rememberSelector
        }
        if socialId != nil{
        	dictionary["social_id"] = socialId
        }
        if socialType != nil{
        	dictionary["social_type"] = socialType
        }
        if updatedAt != nil{
        	dictionary["updated_at"] = updatedAt
        }
        if username != nil{
        	dictionary["username"] = username
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		activationCode = aDecoder.decodeObject(forKey: "activation_code") as? String
		activationSelector = aDecoder.decodeObject(forKey: "activation_selector") as? String
		active = aDecoder.decodeObject(forKey: "active") as? String
		advisorDescription = aDecoder.decodeObject(forKey: "advisor_description") as? String
		apiKey = aDecoder.decodeObject(forKey: "api_key") as? String
		approveAdvisorDetail = aDecoder.decodeObject(forKey: "approve_advisor_detail") as? String
		categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
		createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
		createdOn = aDecoder.decodeObject(forKey: "created_on") as? String
		deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? String
		deviceToken = aDecoder.decodeObject(forKey: "device_token") as? String
		deviceType = aDecoder.decodeObject(forKey: "device_type") as? String
		dob = aDecoder.decodeObject(forKey: "dob") as? String
		email = aDecoder.decodeObject(forKey: "email") as? String
		favouriteAdvisor = aDecoder.decodeObject(forKey: "favourite_advisor") as? String
		firstName = aDecoder.decodeObject(forKey: "first_name") as? String
		forgottenPasswordCode = aDecoder.decodeObject(forKey: "forgotten_password_code") as? String
		forgottenPasswordSelector = aDecoder.decodeObject(forKey: "forgotten_password_selector") as? String
		forgottenPasswordTime = aDecoder.decodeObject(forKey: "forgotten_password_time") as? String
		fullName = aDecoder.decodeObject(forKey: "full_name") as? String
		id = aDecoder.decodeObject(forKey: "id") as? String
		ipAddress = aDecoder.decodeObject(forKey: "ip_address") as? String
		lastLogin = aDecoder.decodeObject(forKey: "last_login") as? String
		lastName = aDecoder.decodeObject(forKey: "last_name") as? String
		nickName = aDecoder.decodeObject(forKey: "nick_name") as? String
		password = aDecoder.decodeObject(forKey: "password") as? String
		phone = aDecoder.decodeObject(forKey: "phone") as? String
		profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
		rememberCode = aDecoder.decodeObject(forKey: "remember_code") as? String
		rememberSelector = aDecoder.decodeObject(forKey: "remember_selector") as? String
		socialId = aDecoder.decodeObject(forKey: "social_id") as? String
		socialType = aDecoder.decodeObject(forKey: "social_type") as? String
		updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
		username = aDecoder.decodeObject(forKey: "username") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if activationCode != nil{
			aCoder.encode(activationCode, forKey: "activation_code")
		}
		if activationSelector != nil{
			aCoder.encode(activationSelector, forKey: "activation_selector")
		}
		if active != nil{
			aCoder.encode(active, forKey: "active")
		}
		if advisorDescription != nil{
			aCoder.encode(advisorDescription, forKey: "advisor_description")
		}
		if apiKey != nil{
			aCoder.encode(apiKey, forKey: "api_key")
		}
		if approveAdvisorDetail != nil{
			aCoder.encode(approveAdvisorDetail, forKey: "approve_advisor_detail")
		}
		if categoryId != nil{
			aCoder.encode(categoryId, forKey: "category_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if createdOn != nil{
			aCoder.encode(createdOn, forKey: "created_on")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if deviceToken != nil{
			aCoder.encode(deviceToken, forKey: "device_token")
		}
		if deviceType != nil{
			aCoder.encode(deviceType, forKey: "device_type")
		}
		if dob != nil{
			aCoder.encode(dob, forKey: "dob")
		}
		if email != nil{
			aCoder.encode(email, forKey: "email")
		}
		if favouriteAdvisor != nil{
			aCoder.encode(favouriteAdvisor, forKey: "favourite_advisor")
		}
		if firstName != nil{
			aCoder.encode(firstName, forKey: "first_name")
		}
		if forgottenPasswordCode != nil{
			aCoder.encode(forgottenPasswordCode, forKey: "forgotten_password_code")
		}
		if forgottenPasswordSelector != nil{
			aCoder.encode(forgottenPasswordSelector, forKey: "forgotten_password_selector")
		}
		if forgottenPasswordTime != nil{
			aCoder.encode(forgottenPasswordTime, forKey: "forgotten_password_time")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "full_name")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if ipAddress != nil{
			aCoder.encode(ipAddress, forKey: "ip_address")
		}
		if lastLogin != nil{
			aCoder.encode(lastLogin, forKey: "last_login")
		}
		if lastName != nil{
			aCoder.encode(lastName, forKey: "last_name")
		}
		if nickName != nil{
			aCoder.encode(nickName, forKey: "nick_name")
		}
		if password != nil{
			aCoder.encode(password, forKey: "password")
		}
		if phone != nil{
			aCoder.encode(phone, forKey: "phone")
		}
		if profilePicture != nil{
			aCoder.encode(profilePicture, forKey: "profile_picture")
		}
		if rememberCode != nil{
			aCoder.encode(rememberCode, forKey: "remember_code")
		}
		if rememberSelector != nil{
			aCoder.encode(rememberSelector, forKey: "remember_selector")
		}
		if socialId != nil{
			aCoder.encode(socialId, forKey: "social_id")
		}
		if socialType != nil{
			aCoder.encode(socialType, forKey: "social_type")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}
		if username != nil{
			aCoder.encode(username, forKey: "username")
		}

	}

}
