//
//  RegisterOtpResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 12, 2021

import Foundation
import SwiftyJSON


class RegisterOtpResModel : NSObject, NSCoding{

    var message : String!
    var otp : String!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        message = json["message"].stringValue
        otp = json["otp"].stringValue
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if message != nil{
        	dictionary["message"] = message
        }
        if otp != nil{
        	dictionary["otp"] = otp
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		message = aDecoder.decodeObject(forKey: "message") as? String
		otp = aDecoder.decodeObject(forKey: "otp") as? String
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if otp != nil{
			aCoder.encode(otp, forKey: "otp")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
