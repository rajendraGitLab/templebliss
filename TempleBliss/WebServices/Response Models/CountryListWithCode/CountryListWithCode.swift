//
//  CountryListWithCode.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 4, 2021

import Foundation
import SwiftyJSON


class CountryListWithCode : NSObject, NSCoding{

    var countryCode : [CountryCode]!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        countryCode = [CountryCode]()
        let countryCodeArray = json["country_code"].arrayValue
        for countryCodeJson in countryCodeArray{
            let value = CountryCode(fromJson: countryCodeJson)
            countryCode.append(value)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if countryCode != nil{
        var dictionaryElements = [[String:Any]]()
        for countryCodeElement in countryCode {
        	dictionaryElements.append(countryCodeElement.toDictionary())
        }
        dictionary["countryCode"] = dictionaryElements
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		countryCode = aDecoder.decodeObject(forKey: "country_code") as? [CountryCode]
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if countryCode != nil{
			aCoder.encode(countryCode, forKey: "country_code")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
