//
//  Card.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 11, 2021

import Foundation
import SwiftyJSON


class Card : NSObject, NSCoding{

    var cardHolderName : String!
    var cardNumber : String!
    var cardType : String!
    var cvv : String!
    var displayExpiryDate : String!
    var displayNumber : String!
    var expiryDate : String!
    var id : String!
    var originalCvv : String!
    var userId : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        cardHolderName = json["card_holder_name"].stringValue
        cardNumber = json["card_number"].stringValue
        cardType = json["card_type"].stringValue
        cvv = json["cvv"].stringValue
        displayExpiryDate = json["display_expiry_date"].stringValue
        displayNumber = json["display_number"].stringValue
        expiryDate = json["expiry_date"].stringValue
        id = json["id"].stringValue
        originalCvv = json["original_cvv"].stringValue
        userId = json["user_id"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if cardHolderName != nil{
            dictionary["card_holder_name"] = cardHolderName
        }
        if cardNumber != nil{
            dictionary["card_number"] = cardNumber
        }
        if cardType != nil{
            dictionary["card_type"] = cardType
        }
        if cvv != nil{
            dictionary["cvv"] = cvv
        }
        if displayExpiryDate != nil{
            dictionary["display_expiry_date"] = displayExpiryDate
        }
        if displayNumber != nil{
            dictionary["display_number"] = displayNumber
        }
        if expiryDate != nil{
            dictionary["expiry_date"] = expiryDate
        }
        if id != nil{
            dictionary["id"] = id
        }
        if originalCvv != nil{
            dictionary["original_cvv"] = originalCvv
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        cardHolderName = aDecoder.decodeObject(forKey: "card_holder_name") as? String
        cardNumber = aDecoder.decodeObject(forKey: "card_number") as? String
        cardType = aDecoder.decodeObject(forKey: "card_type") as? String
        cvv = aDecoder.decodeObject(forKey: "cvv") as? String
        displayExpiryDate = aDecoder.decodeObject(forKey: "display_expiry_date") as? String
        displayNumber = aDecoder.decodeObject(forKey: "display_number") as? String
        expiryDate = aDecoder.decodeObject(forKey: "expiry_date") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        originalCvv = aDecoder.decodeObject(forKey: "original_cvv") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if cardHolderName != nil{
            aCoder.encode(cardHolderName, forKey: "card_holder_name")
        }
        if cardNumber != nil{
            aCoder.encode(cardNumber, forKey: "card_number")
        }
        if cardType != nil{
            aCoder.encode(cardType, forKey: "card_type")
        }
        if cvv != nil{
            aCoder.encode(cvv, forKey: "cvv")
        }
        if displayExpiryDate != nil{
            aCoder.encode(displayExpiryDate, forKey: "display_expiry_date")
        }
        if displayNumber != nil{
            aCoder.encode(displayNumber, forKey: "display_number")
        }
        if expiryDate != nil{
            aCoder.encode(expiryDate, forKey: "expiry_date")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if originalCvv != nil{
            aCoder.encode(originalCvv, forKey: "original_cvv")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }

    }

}
