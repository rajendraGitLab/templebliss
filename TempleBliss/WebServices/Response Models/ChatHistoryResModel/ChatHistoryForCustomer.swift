//
//  ChatHistory.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 8, 2021

import Foundation
import SwiftyJSON


class ChatHistoryForCustomer : NSObject, NSCoding{
    
    var bookingDate : String!
    var bookingId : String!
    var chatId : String!
    var fullName : String!
    var message : String!
    var messageDate : String!
    var nickName : String!
    var profilePicture : String!
    var readStatus : String!
    var receiverId : String!
    var receiverType : String!
    var senderId : String!
    var senderType : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        bookingDate = json["booking_date"].stringValue
        bookingId = json["booking_id"].stringValue
        chatId = json["chat_id"].stringValue
        fullName = json["full_name"].stringValue
        message = json["message"].stringValue
        messageDate = json["message_date"].stringValue
        nickName = json["nick_name"].stringValue
        profilePicture = json["profile_picture"].stringValue
        readStatus = json["read_status"].stringValue
        receiverId = json["receiver_id"].stringValue
        receiverType = json["receiver_type"].stringValue
        senderId = json["sender_id"].stringValue
        senderType = json["sender_type"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bookingDate != nil{
            dictionary["booking_date"] = bookingDate
        }
        if bookingId != nil{
            dictionary["booking_id"] = bookingId
        }
        if chatId != nil{
            dictionary["chat_id"] = chatId
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if message != nil{
            dictionary["message"] = message
        }
        if messageDate != nil{
            dictionary["message_date"] = messageDate
        }
        if nickName != nil{
            dictionary["nick_name"] = nickName
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if readStatus != nil{
            dictionary["read_status"] = readStatus
        }
        if receiverId != nil{
            dictionary["receiver_id"] = receiverId
        }
        if receiverType != nil{
            dictionary["receiver_type"] = receiverType
        }
        if senderId != nil{
            dictionary["sender_id"] = senderId
        }
        if senderType != nil{
            dictionary["sender_type"] = senderType
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
        chatId = aDecoder.decodeObject(forKey: "chat_id") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        message = aDecoder.decodeObject(forKey: "message") as? String
        messageDate = aDecoder.decodeObject(forKey: "message_date") as? String
        nickName = aDecoder.decodeObject(forKey: "nick_name") as? String
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        readStatus = aDecoder.decodeObject(forKey: "read_status") as? String
        receiverId = aDecoder.decodeObject(forKey: "receiver_id") as? String
        receiverType = aDecoder.decodeObject(forKey: "receiver_type") as? String
        senderId = aDecoder.decodeObject(forKey: "sender_id") as? String
        senderType = aDecoder.decodeObject(forKey: "sender_type") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if bookingDate != nil{
            aCoder.encode(bookingDate, forKey: "booking_date")
        }
        if bookingId != nil{
            aCoder.encode(bookingId, forKey: "booking_id")
        }
        if chatId != nil{
            aCoder.encode(chatId, forKey: "chat_id")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if messageDate != nil{
            aCoder.encode(messageDate, forKey: "message_date")
        }
        if nickName != nil{
            aCoder.encode(nickName, forKey: "nick_name")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if readStatus != nil{
            aCoder.encode(readStatus, forKey: "read_status")
        }
        if receiverId != nil{
            aCoder.encode(receiverId, forKey: "receiver_id")
        }
        if receiverType != nil{
            aCoder.encode(receiverType, forKey: "receiver_type")
        }
        if senderId != nil{
            aCoder.encode(senderId, forKey: "sender_id")
        }
        if senderType != nil{
            aCoder.encode(senderType, forKey: "sender_type")
        }

    }

}
