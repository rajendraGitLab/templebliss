//
//  AdvisorDetailResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 12, 2021

import Foundation
import SwiftyJSON


class AdvisorDetailResModel : NSObject, NSCoding{

    var advisorDetails : Advisor!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        let advisorDetailsJson = json["advisor_details"]
        if !advisorDetailsJson.isEmpty{
            advisorDetails = Advisor(fromJson: advisorDetailsJson)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisorDetails != nil{
        	dictionary["advisorDetails"] = advisorDetails.toDictionary()
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}
    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisorDetails = aDecoder.decodeObject(forKey: "advisor_details") as? Advisor
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisorDetails != nil{
			aCoder.encode(advisorDetails, forKey: "advisor_details")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
