//
//  SessionList.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 8, 2021

import Foundation
import SwiftyJSON


class SessionList : NSObject, NSCoding{

    var bookingDate : String!
    var bookingId : String!
    var nickName : String!
    var profilePicture : String!
    var type : String!
    var total_minutes : String!
    var advisor_id : String!
    var rating_availible : String!
    var Amount : String!
    var postUnreadMessageCount : String!
    

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        bookingDate = json["booking_date"].stringValue
        bookingId = json["booking_id"].stringValue
        nickName = json["nick_name"].stringValue
        profilePicture = json["profile_picture"].stringValue
        type = json["type"].stringValue
        total_minutes = json["total_minutes"].stringValue
        advisor_id = json["advisor_id"].stringValue
        rating_availible = json["rating_availible"].stringValue
        Amount = json["total_price"].stringValue
        postUnreadMessageCount = json["post_unread_message_count"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if bookingDate != nil{
        	dictionary["booking_date"] = bookingDate
        }
        if bookingId != nil{
        	dictionary["booking_id"] = bookingId
        }
        if nickName != nil{
        	dictionary["nick_name"] = nickName
        }
        if profilePicture != nil{
        	dictionary["profile_picture"] = profilePicture
        }
        if type != nil{
        	dictionary["type"] = type
        }
        if total_minutes != nil{
            dictionary["total_minutes"] = total_minutes
        }
        if advisor_id != nil{
            dictionary["advisor_id"] = advisor_id
        }
        if rating_availible != nil{
            dictionary["rating_availible"] = rating_availible
        }
        if Amount != nil{
            dictionary["total_price"] = Amount
        }
        if postUnreadMessageCount != nil{
            dictionary["post_unread_message_count"] = postUnreadMessageCount
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		bookingDate = aDecoder.decodeObject(forKey: "booking_date") as? String
		bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
		nickName = aDecoder.decodeObject(forKey: "nick_name") as? String
		profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
		type = aDecoder.decodeObject(forKey: "type") as? String
        total_minutes = aDecoder.decodeObject(forKey: "total_minutes") as? String
        advisor_id = aDecoder.decodeObject(forKey: "advisor_id") as? String
        rating_availible = aDecoder.decodeObject(forKey: "rating_availible") as? String
        Amount = aDecoder.decodeObject(forKey: "total_price") as? String
        postUnreadMessageCount = aDecoder.decodeObject(forKey: "post_unread_message_count") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if bookingDate != nil{
			aCoder.encode(bookingDate, forKey: "booking_date")
		}
		if bookingId != nil{
			aCoder.encode(bookingId, forKey: "booking_id")
		}
		if nickName != nil{
			aCoder.encode(nickName, forKey: "nick_name")
		}
		if profilePicture != nil{
			aCoder.encode(profilePicture, forKey: "profile_picture")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
        if total_minutes != nil{
            aCoder.encode(total_minutes, forKey: "total_minutes")
        }
        if advisor_id != nil{
            aCoder.encode(advisor_id, forKey: "advisor_id")
        }
        if rating_availible != nil{
            aCoder.encode(rating_availible, forKey: "rating_availible")
        }
        if Amount != nil{
            aCoder.encode(Amount, forKey: "total_price")
        }
        if postUnreadMessageCount != nil{
            aCoder.encode(postUnreadMessageCount, forKey: "post_unread_message_count")
        }

	}

}
