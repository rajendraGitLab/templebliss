//
//  ShowSummaryResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 28, 2021

import Foundation
import SwiftyJSON


class ShowSummaryResModel : NSObject, NSCoding{

    var data : Datum!
    var status : Bool!
    var message : String!
    
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        let dataJson = json["data"]
        if !dataJson.isEmpty{
            data = Datum(fromJson: dataJson)
        }
        status = json["status"].boolValue
        message = json["message"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if data != nil{
        	dictionary["data"] = data.toDictionary()
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if message != nil{
            dictionary["message"] = message
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		data = aDecoder.decodeObject(forKey: "data") as? Datum
		status = aDecoder.decodeObject(forKey: "status") as? Bool
        message = aDecoder.decodeObject(forKey: "message") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if data != nil{
			aCoder.encode(data, forKey: "data")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }

	}

}
