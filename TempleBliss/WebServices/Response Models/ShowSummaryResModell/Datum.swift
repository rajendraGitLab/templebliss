//
//  Datum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on April 28, 2021

import Foundation
import SwiftyJSON


class Datum : NSObject, NSCoding{
    
    var bookingAmount : String!
    var discountType : String!
    var discountValue : String!
    var freeMinute : String!
    var totalChargeableAmount : String!
    var totalMinute : String!
    var advisorSpecialDayCommission : String!
    var commissionRate : String!
    var sessionEarning : String!
    var category_name : String!
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        bookingAmount = json["booking_amount"].stringValue
        discountType = json["discount_type"].stringValue
        discountValue = json["discount_value"].stringValue
        freeMinute = json["free_minute"].stringValue
        totalChargeableAmount = json["total_chargeable_amount"].stringValue
        totalMinute = json["total_minute"].stringValue
        
        advisorSpecialDayCommission = json["advisor_special_day_commission"].stringValue
        commissionRate = json["commission_rate"].stringValue
        sessionEarning = json["session_earning"].stringValue
        category_name = json["category_name"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if bookingAmount != nil{
            dictionary["booking_amount"] = bookingAmount
        }
        if discountType != nil{
            dictionary["discount_type"] = discountType
        }
        if discountValue != nil{
            dictionary["discount_value"] = discountValue
        }
        if freeMinute != nil{
            dictionary["free_minute"] = freeMinute
        }
        if totalChargeableAmount != nil{
            dictionary["total_chargeable_amount"] = totalChargeableAmount
        }
        if totalMinute != nil{
            dictionary["total_minute"] = totalMinute
        }
        
        if advisorSpecialDayCommission != nil{
            dictionary["advisor_special_day_commission"] = advisorSpecialDayCommission
        }
        if commissionRate != nil{
            dictionary["commission_rate"] = commissionRate
        }
        if sessionEarning != nil{
            dictionary["session_earning"] = sessionEarning
        }
        if category_name != nil{
            dictionary["category_name"] = category_name
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        bookingAmount = aDecoder.decodeObject(forKey: "booking_amount") as? String
        discountType = aDecoder.decodeObject(forKey: "discount_type") as? String
        discountValue = aDecoder.decodeObject(forKey: "discount_value") as? String
        freeMinute = aDecoder.decodeObject(forKey: "free_minute") as? String
        totalChargeableAmount = aDecoder.decodeObject(forKey: "total_chargeable_amount") as? String
        totalMinute = aDecoder.decodeObject(forKey: "total_minute") as? String
        
        advisorSpecialDayCommission = aDecoder.decodeObject(forKey: "advisor_special_day_commission") as? String
        commissionRate = aDecoder.decodeObject(forKey: "commission_rate") as? String
        sessionEarning = aDecoder.decodeObject(forKey: "session_earning") as? String
        category_name = aDecoder.decodeObject(forKey: "category_name") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    
    func encode(with aCoder: NSCoder)
    {
        if bookingAmount != nil{
            aCoder.encode(bookingAmount, forKey: "booking_amount")
        }
        if discountType != nil{
            aCoder.encode(discountType, forKey: "discount_type")
        }
        if discountValue != nil{
            aCoder.encode(discountValue, forKey: "discount_value")
        }
        if freeMinute != nil{
            aCoder.encode(freeMinute, forKey: "free_minute")
        }
        if totalChargeableAmount != nil{
            aCoder.encode(totalChargeableAmount, forKey: "total_chargeable_amount")
        }
        if totalMinute != nil{
            aCoder.encode(totalMinute, forKey: "total_minute")
        }
        
        
        if advisorSpecialDayCommission != nil{
            aCoder.encode(advisorSpecialDayCommission, forKey: "advisor_special_day_commission")
        }
        if commissionRate != nil{
            aCoder.encode(commissionRate, forKey: "commission_rate")
        }
        if sessionEarning != nil{
            aCoder.encode(sessionEarning, forKey: "session_earning")
        }
        if category_name != nil{
            aCoder.encode(category_name, forKey: "category_name")
        }
    }
    
}
