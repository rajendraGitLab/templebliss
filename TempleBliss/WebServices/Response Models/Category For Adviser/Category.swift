//
//  Category.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 25, 2021

import Foundation
import SwiftyJSON


class Category : NSObject, NSCoding{

    var createdAt : String!
    var deletedAt : String!
    var descriptionField : String!
    var id : String!
    var image : String!
    var name : String!
    var priceRange : [PriceRange]!
    var status : String!
    var updatedAt : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        createdAt = json["created_at"].stringValue
        deletedAt = json["deleted_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        image = json["image"].stringValue
        name = json["name"].stringValue
        priceRange = [PriceRange]()
        let priceRangeArray = json["price_range"].arrayValue
        for priceRangeJson in priceRangeArray{
            let value = PriceRange(fromJson: priceRangeJson)
            priceRange.append(value)
        }
        status = json["status"].stringValue
        updatedAt = json["updated_at"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if createdAt != nil{
        	dictionary["created_at"] = createdAt
        }
        if deletedAt != nil{
        	dictionary["deleted_at"] = deletedAt
        }
        if descriptionField != nil{
        	dictionary["description"] = descriptionField
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if image != nil{
        	dictionary["image"] = image
        }
        if name != nil{
        	dictionary["name"] = name
        }
        if priceRange != nil{
        var dictionaryElements = [[String:Any]]()
        for priceRangeElement in priceRange {
        	dictionaryElements.append(priceRangeElement.toDictionary())
        }
        dictionary["priceRange"] = dictionaryElements
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if updatedAt != nil{
        	dictionary["updated_at"] = updatedAt
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
		deletedAt = aDecoder.decodeObject(forKey: "deleted_at") as? String
		descriptionField = aDecoder.decodeObject(forKey: "description") as? String
		id = aDecoder.decodeObject(forKey: "id") as? String
		image = aDecoder.decodeObject(forKey: "image") as? String
		name = aDecoder.decodeObject(forKey: "name") as? String
		priceRange = aDecoder.decodeObject(forKey: "price_range") as? [PriceRange]
		status = aDecoder.decodeObject(forKey: "status") as? String
		updatedAt = aDecoder.decodeObject(forKey: "updated_at") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if deletedAt != nil{
			aCoder.encode(deletedAt, forKey: "deleted_at")
		}
		if descriptionField != nil{
			aCoder.encode(descriptionField, forKey: "description")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if priceRange != nil{
			aCoder.encode(priceRange, forKey: "price_range")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if updatedAt != nil{
			aCoder.encode(updatedAt, forKey: "updated_at")
		}

	}

}
