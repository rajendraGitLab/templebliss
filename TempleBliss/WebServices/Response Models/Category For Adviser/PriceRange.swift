//
//  PriceRange.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 25, 2021

import Foundation
import SwiftyJSON


class PriceRange : NSObject, NSCoding{

    var from : String!
    var to : String!
    var type : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        from = json["from"].stringValue
        to = json["to"].stringValue
        type = json["type"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if from != nil{
        	dictionary["from"] = from
        }
        if to != nil{
        	dictionary["to"] = to
        }
        if type != nil{
        	dictionary["type"] = type
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		from = aDecoder.decodeObject(forKey: "from") as? String
		to = aDecoder.decodeObject(forKey: "to") as? String
		type = aDecoder.decodeObject(forKey: "type") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if from != nil{
			aCoder.encode(from, forKey: "from")
		}
		if to != nil{
			aCoder.encode(to, forKey: "to")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}

	}

}
