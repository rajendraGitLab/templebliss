//
//  WalletHistory.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 8, 2021

import Foundation
import SwiftyJSON


class WalletHistory : NSObject, NSCoding{

    var amount : String!
    var amountType : String!
    var bookingId : String!
    var createdAt : String!
    var descriptionField : String!
    var id : String!
    var userId : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        amount = json["amount"].stringValue
        amountType = json["amount_type"].stringValue
        bookingId = json["booking_id"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        id = json["id"].stringValue
        userId = json["user_id"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if amount != nil{
            dictionary["amount"] = amount
        }
        if amountType != nil{
            dictionary["amount_type"] = amountType
        }
        if bookingId != nil{
            dictionary["booking_id"] = bookingId
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if id != nil{
            dictionary["id"] = id
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        amount = aDecoder.decodeObject(forKey: "amount") as? String
        amountType = aDecoder.decodeObject(forKey: "amount_type") as? String
        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if amount != nil{
            aCoder.encode(amount, forKey: "amount")
        }
        if amountType != nil{
            aCoder.encode(amountType, forKey: "amount_type")
        }
        if bookingId != nil{
            aCoder.encode(bookingId, forKey: "booking_id")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }

    }

}
