//
//  WalletHistoryResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 16, 2021

import Foundation
import SwiftyJSON


class WalletHistoryResModel : NSObject, NSCoding{

    var status : Bool!
    var walletAmount : String!
    var walletHistory : [WalletHistory]!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        status = json["status"].boolValue
        walletAmount = json["wallet_amount"].stringValue
        walletHistory = [WalletHistory]()
        let walletHistoryArray = json["wallet_history"].arrayValue
        for walletHistoryJson in walletHistoryArray{
            let value = WalletHistory(fromJson: walletHistoryJson)
            walletHistory.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if status != nil{
        	dictionary["status"] = status
        }
        if walletAmount != nil{
        	dictionary["wallet_amount"] = walletAmount
        }
        if walletHistory != nil{
        var dictionaryElements = [[String:Any]]()
        for walletHistoryElement in walletHistory {
        	dictionaryElements.append(walletHistoryElement.toDictionary())
        }
        dictionary["walletHistory"] = dictionaryElements
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		status = aDecoder.decodeObject(forKey: "status") as? Bool
		walletAmount = aDecoder.decodeObject(forKey: "wallet_amount") as? String
		walletHistory = aDecoder.decodeObject(forKey: "wallet_history") as? [WalletHistory]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
		if walletAmount != nil{
			aCoder.encode(walletAmount, forKey: "wallet_amount")
		}
		if walletHistory != nil{
			aCoder.encode(walletHistory, forKey: "wallet_history")
		}

	}

}
