//
//  Availability.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 29, 2021

//import Foundation
//import SwiftyJSON
//
//
//class AvailabilityForAdviser : NSObject, NSCoding{
//    var categoryId : String!
//    var price : String!
//    var status : String!
//    var type : String!
//    var userId : String!
//
//    /**
//     * Instantiate the instance using the passed json values to set the properties values
//     */
//    init(fromJson json: JSON!){
//        if json.isEmpty{
//            return
//        }
//        categoryId = json["category_id"].stringValue
//        price = json["price"].stringValue
//        status = json["status"].stringValue
//        type = json["type"].stringValue
//        userId = json["user_id"].stringValue
//    }
//
//    /**
//     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
//     */
//    func toDictionary() -> [String:Any]
//    {
//        var dictionary = [String:Any]()
//        if categoryId != nil{
//            dictionary["category_id"] = categoryId
//        }
//        if price != nil{
//            dictionary["price"] = price
//        }
//        if status != nil{
//            dictionary["status"] = status
//        }
//        if type != nil{
//            dictionary["type"] = type
//        }
//        if userId != nil{
//            dictionary["user_id"] = userId
//        }
//        return dictionary
//    }
//
//    /**
//    * NSCoding required initializer.
//    * Fills the data from the passed decoder
//    */
//    @objc required init(coder aDecoder: NSCoder)
//    {
//        categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
//        price = aDecoder.decodeObject(forKey: "price") as? String
//        status = aDecoder.decodeObject(forKey: "status") as? String
//        type = aDecoder.decodeObject(forKey: "type") as? String
//        userId = aDecoder.decodeObject(forKey: "user_id") as? String
//    }
//
//    /**
//    * NSCoding required method.
//    * Encodes mode properties into the decoder
//    */
//    func encode(with aCoder: NSCoder)
//    {
//        if categoryId != nil{
//            aCoder.encode(categoryId, forKey: "category_id")
//        }
//        if price != nil{
//            aCoder.encode(price, forKey: "price")
//        }
//        if status != nil{
//            aCoder.encode(status, forKey: "status")
//        }
//        if type != nil{
//            aCoder.encode(type, forKey: "type")
//        }
//        if userId != nil{
//            aCoder.encode(userId, forKey: "user_id")
//        }
//
//    }
//
//}
import Foundation
import SwiftyJSON


class AvailabilityForAdviser : NSObject, NSCoding{

    var categoryId : String!
    var discountPrice : String!
    var price : String!
    var status : String!
    var type : String!
    var userId : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        categoryId = json["category_id"].stringValue
        discountPrice = json["discount_price"].stringValue
        price = json["price"].stringValue
        status = json["status"].stringValue
        type = json["type"].stringValue
        userId = json["user_id"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if categoryId != nil{
            dictionary["category_id"] = categoryId
        }
        if discountPrice != nil{
            dictionary["discount_price"] = discountPrice
        }
        if price != nil{
            dictionary["price"] = price
        }
        if status != nil{
            dictionary["status"] = status
        }
        if type != nil{
            dictionary["type"] = type
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        categoryId = aDecoder.decodeObject(forKey: "category_id") as? String
        discountPrice = aDecoder.decodeObject(forKey: "discount_price") as? String
        price = aDecoder.decodeObject(forKey: "price") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if categoryId != nil{
            aCoder.encode(categoryId, forKey: "category_id")
        }
        if discountPrice != nil{
            aCoder.encode(discountPrice, forKey: "discount_price")
        }
        if price != nil{
            aCoder.encode(price, forKey: "price")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }

    }

}
