//
//  adviserListResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 29, 2021

import Foundation
import SwiftyJSON


class adviserListResModel : NSObject, NSCoding{

    var advisors : [Advisor]!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        advisors = [Advisor]()
        let advisorsArray = json["advisors"].arrayValue
        for advisorsJson in advisorsArray{
            let value = Advisor(fromJson: advisorsJson)
            advisors.append(value)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisors != nil{
        var dictionaryElements = [[String:Any]]()
        for advisorsElement in advisors {
        	dictionaryElements.append(advisorsElement.toDictionary())
        }
        dictionary["advisors"] = dictionaryElements
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisors = aDecoder.decodeObject(forKey: "advisors") as? [Advisor]
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisors != nil{
			aCoder.encode(advisors, forKey: "advisors")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
