//
//  Advisor.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 11, 2021

import Foundation
import SwiftyJSON


class Advisor : NSObject, NSCoding{

    var advisorDescription : String!
    var availabileFor : String!
    var availabileForCount : Int!
    var availability : [AvailabilityForAdviser]!
//    var availability : [Availability]!
    var avgRating : String!
    var categories : String!
    var categoriesName : String!
    var fullName : String!
    var id : String!
    var isAvailabile : Int!
    var isFavourite : String!
    var nickName : String!
    var profilePicture : String!
    var ratings : Int!
    var reviewHistory : [ReviewHistoryAdvisorList]!
    var totalRating : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        advisorDescription = json["advisor_description"].stringValue
        availabileFor = json["availabile_for"].stringValue
        availabileForCount = json["availabile_for_count"].intValue
        availability = [AvailabilityForAdviser]()
        let availabilityArray = json["availability"].arrayValue
        for availabilityJson in availabilityArray{
            let value = AvailabilityForAdviser(fromJson: availabilityJson)
            availability.append(value)
        }
        avgRating = json["avg_rating"].stringValue
        categories = json["categories"].stringValue
        categoriesName = json["categories_name"].stringValue
        fullName = json["full_name"].stringValue
        id = json["id"].stringValue
        isAvailabile = json["is_availabile"].intValue
        isFavourite = json["is_favourite"].stringValue
        nickName = json["nick_name"].stringValue
        profilePicture = json["profile_picture"].stringValue
        ratings = json["ratings"].intValue
        reviewHistory = [ReviewHistoryAdvisorList]()
        let reviewHistoryArray = json["review_history"].arrayValue
        for reviewHistoryJson in reviewHistoryArray{
            let value = ReviewHistoryAdvisorList(fromJson: reviewHistoryJson)
            reviewHistory.append(value)
        }
        totalRating = json["total_rating"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if advisorDescription != nil{
            dictionary["advisor_description"] = advisorDescription
        }
        if availabileFor != nil{
            dictionary["availabile_for"] = availabileFor
        }
        if availabileForCount != nil{
            dictionary["availabile_for_count"] = availabileForCount
        }
        if availability != nil{
        var dictionaryElements = [[String:Any]]()
        for availabilityElement in availability {
            dictionaryElements.append(availabilityElement.toDictionary())
        }
        dictionary["availability"] = dictionaryElements
        }
        if avgRating != nil{
            dictionary["avg_rating"] = avgRating
        }
        if categories != nil{
            dictionary["categories"] = categories
        }
        if categoriesName != nil{
            dictionary["categories_name"] = categoriesName
        }
        if fullName != nil{
            dictionary["full_name"] = fullName
        }
        if id != nil{
            dictionary["id"] = id
        }
        if isAvailabile != nil{
            dictionary["is_availabile"] = isAvailabile
        }
        if isFavourite != nil{
            dictionary["is_favourite"] = isFavourite
        }
        if nickName != nil{
            dictionary["nick_name"] = nickName
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if ratings != nil{
            dictionary["ratings"] = ratings
        }
        if reviewHistory != nil{
        var dictionaryElements = [[String:Any]]()
        for reviewHistoryElement in reviewHistory {
            dictionaryElements.append(reviewHistoryElement.toDictionary())
        }
        dictionary["reviewHistory"] = dictionaryElements
        }
        if totalRating != nil{
            dictionary["total_rating"] = totalRating
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        advisorDescription = aDecoder.decodeObject(forKey: "advisor_description") as? String
        availabileFor = aDecoder.decodeObject(forKey: "availabile_for") as? String
        availabileForCount = aDecoder.decodeObject(forKey: "availabile_for_count") as? Int
        availability = aDecoder.decodeObject(forKey: "availability") as? [AvailabilityForAdviser]
        avgRating = aDecoder.decodeObject(forKey: "avg_rating") as? String
        categories = aDecoder.decodeObject(forKey: "categories") as? String
        categoriesName = aDecoder.decodeObject(forKey: "categories_name") as? String
        fullName = aDecoder.decodeObject(forKey: "full_name") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        isAvailabile = aDecoder.decodeObject(forKey: "is_availabile") as? Int
        isFavourite = aDecoder.decodeObject(forKey: "is_favourite") as? String
        nickName = aDecoder.decodeObject(forKey: "nick_name") as? String
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        ratings = aDecoder.decodeObject(forKey: "ratings") as? Int
        reviewHistory = aDecoder.decodeObject(forKey: "review_history") as? [ReviewHistoryAdvisorList]
        totalRating = aDecoder.decodeObject(forKey: "total_rating") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if advisorDescription != nil{
            aCoder.encode(advisorDescription, forKey: "advisor_description")
        }
        if availabileFor != nil{
            aCoder.encode(availabileFor, forKey: "availabile_for")
        }
        if availabileForCount != nil{
            aCoder.encode(availabileForCount, forKey: "availabile_for_count")
        }
        if availability != nil{
            aCoder.encode(availability, forKey: "availability")
        }
        if avgRating != nil{
            aCoder.encode(avgRating, forKey: "avg_rating")
        }
        if categories != nil{
            aCoder.encode(categories, forKey: "categories")
        }
        if categoriesName != nil{
            aCoder.encode(categoriesName, forKey: "categories_name")
        }
        if fullName != nil{
            aCoder.encode(fullName, forKey: "full_name")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if isAvailabile != nil{
            aCoder.encode(isAvailabile, forKey: "is_availabile")
        }
        if isFavourite != nil{
            aCoder.encode(isFavourite, forKey: "is_favourite")
        }
        if nickName != nil{
            aCoder.encode(nickName, forKey: "nick_name")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if ratings != nil{
            aCoder.encode(ratings, forKey: "ratings")
        }
        if reviewHistory != nil{
            aCoder.encode(reviewHistory, forKey: "review_history")
        }
        if totalRating != nil{
            aCoder.encode(totalRating, forKey: "total_rating")
        }

    }

}
