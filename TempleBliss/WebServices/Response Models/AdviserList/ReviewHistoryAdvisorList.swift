//
//  ReviewHistory.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 11, 2021

import Foundation
import SwiftyJSON


class ReviewHistoryAdvisorList : NSObject, NSCoding{

    var advisorId : String!
    var createdAt : String!
    var fullName : String!
    var note : String!
    var profilePicture : String!
    var rating : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        advisorId = json["advisor_id"].stringValue
        createdAt = json["created_at"].stringValue
        fullName = json["full_name"].stringValue
        note = json["note"].stringValue
        profilePicture = json["profile_picture"].stringValue
        rating = json["rating"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisorId != nil{
        	dictionary["advisor_id"] = advisorId
        }
        if createdAt != nil{
        	dictionary["created_at"] = createdAt
        }
        if fullName != nil{
        	dictionary["full_name"] = fullName
        }
        if note != nil{
        	dictionary["note"] = note
        }
        if profilePicture != nil{
        	dictionary["profile_picture"] = profilePicture
        }
        if rating != nil{
        	dictionary["rating"] = rating
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisorId = aDecoder.decodeObject(forKey: "advisor_id") as? String
		createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
		fullName = aDecoder.decodeObject(forKey: "full_name") as? String
		note = aDecoder.decodeObject(forKey: "note") as? String
		profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
		rating = aDecoder.decodeObject(forKey: "rating") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisorId != nil{
			aCoder.encode(advisorId, forKey: "advisor_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if fullName != nil{
			aCoder.encode(fullName, forKey: "full_name")
		}
		if note != nil{
			aCoder.encode(note, forKey: "note")
		}
		if profilePicture != nil{
			aCoder.encode(profilePicture, forKey: "profile_picture")
		}
		if rating != nil{
			aCoder.encode(rating, forKey: "rating")
		}

	}

}
