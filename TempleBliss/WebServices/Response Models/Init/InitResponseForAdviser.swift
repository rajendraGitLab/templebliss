//
//  InitResponseForAdviser.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 25, 2021

import Foundation
import SwiftyJSON


class InitResponseForAdviser : NSObject, NSCoding{

    var categoryCount : String!
    var status : Bool!
    var device_token:String!
    var current_time:String!
    var current_time2:String!
    var profile:Profile!
	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        categoryCount = json["category_count"].stringValue
        status = json["status"].boolValue
        device_token = json["device_token"].stringValue
        current_time = json["current_time"].stringValue
        current_time2 = json["current_time2"].stringValue
        let profileJson = json["profile"]
        if !profileJson.isEmpty{
            profile = Profile(fromJson: profileJson)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if categoryCount != nil{
        	dictionary["category_count"] = categoryCount
        }
        if status != nil{
        	dictionary["status"] = status
        }
        if device_token != nil {
            dictionary["device_token"] = device_token
        }
        if current_time != nil {
            dictionary["current_time"] = current_time
        }
        if current_time2 != nil {
            dictionary["current_time2"] = current_time2
        }
        if profile != nil{
            dictionary["profile"] = profile.toDictionary()
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		categoryCount = aDecoder.decodeObject(forKey: "category_count") as? String
		status = aDecoder.decodeObject(forKey: "status") as? Bool
        device_token = aDecoder.decodeObject(forKey: "device_token") as? String
        current_time = aDecoder.decodeObject(forKey: "current_time") as? String
        current_time2 = aDecoder.decodeObject(forKey: "current_time2") as? String
        profile = aDecoder.decodeObject(forKey: "profile") as? Profile
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if categoryCount != nil{
			aCoder.encode(categoryCount, forKey: "category_count")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}
        if device_token != nil{
            aCoder.encode(device_token, forKey: "device_token")
        }
        if current_time != nil{
            aCoder.encode(device_token, forKey: "current_time")
        }
        if current_time2 != nil{
            aCoder.encode(device_token, forKey: "current_time2")
        }
        if profile != nil{
            aCoder.encode(profile, forKey: "profile")
        }
	}

}
