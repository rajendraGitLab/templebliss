//
//  minutesResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 3, 2021

import Foundation
import SwiftyJSON


class minutesResModel : NSObject, NSCoding{

    var minutes : [String]!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        minutes = [String]()
        let minutesArray = json["minutes"].arrayValue
        for minutesJson in minutesArray{
            minutes.append(minutesJson.stringValue)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if minutes != nil{
        	dictionary["minutes"] = minutes
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		minutes = aDecoder.decodeObject(forKey: "minutes") as? [String]
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if minutes != nil{
			aCoder.encode(minutes, forKey: "minutes")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
