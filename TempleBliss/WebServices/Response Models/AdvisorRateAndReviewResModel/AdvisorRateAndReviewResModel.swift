//
//  AdvisorRateAndReviewResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 10, 2021

import Foundation
import SwiftyJSON


class AdvisorRateAndReviewResModel : NSObject, NSCoding{

    var message : String!
    var reviewHistory : [ReviewHistory]!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        message = json["message"].stringValue
        reviewHistory = [ReviewHistory]()
        let reviewHistoryArray = json["review_history"].arrayValue
        for reviewHistoryJson in reviewHistoryArray{
            let value = ReviewHistory(fromJson: reviewHistoryJson)
            reviewHistory.append(value)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if message != nil{
        	dictionary["message"] = message
        }
        if reviewHistory != nil{
        var dictionaryElements = [[String:Any]]()
        for reviewHistoryElement in reviewHistory {
        	dictionaryElements.append(reviewHistoryElement.toDictionary())
        }
        dictionary["reviewHistory"] = dictionaryElements
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		message = aDecoder.decodeObject(forKey: "message") as? String
		reviewHistory = aDecoder.decodeObject(forKey: "review_history") as? [ReviewHistory]
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if reviewHistory != nil{
			aCoder.encode(reviewHistory, forKey: "review_history")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
