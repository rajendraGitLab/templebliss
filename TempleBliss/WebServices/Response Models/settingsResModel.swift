//
//  settingsResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 29, 2021

import Foundation
import SwiftyJSON


class settingsResModel : NSObject, NSCoding{

    var advisorTermsCondition : String!
    var customerTermsCondition : String!
    var message : String!
    var privacyPolicy : String!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        advisorTermsCondition = json["advisor_terms_condition"].stringValue
        customerTermsCondition = json["customer_terms_condition"].stringValue
        message = json["message"].stringValue
        privacyPolicy = json["privacy_policy"].stringValue
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisorTermsCondition != nil{
        	dictionary["advisor_terms_condition"] = advisorTermsCondition
        }
        if customerTermsCondition != nil{
        	dictionary["customer_terms_condition"] = customerTermsCondition
        }
        if message != nil{
        	dictionary["message"] = message
        }
        if privacyPolicy != nil{
        	dictionary["privacy_policy"] = privacyPolicy
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisorTermsCondition = aDecoder.decodeObject(forKey: "advisor_terms_condition") as? String
		customerTermsCondition = aDecoder.decodeObject(forKey: "customer_terms_condition") as? String
		message = aDecoder.decodeObject(forKey: "message") as? String
		privacyPolicy = aDecoder.decodeObject(forKey: "privacy_policy") as? String
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisorTermsCondition != nil{
			aCoder.encode(advisorTermsCondition, forKey: "advisor_terms_condition")
		}
		if customerTermsCondition != nil{
			aCoder.encode(customerTermsCondition, forKey: "customer_terms_condition")
		}
		if message != nil{
			aCoder.encode(message, forKey: "message")
		}
		if privacyPolicy != nil{
			aCoder.encode(privacyPolicy, forKey: "privacy_policy")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
