//
//  NotesResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 24, 2021

import Foundation
import SwiftyJSON


class NotesResModel : NSObject, NSCoding{

    var notes : Note!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        let notesJson = json["notes"]
        if !notesJson.isEmpty{
            notes = Note(fromJson: notesJson)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if notes != nil{
        	dictionary["notes"] = notes.toDictionary()
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		notes = aDecoder.decodeObject(forKey: "notes") as? Note
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if notes != nil{
			aCoder.encode(notes, forKey: "notes")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
