//
//  Note.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on September 24, 2021

import Foundation
import SwiftyJSON


class Note : NSObject, NSCoding{

    var advisorId : String!
    var createdAt : String!
    var id : String!
    var note : String!
    var userId : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        advisorId = json["advisor_id"].stringValue
        createdAt = json["created_at"].stringValue
        id = json["id"].stringValue
        note = json["note"].stringValue
        userId = json["user_id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisorId != nil{
        	dictionary["advisor_id"] = advisorId
        }
        if createdAt != nil{
        	dictionary["created_at"] = createdAt
        }
        if id != nil{
        	dictionary["id"] = id
        }
        if note != nil{
        	dictionary["note"] = note
        }
        if userId != nil{
        	dictionary["user_id"] = userId
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisorId = aDecoder.decodeObject(forKey: "advisor_id") as? String
		createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
		id = aDecoder.decodeObject(forKey: "id") as? String
		note = aDecoder.decodeObject(forKey: "note") as? String
		userId = aDecoder.decodeObject(forKey: "user_id") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisorId != nil{
			aCoder.encode(advisorId, forKey: "advisor_id")
		}
		if createdAt != nil{
			aCoder.encode(createdAt, forKey: "created_at")
		}
		if id != nil{
			aCoder.encode(id, forKey: "id")
		}
		if note != nil{
			aCoder.encode(note, forKey: "note")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
