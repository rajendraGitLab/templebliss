//
//  MyEarningDetail.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 15, 2021

import Foundation
import SwiftyJSON


class MyEarningDetail : NSObject, NSCoding{

    var advisorId : String!
    var availableForWithdraw : String!
    var paymentFee : String!
    var pendingWithdrawAmount : String!
    var totalEarning : String!
    var totalWithdrawedAmount : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        advisorId = json["advisor_id"].stringValue
        availableForWithdraw = json["available_for_withdraw"].stringValue
        paymentFee = json["payment_fee"].stringValue
        pendingWithdrawAmount = json["pending_withdraw_amount"].stringValue
        totalEarning = json["total_earning"].stringValue
        totalWithdrawedAmount = json["total_withdrawed_amount"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if advisorId != nil{
            dictionary["advisor_id"] = advisorId
        }
        if availableForWithdraw != nil{
            dictionary["available_for_withdraw"] = availableForWithdraw
        }
        if paymentFee != nil{
            dictionary["payment_fee"] = paymentFee
        }
        if pendingWithdrawAmount != nil{
            dictionary["pending_withdraw_amount"] = pendingWithdrawAmount
        }
        if totalEarning != nil{
            dictionary["total_earning"] = totalEarning
        }
        if totalWithdrawedAmount != nil{
            dictionary["total_withdrawed_amount"] = totalWithdrawedAmount
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        advisorId = aDecoder.decodeObject(forKey: "advisor_id") as? String
        availableForWithdraw = aDecoder.decodeObject(forKey: "available_for_withdraw") as? String
        paymentFee = aDecoder.decodeObject(forKey: "payment_fee") as? String
        pendingWithdrawAmount = aDecoder.decodeObject(forKey: "pending_withdraw_amount") as? String
        totalEarning = aDecoder.decodeObject(forKey: "total_earning") as? String
        totalWithdrawedAmount = aDecoder.decodeObject(forKey: "total_withdrawed_amount") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if advisorId != nil{
            aCoder.encode(advisorId, forKey: "advisor_id")
        }
        if availableForWithdraw != nil{
            aCoder.encode(availableForWithdraw, forKey: "available_for_withdraw")
        }
        if paymentFee != nil{
            aCoder.encode(paymentFee, forKey: "payment_fee")
        }
        if pendingWithdrawAmount != nil{
            aCoder.encode(pendingWithdrawAmount, forKey: "pending_withdraw_amount")
        }
        if totalEarning != nil{
            aCoder.encode(totalEarning, forKey: "total_earning")
        }
        if totalWithdrawedAmount != nil{
            aCoder.encode(totalWithdrawedAmount, forKey: "total_withdrawed_amount")
        }

    }

}
