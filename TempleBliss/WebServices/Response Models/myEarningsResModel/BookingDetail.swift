//
//  BookingDetail.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 15, 2021

import Foundation
import SwiftyJSON


class BookingDetail : NSObject, NSCoding{

    var advisorCommission : String!
    var bookingId : String!
    var customerName : String!
    var endTime : String!
    var minute : String!
    var paymentDate : String!
    var paymentFee : String!
    var profilePicture : String!
    var sessionMinute : String!
    var timeAgo : String!
    var type : String!
    var withdrawalDate : String!
    var withdrawalStatus : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        advisorCommission = json["advisor_commission"].stringValue
        bookingId = json["booking_id"].stringValue
        customerName = json["customer_name"].stringValue
        endTime = json["end_time"].stringValue
        minute = json["minute"].stringValue
        paymentDate = json["payment_date"].stringValue
        paymentFee = json["payment_fee"].stringValue
        profilePicture = json["profile_picture"].stringValue
        sessionMinute = json["session_minute"].stringValue
        timeAgo = json["time_ago"].stringValue
        type = json["type"].stringValue
        withdrawalDate = json["withdrawal_date"].stringValue
        withdrawalStatus = json["withdrawal_status"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if advisorCommission != nil{
            dictionary["advisor_commission"] = advisorCommission
        }
        if bookingId != nil{
            dictionary["booking_id"] = bookingId
        }
        if customerName != nil{
            dictionary["customer_name"] = customerName
        }
        if endTime != nil{
            dictionary["end_time"] = endTime
        }
        if minute != nil{
            dictionary["minute"] = minute
        }
        if paymentDate != nil{
            dictionary["payment_date"] = paymentDate
        }
        if paymentFee != nil{
            dictionary["payment_fee"] = paymentFee
        }
        if profilePicture != nil{
            dictionary["profile_picture"] = profilePicture
        }
        if sessionMinute != nil{
            dictionary["session_minute"] = sessionMinute
        }
        if timeAgo != nil{
            dictionary["time_ago"] = timeAgo
        }
        if type != nil{
            dictionary["type"] = type
        }
        if withdrawalDate != nil{
            dictionary["withdrawal_date"] = withdrawalDate
        }
        if withdrawalStatus != nil{
            dictionary["withdrawal_status"] = withdrawalStatus
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        advisorCommission = aDecoder.decodeObject(forKey: "advisor_commission") as? String
        bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
        customerName = aDecoder.decodeObject(forKey: "customer_name") as? String
        endTime = aDecoder.decodeObject(forKey: "end_time") as? String
        minute = aDecoder.decodeObject(forKey: "minute") as? String
        paymentDate = aDecoder.decodeObject(forKey: "payment_date") as? String
        paymentFee = aDecoder.decodeObject(forKey: "payment_fee") as? String
        profilePicture = aDecoder.decodeObject(forKey: "profile_picture") as? String
        sessionMinute = aDecoder.decodeObject(forKey: "session_minute") as? String
        timeAgo = aDecoder.decodeObject(forKey: "time_ago") as? String
        type = aDecoder.decodeObject(forKey: "type") as? String
        withdrawalDate = aDecoder.decodeObject(forKey: "withdrawal_date") as? String
        withdrawalStatus = aDecoder.decodeObject(forKey: "withdrawal_status") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if advisorCommission != nil{
            aCoder.encode(advisorCommission, forKey: "advisor_commission")
        }
        if bookingId != nil{
            aCoder.encode(bookingId, forKey: "booking_id")
        }
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customer_name")
        }
        if endTime != nil{
            aCoder.encode(endTime, forKey: "end_time")
        }
        if minute != nil{
            aCoder.encode(minute, forKey: "minute")
        }
        if paymentDate != nil{
            aCoder.encode(paymentDate, forKey: "payment_date")
        }
        if paymentFee != nil{
            aCoder.encode(paymentFee, forKey: "payment_fee")
        }
        if profilePicture != nil{
            aCoder.encode(profilePicture, forKey: "profile_picture")
        }
        if sessionMinute != nil{
            aCoder.encode(sessionMinute, forKey: "session_minute")
        }
        if timeAgo != nil{
            aCoder.encode(timeAgo, forKey: "time_ago")
        }
        if type != nil{
            aCoder.encode(type, forKey: "type")
        }
        if withdrawalDate != nil{
            aCoder.encode(withdrawalDate, forKey: "withdrawal_date")
        }
        if withdrawalStatus != nil{
            aCoder.encode(withdrawalStatus, forKey: "withdrawal_status")
        }

    }

}
