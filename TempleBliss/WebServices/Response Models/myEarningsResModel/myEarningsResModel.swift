//
//  myEarningsResModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 24, 2021

import Foundation
import SwiftyJSON


class myEarningsResModel : NSObject, NSCoding{

    var bookingDetails : [BookingDetail]!
    var myEarningDetails : MyEarningDetail!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        bookingDetails = [BookingDetail]()
        let bookingDetailsArray = json["booking_details"].arrayValue
        for bookingDetailsJson in bookingDetailsArray{
            let value = BookingDetail(fromJson: bookingDetailsJson)
            bookingDetails.append(value)
        }
        let myEarningDetailsJson = json["my_earning_details"]
        if !myEarningDetailsJson.isEmpty{
            myEarningDetails = MyEarningDetail(fromJson: myEarningDetailsJson)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if bookingDetails != nil{
        var dictionaryElements = [[String:Any]]()
        for bookingDetailsElement in bookingDetails {
        	dictionaryElements.append(bookingDetailsElement.toDictionary())
        }
        dictionary["bookingDetails"] = dictionaryElements
        }
        if myEarningDetails != nil{
        	dictionary["myEarningDetails"] = myEarningDetails.toDictionary()
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		bookingDetails = aDecoder.decodeObject(forKey: "booking_details") as? [BookingDetail]
		myEarningDetails = aDecoder.decodeObject(forKey: "my_earning_details") as? MyEarningDetail
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if bookingDetails != nil{
			aCoder.encode(bookingDetails, forKey: "booking_details")
		}
		if myEarningDetails != nil{
			aCoder.encode(myEarningDetails, forKey: "my_earning_details")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
