import Foundation

class ValidationError: Error {
    var message: String
    
    init(_ message: String) {
        self.message = message
    }
}

protocol ValidatorConvertible {
    func validated(_ value: String) -> (Bool,String)
}

enum ValidatorType {
    case email
    case password
    case username(field: String)
    case requiredField(field: String)
    case age
    
    case cardHolder
    case cardNumber
    case expDate
    case cvv
}

enum VaildatorFactory {
    static func validatorFor(type: ValidatorType) -> ValidatorConvertible {
        switch type {
        case .email: return EmailValidator()
        case .password: return PasswordValidator()
//        case .username: return UserNameValidator()
        case .requiredField(let fieldName): return RequiredFieldValidator(fieldName)
        case .age: return AgeValidator()
        case .username(let fieldName): return UserNameValidator(fieldName)
        case .cardHolder: return CardHolderValidator()
        case .cardNumber: return CardNumberValidator()
        case .expDate: return ExpDateValidator()
        case .cvv: return CvvValidator()
        }
    }
}



class AgeValidator: ValidatorConvertible {
    func validated(_ value: String) -> (Bool, String)
    {
        guard value.count > 0 else{return (false,ValidationError("Age is required").message)}
        guard let age = Int(value) else {return (false,ValidationError("Age must be a number!").message)}
        guard value.count < 3 else {return (false,ValidationError("Invalid age number!").message)}
        guard age >= 18 else {return (false,ValidationError("You have to be over 18 years old to user our app :)").message)}
        return (true, "")
    }
}

struct RequiredFieldValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    
    func validated(_ value: String) -> (Bool, String) {
        guard !value.isEmpty else {
            return (false,ValidationError("Please enter " + fieldName).message)
        }
        return (true,"")
    }
}

struct UserNameValidator: ValidatorConvertible {
    private let fieldName: String
    
    init(_ field: String) {
        fieldName = field
    }
    func validated(_ value: String) -> (Bool, String) {
        guard value.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {return (false,ValidationError("Please enter \(fieldName)").message)}
        
        guard value.count >= 3 else {
            return (false , ValidationError("\(fieldName.firstCharacterUpperCase() ?? "") must contain more than three characters").message)
//            ValidationError("Username must contain more than three characters" )
        }
        guard value.count < NameTotalCount else {
            return (false , ValidationError("\(fieldName.firstCharacterUpperCase() ?? "") shouldn't contain more than \(NameTotalCount) characters").message)
//            throw ValidationError("Username shoudn't conain more than 18 characters" )
        }
        guard !value.containsEmoji else {
            return (false,ValidationError("\(fieldName.firstCharacterUpperCase() ?? "") shouldn't contain emoji characters").message)
//            throw ValidationError("Username shoudn't conain more than 18 characters" )
        }
        do {
            if try NSRegularExpression(pattern: "[!\"#$%&'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~]+",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) != nil {
                return (false,ValidationError("\(fieldName.firstCharacterUpperCase() ?? "") shouldn't contain special characters").message)
            }
        } catch {
            return (false,ValidationError("\(fieldName.firstCharacterUpperCase() ?? "") shouldn't contain special characters").message)
        }
        
        
        
        return (true , "")
//        return value
    }
    
   /* func validated(_ value: String) throws -> String {
        guard value.count >= 3 else {
            throw ValidationError("Username must contain more than three characters" )
        }
        guard value.count < 18 else {
            throw ValidationError("Username shoudn't conain more than 18 characters" )
        }
        
        do {
            if try NSRegularExpression(pattern: "^[a-z]{1,18}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                throw ValidationError("Invalid username, username should not contain whitespaces, numbers or special characters")
            }
        } catch {
            throw ValidationError("Invalid username, username should not contain whitespaces,  or special characters")
        }
        return value
    } */
}
struct CardHolderValidator: ValidatorConvertible {
    func validated(_ value: String) -> (Bool, String) {
        ()
        let newvalue = value.trim()
        guard newvalue != "" else {
            return (false , ValidationError("Please enter card holder name").message)
           
             }
//        guard newvalue.count >= 2 else {
//            return (false , ValidationError("Cardholder name must contain more than two characters").message)
////            ValidationError("Username must contain more than three characters" )
//        }
//        guard newvalue.count < 26 else {
//            return (false , ValidationError("Cardholder name shoudn't contain more than 26 characters").message)
////            throw ValidationError("Username shoudn't conain more than 18 characters" )
//        }
        
        return (true, "")
    }
    

    
}

struct CardNumberValidator: ValidatorConvertible {
    func validated(_ value: String) -> (Bool, String) {
        guard value.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {return (false,ValidationError("Please enter card number").message)}
        let number = value.replacingOccurrences(of: " ", with: "")
        guard number.count >= 16 else { return(false, ValidationError("Card number must have at least 14 characters").message) }
        
        
        let v = CreditCardValidator()
        guard v.validate(string: number) else { return(false, ValidationError("Please enter a valid card number").message) }
        return (true, "")
    }
    
    
}

struct CvvValidator: ValidatorConvertible {
    func validated(_ value: String) -> (Bool, String) {
        guard value != "" else {return(false, ValidationError("Please enter CVV").message)}
        guard value.count >= 3 && value.count < 5 else { return(false, ValidationError("Please enter valid CVV number").message) }
        return (true, "")
    }
  
}

struct ExpDateValidator: ValidatorConvertible {
    func validated(_ value: String) -> (Bool, String) {
        guard value != "" else {return(false, ValidationError("Please enter expiry date").message)}
        guard expDateValidation(dateStr: value) else { return(false, ValidationError("Please enter a valid expiry date").message) }
        return (true, "")
    }
   
    
    func expDateValidation(dateStr:String) -> Bool {

        let currentYear = Calendar.current.component(.year, from: Date()) % 100   // This will give you current year (i.e. if 2019 then it will be 19)
        let currentMonth = Calendar.current.component(.month, from: Date()) // This will give you current month (i.e if June then it will be 6)

        let enterdYr = Int(dateStr.suffix(2)) ?? 0   // get last two digit from entered string as year
        let enterdMonth = Int(dateStr.prefix(2)) ?? 0  // get first two digit from entered string as month
        print(dateStr) // This is MM/YY Entered by user

        if enterdYr > currentYear{
            if (1 ... 12).contains(enterdMonth){
                return true
            } else{
                return false
            }
        } else  if currentYear == enterdYr {
            if enterdMonth >= currentMonth{
                if (1 ... 12).contains(enterdMonth) {
                    return true
                } else{
                    return false
                }
            } else {
                return false
            }
        } else {
            return true
        }
    }
}

struct PasswordValidator: ValidatorConvertible {
    func validated(_ value: String)  -> (Bool,String) {
        guard value.trimmingCharacters(in: .whitespacesAndNewlines) != "" else {return (false,ValidationError("Please enter password").message)}
        guard value.trimmingCharacters(in: .whitespacesAndNewlines).count >= 8 else { return (false,ValidationError("Password must have at least 8 characters").message) }
        
//        do {
//            if try NSRegularExpression(pattern: "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$",  options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
//                return (false,ValidationError("Password must be more than 6 characters, with at least one character and one numeric character").message)
//            }
//        } catch {
//            return (false,ValidationError("Password must be more than 6 characters, with at least one character and one numeric character").message)
//        }
        return (true, "")
    }
}

struct EmailValidator: ValidatorConvertible {
    func validated(_ value: String)  -> (Bool,String) {
        if value.trimmingCharacters(in: .whitespacesAndNewlines).count == 0 {
            return (false, "Please enter email")
        } else {
            do {
                if try NSRegularExpression(pattern: "^[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$", options: .caseInsensitive).firstMatch(in: value, options: [], range: NSRange(location: 0, length: value.count)) == nil {
                    return (false,ValidationError("Please enter a valid email").message)
                }
            } catch {
                return (false,ValidationError("Please enter a valid email").message)
            }
            return (true, "")
        }
        
    }
}


extension String {
    func firstCharacterUpperCase() -> String? {
        guard !isEmpty else { return nil }
        let lowerCasedString = self.lowercased()
        return lowerCasedString.replacingCharacters(in: lowerCasedString.startIndex...lowerCasedString.startIndex, with: String(lowerCasedString[lowerCasedString.startIndex]).uppercased())
    }
}
extension String {

    var containsEmoji: Bool {
        for scalar in unicodeScalars {
            switch scalar.value {
            case 0x1F600...0x1F64F, // Emoticons
                 0x1F300...0x1F5FF, // Misc Symbols and Pictographs
                 0x1F680...0x1F6FF, // Transport and Map
                 0x2600...0x26FF,   // Misc symbols
                 0x2700...0x27BF,   // Dingbats
                 0xFE00...0xFE0F:   // Variation Selectors
                return true
            default:
                continue
            }
        }
        return false
    }

}
