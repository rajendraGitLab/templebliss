//
//  AppDelegate+AudioCall.swift
//  TempleBliss
//
//  Created by Apple on 24/06/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
import AVFoundation
import PushKit
import CallKit
import TwilioVoice
import UIKit


// MARK: - TVOCallDelegate

extension AppDelegate: CallDelegate {
    func callIsReconnecting(call: Call, error: Error) {
        
        print("DEBUG11 :: \(#function)")
        
        
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print("ATDebug New :: error \(error)")
    }
    
    
    func callDidFailToConnect(call: Call, error: Error) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print("ATDebug New :: error \(error)")
        NSLog("SJDebug call:isReconnectingWithError:")
        // lblTimer.text = "Reconnecting"
        //placeCallButton.setTitle("Reconnecting", for: .normal)
        
        //                toggleUIState(isEnabled: false, showCallControl: false)
    }
    
    func callDidDisconnect(call: Call, error: Error?) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print(#function)
        if !self.userInitiatedDisconnect {
            var reason = CXCallEndedReason.remoteEnded
            
            if error != nil {
                reason = .failed
            }
            
            if let provider = self.callKitProvider {
                provider.reportCall(with: call.uuid ?? UUID(), endedAt: Date(), reason: reason)
                userInitiatedDisconnect = true
                provider.invalidate()
                AdvisorActiveCall = nil
                activeCalls = [:]
                activeCallInvites = [:]
                callKitCompletionCallback = nil
            }
        }
        
        callDisconnected(call: call)
        
    }
    
    func callDidStartRinging(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        NSLog("SJDebug callDidStartRinging:")
        
        //        lblTimer.text = "Ringing"
        //        placeCallButton.setTitle("Ringing", for: .normal)
        
        /*
         When [answerOnBridge](https://www.twilio.com/docs/voice/twiml/dial#answeronbridge) is enabled in the
         <Dial> TwiML verb, the caller will not hear the ringback while the call is ringing and awaiting to be
         accepted on the callee's side. The application can use the `AVAudioPlayer` to play custom audio files
         between the `[TVOCallDelegate callDidStartRinging:]` and the `[TVOCallDelegate callDidConnect:]` callbacks.
         */
        if self.playCustomRingback {
            playRingback()
        }
    }
    
    func callDidConnect(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        NSLog("SJDebug callDidConnect:")
        
        
        
        
        if self.playCustomRingback {
            stopRingback()
        }
        
        if let callKitCompletionCallback = self.callKitCompletionCallback {
            callKitCompletionCallback(true)
        }
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AdvisorAudioCallViewController.self) {
                
            } else {
                let controller:AdvisorAudioCallViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorAudioCallViewController.storyboardID) as! AdvisorAudioCallViewController
                
                
                controller.hidesBottomBarWhenPushed = true
                controller.modalPresentationStyle = .overCurrentContext
                controller.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                topVC.navigationController?.pushViewController(controller, animated: true)
            }
        }
        //                placeCallButton.setTitle("Hang Up", for: .normal)
        //
        //                toggleUIState(isEnabled: true, showCallControl: true)
        //                stopSpin()
        AdvisorAudioCall.sharedInstance.toggleAudioRoute(toSpeaker: false)
    }
    
    //    func call(_ call: TVOCall, didFailToConnectWithError error: Error?) {
    
    //    }
    
    func callDidReconnect(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        NSLog("SJDebug callDidReconnect:")
        
        //        placeCallButton.setTitle("Hang Up", for: .normal)
        //
        //        toggleUIState(isEnabled: true, showCallControl: true)
    }
    
    //    func callDidFailToConnect(call: Call, error: Error) {
    //        print("ATDebug New :: \(#function)")
    //        NSLog("SJDebug Call failed to connect: \(error.localizedDescription)")
    //        DispatchQueue.main.async {
    //            self.dismissCallControllerScreen()
    //        }
    //        if let completion = self.callKitCompletionCallback {
    //            completion(false)
    //        }
    //
    //        //        if let provider = callKitProvider {
    //        //            provider.reportCall(with: call.uuid, endedAt: Date(), reason: CXCallEndedReason.failed)
    //        //        }
    //
    //        callDisconnected(call: call)
    //    }
    
    //    func callDidDisconnect(call: Call, error: Error?) {
    //        print("ATDebug New :: \(#function)")
    //        print(#function)
    //        if let error = error {
    //            NSLog("SJDebug Call failed: \(error.localizedDescription)")
    //        } else {
    //            NSLog("SJDebug Call disconnected")
    //        }
    //        // lblTimer.text = "Call ended"
    //        DispatchQueue.main.async {
    //            self.dismissCallControllerScreen()
    //        }
    //        if !self.userInitiatedDisconnect {
    //            var reason = CXCallEndedReason.remoteEnded
    //
    //            if error != nil {
    //                reason = .failed
    //            }
    //
    //            if let provider = self.callKitProvider {
    //                provider.reportCall(with: call.uuid ?? UUID(), endedAt: Date(), reason: reason)
    //            }
    //        }
    //
    //        callDisconnected(call: call)
    //    }
    
    func callDisconnected(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print("ATDebug New :: \(#function)")
        if call == AdvisorActiveCall {
            AdvisorActiveCall = nil
        }
        
        self.activeCalls.removeValue(forKey: call.uuid?.uuidString ?? UUID().uuidString)
        
        self.userInitiatedDisconnect = false
        
        if self.playCustomRingback {
            self.stopRingback()
        }
        
        //        stopSpin()
        //        toggleUIState(isEnabled: true, showCallControl: false)
        //        placeCallButton.setTitle("Call", for: .normal)
    }
    
    func call(call: Call, didReceiveQualityWarnings currentWarnings: Set<NSNumber>, previousWarnings: Set<NSNumber>) {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print(#function)
        /**
         * currentWarnings: existing quality warnings that have not been cleared yet
         * previousWarnings: last set of warnings prior to receiving this callback
         *
         * Example:
         *   - currentWarnings: { A, B }
         *   - previousWarnings: { B, C }
         *   - intersection: { B }
         *
         * Newly raised warnings = currentWarnings - intersection = { A }
         * Newly cleared warnings = previousWarnings - intersection = { C }
         */
        var warningsIntersection: Set<NSNumber> = currentWarnings
        warningsIntersection = warningsIntersection.intersection(previousWarnings)
        
        var newWarnings: Set<NSNumber> = currentWarnings
        newWarnings.subtract(warningsIntersection)
        if newWarnings.count > 0 {
            qualityWarningsUpdatePopup(newWarnings, isCleared: false)
        }
        
        var clearedWarnings: Set<NSNumber> = previousWarnings
        clearedWarnings.subtract(warningsIntersection)
        if clearedWarnings.count > 0 {
            qualityWarningsUpdatePopup(clearedWarnings, isCleared: true)
        }
    }
    
    func qualityWarningsUpdatePopup(_ warnings: Set<NSNumber>, isCleared: Bool) {
        print("DEBUG11 :: \(#function)")
        print(#function)
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        //        var popupMessage: String = "Warnings detected: "
        //        if isCleared {
        //            popupMessage = "Warnings cleared: "
        //        }
        //        //let mappedWarnings: [String] = warnings.map({number in warningString(.)})
        //        let mappedWarnings: [String] = warnings.map { number in warningString(Call.QualityWarning(rawValue: number.uintValue)!)}
        //        popupMessage += mappedWarnings.joined(separator: ", ")
        //
        //        qualityWarningsToaster.alpha = 0.0
        //        qualityWarningsToaster.text = popupMessage
        //        UIView.animate(withDuration: 1.0, animations: {
        //            self.qualityWarningsToaster.isHidden = false
        //            self.qualityWarningsToaster.alpha = 1.0
        //        }) { [weak self] finish in
        //            guard let strongSelf = self else { return }
        //            let deadlineTime = DispatchTime.now() + .seconds(5)
        //            DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
        //                UIView.animate(withDuration: 1.0, animations: {
        //                    strongSelf.qualityWarningsToaster.alpha = 0.0
        //                }) { (finished) in
        //                    strongSelf.qualityWarningsToaster.isHidden = true
        //                }
        //            })
        //        }
    }
    
    func warningString(_ warning: Call) -> String {
        print("DEBUG11 :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print(#function)
        //        switch warning {
        //        case .highRtt: return "high-rtt"
        //        case .highJitter: return "high-jitter"
        //        case .highPacketsLostFraction: return "high-packets-lost-fraction"
        //        case .lowMos: return "low-mos"
        //        case .constantAudioInputLevel: return "constant-audio-input-level"
        //        default: return "Unknown warning"
        //        }
        return ""
    }
    
    //AUTHOR :
    //MARK: Ringtone
    
    func playRingback() {
        print("Ankur's Debug :: \(#function)")
        let ringtonePath = URL(fileURLWithPath: Bundle.main.path(forResource: "\(RingtoneSoundName)", ofType: "mp3")!)
        DispatchQueue.main.async {
            do {
                self.ringtonePlayer = try AVAudioPlayer(contentsOf: ringtonePath)
                self.ringtonePlayer?.delegate = self
                self.ringtonePlayer?.numberOfLoops = -1
                
                self.ringtonePlayer?.volume = 1.0
                self.ringtonePlayer?.play()
                
                
            } catch {
                NSLog("Failed to initialize audio player")
            }
            
        }
    }
    
    func stopRingback() {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        guard let ringtonePlayer = self.ringtonePlayer, ringtonePlayer.isPlaying else { return }
        
        ringtonePlayer.stop()
    }
}
// MARK: - AVAudioPlayerDelegate

extension AppDelegate: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("Ankur's Debug :: \(#function)")
        if flag {
            NSLog("Audio player finished playing successfully");
        } else {
            NSLog("Audio player finished playing with some error");
        }
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Ankur's Debug :: \(#function)")
        if let error = error {
            NSLog("Decode error occurred: \(error.localizedDescription)")
        }
    }
}
extension AppDelegate {
    func mainButtonPressed() {
        guard activeCall == nil else {
            userInitiatedDisconnect = true
            performEndCallAction(uuid: activeCall!.uuid!)
            
            
            return
        }
        
        checkRecordPermission { [weak self] permissionGranted in
            let uuid = UUID()
            let handle = "Voice Bot"
            
            guard !permissionGranted else {
                self?.performStartCallAction(uuid: uuid, handle: handle)
                return
            }
            
            //self?.showMicrophoneAccessRequest(uuid, handle)
        }
    }
}
// MARK: - PushKitEventDelegate

extension AppDelegate: PushKitEventDelegate {
    func credentialsUpdated(credentials: PKPushCredentials) {
        print("Ankur's Debug :: \(#function)")
        print(credentials.token.map { String(format: "%02x", $0) }.joined())
        guard let profileId = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile?.id else { return }
        let Parameters = "\(profileId)/\(credentials.token.map { String(format: "%02x", $0) }.joined())"
        print("Ankur's Debug :: \(Parameters)")
        WebServiceSubClass.TrainingSection(strParams: Parameters, completion: { (response, status, error) in
            
            if status{
                
            }else{
                
            }
        })
        
        //        guard
        //            (registrationRequired() || UserDefaults.standard.data(forKey: kCachedDeviceToken) != credentials.token)
        //        else {
        //            return
        //        }
        
        let cachedDeviceToken = credentials.token
        pushTokenForVOIP = cachedDeviceToken
        /*
         * Perform registration if a new device token is detected.
         */
        print("Ravi Debug register tocken audio call : \(AccessTokenForVOIP)")
        print("Ravi Debug register push tocken audio call : \(pushTokenForVOIP)")
        guard AccessTokenForVOIP.count > 0, cachedDeviceToken.count > 0 else {
            return
        }
        TwilioVoiceSDK.register(accessToken: AccessTokenForVOIP, deviceToken: cachedDeviceToken) { error in
            if let error = error {
//                let alert = UIAlertController(title: "Error", message: "An error occurred while registering: \(error.localizedDescription)  : Appdelegate", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { alert in
//                    print("")
//                }))
//                if let topController = UIApplication.topViewController() {
//                    topController.present(alert, animated: true)
//                }
                NSLog("An error occurred while registering: \(error.localizedDescription)")
            } else {
                NSLog("Successfully registered for VoIP push notifications.")
                // Save the device token after successfully registered.
                UserDefaults.standard.set(cachedDeviceToken, forKey: self.kCachedDeviceToken)
                
                /**
                 * The TTL of a registration is 1 year. The TTL for registration for this device/identity
                 * pair is reset to 1 year whenever a new registration occurs or a push notification is
                 * sent to this device/identity pair.
                 */
                UserDefaults.standard.set(Date(), forKey: self.kCachedBindingDate)
            }
        }
    }
    
    
    
    /**
     * The TTL of a registration is 1 year. The TTL for registration for this device/identity pair is reset to
     * 1 year whenever a new registration occurs or a push notification is sent to this device/identity pair.
     * This method checks if binding exists in UserDefaults, and if half of TTL has been passed then the method
     * will return true, else false.
     */
    func registrationRequired() -> Bool {
        print("Ankur's Debug :: \(#function)")
        guard
            let lastBindingCreated = UserDefaults.standard.object(forKey: kCachedBindingDate)
        else { return true }
        
        let date = Date()
        var components = DateComponents()
        components.setValue(kRegistrationTTLInDays/2, for: .day)
        let expirationDate = Calendar.current.date(byAdding: components, to: lastBindingCreated as! Date)!
        
        if expirationDate.compare(date) == ComparisonResult.orderedDescending {
            return false
        }
        return true;
    }
    
    func credentialsInvalidated() {
        
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                print("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                print("Successfully unregistered from VoIP push notifications.")
            }
        })
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
    
    func incomingPushReceived(payload: PKPushPayload) {
        print("Ankur's Debug :: \(#function)")
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
    }
    
    func incomingPushReceived(payload: PKPushPayload, completion: @escaping () -> Void) {
        print("Ankur's Debug :: \(#function)")
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
        
        if DeviceVersion < 13.0 {
            // Save for later when the notification is properly handled.
            incomingPushCompletionCallback = completion
        }
    }
    
    func incomingPushHandled() {
        print("Ankur's Debug :: \(#function)")
        guard let completion = incomingPushCompletionCallback else { return }
        
        incomingPushCompletionCallback = nil
        completion()
    }
}


// MARK: - TVONotificaitonDelegate

extension AppDelegate: NotificationDelegate {
    
    func callInviteReceived(callInvite: CallInvite) {
        StartTimeOfIncomingCall = Date()
        
        
        print("raviDebug : \(#function)")
        print("Ankur's Debug :: \(#function)")
        NSLog("callInviteReceived:")
        
        /**
         * The TTL of a registration is 1 year. The TTL for registration for this device/identity
         * pair is reset to 1 year whenever a new registration occurs or a push notification is
         * sent to this device/identity pair.
         */
        
        
        print("Custom Parameters are :: \(callInvite.customParameters ?? [:])")
        print("XXXXX: ", callInvite)
        
        UserDefaults.standard.set(Date(), forKey: kCachedBindingDate)
        
        let callerInfo: TVOCallerInfo = callInvite.callerInfo
        if let verified: NSNumber = callerInfo.verified {
            if verified.boolValue {
                NSLog("Call invite received from verified caller number!")
            }
        }
        
        let from = ((callInvite.from ?? "Voice Bot").replacingOccurrences(of: "client:", with: "").components(separatedBy: .decimalDigits) as NSArray).componentsJoined(by: "").replacingOccurrences(of: "_", with: " ")
        
        // Always report to CallKit
        reportIncomingCall(from: from, uuid: callInvite.uuid, totlaMinutes: "1")
        activeCallInvites[callInvite.uuid.uuidString] = callInvite
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "CallReject"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CallReject), name: NSNotification.Name(rawValue: "CallReject"), object: nil)
    }
    @objc func CallReject(){
        print("Ankur's Debug :: \(#function)")
        print("Ankur's Debug :: \(#function)")
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AdvisorAudioCallViewController.self) {
                let vc = topVC as! AdvisorAudioCallViewController
                
            }
        }
        //self.makeAvailableToggle()
        CutIncomingCall()
        
    }
    func cancelledCallInviteReceived(cancelledCallInvite: CancelledCallInvite, error: Error) {
        print("raviDebug : \(#function)")
        EndTimeOfIncomingCall = Date()
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.second]
        
        let difference = formatter.string(from: StartTimeOfIncomingCall, to: EndTimeOfIncomingCall)
        
        print("The difference between start time and end time is :: \(Int(difference ?? "0") ?? 0)")//output "8 seconds"
        
        if Int(difference ?? "0") ?? 0 > 28 {
            EmitForOffAvailability(param: ["advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0])
        }
        print("Ankur's Debug :: \(#function)")
        NSLog("cancelledCallInviteCanceled:error:, error: \(error.localizedDescription)")
        
        guard let activeCallInvites = activeCallInvites, !activeCallInvites.isEmpty else {
            NSLog("No pending call invite")
            return
        }
        
        
        let callInvite = activeCallInvites.values.first { invite in invite.callSid == cancelledCallInvite.callSid }
        
        if let callInvite = callInvite {
            performEndCallAction(uuid: callInvite.uuid)
            self.activeCallInvites.removeValue(forKey: callInvite.uuid.uuidString)
        }
    }
    func EmitForOffAvailability(param: [String : Any]) {
        print("raviDebug : \(#function)")
        print(#function)
        SingletonClass.sharedInstance.adviserAudioChatOn = false
        SingletonClass.sharedInstance.adviserTextChatOn = false
        SingletonClass.sharedInstance.adviserVideoChatOn = false
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                    for controller in topVC as Array {
                        if controller.isKind(of: AdviserHomeViewController.self) {
                            let HomeVC = controller as! AdviserHomeViewController
                            HomeVC.setTopAvability()
                            break
                        }
                    }
                }
                if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                    
                    if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                        for controller in topVC as Array {
                            if controller.isKind(of: AdviserMyAccountViewController.self) {
                                let MyAccount = controller as! AdviserMyAccountViewController
                                MyAccount.setTopAvability()
                                break
                            }
                        }
                    }
                }
            }
        })
        SocketIOManager.shared.socketEmit(for: socketApiKeys.advisor_availability.rawValue, with: param)
    }
    //    func MakeAModelForToggle() {
    //        var ToggleModel : [AvailabilityToggleModel] = []
    //
    //
    //        ToggleModel.append(AvailabilityToggleModel(type: "audio", status: "0"))
    //
    //        ToggleModel.append(AvailabilityToggleModel(type: "video", status: "0"))
    //
    //        ToggleModel.append(AvailabilityToggleModel(type: "chat", status: "0"))
    //
    //        SingletonClass.sharedInstance.adviserAudioChatOn = false
    //        SingletonClass.sharedInstance.adviserTextChatOn = false
    //        SingletonClass.sharedInstance.adviserVideoChatOn = false
    //
    //        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
    //            if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
    //                if let topVC = (NavigationController.children.first?.children[0] as? UINavigationController)?.viewControllers {
    //                    for controller in topVC as Array {
    //                        if controller.isKind(of: AdviserHomeViewController.self) {
    //                            let HomeVC = controller as! AdviserHomeViewController
    //                            HomeVC.setTopAvability()
    //                            break
    //                        }
    //                    }
    //                }
    //                if let topVC = (NavigationController.children.first?.children[3] as? UINavigationController)?.viewControllers {
    //                    for controller in topVC as Array {
    //                       if controller.isKind(of: AdviserMyAccountViewController.self) {
    //                            let MyAccount = controller as! AdviserMyAccountViewController
    //                            MyAccount.setTopAvability()
    //                        break
    //                        }
    //                    }
    //                }
    //            }
    //        })
    //
    //        let productsDict = AvailabilityToggleModel.convertDataCartArrayToProductsDictionary(arrayDataCart: ToggleModel);
    //
    //        let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
    //        let jsonString:String = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
    //
    //
    //        let reqModel = changeAvailibalityStatusReqModelWithAllToggle()
    //        reqModel.user_id = SingletonClass.sharedInstance.UserId
    //        reqModel.status = jsonString
    //
    //        webServiceCallForToggleWithAllToggle(Model: reqModel)
    //    }
    //    func webServiceCallForToggleWithAllToggle(Model:changeAvailibalityStatusReqModelWithAllToggle) {
    //
    //        WebServiceSubClass.webServiceCallForToggleWithAllToggle(CategoryModel: Model, completion: { (response, status, error) in
    //
    //            if status{
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
    //                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
    //                        if let topVC = (NavigationController.children.first?.children[0] as? UINavigationController)?.viewControllers {
    //                            for controller in topVC as Array {
    //                                if controller.isKind(of: AdviserHomeViewController.self) {
    //                                    let HomeVC = controller as! AdviserHomeViewController
    //                                    HomeVC.setTopAvability()
    //                                    break
    //                                }
    //                            }
    //                        }
    //                        if let topVC = (NavigationController.children.first?.children[3] as? UINavigationController)?.viewControllers {
    //                            for controller in topVC as Array {
    //                               if controller.isKind(of: AdviserMyAccountViewController.self) {
    //                                    let MyAccount = controller as! AdviserMyAccountViewController
    //                                    MyAccount.setTopAvability()
    //                                break
    //                                }
    //                            }
    //                        }
    //                    }
    //                })
    //
    //            }else{
    //
    //            }
    //        })
    //    }
}

//MARK: - Socket call
extension AppDelegate {
    func callKitProviderCreate() {
        
        print("ATDebug New :: \(#function)")
        let configuration = CXProviderConfiguration(localizedName: "Voice Quickstart")
        configuration.ringtoneSound = "\(RingtoneSoundName).mp3"
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.includesCallsInRecents = false
        configuration.supportsVideo = false
        if let callKitIcon = UIImage(named: CallKitIconName) {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }
        self.callKitProvider = CXProvider(configuration: configuration)
        if let provider = self.callKitProvider {
            
            provider.setDelegate(self, queue: nil)
        }
        print("ATDebug for advisor call :: \(#function)")
        
    }
    func CutIncomingCall() {
        print("Sandeep activeCallInvites UUID: ", self.activeCallInvites.values.first?.uuid ?? "")
        print("Sandeep activeCalls UUID: ", self.activeCalls.values.first?.uuid ?? "")
        
        let uuid = self.activeCallInvites.values.first?.uuid ?? self.activeCalls.values.first?.uuid
        if let callUUID = uuid,  let provider = self.callKitProvider  {
            provider.reportCall(with: callUUID, endedAt: Date(), reason: .answeredElsewhere)
            self.callKitProvider?.invalidate()
            AdvisorActiveCall = nil
            self.activeCalls = [:]
            self.activeCallInvites = [:]
            self.callKitCompletionCallback = nil
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "CallReject"), object: nil)
    }
    //MARK: - Microphone permission
    func showMicrophoneAccessRequest(_ uuid: UUID, _ handle: String) {
        print("ATDebug New :: \(#function)")
        let alertController = UIAlertController(title: "Voice Quick Start",
                                                message: "Microphone permission not granted",
                                                preferredStyle: .alert)
        
        let continueWithoutMic = UIAlertAction(title: "Continue without microphone", style: .default) { [weak self] _ in
            self?.performStartCallAction(uuid: uuid, handle: handle)
        }
        
        let goToSettings = UIAlertAction(title: "Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                      options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly: false],
                                      completionHandler: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
        }
        
        [continueWithoutMic, goToSettings, cancel].forEach { alertController.addAction($0) }
        
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
    }
    func checkRecordPermission(completion: @escaping (_ permissionGranted: Bool) -> Void) {
        print("ATDebug New :: \(#function)")
        let permissionStatus = AVAudioSession.sharedInstance().recordPermission
        
        switch permissionStatus {
        case .granted:
            // Record permission already granted.
            completion(true)
        case .denied:
            // Record permission denied.
            completion(false)
        case .undetermined:
            // Requesting record permission.
            // Optional: pop up app dialog to let the users know if they want to request.
            AVAudioSession.sharedInstance().requestRecordPermission { granted in completion(granted) }
        default:
            completion(false)
        }
    }
    //MARK: - call function
    func CallCut() {
        print("ATDebug New :: \(#function)")
        guard AdvisorActiveCall == nil else {
            self.userInitiatedDisconnect = true
            performEndCallAction(uuid: AdvisorActiveCall!.uuid ?? UUID())
            
            return
        }
        
    }
    //MARK: - other methods
    func GoBack() {
        //        if let provider = self.callKitProvider {
        //            provider.invalidate()
        //        }
        guard AdvisorActiveCall == nil else {
            
            userInitiatedDisconnect = true
            callKitProvider?.invalidate()
            performEndCallAction(uuid: AdvisorActiveCall!.uuid ?? UUID())
            AdvisorActiveCall = nil
            activeCalls = [:]
            activeCallInvites = [:]
            callKitCompletionCallback = nil
            return
        }
    }
    func dismissCallControllerScreen() {
        print("ATDebug New :: \(#function)")
        
        self.callKitProvider?.invalidate()
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AdvisorAudioCallViewController.self) {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: nil)
                } else {
                    topVC.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        print("ATDebug New :: \(#function)")
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    func fetchAccessToken(myIdentity:String) -> String? {
        print("ATDebug New :: \(#function)")
        guard let accessTokenURL = URL(string: "\(SOCKET_URL)/token/\(myIdentity)") else { return nil }
        
        let jsonString = try? String(contentsOf: accessTokenURL, encoding: .utf8)
        
        let jsonDictonary = convertToDictionary(text: jsonString ?? "")
        let identityFromJson:String = jsonDictonary?["identity"] as? String ?? ""
        let accessTokenMain:String = jsonDictonary?["token"] as? String ?? ""
        
        print("identityFromJson from json is    :: \(identityFromJson)")
        print("accessToken from json is :: \(accessTokenMain)")
        AccessTokenForVOIP = accessTokenMain
        return accessTokenMain
        
    }
    
    func SocketEmitForCutVideoCall(reject: Int?,user_id:Int,user_name:String,advisor_id:Int,booking_id:Int) {
        print("ATDebug New :: \(#function)")
        is_rejected = reject
        var param = [String: Any]()
        
        param = ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "before_receive"]
        
        if is_rejected != nil {
            
            if isPickedUp != nil {
                
                param = ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "after_receive"]
                
            } else {
                param =  ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "before_receive"]
                
            }
        }
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
    }
    func SocketEmitForCutCall(reject: Int?) {
        print("ATDebug New :: \(#function)")
        is_rejected = reject
        var param = [String: Any]()
        
        param = ["user_id":SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"booking_id": SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0, "call_status" : "before_receive"]
        
        if is_rejected != nil {
            
            if isPickedUp != nil {
                
                param = ["user_id":SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"booking_id": SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0, "call_status" : "after_receive"]
                
            } else {
                param =  ["user_id":SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"booking_id": SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0, "call_status" : "before_receive"]
                
            }
        }
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
    }
    
    
    func UnregisterVOIP() {
        print("ATDebug New :: \(#function)")
        print("Ravi Debug unregister tocken : \(AccessTokenForVOIP)")
        print("Ravi Debug unregister push tocken : \(pushTokenForVOIP)")
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                NSLog("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                NSLog("Successfully unregistered from VoIP push notifications.")
            }
        })
        // disconnect all incoming call
        for (k, _) in self.activeCallInvites {
            if let invitee = self.activeCallInvites[k] {
                self.callKitProvider?.reportCall(with: invitee.uuid, endedAt: Date(), reason: .answeredElsewhere)
            }
        }
        
        // disconnect all active calls
        for (k, _) in self.activeCalls {
            if let invitee = self.activeCalls[k] {
                self.callKitProvider?.reportCall(with: invitee.uuid ?? UUID(), endedAt: Date(), reason: .answeredElsewhere)
            }
        }
        
        // disconnect all active call
        if let ac = self.activeCall {
            self.callKitProvider?.reportCall(with: ac.uuid ?? UUID(), endedAt: Date(), reason: .answeredElsewhere)
        }
        self.activeCall = nil
        self.activeCallInvites.removeAll()
        self.activeCalls.removeAll()
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
}
