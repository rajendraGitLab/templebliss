//
//  AppDelegate+CXProviderDelegate.swift
//  TempleBliss
//
//  Created by Apple on 30/06/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
import CallKit
import AVFoundation
import TwilioVoice
import PushKit
import TwilioVideo
import SwiftyJSON
import os.log
import UIKit

extension AppDelegate: CXProviderDelegate {
   
    func logMessage(messageText: String) {
        print(messageText)
        os_log("foo: %@", log: .default, type: .debug, messageText)
        
        if #available(iOS 14.0, *) {
            let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "Komal")
            logger.log("\(messageText)")

        } else {
            // Fallback on earlier versions
        }
    }
    
    func providerDidReset(_ provider: CXProvider) {
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  CallproviderDidReset:")
            
            // AudioDevice is enabled by default
            AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.isEnabled = true
            
            self.room?.disconnect()
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug providerDidReset:")
                print("ATDebug New :: \(#function)")
                AdvisorAudioCall.sharedInstance.audioDevice.isEnabled = false
            }
          
        }
      
    }
    
    func providerDidBegin(_ provider: CXProvider) {
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  CallproviderDidBegin")
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug providerDidBegin")
                print("ATDebug New :: \(#function)")
            }
           
        }
       
    }
    
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:didActivateAudioSession:")
            
            AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.isEnabled = true
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:didActivateAudioSession:")
                print("ATDebug New :: \(#function)")
                AdvisorAudioCall.sharedInstance.audioDevice.isEnabled = true
            }
           
        }
       
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:didDeactivateAudioSession:")
        } else {
            if RequestFromChat {
                if let provider = self.callKitProvider {
                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                    self.callKitProvider?.invalidate()
                }
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:didDeactivateAudioSession:")
                print("ATDebug New :: \(#function)")
                AdvisorAudioCall.sharedInstance.audioDevice.isEnabled = false
                DispatchQueue.main.async {
                    self.dismissCallControllerScreen()
                }
                self.callKitProvider?.invalidate()
            }
            
        }
        

    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:timedOutPerformingAction:")
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:timedOutPerformingAction:")
                print("ATDebug New :: \(#function)")
            }
           
        }
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
       
        if provider.configuration.supportsVideo == true {
            logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:performStartCallAction:")
            
            AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.isEnabled = false
            
            // Configure the AVAudioSession by executign the audio device's `block`.
            AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.block()
            
            self.callKitProvider?.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
            
            //        self.reportIncomingCall(uuid: action.callUUID, roomName: strRoomName)
            
           
            
            /*
             * Configure the audio session, but do not start call audio here, since it must be done once
             * the audio session has been activated by the system after having its priority elevated.
             */
            
            // Stop the audio unit by setting isEnabled to `false`.
            
            
            
            
           // AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.isEnabled = false
            
            // Configure the AVAudioSession by executign the audio device's `block`.
          //  AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.block()
            
          //  self.callKitProvider?.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
            
            //        self.reportIncomingCall(uuid: action.callUUID, roomName: strRoomName)
            if let TOPVC = UIApplication.topViewController() {
                if TOPVC.isKind(of: CallControllerViewController.self) {
                    let vc = TOPVC as! CallControllerViewController
                    vc.performRoomConnect(uuid: action.callUUID, roomName: action.handle.value) { (success) in
                        if (success) {
                            provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                            //                self.reportIncomingCall(uuid: action.callUUID, roomName: self.strRoomName)

                            action.fulfill()
                        } else {
                            action.fail()
                        }
                    }
                    //vc.muteAudio(isMuted: action.isMuted)
                }
            }
            
            
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:performStartCallAction:")
                print("ATDebug New :: \(#function)")
                
                //        toggleUIState(isEnabled: false, showCallControl: false)
                //        startSpin()
                
                provider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: Date())
                self.performVoiceCall(uuid: action.callUUID, client: "") { success in
                    if success {
                        print("performVoiceCall() successful")
                        provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                    } else {
                        print("performVoiceCall() failed")
                    }
                }
                
                action.fulfill()
            }
            
        }
       
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        let state = UIApplication.shared.applicationState
        logMessage(messageText: "Voice Chat: call answered")
        DispatchQueue.main.asyncAfter(deadline: .now() + (3.0), execute: { [self] in
             if WebService.shared.isConnected {
                 logMessage(messageText: "Voice Chat: WebService isConnected")
                 if provider.configuration.supportsVideo == true {
                    if #available(iOS 14.0, *) {
                        let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "network")
                        logger.log("ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:performAnswerCallAction:")

                    } else {
                        // Fallback on earlier versions
                    }
                    logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  Callprovider:performAnswerCallAction:")
                     let dataForShare:[String:Any] = ["message": "Request accepted" ,
                                                      "user_id": Int(DataForVideoCall["user_id"] ?? "") ?? 0,
                                                      "room":DataForVideoCall["room"] ?? "",
                                                      "booking_id":Int(DataForVideoCall["booking_id"] ?? "") ?? 0,
                                                      "token":DataForVideoCall["token"] ?? "",
                                                      "name":DataForVideoCall["name"] ?? "",
                                                      "receiver_id":Int(DataForVideoCall["receiver_id"] ?? "") ?? 0]
                    print("YYYY: ", dataForShare)
                    self.navigateToCallController(withData: dataForShare)
                    
                    WebServiceForGetNotes(UserID: DataForVideoCall["user_id"] ?? "", bookingID: DataForVideoCall["booking_id"] ?? "")
                    
                    
                    // Sandeep Suthar (ToDo check)
//                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//                        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.isEnabled = false
//
//                        // Configure the AVAudioSession by executign the audio device's `block`.
//                        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall.block()
//
//                        if let TOPVC = UIApplication.topViewController() {
//                            if TOPVC.isKind(of: CallControllerViewController.self) {
//                                let vc = TOPVC as! CallControllerViewController
//                                vc.performRoomConnect(uuid: action.callUUID, roomName: self.strRoomName) { (success) in
//                                    if (success) {
//                                        print("NEW ATDEBUG ::")
//                                        action.fulfill(withDateConnected: Date())
//                                    } else {
//                                        action.fail()
//                                    }
//                                }
//                            }
//                        }
//                    })
                    
                } else {
                    if RequestFromChat {
                        if state != .active {
                            logMessage(messageText: "Voice Chat: show handle UI at answer")
                            self.HandleBookingRequest(response: JSON(rawValue: self.DataForChat) ?? JSON(), isActive: false)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AcceptChat"), object: nil, userInfo: nil)
                        } else {
                            logMessage(messageText: "Voice Chat: accpet request at answer")
                            DispatchQueue.main.async {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AcceptChat"), object: nil, userInfo: nil)
                            }
                        }
                        
//                        let AcceptBookingRequest:[String:Any] = ["user_id": DataForChat["user_id"] ?? 0 , "booking_id":DataForChat["booking_id"] ?? 0, "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": DataForChat["type"] ?? ""]
//                        SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                        currentRequestArrived = false
                        
                        
                        //                if let topController = UIApplication.topViewController() {
                        //                    let controller:AdvisorMessageChatViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorMessageChatViewController.storyboardID) as! AdvisorMessageChatViewController
                        //                    SingletonClass.sharedInstance.isNoteAdded = false
                        //
                        //
                        //                    controller.sessionType = "session"
                        //                    controller.RequestAcceptDataOfUser = DataForChat
                        //                    topController.navigationController?.pushViewController(controller, animated: true)
                        //
                        //                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StartTimerForChat"), object: nil, userInfo: nil)
                        //
                        //                }
                        print("Push to message screen")
                        
                    } else {
                        logMessage(messageText: "Voice Chat: Voice Call Block")
                        print("Ankur's Debug :: \(#function)")
                        print("SJDebug provider:performAnswerCallAction:")
                        print("ATDebug New :: \(#function)")
                        print("RaviCheck uuid: \(action.callUUID)")
                        self.performAnswerVoiceCall(uuid: action.callUUID)
                        { (success) in
                            if (success)
                            {
                                action.fulfill()
                                
                            }
                            else
                            {
                                action.fail()
                            }
                        }
                        // Komal Change
//                        action.fulfill()
                        
                        if SingletonClass.sharedInstance.AdvisorAudioCallData != nil {
                            let controller:AdvisorAudioCallViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorAudioCallViewController.storyboardID) as! AdvisorAudioCallViewController
                            
                            controller.hidesBottomBarWhenPushed = true
                            
                            controller.CustomerName = SingletonClass.sharedInstance.AdvisorAudioCallData?["customer_name"].string ?? ""
                            controller.CustomerUserImageURl = SingletonClass.sharedInstance.AdvisorAudioCallData?["customer_profile_picture"].string ?? ""
                            
                            controller.bookingID = "\(SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0)"
                            controller.AdvisorID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
                            controller.customerID = "\(SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0)"
                            self.isPickedUp = 1
                            if let _ = UIApplication.topViewController()?.navigationController {
                                logMessage(messageText: "Voice Chat: Nav Found")
                            } else {
                                logMessage(messageText: "Voice Chat: Nav not Found")
                            }
                            UIApplication.topViewController()?.navigationController?.pushViewController(controller, animated: true)
                            
                            let param = ["user_id" : SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0,
                                         "advisor_id": Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
                                         "advisor_name" : SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? "",
                                         "booking_id" : SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0] as [String: Any]
                            
                            print("RaviCheck param: \(param)")
                            WebServiceForGetNotes(UserID: "\(SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0)", bookingID: "\(SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0)")
                            
                            SocketIOManager.shared.socketEmit(for: socketApiKeys.PickupCall.rawValue, with: param)
                            self.OnPickupCall()
                            //Timer start
                            if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                                if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                                    for controller in topVC as Array {
                                        if controller.isKind(of: AdviserHomeViewController.self) {
                                            let HomeVC = controller as! AdviserHomeViewController
                                            HomeVC.totalMinutesTimer = 1
                                            
                                            HomeVC.TotalTimecounter = 0
                                            
                                            HomeVC.startTimer(withInterval: 1.0)
                                            break
                                        }
                                    }
                                }
                            }
                        } else {
                            logMessage(messageText: "Voice Chat: GetBookingDetails")
                            self.GetBookingDetails()
                        }
                    }
                    
                }
            } else {
                logMessage(messageText: "Voice Chat: not connected at answer")

                if let provider = appDel.callKitProvider {

                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .remoteEnded)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        appDel.callKitProvider?.invalidate()
                    })

                }
            }
            
        })
        
    }
    func WebServiceForGetNotes(UserID:String,bookingID:String) {
        let userID = UserID
        let advisorID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        print("ATDebug :: \("\(advisorID)/\(userID)")")
        
        WebServiceSubClass.GetNotes(strParams: "\(advisorID)/\(userID)", completion: { (response, status, error) in
          
            if status {
                
                if response["notes"]["note"].stringValue != "" {
                    let AddNoteModel = SessionNoteReqModel()
                    AddNoteModel.booking_id = bookingID
                    AddNoteModel.note = response["notes"]["note"].stringValue
                    self.webserviceCallForAddNotes(reqModel: AddNoteModel)
                } else {
                    print("else case")
                }
               
            
                
            }else{
              
                Utilities.showAlertOfAPIResponse(param: error, vc: UIApplication.topViewController() ?? UIViewController())
            }
        })
    }
    
    func webserviceCallForAddNotes(reqModel:SessionNoteReqModel) {
       
        WebServiceSubClass.AddNotes(addCategory: reqModel, completion: {(json, status, response) in
          
            
            if status {
              
            } else {
               
             
            }
        })
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        if provider.configuration.supportsVideo == true {
           // room?.disconnect()
            print("User_id :: \(Int(DataForVideoCall["user_id"] ?? "") ?? 0)")
            print("user_name :: \(DataForVideoCall["user_name"] ?? "")")
            print("advisor_id :: \(Int(DataForVideoCall["advisor_id"] ?? "") ?? 0)")
            print("booking_id :: \(Int(DataForVideoCall["booking_id"] ?? "") ?? 0)")
            if !VideoCallScreenOpen {
                SocketEmitForCutVideoCall(reject: 1, user_id: Int(DataForVideoCall["user_id"] ?? "") ?? 0, user_name: DataForVideoCall["user_name"] ?? "", advisor_id: Int(DataForVideoCall["advisor_id"] ?? "") ?? 0, booking_id:  Int(DataForVideoCall["booking_id"] ?? "") ?? 0)
            }
            
           
            print("provider:performEndCallAction:")
//            if let provider = self.callKitProvider {
//                provider.invalidate()
//            }
            self.room?.disconnect()
            
            action.fulfill()
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RejectChat"), object: nil, userInfo: nil)
                    let RejectBookingRequest:[String:Any] = ["user_id": DataForChat["user_id"] ?? 0 , "booking_id":DataForChat["booking_id"] ?? 0, "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": DataForChat["type"] ?? ""]
                    SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                    currentRequestArrived = false
                self.callKitProvider?.invalidate()
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:performEndCallAction:")
                print("ATDebug New :: \(#function)")

                if let invite = self.activeCallInvites[action.callUUID.uuidString] {
    //                SocketEmitForCutVideoCall(reject: 1, user_id: Int(DataForVideoCall["user_id"] ?? "") ?? 0, user_name: DataForVideoCall["user_name"] ?? "", advisor_id: Int(DataForVideoCall["advisor_id"] ?? "") ?? 0, booking_id:  Int(DataForVideoCall["booking_id"] ?? "") ?? 0)
    //
    //                SocketEmitForCutCall(reject: 1)
                    invite.reject()
                    self.activeCallInvites.removeValue(forKey: action.callUUID.uuidString)
                    self.callKitProvider?.invalidate()
                } else if let call = self.activeCalls[action.callUUID.uuidString] {
                    call.disconnect()
                } else {
                    print("Unknown UUID to perform end-call action with")
                }
                
                action.fulfill()

                self.callKitProvider?.invalidate()
            }
           
        }
       
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        if provider.configuration.supportsVideo == true {
            print("provier:performSetHeldCallAction:")
            
            if(self.callKitCallController == nil)
            {
                self.callKitCallController = CXCallController()
            }
            let cxObserver = self.callKitCallController?.callObserver
            let calls = cxObserver?.calls
            if(self.callKitCallController == nil)
            {
                self.callKitCallController = CXCallController()
            }
            guard let call = calls?.first(where:{$0.uuid == action.callUUID}) else {
                action.fail()
                return
            }
            if let TOPVC = UIApplication.topViewController() {
                if TOPVC.isKind(of: CallControllerViewController.self) {
                    let vc = TOPVC as! CallControllerViewController
                    if call.isOnHold {
                        
                        vc.holdCall(onHold: false)
                    } else {
                        vc.holdCall(onHold: true)
                    }
                    //vc.muteAudio(isMuted: action.isMuted)
                }
            }
            
            action.fulfill()
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:performSetHeldAction:")
                print("ATDebug New :: \(#function)")
                if let call = self.activeCalls[action.callUUID.uuidString] {
                    call.isOnHold = action.isOnHold
                    action.fulfill()
                } else {
                    action.fail()
                }
            }
           
        }
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        if provider.configuration.supportsVideo == true {
            print("provier:performSetMutedCallAction:")
            if let TOPVC = UIApplication.topViewController() {
                if TOPVC.isKind(of: CallControllerViewController.self) {
                    let vc = TOPVC as! CallControllerViewController
                    vc.muteAudio(isMuted: action.isMuted)
                }
            }
            action.fulfill()
            print("ATDebug Video request")
        } else {
            if RequestFromChat {
                
            } else {
                print("Ankur's Debug :: \(#function)")
                print("SJDebug provider:performSetMutedAction:")
                print("ATDebug New :: \(#function)")
                if let call = self.activeCalls[action.callUUID.uuidString] {
                    call.isMuted = action.isMuted
                    action.fulfill()
                } else {
                    action.fail()
                }
            }
            
        }
        
    }
    
    // MARK: - Call Kit Actions
    func performStartCallAction(uuid: UUID, handle: String) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        guard let provider = self.callKitProvider else {
            print("SJDebug CallKit provider not available")
            return
        }
        
        //SJ_Change :
        let callHandle = CXHandle(type: .generic, value: handle)
        
        print("handle : \(handle)")
        
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        let transaction = CXTransaction(action: startCallAction)
        
        self.callKitCallController?.request(transaction) { error in
            if let error = error {
                print("SJDebug StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            
            print("SJDebug StartCallAction transaction request successful")
            
            let callUpdate = CXCallUpdate()
            
            callUpdate.remoteHandle = callHandle
            callUpdate.supportsDTMF = true
            callUpdate.supportsHolding = false
            callUpdate.supportsGrouping = false
            callUpdate.supportsUngrouping = false
            callUpdate.hasVideo = false
            
            provider.reportCall(with: uuid, updated: callUpdate)
        }
    }
    
    func reportIncomingCall(from: String, uuid: UUID,totlaMinutes : String) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        self.callKitProvider = nil
        if let provider = self.callKitProvider {
            print("provider already created. \(provider)")
        } else {
            print("ATDebug for advisor call :: \(#function)")
            self.callKitProviderCreate()
        }
        guard let provider = self.callKitProvider else {
            print("SJDebug CallKit provider not available")
            return
        }
        
        //\(totlaMinutes)min video: 
        let callHandle = CXHandle(type: .generic, value: "\(from)")
        let callUpdate = CXCallUpdate()
        
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = true
        callUpdate.supportsHolding = false
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false
        print("Sandeep Incoming Call UUID: ", uuid)
        provider.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if let error = error {
                print("SJDebug Failed to report incoming call successfully: \(error.localizedDescription).")
            } else {
                self.onSocketCallRejectBySender()
            
                print("SJDebug Incoming call successfully reported.")
            }
        }
    }
    
    func onSocketCallRejectBySender() {
        print("Ankur's Debug :: \(#function)")
        SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCallRejectedBySender.rawValue) { (json) in
            print(#function, "\n ", json)
            ///If call ended, then need to refresh status of call controller
            self.CutIncomingCall()
            
            self.CallCut()
          
            SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
        }
    }
    func OnSocketRejectByCustomerForVideoCall() {
            print("Ankur's Debug :: \(#function)")
            SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCallRejectedBySender.rawValue) { (json) in
                print(#function, "\n ", json)
                if let provider = self.callKitProvider {
                    provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                    self.callKitProvider?.invalidate()
                   
                   
                }
//                ///If call ended, then need to refresh status of call controller
//                self.CutIncomingCall()
//
//                self.CallCut()
//
//                self.makeAvailableToggle()
                SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
            }
        }
    func performEndCallAction(uuid: UUID) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        print(#function)
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        if(self.callKitCallController == nil)
        {
            self.callKitCallController = CXCallController()
        }
        print("Sandeep Phone disconnected UUID: ", self.activeCall?.uuid ?? " ")
        self.activeCall?.disconnect()
        self.AdvisorActiveCall?.disconnect()
        self.callKitCallController?.request(transaction) { error in
            if let error = error {
                print("SJDebug EndCallAction transaction request failed: \(error.localizedDescription).")
            } else {
                print("SJDebug EndCallAction transaction request successful")
                DispatchQueue.main.async {
                    self.dismissCallControllerScreen()
                }
            }
            self.SocketEmitForCutCall(reject: 1)
        }
    }
    
    func performVoiceCall(uuid: UUID, client: String?, completionHandler: @escaping (Bool) -> Void) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        
        
        let connectOptions = ConnectOptions(accessToken: AccessTokenForVOIP) { builder in
            builder.params = [self.twimlParamTo: self.CallToCustomerName]
            builder.uuid = uuid
            
        }
        
        let call = TwilioVoiceSDK.connect(options: connectOptions, delegate: self)
        AdvisorActiveCall = call
        self.activeCalls[call.uuid?.uuidString ?? UUID().uuidString] = call
        self.callKitCompletionCallback = completionHandler
    }
    
    func performAnswerVoiceCall(uuid: UUID, completionHandler: @escaping (Bool) -> Void) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        guard let callInvite = activeCallInvites[uuid.uuidString] else {
            print("SJDebug No CallInvite matches the UUID")
            return
        }
        
        let acceptOptions = AcceptOptions(callInvite: callInvite) { builder in
            builder.uuid = callInvite.uuid
        }
        
        let call = callInvite.accept(options: acceptOptions, delegate: self)
        AdvisorActiveCall = call
        self.activeCalls[call.uuid?.uuidString ?? UUID().uuidString] = call
        self.callKitCompletionCallback = completionHandler
        
        self.activeCallInvites.removeValue(forKey: uuid.uuidString)
        
        guard #available(iOS 13, *) else {
            self.incomingPushHandled()
            return
        }
    }
   
}

extension OSLog {
    private static var subsystem = Bundle.main.bundleIdentifier!

    /// Logs the view cycles like viewDidLoad.
    static let viewCycle = OSLog(subsystem: subsystem, category: "viewcycle")
}
