//
//  customRegisterBG.swift
//  HC Pro Doctor
//
//  Created by Apple on 04/11/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class customRegisterBG: UIView {
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
    }
    @IBInspectable var nibName:String?
    var contentView:UIView?
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
}
