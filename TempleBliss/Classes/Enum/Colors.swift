//
//  Colors.swift
//  CoreSound
//
//  Created by EWW083 on 04/02/20.
//  Copyright © 2020 EWW083. All rights reserved.
//


import Foundation
import  UIKit

import Foundation
import  UIKit

enum colors{
    case white,black,submitButtonTextColor,templeBlissTextColor,tabBarBGColor,logoutButtonColor,rattingTextColor,AdviserActive,AdviserInActive,AdviserBusy
    
    var value:UIColor{
        switch self {
        case .white:
            return UIColor.white
        case .black:
            return UIColor.black
        case .submitButtonTextColor:
            return UIColor(hexString:"#334D5C")
        case .templeBlissTextColor:
            return UIColor(hexString: "#E6C3AF")
        case .tabBarBGColor:
            return UIColor(hexString: "#243640")
        case .logoutButtonColor:
            return UIColor(hexString: "#FF4E4E")
        case .rattingTextColor:
            return UIColor(hexString: "#ECC24F")
        case .AdviserActive:
            return UIColor(hexString: "#3CC71B")
        case .AdviserInActive:
            return UIColor.white.withAlphaComponent(0.4)
        case .AdviserBusy:
            return UIColor(hexString: "#EC3838")
        }
    }
}
