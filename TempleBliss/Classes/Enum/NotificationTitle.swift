//
//  NotificationTitle.swift
//  TempleBliss
//
//  Created by Apple on 30/06/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
enum PushNotifications {
    
    case notify_me,chat,call_reject_by_advisor,call_reject_by_user,pick_up_call,logout,profile_under_approval,block_by_admin,profile_approved,BookingRequest,BookingRequest1, callRequest
    
    var Name:String {
        switch self {
        case .notify_me:
            return "notify_me"
        case .chat:
            return "chat"
        case .call_reject_by_advisor:
            return "call_reject_by_advisor"
        case .call_reject_by_user:
            return "call_reject_by_user"
        case .pick_up_call:
            return "pick_up_call"
        case .logout:
            return "logout"
        case .profile_under_approval:
            return "profile_under_approval"
        case .block_by_admin:
            return "block_by_admin"
        case .profile_approved:
            return "profile_approved"
        case .BookingRequest:
            return "booking_request"
        case .BookingRequest1:
            return "booking_request1"
        case .callRequest:
            return "call_request"
        }
    }
}
