//
//  NavigationItems.swift
//  HC Pro Patient
//
//  Created by Shraddha Parmar on 30/09/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import Foundation
import UIKit

enum NavItemsLeft {
    case none, back ,backFromPresnet,backForSkip
    
    var value:String {
        switch self {
        case .none:
            return ""
        case .back:
            return "back"
        case .backFromPresnet:
            return "backFromPresnet"
        case .backForSkip:
            return "backForSkip"
        }
    }
}


enum NavItemsRight {
    case none,skip , like , edit,viewNotes,AddNotes,timer,skipTour
    
    var value:String {
        switch self {
        case .none:
            return ""
        case .skip:
            return "skip"
        case .like:
            return "like"
        case .edit:
            return "edit"
        case .viewNotes:
            return "View notes"
        case .AddNotes:
            return "Add notes"
        case .timer:
            return "timer"
        case .skipTour:
            return "skipTour"
        }
    }
}
enum NavTitles {
    case none, Home,reasonForCancle
    
    var value:String {
        switch self {
        case .none:
            return ""
        case .Home:
            return ""
        case .reasonForCancle:
            return "NavigationTitle_reasonForCancle".Localized()
        }
    }
}
