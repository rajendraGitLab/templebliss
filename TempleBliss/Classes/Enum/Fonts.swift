//
//  Fonts.swift
//  CoreSound
//
//  Created by EWW083 on 04/02/20.
//  Copyright © 2020 EWW083. All rights reserved.
//

import Foundation
import UIKit
enum CustomFont{
    case bold,light,medium,regular,semibold,SurannaRegular
    func returnFont(_ font:CGFloat)->UIFont{
        switch self {
        case .bold:
            return UIFont(name: "Quicksand-Bold", size: font)!
        case .light:
            return UIFont(name: "Quicksand-Light", size: font)!
        case .medium:
            return UIFont(name: "Quicksand-Medium", size: font)!
        case .regular:
            return UIFont(name: "Quicksand-Regular", size: font)!
        case .semibold:
            return UIFont(name: "Quicksand-SemiBold", size: font)!
        case .SurannaRegular:
            return UIFont(name: "Suranna", size: font)!
        }
    }
}





