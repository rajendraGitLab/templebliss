//
//  CustomBGView.swift
//  HC Pro Patient
//
//  Created by EWW077 on 24/09/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import UIKit


class CustomBGView : UIView {
    
    // MARK: - Properties
    var contentView:UIView?
    @IBInspectable var nibName:String?

    
  
    
    // MARK: - IBOutlets
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        xibSetup()
      
    }
    
    func xibSetup() {
        guard let view = loadViewFromNib() else { return }
        view.frame = bounds
        view.autoresizingMask =
            [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        contentView = view
    }
    
    func loadViewFromNib() -> UIView? {
        guard let nibName = nibName else { return nil }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(
            withOwner: self,
            options: nil).first as? UIView
    }
}

