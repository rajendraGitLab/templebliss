//
//  UILabel+Extension.swift
//  CoreSound
//
//  Created by EWW083 on 04/02/20.
//  Copyright © 2020 EWW083. All rights reserved.
//

import Foundation
import UIKit
extension UILabel{
    //MARK:- color font
   func setTitleColorFont(color:colors,font:UIFont){
    self.textColor = color.value
          self.font = font
      }

    func addImageWithText(strText: String, img:UIImage, afterAttributedText: Bool, textfont: UIFont, textColor: UIColor) {

        let attachment = NSTextAttachment()
        attachment.image = img
        let attachmentString = NSAttributedString(attachment: attachment)

        guard let txt = self.text else {
            return
        }
        let range = (txt + strText as NSString).range(of: strText)
//CustomFont.regular.returnFont(15)
//        colors.NotifProfileLinkCol.value
        if afterAttributedText {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(NSMutableAttributedString(string: strText))
            strLabelText.append(attachmentString)
            strLabelText.addAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: textfont], range: range)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(attachmentString)
            strLabelText.append(NSMutableAttributedString(string: strText))
            strLabelText.addAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: textfont], range: range)
            self.attributedText = strLabelText
        }
    }

    func addImageBetweenTwoText(strText1: String, strText2:String, img:UIImage, textfont: UIFont, textColor: UIColor) {

            let attachment = NSTextAttachment()
            attachment.image = img
            let attachmentString = NSAttributedString(attachment: attachment)

            guard let txt = self.text else {
                return
            }
        let range1 = NSString(string: txt + strText1 + strText2).range(of: strText1)
        var range2 = NSString(string: txt + strText1 + strText2).range(of: strText2)
        range2.location = range2.location + 1
//            let range1 = (txt + strText1 as NSString).range(of: strText1)
    //CustomFont.regular.returnFont(15)
    //        colors.NotifProfileLinkCol.value
                let strLabelText = NSMutableAttributedString(string: txt)
                strLabelText.append(NSMutableAttributedString(string: strText1))
                strLabelText.append(attachmentString)
                strLabelText.append(NSMutableAttributedString(string: strText2))
                strLabelText.addAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: textfont], range: range1)
                strLabelText.addAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: textfont], range: range2)
                self.attributedText = strLabelText
            
        }
    
    func removeImage() {
        let text = self.text
        self.attributedText = nil
        self.text = text
    }
    
    func addText(strText: String, textfont: UIFont, textColor: UIColor) {

        guard let txt = self.text else {
            return
        }
        
        let range = (txt + strText as NSString).range(of: strText)

        let strLabelText = NSMutableAttributedString(string: txt)
            strLabelText.append(NSMutableAttributedString(string: strText))
            strLabelText.addAttributes([NSAttributedString.Key.foregroundColor: textColor, NSAttributedString.Key.font: textfont], range: range)
        self.attributedText = strLabelText
    }
}
extension UILabel {
    
    func addTrailing(with trailingText: String, moreText: String, moreTextFont: UIFont, moreTextColor: UIColor) {
        let readMoreText: String = trailingText + moreText
        
        let lengthForVisibleString: Int = self.vissibleTextLength
        let mutableString: String = self.text!
        let trimmedString: String? = (mutableString as NSString).replacingCharacters(in: NSRange(location: lengthForVisibleString, length: ((self.text?.count)! - lengthForVisibleString)), with: "")
        let readMoreLength: Int = (readMoreText.count)
        let trimmedForReadMore: String = (trimmedString! as NSString).replacingCharacters(in: NSRange(location: ((trimmedString?.count ?? 0) - readMoreLength), length: readMoreLength), with: "") + trailingText
        let answerAttributed = NSMutableAttributedString(string: trimmedForReadMore, attributes: [NSAttributedString.Key.font: self.font])
        let readMoreAttributed = NSMutableAttributedString(string: moreText, attributes: [NSAttributedString.Key.font: moreTextFont, NSAttributedString.Key.foregroundColor: moreTextColor])
        answerAttributed.append(readMoreAttributed)
        self.attributedText = answerAttributed
    }
    
    var vissibleTextLength: Int {
        let font: UIFont = self.font
        let mode: NSLineBreakMode = self.lineBreakMode
        let labelWidth: CGFloat = self.frame.size.width
        let labelHeight: CGFloat = self.frame.size.height
        let sizeConstraint = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        
        let attributes: [AnyHashable: Any] = [NSAttributedString.Key.font: font]
        let attributedText = NSAttributedString(string: self.text!, attributes: attributes as? [NSAttributedString.Key : Any])
        let boundingRect: CGRect = attributedText.boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, context: nil)
        
        if boundingRect.size.height > labelHeight {
            var index: Int = 0
            var prev: Int = 0
            let characterSet = CharacterSet.whitespacesAndNewlines
            repeat {
                prev = index
                if mode == NSLineBreakMode.byCharWrapping {
                    index += 1
                } else {
                    index = (self.text! as NSString).rangeOfCharacter(from: characterSet, options: [], range: NSRange(location: index + 1, length: self.text!.count - index - 1)).location
                }
            } while index != NSNotFound && index < self.text!.count && (self.text! as NSString).substring(to: index).boundingRect(with: sizeConstraint, options: .usesLineFragmentOrigin, attributes: attributes as? [NSAttributedString.Key : Any], context: nil).size.height <= labelHeight
            return prev
        }
        return self.text!.count
    }
}
