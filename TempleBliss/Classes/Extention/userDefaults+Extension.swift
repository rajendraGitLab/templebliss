//
//  userDefaults+Extension.swift
//  TempleBliss
//
//  Created by baps on 11/01/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
extension UserDefaults{
    func set<T: Codable>(object: T, forKey: String) throws {
        
        let jsonData = try JSONEncoder().encode(object)
        
        set(jsonData, forKey: forKey)
    }
    
    
    func get<T: Codable>(objectType: T.Type, forKey: String) throws -> T? {
        
        guard let result = value(forKey: forKey) as? Data else {
            return nil
        }
        
        return try JSONDecoder().decode(objectType, from: result)
    }
    
    func setUserData(objProfile: UserInfo) {
        try? userDefault.set(object: objProfile, forKey: UserDefaultsKey.userProfile.rawValue)
    }
    
    func getUserData() -> UserInfo? {
        let objResponse = try? userDefault.get(objectType: UserInfo.self, forKey:  UserDefaultsKey.userProfile.rawValue)
        return objResponse ?? nil
    }
    
    func setUserDataForCustomer(objProfile: userInforForCustomer) {
        try? userDefault.set(object: objProfile, forKey: UserDefaultsKey.userProfile.rawValue)
    }
    
    func getUserDataForCustomer() -> userInforForCustomer? {
        let objResponse = try? userDefault.get(objectType: userInforForCustomer.self, forKey:  UserDefaultsKey.userProfile.rawValue)
        return objResponse ?? nil
    }
    
   
}
