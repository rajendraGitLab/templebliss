//
//  CustomLabel.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import UIKit
import DWAnimatedLabel
class SplashScreenLabel : DWAnimatedLabel {
    override func awakeFromNib() {
      
      
        self.font = CustomFont.SurannaRegular.returnFont(63)
        self.textAlignment = .center
        self.textColor = colors.templeBlissTextColor.value
        self.animationType = .fade
        self.startAnimation(duration: 1.5, nil)
    }
}

class loginPageLabel : UILabel {
    @IBInspectable var islogintoDescription : Bool = false
    @IBInspectable var istitle : Bool = false
    @IBInspectable var isDontHaveAccount : Bool = false
    override func awakeFromNib() {
        self.textColor = colors.white.value
        self.textAlignment = .center
        if islogintoDescription {
            self.font = CustomFont.regular.returnFont(19)
        } else if istitle {
            self.font = CustomFont.bold.returnFont(35)
           
        } else if isDontHaveAccount {
            self.textAlignment = .left
            self.font = CustomFont.regular.returnFont(15)
        }
    }
}
class RegistrationPageLabel : UILabel {
    @IBInspectable var isRegistertoDescription : Bool = false
    @IBInspectable var istitle : Bool = false
    @IBInspectable var isByCLicking : Bool = false
    @IBInspectable var isAlreadyUser : Bool = false
    override func awakeFromNib() {
        self.textColor = colors.white.value
        self.textAlignment = .center
        if isRegistertoDescription {
            self.font = CustomFont.regular.returnFont(19)
        } else if istitle {
            self.font = CustomFont.bold.returnFont(35)
           
        } else if isByCLicking {
            self.textAlignment = .left
            self.font = CustomFont.regular.returnFont(13)
        } else if isAlreadyUser {
            self.textAlignment = .left
            self.font = CustomFont.regular.returnFont(15)
        }
    }
}
class HomePageLabel : UILabel {
    @IBInspectable var isCateory : Bool = false
    @IBInspectable var isCategories : Bool = false
    @IBInspectable var isCategoriesName : Bool = false
    @IBInspectable var isfreeMinute : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .left
        if isCateory {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isCategories {
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(18)
        } else if isfreeMinute {
            self.textAlignment = .right
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(18)
        } else if isCategoriesName {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(17)
            self.textAlignment = .center
        }
    }
    
}

class AdviserPageLabel : UILabel {
    @IBInspectable var isCateory : Bool = false
    @IBInspectable var isCategories : Bool = false
    @IBInspectable var isDescription : Bool = false
    @IBInspectable var isAdviserName : Bool = false
    @IBInspectable var iscategoryName : Bool = false
    @IBInspectable var isRatting : Bool = false
    @IBInspectable var isfreeMinute : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .left
        if isCateory {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isDescription {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isCategories {
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(18)
        } else if iscategoryName {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
         
        } else if isfreeMinute {
            self.textAlignment = .right
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
         
        }  else if isAdviserName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(17)
        } else if isRatting {
            self.textAlignment = .right
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(9)
        }
    }
    
}

class MySessionLabel : UILabel {
    @IBInspectable var isTotalMoney : Bool = false
    @IBInspectable var isAdviserName : Bool = false
    @IBInspectable var isTotalTime : Bool = false
    @IBInspectable var isDate : Bool = false
    @IBInspectable var isMySession : Bool = false
    @IBInspectable var isPaypalFee : Bool = false
    override func awakeFromNib() {
        if isAdviserName {
            self.textColor = colors.white.value.withAlphaComponent(1.0)
            self.font = CustomFont.regular.returnFont(17)
        } else if isTotalTime {
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(12)
        } else if isDate {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(15)
        } else if isPaypalFee {
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(15)
        } else if isMySession {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        } else if isTotalMoney {
            self.textColor = colors.white.value.withAlphaComponent(0.85)
            self.font = CustomFont.medium.returnFont(15)
        }
    }
   
}

class MyProfileLabel : UILabel {
    @IBInspectable var isUserName : Bool = false
    @IBInspectable var isUserEmail : Bool = false
    @IBInspectable var isUserContactNo : Bool = false
    @IBInspectable var isCategoryName : Bool = false
    override func awakeFromNib() {
        if isUserName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(21)
        } else if isUserEmail {
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(15)
        } else if isUserContactNo {
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(15)
        } else if isCategoryName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(19)
        }
    }
}
class MyFavouritePageLabel : UILabel {
    @IBInspectable var isCateory : Bool = false
    @IBInspectable var isCategories : Bool = false
    @IBInspectable var iscategoryName : Bool = false
    @IBInspectable var isRatting : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .left
        if isCateory {
            self.textColor = colors.white.value.withAlphaComponent(0.28)
            self.font = CustomFont.regular.returnFont(12)
        } else if isCategories {
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(18)
        } else if iscategoryName {
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(12)
         
        } else if isRatting {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(9)
        }
    }
    
}
class settingsLabel : UILabel {
    @IBInspectable var isCategoryName : Bool = false
    @IBInspectable var isVersionNumber : Bool = false
    override func awakeFromNib() {
       if isCategoryName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(19)
       } else if isVersionNumber {
        self.textAlignment = .center
        self.textColor = colors.white.value.withAlphaComponent(0.3)
        self.font = CustomFont.medium.returnFont(14)
        
       }
    }
    
}
class EnterOTPLabel : UILabel {
    @IBInspectable var isTitle : Bool = false
    @IBInspectable var isdescription : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .center
        if isTitle {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(35)
        } else if isdescription {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(19)
        }
    }
}
class ForgotPasswordLabel : UILabel {
    @IBInspectable var isTitle : Bool = false
    @IBInspectable var isdescription : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .center
        if isTitle {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(35)
        } else if isdescription {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(19)
        }
    }
}
class SetNewPasswordLabel : UILabel {
    @IBInspectable var isTitle : Bool = false
    @IBInspectable var isdescription : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .center
        if isTitle {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(35)
        } else if isdescription {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(19)
        }
    }
}
class adviserDetailsDescriptionLabel : ExpandableLabel {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.textColor = colors.white.value.withAlphaComponent(0.5)
        self.font = CustomFont.regular.returnFont(12)
    }
}

class adviserDetailsLabel : UILabel {
    @IBInspectable var isAdviserName : Bool = false
    @IBInspectable var isAdviserRatting : Bool = false
    @IBInspectable var isDescriptionTitle : Bool = false
    @IBInspectable var isDescription : Bool = false
    @IBInspectable var isCategoriesTitle : Bool = false
    @IBInspectable var isCategoryName : Bool = false
    @IBInspectable var isCommunicationType : Bool = false
    @IBInspectable var isCommunicationPrice : Bool = false
    @IBInspectable var isRateAndReviewTitle : Bool = false
    @IBInspectable var isRattingName : Bool = false
    @IBInspectable var isRattingDate : Bool = false
    @IBInspectable var isRattingDescription : Bool = false
    @IBInspectable var isRateAndReviewRatting : Bool = false
    @IBInspectable var isRateAndReviewCustomRatting : Bool = false
    
    override func awakeFromNib() {
        if isAdviserName {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(21)
        } else if isAdviserRatting {
            self.textAlignment = .left
            self.textColor = colors.rattingTextColor.value
            self.font = CustomFont.medium.returnFont(13)
        } else if isDescriptionTitle {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        } else if isDescription {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isCategoriesTitle {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        } else if isCategoryName {
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        }else if isCommunicationType {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(14)
        }else if isCommunicationPrice {
            self.numberOfLines = 0
            self.textAlignment = .right
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(14)
        }else if isRateAndReviewTitle {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        }else if isRattingName {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(17)
        }else if isRattingDate {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(13)
        }else if isRattingDescription {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(12)
        }else if isRateAndReviewRatting {
            self.textAlignment = .left
            self.textColor = colors.rattingTextColor.value
            self.font = CustomFont.medium.returnFont(13)
        }else if isRateAndReviewCustomRatting {
            self.textAlignment = .left
            self.textColor = colors.rattingTextColor.value
            self.font = CustomFont.regular.returnFont(11)
        }
    }
}
class MyCreditLabel : UILabel {
    @IBInspectable var isTitle : Bool = false
    @IBInspectable var isTotalMoney : Bool = false
    override func awakeFromNib() {
        if isTitle {
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        } else if isTotalMoney {
            self.textAlignment = .center
            self.textColor = colors.submitButtonTextColor.value
            self.font = CustomFont.bold.returnFont(31)
        }
    }
}
class buyMinutesLabel : UILabel {
    @IBInspectable var isTitle : Bool = false
    @IBInspectable var isMinutesCategory : Bool = false
    @IBInspectable var isMinutesPrice : Bool = false
    override func awakeFromNib() {
        self.numberOfLines = 0
        if isTitle {
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(30)
        } else if isMinutesCategory {
            self.textAlignment = .right
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(15)
        } else if isMinutesPrice {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.85)
            self.font = CustomFont.regular.returnFont(15)
        }
    }
}
class chatReminederLabel : UILabel {
    @IBInspectable var isMinutesLeft : Bool = false
    @IBInspectable var isYourchat : Bool = false
    override func awakeFromNib() {
        self.textAlignment = .center
        self.textColor = colors.white.value
        if isMinutesLeft {
            self.font = CustomFont.medium.returnFont(33)
        } else if isYourchat {
            self.font = CustomFont.regular.returnFont(16)
        }
    }
}
class conversationChatLabel : UILabel {
    @IBInspectable var isTime : Bool = false
    @IBInspectable var isChatMessage : Bool = false
    override func awakeFromNib() {
        if isTime {
          
            self.textColor = colors.white.value.withAlphaComponent(0.60)
            self.font = CustomFont.regular.returnFont(12)
        } else if isChatMessage {
        
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(15)
        }
    }
}
class AdviserRattingLabel : UILabel {
    @IBInspectable var isAdviserName : Bool = false
    @IBInspectable var isTotalMinutes : Bool = false
    @IBInspectable var isRateAdviserName: Bool = false
  
    override func awakeFromNib() {
        self.textAlignment = .center
        if isAdviserName {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(22)
        } else if isTotalMinutes {
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(15)
        } else if isRateAdviserName {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(17)
        }
    }
}
class deleteSessionLabel : UILabel {
    override func awakeFromNib() {
        self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(18)
    }
}
class commonLabelOnPopup : UILabel {
    override func awakeFromNib() {
        self.textAlignment = .center
        self.textColor = colors.white.value
        self.font = CustomFont.medium.returnFont(33)
    }
}
class AdviserMyProfileLabel : UILabel {
    @IBInspectable var isChatType : Bool = false
    @IBInspectable var isUserName : Bool = false
    @IBInspectable var isUserEmail : Bool = false
    @IBInspectable var isUserContactNo : Bool = false
    @IBInspectable var isCategoryName : Bool = false
    override func awakeFromNib() {
        if isUserName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(21)
        } else if isUserEmail {
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(15)
        } else if isUserContactNo {
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(15)
        } else if isCategoryName {
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(19)
        } else if isChatType {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(15)
        }
    }
}

class adviserRattingLabel : UILabel {
 
    @IBInspectable var isRattingName : Bool = false
    @IBInspectable var isRattingDate : Bool = false
    @IBInspectable var isRattingDescription : Bool = false
    @IBInspectable var isRateAndReviewCustomRatting : Bool = false
    
    override func awakeFromNib() {
      if isRattingName {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(18)
        }else if isRattingDate {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(14)
        }else if isRattingDescription {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.4)
            self.font = CustomFont.regular.returnFont(17)
        }else if isRateAndReviewCustomRatting {
            self.textAlignment = .left
            self.textColor = colors.rattingTextColor.value
            self.font = CustomFont.regular.returnFont(13)
        }
    }
}
class AdviserAddCategoryLabel : UILabel {
    @IBInspectable var isTtile : Bool = false
    @IBInspectable var isSubCategoryName : Bool = false
    override func awakeFromNib() {
        if isTtile {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isSubCategoryName {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(14)
        }
    }
}
class requestAcceptedLabel : UILabel {
    override func awakeFromNib() {
       
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(21)
        
    }
}
class commonPopupLabel : UILabel {
    override func awakeFromNib() {
       
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(21)
        
    }
}
class adviserHomeLabel : UILabel {
    @IBInspectable var isTraningSession : Bool = false
    @IBInspectable var isTtile : Bool = false
    @IBInspectable var isDescription : Bool = false
    @IBInspectable var isView : Bool = false
    @IBInspectable var isFacingIssue : Bool = false
    override func awakeFromNib() {
        if isTtile {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(15)
        } else if isDescription {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(13)
        } else if isView {
            self.textAlignment = .center
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(13)
        } else if isTraningSession {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(18)
        } else if isFacingIssue {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.3)
            self.font = CustomFont.regular.returnFont(13)
        }
    }
}
class AdvisorHomeScreenDescriptionLabel : ExpandableLabel {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.textColor = colors.white.value.withAlphaComponent(0.5)
        self.font = CustomFont.regular.returnFont(13)
    }
}
class LabelNotifyMe : ExpandableLabel {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.textColor = UIColor(hexString: "#c7211b")
        self.font = CustomFont.bold.returnFont(17)
    }
}

class AdviserMyEarningsLabel : UILabel {
    @IBInspectable var isTrasactionPending : Bool = false
    @IBInspectable var isUserName : Bool = false
    @IBInspectable var isDaysLeft : Bool = false
    @IBInspectable var isMinutes : Bool = false
    @IBInspectable var isTotalMoney : Bool = false
    override func awakeFromNib() {
        if isTrasactionPending {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(12)
        } else if isUserName {
            self.textAlignment = .left
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(17)
        } else if isDaysLeft {
            self.textAlignment = .right
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(12)
        } else if isMinutes {
            self.textAlignment = .left
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(12)
        } else if isTotalMoney {
            self.textAlignment = .right
            self.textColor = colors.white.value
            self.font = CustomFont.medium.returnFont(15)
        }
    }
}
class addCardLabel : UILabel {
    @IBInspectable var isDetailsTitle : Bool = false
    override func awakeFromNib() {
        self.font = CustomFont.regular.returnFont(11)
        self.textColor = UIColor(hexString: "#ACB1C0")
        self.textAlignment = .left
        if isDetailsTitle {
            self.font = CustomFont.medium.returnFont(12)
            self.textColor = colors.white.value
            self.textAlignment = .left
        }
        
    }
}
class InValidCVVLabel : UILabel {
  
    override func awakeFromNib() {
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = UIColor(hexString: "#FF4E4E")
        self.textAlignment = .left
    
        
    }
}
class customerMessageTimeLabel : UILabel {
    override func awakeFromNib() {
        self.font = CustomFont.medium.returnFont(16)
        self.textColor = UIColor(hexString: "#FFFFFF")
        self.textAlignment = .center
    
        
    }
}
class AdvisorAcceptlabel : DWAnimatedLabel {
    
    @IBInspectable var isTitle : Bool = false
    override func awakeFromNib() {
        //self.numberOfLines = 0
        if isTitle {
            self.font = CustomFont.bold.returnFont(28)
            self.textAlignment = .center
            self.textColor = colors.white.value
        } else {
            self.font = CustomFont.medium.returnFont(23)
            self.textAlignment = .center
            self.textColor = colors.white.value.withAlphaComponent(0.5)
        }
       
  
    }
}
