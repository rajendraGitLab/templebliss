//
//  CustomButton.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import UIKit

class theamSubmitButton : UIButton {
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
        self.backgroundColor = colors.white.value
        self.setTitleColor(colors.submitButtonTextColor.value, for: .normal)
       // self.titleLabel?.text = self.titleLabel?.text?.uppercased()
        self.titleLabel?.font = CustomFont.medium.returnFont(17)
        self.setTitle(self.title(for: .normal)?.uppercased(), for: .normal)
    }
}
class theamButtonWithBorder : UIButton {
    @IBInspectable var fontSize : CGFloat = 11
    @IBInspectable var bordrColor: UIColor? = colors.white.value
    @IBInspectable var fontColor: UIColor? = colors.white.value
    override func awakeFromNib() {
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 1
        self.layer.borderColor = bordrColor?.cgColor
        self.backgroundColor = .clear
        self.setTitleColor(fontColor, for: .normal)
        self.titleLabel?.font = CustomFont.medium.returnFont(fontSize)
    }
}
class BtnNotifyMe : UIButton {
   
    override func awakeFromNib() {
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
        self.layer.borderColor = UIColor(hexString: "#c7211b").cgColor
        self.backgroundColor = .clear
        self.setTitleColor(UIColor(hexString: "#c7211b"), for: .normal)
        self.titleLabel?.font = CustomFont.bold.returnFont(17)
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    }
}
class LoginScreenButon : UIButton {
    @IBInspectable var isForgotPassword : Bool = false
    @IBInspectable var issignUp : Bool = false
    override func awakeFromNib() {
        if isForgotPassword {
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(11))
        } else if issignUp {
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(15))
        }
    }
}
class MySessionButton : UIButton {
    @IBInspectable var isChatButton : Bool = false
    @IBInspectable var isRattingViewButton : Bool = false
    override func awakeFromNib() {
        if isRattingViewButton {
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value.withAlphaComponent(0.5), font: CustomFont.regular.returnFont(12))
        } else if isChatButton {
            self.setTitleColor(colors.white.value.withAlphaComponent(0.5), for: .normal)
            self.titleLabel?.font = CustomFont.regular.returnFont(12)
            self.contentMode = .scaleAspectFit
            self.imageEdgeInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 5)
        }
       
    }
}
class myProfileButton : UIButton {
    @IBInspectable var isLogout : Bool = false
    override func awakeFromNib() {
        if isLogout {
            self.setTitleColor(colors.logoutButtonColor.value, for: .normal)
            self.titleLabel?.font = CustomFont.bold.returnFont(19)
        }
    }
}
class RegisterScreenButon : UIButton {
    @IBInspectable var isClickHere : Bool = false
    @IBInspectable var isTermsCondition : Bool = false
    override func awakeFromNib() {
        if isClickHere {
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(15))
        } else if isTermsCondition {
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.bold.returnFont(13))
        }
    }
}
class EnterOTPButton : UIButton {
    override func awakeFromNib() {
        
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(11))
       
    }
}
class ShowAllReview : UIButton {
    override func awakeFromNib() {
        
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(11))
       
    }
}
class adviserDetailsButton : UIButton {
    override func awakeFromNib() {
        
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.medium.returnFont(14))
       
    }
}
class deleteSessionDelete : UIButton {
    @IBInspectable var isCancel : Bool = false
    override func awakeFromNib() {
        if isCancel {
            self.setTitleColor(colors.white.value, for: .normal)
            self.titleLabel?.font = CustomFont.bold.returnFont(17)
        } else {
            self.setTitleColor(colors.logoutButtonColor.value, for: .normal)
            self.titleLabel?.font = CustomFont.bold.returnFont(17)
        }
    }
}
class AdviserHomeButton : UIButton {
    @IBInspectable var isContactUs : Bool = false
    override func awakeFromNib() {
        
            self.setunderline(title: self.titleLabel?.text ?? "", color: colors.white.value, font: CustomFont.regular.returnFont(13))
       
    }
}
class AdviserAddMoneyButton  : UIButton {
    @IBInspectable var isCancel : Bool = false
    override func awakeFromNib() {
        if isCancel {
            self.setTitleColor(colors.logoutButtonColor.value, for: .normal)
            self.titleLabel?.font = CustomFont.bold.returnFont(17)
        } else {
            self.setTitleColor(UIColor(hexString: "#2CC168"), for: .normal)
            self.titleLabel?.font = CustomFont.bold.returnFont(17)
        }
    }
}
class AddNewCardButton  : UIButton {
  
    override func awakeFromNib() {
        self.setTitleColor(colors.white.value, for: .normal)
        self.titleLabel?.font = CustomFont.regular.returnFont(12)
    }
}
