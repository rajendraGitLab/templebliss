//
//  CustomImageView.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import AVKit

class myProfileImageView : UIImageView {
    @IBInspectable var isProfilePhotoView: Bool = false
    override func awakeFromNib() {
        if isProfilePhotoView {
           
            self.layer.cornerRadius = self.frame.size.height / 2
            self.clipsToBounds = true
            
            self.backgroundColor = .clear
        }
        
    }
}
