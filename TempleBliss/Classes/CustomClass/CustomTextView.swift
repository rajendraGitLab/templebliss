//
//  CustomTextView.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import AVKit
import GrowingTextView


class ContacsUsTextView : UITextView {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
    }
}
class RattingTextView : UITextView {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(13)
        self.textColor = colors.white.value
    }
}
class commonPopupTextView : UITextView {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
    }
}
class themeTextView: GrowingTextView {
    @IBInspectable var isBorder: Bool = false
    @IBInspectable var isFeedback:Bool = false
    @IBInspectable var isCornerRadius:Bool = false
    @IBInspectable var isMessageChat:Bool = false
    @IBInspectable var RadiusValue:CGFloat = 0
    override func awakeFromNib() {
        super.awakeFromNib()
       
        self.minHeight = 44
       
        self.backgroundColor = .clear
        if isFeedback{
            self.font = CustomFont.regular.returnFont(12)
            self.textColor = colors.white.value
            self.minHeight = 79
            self.maxHeight = 79
            self.textContainerInset = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        }else if isMessageChat {
            self.font = CustomFont.regular.returnFont(14)
            self.textColor = colors.white.value
        }else{
            self.font = CustomFont.regular.returnFont(12)
            self.textColor = colors.white.value
            self.minHeight = 134
            //self.textContainerInset = UIEdgeInsets(top: 13, left: 10, bottom: 13, right: 13)
        }
        
        if isBorder{
            self.layer.borderColor = UIColor(hexString: "#707070").withAlphaComponent(0.2).cgColor
            self.layer.borderWidth = 1
        }
        if isCornerRadius{
            self.layer.cornerRadius = RadiusValue
            self.clipsToBounds = true
        }
    }

}
