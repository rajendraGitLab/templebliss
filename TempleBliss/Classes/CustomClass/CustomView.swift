//
//  CustomView.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import Foundation
import AVKit
class BlurView : UIView {
    
    @IBInspectable var isLight : Bool = false
    
    override func awakeFromNib() {
        self.backgroundColor = .clear
        setUpBlurView()
    }
    func setUpBlurView() {
        
        
        if !UIAccessibility.isReduceTransparencyEnabled {
            self.backgroundColor = .clear

            let blurEffect = UIBlurEffect(style: .dark)
            let blurEffectView = UIVisualEffectView(effect: blurEffect)
            //always fill the view
            blurEffectView.frame = self.bounds
            blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

            self.addSubview(blurEffectView) //if you have more UIViews, use an insertSubview API to place it where needed
        } else {
            self.backgroundColor = .black
        }
        
   /*
        //        self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        
        //If iOS 13 is available, add blur effect:
        if #available(iOS 13.0, *) {
            //check if transparency is reduced in system accessibility settings..
            if UIAccessibility.isReduceTransparencyEnabled == true {
                
            } else {
                let backView = UIView(frame: self.bounds)
                backView.backgroundColor =  colors.submitButtonTextColor.value.withAlphaComponent(0.2)//UIColor(red: 8/255, green: 93/255, blue: 127/255, alpha: 0.67)

                self.addSubview(backView)
                
                let blurEffect = UIBlurEffect(style: .dark)
                let bluredEffectView = UIVisualEffectView(effect: blurEffect)
                bluredEffectView.frame = self.bounds
                
                let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
                let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
                vibrancyEffectView.frame = bluredEffectView.bounds
                
                bluredEffectView.layer.masksToBounds = true
                bluredEffectView.contentView.addSubview(vibrancyEffectView)
                self.addSubview(bluredEffectView)

                self.bringSubviewToFront(self)
                //self.bringSubviewToFront(vwMain)
            }
        } else {
            if UIAccessibility.isReduceTransparencyEnabled == true {
                
            } else {
                
                let backView = UIView(frame: self.bounds)
                backView.backgroundColor =  colors.submitButtonTextColor.value.withAlphaComponent(0.2) //UIColor(red: 8/255, green: 93/255, blue: 127/255, alpha: 0.67)
                self.addSubview(backView)
                let blurEffect = UIBlurEffect(style: .dark)
                let bluredEffectView = UIVisualEffectView(effect: blurEffect)
                bluredEffectView.frame = self.bounds
                
                let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
                let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
                vibrancyEffectView.frame = bluredEffectView.bounds
                
                bluredEffectView.layer.masksToBounds = true
                bluredEffectView.contentView.addSubview(vibrancyEffectView)
                self.addSubview(bluredEffectView)
                self.bringSubviewToFront(self)
               // self.view.bringSubviewToFront(vwMain)
            }
        }
        if isLight {
            if #available(iOS 13.0, *) {
                //check if transparency is reduced in system accessibility settings..
                if UIAccessibility.isReduceTransparencyEnabled == true {
                    
                } else {
                    let backView = UIView(frame: self.bounds)
                    backView.backgroundColor =  colors.submitButtonTextColor.value.withAlphaComponent(0.2)//UIColor(red: 8/255, green: 93/255, blue: 127/255, alpha: 0.67)

                    self.addSubview(backView)
                    
                    let blurEffect = UIBlurEffect(style: .light)
                    let bluredEffectView = UIVisualEffectView(effect: blurEffect)
                    bluredEffectView.frame = self.bounds
                    
                    let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
                    let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
                    vibrancyEffectView.frame = bluredEffectView.bounds
                    
                    bluredEffectView.layer.masksToBounds = true
                    bluredEffectView.contentView.addSubview(vibrancyEffectView)
                    self.addSubview(bluredEffectView)

                    self.bringSubviewToFront(self)
                    //self.bringSubviewToFront(vwMain)
                }
            } else {
                if UIAccessibility.isReduceTransparencyEnabled == true {
                    
                } else {
                    
                    let backView = UIView(frame: self.bounds)
                    backView.backgroundColor =  colors.submitButtonTextColor.value.withAlphaComponent(0.2) //UIColor(red: 8/255, green: 93/255, blue: 127/255, alpha: 0.67)
                    self.addSubview(backView)
                    let blurEffect = UIBlurEffect(style: .light)
                    let bluredEffectView = UIVisualEffectView(effect: blurEffect)
                    bluredEffectView.frame = self.bounds
                    
                    let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
                    let vibrancyEffectView = UIVisualEffectView(effect: vibrancyEffect)
                    vibrancyEffectView.frame = bluredEffectView.bounds
                    
                    bluredEffectView.layer.masksToBounds = true
                    bluredEffectView.contentView.addSubview(vibrancyEffectView)
                    self.addSubview(bluredEffectView)
                    self.bringSubviewToFront(self)
                   // self.view.bringSubviewToFront(vwMain)
                }
            }
        }
        
       
       */
    }
}
class viewViewClearBG : UIView {
    override func awakeFromNib() {
        self.backgroundColor = .clear
    }
}
class loginScreensBGView : UIView {
    override func awakeFromNib() {
        self.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        self.backgroundColor = colors.submitButtonTextColor.value.withAlphaComponent(0.11)
    }
}
class loginPageView : UIView {
    @IBInspectable var isBorder : Bool = false
    override func awakeFromNib() {
        if isBorder {
            self.backgroundColor = .clear
            self.layer.cornerRadius = 8
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.cgColor
            self.clipsToBounds = true
        }
    }
}
class BorderView : UIView {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.layer.cornerRadius = 8
        self.layer.borderWidth = 1
        self.layer.borderColor = colors.white.value.cgColor
        self.clipsToBounds = true
    }
}
class HomePageView : UIView {
    @IBInspectable var isTopSearchView: Bool = false
    @IBInspectable var isSeperatorView: Bool = false
    override func awakeFromNib() {
        self.backgroundColor = .clear
        if isTopSearchView {
            
            self.layer.cornerRadius = self.frame.size.height / 2
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.withAlphaComponent(0.38).cgColor
            self.clipsToBounds = true
        } else if isSeperatorView {
            self.backgroundColor = colors.white.value.withAlphaComponent(0.38)
        }
    }
}


class AdviserPageView : UIView {
    @IBInspectable var isTopSearchView: Bool = false
    @IBInspectable var isSeperatorView: Bool = false
    override func awakeFromNib() {
        self.backgroundColor = .clear
        if isTopSearchView {
            
            self.layer.cornerRadius = self.frame.size.height / 2
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.withAlphaComponent(0.38).cgColor
            self.clipsToBounds = true
        } else if isSeperatorView {
            self.backgroundColor = colors.white.value.withAlphaComponent(0.38)
        }
    }
}
class MySessionPageView : UIView {
    @IBInspectable var isMySessionView: Bool = false
    @IBInspectable var isChatView: Bool = false
    @IBInspectable var isMySessionAdviserView: Bool = false
    override func awakeFromNib() {
        self.backgroundColor = .clear
        if isMySessionView {
            
            self.layer.cornerRadius = 10
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.withAlphaComponent(0.10).cgColor
            self.clipsToBounds = true
        } else if isMySessionAdviserView {
            self.backgroundColor = colors.white.value.withAlphaComponent(0.10)
        } else if isChatView {
            self.backgroundColor = .clear
            self.layer.cornerRadius = self.frame.size.height / 2
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.withAlphaComponent(0.5).cgColor
            self.clipsToBounds = true
        }
    }
}
class myprofileView : UIView {
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.backgroundColor = colors.white.value.withAlphaComponent(0.10)
        self.clipsToBounds = true
    }
}
class AdviserDetailsView : UIView {
    @IBInspectable var isRateAndReviewBG: Bool = false
    @IBInspectable var isSeperator: Bool = false
    @IBInspectable var isCommunicationView: Bool = false
    override func awakeFromNib() {
        if isRateAndReviewBG {
            self.layer.cornerRadius = 10
            self.backgroundColor = colors.white.value.withAlphaComponent(0.05)
            self.clipsToBounds = true
        } else if isSeperator {
            self.backgroundColor = colors.white.value.withAlphaComponent(0.13)
        } else if isCommunicationView {
            self.layer.cornerRadius = 7
                self.layer.borderWidth = 1
            self.backgroundColor = .clear
            
        }
    }
}
class MYCreditView : UIView {
    override func awakeFromNib() {
        self.backgroundColor = colors.tabBarBGColor.value
    }
}
class seperatorView : UIView {
    override func awakeFromNib() {
        self.backgroundColor = colors.white.value.withAlphaComponent(0.13)
    }
}
class myProfileView : UIView {
   
}
class rateAdviserView : UIView {
    @IBInspectable var rattingView: Bool = false
    override func awakeFromNib() {
        if rattingView {
            self.layer.cornerRadius = 8
            self.layer.borderWidth = 1
            self.layer.borderColor = colors.white.value.cgColor
            self.clipsToBounds = true
            self.backgroundColor = colors.black.value.withAlphaComponent(0.28)
        }
    }
}
class deleteSessionView : UIView {
    @IBInspectable var isBGView: Bool = false
    @IBInspectable var isSeperatorView: Bool = false
    override func awakeFromNib() {
        if isBGView {
            self.layer.cornerRadius = 12
            
         
            self.clipsToBounds = true
            self.backgroundColor = colors.black.value.withAlphaComponent(0.28)
        } else if isSeperatorView {
            self.backgroundColor = colors.white.value.withAlphaComponent(0.23)
        }
    }
}
