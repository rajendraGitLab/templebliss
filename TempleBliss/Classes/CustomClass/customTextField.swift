//
//  customTextField.swift
//  CoreSound
//
//  Created by EWW083 on 03/02/20.
//  Copyright © 2020 EWW083. All rights reserved.
//

import UIKit
import AVKit
import FormTextField
class loginPageTextField : UITextField {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
            self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
    }
}
class HomePageTextField : UITextField {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
      
    }
   
}
class AdviserPageTextField : UITextField {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
    }
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                let button = UIButton(frame: CGRect(x: 10, y: 0, width: 20, height: 20))
                //let imageView = UIImageView(frame: )
                button.setImage(image, for: .normal)
               
                button.contentMode = .scaleAspectFit
                button.tintColor = tintColor
                button.isUserInteractionEnabled = false
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 30, height: 20))
                view.isUserInteractionEnabled = false
                view.addSubview(button)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
class changePasswodTextField : UITextField {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
    }
}
class ContactUsTextField : UITextField {
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
    }
}
class RegisterTextField : UITextField {
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                imageView.image = image
                imageView.contentMode = .scaleAspectFit
                imageView.tintColor = tintColor
                
                
               
                imageView.isUserInteractionEnabled = false
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 22, height: 22))
                view.isUserInteractionEnabled = false
                view.addSubview(imageView)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
class EnterOTPTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .center
        self.textContentType = .oneTimeCode
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(22)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class CountryCodeTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .center
       
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.tintColor = .clear
        
    }
    
     open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
         return false
     }
}
class ForgotPasswordTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class setNewPasswordTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}

class BuyMinutesTextField : UITextField {
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                button.setImage(image, for: .normal)
               // let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                //imageView.image = image
                button.tintColor = tintColor
                button.isUserInteractionEnabled = false
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 22, height: 22))
                view.isUserInteractionEnabled = false
               
                view.addSubview(button)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    override func awakeFromNib() {
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
       
        
    }
   
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
class EditProfileTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class SendMessageTextField : UITextField {
    var sendMessageClick : (() -> ())?
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value.withAlphaComponent(0.35)])
        
    }
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                
                
                let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                imgView.image = image
                
                
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: self.superview?.frame.size.height ?? 0.0, height: self.superview?.frame.size.height ?? 0.0))
               // button.setImage(image, for: .normal)
               // let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 22, height: 22))
                //imageView.image = image
                button.addTarget(self, action: #selector(sendMessage(_:)), for: .touchUpInside)
                button.tintColor = tintColor
               
                let view = UIView(frame : CGRect(x: 0, y: 0, width: self.superview?.frame.size.height ?? 0.0, height: self.superview?.frame.size.height ?? 0.0))
                imgView.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height / 2)
                view.addSubview(imgView)
                view.addSubview(button)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    @objc func sendMessage(_ sender : UIButton) {
        if let click = self.sendMessageClick {
            click()
        }
    }
}
class addCategoryTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .center
        self.backgroundColor = .clear
        self.font = CustomFont.medium.returnFont(14)
      
        let rightView = UIView(frame: CGRect(x: -5, y: 0, width: 40, height: 42))

        let label = UILabel(frame: CGRect(x: -5, y: 0, width: 40, height: 42))
        label.font = CustomFont.medium.returnFont(14)
        label.text = "/min"
        label.textAlignment = .center
       // label.isHidden = true
       
        rightView.addSubview(label)
        

        self.rightView = rightView
        self.rightViewMode = .always
        
        
    }
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
}
class addPayPalBusinessIDTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class AdviserEditProfileTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .left
        self.backgroundColor = .clear
        self.font = CustomFont.regular.returnFont(12)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class AdviserAddMoneyTextField : UITextField {
    override func awakeFromNib() {
        self.textAlignment = .center
        self.backgroundColor = .clear
        self.font = CustomFont.medium.returnFont(15)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
    }
}
class AddCategoryTextField: UITextField {
    
   
    var dropDownClick:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()

        
        self.textAlignment = .left
        self.textColor = colors.white.value
        self.font = CustomFont.regular.returnFont(12)
        
       
    }

 
    //MARK:- LeftImage Set
   
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                //let imageView = UIImageView(frame: )
                button.setImage(image, for: .normal)
                button.addTarget(self, action: #selector(DropDown(_:)), for: .touchUpInside)
                button.contentMode = .scaleAspectFit
                button.tintColor = tintColor
                button.isUserInteractionEnabled = false
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 30, height: 22))
                view.isUserInteractionEnabled = false
                view.addSubview(button)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    @objc func DropDown(_ sender: Any) {
        if let click = self.dropDownClick {
            click()
        }
        print("DropDown")
    }
}
class EnterCVVTextField: FormTextField {
    
   
    var dropDownClick:(()->())?
    override func awakeFromNib() {
        super.awakeFromNib()

        
        self.textAlignment = .left
        self.textColor = colors.white.value
        self.font = CustomFont.regular.returnFont(12)
        
        apply()
    }

 
    //MARK:- LeftImage Set
   
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let image = rightImage {
                rightViewMode = .always
                let button = UIButton(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
                //let imageView = UIImageView(frame: )
                button.setImage(image, for: .normal)
                button.addTarget(self, action: #selector(DropDown(_:)), for: .touchUpInside)
                button.contentMode = .scaleAspectFit
                button.tintColor = tintColor
                button.isUserInteractionEnabled = false
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 30, height: 22))
                view.isUserInteractionEnabled = false
                view.addSubview(button)
                rightView = view
            }else{
                rightViewMode = .never
            }
        }
    }
    @objc func DropDown(_ sender: Any) {
        if let click = self.dropDownClick {
            click()
        }
        print("DropDown")
    }
    func apply() {
           let enabledBackgroundColor = UIColor.clear
       let enabledBorderColor = UIColor(hexString: "FFFFFF")
       let enabledTextColor = UIColor(hexString: "FFFFFF")
       let activeBorderColor = UIColor(hexString: "FFFFFF")

        FormTextField.appearance().clearButtonMode = .never
        
           FormTextField.appearance().borderWidth = 0
       FormTextField.appearance().placeHolderColor = enabledBorderColor
           FormTextField.appearance().clearButtonColor = activeBorderColor
           FormTextField.appearance().font = CustomFont.regular.returnFont(15)

           FormTextField.appearance().enabledBackgroundColor = enabledBackgroundColor
           FormTextField.appearance().enabledBorderColor = enabledBorderColor
           FormTextField.appearance().enabledTextColor = enabledTextColor

           FormTextField.appearance().validBackgroundColor = enabledBackgroundColor
           FormTextField.appearance().validBorderColor = enabledBorderColor
           FormTextField.appearance().validTextColor = enabledTextColor

           FormTextField.appearance().activeBackgroundColor = enabledBackgroundColor
           FormTextField.appearance().activeBorderColor = activeBorderColor
           FormTextField.appearance().activeTextColor = enabledTextColor

           FormTextField.appearance().inactiveBackgroundColor = enabledBackgroundColor
           FormTextField.appearance().inactiveBorderColor = enabledBorderColor
           FormTextField.appearance().inactiveTextColor = enabledTextColor

       FormTextField.appearance().disabledBackgroundColor = UIColor(hexString: "DFDFDF")
           FormTextField.appearance().disabledBorderColor = UIColor(hexString: "DFDFDF")
           FormTextField.appearance().disabledTextColor = UIColor.white

           FormTextField.appearance().invalidBackgroundColor = UIColor(hexString: "FFC9C8")
           FormTextField.appearance().invalidBorderColor = UIColor(hexString: "FF4B47")
           FormTextField.appearance().invalidTextColor = UIColor(hexString: "FF4B47")
       }
}
class GeneralPickerView: UIPickerView {

    public private(set) var toolbar: UIToolbar?
    public weak var generalPickerDelegate: GeneralPickerViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    private func commonInit() {
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        //toolBar.tintColor = .black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTapped))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)


        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true

        self.toolbar = toolBar
    }

    @objc func doneTapped() {
        self.generalPickerDelegate?.didTapDone()
    }

    
}
protocol GeneralPickerViewDelegate: class {
    func didTapDone()
    func didTapCancel()
}
extension UITextField {
    
    func setInputViewDatePicker(target: Any, selector: Selector) {
        
        
        // Create a UIDatePicker object and assign to inputView
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))//1
        datePicker.datePickerMode = .date //2
        // datePicker.maximumDate = Date()
        // iOS 14 and above
        
        let calendar = Calendar(identifier: .gregorian)
        
        let currentDate = Date()
        var components = DateComponents()
        components.calendar = calendar
        
        components.year = -18
        let maxDate = calendar.date(byAdding: components, to: currentDate)!
        
        components.year = -118
        let minDate = calendar.date(byAdding: components, to: currentDate)!
        
        datePicker.minimumDate = minDate
        datePicker.maximumDate = maxDate
        
        
        if #available(iOS 14, *) {// Added condition for iOS 14
          datePicker.preferredDatePickerStyle = .wheels
          datePicker.sizeToFit()
        }
        self.inputView = datePicker //3
        
        // Create a toolbar and assign it to inputAccessoryView
        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0)) //4
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil) //5
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel)) // 6
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector) //7
        toolBar.setItems([cancel, flexible, barButton], animated: false) //8
        self.inputAccessoryView = toolBar //9
    }
    
  
    @objc func tapCancel() {
        self.resignFirstResponder()
    }
    
}
class addCarddetailsTextField : FormTextField {
    @IBInspectable var RightSideImage : UIImage? {
        didSet {
            if let image = RightSideImage{
                rightViewMode = .always
                let view = UIView(frame : CGRect(x: 0, y: 0, width: 33, height: 24))
                
                let imageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 14, height: 14))
                imageView.image = image
                imageView.tintColor = tintColor
                imageView.contentMode = .scaleAspectFit
                imageView.isUserInteractionEnabled = false
                view.addSubview(imageView)
                view.isUserInteractionEnabled = false
                rightView = view
            }else {
                rightViewMode = .never
            }

        }
    }
    override func awakeFromNib() {
        
        self.font = CustomFont.regular.returnFont(15)
        self.textColor = colors.white.value
        self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        
        apply()
    }
    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
     func apply() {
            let enabledBackgroundColor = UIColor.clear
        let enabledBorderColor = UIColor(hexString: "FFFFFF")
        let enabledTextColor = UIColor(hexString: "FFFFFF")
        let activeBorderColor = UIColor(hexString: "FFFFFF")

            FormTextField.appearance().borderWidth = 0
        FormTextField.appearance().placeHolderColor = enabledBorderColor
            FormTextField.appearance().clearButtonColor = activeBorderColor
            FormTextField.appearance().font = CustomFont.regular.returnFont(15)

            FormTextField.appearance().enabledBackgroundColor = enabledBackgroundColor
            FormTextField.appearance().enabledBorderColor = enabledBorderColor
            FormTextField.appearance().enabledTextColor = enabledTextColor

            FormTextField.appearance().validBackgroundColor = enabledBackgroundColor
            FormTextField.appearance().validBorderColor = enabledBorderColor
            FormTextField.appearance().validTextColor = enabledTextColor

            FormTextField.appearance().activeBackgroundColor = enabledBackgroundColor
            FormTextField.appearance().activeBorderColor = activeBorderColor
            FormTextField.appearance().activeTextColor = enabledTextColor

            FormTextField.appearance().inactiveBackgroundColor = enabledBackgroundColor
            FormTextField.appearance().inactiveBorderColor = enabledBorderColor
            FormTextField.appearance().inactiveTextColor = enabledTextColor

        FormTextField.appearance().disabledBackgroundColor = UIColor(hexString: "DFDFDF")
            FormTextField.appearance().disabledBorderColor = UIColor(hexString: "DFDFDF")
            FormTextField.appearance().disabledTextColor = UIColor.white

            FormTextField.appearance().invalidBackgroundColor = UIColor(hexString: "FFC9C8")
            FormTextField.appearance().invalidBorderColor = UIColor(hexString: "FF4B47")
            FormTextField.appearance().invalidTextColor = UIColor(hexString: "FF4B47")
        }
}
