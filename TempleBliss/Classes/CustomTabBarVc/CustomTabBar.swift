//
//  CustomTabBar.swift
//  TempleBliss
//
//  Created by Sj's iMac on 14/04/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class CustomTabBar: UITabBar {

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 20
        layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        layer.backgroundColor = colors.tabBarBGColor.value.cgColor

    }

    
    override func layoutSubviews() {
          super.layoutSubviews()
          self.isTranslucent = true
          var tabFrame            = self.frame
          tabFrame.size.height    = 65 + (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? CGFloat.zero)
          tabFrame.origin.y       = self.frame.origin.y +   ( self.frame.height - 65 - (UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? CGFloat.zero))
          self.layer.cornerRadius = 20
          self.frame            = tabFrame

          self.items?.forEach({ $0.titlePositionAdjustment = UIOffset(horizontal: 0.0, vertical: -5.0) })


      }

}
