//
//  AdviserCommonLabelPopupViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserCommonLabelPopupViewController: BaseViewController {
    
    //MARK: - Properties
    var textForShow : String?
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblText: commonLabelOnPopup!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: { [self] in
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserContactUsViewController.storyboardID)
            self.navigationController?.pushViewController(controller, animated: true)
        })
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        lblText.text = textForShow
    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
}
