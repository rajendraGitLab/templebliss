//
//  CommonPopup.swift
//  TempleBliss
//
//  Created by Apple on 25/03/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class CommonPopup: BaseViewController {

    //MARK: - Properties
    var isLeftClickClosour : (() -> ())?
    var isRightClickClosour : (() -> ())?
     
    var lblTextString : String = ""
    var LblDescriptionText : String = ""
    var btnLeftString : String = ""
    var btnRightString : String = ""
    
    var leftbtnColor = UIColor()
    var rightbtnColor = UIColor()
    //MARK: - IBOutlets
    
    @IBOutlet weak var btnRight: deleteSessionDelete!
    @IBOutlet weak var btnleft: deleteSessionDelete!
    @IBOutlet weak var lblText: deleteSessionLabel!
    @IBOutlet weak var LblDescription: deleteSessionLabel!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
      
        
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        if LblDescriptionText == "" {
            LblDescription.isHidden = true
        } else {
            LblDescription.isHidden = false
            LblDescription.text = LblDescriptionText
        }
        lblText.text = lblTextString
        btnleft.setTitle(btnLeftString, for: .normal)
        btnRight.setTitle(btnRightString, for: .normal)
        
        btnleft.setTitleColor(leftbtnColor, for: .normal)
        btnRight.setTitleColor(rightbtnColor, for: .normal)
    }
    //MARK: - IBActions
    
    @IBAction func btnLeftClick(_ sender: Any) {
        if let click = self.isLeftClickClosour {
            click()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnRightClick(_ sender: Any) {
        if let click = self.isRightClickClosour {
            click()
            self.dismiss(animated: true, completion: nil)
        }
       
    }
    //MARK: - API Calls
    
    
    
    

}
