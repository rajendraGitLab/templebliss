//
//  AdviserCommonPopupViewController.swift
//  TempleBliss
//
//  Created by Apple on 29/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserCommonPopupViewController: BaseViewController {

    //MARK: - Properties
    var isFromAdvisorMessageController : Bool = false
    
    
    var bookingID = ""
    
    var customTabBarController : CustomTabBarVC?
    var isDismissCLosour : (() -> ())?
   
    var SessionNoteDescripiton = ""
    
    var isShowLeftIcon : Bool = false
    var isShowRightIcon : Bool = false
    var isShowNoteTextView : Bool = false
    var isShowNoteTextViewWithData : Bool = false
    
    var StringButton : String?
    var stringDescription : String?
    var btnSubmitClosour : (() -> ())?
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblDescription: commonPopupLabel!
    @IBOutlet weak var textViewNoteMainView: BorderView!
    @IBOutlet weak var textViewNote: themeTextView!
    @IBOutlet weak var btnSubmit: theamSubmitButton!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
      
        setLocalization()
        setValue()
     
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: isShowLeftIcon == true ? NavItemsLeft.backFromPresnet.value : NavItemsLeft.none.value, rightImages: isShowRightIcon == true ? [NavItemsRight.edit.value] : [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        textViewNoteMainView.isHidden = isShowNoteTextView ? false : true
        textViewNote.text = SessionNoteDescripiton
        if isShowNoteTextView {
            lblDescription.font = CustomFont.medium.returnFont(18)
        }
        
        if(!isShowNoteTextView && isShowNoteTextViewWithData)
        {
            lblDescription.isHidden = true
            textViewNoteMainView.isHidden = false
            textViewNote.text = stringDescription
            textViewNote.isEditable = false
        }
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
       
    }
    override func viewDidDisappear(_ animated: Bool) {
        if let click = self.isDismissCLosour {
            click()
        }
    }
   
    
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        btnSubmit.setTitle(StringButton?.uppercased(), for: .normal)
        lblDescription.text  = stringDescription
        SessionNote = stringDescription ?? ""
    }
    //MARK: - IBActions
    
    @IBAction func btnSubmitClick(_ sender: Any) {
        if textViewNoteMainView.isHidden {
            if let click = self.btnSubmitClosour {
                click()
            }
        } else {
            if textViewNote.text.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
                let alert = UIAlertController(title: AppInfo.appName , message: "Please enter note", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                }))
        
                self.present(alert, animated: true, completion: nil)
                //Utilities.ShowAlert(OfMessage: "")
            } else {
            let AddNoteModel = SessionNoteReqModel()
            AddNoteModel.booking_id = bookingID
            AddNoteModel.note = textViewNote.text.trimmingCharacters(in: .whitespacesAndNewlines)
            webserviceCallForAddNotes(reqModel: AddNoteModel)
            }
        }
    }
    
    //MARK: - API Calls
    
    
}

extension AdviserCommonPopupViewController {
    func webserviceCallForAddNotes(reqModel:SessionNoteReqModel) {
        Utilities.showHud()
        WebServiceSubClass.AddNotes(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if self.isFromAdvisorMessageController {
                SingletonClass.sharedInstance.isNoteAdded = true
                SingletonClass.sharedInstance.noteText = reqModel.note
                
            } else {
                SingletonClass.sharedInstance.isNoteAdded = false
                SingletonClass.sharedInstance.noteText = ""
            }
           
            if let click = self.btnSubmitClosour {
                
                
                click()
            }
            
            if status {
              
            } else {
               
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}
