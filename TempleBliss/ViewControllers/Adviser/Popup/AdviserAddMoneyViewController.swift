//
//  AdviserAddMoneyViewController.swift
//  TempleBliss
//
//  Created by Apple on 30/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserAddMoneyViewController: BaseViewController {

    //MARK: - Properties
    var isDismiss : (() -> ())?
    var isCancleClosour : (() -> ())?
    var isDoneClosour : (() -> ())?
    
    
    var penndingForWithDraw = ""
    //MARK: - IBOutlets
    
    @IBOutlet weak var textFieldAmount: AdviserAddMoneyTextField!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let click = self.isDismiss {
            click()
           // self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //MARK: - IBActions
    
    @IBAction func btnCancle(_ sender: Any) {
        if let click = self.isCancleClosour {
            click()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        if validation() {
            WithDrawMoney()
        }
        
    }
    
    //MARK: - validation
    func validation() -> Bool {
        let pendingAmount : Double = Double(penndingForWithDraw) ?? 0.0
        let enteredAmount : Double = Double(textFieldAmount.text ?? "") ?? 0.0
        
        if textFieldAmount.text?.trim() == "" {
            self.direction = 1
            self.shakes = 0
            textFieldAmount.text = ""
            self.shake(textFieldAmount)
            
            return false
        }  else if pendingAmount == 0.0 {
            self.direction = 1
            self.shakes = 0
            textFieldAmount.text = ""
            self.shake(textFieldAmount)
          
            return false
        }  else if pendingAmount < enteredAmount {
            self.direction = 1
            self.shakes = 0
            textFieldAmount.text = ""
            self.shake(textFieldAmount)
          
            return false
        }
        return true
    }
    var direction:CGFloat = 1
    var shakes = 0
    func shake(_ theOneYouWannaShake: UIView?) {
        UIView.animate(withDuration: 0.06, animations: {
            theOneYouWannaShake?.transform = CGAffineTransform(translationX: 5 * self.direction, y: 0)
        }) { [self] finished in
            
            if shakes >= 10 {
                theOneYouWannaShake?.transform = CGAffineTransform.identity
                return
            }
            shakes += 1
            direction = direction * -1
            shake(theOneYouWannaShake)
        }
    }
    //MARK: - API Calls
    
    
    
    

}
extension AdviserAddMoneyViewController {
    func WithDrawMoney() {
        let withdrawModel = withDrawMoneyReqModel()
        withdrawModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        withdrawModel.amount = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        webserviceForWithDraw(reqModel: withdrawModel)
    }
    func webserviceForWithDraw(reqModel:withDrawMoneyReqModel) {
        WebServiceSubClass.AdvisorWithDraw(addCategory: reqModel, completion: {(json, status, response) in
         
            if status {
              
                   // self.dismiss(animated: true, completion: nil)
               
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}
