//
//  AdvisorRequestAcceptViewController.swift
//  TempleBliss
//
//  Created by Apple on 07/04/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class AdvisorRequestAcceptViewController: BaseViewController {

    //MARK: - Properties
    var communicationType = ""
    var totalMinutes = ""
    var customerName = ""
    
    var closourForBtnAccept : (() -> ())?
    var closourForBtnReject : (() -> ())?
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblcommunicationType: AdvisorAcceptlabel!
    @IBOutlet weak var lblName: AdvisorAcceptlabel!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        NotificationCenter.default.addObserver(self, selector: #selector(acceptChatHandler), name: NSNotification.Name(rawValue: "AcceptChat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(rejectChatHandler), name: NSNotification.Name(rawValue: "RejectChat"), object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        print("AdvisorRequestAcceptViewController viewWillAppear")
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AcceptChat"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RejectChat"), object: nil)
        print("AdvisorRequestAcceptViewController viewWillDisappear")
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        
        if totalMinutes == "1" {
            lblName.text = "\(totalMinutes) Minute : \(customerName)"
        } else {
            lblName.text = "\(totalMinutes) Minutes : \(customerName)"
        }
        
        
        
//        let fullString = NSMutableAttributedString(string: "")
//
//        let image1Attachment = NSTextAttachment()
//
//        image1Attachment.image =
//
//        let image1String = NSAttributedString(attachment: image1Attachment)
//
//        let finalNewString = NSMutableAttributedString()
//
//        finalNewString.append(image1String)
//        finalNewString.append(fullString)
//
//        lblcommunicationType.attributedText = finalNewString
        
        
        lblcommunicationType.text = "TempleBliss \(communicationType)"
      
        
    }
    //MARK: - IBActions
    
    @IBAction func btnRejectClick(_ sender: Any) {
        if let click = self.closourForBtnReject {
            click()
        }
    }
    
    @IBAction func btnAcceptClick(_ sender: Any) {
        
        if let click = self.closourForBtnAccept {
            click()
        }
    }
    //MARK: - API Calls
    
    
    @objc func acceptChatHandler() {
        if let click = self.closourForBtnAccept {
            click()
        }
    }
    
    @objc func rejectChatHandler() {
        if let click = self.closourForBtnReject {
            click()
        }
    }

}
