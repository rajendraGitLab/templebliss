//
//  AdvisorAudioCallViewController.swift
//  TempleBliss
//
//  Created by Apple on 05/04/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit
import AVFoundation
import PushKit
import CallKit
import AVFoundation
import TwilioVoice
import SDWebImage
import UserNotifications

class AdvisorAudioCallViewController: BaseViewController {
   
    
    //MARK: - Properties
  
    var customerID : String?
    var AdvisorID : String?
    var bookingID : String?
    
    var audioDevice = AdvisorAudioCall.sharedInstance.audioDevice
    var CustomerUserImageURl = ""
    var CustomerName = ""
    var incomingPushCompletionCallback: (() -> Void)?
    let kRegistrationTTLInDays = 365
    
    let kCachedDeviceToken = "CachedDeviceToken" //"CachedDeviceToken"
    let kCachedBindingDate = "CachedBindingDate"
    
    var callInvite12: CallInvite?
    var activeCallInvites: [String: CallInvite]! = [:]
    var activeCalls: [String: Call]! = [:]
    var activeCall: Call? = nil
    var callKitCompletionCallback: ((Bool) -> Void)? = nil
    
    
    var callKitProvider: CXProvider?
    var callKitCallController : CXCallController? = nil
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil
    var userInitiatedDisconnect: Bool = false
    var myIdentity = ""
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        let update = CXCallUpdate()
//                update.remoteHandle = CXHandle(type: .generic, value: "")
//        callKitProvider!.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
    }
    
    deinit {
        if let provider = appDel.callKitProvider {
            provider.invalidate()
        }

    }
    
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var BtnSpeaker: UIButton!
    @IBOutlet weak var BtnMuteCall: UIButton!
    @IBOutlet weak var BtnEndcall: UIButton!
    @IBOutlet weak var AdvisorImageView: UIImageView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblAdvisorName: UILabel!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let configuration = CXProviderConfiguration(localizedName: "Voice Quickstart")
//                configuration.maximumCallGroups = 1
//                configuration.maximumCallsPerCallGroup = 1
//        appDel.callKitProvider = CXProvider(configuration: configuration)
//        if let provider = appDel.callKitProvider {
//                    provider.setDelegate(self, queue: nil)
//                }

//        callKitProvider = CXProvider(configuration: configuration)
//        callKitCallController = CXCallController()
//
//
//
//        callKitProvider?.setDelegate(self, queue: nil)
//        TwilioVoiceSDK.audioDevice = self.audioDevice
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        setLocalization()
        setValue()
        
//        TwilioVoiceSDK.audioDevice = self.audioDevice
        checkRecordPermission { [weak self] permissionGranted in
            let uuid = UUID()
            let handle = self?.myIdentity ?? ""
            print(self?.myIdentity ?? "")
            guard !permissionGranted else {
//                sleep(1)
                
                if self?.callKitCallController == nil {
                    self?.callKitCallController = CXCallController()
                }
                print("lplplplp")
                let param = ["user_id" : SingletonClass.sharedInstance.AdvisorAudioCallData?["user_id"].int ?? 0,
                             "advisor_id": Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
                             "advisor_name" : SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? "",
                             "booking_id" : SingletonClass.sharedInstance.AdvisorAudioCallData?["booking_id"].int ?? 0] as [String: Any]
                SocketIOManager.shared.socketEmit(for: socketApiKeys.PickupCall.rawValue, with: param)
                
//                let uuid = UUID(uuidString: "828fa2de-913f-0ed4-c337-edddaab8053c")
//                // Always report to CallKit
//                self?.reportIncomingCall(from: "jai_3575", uuid: uuid!)
                return
            }
            
            self?.showMicrophoneAccessRequest()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ATDebug New :: \(#function)")
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
        print("ATDebug New :: \(#function)")
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        appDel.CutIncomingCall()
        
        self.navigationController?.navigationBar.isHidden = false
    }
    func pickupCall()
    {
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.PickupCall.rawValue) { (response) in
 
            print(#function)
            print(response)
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["status"].string?.lowercased() == "accepted" {
//                        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
//                        SocketIOManager.shared.socket.on(socketApiKeys.PickupCall.rawValue) { anyy, SocketAckEmitter in
//                            print("sssdddddddd:")
//                        }
                    } else if singleResponse["status"].string?.lowercased() == "rejected" {
                        self.lblTimer.text = "Call ended"
                        if let topVC = UIApplication.topViewController() {
                            
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                    }
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
        }
    }
    func checkRecordPermission(completion: @escaping (_ permissionGranted: Bool) -> Void) {
        let permissionStatus = AVAudioSession.sharedInstance().recordPermission
        
        switch permissionStatus {
        case .granted:
            // Record permission already granted.
            completion(true)
        case .denied:
            // Record permission denied.
            completion(false)
        case .undetermined:
            // Requesting record permission.
            // Optional: pop up app dialog to let the users know if they want to request.
            AVAudioSession.sharedInstance().requestRecordPermission { granted in completion(granted) }
        default:
            completion(false)
        }
    }
    func showMicrophoneAccessRequest() {
        let alertController = UIAlertController(title: "Voice Quick Start",
                                                message: "Microphone permission not granted",
                                                preferredStyle: .alert)
        
        let continueWithoutMic = UIAlertAction(title: "Continue without microphone", style: .default) { [weak self] _ in
            
//            self?.performStartCallAction(uuid: uuid, handle: handle)
        }
        
        let goToSettings = UIAlertAction(title: "Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                      options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly: false],
                                      completionHandler: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //            self?.toggleUIState(isEnabled: true, showCallControl: false)
            //            self?.stopSpin()
        }
        
        [continueWithoutMic, goToSettings, cancel].forEach { alertController.addAction($0) }
        
        present(alertController, animated: true, completion: nil)
    }
    func fetchAccessToken() -> String? {
        print("My Identity New :: \(self.myIdentity)")
//        print(":::::::: \(socketApiKeys.SocketBaseURL.rawValue)/token/\(self.myIdentity)")
        guard let accessTokenURL = URL(string: "\(SOCKET_URL)/token/\(self.myIdentity)") else { return nil }
       
        let jsonString = try? String(contentsOf: accessTokenURL, encoding: .utf8)
        
        let jsonDictonary = convertToDictionary(text: jsonString ?? "")
        let identityFromJson:String = jsonDictonary?["identity"] as? String ?? ""
        let accessToken:String = jsonDictonary?["token"] as? String ?? ""
        
        print("identityFromJson from json is :: \(identityFromJson)")
        print("accessToken from json is :: \(accessToken)")
        
//        return accessToken
        return AccessTokenForVOIP
    }
    
     func convertToDictionary(text: String) -> [String: Any]? {
         
         if let data = text.data(using: .utf8) {
             do {
                 return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
             } catch {
                 print(error.localizedDescription)
             }
         }
         
         return nil
         
     }
    
    func setLocalization() {
        print("ATDebug New :: \(#function)")
    }
    func setValue() {
        AdvisorImageView.contentMode = .scaleToFill
        print("ATDebug New :: \(#function)")
        let imgURL:String = "\(self.CustomerUserImageURl)"
        if imgURL != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(imgURL)")
            
            AdvisorImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            AdvisorImageView.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
        } else {
            AdvisorImageView.image = UIImage(named: "user_dummy_profile")
        }
        lblAdvisorName.text = self.CustomerName
        lblTimer.text = "00:00"
        
    }
    
    
    //MARK: - IBActions
    
    @IBAction func BtnSpeakerClick(_ sender: UIButton) {
        
        print("ATDebug New :: \(#function)")
        if sender.isSelected {
            sender.isSelected = false
            AdvisorAudioCall.sharedInstance.toggleAudioRoute(toSpeaker: false)
        } else {
            sender.isSelected = true
            AdvisorAudioCall.sharedInstance.toggleAudioRoute(toSpeaker: true)
        }
        
    }
    @IBAction func BtnEndCallClick(_ sender: Any) {
        print("ATDebug New :: \(#function)")
        appDel.SocketEmitForCutCall(reject: 1)
        appDel.CallCut()
//        self.dismiss(animated: true)
    }
    @IBAction func BtnMuteCallClick(_ sender: UIButton) {
        
        print("ATDebug New :: \(#function)")
        guard let activeCall = appDel.AdvisorActiveCall else { return }
        if sender.isSelected {
            sender.isSelected = false
            activeCall.isMuted = false
            
        } else {
            sender.isSelected = true
            activeCall.isMuted = true
            
        }
    }
    
    func callInviteReceived(_ callInvite: CallInvite) {
        NSLog("callInviteReceived:")
        
//        if (self.callInvite != nil) {
//            NSLog("A CallInvite is already in progress. Ignoring the incoming CallInvite from \(callInvite.from)")
//            self.incomingPushHandled()
//            return;
//        } else if (self.call != nil) {
//            NSLog("Already an active call.");
//            NSLog("  >> Ignoring call from \(callInvite.from)");
//            self.incomingPushHandled()
//            return;
//        }
//
//        self.callInvite = callInvite
        let from = (callInvite.from ?? "Voice Bot").replacingOccurrences(of: "client:", with: "")
//        let uuid = UUID(uuidString: "8BA6F529-990C-45F5-892C-8910D3899FD2")
//        // Always report to CallKit
        reportIncomingCall(from: from, uuid: callInvite.uuid)
        self.activeCallInvites[callInvite.uuid.uuidString] = callInvite
    }
    
    //MARK: - API Calls
    
}
extension AdvisorAudioCallViewController: CallDelegate {
    func callDidStartRinging(call: Call) {
        NSLog("callDidStartRinging:")
        
//        placeCallButton.setTitle("Ringing", for: .normal)
        
        /*
         When [answerOnBridge](https://www.twilio.com/docs/voice/twiml/dial#answeronbridge) is enabled in the
         <Dial> TwiML verb, the caller will not hear the ringback while the call is ringing and awaiting to be
         accepted on the callee's side. The application can use the `AVAudioPlayer` to play custom audio files
         between the `[TVOCallDelegate callDidStartRinging:]` and the `[TVOCallDelegate callDidConnect:]` callbacks.
        */
//        if playCustomRingback {
//            playRingback()
//        }
    }
    
    func callDidConnect(call: Call) {
        NSLog("callDidConnect:")
        print("DEBUG11 :: \(#function)")
//        if playCustomRingback {
//            stopRingback()
//        }
        
//        if let callKitCompletionCallback = callKitCompletionCallback {
//            callKitCompletionCallback(true)
//        }
//
//        placeCallButton.setTitle("Hang Up", for: .normal)
//
//        toggleUIState(isEnabled: true, showCallControl: true)
//        stopSpin()
//        toggleAudioRoute(toSpeaker: true)
    }
    
    func call(call: Call, isReconnectingWithError error: Error) {
        NSLog("call:isReconnectingWithError:")
        print("DEBUG11 :: \(#function)")
//        placeCallButton.setTitle("Reconnecting", for: .normal)
//
//        toggleUIState(isEnabled: false, showCallControl: false)
    }
    
    func callDidReconnect(call: Call) {
        NSLog("callDidReconnect:")
        print("DEBUG11 :: \(#function)")
//        placeCallButton.setTitle("Hang Up", for: .normal)
//
//        toggleUIState(isEnabled: true, showCallControl: true)
    }
    
    func callDidFailToConnect(call: Call, error: Error) {
        NSLog("Call failed to connect: \(error.localizedDescription)")
        print("DEBUG11 :: \(#function)")
//        if let completion = callKitCompletionCallback {
//            completion(false)
//        }
//
//        if let provider = callKitProvider {
//            provider.reportCall(with: call.uuid!, endedAt: Date(), reason: CXCallEndedReason.failed)
//        }
//
//        callDisconnected(call: call)
    }
    
    func callDidDisconnect(call: Call, error: Error?) {
        print("DEBUG11 :: \(#function)")
        if let error = error {
            NSLog("Call failed: \(error.localizedDescription)")
        } else {
            NSLog("Call disconnected")
        }
//
//        if !userInitiatedDisconnect {
//            var reason = CXCallEndedReason.remoteEnded
//
//            if error != nil {
//                reason = .failed
//            }
//
//            if let provider = callKitProvider {
//                provider.reportCall(with: call.uuid!, endedAt: Date(), reason: reason)
//            }
//        }
//
//        callDisconnected(call: call)
    }
    
    func callDisconnected(call: Call) {
        print("DEBUG11 :: \(#function)")
//        if call == activeCall {
//            activeCall = nil
//        }
//
//        activeCalls.removeValue(forKey: call.uuid!.uuidString)
//
//        userInitiatedDisconnect = false
//
//        if playCustomRingback {
//            stopRingback()
//        }
//
//        stopSpin()
//        toggleUIState(isEnabled: true, showCallControl: false)
//        placeCallButton.setTitle("Call", for: .normal)
    }
    
    func callDidReceiveQualityWarnings(call: Call, currentWarnings: Set<NSNumber>, previousWarnings: Set<NSNumber>) {
        print("DEBUG11 :: \(#function)")
        /**
        * currentWarnings: existing quality warnings that have not been cleared yet
        * previousWarnings: last set of warnings prior to receiving this callback
        *
        * Example:
        *   - currentWarnings: { A, B }
        *   - previousWarnings: { B, C }
        *   - intersection: { B }
        *
        * Newly raised warnings = currentWarnings - intersection = { A }
        * Newly cleared warnings = previousWarnings - intersection = { C }
        */
        var warningsIntersection: Set<NSNumber> = currentWarnings
        warningsIntersection = warningsIntersection.intersection(previousWarnings)
        
        var newWarnings: Set<NSNumber> = currentWarnings
        newWarnings.subtract(warningsIntersection)
        if newWarnings.count > 0 {
//            qualityWarningsUpdatePopup(newWarnings, isCleared: false)
        }
        
        var clearedWarnings: Set<NSNumber> = previousWarnings
        clearedWarnings.subtract(warningsIntersection)
        if clearedWarnings.count > 0 {
//            qualityWarningsUpdatePopup(clearedWarnings, isCleared: true)
        }
    }

    
    func warningString(_ warning: Call.QualityWarning) -> String {
        switch warning {
        case .highRtt: return "high-rtt"
        case .highJitter: return "high-jitter"
        case .highPacketsLostFraction: return "high-packets-lost-fraction"
        case .lowMos: return "low-mos"
        case .constantAudioInputLevel: return "constant-audio-input-level"
        default: return "Unknown warning"
        }
    }
   
    
    // MARK: Ringtone
}

 
// MARK: - CXProviderDelegate

extension AdvisorAudioCallViewController: CXProviderDelegate {
    func providerDidReset(_ provider: CXProvider) {
        print("DEBUG11 :: \(#function)")
        NSLog("providerDidReset:")
        audioDevice.isEnabled = false
    }

    func providerDidBegin(_ provider: CXProvider) {
        print("DEBUG11 :: \(#function)")
        NSLog("providerDidBegin")
        audioDevice.isEnabled = true
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:didActivateAudioSession:")
        audioDevice.isEnabled = true
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        NSLog("provider:didDeactivateAudioSession:")
        audioDevice.isEnabled = false
    }

    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:timedOutPerformingAction:")
    }

    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:performStartCallAction:")
        audioDevice.isEnabled = true
        
//        toggleUIState(isEnabled: false, showCallControl: false)
//        startSpin()
//
//        provider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: Date())
//
//        performVoiceCall(uuid: action.callUUID, client: "") { success in
//            if success {
//                NSLog("performVoiceCall() successful")
//                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
//            } else {
//                NSLog("performVoiceCall() failed")
//            }
//        }
//
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:performAnswerCallAction:")
        
//        assert(action.callUUID == self.callInvite?.uuid)
        
//        audioDevice.isEnabled = false
//        audioDevice.block();
        self.performAnswerVoiceCall(uuid: action.callUUID) { (success) in
            if (success) {
                action.fulfill()
            } else {
                action.fail()
            }
        }
        
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        print("raviDebugVC : \(#function)")
        print("DEBUG11 :: \(#function)")
        NSLog("provider:performEndCallAction:")

        if let invite = activeCallInvites[action.callUUID.uuidString] {
            invite.reject()
            activeCallInvites.removeValue(forKey: action.callUUID.uuidString)
        } else if let call = activeCalls[action.callUUID.uuidString] {
            call.disconnect()
        } else {
            NSLog("Unknown UUID to perform end-call action with")
        }

        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:performSetHeldAction:")
        
//        if let call = activeCalls[action.callUUID.uuidString] {
//            call.isOnHold = action.isOnHold
//            action.fulfill()
//        } else {
//            action.fail()
//        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        print("DEBUG11 :: \(#function)")
        NSLog("provider:performSetMutedAction:")

//        if let call = activeCalls[action.callUUID.uuidString] {
//            call.isMuted = action.isMuted
//            action.fulfill()
//        } else {
//            action.fail()
//        }
    }

    
    // MARK: Call Kit Actions
    func performStartCallAction(uuid: UUID, handle: String) {
        print("DEBUG11 :: \(#function)")
//        guard let provider = callKitProvider else {
//            NSLog("CallKit provider not available")
//            return
//        }
//
//        let callHandle = CXHandle(type: .generic, value: handle)
//        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
//        let transaction = CXTransaction(action: startCallAction)
//
//        callKitCallController.request(transaction) { error in
//            if let error = error {
//                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
//                return
//            }
//
//            NSLog("StartCallAction transaction request successful")
//
//            let callUpdate = CXCallUpdate()
//
//            callUpdate.remoteHandle = callHandle
//            callUpdate.supportsDTMF = true
//            callUpdate.supportsHolding = true
//            callUpdate.supportsGrouping = false
//            callUpdate.supportsUngrouping = false
//            callUpdate.hasVideo = false
//
//            provider.reportCall(with: uuid, updated: callUpdate)
//        }
    }

    func reportIncomingCall(from: String, uuid: UUID) {
        print("DEBUG11 :: \(#function)")
        guard let provider = callKitProvider else {
            NSLog("CallKit provider not available")
            return
        }

        let callHandle = CXHandle(type: .generic, value: from)
        let callUpdate = CXCallUpdate()

        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = true
        callUpdate.supportsHolding = true
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false

        provider.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if let error = error {
                NSLog("Failed to report incoming call successfully: \(error.localizedDescription).")
            } else {
                NSLog("Incoming call successfully reported.")
            }
        }
    }

    func performEndCallAction(uuid: UUID) {

        let endCallAction = CXEndCallAction(call: uuid)
//        let transaction = CXTransaction(action: endCallAction)
//
//        callKitCallController.request(transaction) { error in
//            if let error = error {
//                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
//            } else {
//                NSLog("EndCallAction transaction request successful")
//            }
//        }
    }
    
    func performVoiceCall(uuid: UUID, client: String?, completionHandler: @escaping (Bool) -> Void) {
//        let connectOptions = ConnectOptions(accessToken: accessToken) { builder in
//            builder.params = [twimlParamTo: self.outgoingValue.text ?? ""]
//            builder.uuid = uuid
//        }
//
//        let call = TwilioVoiceSDK.connect(options: connectOptions, delegate: self)
//        activeCall = call
//        activeCalls[call.uuid!.uuidString] = call
//        callKitCompletionCallback = completionHandler
    }
    
    func performAnswerVoiceCall(uuid: UUID, completionHandler: @escaping (Bool) -> Void) {
        guard let callInvite = activeCallInvites[uuid.uuidString] else {
            NSLog("No CallInvite matches the UUID")
            return
        }

        let acceptOptions = AcceptOptions(callInvite: callInvite) { builder in
            builder.uuid = callInvite.uuid
        }

        let call = callInvite.accept(options: acceptOptions, delegate: self)
        print("SANDEEP CALL CONNECTED: ", call.uuid ?? "")
        activeCall = call
        activeCalls[call.uuid!.uuidString] = call
        callKitCompletionCallback = completionHandler

        activeCallInvites.removeValue(forKey: uuid.uuidString)

        guard #available(iOS 13, *) else {
            incomingPushHandled()
            return
        }
    }
}


// MARK: - AVAudioPlayerDelegate

extension ViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            NSLog("Audio player finished playing successfully");
        } else {
            NSLog("Audio player finished playing with some error");
        }
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        if let error = error {
            NSLog("Decode error occurred: \(error.localizedDescription)")
        }
    }
}

extension AdvisorAudioCallViewController: NotificationDelegate {
    func callInviteReceived(callInvite: CallInvite) {
        NSLog("callInviteReceived:")
        
        /**
         * The TTL of a registration is 1 year. The TTL for registration for this device/identity
         * pair is reset to 1 year whenever a new registration occurs or a push notification is
         * sent to this device/identity pair.
         */
//        UserDefaults.standard.set(Date(), forKey: kCachedBindingDate)
        
        let callerInfo: TVOCallerInfo = callInvite.callerInfo
        if let verified: NSNumber = callerInfo.verified {
            if verified.boolValue {
                NSLog("Call invite received from verified caller number!")
            }
        }
        
        let from = (callInvite.from ?? "Voice Bot").replacingOccurrences(of: "client:", with: "")
        
        // Always report to CallKit
        reportIncomingCall(from: from, uuid: callInvite.uuid)
        self.activeCallInvites[callInvite.uuid.uuidString] = callInvite
    }
    
    func cancelledCallInviteReceived(cancelledCallInvite: CancelledCallInvite, error: Error) {
        NSLog("cancelledCallInviteCanceled:error:, error: \(error.localizedDescription)")
//
//        guard let activeCallInvites = activeCallInvites, !activeCallInvites.isEmpty else {
//            NSLog("No pending call invite")
//            return
//        }
        
//        let callInvite = activeCallInvites.values.first { invite in invite.callSid == cancelledCallInvite.callSid }
        
//        if let callInvite = callInvite {
////            performEndCallAction(uuid: callInvite.uuid)
////            self.activeCallInvites.removeValue(forKey: callInvite.uuid.uuidString)
//        }
    }
}
extension AdvisorAudioCallViewController : PushKitEventDelegate {
    
    
    func credentialsUpdated(credentials: PKPushCredentials) {
        print("\(#function)")
      //  Ankur's Change
//                guard
//                    (registrationRequired() || UserDefaults.standard.data(forKey: kCachedDeviceToken) != credentials.token),let accessToken = fetchAccessToken()
//                else {
//                    return
//                }
        
        
        //Ankur's Change
        
        let accessToken = fetchAccessToken() ?? ""
        print("Assess Token New :: \(accessToken)")
        //        let accessToken = fetchAccessToken() ?? ""
        let cachedDeviceToken = credentials.token
        /*
         * Perform registration if a new device token is detected.
         */
        print("Ravi Debug register tocken : \(accessToken)")
        guard accessToken.count > 0, cachedDeviceToken.count > 0 else {
            return
        }
        TwilioVoiceSDK.register(accessToken: accessToken, deviceToken: cachedDeviceToken,completion: { error in
            if let error = error {
                print("An error occurred while registering: \(error.localizedDescription)")
            } else {
                print("Successfully registered for VoIP push notifications.")
                
                // Save the device token after successfully registered.
                
                UserDefaults.standard.setValue(cachedDeviceToken, forKey: "VOPIDeviceToken")
                UserDefaults.standard.setValue(accessToken, forKey: "VOIPAccessToken")
                
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(cachedDeviceToken, forKey: self.kCachedDeviceToken)
                
                /**
                 * The TTL of a registration is 1 year. The TTL for registration for this device/identity
                 * pair is reset to 1 year whenever a new registration occurs or a push notification is
                 * sent to this device/identity pair.
                 */
                UserDefaults.standard.set(Date(), forKey: self.kCachedBindingDate)
                //UserDefaults.standard.string(forKey: "")
                let AccessToken = UserDefaults.standard.data(forKey: "VOPIDeviceToken")
                let DeviceToken = UserDefaults.standard.value(forKey: "VOIPAccessToken")
                print("access token : \(String(describing: AccessToken)) and device token is :\(String(describing: DeviceToken))")
            }
        })
    }
    
    /**
     * The TTL of a registration is 1 year. The TTL for registration for this device/identity pair is reset to
     * 1 year whenever a new registration occurs or a push notification is sent to this device/identity pair.
     * This method checks if binding exists in UserDefaults, and if half of TTL has been passed then the method
     * will return true, else false.
     */
    func registrationRequired() -> Bool {
        print("\(#function)")
        guard
            let lastBindingCreated = UserDefaults.standard.object(forKey: self.kCachedBindingDate)
        else { return true }
        
        let date = Date()
        var components = DateComponents()
        components.setValue(self.kRegistrationTTLInDays/2, for: .day)
        let expirationDate = Calendar.current.date(byAdding: components, to: lastBindingCreated as! Date)!
        
        if expirationDate.compare(date) == ComparisonResult.orderedDescending {
            return false
        }
        return true;
    }
    func UnregisterVOIP() {
        print("\(#function)")
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                print("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                print("Successfully unregistered from VoIP push notifications.")
            }
        })
        
        
        //
        ////        let AccessToken = UserDefaults.standard.value(forKey: "VOPIDeviceToken")
        ////        let DeviceToken = UserDefaults.standard.value(forKey: "VOPIDeviceToken")
        //        print("access token : \(AccessToken) and device token is :\(DeviceToken)")
        //        guard let deviceToken = UserDefaults.standard.data(forKey: kCachedDeviceToken),
        //            let accessToken = fetchAccessToken() else { return }
        //        TwilioVoice.unregister(withAccessToken: accessToken, deviceToken: deviceToken, completion: { error in
        //            if let error = error {
        //                print("An error occurred while unregistering: \(error.localizedDescription)")
        //            } else {
        //                print("Successfully unregistered from VoIP push notifications.")
        //            }
        //        })
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
    func credentialsInvalidated() {
        print("\(#function)")
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                print("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                print("Successfully unregistered from VoIP push notifications.")
            }
        })
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
    
    func incomingPushReceived(payload: PKPushPayload) {
        print("\(#function)")
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
        // TwilioVoiceSDK.handleNotification(, delegate: self, delegateQueue: nil)
    }
    
    func incomingPushReceived(payload: PKPushPayload, completion: @escaping () -> Void) {
        print("\(#function)")
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
        
        if let version = Float(UIDevice.current.systemVersion), version < 13.0 {
            // Save for later when the notification is properly handled.
            self.incomingPushCompletionCallback = completion
        }
    }
    
    func incomingPushHandled() {
        guard let completion = self.incomingPushCompletionCallback else { return }
        
        self.incomingPushCompletionCallback = nil
        completion()
    }
    
    
    
}
