//
//  AdviserMyAccountViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
class AdviserMyAccountViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK: - Properties
    var categoriesData = [AdviserCategoriesArray]()
    var customTabBarController : CustomTabBarVC?
    var myProfileData = [AdviserMyProfile]()
    var categoryColorArray : [String] = [CategoryTypeColor.Category1.colorHexString(),
                                         CategoryTypeColor.Category2.colorHexString(),
                                         CategoryTypeColor.Category3.colorHexString()]
    //MARK: - IBOutlets
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet weak var lblUserName: MyProfileLabel!
    @IBOutlet weak var lblUserEmail: MyProfileLabel!
    
    @IBOutlet weak var lblUserContactNo: MyProfileLabel!
    @IBOutlet weak var tblMyProfile: UITableView!
    
    @IBOutlet weak var btnAudioChat: UIButton?
    @IBOutlet weak var btnVideoChat: UIButton?
    @IBOutlet weak var btnTextChat: UIButton?
    @IBOutlet weak var userImageView: myProfileImageView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
       
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        //self.customTabBarController?.viewDidLayoutSubviews()
        categoryColorArray = Array(repeating: categoryColorArray, count: 10)
      
        myProfileData.append(AdviserMyProfile(img: AdviserMyProfileCategory.ChangePassword.imageValue, name: AdviserMyProfileCategory.ChangePassword.value))
        myProfileData.append(AdviserMyProfile(img: AdviserMyProfileCategory.RatingsReview.imageValue, name: AdviserMyProfileCategory.RatingsReview.value))
        myProfileData.append(AdviserMyProfile(img: AdviserMyProfileCategory.settings.imageValue, name: AdviserMyProfileCategory.settings.value))
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        //customTabBarController?.showTabBar()
        //self.hidesBottomBarWhenPushed = true
        customTabBarController?.showTabBar()
        
        self.setData()
    }
    func getProfileData() {
        let adviserReqModel = getAdviserData()
        
        adviserReqModel.user_id = SingletonClass.sharedInstance.UserId
        Utilities.showHud()
        WebServiceSubClass.AdviserDetails(CategoryModel: adviserReqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if(status) {
                
                let loginModel = UserInfo.init(fromJson: json)
                let loginModelDetails = loginModel
                
                SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                userDefault.setUserData(objProfile: loginModelDetails)
               
                self.setData()
                
            }else {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    override func viewWillAppear(_ animated: Bool) {
        GetAvailabilty()
        self.view.layoutIfNeeded()
    }
    
    func setData(){
        setValue()
       
        setTopAvability()
        categoriesData.removeAll()
//        
//        let NewCategoryID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.categoryId.split(separator: ",")
//        
//        
//        NewCategoryID?.forEach({ (elementNewCategoryID) in
//            if SingletonClass.sharedInstance.categoryDataForAdviser?.category.count != 0 {
//                SingletonClass.sharedInstance.categoryDataForAdviser?.category.forEach({ (elementCategory) in
//                    if elementCategory.id == elementNewCategoryID
//                    {
//                        categoriesData.append(AdviserCategoriesArray(name: elementCategory.name, color: UIColor(hexString: categoryColorArray[categoriesData.count]).withAlphaComponent(0.25)))
//                    }
//                })
//            }
//        })
        
        if SingletonClass.sharedInstance.categoryDataForAdviser?.category.count != 0 {
            for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 6) - 1
            {
                let foundItems = SingletonClass.sharedInstance.LoginRegisterUpdateData?.availability.filter { $0.categoryId == SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id }
                if foundItems?.count != 0 {
                    for j in 0...(foundItems?.count ?? 0) - 1 {
                        
                       
                        if foundItems?[j].categoryId == SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id
                        {
                            
                            categoriesData.append(AdviserCategoriesArray(name: SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].name ?? "", color: UIColor(hexString: categoryColorArray[categoriesData.count]).withAlphaComponent(0.25)))
                            break
                        }
                    }
                }
    
            }
        }
//        SingletonClass.sharedInstance.adviserAudioChatOn = userDefault.bool(forKey: UserDefaultsKey.toggleAudioOn.rawValue)
//        SingletonClass.sharedInstance.adviserTextChatOn = userDefault.bool(forKey: UserDefaultsKey.toggleChatOn.rawValue)
//        SingletonClass.sharedInstance.adviserVideoChatOn = userDefault.bool(forKey: UserDefaultsKey.toggleVideoOn.rawValue)
        collectionViewCategories.reloadData()
    }
    
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func refereshProfileScreen() {
        setValue()
    }
    func setValue() {
//        userImageView.isSkeletonable = true
        userImageView.image = nil
       // imgProfile.showAnimatedSkeleton()
        //let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
       // userImageView.showAnimatedSkeleton()
        if let userdata = SingletonClass.sharedInstance.LoginRegisterUpdateData{
            lblUserName.text = userdata.profile.fullName ?? ""
            lblUserEmail.text = userdata.profile.email ?? ""
            lblUserContactNo.text = userdata.profile.phone ?? ""
            
            
            if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(userdata.profile.profilePicture ?? "")"
                userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
                userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
               
              
            }else{
                userImageView.image = UIImage.init(named: "user_dummy_profile")
               
            }
            
//            if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture != ""{
//                let strUrl = "\(APIEnvironment.profileBu)\(userdata.profile.profilePicture ?? "")"
//                let ImgURL = URL(string: strUrl)
//
//                userImageView.sd_imageTransition = .fade
//                userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
//                //userImageView.sd_setImage(with: ImgURL,  placeholderImage: UIImage())
//                userImageView.sd_setImage(with: ImgURL, completed: {_,_,_,_ in
//                    //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//                    //self.imgProfile.stopSkeletonAnimation()
//                       // self.userImageView.hideSkeleton()
//                    //})
////
//                })
////                userImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
////                userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
//
//            }else{
//                userImageView.image = UIImage.init(named: "user_dummy_profile")
//            }
        }
        
    
    }
    func setTopAvability() {
        print("Availability Audio :: \(SingletonClass.sharedInstance.adviserAudioChatOn)")
        print("Availability Video :: \(SingletonClass.sharedInstance.adviserVideoChatOn)")
        print("Availability Text :: \(SingletonClass.sharedInstance.adviserTextChatOn)")
        if SingletonClass.sharedInstance.adviserAudioChatOn {
            btnAudioChat?.isSelected = true
        } else {
            btnAudioChat?.isSelected = false
        }
        
        if SingletonClass.sharedInstance.adviserVideoChatOn {
            btnVideoChat?.isSelected = true
        } else {
            btnVideoChat?.isSelected = false
        }
        
        if SingletonClass.sharedInstance.adviserTextChatOn {
            btnTextChat?.isSelected = true
        } else {
            btnTextChat?.isSelected = false
        }
       
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: AppName,
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Open settings", style: .cancel, handler: { (alert) -> Void in

            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    func ShowMicroPhonePermissionAlert() {

            let alert = UIAlertController(title: AppName, message: "Please allow microphone usage from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
       
    }
    func checkCameraAccess() -> Bool {
        var permissionCheck: Bool = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            permissionCheck = false
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
         
            permissionCheck = true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    DispatchQueue.main.async {
                      
                        permissionCheck = true
                    }
                  
                } else {
                    permissionCheck = false
                }
            }
        @unknown default:
            print("Dafaukt casr < Image Picker class")
        }
        return permissionCheck
    }
    func checkMicPermission() -> Bool {

            var permissionCheck: Bool = false

        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
                permissionCheck = true
        case AVAudioSession.RecordPermission.denied:
                permissionCheck = false
        case AVAudioSession.RecordPermission.undetermined:
                AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                    if granted {
                        permissionCheck = true
                    } else {
                        permissionCheck = false
                    }
                })
            default:
                break
            }

            return permissionCheck
        }
    //MARK: - CollectionViewMethods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        return CGSize(width: ((categoriesData[indexPath.row].categoryName?.uppercased() ?? "").sizeOfString(usingFont: CustomFont.medium.returnFont(12)).width) + 30
, height: collectionViewCategories.frame.size.height)

    }
    
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCategories.dequeueReusableCell(withReuseIdentifier: categoriesCell.reuseIdentifier, for: indexPath) as! categoriesCell
        cell.BGView.backgroundColor = categoriesData[indexPath.row].categoryBgColor
        cell.categoryName.text = categoriesData[indexPath.row].categoryName?.uppercased()
        
        cell.categoryName.layer.cornerRadius = 15
        cell.BGView.layer.cornerRadius = 15
        cell.layer.cornerRadius = 15
        cell.clipsToBounds = true
        cell.BGView.clipsToBounds = true
        cell.categoryName.clipsToBounds = true
        return cell
    }
    
    //MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myProfileData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMyProfile.dequeueReusableCell(withIdentifier: myProfileCell.reuseIdentifier, for: indexPath) as! myProfileCell
        cell.categoryImage.image = myProfileData[indexPath.row].profileCategoryImage
        cell.categoryName.text = myProfileData[indexPath.row].profileCategoryName
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch  myProfileData[indexPath.row].profileCategoryName{
        case AdviserMyProfileCategory.RatingsReview.value:
           
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserRatingsReviewsViewController.storyboardID) as! AdviserRatingsReviewsViewController
           //
            controller.isFromCustomer = false
                controller.hidesBottomBarWhenPushed = true

            self.navigationController?.pushViewController(controller, animated: true)
          
          
            //self.hidesBottomBarWhenPushed = false
            break
        case AdviserMyProfileCategory.ChangePassword.value:
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserChangePasswordViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case AdviserMyProfileCategory.settings.value:
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserSettingsViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            break
        }
    }
    //MARK: - IBActions
    func MakeAModelForToggle(isChat: Bool, isAudio: Bool, isVideo: Bool,  completion: ((Bool) -> ())? = nil) {
        var ToggleModel : [AvailabilityToggleModel] = []
        ToggleModel.append(AvailabilityToggleModel(type: "audio", status: isAudio ? "1" : "0"))
        ToggleModel.append(AvailabilityToggleModel(type: "video", status: isVideo ? "1" : "0"))
        ToggleModel.append(AvailabilityToggleModel(type: "chat", status: isChat ? "1" : "0"))
        
        let productsDict = AvailabilityToggleModel.convertDataCartArrayToProductsDictionary(arrayDataCart: ToggleModel);
        
        let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
        let jsonString:String = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
        
        
        let reqModel = changeAvailibalityStatusReqModelWithAllToggle()
        reqModel.user_id = SingletonClass.sharedInstance.UserId
        reqModel.status = jsonString
        
        webServiceCallForToggleWithAllToggle(Model: reqModel, completion: completion)
    }
    
    @IBAction func btnEditProfileCLick(_ sender: Any) {
        
        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserProfileEditViewController.storyboardID)
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
   
    
    @IBAction func btnAudioChatClick(_ sender: UIButton) {
        if SingletonClass.sharedInstance.adviserAudioChatOn == true {
//            SingletonClass.sharedInstance.adviserAudioChatOn = false
            availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: false, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
        } else {
            if checkMicPermission() {
//                SingletonClass.sharedInstance.adviserAudioChatOn = true
                availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: !SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
            } else {
                ShowMicroPhonePermissionAlert()
            }
            
        }

//        MakeAModelForToggle()
//        setTopAvability()
        
    }
    @IBAction func btnVideoChatClick(_ sender: UIButton) {
       
        if SingletonClass.sharedInstance.adviserVideoChatOn == true {
//            SingletonClass.sharedInstance.adviserVideoChatOn = false
//            MakeAModelForToggle()
//            setTopAvability()
            availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: false)
        } else {
            if checkMicPermission() {
                if checkCameraAccess() {
//                    SingletonClass.sharedInstance.adviserVideoChatOn = true
//                    MakeAModelForToggle()
//                    setTopAvability()
                    availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: !SingletonClass.sharedInstance.adviserVideoChatOn)
                } else {
                    alertToEncourageCameraAccessInitially()
                }
            } else {
                ShowMicroPhonePermissionAlert()
            }
            
        }
        
    }
    @IBAction func btnTextChatClick(_ sender: UIButton) {
//        if SingletonClass.sharedInstance.adviserTextChatOn == true {
//            SingletonClass.sharedInstance.adviserTextChatOn = false
//        } else {
//            SingletonClass.sharedInstance.adviserTextChatOn = true
//
//        }
//
//        MakeAModelForToggle()
//        setTopAvability()
        availablityChanged(isChat: !SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
    }
    @IBAction func btnLogoutClick(_ sender: theamSubmitButton) {
//        appDel.navigateToLogin()
        showLogout()
        
    }
    //MARK: - API Calls
    private func availablityChanged(isChat: Bool, isAudio: Bool, isVideo: Bool) {
        Utilities.showHud()
        MakeAModelForToggle(isChat: isChat, isAudio: isAudio, isVideo: isVideo) {  [weak self] isSuccess in
            Utilities.hideHud()
            if isSuccess {
                SingletonClass.sharedInstance.adviserTextChatOn = isChat
                SingletonClass.sharedInstance.adviserAudioChatOn = isAudio
                SingletonClass.sharedInstance.adviserVideoChatOn = isVideo
                self?.setTopAvability()
            }
        }
    }
    func showLogout() {
        
        let alertController = UIAlertController(title: AppName,
                                                message: GlobalStrings.Alert_logout.rawValue,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Yes", style: .default){ _ in
            SingletonClass.sharedInstance.StopAdvisorTimer()
            appDel.performLogout()

        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
}
class AdviserCategoriesArray {
    var categoryName : String?
    var categoryBgColor : UIColor?
    init(name:String,color:UIColor) {
        self.categoryName = name
        self.categoryBgColor = color
    }
}
class AdviserMyProfile {
    var profileCategoryImage : UIImage?
    var profileCategoryName : String?
    init(img:UIImage,name:String) {
        self.profileCategoryImage = img
        self.profileCategoryName = name
    }
}
enum AdviserMyProfileCategory {
    case RatingsReview, ChangePassword , settings
    
    var value:String{
        switch self {
        case .RatingsReview:
            return "Ratings & Reviews"
        case .ChangePassword:
            return "Change Password"
        case .settings:
            return "Settings"
        
        }
    }
    var imageValue : UIImage {
        switch self {
        case .RatingsReview:
            return UIImage(named: "ic_RatingsReview")!
        case .ChangePassword:
            return UIImage(named: "ic_changePassword")!
        case .settings:
            return UIImage(named: "ic_Settings")!
    }
    }
}
extension AdviserMyAccountViewController {
    func webServiceCallForToggleWithAllToggle(Model:changeAvailibalityStatusReqModelWithAllToggle, completion: ((Bool) -> ())? = nil) {
        
        WebServiceSubClass.webServiceCallForToggleWithAllToggle(CategoryModel: Model, completion: { (response, status, error) in
            completion?(status)
            if status{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.setTopAvability()
                                    break
                                }
                            }
                        }
                        if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                            if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                                for controller in topVC as Array {
                                    if controller.isKind(of: AdviserMyAccountViewController.self) {
                                        let MyAccount = controller as! AdviserMyAccountViewController
                                        MyAccount.setTopAvability()
                                        break
                                    }
                                }
                            }
                        }
                    }
                })
                
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func GetAvailabilty() {
        let AvailabiltyDataModel = AvailabiltyData()
        AvailabiltyDataModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
      
        WebserviceForGetAvailability(reqModel: AvailabiltyDataModel)
    }
    
    func WebserviceForGetAvailability(reqModel:AvailabiltyData) {
        WebServiceSubClass.GetAvailabilityData(Data: reqModel, completion: {(json, status, response) in

            if status {
                print(json["availabile_for"].stringValue)
                let AvailibilityArray = json["availabile_for"].stringValue.split(separator: ",")
                if AvailibilityArray.contains("chat") {
                    SingletonClass.sharedInstance.adviserTextChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserTextChatOn = false
                }
                
                if AvailibilityArray.contains("audio") {
                    SingletonClass.sharedInstance.adviserAudioChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserAudioChatOn = false
                }
                
                if AvailibilityArray.contains("video") {
                    SingletonClass.sharedInstance.adviserVideoChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserVideoChatOn = false
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.setTopAvability()
                                    break
                                }
                            }
                        }
                        if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                            
                            if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                               if controller.isKind(of: AdviserMyAccountViewController.self) {
                                    let MyAccount = controller as! AdviserMyAccountViewController
                                    MyAccount.setTopAvability()
                                break
                                }
                            }
                            }
                        }
                    }
                })
//                self.setTopAvability()
                
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
}
extension Array {
  init(repeating: [Element], count: Int) {
    self.init([[Element]](repeating: repeating, count: count).flatMap{$0})
  }

  func repeated(count: Int) -> [Element] {
    return [Element](repeating: self, count: count)
  }
}
