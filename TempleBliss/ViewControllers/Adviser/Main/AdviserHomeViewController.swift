//
//  AdviserHomeViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SkeletonView
import SwiftyJSON
var currentRequestArrived : Bool = false
class AdviserHomeViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,ExpandableLabelDelegate {
    
    
    //MARK: -  Properties
    
    var TotalTimecounter = 0
    var totalMinutesTimer = 0
    var internalTimerForAdvisor: Timer?
    var videoThumbnailImage = UIImage()
    var TrainingSectionData : TranningSessionResModel?
    var states : Array<Bool>?
    var refreshControl = UIRefreshControl()
    var customTabBarController : CustomTabBarVC?
    
    var player: AVAudioPlayer?
    //MARK: -  IBOutlets
    
    @IBOutlet weak var btnAudioChat: UIButton?
    @IBOutlet weak var btnVideoChat: UIButton?
    @IBOutlet weak var btnTextChat: UIButton?
    @IBOutlet weak var tblHome: UITableView!
    
    //MARK: -  View Life Cycle Methods
    
    func OnSocket() {
        logMessage(messageText: "Voice Chat: OnSocket")
        if !SocketIOManager.shared.isSocketOn {
            SocketIOManager.shared.establishConnection()
            
        } else {
            CallOnAllSocketToListen()
        }
    }
    override func viewDidLayoutSubviews() {
        //        self.tblHome.reloadData()
        //        DispatchQueue.main.async {
        //            self.tblHome.reloadData()
        //        }
        
    }
    @objc func StartTimerForChat(){
        self.totalMinutesTimer = 1
        
        self.TotalTimecounter = 0
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.startTimer(withInterval: 1.0)
        })
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        logMessage(messageText: "Voice Chat: viewDidLoad")
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "StartTimerForChat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StartTimerForChat), name: NSNotification.Name(rawValue: "StartTimerForChat"), object: nil)
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil, userInfo: nil)
        
        OnSocket()
        setLocalization()
        setValue()
        DispatchQueue.main.async {
            self.RegisterForVOIP()
        }
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "StartTimer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StartTimerOfVideoCall), name: NSNotification.Name(rawValue: "StartTimer"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "StopTimer"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(StopTimerOfVideoCall), name: NSNotification.Name(rawValue: "StopTimer"), object: nil)
        
        WebServiceCallForTranningSession()
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        self.tblHome.refreshControl = refreshControl
        
        
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        WebServiceCallForTranningSession()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
            if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id != "" {
                GetAvailabilty()
            }
        }
        
        
        // self.customTabBarController?.tabBar.items?[2].isEnabled = false
        //self.customTabBarController?.tabBar.items?[1].isEnabled = false
        //  self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        
        //customTabBarController?.showTabBar()
        setTopAvability()
        //  self.view.layoutIfNeeded()
    }
    override func viewWillDisappear(_ animated: Bool) {
        internalTimerForAdvisor?.invalidate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //        tblHome.reloadData()
        customTabBarController?.showTabBar()
        self.view.layoutIfNeeded()
    }
    //MARK: -  Register for VOIP
    func RegisterForVOIP() {
        print("hello how are you?")
        
        appDel.pushKitEventDelegate = appDel
        appDel.initializePushKit()
        //self.initializePushKit()
        
    }
    
    //MARK: -  other method
    
    @objc func StopTimerOfVideoCall(){
        self.stopTimer()
    }
    
    @objc func StartTimerOfVideoCall(){
        self.totalMinutesTimer = 1
        
        self.TotalTimecounter = 0
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.startTimer(withInterval: 1.0)
//        })
    }
    func setLocalization() {
        
    }
    func setValue() {
    }
    func setTopAvability() {
        print("Availability Audio :: \(SingletonClass.sharedInstance.adviserAudioChatOn)")
        print("Availability Video :: \(SingletonClass.sharedInstance.adviserVideoChatOn)")
        print("Availability Text :: \(SingletonClass.sharedInstance.adviserTextChatOn)")
        if SingletonClass.sharedInstance.adviserAudioChatOn {
            btnAudioChat?.isSelected = true
        } else {
            btnAudioChat?.isSelected = false
        }
        
        if SingletonClass.sharedInstance.adviserVideoChatOn {
            btnVideoChat?.isSelected = true
        } else {
            btnVideoChat?.isSelected = false
        }
        
        if SingletonClass.sharedInstance.adviserTextChatOn {
            btnTextChat?.isSelected = true
        } else {
            btnTextChat?.isSelected = false
        }
    }
    func playVideo(url: URL) {
        
        let player = AVPlayer(url: url)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        present(playerViewController, animated: true, completion: {
            playerViewController.player?.play()
        })
    }
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: AppName,
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Open settings", style: .cancel, handler: { (alert) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    func ShowMicroPhonePermissionAlert() {
        
        let alert = UIAlertController(title: AppName, message: "Please allow microphone usage from settings", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    func checkCameraAccess() -> Bool {
        var permissionCheck: Bool = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            permissionCheck = false
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            
            permissionCheck = true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    DispatchQueue.main.async {
                        
                        permissionCheck = true
                    }
                    
                } else {
                    permissionCheck = false
                }
            }
        @unknown default:
            print("Dafaukt casr < Image Picker class")
        }
        return permissionCheck
    }
    func checkMicPermission() -> Bool {
        
        var permissionCheck: Bool = false
        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            permissionCheck = true
        case AVAudioSession.RecordPermission.denied:
            permissionCheck = false
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    permissionCheck = true
                } else {
                    permissionCheck = false
                }
            })
        default:
            break
        }
        
        return permissionCheck
    }
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        
        let asset: AVAsset = AVAsset(url: url)
        let imageGenerator = AVAssetImageGenerator(asset: asset)
        
        do {
            let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 3, timescale: 5) , actualTime: nil)
            return UIImage(cgImage: thumbnailImage)
        } catch let error {
            print(error)
            return nil
        }
    }
    //MARK: -  tblView Methods
    var expandedCells = Set<Int>()
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if TrainingSectionData == nil {
            return 6
        }
        return TrainingSectionData?.training.count ?? 0
        
    }
    func documentDirectoryPath() -> URL? {
        let path = FileManager.default.urls(for: .documentDirectory,
                                            in: .userDomainMask)
        return path.first
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if TrainingSectionData == nil {
            let cell = tblHome.dequeueReusableCell(withIdentifier: AdviserHomePdfCell.reuseIdentifier, for: indexPath) as! AdviserHomePdfCell
            cell.lblPDFTitle.text = ""
            cell.lblPdfDescription.text = ""
            cell.lblView.superview?.superview?.isHidden = true
            //            cell.lblPdfDescription.isHidden = true
            //            cell.lblView.superview?.superview?.isHidden = true
            
            cell.viewForSkeleton.isHidden = false
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            
            cell.lblPDFTitle.numberOfLines = 2
            
            cell.viewForSkeleton.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            // cell.lblPdfDescription.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            
            return cell
        } else {
            var cell = UITableViewCell()
            
            
            if TrainingSectionData?.training?[indexPath.row].videoUrl != "" {
                TrainingSectionData?.training?[indexPath.row].videoUrl = "assets/website/video/IMG_0634.mp4"
                let Videocell = tblHome.dequeueReusableCell(withIdentifier: AdviserHomeVideoCell.reuseIdentifier, for: indexPath) as! AdviserHomeVideoCell
                Videocell.lblTitle.text = TrainingSectionData?.training?[indexPath.row].title ?? ""
                
                Videocell.lblDescription.delegate = self
                Videocell.lblDescription.setLessLinkWith(lessLink: "show less", attributes: [.foregroundColor:colors.white.value,.font: CustomFont.medium.returnFont(15)], position: .left)
                
                Videocell.layoutIfNeeded()
                
                
                Videocell.lblDescription.shouldCollapse = true
                Videocell.lblDescription.textReplacementType = .word
                Videocell.lblDescription.numberOfLines = 3
                Videocell.lblDescription.collapsed = states?[indexPath.row] ?? true
                Videocell.lblDescription.text = TrainingSectionData?.training?[indexPath.row].descriptionField ?? ""
                
                //                Videocell.lblDescription.numberOfLines = 2
                //                Videocell.lblDescription.text = TrainingSectionData?.training?[indexPath.row].descriptionField ?? ""
                
                Videocell.VideoThumbImage.image = nil
                Videocell.VideoThumbImage.isSkeletonable = true
                
                let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
                Videocell.VideoThumbImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
                let strinForVideoPlay:String =  "\(APIEnvironment.profileBu)\(self.TrainingSectionData?.training[indexPath.row].videoUrl ?? "")"
                
                let urlString = strinForVideoPlay.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "" //This will fill the spaces with the %20
                
                let strUrl = URL(string: urlString)
                
                if let urlForThumbnail = strUrl {
                    AVAsset(url: urlForThumbnail).generateThumbnail { [weak self] (image) in
                        DispatchQueue.main.async {
                            guard let image = image else { return }
                            Videocell.VideoThumbImage.hideSkeleton()
                            self?.videoThumbnailImage = image
                            Videocell.VideoThumbImage.image = image
                        }
                    }
                    
                }
                
                
                cell = Videocell
                
            } else {
                let pdfCell = tblHome.dequeueReusableCell(withIdentifier: AdviserHomePdfCell.reuseIdentifier, for: indexPath) as! AdviserHomePdfCell
                
                pdfCell.viewForSkeleton.isHidden = true
                pdfCell.lblView.superview?.superview?.isHidden = false
                //pdfCell.lblView.superview?.superview?.isHidden = false
                
                pdfCell.lblPdfDescription.numberOfLines = 2
                pdfCell.lblPDFTitle.numberOfLines = 1
                
                //pdfCell.lblPdfDescription.isHidden = false
                
                pdfCell.lblPDFTitle.hideSkeleton()
                pdfCell.lblPdfDescription.hideSkeleton()
                
                pdfCell.lblPDFTitle.text = TrainingSectionData?.training?[indexPath.row].title ?? ""
                
                pdfCell.lblView.text = "View"
                
                
                pdfCell.lblPdfDescription.delegate = self
                pdfCell.lblPdfDescription.setLessLinkWith(lessLink: "show less", attributes: [.foregroundColor:colors.white.value,.font: CustomFont.medium.returnFont(15)], position: .left)
                
                pdfCell.layoutIfNeeded()
                
                pdfCell.lblPdfDescription.shouldCollapse = true
                pdfCell.lblPdfDescription.textReplacementType = .word
                pdfCell.lblPdfDescription.numberOfLines = 3
                
                pdfCell.lblPdfDescription.collapsed = states?[indexPath.row] ?? true
                pdfCell.lblPdfDescription.text = TrainingSectionData?.training?[indexPath.row].descriptionField ?? ""
                
                cell = pdfCell
            }
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if TrainingSectionData?.training?[indexPath.row].videoUrl != "" {
            
            let strinForVideoPlay:String =  "\(APIEnvironment.profileBu)\(TrainingSectionData?.training?[indexPath.row].videoUrl ?? "")"
            guard let strUrl = URL(string: strinForVideoPlay) else {
                return
            }
            playVideo(url: strUrl)
        }
        else {
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonWebViewViewController.storyboardID) as! CommonWebViewViewController
            controller.strNavTitle = TrainingSectionData?.training?[indexPath.row].title ?? ""
            controller.strUrl = "\(TrainingSectionData?.training?[indexPath.row].file ?? "")"
            print("\(APIEnvironment.profileBu)\(TrainingSectionData?.training?[indexPath.row].file ?? "")")
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if TrainingSectionData == nil {
            return 85
        }
        return UITableView.automaticDimension
    }
    
    //MARK: -  IBActions
    func MakeAModelForToggle(isChat: Bool, isAudio: Bool, isVideo: Bool,  completion: ((Bool) -> ())? = nil) {
        var ToggleModel : [AvailabilityToggleModel] = []
        ToggleModel.append(AvailabilityToggleModel(type: "audio", status: isAudio ? "1" : "0"))
        ToggleModel.append(AvailabilityToggleModel(type: "video", status: isVideo ? "1" : "0"))
        ToggleModel.append(AvailabilityToggleModel(type: "chat", status: isChat ? "1" : "0"))
        
        let productsDict = AvailabilityToggleModel.convertDataCartArrayToProductsDictionary(arrayDataCart: ToggleModel);
        
        let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
        let jsonString:String = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
        
        
        let reqModel = changeAvailibalityStatusReqModelWithAllToggle()
        reqModel.user_id = SingletonClass.sharedInstance.UserId
        reqModel.status = jsonString
        
        webServiceCallForToggleWithAllToggle(Model: reqModel, completion: completion)
    }
    
    
    @IBAction func btnAudioChatClick(_ sender: UIButton) {
        
        if SingletonClass.sharedInstance.adviserAudioChatOn == true {
            availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: false, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
        } else {
            if checkMicPermission() {
                availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: !SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
            } else {
                ShowMicroPhonePermissionAlert()
            }
            
        }

    }
    @IBAction func btnVideoChatClick(_ sender: UIButton) {
        
        if SingletonClass.sharedInstance.adviserVideoChatOn == true {
            availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: false)
        } else {
            if checkMicPermission() {
                if checkCameraAccess() {
                    availablityChanged(isChat: SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: !SingletonClass.sharedInstance.adviserVideoChatOn)

                } else {
                    alertToEncourageCameraAccessInitially()
                }
            } else {
                ShowMicroPhonePermissionAlert()
            }
            
        }
    }
    
    @IBAction func btnTextChatClick(_ sender: UIButton) {
        availablityChanged(isChat: !SingletonClass.sharedInstance.adviserTextChatOn, isAudio: SingletonClass.sharedInstance.adviserAudioChatOn, isVideo: SingletonClass.sharedInstance.adviserVideoChatOn)
    }
    
    private func availablityChanged(isChat: Bool, isAudio: Bool, isVideo: Bool) {
        Utilities.showHud()
        MakeAModelForToggle(isChat: isChat, isAudio: isAudio, isVideo: isVideo) {  [weak self] isSuccess in
            Utilities.hideHud()
            if isSuccess {
                SingletonClass.sharedInstance.adviserTextChatOn = isChat
                SingletonClass.sharedInstance.adviserAudioChatOn = isAudio
                SingletonClass.sharedInstance.adviserVideoChatOn = isVideo
                self?.setTopAvability()
            }
        }
    }
    @IBAction func btnContactUsClick(_ sender: UIButton) {
        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserContactUsViewController.storyboardID)
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    func willExpandLabel(_ label: ExpandableLabel) {
        tblHome.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tblHome)
        if let indexPath = tblHome.indexPathForRow(at: point) as IndexPath? {
            states?[indexPath.row] = false
            DispatchQueue.main.async { [weak self] in
                self?.tblHome.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tblHome.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tblHome.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tblHome)
        if let indexPath = tblHome.indexPathForRow(at: point) as IndexPath? {
            states?[indexPath.row] = true
            DispatchQueue.main.async { [weak self] in
                self?.tblHome.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tblHome.endUpdates()
    }
    //MARK: -  API Calls
    func GetAvailabilty() {
        let AvailabiltyDataModel = AvailabiltyData()
        AvailabiltyDataModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        
        WebserviceForGetAvailability(reqModel: AvailabiltyDataModel)
    }
    
    func WebserviceForGetAvailability(reqModel:AvailabiltyData) {
        WebServiceSubClass.GetAvailabilityData(Data: reqModel, completion: {(json, status, response) in
            
            if status {
                print(json["availabile_for"].stringValue)
                let AvailibilityArray = json["availabile_for"].stringValue.split(separator: ",")
                if AvailibilityArray.contains("chat") {
                    SingletonClass.sharedInstance.adviserTextChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserTextChatOn = false
                }
                
                if AvailibilityArray.contains("audio") {
                    SingletonClass.sharedInstance.adviserAudioChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserAudioChatOn = false
                }
                
                if AvailibilityArray.contains("video") {
                    SingletonClass.sharedInstance.adviserVideoChatOn = true
                    
                } else {
                    SingletonClass.sharedInstance.adviserVideoChatOn = false
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.setTopAvability()
                                    break
                                }
                            }
                        }
                        if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                            if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                                for controller in topVC as Array {
                                    if controller.isKind(of: AdviserMyAccountViewController.self) {
                                        let MyAccount = controller as! AdviserMyAccountViewController
                                        MyAccount.setTopAvability()
                                        break
                                    }
                                }
                                
                            }
                        }
                    }
                })
                //self.setTopAvability()
                
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    func webServiceCallForToggleWithAllToggle(Model:changeAvailibalityStatusReqModelWithAllToggle, completion: ((Bool) -> ())? = nil) {
        
        WebServiceSubClass.webServiceCallForToggleWithAllToggle(CategoryModel: Model, completion: { (response, status, error) in
            completion?(status)
            if status{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                    if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                        if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserHomeViewController.self) {
                                    let HomeVC = controller as! AdviserHomeViewController
                                    HomeVC.setTopAvability()
                                    break
                                }
                            }
                        }
                        if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                            if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                                for controller in topVC as Array {
                                    if controller.isKind(of: AdviserMyAccountViewController.self) {
                                        let MyAccount = controller as! AdviserMyAccountViewController
                                        MyAccount.setTopAvability()
                                        break
                                    }
                                }
                            }
                        }
                    }
                })
                
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func WebServiceCallForTranningSession() {
        
        WebServiceSubClass.TrainingSection(strParams: "", completion: { (response, status, error) in
            self.tblHome.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            
            if status{
                
                self.TrainingSectionData = TranningSessionResModel.init(fromJson: response)
                //                self.TrainingSectionData?.training.forEach({ (element) in
                //                    element.videoUrl = ""
                //                })
                
                DispatchQueue.main.async {
                    self.states = [Bool](repeating: true, count: self.TrainingSectionData?.training.count ?? 0)
                    DispatchQueue.main.async {
                        self.tblHome.reloadDataWithAutoSizingCellWorkAround()
                    }
                    
                }
                
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func WebServiceForGetNotes(singleResponse:JSON) {
        logMessage(messageText: "Voice Chat: WebServiceForGetNotes")
        let userID = "\(singleResponse["user_id"].int ?? 0)"
        let advisorID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        print("ATDebug :: \("\(advisorID)/\(userID)")")
        
        WebServiceSubClass.GetNotes(strParams: "\(advisorID)/\(userID)", completion: { (response, status, error) in
            if status{
                logMessage(messageText: "Voice Chat: WebServiceForGetNotes success")
                if response["notes"]["note"].stringValue != "" {
                    let AddNoteModel = SessionNoteReqModel()
                    AddNoteModel.booking_id = "\(singleResponse["booking_id"].int ?? 0)"
                    AddNoteModel.note = response["notes"]["note"].stringValue
                    self.webserviceCallForAddNotes(reqModel: AddNoteModel)
                    DispatchQueue.main.async {
                        self.GotoAdvisorMessageScreen(singleResponse: singleResponse, notes: response["notes"]["note"].stringValue)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.GotoAdvisorMessageScreen(singleResponse: singleResponse, notes: "")
                    }
                }
               
                
             //   let data = NotesResModel.init(fromJson: response)
                
             //
               
                
            }else{
                logMessage(messageText: "Voice Chat: WebServiceForGetNotes failed")
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func webserviceCallForAddNotes(reqModel:SessionNoteReqModel) {
        Utilities.showHud()
        WebServiceSubClass.AddNotes(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            
            if status {
              
            } else {
               
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
}



//MARK: - TimerForSession
extension AdviserHomeViewController {
    
    func startTimer(withInterval interval: Double) {
        DispatchQueue.main.async {
            print("Sandeep startTimer called1: ", self.internalTimerForAdvisor)
            if self.internalTimerForAdvisor != nil {
                self.internalTimerForAdvisor?.invalidate()
            }
            
            self.internalTimerForAdvisor = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(self.doJob), userInfo: nil, repeats: true)
            print("Sandeep startTimer called2: ", self.internalTimerForAdvisor)
        }
    }
    
    func stopTimer() {
        DispatchQueue.main.async {
            print("Sandeep startTimer called3: ", self.internalTimerForAdvisor)
            guard self.internalTimerForAdvisor != nil else {
                print("No timer active, start the timer before you stop it.")
                return
            }
            self.internalTimerForAdvisor?.invalidate()
        }
    }
    
    @objc func doJob() {
        TotalTimecounter += 1
        print(totalMinutesTimer)
        print(TotalTimecounter)
        if let topVC = UIApplication.topViewController() {
            if UIApplication.shared.applicationState == .active {
                if topVC.isKind(of: AdvisorMessageChatViewController.self) {
                    let vc : AdvisorMessageChatViewController = topVC as! AdvisorMessageChatViewController
                    let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
                    vc.lblShowTimer.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                } else if topVC.isKind(of: CallControllerViewController.self) {
                    let vc : CallControllerViewController = topVC as! CallControllerViewController
                    let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
                    vc.lblTimer?.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                } else if topVC.isKind(of: AdvisorAudioCallViewController.self) {
                    let vc : AdvisorAudioCallViewController = topVC as! AdvisorAudioCallViewController
                    let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
                    vc.lblTimer?.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                }
            }
            
        }
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (0, (seconds / 60), (seconds % 60))
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
}
extension AVAsset {
    
    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 4.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
class AdviserHomeVideoCell : UITableViewCell {
    
    var buttonPlayClick : (() -> ())?
    
    @IBOutlet weak var lblTitle: adviserHomeLabel!
    @IBOutlet weak var VideoThumbImage: UIImageView!
    @IBOutlet weak var btnPlayOutlet: UIButton!
    @IBOutlet weak var btnPlayPauseOutlet: UIButton!
    
    @IBAction func btnPlayPause(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            
        } else {
            sender.isSelected = true
        }
    }
    
    @IBOutlet weak var viewPlaying: viewViewClearBG!
    @IBAction func btnPlay(_ sender: Any) {
        
        if let buttonPlay = self.buttonPlayClick {
            buttonPlay()
        }
    }
    override func awakeFromNib() {
        viewPlaying.isHidden = true
        VideoThumbImage.contentMode = .scaleAspectFill
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        lblDescription.collapsed = true
        lblDescription.text = nil
    }
    
    @IBOutlet weak var lblDescription: AdvisorHomeScreenDescriptionLabel!
}
class AdviserHomePdfCell : UITableViewCell {
    
    @IBOutlet weak var viewForSkeleton: UIImageView!
    
    @IBOutlet weak var pdfMainView: UIView!
    @IBOutlet weak var lblView: adviserHomeLabel!
    @IBOutlet weak var lblPdfDescription: AdvisorHomeScreenDescriptionLabel!
    @IBOutlet weak var lblPDFTitle: adviserHomeLabel!
    
    @IBOutlet weak var ViewPDFMainView: viewViewClearBG!
    
    @IBOutlet weak var pdfDetailsMainView: viewViewClearBG!
    
    override func awakeFromNib() {
        
        lblPDFTitle.numberOfLines = 2
        lblPdfDescription.numberOfLines = 1
        
        viewForSkeleton.isSkeletonable = true
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        
        viewForSkeleton.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        
    }
    override func prepareForReuse() {
        
        super.prepareForReuse()
        lblPdfDescription.collapsed = true
        lblPdfDescription.text = nil
    }
}
