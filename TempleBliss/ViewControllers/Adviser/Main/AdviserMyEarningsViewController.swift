

//
//  AdviserMyEarningsViewController.swift
//  TempleBliss
//
//  Created by Apple on 30/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
class AdviserMyEarningsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate {

    //MARK: - Properties
//    var bookingDetails : [BookingDetail]?
//    var myEarningDetails : MyEarningDetail?
//
    
    @IBOutlet weak var HeightOfWithdrawButton: NSLayoutConstraint!
    var totalPagesLoad = 1
    
    var WholeResponseOfMyEarnings : myEarningsResModel?
    var WholeMyEarningsData : [BookingDetail] = []
    var MyEarningsData : [[BookingDetail]] = []
    var myEarningsShowAllData : [myEarnings]?
    var customTabBarController : CustomTabBarVC?
    let choosePaymentDropDown = DropDown()
    let pickerViewForAll = GeneralPickerView()
    var WithdrawTypeArray : [String] = [WithDrawOption.pending.ForDisplayName,WithDrawOption.complete.ForDisplayName,WithDrawOption.available.ForDisplayName]
    //MARK: - IBOutlets
  //  @IBOutlet weak var tblMyearningHistoryHeight: NSLayoutConstraint!
    @IBOutlet weak var withDrawView: BorderView!
    @IBOutlet weak var btnwithdraw: theamSubmitButton!
    //@IBOutlet weak var lblShowPendingForWithdraw: AdviserMyEarningsLabel!
   
    @IBOutlet weak var textFieldPendingForWithdraw: AdviserPageTextField!
    @IBOutlet weak var lblAvailableForWithdraw: MyCreditLabel!
    
    //@IBOutlet weak var btnShowDropDown: UIButton!
    @IBOutlet weak var tblMyEarningsHeight: NSLayoutConstraint!
    @IBOutlet weak var tblEarningsData: UITableView!
    @IBOutlet weak var tblEarningsHistory: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        tblEarningsHistory.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
        setLocalization()
        setValue()
        setupDelegateForPickerView()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "My Earnings", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        

        self.tblEarningsData.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        self.tblEarningsHistory.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
//        DispatchQueue.main.async { [self] in
//            tblMyEarningsHeight.constant = tblEarningsData.contentSize.height
//            tblMyearningHistoryHeight.constant = tblEarningsHistory.contentSize.height
//        }
        textFieldPendingForWithdraw.text = WithdrawTypeArray[0]
        
        tblEarningsHistory.delegate = self
        tblEarningsHistory.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        btnwithdraw.isHidden = true
        MyEarningsData = []
        self.tblEarningsHistory.reloadData()
        
        totalPagesLoad = 1
        getMyEarningsData()
        customTabBarController?.showTabBar()
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblEarningsHistory.layer.removeAllAnimations()
        //tblMyearningHistoryHeight.constant = tblEarningsHistory.contentSize.height
        
        tblEarningsData.layer.removeAllAnimations()
        tblMyEarningsHeight.constant = tblEarningsData.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }
//        if WithdrawTypeArray[pickerViewForAll.selectedRow(inComponent: 0)] == WithDrawOption.available.ForDisplayName {
//            self.btnwithdraw.isUserInteractionEnabled = true
//            self.btnwithdraw.alpha = 1.0
//            self.HeightOfWithdrawButton.constant = 54
//
//        } else {
//            self.btnwithdraw.isUserInteractionEnabled = false
//            self.btnwithdraw.alpha = 0.0
//            self.HeightOfWithdrawButton.constant = 0
//        }
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldPendingForWithdraw {
            textFieldPendingForWithdraw.inputView = pickerViewForAll
            textFieldPendingForWithdraw.inputAccessoryView = pickerViewForAll.toolbar
            
            
            
            let indexOfA = WithdrawTypeArray.firstIndex(of: textFieldPendingForWithdraw.text ?? "") ?? 0 // 0
            pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
            
            
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        pickerViewForAll.generalPickerDelegate = self
    }
    //MARK: - tbl view methods
    func numberOfSections(in tableView: UITableView) -> Int {
        switch tableView {
        case tblEarningsData:
            return 1
        case tblEarningsHistory:
            if MyEarningsData.count == 0 {
                return 1
            }
            else {
                return MyEarningsData.count
            }
        default:
            return 0
        }
            
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        switch tableView {
        case tblEarningsData:
            let headerView  = UIView()
            return headerView
        case tblEarningsHistory:
            if MyEarningsData.count == 0 {
                let headerView  = UIView()
                return headerView
            }
            else {
                switch textFieldPendingForWithdraw.text {
                case WithDrawOption.pending.ForDisplayName:
                    let headerView  = UIView()
                    return headerView
                case WithDrawOption.complete.ForDisplayName:
                    let cell = tblEarningsHistory.dequeueReusableCell(withIdentifier: AdvisorHeaderCell.reuseIdentifier) as! AdvisorHeaderCell
                    
                    cell.SessionDate.text = "Paypal Payout Fee \(Currency) \(String(format: "%.2f", ((MyEarningsData[section][0].paymentFee as NSString?)?.floatValue) ?? 0.00))"
                    return cell
                case WithDrawOption.available.ForDisplayName:
                    let headerView  = UIView()
                    return headerView
                    
                case .none:
                    let headerView  = UIView()
                    return headerView
                case .some(_):
                    let headerView  = UIView()
                    return headerView
                }
                
            }
        
        default:
            let headerView  = UIView()
            return headerView
        }
       
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        switch tableView {
        case tblEarningsData:
            return myEarningsShowAllData?.count ?? 0
        case tblEarningsHistory:
            if MyEarningsData.count == 0 {
                return 1
            } else {
                return MyEarningsData[section].count
            }
            
            
        default:
            return 0
        }
       
    }
    var isLoadingList : Bool = false
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){

            if MyEarningsData.count != 0 {
                self.isLoadingList = true
                let spinner = UIActivityIndicatorView(style: .white)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblEarningsHistory.bounds.width, height: CGFloat(44))

                self.tblEarningsHistory.tableFooterView = spinner
                self.tblEarningsHistory.tableFooterView?.isHidden = false
                LoadMoreData()
            }
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblEarningsHistory {
            if MyEarningsData.count == 0 {
                return 100
            } else {
                return UITableView.automaticDimension
            }
            
        }
        else {
            return UITableView.automaticDimension
        }
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if tableView == tblEarningsHistory {
//            if indexPath.row != 0 {
//                let lastItem = (MyEarningsData?.count ?? 0 ) - 1
//                if(indexPath.row) == lastItem {
//                    let spinner = UIActivityIndicatorView(style: .white)
//                    spinner.startAnimating()
//                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//                    self.tblEarningsHistory.tableFooterView = spinner
//                    self.tblEarningsHistory.tableFooterView?.isHidden = false
//                    LoadMoreData()
//                }
//            }
//
//        }
//
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tblEarningsData:
            let cell = tblEarningsData.dequeueReusableCell(withIdentifier: minutesPriceCell.reuseIdentifier, for: indexPath) as! minutesPriceCell
            cell.lblMinutesCategoryName.text = myEarningsShowAllData?[indexPath.row].categoryName
            cell.lblMinutesCategoryPrice.text = myEarningsShowAllData?[indexPath.row].categoryPrice
            return cell
        case tblEarningsHistory:
            if MyEarningsData.count != 0 {
                let cell = tblEarningsHistory.dequeueReusableCell(withIdentifier: MyEarningHistoryCell.reuseIdentifier, for: indexPath) as! MyEarningHistoryCell
                cell.customerName.text = MyEarningsData[indexPath.section][indexPath.row].customerName
               
                
             
                if MyEarningsData[indexPath.section][indexPath.row].sessionMinute == "01:00" || MyEarningsData[indexPath.section][indexPath.row].sessionMinute.components(separatedBy: ":")[0] == "00" {
                    cell.lblMinutes.text = "\(MyEarningsData[indexPath.section][indexPath.row].sessionMinute ?? "") Minute | \(MyEarningsData[indexPath.section][indexPath.row].type ?? "")"
                } else {
                    cell.lblMinutes.text = "\(MyEarningsData[indexPath.section][indexPath.row].sessionMinute ?? "") Minutes | \(MyEarningsData[indexPath.section][indexPath.row].type ?? "")"
                }
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = DateFormatterString.timeWithDate.rawValue
                
                let sessionDate:Date = dateFormatter.date(from: MyEarningsData[indexPath.section][indexPath.row].endTime ?? "") ?? Date()
               
                
                let dateToday:Date = SingletonClass.sharedInstance.TodayDate
                
               // cell.lblDaysBefore.text = "\(dateToday.offset(from: sessionDate)) ago"
                
                if (MyEarningsData[indexPath.section][indexPath.row].profilePicture ?? "") == "" {
                    cell.customerImageView.image = UIImage(named: "user_dummy_profile")
                } else {
                    
                    let imgURL = MyEarningsData[indexPath.section][indexPath.row].profilePicture ?? ""
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    cell.customerImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
                    
                    cell.customerImageView.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
               
                cell.lblDaysBefore.text = MyEarningsData[indexPath.section][indexPath.row].timeAgo ?? ""
                
                cell.lblPrice.text = "\(Currency) \(MyEarningsData[indexPath.section][indexPath.row].advisorCommission ?? "")"
                return cell
            } else {
                let NoDatacell = tblEarningsHistory.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
                switch textFieldPendingForWithdraw.text {
                case WithDrawOption.pending.ForDisplayName:
                    NoDatacell.imgNoData.image = UIImage(named: "")
                    NoDatacell.lblNoDataTitle.text = "Pending withdrawal not found"
                case WithDrawOption.complete.ForDisplayName:
                    NoDatacell.imgNoData.image = UIImage(named: "")
                    NoDatacell.lblNoDataTitle.text = "Completed withdrawal not found"
                case WithDrawOption.available.ForDisplayName:
                    NoDatacell.imgNoData.image = UIImage(named: "")
                    NoDatacell.lblNoDataTitle.text = "Available withdrawal not found"
                    
                case .none:
                    break
                case .some(_):
                    break
                }
                
                //NodataTitleText.ItemList
                
                return NoDatacell
            }
           
        default:
            return UITableViewCell()
        }
       
    }
    //MARK: - IBActions
    @IBAction func btnWithdrawCLick(_ sender: Any) {
        
        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: CommonPopup.storyboardID) as! CommonPopup
        controller.lblTextString = "Are you sure you want to withdraw \(Currency) \(self.WholeResponseOfMyEarnings?.myEarningDetails.availableForWithdraw ?? "0")?"
        controller.LblDescriptionText = "Note:- (\(Currency) \(String(format: "%.2f", ((WholeResponseOfMyEarnings?.myEarningDetails.paymentFee as NSString?)?.floatValue) ?? 0.00)) PayPal payout fee will apply in withdrawal)"
        controller.btnLeftString = "NO"
        controller.btnRightString = "YES"
        
        controller.leftbtnColor = UIColor(hexString: "#E24444")
        controller.rightbtnColor = UIColor(hexString: "#2CC168")
        
        controller.isLeftClickClosour = {
           
        }
        controller.isRightClickClosour = {
            self.WithDrawMoney()
        }
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .crossDissolve
        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
        
        
        
        
        
        
//        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserAddMoneyViewController.storyboardID) as! AdviserAddMoneyViewController
//        
//        //self.customTabBarController?.hideTabBar()
//        controller.isCancleClosour = {
//         
//           // self.dismiss(animated: true, completion: nil)
//        }
////        controller.isDelete = {
////
////            self.dismiss(animated: true, completion: nil)
////        }
//        if MyEarningsData?.myEarningDetails.availableForWithdraw == "" {
//            controller.penndingForWithDraw = MyEarningsData?.myEarningDetails.availableForWithdraw ?? "0"
//        } else {
//            controller.penndingForWithDraw = "0"
//        }
//        
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.modalPresentationStyle = .overCurrentContext
//        navigationController.modalTransitionStyle = .crossDissolve
//        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
    }
 
    
    //MARK: - API Calls


}
class MyEarningsCell : UITableViewCell {
    
    @IBOutlet weak var lblMinutesCategoryName: buyMinutesLabel!
    @IBOutlet weak var lblMinutesCategoryPrice: buyMinutesLabel!
}
class myEarnings {
    var categoryName : String?
    var categoryPrice : String?
    init(name:String,price:String) {
        self.categoryName = name
        self.categoryPrice = price
    }
}
class MyEarningHistoryCell : UITableViewCell {
    
    @IBOutlet weak var lblPrice: AdviserMyEarningsLabel!
    @IBOutlet weak var lblMinutes: AdviserMyEarningsLabel!
    @IBOutlet weak var lblDaysBefore: AdviserMyEarningsLabel!
    @IBOutlet weak var customerImageView: UIImageView!
    @IBOutlet weak var customerName: AdviserMyEarningsLabel!
}
extension AdviserMyEarningsViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
//        let item = WithdrawTypeArray[pickerViewForAll.selectedRow(inComponent: 0)]
//
//        self.textFieldPendingForWithdraw.text = item
//
//        self.textFieldPendingForWithdraw.resignFirstResponder()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return WithdrawTypeArray.count
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return WithdrawTypeArray[row]
    }
    
}
extension AdviserMyEarningsViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        let item = WithdrawTypeArray[pickerViewForAll.selectedRow(inComponent: 0)]
        
        self.textFieldPendingForWithdraw.text = item
      
        self.textFieldPendingForWithdraw.resignFirstResponder()
        
        if WithdrawTypeArray[pickerViewForAll.selectedRow(inComponent: 0)] == WithDrawOption.available.ForDisplayName {
            self.btnwithdraw.isUserInteractionEnabled = true
            self.btnwithdraw.alpha = 1.0
            self.HeightOfWithdrawButton.constant = 54

        } else {
            self.btnwithdraw.isUserInteractionEnabled = false
            self.btnwithdraw.alpha = 0.0
            self.HeightOfWithdrawButton.constant = 0
        }
        
        
        MyEarningsData = []
        totalPagesLoad = 1
        
        getMyEarningsData()
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension AdviserMyEarningsViewController {
    func getMyEarningsData() {
        let MySessionModel = AdvisorMyEarningsReqModel()
           
        switch textFieldPendingForWithdraw.text {
        case WithDrawOption.pending.ForDisplayName:
            MySessionModel.filter = WithDrawOption.pending.forapiName
        case WithDrawOption.complete.ForDisplayName:
            MySessionModel.filter = WithDrawOption.complete.forapiName
        case WithDrawOption.available.ForDisplayName:
            MySessionModel.filter = WithDrawOption.available.forapiName
       
        case .none:
            break
        case .some(_):
            break
        }
        
        MySessionModel.page = "\(totalPagesLoad)"
        MySessionModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        webserviceForMySession(reqModel: MySessionModel)
    }
    func webserviceForMySession(reqModel:AdvisorMyEarningsReqModel) {
        self.tblEarningsHistory.isHidden = true
        Utilities.showHud()
        WebServiceSubClass.AdvisorMyEarnings(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            self.tblEarningsHistory.isHidden = false
            if status {
                let EarningsResModel = myEarningsResModel.init(fromJson: json)
                self.WholeResponseOfMyEarnings = EarningsResModel
                self.MyEarningsData = []
                self.WholeMyEarningsData.removeAll()
                self.WholeMyEarningsData.append(contentsOf: EarningsResModel.bookingDetails)
                self.myEarningsShowAllData = []
                
                if EarningsResModel.myEarningDetails.totalEarning == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Earnings", price: "\(Currency) 0.00"))

                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Earnings", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.totalEarning as NSString?)?.floatValue) ?? 0.00))"))
                }
                if EarningsResModel.myEarningDetails.totalWithdrawedAmount == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Withdrawal", price: "\(Currency) 0.00"))
                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Withdrawal", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.totalWithdrawedAmount as NSString?)?.floatValue) ?? 0.00))"))
                }
                if EarningsResModel.myEarningDetails.pendingWithdrawAmount == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Pending Withdrawal", price: "\(Currency) 0.00"))
                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Pending Withdrawal", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.pendingWithdrawAmount as NSString?)?.floatValue) ?? 0.00))"))
                }
                self.WholeMyEarningsData.forEach({ element in
                    
                })
                self.lblAvailableForWithdraw.text = "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.availableForWithdraw as NSString?)?.floatValue) ?? 0.00))"
             
                self.btnwithdraw.isHidden = false
                let dictionary = Dictionary(grouping: self.WholeMyEarningsData, by: { (element: BookingDetail) in
                    return element.paymentDate
                })
                
                dictionary.values.forEach { element in
                    self.MyEarningsData.append(element)
                }
                
                
                if reqModel.filter == WithDrawOption.available.forapiName {
                    print("ATDebug :: \(self.WholeMyEarningsData.count ?? 0)")
                    if self.WholeMyEarningsData.count != 0 {
                        self.btnwithdraw.isUserInteractionEnabled = true
                        self.btnwithdraw.alpha = 1.0
                        self.HeightOfWithdrawButton.constant = 54
                    } else {
                        self.btnwithdraw.isUserInteractionEnabled = false
                         self.btnwithdraw.alpha = 0.0
                        self.HeightOfWithdrawButton.constant = 0
                    }
                } else {
                    self.btnwithdraw.isUserInteractionEnabled = false
                     self.btnwithdraw.alpha = 0.0
                    self.HeightOfWithdrawButton.constant = 0
                }
                
                self.tblEarningsHistory.reloadData()
                self.tblEarningsData.reloadData()
                
                
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}

extension AdviserMyEarningsViewController {
    func WithDrawMoney() {
        let withdrawModel = withDrawMoneyReqModel()
        withdrawModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        withdrawModel.amount =  WholeResponseOfMyEarnings?.myEarningDetails.availableForWithdraw ?? "0"
        webserviceForWithDraw(reqModel: withdrawModel)
    }
    func webserviceForWithDraw(reqModel:withDrawMoneyReqModel) {
        Utilities.showHud()
        WebServiceSubClass.AdvisorWithDraw(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                let alert = UIAlertController(title: AppInfo.appName, message: json["message"].string ?? "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                    self.getMyEarningsData()
                }))
                self.present(alert, animated: true, completion: nil)
                
                 
            } else {
                
                Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
            }
        })
    }
}
extension AdviserMyEarningsViewController {
    func LoadMoreData() {
        let MySessionModel = AdvisorMyEarningsReqModel()
           
        switch textFieldPendingForWithdraw.text {
        case WithDrawOption.pending.ForDisplayName:
            MySessionModel.filter = WithDrawOption.pending.forapiName
        case WithDrawOption.complete.ForDisplayName:
            MySessionModel.filter = WithDrawOption.complete.forapiName
        case WithDrawOption.available.ForDisplayName:
            MySessionModel.filter = WithDrawOption.available.forapiName
         
        case .none:
            break
        case .some(_):
            break
        }
     
        MySessionModel.page = "\(totalPagesLoad + 1)"
        MySessionModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        webserviceForLoadMoreData(reqModel: MySessionModel)
    }
    func webserviceForLoadMoreData(reqModel:AdvisorMyEarningsReqModel) {
       
        WebServiceSubClass.AdvisorMyEarnings(addCategory: reqModel, completion: {(json, status, response) in
            self.tblEarningsHistory.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                let EarningsResModel = myEarningsResModel.init(fromJson: json)
                self.WholeResponseOfMyEarnings = EarningsResModel
                if EarningsResModel.bookingDetails.count != 0 {
                    self.totalPagesLoad = self.totalPagesLoad + 1
                }
                self.MyEarningsData = []
                self.WholeMyEarningsData.append(contentsOf: EarningsResModel.bookingDetails)
                let dictionary = Dictionary(grouping: self.WholeMyEarningsData, by: { (element: BookingDetail) in
                    return element.paymentDate
                })
                
                dictionary.values.forEach { element in
                    self.MyEarningsData.append(element)
                }
                self.myEarningsShowAllData = []
                
                if EarningsResModel.myEarningDetails.totalEarning == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Earnings", price: "\(Currency) 0.00"))

                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Earnings", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.totalEarning as NSString?)?.floatValue) ?? 0.00))"))
                }
                if EarningsResModel.myEarningDetails.totalWithdrawedAmount == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Withdrawal", price: "\(Currency) 0.00"))
                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Total Withdrawal", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.totalWithdrawedAmount as NSString?)?.floatValue) ?? 0.00))"))
                }
                if EarningsResModel.myEarningDetails.pendingWithdrawAmount == "" {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Pending Withdrawal", price: "\(Currency) 0.00"))
                }
                else {
                    self.myEarningsShowAllData?.append(myEarnings(name: "Pending Withdrawal", price: "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.pendingWithdrawAmount as NSString?)?.floatValue) ?? 0.00))"))
                }
                
                
                self.lblAvailableForWithdraw.text = "\(Currency) \(String(format: "%.2f", ((EarningsResModel.myEarningDetails.availableForWithdraw as NSString?)?.floatValue) ?? 0.00))"
                self.tblEarningsHistory.reloadData()
                self.tblEarningsData.reloadData()
                self.btnwithdraw.isHidden = false
//                if self.MyEarningsData?.count != 0 {
//                    self.btnwithdraw.isUserInteractionEnabled = true
//                    self.btnwithdraw.alpha = 1.0
//                    self.HeightOfWithdrawButton.constant = 54
//                } else {
//                    self.btnwithdraw.isUserInteractionEnabled = false
//                     self.btnwithdraw.alpha = 0.0
//                    self.HeightOfWithdrawButton.constant = 0
//                }
                if reqModel.filter == WithDrawOption.available.forapiName {
                    print("ATDebug :: \(self.WholeMyEarningsData.count)")
                    if self.WholeMyEarningsData.count != 0 {
                        self.btnwithdraw.isUserInteractionEnabled = true
                        self.btnwithdraw.alpha = 1.0
                        self.HeightOfWithdrawButton.constant = 54
                    } else {
                        self.btnwithdraw.isUserInteractionEnabled = false
                         self.btnwithdraw.alpha = 0.0
                        self.HeightOfWithdrawButton.constant = 0
                    }
                } else {
                    self.btnwithdraw.isUserInteractionEnabled = false
                     self.btnwithdraw.alpha = 0.0
                    self.HeightOfWithdrawButton.constant = 0
                }
//                if EarningsResModel.myEarningDetails.availableForWithdraw ?? "0.00" == "0.00" {
//                    self.btnwithdraw.isUserInteractionEnabled = false
//                     self.btnwithdraw.alpha = 0.0
//                    self.HeightOfWithdrawButton.constant = 0
//
//                } else {
//                    self.btnwithdraw.isUserInteractionEnabled = true
//                    self.btnwithdraw.alpha = 1.0
//                    self.HeightOfWithdrawButton.constant = 54
//                }
                
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}

//MARK: - enum
enum WithDrawOption {
    case pending , complete , available
    
    var forapiName:String {
        switch self {
        case .pending:
            return "pending"
        case .complete:
            return "complete"
        case .available:
            return "available"
        }
    }
    var ForDisplayName:String {
        switch self {
        case .pending:
            return "Pending for withdrawal"
        case .complete:
            return "Completed withdrawal"
        case .available:
            return "Available for withdrawal"
        }
    }
       
}

