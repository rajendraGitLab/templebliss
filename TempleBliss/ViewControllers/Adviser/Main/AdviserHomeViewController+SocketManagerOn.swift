//
//  AdviserHomeViewController+SocketManagerOn.swift
//  TempleBliss
//
//  Created by Apple on 12/07/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import SkeletonView
import SwiftyJSON
//MARK: - AllSocketOnMethods
extension AdviserHomeViewController {
    
    //MARK: - CallOnAllSocketToListen
    
    func CallOnAllSocketToListen() {
        logMessage(messageText: "Voice Chat: CallOnAllSocketToListen")
        print("ATDebug :: Advisor Home \(#function)")
        OnForVideoCallAdvisor()
        OnBuyMoreMintues()
        AcceptRequest()
        //BookingRequst()
        availibilitySwitchOff()
        voice_call_advisor()
        OnEndSession()
        resume_timer()
        stop_timer()
        RequestRejectDisconnect()
        connect_advisor_interval()
    }
    
    //MARK: - connect_advisor_interval
    func connect_advisor_interval() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.connect_advisor_interval.rawValue) { (response) in
            
//            print(#function)
//            print(response)
//            print("ATDebug :: Advisor Home \(#function)")
            
        }
    }
    //MARK: -  OnBuyMoreMintues()
    
    func OnBuyMoreMintues() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.buy_more_minute.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CallControllerViewController.self) {
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            let alert = UIAlertController(title: AppInfo.appName , message: singleResponse["message"].string, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                            topVC.present(alert, animated: true, completion: nil)
                        }
                    }
                } else if topVC.isKind(of: AdviserCommonPopupViewController.self) {
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            let alert = UIAlertController(title: AppInfo.appName , message: singleResponse["message"].string, preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                            topVC.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                } else {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    let alert = UIAlertController(title: AppInfo.appName , message: singleResponse["message"].string, preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                                    
                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                let alert = UIAlertController(title: AppInfo.appName , message: singleResponse["message"].string, preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in }))
                                UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                                
                            }
                        }
                    }
                }
            }
            
        }
    }
    
    //MARK: -  BookingRequst()
    func playSound() {

        guard let url = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }
            player.numberOfLoops = -1

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
   
    
    func BookingRequst() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.booking_request.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            print("ATDebug :: Advisor Home Request Reject Status \(currentRequestArrived)")
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdvisorRequestAcceptViewController.self) {
                    currentRequestArrived = true

                } else {
                    currentRequestArrived = false
                }
            }
            if currentRequestArrived {
                if let arrResponse = response.array {
                    if let singleResponse = arrResponse.first {
                        if SocketIOManager.shared.socket.status == .connected {
                            let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                        }
                    }
                }
            } else {
                currentRequestArrived = true
                if let topVC = UIApplication.topViewController() {
//                    let systemSoundID: SystemSoundID = 1315
//                    AudioServicesPlaySystemSound (systemSoundID)
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {

                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {

                                    if singleResponse["type"].string?.lowercased() ?? "" == "audio" || singleResponse["type"].string?.lowercased() ?? "" == "video" {
                                        if SocketIOManager.shared.socket.status == .connected {
                                            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                            currentRequestArrived = false

                                        }
                                    } else {
                                        self.playSound()
                                        let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                                        controller.closourForBtnAccept = {
                                            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                            self.player?.stop()
                                            controller.dismiss(animated: true, completion: nil)
                                            if SocketIOManager.shared.socket.status == .connected {
                                                let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                                currentRequestArrived = false

                                            }
                                        }
                                        controller.closourForBtnReject = {
                                            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                            self.player?.stop()
                                            controller.dismiss(animated: true, completion: nil)
                                            if SocketIOManager.shared.socket.status == .connected {
                                                let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                                SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                                currentRequestArrived = false
                                            }
                                        }

                                        controller.customerName = singleResponse["username"].string ?? ""
                                        controller.totalMinutes = singleResponse["minute"].string ?? ""
                                        controller.communicationType = singleResponse["type"].string ?? ""

                                        controller.modalPresentationStyle = .overCurrentContext
                                        controller.modalTransitionStyle = .crossDissolve
                                        appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                                    }

                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                if singleResponse["type"].string?.lowercased() ?? "" == "audio" || singleResponse["type"].string?.lowercased() ?? "" == "video" {
                                    if SocketIOManager.shared.socket.status == .connected {
                                        let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                        SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)

                                        currentRequestArrived = false

                                    }

                                } else {
                                    self.playSound()
                                    let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                                    controller.closourForBtnAccept = {
                                        AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                        self.player?.stop()
                                        controller.dismiss(animated: true, completion: nil)
                                        if SocketIOManager.shared.socket.status == .connected {
                                            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)

                                            currentRequestArrived = false

                                        }
                                    }
                                    controller.closourForBtnReject = {
                                        AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
                                        self.player?.stop()
                                        controller.dismiss(animated: true, completion: nil)

                                        if SocketIOManager.shared.socket.status == .connected {
                                            let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                            currentRequestArrived = false
                                        }  
                                    }
                                    controller.customerName = singleResponse["username"].string ?? ""
                                    controller.totalMinutes = singleResponse["minute"].string ?? ""
                                    controller.communicationType = singleResponse["type"].string ?? ""

                                    controller.modalPresentationStyle = .overCurrentContext
                                    controller.modalTransitionStyle = .crossDissolve
                                    appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                                }


                            }
                        }
                    }
                }
            }
            
        }
    }
    
    /*
    
    
    func BookingRequst() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.booking_request.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            print("ATDebug :: Advisor Home Request Reject Status \(currentRequestArrived)")
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdvisorRequestAcceptViewController.self) {
                    currentRequestArrived = true
    
                } else {
                    currentRequestArrived = false
                }
            }
            if currentRequestArrived {
                if let arrResponse = response.array {
                    if let singleResponse = arrResponse.first {
                        if SocketIOManager.shared.socket.status == .connected {
                            let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                        }
                    }
                }
            } else {
                currentRequestArrived = true
                if let topVC = UIApplication.topViewController() {
                    let systemSoundID: SystemSoundID = 1315
                    AudioServicesPlaySystemSound (systemSoundID)
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    
                                    let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                                    controller.closourForBtnAccept = {
                                        controller.dismiss(animated: true, completion: nil)
                                        if SocketIOManager.shared.socket.status == .connected {
                                            let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                            currentRequestArrived = false
                                            
                                        }
                                    }
                                    controller.closourForBtnReject = {
                                        controller.dismiss(animated: true, completion: nil)
                                        if SocketIOManager.shared.socket.status == .connected {
                                            let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                            SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                            currentRequestArrived = false
                                        }
                                    }
                                    
                                    controller.customerName = singleResponse["username"].string ?? ""
                                    controller.totalMinutes = singleResponse["minute"].string ?? ""
                                    controller.communicationType = singleResponse["type"].string ?? ""
                                    
                                    controller.modalPresentationStyle = .overCurrentContext
                                    controller.modalTransitionStyle = .crossDissolve
                                    appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
    
                                    
                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                
                                let controller:AdvisorRequestAcceptViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorRequestAcceptViewController.storyboardID) as! AdvisorRequestAcceptViewController
                                controller.closourForBtnAccept = {
                                    controller.dismiss(animated: true, completion: nil)
                                    if SocketIOManager.shared.socket.status == .connected {
                                        let AcceptBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                        SocketIOManager.shared.socketEmit(for: socketApiKeys.request_accept.rawValue, with: AcceptBookingRequest)
                                        
                                        currentRequestArrived = false
                                        
                                    }
                                }
                                controller.closourForBtnReject = {
                                    controller.dismiss(animated: true, completion: nil)
                                    
                                    if SocketIOManager.shared.socket.status == .connected {
                                        let RejectBookingRequest:[String:Any] = ["user_id": Int(singleResponse["user_id"].int ?? 0) , "booking_id":Int(singleResponse["booking_id"].int ?? 0), "advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"type": singleResponse["type"].string ?? ""]
                                        SocketIOManager.shared.socketEmit(for: socketApiKeys.request_reject.rawValue, with: RejectBookingRequest)
                                        currentRequestArrived = false
                                    }
                                }
                                controller.customerName = singleResponse["username"].string ?? ""
                                controller.totalMinutes = singleResponse["minute"].string ?? ""
                                controller.communicationType = singleResponse["type"].string ?? ""
                                
                                controller.modalPresentationStyle = .overCurrentContext
                                controller.modalTransitionStyle = .crossDissolve
                                appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
                                
                            }
                        }
                    }
                }
            }
            
        }
    }
    */
    //MARK: -  voice_call_advisor()
    
    func voice_call_advisor() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.voiceCallAdvisor.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
        }
    }
    //MARK: -  AcceptRequest()
    
    func AcceptRequest() {
        logMessage(messageText: "Voice Chat: AcceptRequest")
        // Komal
        SocketIOManager.shared.socketCall(for: socketApiKeys.request_accept.rawValue) { (response) in
            if let vc = UIApplication.topViewController(), !vc.isKind(of: AdvisorMessageChatViewController.self) {
                self.acceptRequestHandler(response)
            } else {
                print("else case")
            }
        }
    }
    
    func acceptRequestHandler(_ response: JSON) {
        logMessage(messageText: "Voice Chat: request_accept event received")

        print(#function)
        print("Advisor Screen",response)
        print("ATDebug :: Advisor Home \(#function)")
        if let topVC = UIApplication.topViewController() {
            if topVC.isModal {
                logMessage(messageText: "Voice Chat: top vc is modal")
                topVC.dismiss(animated: true, completion: {
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            if String(singleResponse["type"].string ?? "") == "chat" {
                                if String(singleResponse["status"].string ?? "") == "accepted" {
                                    self.WebServiceForGetNotes(singleResponse: singleResponse)
                                } else if String(singleResponse["status"].string ?? "") == "rejected" {
                                    print("else case")
                                }
                            }else  if String(singleResponse["type"].string ?? "") == "audio" {
                                SingletonClass.sharedInstance.AdvisorAudioCallData = singleResponse
                            } else  if String(singleResponse["type"].string ?? "") == "video" {
                                let dataForShare:[String:Any] = ["message":singleResponse["message"].string ?? "" ,"user_id":singleResponse["user_id"].int ?? 0,"room":singleResponse["room"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"token":singleResponse["receiver_token"].string ?? "","name":singleResponse["customer_name"].string ?? "","receiver_id":singleResponse["advisor_id"].int ?? 0]
                                self.navigateToCallController(withData: dataForShare)
                            }
                        }
                    }
                })
                
                
            } else {
                logMessage(messageText: "Voice Chat: top vc is not modal")
                if let arrResponse = response.array {
                    if let singleResponse = arrResponse.first {
                        if String(singleResponse["type"].string ?? "") == "chat" {
                            if String(singleResponse["status"].string ?? "") == "accepted" {
                                self.WebServiceForGetNotes(singleResponse: singleResponse)
                            } else if String(singleResponse["status"].string ?? "") == "rejected" {
                                
                            }
                        } else  if String(singleResponse["type"].string ?? "") == "audio" {
                            print("qwqwqw")
                            print(singleResponse)
                            SingletonClass.sharedInstance.AdvisorAudioCallData = singleResponse
                        } else  if String(singleResponse["type"].string ?? "") == "video" {
                            // Sandeep Suthar (ToDo check)
//                                let dataForShare:[String:Any] = ["message":singleResponse["message"].string ?? "" ,"user_id":singleResponse["user_id"].int ?? 0,"room":singleResponse["room"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"token":singleResponse["receiver_token"].string ?? "","name":singleResponse["customer_name"].string ?? "","receiver_id":singleResponse["advisor_id"].int ?? 0]
//                                print("Server Json Video Call: ", dataForShare)
//                                self.navigateToCallController(withData: dataForShare)
                        }
                        
                    }
                }
                
            }
        } else {
            logMessage(messageText: "Voice Chat: top vc not found")
        }
    }
    
    func GotoAdvisorMessageScreen(singleResponse:JSON,notes:String) {
        logMessage(messageText: "Voice Chat: GotoAdvisorMessageScreen")
      
        if let topController = UIApplication.topViewController(), !topController.isKind(of: AdvisorMessageChatViewController.self) {
            let controller:AdvisorMessageChatViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorMessageChatViewController.storyboardID) as! AdvisorMessageChatViewController
            
            if notes != "" {
                SingletonClass.sharedInstance.isNoteAdded = true
                SingletonClass.sharedInstance.noteText = notes
            } else {
                SingletonClass.sharedInstance.isNoteAdded = false
                SingletonClass.sharedInstance.noteText = ""
            }
            
            let DataForShare : [String:Any] = ["user_id":singleResponse["user_id"].int ?? 0,"type":singleResponse["type"].string ?? "","start_time":singleResponse["start_time"].string ?? "","customer_profile_picture":singleResponse["customer_profile_picture"].string ?? "","message":singleResponse["message"].string ?? "","default_message":singleResponse["default_message"].string ?? "","advisor_profile_picture":singleResponse["advisor_profile_picture"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"customer_name":singleResponse["customer_name"].string ?? ""]
            controller.sessionType = "session"
            controller.RequestAcceptDataOfUser = DataForShare
//            topController.navigationController?.pushViewController(controller, animated: true)
            let window = UIApplication.shared.keyWindow ?? UIApplication.shared.windows.first
            if let tab = (window?.rootViewController as? UINavigationController)?.viewControllers.first as? CustomTabBarVC, let nav = tab.selectedViewController as? UINavigationController {
                logMessage(messageText: "Voice Chat: nav found manual")
                nav.pushViewController(controller, animated: true)
            } else {
                logMessage(messageText: "Voice Chat: nav not found manual")
                topController.navigationController?.pushViewController(controller, animated: true)
            }
            logMessage(messageText: "Voice Chat: screen pushed")
            self.totalMinutesTimer = 1
            self.TotalTimecounter = 0
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                self.startTimer(withInterval: 1.0)
            })
        } else {
            logMessage(messageText: "Voice Chat: GotoAdvisorMessageScreen top vc not found")
        }
    }
    
    
    //MARK: -  availibilitySwitchOff()
    
    func availibilitySwitchOff() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.availibility_switch_off.rawValue) { (response) in
            currentRequestArrived = false
            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
            self.player?.stop()
            PlayerForRequestAccept?.stop()
            print(#function)
            print(response)
            if let provider = appDel.callKitProvider {
                provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                appDel.callKitProvider?.invalidate()
            }
//            if userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) {
//                if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id != "" {
//                    self.GetAvailabilty()
//                }
//            }
            print("ATDebug :: Advisor Home \(#function)")
            SingletonClass.sharedInstance.adviserAudioChatOn = false
            SingletonClass.sharedInstance.adviserTextChatOn = false
            SingletonClass.sharedInstance.adviserVideoChatOn = false
            //SocketIOManager.shared.socketEmit(for: socketApiKeys.advisor_availability.rawValue, with: ["advisor_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0])
            //self.MakeAModelForToggle()
            
            if let topVC = UIApplication.topViewController() {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: nil)
                    
                }
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                        for controller in topVC as Array {
                            if controller.isKind(of: AdviserHomeViewController.self) {
                                let HomeVC = controller as! AdviserHomeViewController
                                HomeVC.setTopAvability()
                                break
                            }
                        }
                    }
                    if let arrChild = (NavigationController.children.first?.children), !arrChild.isEmpty {
                        
                        if let topVC = (arrChild[3] as? UINavigationController)?.viewControllers {
                            for controller in topVC as Array {
                                if controller.isKind(of: AdviserMyAccountViewController.self) {
                                    let MyAccount = controller as! AdviserMyAccountViewController
                                    MyAccount.setTopAvability()
                                    break
                                }
                            }
                        }
                    }
                }
            })
            
            //self.setTopAvability()
        }
    }
    //MARK: -  OnEndSession()
    func OnEndSession() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.end_session.rawValue) { (response) in
            
            print(#function)
            print(response)
            
            print("ATDebug :: Advisor Home \(#function)")
            self.stopTimer()
            let AvailabiltyDataModel = AvailabiltyData()
            AvailabiltyDataModel.advisor_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
          
            self.WebserviceForGetAvailability(reqModel: AvailabiltyDataModel)
            
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                SingletonClass.sharedInstance.StartTimerForAdvisor()
                if SingletonClass.sharedInstance.adviserAudioChatOn == true || SingletonClass.sharedInstance.adviserTextChatOn == true || SingletonClass.sharedInstance.adviserVideoChatOn == true {
                    
                    //self.MakeAModelForToggle()
                }
                
                
            }
            if let viewController = self.navigationController?.viewControllers.first(where: {$0 is AdvisorMessageChatViewController}) {
                
                self.navigationController?.popToViewController(viewController, animated: false)
                
            }
            
            if let newTopVC = UIApplication.topViewController() {
                if newTopVC.isKind(of: UIAlertController.self) {
                    newTopVC.dismiss(animated: true, completion: {
                        DispatchQueue.main.async {
                            if let TopVC = UIApplication.topViewController() {
                                if TopVC.isKind(of: AdvisorMessageChatViewController.self) || TopVC.isKind(of: CallControllerViewController.self) || TopVC.isKind(of: AdvisorAudioCallViewController.self) {
                                    if TopVC.isKind(of: AdvisorMessageChatViewController.self) {
                                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                            }
                                        }
                                        
                                    } else if TopVC.isKind(of: CallControllerViewController.self) {
                                        
                                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                            }
                                        }
                                    }
                                    
                                    else if TopVC.isKind(of: AdvisorAudioCallViewController.self) {
                                        let VC = TopVC as! AdvisorAudioCallViewController
                                        
                                        appDel.GoBack()
                                        
                                        VC.navigationController?.popViewController(animated: true)
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                
                                                let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                                let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                    
                                                    let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                                    controller.isFromCustomer = false
                                                    controller.dataForTable = dataForShare
                                                    let navigationController = UINavigationController(rootViewController: controller)
                                                    navigationController.modalPresentationStyle = .overCurrentContext
                                                    navigationController.modalTransitionStyle = .crossDissolve
                                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                                }))
                                                appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                            }
                                        }})
                                    }
                                } else {
                                    if TopVC.isModal {
                                        TopVC.dismiss(animated: true, completion: {
                                            if let LatestTopVC = UIApplication.topViewController() {
                                                if LatestTopVC.isKind(of: AdvisorAudioCallViewController.self)
                                                {
                                                    let VC = LatestTopVC as! AdvisorAudioCallViewController
                                                    appDel.GoBack()
                                                    
                                                    VC.navigationController?.popViewController(animated: true)
                                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                                                        if let singleResponse = arrResponse.first {
                                                            
                                                            let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                                            let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                                
                                                                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                                                controller.isFromCustomer = false
                                                                controller.dataForTable = dataForShare
                                                                let navigationController = UINavigationController(rootViewController: controller)
                                                                navigationController.modalPresentationStyle = .overCurrentContext
                                                                navigationController.modalTransitionStyle = .crossDissolve
                                                                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                                            }))
                                                            appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                                        }
                                                    }})
                                                     
                                                } else {
                                                    if let arrResponse = response.array {
                                                        if let singleResponse = arrResponse.first {
                                                            self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                                        }
                                                    }
                                                }
                                                
                                            }
                                            
                                        })
                                    } else {
                                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                            }
                                        }
                                    }
                                    
                                }
                            }
                        }
                        
                    })
                    
                } else {
                    
                    if newTopVC.isKind(of: AdvisorMessageChatViewController.self) || newTopVC.isKind(of: CallControllerViewController.self) || newTopVC.isKind(of: AdvisorAudioCallViewController.self) {
                        if newTopVC.isKind(of: AdvisorMessageChatViewController.self) {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                }
                            }
                            
                            
                        } else if newTopVC.isKind(of: CallControllerViewController.self) {
                            
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                }
                            }
                        }
                        
                        else if newTopVC.isKind(of: AdvisorAudioCallViewController.self) {
                            let VC = newTopVC as! AdvisorAudioCallViewController
                            
                            appDel.GoBack()
                            
                            VC.navigationController?.popViewController(animated: true)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    
                                    let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                    let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        
                                        let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                        controller.isFromCustomer = false
                                        controller.dataForTable = dataForShare
                                        let navigationController = UINavigationController(rootViewController: controller)
                                        navigationController.modalPresentationStyle = .overCurrentContext
                                        navigationController.modalTransitionStyle = .crossDissolve
                                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                    }))
                                    appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                }
                            }})
                        }
                    } else {
                        if newTopVC.isModal {
                            newTopVC.dismiss(animated: true, completion: {
                                if let LatestTopVC = UIApplication.topViewController() {
                                    if LatestTopVC.isKind(of: AdvisorAudioCallViewController.self)
                                    {
                                        let VC = LatestTopVC as! AdvisorAudioCallViewController
                                        appDel.GoBack()
                                        
                                        VC.navigationController?.popViewController(animated: true)
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                
                                                let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                                let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                                    
                                                    let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                                    controller.isFromCustomer = false
                                                    controller.dataForTable = dataForShare
                                                    let navigationController = UINavigationController(rootViewController: controller)
                                                    navigationController.modalPresentationStyle = .overCurrentContext
                                                    navigationController.modalTransitionStyle = .crossDissolve
                                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                                }))
                                                appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                            }
                                        }})
                                         
                                    } else {
                                        if let arrResponse = response.array {
                                            if let singleResponse = arrResponse.first {
                                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                            }
                                        }
                                    }
                                    
                                }
                                
                            })
                        } else {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                }
                            }
                        }
                    }
                }
            }
            self.setTopAvability()
        }
    }
    func goBackFromMessageOrVideoScreen(ArrayOfJson:JSON) {
        let singleResponse = ArrayOfJson
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdvisorMessageChatViewController.self) || topVC.isKind(of: AdvisorAudioCallViewController.self)  {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            
                            let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                            
                            let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                
                                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                controller.isFromCustomer = false
                                controller.dataForTable = dataForShare
                                let navigationController = UINavigationController(rootViewController: controller)
                                navigationController.modalPresentationStyle = .overCurrentContext
                                navigationController.modalTransitionStyle = .crossDissolve
                                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                
                            }))
                            
                            appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        topVC.navigationController?.popViewController(animated: true)
                        let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                        let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                            let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                            controller.isFromCustomer = false
                            controller.dataForTable = dataForShare
                            let navigationController = UINavigationController(rootViewController: controller)
                            navigationController.modalPresentationStyle = .overCurrentContext
                            navigationController.modalTransitionStyle = .crossDissolve
                            appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                        }))
                        appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                } else {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            
                            let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                            let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                
                                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                controller.isFromCustomer = false
                                controller.dataForTable = dataForShare
                                let navigationController = UINavigationController(rootViewController: controller)
                                navigationController.modalPresentationStyle = .overCurrentContext
                                navigationController.modalTransitionStyle = .crossDissolve
                                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                            }))
                            appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                        })
                    } else {
                        let dataForShare:[String:String] = ["commission_rate":singleResponse["commission_rate"].string ?? "","session_earning":singleResponse["session_earning"].string ?? "","total_minute":singleResponse["total_minute"].string ?? "","message":singleResponse["message"].string ?? "","advisor_special_day_commission":singleResponse["advisor_special_day_commission"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                        let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                            let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                            controller.isFromCustomer = false
                            controller.dataForTable = dataForShare
                            let navigationController = UINavigationController(rootViewController: controller)
                            navigationController.modalPresentationStyle = .overCurrentContext
                            navigationController.modalTransitionStyle = .crossDissolve
                            appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                        }))
                        appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                    }
                }
                
            }
        })
    }
    //MARK: -  OnForVideoCallAdvisor()
    
    func OnForVideoCallAdvisor() {
        print("OnForVideoCallAdvisor ----")
        SocketIOManager.shared.socketCall(for: socketApiKeys.videoCallAdvisor.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    
                    let dataForShare:[String:Any] = ["message":singleResponse["message"].string ?? "" ,"user_id":singleResponse["user_id"].int ?? 0,"room":singleResponse["room"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"token":singleResponse["token"].string ?? "","name":singleResponse["name"].string ?? "","receiver_id":singleResponse["receiver_id"].int ?? 0]
                    self.navigateToCallController(withData: dataForShare)
                    
                }
            }
        }
    }
    //MARK: -  RequestRejectDisconnect()
    
    func RequestRejectDisconnect() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.request_reject_disconnect.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            currentRequestArrived = false
            
            
            
            if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
                SingletonClass.sharedInstance.StartTimerForAdvisor()

                if SingletonClass.sharedInstance.adviserAudioChatOn == true || SingletonClass.sharedInstance.adviserVideoChatOn == true || SingletonClass.sharedInstance.adviserTextChatOn == true {
                    
                   // self.MakeAModelForToggle()
                }
                
                
                //self.MakeAModelForToggle()
            }
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["type"].stringValue == "audio"
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CallReject"), object: nil, userInfo: nil)
                    } else if singleResponse["type"].stringValue == "chat" {
                        if let provider = appDel.callKitProvider {
                            provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                            appDel.callKitProvider?.invalidate()
                           
                        }
                    } else if singleResponse["type"].stringValue == "video" {
                        if let provider = appDel.callKitProvider {
                            provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                            appDel.callKitProvider?.invalidate()
                           
                        }
                    }
                }
            }
      
            if let topVC = UIApplication.topViewController() {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                            }
                        }
                    })
                }
            }
        }
    }
    //MARK: - Go to video call screen
    func navigateToCallController(withData data : [String:Any])
    {
        print(#function)
        print(data)
        let response = data// {
        
        let roomName = JSON(response["room"] as Any).stringValue // response.first?["room"] as? String ?? ""
        let senderName = JSON(response["name"] as Any).stringValue // response.first?["name"] as? String ?? ""
        let visitID = JSON(response["booking_id"] as Any).int // "\(response.first?["visit_id"] as? Int ?? 0)"
        let sender_id = JSON(response["user_id"] as Any).int
        let token = JSON(response["token"] as Any).stringValue
        let reciverID = JSON(response["receiver_id"] as Any).int
        let story = UIStoryboard(name: "AdviserMain", bundle: nil)
        guard let vc = story.instantiateViewController(withIdentifier: "CallControllerViewController") as? CallControllerViewController else { return }
        
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: CallControllerViewController.self) {
                return
            }
        }
        
        vc.senderID = "\(sender_id ?? 0)"
        
        vc.reciverID = "\(reciverID ?? 0)"
        vc.bookingID = "\(visitID ?? 0)"
        
        vc.senderName = senderName
        vc.roomName = roomName
        vc.CallingTo = senderName
        vc.visit_id = "\(visitID ?? 0)"
        appDel.strRoomName = roomName
        vc.vetID = "\(sender_id ?? 0)"
        vc.token = token
        vc.navigationController?.isNavigationBarHidden = true
        vc.modalPresentationStyle = .fullScreen
        
        if let topVC =  UIApplication.topViewController() as? UINavigationController
        {
            print("operoller")
            topVC.present(vc, animated: true, completion: nil)
        }
        else
        {
            print("opening from normall vc")
            UIApplication.topViewController()?.present(vc, animated: true, completion: nil)
        }

    }
   
    
    //MARK: - stop_timer
    func stop_timer() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.stop_timer.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
            self.stopTimer()
            
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CallControllerViewController.self) {
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            let CommonLabelController:CommonLabelPopupViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonLabelPopupViewController.storyboardID) as! CommonLabelPopupViewController
                            
                            CommonLabelController.textForShow = singleResponse["message"].stringValue
                            CommonLabelController.modalPresentationStyle = .overCurrentContext
                            CommonLabelController.modalTransitionStyle = .crossDissolve
                            if let topVC = UIApplication.topViewController() {
                                if topVC.isKind(of: AdviserCommonPopupViewController.self) {
                                    
                                } else {
                                    UIApplication.topViewController()?.present(CommonLabelController, animated: true, completion: nil)
                                }
                            }

                        }
                    }
                } else {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    let CommonLabelController:CommonLabelPopupViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonLabelPopupViewController.storyboardID) as! CommonLabelPopupViewController

                                    CommonLabelController.textForShow = singleResponse["message"].stringValue
                                    CommonLabelController.modalPresentationStyle = .overCurrentContext
                                    CommonLabelController.modalTransitionStyle = .crossDissolve
                                    if let topVC = UIApplication.topViewController() {
                                        if topVC.isKind(of: AdviserCommonPopupViewController.self) {

                                        } else {
                                            UIApplication.topViewController()?.present(CommonLabelController, animated: true, completion: nil)
                                        }
                                    }

                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                let CommonLabelController:CommonLabelPopupViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonLabelPopupViewController.storyboardID) as! CommonLabelPopupViewController
                                
                                CommonLabelController.textForShow = singleResponse["message"].stringValue
                                CommonLabelController.modalPresentationStyle = .overCurrentContext
                                CommonLabelController.modalTransitionStyle = .crossDissolve
                                if let topVC = UIApplication.topViewController() {
                                    if topVC.isKind(of: AdviserCommonPopupViewController.self) {
                                        
                                    } else {
                                        UIApplication.topViewController()?.present(CommonLabelController, animated: true, completion: nil)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - resume_timer
    func resume_timer() {
        let timer = TotalTimecounter
        SocketIOManager.shared.socketCall(for: socketApiKeys.resume_timer.rawValue) { [self] (response) in
            
            print(#function)
            print(response)
            print(timer)
            print("ATDebug :: Advisor Home \(#function)")
            print(self.TotalTimecounter)
            startTimer(withInterval: 1.0)
            
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommonLabelPopupViewController.self) {
                    topVC.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
}
