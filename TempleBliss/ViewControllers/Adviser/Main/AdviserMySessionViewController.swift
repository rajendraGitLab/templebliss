//
//  AdviserMySessionViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SkeletonView
import SDWebImage
class AdviserMySessionViewController:  BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    //MARK: - Properties
    var isLoadingList : Bool = false
    var FirstTimeLoad = true
    var firstTimeTotalCount = 0
    var totalPagesLoad = 1
    var customTabBarController : CustomTabBarVC?
    var MainSessionListArray : [SessionListForAdvisor] = []
    
    var MysessionData : [[SessionListForAdvisor]] = []
    // var MysessionData : [SessionListForAdvisor] = []
    var refreshControl = UIRefreshControl()
    //MARK: - IBOutlets
    
    @IBOutlet weak var tblMySession: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
        tblMySession.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
         self.tblMySession.refreshControl = refreshControl
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        customTabBarController?.showTabBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        //       self.MainSessionListArray = []
        
        //        self.tblMySession.reloadData()
        //
        self.ReloadAllLoadedSessionData()
        customTabBarController?.showTabBar()
    }
    override func viewDidDisappear(_ animated: Bool) {
        SingletonClass.sharedInstance.isNoteAdded = false
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        totalPagesLoad = 1
        self.PullToRefreshGetData()
        
    }
    //MARK: - tableViewMethods
    func numberOfSections(in tableView: UITableView) -> Int {
        if FirstTimeLoad == true {
            return 1
        } else {
            if MysessionData.count == 0 {
                return 1
            } else {
                return MysessionData.count
            }
            
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if FirstTimeLoad == true {
            return 7
        } else {
            if MysessionData.count == 0 {
                return 1
            } else {
                return MysessionData[section].count
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if FirstTimeLoad == true {
            return 100
        } else {
            if MysessionData.count == 0 {
                return tableView.frame.size.height
            } else {
                return UITableView.automaticDimension
            }
            
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
            
            if MysessionData.count != 0 {
                self.isLoadingList = true
                let spinner = UIActivityIndicatorView(style: .white)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblMySession.bounds.width, height: CGFloat(44))
                
                self.tblMySession.tableFooterView = spinner
                self.tblMySession.tableFooterView?.isHidden = false
                LoadMoreData()
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if MysessionData.count != 0 {
            GetSessionSummaryDetails(bookingID: Int(MysessionData[indexPath.section][indexPath.row].bookingId) ?? 0)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if FirstTimeLoad == true {
            return 0
        } else {
            if MysessionData.count == 0 {
                return 0
            } else {
                return 35
            }
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if FirstTimeLoad == true {
            let headerView  = UIView()
            return headerView
        } else {
            if MysessionData.count == 0 {
                let headerView  = UIView()
                return headerView
            } else {
                
                let cell = tblMySession.dequeueReusableCell(withIdentifier: AdvisorHeaderCell.reuseIdentifier) as! AdvisorHeaderCell
                cell.SessionDate.text = convertDateFormater(MysessionData[section][0].bookingDate ?? "")
                return cell
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if FirstTimeLoad == true {
            let cell = tblMySession.dequeueReusableCell(withIdentifier: AdviserMySessionCell.reuseIdentifier, for: indexPath) as! AdviserMySessionCell
            cell.mySessionAdvisorData.isHidden = true
            cell.mySessionGiveRattingView.isHidden = true
            cell.skeletonImageView.isHidden = false
            
            //
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.skeletonImageView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            // cell.lblPdfDescription.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            
            return cell
        } else {
            if MysessionData.count != 0 {
                let cell = tblMySession.dequeueReusableCell(withIdentifier: AdviserMySessionCell.reuseIdentifier, for: indexPath) as! AdviserMySessionCell
                
                if MysessionData[indexPath.section][indexPath.row].IsPostMessageAvailable == "0" || MysessionData[indexPath.section][indexPath.row].IsPostMessageAvailable == "" {
                    cell.btnPostChat.isEnabled = false
                    
                } else {
                    cell.btnPostChat.isEnabled = true
                    
                }
                
                if MysessionData[indexPath.section][indexPath.row].postUnreadMessageCount == "0" || MysessionData[indexPath.section][indexPath.row].postUnreadMessageCount == "" {
                    cell.btnPostChat.setImage(UIImage(named: "ic_messageMySession"), for: .normal)
                    cell.btnPostChat.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

                } else {
                    cell.btnPostChat.setImage(UIImage(named: "ic_PostChatWithMessage"), for: .normal)
                    cell.btnPostChat.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: -12)
                }
                
                cell.mySessionAdvisorData.isHidden = false
                cell.mySessionGiveRattingView.isHidden = false
                cell.skeletonImageView.isHidden = true
                
                cell.CustomerName.text = MysessionData[indexPath.section][indexPath.row].fullName
                
                if (MysessionData[indexPath.section][indexPath.row].profilePicture ?? "") == "" {
                    cell.CustomerImageView.image = UIImage(named: "user_dummy_profile")
                } else {
                    
                    let imgURL = MysessionData[indexPath.section][indexPath.row].profilePicture ?? ""
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    cell.CustomerImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
                    
                    cell.CustomerImageView.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                if MysessionData[indexPath.section][indexPath.row].note == "" {
                    cell.btnViewNotesButton.setunderline(title: "Add Notes",color: colors.white.value.withAlphaComponent(0.5), font: CustomFont.regular.returnFont(12))
                } else {
                    cell.btnViewNotesButton.setunderline(title: "View Notes",color: colors.white.value.withAlphaComponent(0.5), font: CustomFont.regular.returnFont(12))
                }
                cell.SessionTime.text = ConvertToTime(MysessionData[indexPath.section][indexPath.row].bookingDate ?? "")
              //  cell.SessionDate.text = convertDateFormater(MysessionData[indexPath.section][indexPath.row].bookingDate ?? "")
                if MysessionData[indexPath.section][indexPath.row].totalMinutes ?? "" == "01.00" || MysessionData[indexPath.section][indexPath.row].totalMinutes.components(separatedBy: ":")[0] == "00" {
                    cell.TotalTime.text = "\(MysessionData[indexPath.section][indexPath.row].totalMinutes ?? "") Minute"
                } else {
                    cell.TotalTime.text = "\(MysessionData[indexPath.section][indexPath.row].totalMinutes ?? "") Minutes"
                }
                // cell.TotalTime.text = "\(MysessionData[indexPath.section][indexPath.row].totalMinutes ?? "") Minutes"
                cell.TotalAmount.text = "$ \(MysessionData[indexPath.section][indexPath.row].advisorCommission ?? "")"
                switch MysessionData[indexPath.section][indexPath.row].type {
                case "video":
                    cell.communicationType.setImage(UIImage(named: "ic_videoMySession") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = false
                    cell.communicationType.alpha = 0.7
                // cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_videoMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                
                case "audio":
                    cell.communicationType.setImage(UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = false
                    cell.communicationType.alpha = 0.7
                // cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_audioMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                
                case "chat":
                    cell.communicationType.setImage(UIImage(named: "ic_chatMySession") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = true
                    cell.communicationType.alpha = 1.0
                //    cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_chatMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                
                default:
                    break
                }
                cell.btnChat = {
                    let controller:ShowConversationViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowConversationViewController.storyboardID) as! ShowConversationViewController
                    controller.sessionType = "session"
                    controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                    controller.userName = self.MysessionData[indexPath.section][indexPath.row].fullName ?? ""
                    controller.isFormAdvisor = true
                    controller.AdvisorImage = self.MysessionData[indexPath.section][indexPath.row].profilePicture
                        ?? ""
                    controller.navigationImage = self.MysessionData[indexPath.section][indexPath.row].profilePicture ?? ""
                    controller.navigationName = self.MysessionData[indexPath.section][indexPath.row].fullName ?? ""
                    controller.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                    
                    
                }
                cell.btnReadMessage = {
                    let controller : AdvisorMessageChatViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorMessageChatViewController.storyboardID) as! AdvisorMessageChatViewController
                    
                    let dataForShare : [String:Any] = ["advisor_profile_picture":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture ?? "","customer_profile_picture":self.MysessionData[indexPath.section][indexPath.row].profilePicture ?? "","type":self.MysessionData[indexPath.section][indexPath.row].type ?? "", "user_id":Int(self.MysessionData[indexPath.section][indexPath.row].customerId ?? "") ?? 0,"booking_id":Int(self.MysessionData[indexPath.section][indexPath.row].bookingId ?? "") ?? 0]
                    
                    
                    
                    if self.MysessionData[indexPath.section][indexPath.row].note != "" {
                        SingletonClass.sharedInstance.isNoteAdded = true
                        SingletonClass.sharedInstance.noteText = self.MysessionData[indexPath.section][indexPath.row].note ?? ""
                    } else {
                        SingletonClass.sharedInstance.isNoteAdded = false
                        SingletonClass.sharedInstance.noteText = ""
                    }
                    
                    controller.sessionType = "free"
                    controller.RequestAcceptDataOfUser = dataForShare
                    controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                    controller.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                    
                    //                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MessageViewController.storyboardID) as! MessageViewController
                    //                    //self.customTabBarController?.hideTabBar()
                    //                    controller.hidesBottomBarWhenPushed = true
                    //                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
                cell.btnGiveRattingReviews = {
                    
                    if self.MysessionData[indexPath.section][indexPath.row].note == "" {
                        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
                        controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                        //  self.customTabBarController?.hideTabBar()
                        controller.StringButton = "SUBMIT"
                        controller.isShowLeftIcon = true
                        controller.isShowRightIcon = false
                        controller.isShowNoteTextView = true
                        controller.stringDescription = "Add Session Note"
                        controller.btnSubmitClosour = {
                            //self.customTabBarController?.showTabBar()
                            self.dismiss(animated: true, completion: {
                                
                                let TopVC = UIApplication.topViewController()
                                if ((TopVC?.isKind(of: AdviserMySessionViewController.self)) != nil) {
                                    let CommonVC = TopVC as! AdviserMySessionViewController
                                    CommonVC.ReloadAllLoadedSessionData()
                                    
                                }
                                
                            })
                            
                            
                            //self.ReloadAllLoadedSessionData()
                        }
                        //            controller.textForShow = "Waiting for client to add minutes..."
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.modalPresentationStyle = .overCurrentContext
                        navigationController.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                    } else {
                        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
                        
                        //  self.customTabBarController?.hideTabBar()
                        controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                        controller.StringButton = "Okay"
                        controller.isShowLeftIcon = true
                        controller.isShowRightIcon = true
                        if(self.MysessionData[indexPath.section][indexPath.row].isLatest == "0")
                        {
                            controller.isShowRightIcon = false
                        }
                        controller.isShowNoteTextView = false
                        controller.isShowNoteTextViewWithData = true
                        controller.stringDescription = self.MysessionData[indexPath.section][indexPath.row].note ?? ""
                        
                        controller.btnSubmitClosour = {
                            //  self.customTabBarController?.showTabBar()
                            self.dismiss(animated: true, completion: {
                                
                                let TopVC = UIApplication.topViewController()
                                if ((TopVC?.isKind(of: AdviserMySessionViewController.self)) != nil) {
                                    let CommonVC = TopVC as! AdviserMySessionViewController
                                    CommonVC.ReloadAllLoadedSessionData()
                                    
                                }
                                
                            })
                            //self.ReloadAllLoadedSessionData()
                        }
                        controller.isDismissCLosour = {
                            self.customTabBarController?.showTabBar()
                        }
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.modalPresentationStyle = .overCurrentContext
                        navigationController.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                    }
                    
                    
                }
                cell.btnDeleteSession = {
                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: DeleteSessionViewController.storyboardID) as! DeleteSessionViewController
                    controller.modalPresentationStyle = .overCurrentContext
                    controller.modalTransitionStyle = .crossDissolve
                    //self.customTabBarController?.hideTabBar()
                    controller.isDismiss = {
                        self.customTabBarController?.showTabBar()
                    }
                    controller.isCancle = {
                        self.customTabBarController?.showTabBar()
                    }
                    controller.isDelete = {
                        self.customTabBarController?.showTabBar()
                    }
                    self.present(controller, animated: true, completion: nil)
                    
                }
                return cell
                
            } else {
                let NoDatacell = tblMySession.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
                NoDatacell.imgNoData.image = UIImage(named: "ic_noSessionFound")
                NoDatacell.lblNoDataTitle.text = "No Session found"
                //NodataTitleText.ItemList
                
                return NoDatacell
            }
            
        }
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func ConvertToTime(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "HH:mm a"
        return  dateFormatter.string(from: date)
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "dd MMM YYYY"
        return  dateFormatter.string(from: date)
        
    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
    
    
}
class AdviserMySessionCell : UITableViewCell {
    @IBOutlet weak var skeletonImageView: UIImageView!
    
    @IBOutlet weak var btnPostChat: UIButton!
    @IBOutlet weak var mySessionAdvisorData: MySessionPageView!
    @IBOutlet weak var mySessionGiveRattingView: viewViewClearBG!
    
    @IBOutlet weak var communicationType: MySessionButton!
    @IBOutlet weak var btnViewNotesButton: MySessionButton!
    @IBOutlet weak var CustomerImageView: UIImageView!
    
    @IBOutlet weak var SessionTime: MySessionLabel!
    @IBOutlet weak var TotalTime: MySessionLabel!
    @IBOutlet weak var CustomerName: MySessionLabel!
    @IBOutlet weak var TotalAmount: MySessionLabel!
    
    var btnChat : (() -> ())?
    var btnReadMessage : (() -> ())?
    var btnGiveRattingReviews : (() -> ())?
    var btnDeleteSession : (() -> ())?
    
    override func awakeFromNib() {
        CustomerImageView.contentMode = .scaleAspectFill
    }
    @IBAction func btnReadMessageClick(_ sender: Any) {
        if let click = self.btnReadMessage {
            click()
        }
        
    }
    
    @IBAction func btnChatClick(_ sender: Any) {
        if let click = self.btnChat {
            click()
        }
    }
    
    @IBAction func BtnViewNotesButtonClick(_ sender: Any) {
        if let click = self.btnGiveRattingReviews {
            click()
        }
    }
}
class AdvisorHeaderCell : UITableViewCell {
    
    @IBOutlet weak var SessionDate: MySessionLabel!
    
}
//extension AdviserMySessionViewController {
//    func getSessionData() {
//        let MySessionModel = AdvisorMySessionReqModel()
//        MySessionModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
//        MySessionModel.page = "\(totalPagesLoad)"
//        webserviceForMySession(reqModel: MySessionModel)
//    }
//    func webserviceForMySession(reqModel:AdvisorMySessionReqModel) {
//        WebServiceSubClass.AdvisorMySession(addCategory: reqModel, completion: {(json, status, response) in
//
//            if status {
//                let MySessionResModel = sessionForAdvisor.init(fromJson: json)
//                self.MysessionData.append(contentsOf: MySessionResModel.sessionList)
//                self.tblMySession.reloadData()
//
//            } else {
//                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
//            }
//        })
//
//    }
//
//
//}

extension AdviserMySessionViewController {
    func LoadMoreData() {
        let MySessionModel = AdvisorMySessionReqModel()
        MySessionModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        MySessionModel.page = "\(totalPagesLoad + 1)"
        webserviceForLoadMoreData(reqModel: MySessionModel)
    }
    func webserviceForLoadMoreData(reqModel:AdvisorMySessionReqModel) {
        WebServiceSubClass.AdvisorMySession(addCategory: reqModel, completion: {(json, status, response) in
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                
                
                let MySessionResModel = sessionForAdvisor.init(fromJson: json)
                
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                if MySessionResModel.sessionList.count != 0 {
                    self.totalPagesLoad  = self.totalPagesLoad + 1
                }
                let MySessionListDummy = self.MainSessionListArray
                self.MysessionData = []
                var datesArray = self.MainSessionListArray.compactMap({self.convertDateFormater($0.bookingDate)})
                datesArray = datesArray.uniqued()
                var dic = [[SessionListForAdvisor]]() // Your required result
                
                datesArray.forEach { (element) in
                    
                    let NewDictonary =  MySessionListDummy.filter({self.convertDateFormater($0.bookingDate) == element})
                    
                    
                    dic.append(NewDictonary)
                }
                self.MysessionData = dic
                self.tblMySession.reloadData()
            } else {
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}

extension AdviserMySessionViewController {
    func ReloadAllLoadedSessionData() {
        
        self.MainSessionListArray = []
        
        ReloadSessionData(pageNumber: "1")
        
    }
    func ReloadSessionData(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            Utilities.hideHud()
            self.FirstTimeLoad = false
            //self.totalPagesLoad = self.totalPagesLoad + 1
            
            self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            let MySessionListDummy = self.MainSessionListArray
            self.MysessionData = []
            var datesArray = self.MainSessionListArray.compactMap({convertDateFormater($0.bookingDate)})
            datesArray = datesArray.uniqued()
            var dic = [[SessionListForAdvisor]]() // Your required result
            
            datesArray.forEach { (element) in
                
                let NewDictonary =  MySessionListDummy.filter({convertDateFormater($0.bookingDate) == element})
                
                
                dic.append(NewDictonary)
            }
            self.MysessionData = dic
            print(dic)
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
            Utilities.hideHud()
        } else {
            if pageNumber == "1" {
                self.MainSessionListArray = []
                
            }
            let MySessionModel = AdvisorMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionData(reqModel: MySessionModel)
        }
    }
    func webserviceForReloadSessionData(reqModel:AdvisorMySessionReqModel) {
        if self.FirstTimeLoad == false {
            Utilities.showHud()
        }
        print(#function)
        WebServiceSubClass.AdvisorMySession(addCategory: reqModel, completion: {(json, status, response) in
            
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                
                let MySessionResModel = sessionForAdvisor.init(fromJson: json)
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                if MySessionResModel.sessionList.count == 0 {
                    Utilities.hideHud()
                }
            } else {
                self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
                self.refreshControl.endRefreshing()
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
}
extension AdviserMySessionViewController {
    func PullToRefreshGetData() {
        
        ReloadSessionDataByPullToRefresh(pageNumber: "1")
        
    }
    func ReloadSessionDataByPullToRefresh(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            
            self.FirstTimeLoad = false
            
            let MySessionListDummy = self.MainSessionListArray
            self.MysessionData = []
            var datesArray = self.MainSessionListArray.compactMap({convertDateFormater($0.bookingDate)})
            datesArray = datesArray.uniqued()
            // let datesArray = self.MainSessionListArray.compactMap { $0.bookingDate} // return array of date
            var dic = [[SessionListForAdvisor]]() // Your required result
            
            datesArray.forEach { (element) in
                
                let NewDictonary =  MySessionListDummy.filter({convertDateFormater($0.bookingDate) == element})
                
                
                dic.append(NewDictonary)
            }
            self.MysessionData = dic
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
            self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
        } else {
            
            if pageNumber == "1" {
                self.MainSessionListArray = []
                
            }
            let MySessionModel = AdvisorMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionDataByPullToRefresh(reqModel: MySessionModel)
        }
    }
    func webserviceForReloadSessionDataByPullToRefresh(reqModel:AdvisorMySessionReqModel) {
        
        print(#function)
        WebServiceSubClass.AdvisorMySession(addCategory: reqModel, completion: {(json, status, response) in
            
            
            if status {
                
                let MySessionResModel = sessionForAdvisor.init(fromJson: json)
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                
                
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                
            } else {
                self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
                self.refreshControl.endRefreshing()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
    
}
extension AdviserMySessionViewController {
    
    func GetSessionSummaryDetails(bookingID:Int) {
        print(#function)
        let GetSummaryReqModel = GetSessionSummary()
        GetSummaryReqModel.booking_id = bookingID
        GetSummaryReqModel.type = "advisor"
        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
        
        
    }
    func webserviceForGetSessionSummatyDetails(reqModel:GetSessionSummary) {
        Utilities.showHud()
        print(#function)
        WebServiceSubClass.SessionSummary(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                
                let SummaryModel = ShowSummaryResModel.init(fromJson: json)
                let dataForShare:[String:String] = ["commission_rate": SummaryModel.data.commissionRate ?? "","session_earning": SummaryModel.data.sessionEarning ?? "","total_minute": SummaryModel.data.totalMinute ?? "","advisor_special_day_commission": SummaryModel.data.advisorSpecialDayCommission ?? "","category_name":SummaryModel.data.category_name ?? ""]
                
                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                controller.isFromCustomer = false
                controller.dataForTable = dataForShare
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                
            } else {
                
            }
        })
    }
}

extension Sequence where Element: Hashable {
    func uniqued() -> [Element] {
        var set = Set<Element>()
        return filter { set.insert($0).inserted }
    }
}
