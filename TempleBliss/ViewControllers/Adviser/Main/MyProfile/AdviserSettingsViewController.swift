//
//  AdviserSettingsViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserSettingsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    //MARK: - Properties
    var customTabBarController : CustomTabBarVC?
    var SettingsData = [AdvisersettingsCategoryData]()
    //MARK: - IBOutlets
    @IBOutlet weak var lblVersionNumber: settingsLabel!
    @IBOutlet weak var tblSettings: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        setLocalization()
        setValue()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Settings", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        
        SettingsData.append(AdvisersettingsCategoryData(img: AdvisersettingsCategory.TermsCondition.imageValue, name: AdvisersettingsCategory.TermsCondition.value))
        SettingsData.append(AdvisersettingsCategoryData(img: AdvisersettingsCategory.PrivacyPolicy.imageValue, name: AdvisersettingsCategory.PrivacyPolicy.value))
        SettingsData.append(AdvisersettingsCategoryData(img: AdvisersettingsCategory.ContactUs.imageValue, name: AdvisersettingsCategory.ContactUs.value))
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        lblVersionNumber.text = "v \(AppInfo.appVersion)"
        
    }
    
    //MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SettingsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblSettings.dequeueReusableCell(withIdentifier: SettingsCell.reuseIdentifier, for: indexPath) as! SettingsCell
        cell.categoryImage.image = SettingsData[indexPath.row].settingsCategoryImage
        cell.categoryName.text = SettingsData[indexPath.row].settingsCategoryName
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch  SettingsData[indexPath.row].settingsCategoryName{
        case settingsCategory.TermsCondition.value:
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonWebViewViewController.storyboardID) as! CommonWebViewViewController
            controller.strNavTitle = "Terms & Conditions"
            controller.strUrl = SingletonClass.sharedInstance.settingsModel?.advisorTermsCondition ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
        case settingsCategory.PrivacyPolicy.value:
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonWebViewViewController.storyboardID) as! CommonWebViewViewController
            controller.strNavTitle = "Privacy Policy"
            controller.strUrl = SingletonClass.sharedInstance.settingsModel?.privacyPolicy ?? ""
            self.navigationController?.pushViewController(controller, animated: true)
            print(myProfileCategory.MyCredit.value)
            break
        case settingsCategory.ContactUs.value:            
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserContactUsViewController.storyboardID)
            self.navigationController?.pushViewController(controller, animated: true)
            
          
            break
        default:
            break
        }
    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
    

}
class AdviserSettingsCell : UITableViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: settingsLabel!
    
}

class AdvisersettingsCategoryData {
    var settingsCategoryImage : UIImage?
    var settingsCategoryName : String?
    init(img:UIImage,name:String) {
        self.settingsCategoryImage = img
        self.settingsCategoryName = name
    }
}
enum AdvisersettingsCategory {
    case TermsCondition, PrivacyPolicy, ContactUs
    
    var value:String{
        switch self {
        case .TermsCondition:
            return "Terms & Conditions"
        case .PrivacyPolicy:
            return "Privacy Policy"
        case .ContactUs:
            return "Contact Us"
        
        }
    }
    var imageValue : UIImage {
        switch self {
        case .TermsCondition:
            return UIImage(named: "ic_messageOnSettings")!
        case .PrivacyPolicy:
            return UIImage(named: "ic_messageOnSettings")!
        case .ContactUs:
            return UIImage(named: "ic_messageOnSettings")!
    }
    }
}
