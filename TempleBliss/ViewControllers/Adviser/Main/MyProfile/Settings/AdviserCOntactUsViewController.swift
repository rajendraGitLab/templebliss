//
//  AdviserContactUsViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserContactUsViewController:BaseViewController,UITextViewDelegate, UITextFieldDelegate {
    
    //MARK: - Properties
    var customTabBarController : CustomTabBarVC?
    //MARK: - IBOutlets
    @IBOutlet weak var textFieldName: ContactUsTextField!
    @IBOutlet weak var textFieldEmail: ContactUsTextField!
    @IBOutlet weak var textFieldPhoneNumber: ContactUsTextField!
    @IBOutlet weak var btnSend: theamSubmitButton!
    @IBOutlet weak var textViewMessage: themeTextView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Contact Us", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        textFieldPhoneNumber.delegate = self
        
        
        textFieldName.superview?.isUserInteractionEnabled = false
        textFieldEmail.superview?.isUserInteractionEnabled = false
        textFieldPhoneNumber.superview?.isUserInteractionEnabled = false
        
        textFieldName.superview?.alpha = 0.6
        textFieldEmail.superview?.alpha = 0.6
        textFieldPhoneNumber.superview?.alpha = 0.6
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        textFieldName.text = "\(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? "") \(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.lastName ?? "")"
        textFieldName.attributedPlaceholder = NSAttributedString(string: "Name",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        textFieldEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        textFieldPhoneNumber.attributedPlaceholder = NSAttributedString(string: "Phone Number",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
       
        textFieldEmail.text = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.email ?? ""
        textFieldPhoneNumber.text = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.phone ?? ""
        btnSend.setTitle("Send".uppercased(), for: .normal)
    }
  
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldPhoneNumber {
            
            let charsLimit = 20

               let startingLength = textField.text?.count ?? 0
               let lengthToAdd = string.count
               let lengthToReplace =  range.length
               let newLength = startingLength + lengthToAdd - lengthToReplace

               return newLength <= charsLimit
            

        }
        return false
    }
    
    func validation()->Bool{
        let firstName = textFieldName.validatedText(validationType: ValidatorType.username(field: "full name"))
        let checkEmail = textFieldEmail.validatedText(validationType: ValidatorType.email)
        let phone = textFieldPhoneNumber.validatedText(validationType: ValidatorType.requiredField(field: "contact number"))
        
        if (!firstName.0){
            Utilities.ShowAlert(OfMessage: firstName.1)
            return firstName.0
        }else if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
        else if textFieldPhoneNumber.text?.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter phone number")
            return false
        }
        else if (textFieldPhoneNumber.text?.count ?? 0) < 10 {
            Utilities.ShowAlert(OfMessage: "Please enter valid phone number")
            return false
        } else if textViewMessage.text.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter message")
            return false
        }
        return true
    }
    //MARK: - IBActions
    
    @IBAction func btnSendClick(_ sender: Any) {
        if validation() {
            webServiceCallForContactUs()
        }
        
    }
    
   
    //MARK: - API Calls
}
extension AdviserContactUsViewController {
    func webServiceCallForContactUs() {
        
        let requstModel = contactReqModel()
        requstModel.user_id = SingletonClass.sharedInstance.UserId
        requstModel.full_name = textFieldName.text ?? ""
        requstModel.email = textFieldEmail.text ?? ""
        requstModel.phone = textFieldPhoneNumber.text ?? ""
        requstModel.message = textViewMessage.text ?? ""
        Utilities.showHud()
        WebServiceSubClass.ContactUs(addCategory: requstModel, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                
                Utilities.displayAlert("", message: json["message"].string ?? "MessageTitle".Localized(), completion: {_ in
                   
                    self.navigationController?.popViewController(animated: true)
                }, otherTitles: nil)
               
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
        
    }
}
