//
//  AdvisorMessageChatViewController.swift
//  TempleBliss
//
//  Created by Apple on 04/03/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView
import SwiftyJSON
import IQKeyboardManagerSwift
class AdvisorMessageChatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate, UITextViewDelegate {
    
    //MARK:- ======== Properties ========
    @IBOutlet weak var txtMessage: themeTextView!
    @IBOutlet weak var textHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSendMessage: UIButton!
    
    @IBOutlet weak var lblShowTimer: customerMessageTimeLabel!
    @IBOutlet weak var costomerNameLbl: UILabel!
    @IBOutlet weak var advisorNameLbl: UILabel!
    var customTabBarController : CustomTabBarVC?
   
    let NotificationSocketTypingGet = NSNotification.Name(rawValue: "NotificationSocketTypingGet")
  
   
    var RequestAcceptDataOfUser : [String : Any] = [:]
    var timer: Timer?
    var timeLeft = 5
    var senderImage = UIImage()
    var reciverImage = UIImage()
    var userName = "Lina Ryan"
    var MessageArray = [ChatHistory]()
    
    var sessionType = ""
    var bookingID = ""
    //MARK:- ======== IBOutlets ========
   
    @IBOutlet weak var viewIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var viewTyping: viewViewClearBG!
    @IBOutlet weak var communicationTypeImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var conBottomChatBox: NSLayoutConstraint!
    @IBOutlet weak var advisorImageView: UIImageView!
    @IBOutlet weak var tblShowMessageTop: NSLayoutConstraint!
    @IBOutlet weak var tblShowMessage: UITableView!
    @IBOutlet weak var textFieldSendMessage: SendMessageTextField!
    @IBOutlet weak var lblTypingStatus: RegistrationPageLabel!
   // @IBOutlet weak var tblShowMessageHeight: NSLayoutConstraint!
    //MARK:- ======== View Life Cycle Methods ========
    func getHistoryOfChat() {
        let chatHistoryReqModel = getChatHistoryReqModel()
        chatHistoryReqModel.booking_id = bookingID
        chatHistoryReqModel.chat_type = sessionType
        
        getHistory(ReqModel: chatHistoryReqModel)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        costomerNameLbl.text = "\(RequestAcceptDataOfUser["customer_name"] ?? "")"
//        advisorNameLbl.text = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? ""
        costomerNameLbl.text = ""
        advisorNameLbl.text = ""
        
        logMessage(messageText: "voice chat: message controller viewdidload")
        userImageView.contentMode = .scaleAspectFill
        advisorImageView.contentMode = .scaleAspectFill
        lblShowTimer.text = "00:00"
        if sessionType == "session" {
           
            self.lblShowTimer.isHidden = false
        } else {
             
            self.lblShowTimer.isHidden = true
        }
        txtMessage.delegate = self
        adjustTextViewHeight()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        tblShowMessage.keyboardDismissMode = .onDrag
        bookingID = "\(RequestAcceptDataOfUser["booking_id"] as? Int ?? 0)"
        getHistoryOfChat()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
         

        //Ankur's Change
        
        if SingletonClass.sharedInstance.isNoteAdded == true {
            if self.sessionType == "free" {
                self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                self.txtMessage.superview?.isHidden = true
            } else {
                self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
               
            }
        } else {
            if self.sessionType == "free" {
                self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                self.txtMessage.superview?.isHidden = true
            } else {
                self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                
            }
        }
   //Ankur's Change
        btnAddNotesClick = {
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
            controller.bookingID = self.bookingID
          //  self.customTabBarController?.hideTabBar()
            controller.StringButton = "SUBMIT"
            controller.isShowLeftIcon = true
            controller.isShowRightIcon = false
            controller.isShowNoteTextView = true
            controller.isFromAdvisorMessageController = true
            controller.stringDescription = "Add Session Note"
            controller.btnSubmitClosour = {
                //self.customTabBarController?.showTabBar()
                self.dismiss(animated: true, completion: {
                    if SingletonClass.sharedInstance.isNoteAdded == true {
                        if self.sessionType == "free" {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        } else {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                            
                        }
                    } else {
                        if self.sessionType == "free" {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        } else {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                           
                        }
                    }
                    
                    
                    let TopVC = UIApplication.topViewController()
                        if ((TopVC?.isKind(of: AdviserMySessionViewController.self)) != nil) {
                        }
                    })
                
            }
    //            controller.textForShow = "Waiting for client to add minutes..."
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            UIApplication.topViewController()?.present(navigationController, animated: true, completion: nil)
            //appDel.window?.rootViewController?
            
        }
        btnViewNotesClick = {
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
            
          //  self.customTabBarController?.hideTabBar()
            controller.bookingID = self.bookingID
            controller.StringButton = "Okay"
            controller.isShowLeftIcon = true
            controller.isShowRightIcon = true
            controller.isShowNoteTextView = false
            controller.stringDescription = SingletonClass.sharedInstance.noteText
            controller.isFromAdvisorMessageController = true
            
            controller.btnSubmitClosour = {
                self.setupKeyboard(false)
              //  self.customTabBarController?.showTabBar()
                self.dismiss(animated: true, completion: {
                    if SingletonClass.sharedInstance.isNoteAdded == true {
                        if self.sessionType == "free" {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        } else {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.viewNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        }
                    } else {
                        if self.sessionType == "free" {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        } else {
                            self.setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.AddNotes.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
                        }
                    }
                    let TopVC = UIApplication.topViewController()
                        if ((TopVC?.isKind(of: AdviserMySessionViewController.self)) != nil) {
                           
                        }
                    
                    })
            }
            controller.isDismissCLosour = {
                self.setupKeyboard(false)
                //self.customTabBarController?.showTabBar()
            }
            self.setupKeyboard(true)
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            
            UIApplication.topViewController()?.present(navigationController, animated: true, completion: nil)
        }
 
        textFieldSendMessage.sendMessageClick = { [self] in
            (textFieldSendMessage.rightView?.subviews[0] as? UIButton)?.isUserInteractionEnabled = false
            if textFieldSendMessage.text?.trim() != "" {

                
                if SocketIOManager.shared.socket.status == .connected {
                    emitSocketSendMesage(message: textFieldSendMessage.text?.trim() ?? "")
                }
                
            }
            else{
                (textFieldSendMessage.rightView?.subviews[0] as? UIButton)?.isUserInteractionEnabled = true
            }
            
           
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.getClicIsTyping(notification:)), name: NotificationSocketTypingGet, object: nil)
        setLocalization()
        setValue()
        textFieldSendMessage.delegate = self
        //tblShowMessage.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        
        textFieldSendMessage.attributedPlaceholder = NSAttributedString(string: textFieldSendMessage.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        resetALL()
        AllSocketOn()
        
     //   initUIForNavigation("\(self.RequestAcceptDataOfUser["customer_name"] ?? "")", andImage: "\(self.RequestAcceptDataOfUser["customer_profile_picture"] ?? "")")
        // Do any additional setup after loading the view.
    }
    func initUIForNavigation(_ title: String, andImage image: String) {

        let rect:CGRect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.navigationController?.navigationBar.frame.size.width ?? 0.0, height: 44))

        let titleView:UIView = UIView.init(frame: rect)
       
        let image_view:UIImageView = UIImageView()
       
        if image != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(image)")

            image_view.sd_imageIndicator = SDWebImageActivityIndicator.white
            image_view.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))

        } else {
            image_view.image = UIImage(named: "user_dummy_profile")
        }
        image_view.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        image_view.center.y = titleView.center.y
        image_view.layer.cornerRadius = image_view.bounds.size.width / 2.0
        image_view.layer.masksToBounds = true
        titleView.addSubview(image_view)
        
        /* label */
        let label:UILabel = UILabel.init(frame: CGRect(x: 45, y: 0, width: rect.width - 170, height: 40))

        label.text = title
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        label.center.y = titleView.center.y
        label.textAlignment = .left
        titleView.addSubview(label)

        self.navigationItem.titleView = titleView

    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        logMessage(messageText: "voice chat: message controller viewDidDisappear")
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SingletonClass.sharedInstance.WorkingChat = false
        if let provider = appDel.callKitProvider {
            provider.reportCall(with: UUID(), endedAt: Date(), reason: .declinedElsewhere)
            appDel.callKitProvider?.invalidate()
           
           
        }
        SingletonClass.sharedInstance.noteText = ""
        setupKeyboard(true)
        self.deregisterFromKeyboardNotifications()
       
        SocketIOManager.shared.socket.off(socketApiKeys.all_message_read.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.send_message.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.check_typing.rawValue)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       
        //        if self.isMovingFromParent {
        //          SocketIOManager.shared.closeConnection()
        //            allSocketOffMethods()
        //        }
    }
    override func viewWillAppear(_ animated: Bool) {
        SingletonClass.sharedInstance.WorkingChat = true
        if UIApplication.shared.applicationState == .active {
            if let provider = appDel.callKitProvider {
                provider.reportCall(with: UUID(), endedAt: Date(), reason: .answeredElsewhere)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                    appDel.callKitProvider?.invalidate()
                })
                //
               
               
            }
        }
        
        if sessionType == "session" {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
        setupKeyboard(false)
        customTabBarController?.hideTabBar()
        self.registerForKeyboardNotifications()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        AllSocketOn()
        EmitAllmsgRead()
    }
    //MARK: -  notification center for Keyboard Hide and Show
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func registerForKeyboardNotifications(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func animateConstraintWithDuration(duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.loadViewIfNeeded() ?? ()
        })
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        
        let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) + 15
        
        if #available(iOS 11.0, *) {
            conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) - view.safeAreaInsets.bottom + 35
        } else {
            conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) + 35
        }
        self.animateConstraintWithDuration()
        if MessageArray.count != 0 {
            self.tblShowMessage.scrollToBottom()

        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
         conBottomChatBox.constant = 35
        self.animateConstraintWithDuration()
        if MessageArray.count != 0 {
            self.tblShowMessage.scrollToBottom()

        }
    }
    func setupKeyboard(_ enable: Bool) {
        IQKeyboardManager.shared.enable = enable
        IQKeyboardManager.shared.enableAutoToolbar = enable
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = !enable
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    //MARK:- ======== other methods ========
    func textViewDidChange(_ textView: UITextView) {
        EmitSocketTyping()
        adjustTextViewHeight()
    }
    func adjustTextViewHeight()
    {
        
        let fixedWidth = txtMessage.frame.size.width
        let newSize = txtMessage.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))

        let numLines = Int(txtMessage.contentSize.height / (txtMessage.font?.lineHeight)! )
        print(numLines)
        print("textFieldHeight: \(newSize.height)")
        self.textHeightConstraint?.constant = newSize.height
        
        if numLines <= 4 {
            self.txtMessage.isScrollEnabled = false
            let difference = newSize.height - 44 //new line
            self.textHeightConstraint?.constant = newSize.height
            self.containerHeightConstraint?.constant = 56 + difference //new line
        }
        else {
            self.txtMessage.isScrollEnabled = true
            
        }
        self.view.layoutIfNeeded()
        
    }
    
    func resetALL() {
        self.viewTyping.isHidden = true
    }
    
    func setLocalization() {
        
    }
    func setValue() {
        if RequestAcceptDataOfUser["customer_profile_picture"] as? String != ""{
            let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["customer_profile_picture"] ?? "")"
            userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
        }else{
            userImageView.image = UIImage.init(named: "user_dummy_profile")
        }
        
        if RequestAcceptDataOfUser["advisor_profile_picture"] as? String != ""{
            let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["advisor_profile_picture"] ?? "")"
            advisorImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            advisorImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
           
          
        }else{
            advisorImageView.image = UIImage.init(named: "user_dummy_profile")
           
        }
        senderImage = advisorImageView.image ?? UIImage()
        reciverImage = userImageView.image ?? UIImage()
//        switch RequestAcceptDataOfUser["type"] as? String {
//        case "chat":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_chat")
//        case "audio":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_audioCall")
//        case "video":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_videoCall")
//        default:
//            break
//        }
        
        communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_chat")
        
    }
    //MARK:- ======== tableViewMethods ========
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageArray.count
      //  return MessageArray[section].MessageData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if MessageArray[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "Customer" || MessageArray[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "customer" {
            let SenderCell = tblShowMessage.dequeueReusableCell(withIdentifier: MessageSenderCell.reuseIdentifier, for: indexPath) as! MessageSenderCell
           
            
            SenderCell.viewBG.backgroundColor = UIColor(hexString: "#273F4D")
            SenderCell.chatMessage.text = MessageArray[indexPath.row].ChatMessage["message"] as? String ?? ""
            if RequestAcceptDataOfUser["customer_profile_picture"] as? String != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["customer_profile_picture"] ?? "")"
                SenderCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                SenderCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
               
              
            }else{
                SenderCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
               
            }
          
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: MessageArray[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
            inputFormatter.dateFormat = "h:mm a"
            let resultString = inputFormatter.string(from: showDate)
            print(resultString)
            
            
            SenderCell.lblTime.text = resultString
//            SenderCell.isHidden = true
            // Create Date Formatter
           
            cell = SenderCell
        } else {
            let ReciverCell = tblShowMessage.dequeueReusableCell(withIdentifier: MessageReciverCell.reuseIdentifier, for: indexPath) as! MessageReciverCell
            ReciverCell.viewBG.backgroundColor = UIColor(hexString: "#5A7685")
            ReciverCell.chatMessage.text = MessageArray[indexPath.row].ChatMessage["message"] as? String ?? ""
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: MessageArray[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
            inputFormatter.dateFormat = "h:mm a"
            let resultString = inputFormatter.string(from: showDate)
            print(resultString)
            ReciverCell.chatMessageReadStatus?.text = MessageArray[indexPath.row].readStatus == 1 ? "Read" : "Sent"
            
            ReciverCell.lblTime.text = resultString
            //ReciverCell.profileImage.image = reciverImage
            
            if RequestAcceptDataOfUser["advisor_profile_picture"] as? String != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["advisor_profile_picture"] ?? "")"
                ReciverCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                ReciverCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
            }else{
                ReciverCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
            }
//            ReciverCell.isHidden = true
            cell = ReciverCell
        }
        
//        if ((MessageArray[indexPath.section].MessageData?[indexPath.row].isFromSender) != nil) {
//
//            SenderCell.chatMessage.text = MessageArray[indexPath.section].MessageData?[indexPath.row].chatMessage
//            SenderCell.lblTime.text = MessageArray[indexPath.section].MessageData?[indexPath.row].chatTime
//            SenderCell.profileImage.image = senderImage
//
//
//        } else {
//
//            ReciverCell.chatMessage.text = MessageArray[indexPath.section].MessageData?[indexPath.row].chatMessage
//            ReciverCell.lblTime.text = MessageArray[indexPath.section].MessageData?[indexPath.row].chatTime
//            ReciverCell.profileImage.image = reciverImage
//
//        }
       
        return cell
       
    }
    
    //MARK:- ======== IBActions ========
    @IBAction func btnSendMessageClick(_ sender: Any) {
      btnSendMessage.isUserInteractionEnabled = false
        if txtMessage.text?.trim() != "" {

            
            if SocketIOManager.shared.socket.status == .connected {
                emitSocketSendMesage(message: txtMessage.text?.trim() ?? "")
            }
            
        }
        else{
            btnSendMessage.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- ======== API Calls ========
    
    func getHistory(ReqModel : getChatHistoryReqModel) {
        WebServiceSubClass.GetChatHistory(addCategory: ReqModel, completion: {(json, status, response) in
            if status {
                
                
                
                self.MessageArray = []
                let chatHistoryRes = ChatHistoryResModel.init(fromJson: json)
                
                chatHistoryRes.chatHistory.forEach { (element) in
                    
                    
                    let messageData : [String:Any] = ["sender_type": element.senderType ?? "","receiver_type":element.receiverType ?? "","current_date":element.bookingDate ?? "","message_send_date":element.messageDate ?? "","message":element.message ?? "","booking_id":Int(element.bookingId) ?? 0,"sender_id":Int(element.senderId) ?? 0,"receiver_id":Int(element.receiverId) ?? 0]
                    
                    self.MessageArray.append(ChatHistory(ReadStatus: Int(element.readStatus) ?? 0, FullName: element.fullName, NickName:element.nickName, Profile: element.profilePicture, message: messageData))
                }
                DispatchQueue.main.async {
                    
                    self.MessageArray = self.MessageArray.reversed()
                    
                    if self.MessageArray.count != 0 {
                        self.tblShowMessage.reloadData()
                        DispatchQueue.main.async {
                            self.tblShowMessage.scrollToBottom()
                        }
                        if self.sessionType == "free" {
                            self.txtMessage.superview?.isHidden = false
                        }
                        
                        
                      
                    } else {
                        if self.sessionType == "free" {
                            self.txtMessage.superview?.isHidden = true
                        }
                        
                    }

                }
                
                self.EmitAllmsgRead()
                
                
            }else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
             
            }
            
            
        })
    }
    //MARK:- ======== SocketsMethods ========
    
    
    //MARK:- ======== TextViewc Delegate Methods  ==========
    
    //MARK:- ====== Emit socket Check Typing =======
    func EmitSocketTyping(){
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
            "sender_type" : "Advisor",
            "receiver_id" : RequestAcceptDataOfUser["user_id"] as? Int ?? 0,
            "receiver_type" : "Customer",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0
            ] as [String : Any]
        
        print(param)
      print(RequestAcceptDataOfUser)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.check_typing.rawValue, with: param)
    }
    func onSocketTyping() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.check_typing.rawValue) { (json) in
            print(#function)
            print(json)
            self.viewTyping?.isHidden = false
            print(json)
            print("Get on for Typing")
            let dict = json[0]
            if dict["booking_id"].int ?? 0 == self.RequestAcceptDataOfUser["booking_id"] as? Int ?? 0 {
                self.viewTyping.isHidden = false
                NotificationCenter.default.post(name: self.NotificationSocketTypingGet, object: json)
                if self.MessageArray.count != 0 {
                    self.tblShowMessage.scrollToBottom()
                }
            }
            else{
                self.viewTyping.isHidden = true
                return
            }
        }
    }
    @objc func getClicIsTyping(notification:Notification) -> Void {
        
        let message =   JSON(notification.object as Any).array?.first?.dictionary?["message"]?.string ?? ""
        lblTypingStatus.text = "\(message)"

        viewTyping.isHidden = false
        timeLeft = 2
        if timer == nil {
            loadTyping()
        }
    }
    func loadTyping() {
        
        self.viewIndicator.type = .ballPulseSync
        self.viewIndicator.color = colors.white.value
        self.viewIndicator.startAnimating()
        
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                
                self.timeLeft -= 1
                if(self.timeLeft <= 0) {
                    self.timer?.invalidate()
                    timer.invalidate()
                    self.timer = nil
                    self.viewIndicator.stopAnimating()
                    self.viewTyping.isHidden = true
                }
            }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
        EmitSocketTyping()
    }
    
    func AllSocketOn() {
        OnSocketToReciveMesage()
        onSocketAllmsgRead()
        onSocketTyping()
       
       
    }
    //MARK:- ========== Emit Socket All msg Read =======
    func EmitAllmsgRead(){
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
            "sender_type" : "Advisor",
            "receiver_id" : RequestAcceptDataOfUser["user_id"] as? Int ?? 0,
            "receiver_type" : "Customer",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,
            ] as [String : Any]
        SocketIOManager.shared.socketEmit(for: socketApiKeys.all_message_read.rawValue, with: param)
    }
    
    
    func emitSocketSendMesage(message : String){
        
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,
            "sender_type" : "Advisor",
            "receiver_id" : RequestAcceptDataOfUser["user_id"] as? Int ?? 0,
            "receiver_type" : "Customer",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,
            "message" : message
            ] as [String : Any]
        
        print(param)
        resetALL()
        btnSendMessage.isUserInteractionEnabled = true
        SocketIOManager.shared.socketEmit(for: socketApiKeys.send_message.rawValue, with: param)
        
//
//        let messageData : [String:Any] = ["booking_id":RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,"sender_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"message":textFieldSendMessage.text ?? "","message_send_date":"","receiver_id":RequestAcceptDataOfUser["user_id"] as? Int ?? 0,"current_date":"","sender_type":"Advisor","receiver_type":"Customer"]
//
//        self.MessageArray.append(ChatHistory(FullName: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? "", NickName: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "", Profile: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture ?? "", message: messageData))
        
        txtMessage.text = ""
        adjustTextViewHeight()
//        let indexPath = IndexPath(row: self.MessageArray.count - 1, section: 0)
//        self.tblShowMessage.insertRows(at: [indexPath], with: .bottom)
//        let path = IndexPath.init(row: self.MessageArray.count - 1, section: 0)
//        self.tblShowMessage.scrollToRow(at: path, at: .bottom, animated: true)
    }

    
    
    func OnSocketToReciveMesage(){
       
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.send_message.rawValue) { (json) in
            print(#function)
            print("Got on for send message")
            print(json)
            
            if let arrResponse = json.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["booking_id"].int == self.RequestAcceptDataOfUser["booking_id"] as? Int ?? 0 {
                        let userInfo = singleResponse["user"].array?.first
                        
                    
                        let messageData : [String:Any] = ["booking_id":singleResponse["booking_id"].int ?? 0,"sender_id":singleResponse["sender_id"].int ?? 0,"message":singleResponse["message"].string ?? "","message_send_date":singleResponse["message_send_date"].string ?? "","receiver_id":singleResponse["receiver_id"].int ?? 0,"current_date":singleResponse["current_date"].string ?? "","sender_type":singleResponse["sender_type"].string ?? "","receiver_type":singleResponse["receiver_type"].string ?? ""]
                        
                        self.MessageArray.append(ChatHistory(ReadStatus: 0, FullName: userInfo?["full_name"] as? String ?? "", NickName: userInfo?["nick_name"] as? String ?? "", Profile: userInfo?["profile_picture"] as? String ?? "", message: messageData))
                        
                        
                        if self.MessageArray.count != 0 {
                            self.tblShowMessage.reloadData()
                            DispatchQueue.main.async {
                                self.tblShowMessage.scrollToBottom()
                            }
                            self.txtMessage.superview?.isHidden = false

                        } else {
                            self.txtMessage.superview?.isHidden = true
                        }

                        self.EmitAllmsgRead()

                        
                    }
                    else {
                        
                        return
                    }
                    
                }
            }
            
        }
    }
    

    //MARK:- ====== On socket Read all message =======
    func onSocketAllmsgRead(){
        SocketIOManager.shared.socketCall(for: socketApiKeys.all_message_read.rawValue) { (json) in
            print(#function)
            print(json)
            if self.MessageArray.count != 0 {
                for i in 0..<self.MessageArray.count{
                    if self.MessageArray[i].readStatus == 0{
                        self.MessageArray[i].readStatus = 1
                        let indexpath = IndexPath.init(row: i, section: 0)
                        self.tblShowMessage?.reloadRows(at: [indexpath], with: .none)
                    }
                }
            }
        }
    }
}


class ChatHistory {
    var readStatus : Int?
    var ChatMessage : [String:Any] = [:]
    var ProfileImage : String?
    var full_name : String?
    var nick_name : String?
    
    init(ReadStatus:Int,FullName:String,NickName:String,Profile:String,message:[String:Any]) {
        self.readStatus = ReadStatus
        self.full_name = FullName
        self.nick_name = NickName
        self.ProfileImage = Profile
        self.ChatMessage = message
    }
}
