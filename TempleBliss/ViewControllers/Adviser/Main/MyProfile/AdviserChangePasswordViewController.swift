//
//  AdviserChangePasswordViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserChangePasswordViewController: BaseViewController {
    
    //MARK: - Properties
    var customTabBarController : CustomTabBarVC?
    //MARK: - IBOutlets
    
    @IBOutlet weak var textFieldCurrentPassword: changePasswodTextField!
    @IBOutlet weak var textFieldNewPassword: changePasswodTextField!
    @IBOutlet weak var textFieldConfirmPassword: changePasswodTextField!
    @IBOutlet weak var btnSave: theamSubmitButton!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Change Password", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        textFieldCurrentPassword.placeholder = "Current Password"
        textFieldNewPassword.placeholder = "New Password"
        textFieldConfirmPassword.placeholder = "Confirm Password"
        btnSave.setTitle("SAVE", for: .normal)
    }
    //MARK: - IBActions
    
    @IBAction func btnSaveClick(_ sender: Any) {
//        self.navigationController?.popViewController(animated: true)
        if validations(){
            webservice_ChangePW()
        }
    }
    //MARK: - API Calls
    func webservice_ChangePW(){
        let changePWReqModel = ChangePasswordReqModel()
        changePWReqModel.old_password = textFieldCurrentPassword.text ?? ""
        changePWReqModel.new_password = textFieldNewPassword.text ?? ""
        changePWReqModel.new_confirm_password = textFieldConfirmPassword.text ?? ""
        changePWReqModel.user_id = SingletonClass.sharedInstance.UserId
        Utilities.showHud()
        WebServiceSubClass.ChangePassword(changepassModel: changePWReqModel, showHud: false, completion: { (response, status, error) in
            Utilities.hideHud()
            if status{
                self.showAlertWithTwoButtonCompletion(title: AppName, Message: response["message"].stringValue, defaultButtonTitle: "OK", cancelButtonTitle: "") { (index) in
                    if index == 0{
                        self.navigationController?.popViewController(animated: true)
                       // appDel.navigateToLogin()
                     //   appDel.performLogout()
                    }
                }
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func validations()->Bool{
        let currentPW = textFieldCurrentPassword.validatedText(validationType: ValidatorType.requiredField(field: "current password"))
        let newPW =  textFieldNewPassword.validatedText(validationType: ValidatorType.requiredField(field: "new password"))
         let confirmPW = textFieldConfirmPassword.validatedText(validationType: ValidatorType.requiredField(field: "confirm password"))
        if (!currentPW.0){
            Utilities.ShowAlert(OfMessage: currentPW.1)
            return currentPW.0
        }else if (!newPW.0){
            Utilities.ShowAlert(OfMessage: newPW.1)
            return newPW.0
        }else if (!confirmPW.0){
            Utilities.ShowAlert(OfMessage: confirmPW.1)
            return confirmPW.0
        }else if (textFieldNewPassword.text?.count ?? 0) <= 7{
            Utilities.ShowAlert(OfMessage: "New Password must contain at least 8 characters")
            return false
        }else if textFieldNewPassword.text?.lowercased() != textFieldConfirmPassword.text?.lowercased(){
            Utilities.ShowAlert(OfMessage: "Password and confirm password must be same")
            return false
        } else if textFieldCurrentPassword.text == textFieldNewPassword.text {
            Utilities.ShowAlert(OfMessage: "Current password and new password cannot be same")
            return false
        }
        return true
        
    }
    func showAlertWithTwoButtonCompletion(title:String, Message:String, defaultButtonTitle:String, cancelButtonTitle:String ,  Completion:@escaping ((Int) -> ())) {
        
        let alertController = UIAlertController(title: title , message:Message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: defaultButtonTitle, style: .default) { (UIAlertAction) in
            Completion(0)
        }
        if cancelButtonTitle != ""{
            let CancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (UIAlertAction) in
                Completion(1)
            }
            alertController.addAction(OKAction)
            alertController.addAction(CancelAction)
        }else{
            alertController.addAction(OKAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    
}
