//
//  AdviserProfileEditViewController.swift
//  TempleBliss
//
//  Created by Apple on 29/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage

protocol EditProfileAdvisorDelegate {
    func refereshProfileScreen()
}
var categoryDataForAdviserProfile : [profileUpdateCategoryData] = []
class AdviserProfileEditViewController:  BaseViewController, UITextViewDelegate {
    
    //MARK: -  Properties
    var categoryRequestModel : [CategoryUpdateRequestModel] = []
    var isUpdateAny : Bool = false
    var delegateEdit : EditprofileAdvisorReqModel!
    private var imagePicker : ImagePicker!
    var selectedImage : UIImage?
    
    var customTabBarController : CustomTabBarVC?
    var isRemovePhoto = false
   // var priceStartFrom = SingletonClass.sharedInstance.categoryDataForAdviser?.category[0].priceRange[0].from ?? ""
    
    var selectedOldImage : UIImage?
    //MARK: -  IBOutlets
    @IBOutlet weak var tblAddCategory: UITableView!
    @IBOutlet weak var btnSave: theamSubmitButton!
    @IBOutlet weak var btnUpdateProfile: UIButton!
    
    @IBOutlet weak var tblAddCategoryHeight: NSLayoutConstraint!
    @IBOutlet weak var userImageView: myProfileImageView!
    @IBOutlet weak var textFieldFullName: AdviserEditProfileTextField!
    @IBOutlet weak var textFieldNickName: AdviserEditProfileTextField!
    @IBOutlet weak var textFieldEmailAddress: AdviserEditProfileTextField!
    @IBOutlet weak var TextFieldPhoneNumber: AdviserEditProfileTextField!
    @IBOutlet weak var textViewDescription: themeTextView!
    @IBOutlet weak var viewInScrollView: viewViewClearBG!
    //MARK: -  View Life Cycle Methods
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblAddCategory.layer.removeAllAnimations()
        tblAddCategoryHeight.constant = tblAddCategory.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categoryDataForAdviserProfile.removeAll()
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Edit Profile", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self, allowsEditing: true)
        
        textFieldEmailAddress.isUserInteractionEnabled = false
        textFieldEmailAddress.superview?.alpha = 0.6
        
        TextFieldPhoneNumber.isUserInteractionEnabled = false
        TextFieldPhoneNumber.superview?.alpha = 0.6
        
        textFieldFullName.isUserInteractionEnabled = false
        textFieldFullName.superview?.alpha = 0.6
        
        textFieldNickName.isUserInteractionEnabled = false
        textFieldNickName.superview?.alpha = 0.6
        self.tblAddCategory.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        DispatchQueue.main.async { [self] in
            
            tblAddCategory.reloadData()
            tblAddCategoryHeight.constant = tblAddCategory.contentSize.height
        }
        textViewDescription.delegate = self

        
        
//        let NewCategoryID = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.categoryId.split(separator: ",")
//
//        NewCategoryID?.forEach({ (elementNewCategoryID) in
//            let foundItems = SingletonClass.sharedInstance.LoginRegisterUpdateData?.availability.filter { $0.categoryId == elementNewCategoryID }
//
//
//            if foundItems?.count != 0 {
//
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "yyyy-MM-dd"
//                //DateFormatterString.onlyDate.rawValue
//
//                var dateArray:[Date] = []
//                for jj in 0...(foundItems?.count ?? 0) - 1 {
//
//
//                    let date1:Date = dateFormatter.date(from: foundItems?[jj].lastUpdatedDate ?? "") ?? SingletonClass.sharedInstance.TodayDate
//                    //let date1:Date = dateFormatter.date(from: foundItems?[jj].lastUpdatedDate ?? "") ?? Date()
//
//                    dateArray.append(date1)
//
//
//                }
//                print(dateArray)
//                var maxDate = dateArray[0]
//                for ii in 0...dateArray.count-1
//                {
//                    if(maxDate.compare(dateArray[ii]).rawValue == -1){
//                        maxDate = dateArray[ii]
//                        print(maxDate)
//                    }
//                    print(maxDate)
//                }
//                var tempNewData : [profileUpdateSubCategoryData] = []
//
//
////                    for j in 0...(foundItems?.count ?? 0) - 1 {
//                    let formatter = DateFormatter()
//
//                    formatter.dateFormat = DateFormatterString.onlyDate.rawValue
//
//                    let myString = formatter.string(from: maxDate)
//
//                let matches1 = foundItems?.filter( {$0.type == "chat"})
//
//                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//
//
//                let matches2 = foundItems?.filter( {$0.type == "audio"})
//
//                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//
//            //    communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), name: indexColorForCommunicationType.audio.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.audio.setColorForCommunicationType()), price: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//               // communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), name: indexColorForCommunicationType.audio.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.audio.setColorForCommunicationType()), price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
////                        }
////                    })
//
////                    foundItems?.forEach({ (element) in
////                        if element.type == "video" {
//                let matches3 = foundItems?.filter( {$0.type == "video"})
//
//                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//              //  communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_videoCall") ?? UIImage(), name: indexColorForCommunicationType.video.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.video.setColorForCommunicationType()), price: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//
//
//                //communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_chat") ?? UIImage(), name: indexColorForCommunicationType.chat.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.chat.setColorForCommunicationType()), price: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00)))
//
//
////                        foundItems?.forEach({ (element) in
////                            if element.type == "chat" {
////                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
////                            }
////                        })
////
////                        foundItems?.forEach({ (element) in
////                            if element.type == "audio" {
////                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
////                            }
////                        })
////
////                        foundItems?.forEach({ (element) in
////                            if element.type == "video" {
////                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
////                            }
////                        })
//
//
//            //}
//                if SingletonClass.sharedInstance.categoryDataForAdviser?.category.count != 0 {
//                    SingletonClass.sharedInstance.categoryDataForAdviser?.category.forEach({ (elementCategory) in
//                        if elementCategory.id == elementNewCategoryID
//                        {
//                            categoryDataForAdviserProfile.append(profileUpdateCategoryData(id: elementCategory.id, newCategoryID: elementCategory.id, name: elementCategory.name ?? "", expand: false, categoryData: tempNewData))
//                        }
//                    })
//                }
//
//
//
//            }
//
//        })
        
        
        
        
        
        
        
        
        if SingletonClass.sharedInstance.categoryDataForAdviser?.category.count != 0 {
            for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 6) - 1
            {

                let foundItems = SingletonClass.sharedInstance.LoginRegisterUpdateData?.availability.filter { $0.categoryId == SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id }

                if foundItems?.count != 0 {

                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    //DateFormatterString.onlyDate.rawValue

                    var dateArray:[Date] = []
                    for jj in 0...(foundItems?.count ?? 0) - 1 {


                        let date1:Date = dateFormatter.date(from: foundItems?[jj].lastUpdatedDate ?? "") ?? (SingletonClass.sharedInstance.TodayDate )
                        //let date1:Date = dateFormatter.date(from: foundItems?[jj].lastUpdatedDate ?? "") ?? Date()

                        dateArray.append(date1)


                    }
                    print(dateArray)
                    var maxDate = dateArray[0]
                    for ii in 0...dateArray.count-1
                    {
                        if(maxDate.compare(dateArray[ii]).rawValue == -1){
                            maxDate = dateArray[ii]
                            print(maxDate)
                        }
                        print(maxDate)
                    }
                    var tempNewData : [profileUpdateSubCategoryData] = []


//                    for j in 0...(foundItems?.count ?? 0) - 1 {
                        let formatter = DateFormatter()

                        formatter.dateFormat = DateFormatterString.onlyDate.rawValue

                        let myString = formatter.string(from: maxDate)

                    let matches1 = foundItems?.filter( {$0.type == "chat"})

                    tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00)))



                    let matches2 = foundItems?.filter( {$0.type == "audio"})

                    tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00)))


                //    communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), name: indexColorForCommunicationType.audio.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.audio.setColorForCommunicationType()), price: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00)))

                   // communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), name: indexColorForCommunicationType.audio.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.audio.setColorForCommunicationType()), price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
//                        }
//                    })

//                    foundItems?.forEach({ (element) in
//                        if element.type == "video" {
                    let matches3 = foundItems?.filter( {$0.type == "video"})

                    tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00)))

                  //  communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_videoCall") ?? UIImage(), name: indexColorForCommunicationType.video.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.video.setColorForCommunicationType()), price: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00)))



                    //communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_chat") ?? UIImage(), name: indexColorForCommunicationType.chat.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.chat.setColorForCommunicationType()), price: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00)))


//                        foundItems?.forEach({ (element) in
//                            if element.type == "chat" {
//                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
//                            }
//                        })
//
//                        foundItems?.forEach({ (element) in
//                            if element.type == "audio" {
//                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
//                            }
//                        })
//
//                        foundItems?.forEach({ (element) in
//                            if element.type == "video" {
//                                tempNewData.append(profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00), date: myString, priceNew: String(format: "%.2f", ((element.price as NSString?)?.floatValue) ?? 0.00)))
//                            }
//                        })


                //}
                    categoryDataForAdviserProfile.append(profileUpdateCategoryData(id: SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id ?? "", newCategoryID: SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id ?? "", name: SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].name ?? "", expand: true, categoryData: tempNewData))
                }


            }
        }
        
        if categoryDataForAdviserProfile.count < Int(SingletonClass.sharedInstance.initDataForAdviser?.categoryCount ?? "0") ?? 0 {
            
            AddNewCategoryCount(total: Int(SingletonClass.sharedInstance.initDataForAdviser?.categoryCount ?? "0") ?? 0)
        } else {
            tblAddCategory.reloadData()
        }
        
        // Do any additional setup after loading the view.
    }
    func scrollTextViewToBottom(textView: UITextView) {
        if textView.text.count > 0 {
            let location = textView.text.count - 1
            let bottom = NSMakeRange(location, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        
        
        customTabBarController?.hideTabBar()
    }
    func AddNewCategoryCount(total:Int) {
        
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = DateFormatterString.onlyDate.rawValue
        
        
        let dateNow =  SingletonClass.sharedInstance.TodayDate
       // let dateNow =  Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date()) ?? Date()
        let myString = formatter.string(from: dateNow) // string purpose I add here
       
        categoryDataForAdviserProfile.append(profileUpdateCategoryData(id: "0", newCategoryID: "0", name: "Select Category", expand: false, categoryData: [profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: "0.00", date: myString, priceNew: "")]))
        
        if categoryDataForAdviserProfile.count != total {
            
            AddNewCategoryCount(total: Int(SingletonClass.sharedInstance.initDataForAdviser?.categoryCount ?? "0") ?? 0)
        } else {
            tblAddCategory.reloadData()
        }
        
    }
    
    //MARK: -  other methods
    func refereshProfileScreen() {
        setValue()
    }
    func setLocalization() {
        
    }
    func setValue() {
        if let userdata = SingletonClass.sharedInstance.LoginRegisterUpdateData{
            textFieldFullName.text = userdata.profile.fullName ?? ""
            textFieldEmailAddress.text = userdata.profile.email ?? ""
            TextFieldPhoneNumber.text = userdata.profile.phone ?? ""
            textFieldNickName.text = userdata.profile.nickName ?? ""
            
            textViewDescription.text = userdata.profile.advisorDescription
            
            if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(userdata.profile.profilePicture ?? "")"
                userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
                userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
                selectedImage = userImageView.image
                selectedOldImage = selectedImage
            }else{
                userImageView.image = UIImage.init(named: "user_dummy_profile")
                selectedOldImage = selectedImage
            }
            
        }
    }
    
    //MARK: -  IBActions
    
    @IBAction func btnSaveClick(_ sender: theamSubmitButton) {
        isUpdateAny = false
        categoryRequestModel.removeAll()
        if !textFieldFullName.isEnabled{
            (sender as AnyObject).setImage(#imageLiteral(resourceName: "imgUpdateDone"), for: .normal)
        }else{
            if validation(){
                let validationTbl = validationForTbl()
                if validationTbl {
                    
                    if categoryRequestModel.count < 3 {
                        Utilities.ShowAlert(OfMessage: "Please select atleast one category")
                        
                    } else {
                        if isUpdateAny {
                            setRequestMdoel()
                        }else {
                            if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName != textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" || SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName != textFieldNickName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" || SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.advisorDescription.trimmingCharacters(in: .whitespacesAndNewlines) != textViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines) {
                                setRequestMdoel()
                            } else {
                                
                                if selectedOldImage == nil && isRemovePhoto {
                                     self.navigationController?.popViewController(animated: true)
                                } else {
                                    if selectedOldImage != nil && isRemovePhoto {
                                        setRequestMdoel()
                                    } else if selectedOldImage == selectedImage {
                                        self.navigationController?.popViewController(animated: true)
                                    } else if (selectedOldImage == selectedImage) == false {
                                        setRequestMdoel()
                                    } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
                                        setRequestMdoel()
                                    } else {
                                         self.navigationController?.popViewController(animated: true)
                                    }
                                }
                            }
                        }
                    }
                    
                    

                } else {
                    Utilities.ShowAlert(OfMessage: "")
                }
            }
        }
    }
    @IBAction func btnChangeUserProfileClick(_ sender: UIButton) {
        resignFirstResponder()
        if (self.userImageView.image != nil || self.selectedImage != nil) && ((self.userImageView.image?.isEqualToImage(UIImage.init(named: "user_dummy_profile")!)) != nil){
            self.imagePicker.present(from: self.userImageView, viewPresented: self.view, isRemove: true)
        } else {
            self.imagePicker.present(from: self.userImageView, viewPresented: self.view, isRemove: false)
        }
    }
    //MARK: -  validations
    func validation()->Bool{
        let FullName = textFieldFullName.validatedText(validationType: ValidatorType.username(field: "full name") )//ValidatorType.requiredField(field: "first name"))
        let Email = textFieldEmailAddress.validatedText(validationType: ValidatorType.email)
        let phone = TextFieldPhoneNumber.validatedText(validationType: ValidatorType.requiredField(field: "contact number"))
        let NickName = textFieldNickName.validatedText(validationType: ValidatorType.username(field: "nick name"))
        
        if (!FullName.0){
            Utilities.ShowAlert(OfMessage: FullName.1)
            return FullName.0
        }else if (!NickName.0){
            Utilities.ShowAlert(OfMessage: NickName.1)
            return NickName.0
        }
        else if (!Email.0){
            Utilities.ShowAlert(OfMessage: Email.1)
            return Email.0
        }
        else if (!phone.0){
            Utilities.ShowAlert(OfMessage: phone.1)
            return phone.0
        }else if textViewDescription.text.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter description")
            return false
        }
        
        return true
    }
    
    
    func CheckCategoryDuplicate() -> (String,Bool) {
        var validateMessage = ("",false)
        print(#function)
        var categoryNameArray : [String] = []
        categoryDataForAdviserProfile.forEach { (element) in
            if element.categoryName != "" && element.categoryName != "Select Category" {
                categoryNameArray.append(element.categoryName ?? "")
            }
        }
        let hasDuplicates = categoryNameArray.count != Set(categoryNameArray).count
        
        if hasDuplicates {
            validateMessage = ("Sorry! You can not select same category multiple time",false)
            
        } else {
            validateMessage = ("",true)
        }
        
        return validateMessage
    }
    
    
    
    func validationForTbl() -> Bool {
        let checkDuplication = CheckCategoryDuplicate()
        if checkDuplication.1 {
            
            let checkPrice = checkPriceIsinRangeOrNot()
            if checkPrice.1 {
                
                for i in 0...categoryDataForAdviserProfile.count - 1 {

                    if categoryDataForAdviserProfile[i].categoryName != "Select Category" {
                    for j in 0...(categoryDataForAdviserProfile[i].subCategoryData?.count ?? 0) - 1 {

                        print(j)

                            let element = categoryDataForAdviserProfile[i]
                            let subElement = categoryDataForAdviserProfile[i].subCategoryData?[j]
                            
                            switch subElement?.subCategoryName {
                            case indexColorForCommunicationType.chat.CommunicationTypeName:
                                let newData = CategoryUpdateRequestModel(idOld: element.OldCategoryID ?? "", idNew: element.NewCategoryID ?? "", CategoryType: "chat", CategoryPrice: subElement?.subCategoryPriceOld?.replacingOccurrences(of: Currency, with: "") ?? "", newPrice: subElement?.subCategoryPriceNew?.replacingOccurrences(of: Currency, with: "") ?? "", lastUpdateDate: subElement?.subLastUpdatedDate ?? "")
                                
                                categoryRequestModel.append(newData)
                            case indexColorForCommunicationType.audio.CommunicationTypeName:
                                let newData = CategoryUpdateRequestModel(idOld: element.OldCategoryID ?? "", idNew: element.NewCategoryID ?? "", CategoryType: "audio", CategoryPrice: subElement?.subCategoryPriceOld?.replacingOccurrences(of: Currency, with: "") ?? "", newPrice: subElement?.subCategoryPriceNew?.replacingOccurrences(of: Currency, with: "") ?? "", lastUpdateDate: subElement?.subLastUpdatedDate ?? "")
                                
                                categoryRequestModel.append(newData)
                            case indexColorForCommunicationType.video.CommunicationTypeName:
                                let newData = CategoryUpdateRequestModel(idOld: element.OldCategoryID ?? "", idNew: element.NewCategoryID ?? "", CategoryType: "video", CategoryPrice: subElement?.subCategoryPriceOld?.replacingOccurrences(of: Currency, with: "") ?? "", newPrice: subElement?.subCategoryPriceNew?.replacingOccurrences(of: Currency, with: "") ?? "", lastUpdateDate: subElement?.subLastUpdatedDate ?? "")
                                
                                categoryRequestModel.append(newData)
                            default:
                                break
                            }
                            
                        }

                    }
                }

                categoryDataForAdviserProfile.forEach { (element) in
                    
                    if element.categoryName != "Select Category" {
                        if element.NewCategoryID != element.OldCategoryID {
                            isUpdateAny = true
                            
                        }
                        element.subCategoryData?.forEach({ (subElement) in
                            if subElement.subCategoryPriceNew != subElement.subCategoryPriceOld {
                                isUpdateAny = true
                                
                            }
                        })
                    }
                }
                
                if categoryRequestModel.count != SingletonClass.sharedInstance.LoginRegisterUpdateData?.availability.count {
                    isUpdateAny = true
                }

                
            } else {
                Utilities.ShowAlert(OfMessage: checkPrice.0)
                return false
            }
            
            
        } else {
            Utilities.ShowAlert(OfMessage: checkDuplication.0)
            return false
        }
        
        
        
//        let checkPrice = checkPriceIsinRangeOrNot()
//        if checkPrice.1 {
//
//            let checkDuplication = CheckCategoryDuplicate()
//            if checkDuplication.1  else {
//
//            }
//
//
////
////
////            let tempArrayForValues = categoryRequestModel.map{$0.new_category_id}.flatMap{$0}
////            let dups = Dictionary(grouping: tempArrayForValues, by: {$0}).filter { $1.count > 3 }.keys
////            print(dups)
////            if dups.count != 0 {
////                validateMessage = ("You cannot enter same category",false)
////            } else {
////                validateMessage = ("",true)
////            }
//        }
//        else {
//
//        }
        return true
        
    }
    /*
    func validatbliionForCategory() -> (String,Bool) {
        var validateMessage = ("",false)
        var minimumRange = ""
        var maximumRange = ""
        
        outerLoop: for element in categoryDataForAdviserProfile {
            element.subCategoryData?.forEach({ (subElement) in
                if element.categoryName != "Select Category" {
                    let foundItems = SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter( {$0.id == element.NewCategoryID})
                    
                    if foundItems?.count != 0 {
                        
                        foundItems?.forEach({ (foundItemsForeach) in
                            if foundItemsForeach.priceRange.count != 0 {
                                 foundItemsForeach.priceRange.forEach({ (priceRangeElement) in
                                    minimumRange = priceRangeElement.from
                                    maximumRange = priceRangeElement.to
                                    
                                    let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
                                    
                                    let dateForCompare:Date = dateFormatter.date(from: subElement.subLastUpdatedDate ?? "") ?? Date()
                                    
                                    let dateCompare = dateNow.months(from: dateForCompare)
                                    
                                    
                                    if dateCompare > 1 {
                                        if element.OldCategoryID == "0" && element.NewCategoryID != "0" {
                                            
                                            if foundItemsForeach.priceRange.count == element.subCategoryData?.count {
                                                
                                                if subElement.subCategoryPriceNew == "" {
                                                    validateMessage = ("Please enter proper price",false)
                                                 
                                                   // return validateMessage
                                                } else {
                                                    if let amountString = subElement.subCategoryPriceNew?.currencyInputFormatting() {
                                                        var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                                        repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                                        
                                                        let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                                        let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                                        
                                                        let textInDouble:Double = Double(repalacedText) ?? 0.0
                                                        
                                                        if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                            validateMessage = ("Please enter proper price",false)
                                                           
                                                           // return validateMessage
                                                            
                                                        } else {
                                                            validateMessage = ("Please enter proper price",true)
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        }
                                    } else {
                                        if let amountString = subElement.subCategoryPriceNew?.currencyInputFormatting() {
                                            var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                            repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                            
                                            let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                            let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                            
                                            let textInDouble:Double = Double(repalacedText) ?? 0.0
                                            
                                            if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                validateMessage = ("Please enter proper price",false)
                                                
                                               // return validateMessage
                                                
                                            } else {
                                                validateMessage = ("Please enter proper price",true)
                                            }
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
//         categoryDataForAdviserProfile.forEach { (element) in
//
//        }
        return validateMessage
    }
    
    */
    
    
    func checkPriceIsinRangeOrNot() -> (String,Bool) {
        var validateMessage = ("",true)
        var minimumRange = ""
        var maximumRange = ""
        
        
        for i in 0...categoryDataForAdviserProfile.count - 1 {
            if categoryDataForAdviserProfile[i].categoryName != "Select Category" {
                
                let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviserProfile[i].NewCategoryID }
                
                
                
                if foundItems?.count != 0 {
                    for jj in 0...(foundItems?.count ?? 0) - 1 {
                        
                        for kk in 0...(foundItems?[jj].priceRange.count ?? 0) - 1 {
                            minimumRange = foundItems?[jj].priceRange[kk].from ?? ""
                            maximumRange = foundItems?[jj].priceRange[kk].to ?? ""
                            
                            let dateNow = SingletonClass.sharedInstance.TodayDate
//                            let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
                            
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
                            
                            let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviserProfile[i].subCategoryData?[kk].subLastUpdatedDate ?? "") ?? (SingletonClass.sharedInstance.TodayDate )
                            
                            let dateCompare = dateNow.months(from: dateForCompare)
                            
                            if dateCompare < 1  {
                                if categoryDataForAdviserProfile[i].OldCategoryID == "0" {
                                   if foundItems?[jj].priceRange.count == categoryDataForAdviserProfile[i].subCategoryData?.count {
                                       if categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryPriceNew == "" {
                                        let msgString = "Please add price of \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") for \(categoryDataForAdviserProfile[i].categoryName ?? "")"
                                   
                                    
                                    validateMessage = (msgString,false)
                                           //validateMessage = ("Please enter proper price",false)
                                           return validateMessage
                                       } else {
                                           if let amountString = categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryPriceNew?.currencyInputFormatting() {
                                               
                                               var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                               repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                               
                                               let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                               let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                               
                                               let textInDouble:Double = Double(repalacedText) ?? 0.0
                                               
                                               print("the category \(categoryDataForAdviserProfile[i].categoryName ?? "") is and the type is \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") and the price for is: \(textInDouble) in this the minimum price range is \(minimumRangeDouble) and maximum price range is \(maximumRangeDouble)")
                                               
                                               
                                               if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                let msgString = "Please add price of \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") for \(categoryDataForAdviserProfile[i].categoryName ?? "")"
                                           
                                            
                                            validateMessage = (msgString,false)
                                                   //validateMessage = ("Please enter proper price",false)
                                                   return validateMessage
                                                   
                                               } else {
                                                   validateMessage = ("Please enter proper price",true)
                                               }
                                           }
                                       }

                                   }
                               }
                            } else {
                                if foundItems?[jj].priceRange.count == categoryDataForAdviserProfile[i].subCategoryData?.count {
                                    if categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryPriceNew == "" {
                                        let msgString = "Please add price of \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") for \(categoryDataForAdviserProfile[i].categoryName ?? "")"
                                   
                                    
                                    validateMessage = (msgString,false)
                                        //validateMessage = ("Please enter proper price",false)
                                        return validateMessage
                                    } else {
                                        if let amountString = categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryPriceNew?.currencyInputFormatting() {
                                            
                                            var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                            repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                            
                                            let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                            let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                            
                                            let textInDouble:Double = Double(repalacedText) ?? 0.0
                                            
                                            print("the category \(categoryDataForAdviserProfile[i].categoryName ?? "") is and the type is \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") and the price for is: \(textInDouble) in this the minimum price range is \(minimumRangeDouble) and maximum price range is \(maximumRangeDouble)")
                                            
                                            
                                            if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                
                                                let msgString = "Please add price of \(categoryDataForAdviserProfile[i].subCategoryData?[kk].subCategoryName ?? "") for \(categoryDataForAdviserProfile[i].categoryName ?? "")"
                                           
                                            
                                            validateMessage = (msgString,false)
                                                
                                               // validateMessage = ("Please enter proper price",false)
                                                return validateMessage
                                                
                                            } else {
                                                validateMessage = ("Please enter proper price",true)
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        return validateMessage
    }
    //MARK: -  API Calls
    func webServiceCallForEditProfile(RequestModel : EditprofileAdvisorReqModel) {
        
        Utilities.showHud()
        WebServiceSubClass.UpdateProfileAdvisor(editProfileModel: RequestModel, img: self.selectedImage ?? UIImage(), isRemoveImage: self.isRemovePhoto, showHud: false, completion: { (response, status, error) in
            Utilities.hideHud()
            if status{
                let loginModel = UserInfo.init(fromJson: response)
                let loginModelDetails = loginModel
                
                SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                userDefault.setUserData(objProfile: loginModelDetails)
                
                self.isRemovePhoto = false
                Utilities.displayAlert("", message: response["message"].string ?? "", completion: {_ in
                    self.navigationController?.popViewController(animated: true)
                   // appDel.performLogout()
                    
                }, otherTitles: nil)
                
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }

    func setRequestMdoel() {
        
        let updateModel = EditprofileAdvisorReqModel()
        updateModel.user_id = SingletonClass.sharedInstance.UserId
        updateModel.full_name = textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        updateModel.nick_name = textFieldNickName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        updateModel.email = textFieldEmailAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        updateModel.phone = TextFieldPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        updateModel.advisor_description = textViewDescription.text.trimmingCharacters(in: .whitespacesAndNewlines)
        var updestring:[String] = []
        if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName != textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" {
            updestring.append("full_name")
        }
        if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName != textFieldNickName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" {
            updestring.append("nick_name")
        }
        
        if SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.advisorDescription != textViewDescription.text {
            updestring.append("advisor_description")
        }
        if selectedOldImage == nil && isRemovePhoto {
            // self.navigationController?.popViewController(animated: true)
        } else {
            if selectedOldImage != nil && isRemovePhoto {
                updestring.append("profile_picture")
            } else if selectedOldImage == selectedImage {
                
            } else if (selectedOldImage == selectedImage) == false {
                updestring.append("profile_picture")
            } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
                updestring.append("profile_picture")
            } else {
            }
        }
        
        if isUpdateAny {
            updestring.append("is_change_availability")
        }
        updateModel.is_change = updestring.map{String($0)}.joined(separator: ",")
        updateModel.is_change_availability = isUpdateAny ? "1" : "0"
        if self.isRemovePhoto{
            updateModel.remove_image = "1"
        }
        
         
            
            let productsDict = CategoryUpdateRequestModel.convertDataCartArrayToProductsDictionary(arrayDataCart: categoryRequestModel);
            let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
            
            let jsonString:String = String(data: jsonData, encoding: .utf8) ?? ""
            
            updateModel.availability = jsonString
       
        if updateModel.is_change.contains("profile_picture") || updateModel.is_change.contains("advisor_description") {
            let alert = UIAlertController(title: "Are you sure ?", message: "Change in profile picture and description will be awaiting approval from the Admin", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { [self]_ in
                webServiceCallForEditProfile(RequestModel: updateModel)
            }))
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        } else {
            webServiceCallForEditProfile(RequestModel: updateModel)
        }
        
    }
    func jsonToString(json: AnyObject){
        do {
            let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
            let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
            print(convertedString ?? "defaultvalue")
        } catch let myJSONError {
            print(myJSONError)
        }
        
    }
}

extension AdviserProfileEditViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if categoryDataForAdviserProfile[section].isExpanded ?? false {
            
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category?[section].priceRange.count ?? 0
            
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblAddCategory.dequeueReusableCell(withIdentifier: AdviserProfileUpdateCategoryCell.reuseIdentifier, for: indexPath) as! AdviserProfileUpdateCategoryCell
        
        
        cell.textFieldCategoryPrice.tag = indexPath.row
        cell.lblCategoryName.text = categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subCategoryName?.uppercased()
        
       // cell.lblCategoryName.text = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[indexPath.section].priceRange?[indexPath.row].type.uppercased()
        cell.textFieldCategoryPrice.superview?.layer.borderWidth = 1
        
        
        cell.indexPathForTextField = indexPath
        
        switch categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subCategoryName
        {
        case indexColorForCommunicationType.chat.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#2AD916")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#2AD916").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#2AD916")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_chat")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#2AD916")
            
        case indexColorForCommunicationType.audio.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#F3A791")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#F3A791").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#F3A791")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_audioCall")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#F3A791")
            
        case indexColorForCommunicationType.video.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#22A9E5")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#22A9E5").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#22A9E5")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_videoCall")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#22A9E5")
            
        default:
            break
        }
        
        
        
        
        //SingletonClass.sharedInstance.categoryDataForAdviser?.category?[indexPath.section].priceRange?[indexPath.row].type
//        {
//        case indexColorForCommunicationType.chat.CommunicationTypeName:
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#2AD916")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#2AD916").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_chat")
//
//            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#2AD916")
//
//        case indexColorForCommunicationType.audio.CommunicationTypeName:
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#F3A791")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#F3A791").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_audioCall")
//
//            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#F3A791")
//
//        case indexColorForCommunicationType.video.CommunicationTypeName:
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#22A9E5")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#22A9E5").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_videoCall")
//
//            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#22A9E5")
//
//        default:
//            break
//        }
        
        
//        switch SingletonClass.sharedInstance.categoryDataForAdviser?.category?[indexPath.section].priceRange?[indexPath.row].type {
//        case "chat":
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#2AD916")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#2AD916").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_chat")
//
//        case "audio":
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#F3A791")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#F3A791").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_audioCall")
//
//        case "video":
//            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#22A9E5")
//            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#22A9E5").cgColor
//            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_videoCall")
//
//        default:
//            break
//        }
        cell.textFieldCategoryPrice.text = categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subCategoryPriceNew
        
        if let amountString = cell.textFieldCategoryPrice.text?.currencyInputFormatting() {
            cell.textFieldCategoryPrice.text = amountString
        }
        cell.isOneMonethDone = false
//        NotificationCenter.default.addObserver(forName: UITextField.textDidBeginEditingNotification, object: cell.textFieldCategoryPrice, queue: OperationQueue.main, using: { (notification) in
//            if cell.isOneMonethDone {
//                cell.textFieldCategoryPrice.resignFirstResponder()
//                let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
//
//                controller.StringButton = "Okay"
//                controller.isShowLeftIcon = false
//                controller.isShowRightIcon = false
//                controller.isShowNoteTextView = false
//                controller.stringDescription = "You are not allowed to change the price more than one time in a month."
//
//                controller.btnSubmitClosour = {
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                let navigationController = UINavigationController(rootViewController: controller)
//                navigationController.modalPresentationStyle = .overCurrentContext
//                navigationController.modalTransitionStyle = .crossDissolve
//                self.present(navigationController, animated: true, completion: nil)
//            }
//
//        })
        print(indexPath.section)
        if categoryDataForAdviserProfile[indexPath.section].OldCategoryID == "0" {
            print("Category is Equal")
            cell.contentView.alpha = 1.0
            cell.textFieldCategoryPrice.isUserInteractionEnabled = true
        } else {
            
            let dateNow = SingletonClass.sharedInstance.TodayDate
            
            
          
            let df = DateFormatter()
            df.dateFormat = DateFormatterString.timeWithDate.rawValue

           

            let time12 = df.string(from: dateNow )
            
            //let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
            print(categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subLastUpdatedDate ?? "")
            let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subLastUpdatedDate ?? "") ?? (SingletonClass.sharedInstance.TodayDate )
            
            let dateCompare = dateNow.months(from: dateForCompare)
            print("Today's Date: \(time12) Date for Compare: \(dateForCompare) and the result of compare is: \(dateCompare)")
           // print("Date for Compare: \(dateForCompare) and the result of compare is: \(dateCompare)")
            
            if
                dateCompare < 1  {
                cell.contentView.alpha = 0.6
                cell.textFieldCategoryPrice.isUserInteractionEnabled = false
                //cell.contentView.isUserInteractionEnabled = false
            } else {
                cell.contentView.alpha = 1.0
                cell.textFieldCategoryPrice.isUserInteractionEnabled = true
                //cell.contentView.isUserInteractionEnabled = true
            }
            
            
        }
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        DispatchQueue.main.async {[self] in
            tblAddCategoryHeight.constant = tblAddCategory.contentSize.height
        }
        print(categoryDataForAdviserProfile.count)
        return categoryDataForAdviserProfile.count
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tblAddCategory.dequeueReusableCell(withIdentifier: AdviserProfileUpdateCategoryCell.reuseIdentifier, for: indexPath) as! AdviserProfileUpdateCategoryCell
        
        
        if categoryDataForAdviserProfile[indexPath.section].OldCategoryID == "0" {
            print("Category is Equal")
            var minimumRange = ""
            var maximumRange = ""
            for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1 {
                if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviserProfile[indexPath.section].categoryName{
                    minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].from ?? ""
                    maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].to ?? "11.00"
                }
            }
            
            
            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
            
            controller.StringButton = "Okay"
            controller.isShowLeftIcon = false
            controller.isShowRightIcon = false
            controller.isShowNoteTextView = false
            controller.stringDescription = "You can set your price between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min."
            
            controller.btnSubmitClosour = {
                self.dismiss(animated: true, completion: nil)
            }
            
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            self.present(navigationController, animated: true, completion: nil)
        } else {
            let dateNow = SingletonClass.sharedInstance.TodayDate
            //let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
            print(categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subLastUpdatedDate ?? "")
            let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subLastUpdatedDate ?? "") ?? (SingletonClass.sharedInstance.TodayDate )
            
            let dateCompare = dateNow.months(from: dateForCompare)
            
            print("Date for Compare: \(dateForCompare) and the result of compare is: \(dateCompare)")
            
            if
                dateCompare < 1  {
                let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
                
                controller.StringButton = "Okay"
                controller.isShowLeftIcon = false
                controller.isShowRightIcon = false
                controller.isShowNoteTextView = false
                controller.stringDescription = "You are not allowed to change the price more than one time in a month."
                
                controller.btnSubmitClosour = {
                    self.dismiss(animated: true, completion: nil)
                }
                
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                self.present(navigationController, animated: true, completion: nil)
                //cell.contentView.isUserInteractionEnabled = false
            } else {
                var minimumRange = ""
                var maximumRange = ""
                for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1 {
                    if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviserProfile[indexPath.section].categoryName{
                        minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].from ?? ""
                        maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].to ?? "11.00"
                    }
                }
                
                
                let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
                
                controller.StringButton = "Okay"
                controller.isShowLeftIcon = false
                controller.isShowRightIcon = false
                controller.isShowNoteTextView = false
                controller.stringDescription = "You can set your price between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min."
                
                controller.btnSubmitClosour = {
                    self.dismiss(animated: true, completion: nil)
                }
                
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                self.present(navigationController, animated: true, completion: nil)
                //cell.contentView.isUserInteractionEnabled = true
            }
            
            
        }
        
        
        
//        print(cell.contentView.alpha)
//
//        let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
//
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
//
//        let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviserProfile[indexPath.section].subCategoryData?[indexPath.row].subLastUpdatedDate ?? "") ?? Date()
//        let dateCompare = dateNow.months(from: dateForCompare)
//
//        if dateCompare < 1  {
//            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
//
//            controller.StringButton = "Okay"
//            controller.isShowLeftIcon = false
//            controller.isShowRightIcon = false
//            controller.isShowNoteTextView = false
//            controller.stringDescription = "You are not allowed to change the price more than one time in a month."
//
//            controller.btnSubmitClosour = {
//                self.dismiss(animated: true, completion: nil)
//            }
//
//            let navigationController = UINavigationController(rootViewController: controller)
//            navigationController.modalPresentationStyle = .overCurrentContext
//            navigationController.modalTransitionStyle = .crossDissolve
//            self.present(navigationController, animated: true, completion: nil)
//            //cell.contentView.isUserInteractionEnabled = false
//        } else {
//            var minimumRange = ""
//            var maximumRange = ""
//            for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1 {
//                if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviserProfile[indexPath.section].categoryName{
//                    minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].from ?? ""
//                    maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].to ?? "11.00"
//                }
//            }
//
//
//            let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
//
//            controller.StringButton = "Okay"
//            controller.isShowLeftIcon = false
//            controller.isShowRightIcon = false
//            controller.isShowNoteTextView = false
//            controller.stringDescription = "You can set your price between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min."
//
//            controller.btnSubmitClosour = {
//                self.dismiss(animated: true, completion: nil)
//            }
//
//            let navigationController = UINavigationController(rootViewController: controller)
//            navigationController.modalPresentationStyle = .overCurrentContext
//            navigationController.modalTransitionStyle = .crossDissolve
//            self.present(navigationController, animated: true, completion: nil)
//            //cell.contentView.isUserInteractionEnabled = true
//        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tblAddCategory.dequeueReusableCell(withIdentifier: adviserProfileHeaderCell.reuseIdentifier) as! adviserProfileHeaderCell
        cell.indexPathForName = section
        cell.tblView = tableView
        cell.TextFieldCategoryName.text = categoryDataForAdviserProfile[section].categoryName
        if categoryDataForAdviserProfile[section].isExpanded ?? false {
            
            cell.btncategoryDelete.setImage(UIImage(named: "ic_removeAdviserCategory"), for: .normal)
            
        } else {
            
            cell.btncategoryDelete.setImage(UIImage(named: "ic_addAdviserCategory"), for: .normal)
            
        }
        cell.deleteButton.superview?.isHidden = true
        if categoryDataForAdviserProfile[section].categoryName != "Select Category" {
            cell.deleteButton.superview?.isHidden = false
        }
        
        
        
        cell.deleteCategoryClosure = {
            
            let formatter = DateFormatter()
            // initially set the format based on your datepicker date / server String
            formatter.dateFormat = DateFormatterString.onlyDate.rawValue
            let dateNow = SingletonClass.sharedInstance.TodayDate
            //let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
            let myString = formatter.string(from: dateNow ) // string purpose I add here
            // convert your string to date
            
            categoryDataForAdviserProfile[section] = profileUpdateCategoryData(id: "0", newCategoryID: "0", name: "Select Category", expand: false, categoryData: [profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: "0.00", date: myString, priceNew: "")])
            
            
         //   categoryDataForAdviserProfile[section] = profileUpdateCategoryData(id: "0", newCategoryID: "0", name: "Select Category", expand: false, categoryData: [profileUpdateSubCategoryData(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: "0.00", date: "", priceNew: ""),profileUpdateSubCategoryData(name: indexColorForCommunicationType.video.CommunicationTypeName, price: "0.00", date: myString, priceNew: "")])
            
            
            
            self.tblAddCategory.reloadData()
        }
        cell.btnCategoryDeleteClosure = { [self] in
            if categoryDataForAdviserProfile[section].NewCategoryID != "0" {
                if categoryDataForAdviserProfile[section].isExpanded ?? false {
                    categoryDataForAdviserProfile[section].isExpanded = false
                    
                } else {
                    categoryDataForAdviserProfile[section].isExpanded = true
                }
                
                DispatchQueue.main.async {
                    tblAddCategory.reloadData()
                }
            }else {
                Utilities.ShowAlert(OfMessage: "Please select Category")
                DispatchQueue.main.async {
                    tblAddCategory.reloadData()
                }
            }
            
            
            
        }
        var tempBoolArray : [Bool] = []
        
        if categoryDataForAdviserProfile[section].OldCategoryID == "0" {
            
            cell.contentView.alpha = 1.0
            cell.contentView.isUserInteractionEnabled = true
        } else {
            
            
            for i in 0...(categoryDataForAdviserProfile[section].subCategoryData?.count ?? 0) {
                if  i == categoryDataForAdviserProfile[section].subCategoryData?.count {
                    if tempBoolArray.contains(false) {
                        cell.stackViewForAlpha.alpha = 0.6
                        //cell.contentView.alpha = 0.6
                        
                        cell.TextFieldCategoryName.isUserInteractionEnabled = false
                        cell.deleteButton.isUserInteractionEnabled = false
                        cell.btncategoryDelete.isUserInteractionEnabled = true
                        //cell.contentView.isUserInteractionEnabled = true
                        // cell.btncategoryDelete.isUserInteractionEnabled = true
                    }
                    else {
                        cell.stackViewForAlpha.alpha = 1.0
                        //cell.contentView.alpha = 1.0
                        
                        cell.TextFieldCategoryName.isUserInteractionEnabled = true
                        cell.deleteButton.isUserInteractionEnabled = true
                        cell.btncategoryDelete.isUserInteractionEnabled = true
                        
                        //cell.contentView.isUserInteractionEnabled = true
                    }
                }
                else {
                    tempBoolArray.append(false)
                    let dateNow = SingletonClass.sharedInstance.TodayDate
                    //let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
                    
                    let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviserProfile[section].subCategoryData?[i].subLastUpdatedDate ?? "") ?? (SingletonClass.sharedInstance.TodayDate )
                    let dateCompare = dateNow.months(from: dateForCompare)
                    
                    if dateCompare < 1  {
                        tempBoolArray[i] = false
                    } else {
                        tempBoolArray[i] = true
                    }
                }
            }
        }
        
        cell.selectedCategoryData = {
            
            categoryDataForAdviserProfile[section].categoryName = cell.selectedCategoryName
            categoryDataForAdviserProfile[section].NewCategoryID = cell.selectedCategoryID
            self.tblAddCategory.reloadData()
        }
        
        return cell
        
        
    }
    
}


class profileUpdateCategoryData {
    var OldCategoryID : String?
    var NewCategoryID : String?
    var categoryName : String?
    var isExpanded : Bool?
    var subCategoryData : [profileUpdateSubCategoryData]?
    init(id:String,newCategoryID:String,name:String,expand:Bool,categoryData:[profileUpdateSubCategoryData]) {
        self.OldCategoryID = id
        self.NewCategoryID = newCategoryID
        self.categoryName = name
        self.subCategoryData = categoryData
        self.isExpanded = expand
    }
}
class profileUpdateSubCategoryData {
    var subCategoryName : String?
    var subCategoryPriceNew : String?
    var subCategoryPriceOld : String?
    var subLastUpdatedDate : String?
    
    
    init(name:String,price:String,date:String,priceNew:String) {
        self.subCategoryName = name
        self.subCategoryPriceOld = price
        self.subLastUpdatedDate = date
        self.subCategoryPriceNew = priceNew
    }
}
class adviserProfileHeaderCell : UITableViewCell {
    
    var deleteCategoryClosure : (() -> ())?
    var btnCategoryDeleteClosure : (() -> ())?
    var selectedCategoryData : (() -> ())?
    
    var selectedCategoryID = ""
    var selectedCategoryName = ""
    
    
    var indexPathForName = Int()
    let pickerViewForAll = GeneralPickerView()
    var tblView = UITableView()
    
    
    @IBOutlet weak var stackViewForAlpha: UIStackView!
    @IBOutlet weak var TextFieldCategoryName: AddCategoryTextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func deleteButtonClick(_ sender: Any) {
        if let click = self.deleteCategoryClosure {
            click()
        }
    }
    
    
    
    @IBOutlet weak var showCategoryView: BorderView!
    @IBOutlet weak var btncategoryDelete: UIButton!
    
    
    @IBAction func btnCategoryDeleteClick(_ sender: UIButton) {
        if let click = self.btnCategoryDeleteClosure {
            click()
        }
    }
    // var arrayForCategoryName : [(name: String, id: String)] = []
    override func awakeFromNib() {
        TextFieldCategoryName.delegate = self
        //        for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 0) - 1 {
        //            arrayForCategoryName.append(((SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].name ?? ""),(SingletonClass.sharedInstance.categoryDataForAdviser?.category[i].id ?? "")))
        //
        //        }
        //
        setupDelegateForPickerView()
    }
    
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        pickerViewForAll.generalPickerDelegate = self
    }
}

extension adviserProfileHeaderCell: GeneralPickerViewDelegate {
    
    func didTapDone() {
        let row = pickerViewForAll.selectedRow(inComponent: 0)
        if TextFieldCategoryName.isFirstResponder {
            
            self.selectedCategoryName = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name ?? ""
            self.selectedCategoryID = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].id ?? ""
            TextFieldCategoryName.text = self.selectedCategoryName
            
            categoryDataForAdviserProfile[indexPathForName].categoryName = self.selectedCategoryName
            categoryDataForAdviserProfile[indexPathForName].NewCategoryID = self.selectedCategoryID
            //
            let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviserProfile[indexPathForName].NewCategoryID }
            if foundItems?.count != 0 {
                for i in 0...(foundItems?.count ?? 0) - 1 {
                    for j in 0...(foundItems?[i].priceRange.count ?? 0) - 1 {
                        categoryDataForAdviserProfile[indexPathForName].subCategoryData?[j].subCategoryPriceNew = ""

                    }
                }
            }
            
            tblView.reloadData()
        }
        

        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension adviserProfileHeaderCell : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == TextFieldCategoryName {
            TextFieldCategoryName.inputView = pickerViewForAll
            TextFieldCategoryName.inputAccessoryView = pickerViewForAll.toolbar
            //let indexOfA = SingletonClass.sharedInstance.categoryDataForAdviser?.category.firstIndex(of: TextFieldCategoryName.text ?? "") ?? 0 /
            
            if let DummyFirst = (SingletonClass.sharedInstance.categoryDataForAdviser?.category?.first(where: {$0.name == TextFieldCategoryName.text ?? ""})) {
                
                let indexOfA = SingletonClass.sharedInstance.categoryDataForAdviser?.category?.firstIndex(of: DummyFirst) ?? 0
                pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
               
                self.pickerViewForAll.reloadAllComponents()
            }
            
          
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       // DispatchQueue.main.async {
            self.tblView.reloadData()
      //  }
    }
    
}
extension adviserProfileHeaderCell : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if TextFieldCategoryName.isFirstResponder {
            
//            self.selectedCategoryName = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name ?? ""
//            self.selectedCategoryID = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].id ?? ""
//            TextFieldCategoryName.text = self.selectedCategoryName
//            
//            categoryDataForAdviserProfile[indexPathForName].categoryName = self.selectedCategoryName
//            categoryDataForAdviserProfile[indexPathForName].NewCategoryID = self.selectedCategoryID
//            //
//            let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviserProfile[indexPathForName].NewCategoryID }
//            if foundItems?.count != 0 {
//                for i in 0...(foundItems?.count ?? 0) - 1 {
//                    for j in 0...(foundItems?[i].priceRange.count ?? 0) - 1 {
//                        categoryDataForAdviserProfile[indexPathForName].subCategoryData?[j].subCategoryPriceNew = ""
//
//                    }
//                }
//            }
//            
//            tblView.reloadData()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if TextFieldCategoryName.isFirstResponder {
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 0
            
        }
        return 0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if TextFieldCategoryName.isFirstResponder {
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name
            
        }
        return ""
        
    }
    
}
class AdviserProfileUpdateCategoryCell : UITableViewCell, UITextFieldDelegate {
    var indexPathForTextField = IndexPath()
    
    var sectionID = Int()
    var IndexID = Int()
    var isOneMonethDone = false
    @IBOutlet weak var textFieldCategoryPrice: addCategoryTextField!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var lblCategoryName: AdviserAddCategoryLabel!
    
    override func awakeFromNib() {
        textFieldCategoryPrice.delegate = self
        textFieldCategoryPrice.placeholder = "$0.00"
       // textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                        // attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        textFieldCategoryPrice.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)

    }
    @objc func textFieldDidChange() {
        if textFieldCategoryPrice.text != "" {
                   if let amountString = textFieldCategoryPrice.text?.currencyInputFormatting() {
                    textFieldCategoryPrice.text = amountString
                       categoryDataForAdviserProfile[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceNew = textFieldCategoryPrice.text
       
                   }
               }

//            if textFieldCategoryPrice.text != "" {
//                if let amountString = textFieldCategoryPrice.text?.currencyInputFormatting() {
//                    textFieldCategoryPrice.text = amountString
//                    categoryDataForAdviser[self.indexPathForTextField.section].subCategory[self.indexPathForTextField.row].categoryPrice = textFieldCategoryPrice.text
//                    
//                }
//            }
        
    }
//    func textFieldDidChangeSelection(_ textField: UITextField) {
//        if textField.text != "" {
//            if let amountString = textField.text?.currencyInputFormatting() {
//                textField.text = amountString
//                categoryDataForAdviserProfile[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceNew = textField.text
//                
//            }
//        }
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldCategoryPrice {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    textField.text = ""
                }
            }
            let charsLimit = 8
            
            let startingLength = textField.text?.replacingOccurrences(of: Currency, with: "").replacingOccurrences(of: ".", with: "").count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace =  range.length
            let newLength = startingLength + lengthToAdd - lengthToReplace
            
            return newLength <= charsLimit
            
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var minimumRange = ""
        var maximumRange = ""
        for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1  {
            
            if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviserProfile[indexPathForTextField.section].categoryName{
                
                minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].from ?? "1.00"
                maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].to ?? "11.00"
                
                if let amountString = textField.text?.currencyInputFormatting() {
                    
                    var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                    repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                    
                    let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                    let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                    
                    let textInDouble:Double = Double(repalacedText) ?? 0.0
                    
                    if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                        Utilities.ShowAlert(OfMessage: "Please enter text between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min")
                        textField.text = ""
                       // textField.text = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].from.currencyInputFormatting()
                        categoryDataForAdviserProfile[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceNew = textField.text
                        //categoryDataForAdviserProfile[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceOld?.currencyInputFormatting()
                    } else {
                        categoryDataForAdviserProfile[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceNew = textField.text
                        
                    }
                }
            }
        }
    }
}
extension Date {
    /// Returns the amount of years from another date
    func years(from date: Date) -> Int {
        return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
    }
    /// Returns the amount of months from another date
    func months(from date: Date) -> Int {
        return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
    }
    /// Returns the amount of weeks from another date
    func weeks(from date: Date) -> Int {
        return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
    }
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    /// Returns the amount of minutes from another date
    func minutes(from date: Date) -> Int {
        return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
    /// Returns the amount of seconds from another date
    func seconds(from date: Date) -> Int {
        return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
    }
    /// Returns the a custom time interval description from another date
    func offset(from date: Date) -> String {
        if years(from: date) > 0 {
            if years(from: date) > 1 {
                return "\(years(from: date)) years"
            } else {
                return "\(years(from: date)) year"
            }
        }
       
        if months(from: date) > 0 {
            if months(from: date) > 1 {
                return "\(months(from: date)) months"
            } else {
                return "\(months(from: date)) month"
            }
        }
        
        if weeks(from: date) > 0 {
            if weeks(from: date) > 1 {
                return "\(weeks(from: date)) weeks"
            } else {
                return "\(weeks(from: date)) week"
            }
        }
        
        if days(from: date) > 0 {
            if days(from: date) > 1 {
                return "\(days(from: date)) days"
            } else {
                return "\(days(from: date)) day"
            }
        }
        
        if hours(from: date) > 0 {
            if hours(from: date) > 1 {
                return "\(hours(from: date)) hours"
            } else {
                return "\(hours(from: date)) hour"
            }
        }
        
        if minutes(from: date) > 0 {
            if minutes(from: date) > 1 {
                return "\(minutes(from: date)) minutes"
            } else {
                return "\(minutes(from: date)) minute"
            }
        }
        
        if seconds(from: date) > 0 {
            if seconds(from: date) > 1 {
                return "\(seconds(from: date)) seconds"
            } else {
                return "\(seconds(from: date)) second"
            }
        }
        
        return ""
    }
}
extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}
extension AdviserProfileEditViewController:ImagePickerDelegate {
    
    func didSelect(image: UIImage?, SelectedTag:Int) {
        
        if(image == nil && SelectedTag == 101){
            self.selectedImage = UIImage()
            self.isRemovePhoto = true
            self.userImageView.image = UIImage.init(named: "user_dummy_profile")
        }else if image != nil{
            let fixedOrientedImage = image?.fixOrientation()
            self.userImageView.image = fixedOrientedImage
            self.selectedImage = self.userImageView.image ?? UIImage()
        }else{
            return
        }
    }
}
