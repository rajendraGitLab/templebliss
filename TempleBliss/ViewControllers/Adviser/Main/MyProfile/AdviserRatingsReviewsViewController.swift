//
//  AdviserRatingsReviewsViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import  SkeletonView
import SDWebImage
import Cosmos
class AdviserRatingsReviewsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource, ExpandableLabelDelegate {

    //MARK: - Properties
    
    var AdvisorID = ""
    var states : Array<Bool>?
    var rateAndReviewData : AdvisorRateAndReviewResModel?
    var customTabBarController : CustomTabBarVC?
    var isFromCustomer = false
    //MARK: - IBOutlets
    @IBOutlet weak var tblRateAndReview: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
        setValue()
        if isFromCustomer == true {
            GetSeeAll()
        } else {
            getSessionData()
        }
        
        tblRateAndReview.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
      
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Ratings & Reviews", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
       
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
            let date = dateFormatter.date(from: date) ?? Date()
            dateFormatter.dateFormat = "dd MMM"
            return  dateFormatter.string(from: date)

        }
    //MARK: - tableViewMethods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if rateAndReviewData == nil {
            return 10
        } else {
            if rateAndReviewData?.reviewHistory.count == 0 {
                return 1
            } else {
                return rateAndReviewData?.reviewHistory.count ?? 1
            }
              
        }
            
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
  
        if rateAndReviewData == nil {
            let cell = tblRateAndReview.dequeueReusableCell(withIdentifier: AdviserRateAndReviewCell.reuseIdentifier, for: indexPath) as! AdviserRateAndReviewCell
            cell.MainView.isHidden = true
            cell.skeletonImageView.isHidden = false
            
            //
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.skeletonImageView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            return cell
        } else {
            if rateAndReviewData?.reviewHistory.count != 0 {
                let cell = tblRateAndReview.dequeueReusableCell(withIdentifier: AdviserRateAndReviewCell.reuseIdentifier, for: indexPath) as! AdviserRateAndReviewCell
                cell.MainView.isHidden = false
                cell.skeletonImageView.isHidden = true
                // cell.UserImage.image = rateAndReviewData?.reviewHistory?[indexPath.row].userImage
                cell.lblRattingDate.text = convertDateFormater(rateAndReviewData?.reviewHistory?[indexPath.row].createdAt ?? "") 
                cell.lblRattingDescription.text = rateAndReviewData?.reviewHistory?[indexPath.row].note ?? ""
               
                cell.lblRattingDescription.delegate = self
                cell.lblRattingDescription.setLessLinkWith(lessLink: "show less", attributes: [.foregroundColor:colors.white.value,.font: CustomFont.medium.returnFont(15)], position: .left)
                
                cell.lblRattingDescription.shouldCollapse = true
                cell.lblRattingDescription.textReplacementType = .word
                if rateAndReviewData?.reviewHistory?[indexPath.row].note ?? "" == "" {
                    cell.lblRattingDescription.numberOfLines = 0
                } else {
                    cell.lblRattingDescription.numberOfLines = 3
                }
                
             
                cell.lblRattingDescription.collapsed = states?[indexPath.row] ?? true
              
                //cell.lblRattingDescription.text = "\(rateAndReviewData?.reviewHistory?[indexPath.row].note ?? "") \n \(rateAndReviewData?.reviewHistory?[indexPath.row].note ?? "") \n \(rateAndReviewData?.reviewHistory?[indexPath.row].note ?? "") \n \(rateAndReviewData?.reviewHistory?[indexPath.row].note ?? "")"
                //rateAndReviewData?.reviewHistory?[indexPath.row].note ?? ""
                
                
                
                cell.lblTotalRatting.text = rateAndReviewData?.reviewHistory?[indexPath.row].rating ?? ""
                cell.lbluserName.text = rateAndReviewData?.reviewHistory?[indexPath.row].fullName
                
                if (rateAndReviewData?.reviewHistory?[indexPath.row].profilePicture ?? "") == "" {
                    cell.UserImage.image = UIImage(named: "user_dummy_profile")
                } else {
                    
                    let imgURL = rateAndReviewData?.reviewHistory?[indexPath.row].profilePicture ?? ""
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    cell.UserImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                    
                    cell.UserImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                cell.RatiningView.rating = Double(rateAndReviewData?.reviewHistory?[indexPath.row].rating ?? "") ?? 0.0
               
                
                return cell
               
            } else {
                let NoDatacell = tblRateAndReview.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
                NoDatacell.imgNoData.image = UIImage(named: "ic_Like")
                NoDatacell.lblNoDataTitle.text = "Reviews not availible"
                //NodataTitleText.ItemList
                
                return NoDatacell
            }
        }
        
    
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if rateAndReviewData == nil {
            return 80
        } else {
            if rateAndReviewData?.reviewHistory.count != 0 {
                return UITableView.automaticDimension
            } else {
                return tableView.frame.size.height
            }
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
    }
    func willExpandLabel(_ label: ExpandableLabel) {
        tblRateAndReview.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tblRateAndReview)
        if let indexPath = tblRateAndReview.indexPathForRow(at: point) as IndexPath? {
            states?[indexPath.row] = false
            DispatchQueue.main.async { [weak self] in
                self?.tblRateAndReview.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tblRateAndReview.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tblRateAndReview.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tblRateAndReview)
        if let indexPath = tblRateAndReview.indexPathForRow(at: point) as IndexPath? {
            states?[indexPath.row] = true
            DispatchQueue.main.async { [weak self] in
                self?.tblRateAndReview.scrollToRow(at: indexPath, at: .top, animated: true)
            }
        }
        tblRateAndReview.endUpdates()
    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls

}
class AdviserRateAndReviewCell : UITableViewCell {
    
    @IBOutlet weak var skeletonImageView: UIImageView!
    
   
    @IBOutlet weak var MainView: AdviserDetailsView!
    
    
    @IBOutlet weak var lblTotalRatting: adviserRattingLabel!
    @IBOutlet weak var lblRattingDescription: AdvisorHomeScreenDescriptionLabel!
    @IBOutlet weak var lblRattingDate: adviserRattingLabel!
    @IBOutlet weak var lbluserName: adviserRattingLabel!
    @IBOutlet weak var UserImage: UIImageView!
    @IBOutlet weak var RatiningView: CosmosView!
    
    override func awakeFromNib() {
        RatiningView.backgroundColor = .clear
        RatiningView.isUserInteractionEnabled = false
    }
    override func prepareForReuse() {
        
        super.prepareForReuse()
        lblRattingDescription.collapsed = true
        lblRattingDescription.text = nil
    }
}


extension AdviserRatingsReviewsViewController {
    func getSessionData() {
        let MySessionModel = AdvisorRateAndReviewReqModel()
        MySessionModel.user_id = SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? ""
        webserviceForReviews(reqModel: MySessionModel)
    }
    func GetSeeAll() {
        let MySessionModel = SeeAllRateAndReviewReqModel()
        MySessionModel.advisor_id = AdvisorID
        webserviceForSeeAll(reqModel: MySessionModel)
    }
    func webserviceForReviews(reqModel:AdvisorRateAndReviewReqModel) {
        WebServiceSubClass.AdvisorRateAndReview(addCategory: reqModel, completion: {(json, status, response) in
         
            if status {
                let MySessionResModel = AdvisorRateAndReviewResModel.init(fromJson: json)
                self.rateAndReviewData = MySessionResModel
                self.tblRateAndReview.reloadDataWithAutoSizingCellWorkAround()
              
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
//                    self.tblRateAndReview.reloadData()
//                })
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
    func webserviceForSeeAll(reqModel:SeeAllRateAndReviewReqModel) {
        WebServiceSubClass.SeeAllReviews(addCategory: reqModel, completion: {(json, status, response) in
         
            if status {
                let MySessionResModel = AdvisorRateAndReviewResModel.init(fromJson: json)
                self.rateAndReviewData = MySessionResModel
                
                self.tblRateAndReview.reloadDataWithAutoSizingCellWorkAround()
              
//                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
//                    self.tblRateAndReview.reloadData()
//                })
                
              
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
        
    }
}


class AdviserRateAndReview {
    var userImage : UIImage?
    var userName : String?
    var userReview : String?
    var userReviewDate : String?
    var userRatting : String?
    
    init(image:UIImage,name:String,review:String,reviewDate:String,ratting:String) {
        self.userImage = image
        self.userName = name
        self.userReview =  review
        self.userReviewDate = reviewDate
        self.userRatting = ratting
    }
}
