//
//  AdviserEnterOTPViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserEnterOTPViewController:BaseViewController {
    
    //MARK: - Properties
    var objectRegister = RegisterReqModel()
    var strOtp = ""
    //MARK: - IBOutlets
    @IBOutlet weak var btnOTP: EnterOTPButton!
    @IBOutlet weak var textFieldOtp: EnterOTPTextField!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if strOtp != "" {
            var secondsRemaining = 30
            self.btnOTP.isEnabled = false
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                if secondsRemaining > 1 {
                    UIView.performWithoutAnimation {
                        self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) seconds", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        secondsRemaining -= 1
                        self.btnOTP.layoutIfNeeded()
                    }
                
                } else if secondsRemaining == 1 {
                    UIView.performWithoutAnimation {
                        self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) second", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        secondsRemaining -= 1
                        self.btnOTP.layoutIfNeeded()
                    }
                } else {
                    UIView.performWithoutAnimation {
                        
                        self.btnOTP.setunderline(title: "Resend OTP", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        self.btnOTP.isEnabled = true
                        Timer.invalidate()
                    }
                    
                }
               }
        }
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    func validation() -> Bool{
        if textFieldOtp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            Utilities.ShowAlert(OfMessage: "Please enter OTP")
            
                return false
          
        } else {
            if strOtp != textFieldOtp.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""{
                Utilities.ShowAlert(OfMessage: ErrorMessages.Invelid_Otp.rawValue)
                return false
            }
        }
        
       
        return true
    }
    //MARK: - IBActions
    @IBAction func btnResendOtp(_ sender: Any) {
        
        webserviceForRegisterOtp()
    }
    @IBAction func btnVerifyClick(_ sender: Any) {
        
        if validation(){
            webserviceForRegister()
        }
    }
    
    //MARK: - API Calls
    func webserviceForRegister(){
        Utilities.showHud()
        WebServiceSubClass.register(registerModel:objectRegister, showHud: false, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                Utilities.displayAlert(AppName, message: json["message"].string ?? "", completion: {_ in
                    let loginModel = UserInfo.init(fromJson: json)
                    let loginModelDetails = loginModel
                    SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                    SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                   
                    appDel.navigateToEnterCategoryData()
                    
                }, acceptTitle: "OK")
                
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    func webserviceForRegisterOtp(){
        Utilities.showHud()
        WebServiceSubClass.registerOtp(registerOtpModel: objectRegister, showHud: false, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                
                let otpModel = RegisterOtpResModel.init(fromJson: json)
                self.strOtp = otpModel.otp
                self.textFieldOtp.text = ""
                
                Utilities.ShowAlert(OfMessage: "OTP sent successfully")
                
                var secondsRemaining = 30
                self.btnOTP.isEnabled = false
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                    if secondsRemaining > 1 {
                        UIView.performWithoutAnimation {
                            self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) seconds", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            secondsRemaining -= 1
                            self.btnOTP.layoutIfNeeded()
                        }
                    
                    } else if secondsRemaining == 1 {
                        UIView.performWithoutAnimation {
                            self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) second", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            secondsRemaining -= 1
                            self.btnOTP.layoutIfNeeded()
                        }
                    } else {
                        UIView.performWithoutAnimation {
                            
                            self.btnOTP.setunderline(title: "Resend OTP", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            self.btnOTP.isEnabled = true
                            Timer.invalidate()
                        }
                        
                    }
                   }
              
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    
    

}
