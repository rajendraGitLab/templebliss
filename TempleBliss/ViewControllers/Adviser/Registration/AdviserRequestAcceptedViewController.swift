//
//  AdviserRequestAcceptedViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserRequestAcceptedViewController: BaseViewController {

    //MARK: - Properties
    var errorMessage : String = ""
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblErrorMessage: requestAcceptedLabel!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        lblErrorMessage.text = errorMessage
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    
    
    //MARK: - IBActions
    @IBAction func btnOkayClick(_ sender: Any) {

        appDel.navigateToAdvisorLogin()
    }
    
    //MARK: - API Calls
    
    
    
    
}
