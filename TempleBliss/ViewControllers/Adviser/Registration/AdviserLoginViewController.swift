//
//  AdviserLoginViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserLoginViewController: BaseViewController {

    //MARK: - Properties
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var mainScrollView: viewViewClearBG!
    @IBOutlet weak var lblLogin: loginPageLabel!
    @IBOutlet weak var lblEnterCredentials: loginPageLabel!
    @IBOutlet weak var textFieldEMail: loginPageTextField!
    @IBOutlet weak var textFieldPassword: loginPageTextField!
    @IBOutlet weak var btnForgotPassword: LoginScreenButon!
    @IBOutlet weak var btnLoginNow: theamSubmitButton!
    @IBOutlet weak var lblNewUser: loginPageLabel!
    @IBOutlet weak var btnSignUp: LoginScreenButon!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        self.navigationController?.navigationBar.isHidden = false
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        backButtonClick = {
            appDel.navigateToLogin()
        }
//        textFieldEMail.text = "vansh@yopmail.com"
//        textFieldPassword.text = "12345678"
//
        
//        if UIDevice.current.name == "iPhone 013" || UIDevice.current.name == "iPhone 016" {
//            textFieldEMail.text = "ramesh@yopmail.com"
//            textFieldPassword.text = "asdfghjk"
//        } else {
//            textFieldEMail.text = ""
//            textFieldPassword.text = ""
//        }
        GetCountryCodeList()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        SingletonClass.sharedInstance.usertype = "2"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? 0.0
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
            
        if mainScrollView.frame.size.height > UIScreen.main.bounds.height - topPadding - bottomPadding {
            (mainScrollView.superview as! UIScrollView).isScrollEnabled = true
           // mainScrollView.isScrollEnabled = true
        } else {
            (mainScrollView.superview as! UIScrollView).isScrollEnabled = false
        }
        }
    }
 
    //MARK: - other methods
    func setLocalization() {
        lblLogin.text = "Login To Continue"
        lblEnterCredentials.text = "Enter your credentials to continue with the app"
        textFieldEMail.placeholder = "Email Address"
        textFieldPassword.placeholder = "Password"
        btnForgotPassword.setTitle("Forgot Password?", for:.normal)
        btnLoginNow.setTitle("login now".uppercased(), for: .normal)
        lblNewUser.text = "New User? Click Here to"
        btnSignUp.setTitle("Sign Up", for: .normal)
         
       // textFieldEMail.text = "johndoe@gmail.com"
        
    }
    //MARK: - IBActions
    @IBAction func btnSignUpClick(_ sender: Any) {
        textFieldEMail.text = ""
        textFieldPassword.text = ""
        let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserRegisterViewController.storyboardID)
        SingletonClass.sharedInstance.usertype = "2"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        textFieldEMail.text = ""
        textFieldPassword.text = ""
        let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserForgotPasswordViewController.storyboardID)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnLoginClick(_ sender: Any) {
        
//        userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
//        userDefault.setValue(false, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
//        userDefault.synchronize()
//        appDel.navigateToHomeAdviser()
        if validation(){
          
            
            webserviceForlogin()
        }
    }
    //MARK: - API Calls
    func webserviceForlogin()
    {
        let login = LoginReqModel()
        login.user_name = textFieldEMail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        login.password = textFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        login.device_token = SingletonClass.sharedInstance.DeviceToken
        login.device_type = ReqDeviceType
        login.role = SingletonClass.sharedInstance.usertype
        
        Utilities.showHud()
        WebServiceSubClass.login(loginModel: login, showHud: false, completion: {(json, status, response) in
            
            Utilities.hideHud()
            if(status) {
                
                let loginModel = UserInfo.init(fromJson: json)
                let loginModelDetails = loginModel
               
                if loginModelDetails.profile.categoryId == "" {
                    SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                    SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                    appDel.navigateToEnterCategoryData()
                }
                else {
                    SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                    SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                    SingletonClass.sharedInstance.LoginRegisterUpdateData = loginModelDetails
                    userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                    userDefault.setValue(loginModelDetails.profile.apiKey , forKey: UserDefaultsKey.X_API_KEY.rawValue)
                    userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue)
                    
                    userDefault.setUserData(objProfile: loginModelDetails)
                    
                    appDel.navigateToHomeAdviser()
                    
                }
    
            }else {
                let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserRequestAcceptedViewController.storyboardID) as! AdviserRequestAcceptedViewController
                controller.errorMessage = json["message"].string ?? ""
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                self.present(navigationController, animated: true, completion: nil)
                //Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
        
        })
    }
    
    func validation() -> Bool
    {
        let checkEmail = textFieldEMail.validatedText(validationType:  ValidatorType.email)
        let checkPassword = textFieldPassword.validatedText(validationType: ValidatorType.password)
        if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
        else  if(!checkPassword.0)
        {
            Utilities.ShowAlert(OfMessage: checkPassword.1)
            return checkPassword.0
        }
        return true
    }
    
    
}
extension AdviserLoginViewController {
   func GetCountryCodeList() {
       WebServiceSubClass.GetCountryCode(strParams: "", showHud: false, completion: { (json, status, error) in
           if status {
               
               let CodeData = CountryListWithCode.init(fromJson: json)
               SingletonClass.sharedInstance.CountryNameWithCode = CodeData.countryCode
               
           } else {
             Utilities.displayAlert(AppName, message: error as? String ?? "Something went wrong")
           }
          
           
       })
   }
}
