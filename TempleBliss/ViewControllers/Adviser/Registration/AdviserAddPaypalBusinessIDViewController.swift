//
//  AddPaypalBusinessIDViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserAddPaypalBusinessIDViewController: BaseViewController {

    //MARK: - Properties
    
    
    @IBOutlet weak var textFieldEmail: addPayPalBusinessIDTextField!
    
    var adviserAddedCategory = String()
    //MARK: - IBOutlets
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(adviserAddedCategory)
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "PayPal Business Account", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //MARK: - IBActions
    @IBAction func btnSubmitClick(_ sender: Any) {
        
        
        if validation(){
            webServiceCallForAddCategory()
        }
        
      
       
    }
    
    func validation()->Bool{
     
        let checkEmail = textFieldEmail.validatedText(validationType: ValidatorType.email)
      
        
        if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
      
        return true
    }
    
    //MARK: - API Calls
    
}
extension AdviserAddPaypalBusinessIDViewController  {
    func webServiceCallForAddCategory() {
        let requestForAddAvailabilty = setAddNewCategory()
        requestForAddAvailabilty.availability = adviserAddedCategory
        requestForAddAvailabilty.user_id = SingletonClass.sharedInstance.UserId
        requestForAddAvailabilty.account_detail = textFieldEmail.text ?? ""
        
        Utilities.showHud()
        WebServiceSubClass.setPostRegisterDetails(addCategory: requestForAddAvailabilty, showHud: false, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserRequestAcceptedViewController.storyboardID) as! AdviserRequestAcceptedViewController
                controller.errorMessage = json["message"].string ?? "Something want wrong".Localized()
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                self.present(navigationController, animated: true, completion: nil)
               
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
       
    }
}
