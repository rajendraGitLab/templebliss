//
//  AdviserRegisterViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserRegisterViewController:BaseViewController, UITextViewDelegate {
    
    //MARK: - Properties
    var isSocialLogin : Bool = false
    var strSelectedDate = ""
    //MARK: - IBOutlets
    @IBOutlet weak var TextFieldSelectCountryCode: CountryCodeTextField!
    
    @IBOutlet weak var lblRegisterToContinue: RegistrationPageLabel!
    @IBOutlet weak var lblDescription: RegistrationPageLabel!
    
    @IBOutlet weak var textFieldFullName: RegisterTextField!
    
    @IBOutlet weak var textFieldNickName: RegisterTextField!
    
    @IBOutlet weak var dtPicker: UIDatePicker!
    @IBOutlet weak var textFieldEmail: RegisterTextField!
    @IBOutlet weak var textFieldPhone: RegisterTextField!
    @IBOutlet weak var textFieldDOB: RegisterTextField!
    @IBOutlet weak var textFieldPassword: RegisterTextField!
    @IBOutlet weak var textFieldConfirmPassoword: RegisterTextField!
    @IBOutlet weak var lblByCLicking: RegistrationPageLabel!
    @IBOutlet weak var btnTerms: RegisterScreenButon!
    @IBOutlet weak var btnRegisterNow: theamSubmitButton!
    @IBOutlet weak var textViewDescription: themeTextView!
    
    @IBOutlet weak var lblAlreadyUser: RegistrationPageLabel!
    @IBOutlet weak var btnClickHereToLogin: RegisterScreenButon!
    let pickerViewForAll = GeneralPickerView()
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldPhone.keyboardType = .asciiCapableNumberPad
        if SingletonClass.sharedInstance.CountryNameWithCode.count != 0 {
            self.TextFieldSelectCountryCode.text = SingletonClass.sharedInstance.CountryNameWithCode[0].dialCode
            self.setupDelegateForPickerView()
        } else {
            self.TextFieldSelectCountryCode.text = "+91"
        }
        
        setLocalization()
        setValue()
        textFieldPhone.delegate = self
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        textViewDescription.delegate = self
        TextFieldSelectCountryCode.delegate = self
      //  textViewDescription.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
//    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
//        var bottom = textViewDescription.contentSize.height - textViewDescription.frame.size.height
//        if bottom < 0 {
//            bottom = 0
//        }
//        if textViewDescription.contentOffset.y != bottom {
//            textViewDescription.setContentOffset(CGPoint(x: 0, y: bottom), animated: true)
//        }
//    }
    func scrollTextViewToBottom(textView: UITextView) {
        if textView.text.count > 0 {
            let location = textView.text.count - 1
            let bottom = NSMakeRange(location, 1)
            textView.scrollRangeToVisible(bottom)
        }
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        lblRegisterToContinue.text = "Register To Continue"
        lblDescription.text = "Enter your details to continue with TempleBliss"
        
        textFieldFullName.placeholder = "Full Name"
        textFieldNickName.placeholder = "Nick Name"
        textFieldEmail.placeholder = "Email Address"
        textFieldPhone.placeholder = "Phone Number"
        textFieldDOB.placeholder = "DOB"
        textFieldPassword.placeholder = "Password"
        textFieldDOB.inputView = dtPicker
        textFieldConfirmPassoword.placeholder = "Confirm Password"
        lblByCLicking.text = "By clicking register, I am agree with"
        btnTerms.setTitle("T&C", for: .normal)
        btnRegisterNow.setTitle("REGISTER NOW", for: .normal)
        
        lblAlreadyUser.text = "Already User?"
        btnClickHereToLogin.setTitle("Click here to Login", for: .normal)
        
        textFieldDOB.setInputViewDatePicker(target: self, selector: #selector(self.btnDoneDatePickerClicked(_:)))
        
    }
    func setUpDatePicker() {
        if "\(userDefault.value(forKey: UserDefaultsKey.selLanguage.rawValue) ?? "")" == "ar" {
            dtPicker.calendar = Calendar(identifier: .islamicTabular)
        } else {
            dtPicker.calendar = Calendar(identifier: .gregorian)
        }
        dtPicker.maximumDate = Date()
        dtPicker.datePickerMode = .date
        dtPicker.date = Date()
    }
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        
        pickerViewForAll.generalPickerDelegate = self
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == TextFieldSelectCountryCode {
            TextFieldSelectCountryCode.inputView = pickerViewForAll
            TextFieldSelectCountryCode.inputAccessoryView = pickerViewForAll.toolbar
            
            
            if let DummyFirst = SingletonClass.sharedInstance.CountryNameWithCode.first(where: {$0.code == TextFieldSelectCountryCode.text ?? ""}) {
                
                let indexOfA = SingletonClass.sharedInstance.CountryNameWithCode.firstIndex(of: DummyFirst) ?? 0
                pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
               
                self.pickerViewForAll.reloadAllComponents()
            }
            
            
            
//            let indexOfA = SingletonClass.sharedInstance.CountryNameWithCode.firstIndex(of: SingletonClass.sharedInstance.CountryNameWithCode.first(where: {$0.code == TextFieldSelectCountryCode.text ?? ""})!) ?? 0
//
//            pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
//            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    //MARK: - IBActions
    
    @IBAction func btnRegisterNowClick(_ sender: UIButton) {
       
        if validation(){
            webserviceForRegisterOtp()
            sender.isEnabled = true
        }
    }

    @IBAction func btnTermsClick(_ sender: Any) {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonWebViewViewController.storyboardID) as! CommonWebViewViewController
        controller.strNavTitle = "Terms & Conditions"
        controller.strUrl = SingletonClass.sharedInstance.settingsModel?.advisorTermsCondition ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnLoginClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnDoneDatePickerClicked(_ sender: UIButton) {
        
        if let datePicker = self.textFieldDOB.inputView as? UIDatePicker {
            
                let formatter = DateFormatter()
                formatter.dateFormat = DateFormatterString.onlyDate.rawValue
                
                textFieldDOB.text = formatter.string(from: datePicker.date)
          
        }
        self.textFieldDOB.resignFirstResponder() // 2-5
        
//        let dt = dtPicker.date
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
//        let resultString = inputFormatter.string(from: dt)
//        textFieldDOB.text = resultString
//        strSelectedDate = resultString
//        self.textFieldDOB.resignFirstResponder()
//        vwDtPicker.isHidden = true
    }
    //MARK: -  IBActions
    func validation()->Bool{
        let firstName = textFieldFullName.validatedText(validationType: ValidatorType.username(field: "full name"))
        let nickName = textFieldNickName.validatedText(validationType: ValidatorType.username(field: "nick name"))
        let checkEmail = textFieldEmail.validatedText(validationType: ValidatorType.email)
        let checkPassword = textFieldPassword.validatedText(validationType: ValidatorType.password)
        let confirmPassword = textFieldConfirmPassoword.validatedText(validationType: ValidatorType.requiredField(field: "confirm Password"))
        
        if (!firstName.0){
            Utilities.ShowAlert(OfMessage: firstName.1)
            return firstName.0
        }else if (!nickName.0){
            Utilities.ShowAlert(OfMessage: nickName.1)
            return nickName.0
        }else if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
        else if textFieldPhone.text?.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter phone number")
            return false
        }
//        else if (textFieldPhone.text?.count ?? 0) < 10 {
//            Utilities.ShowAlert(OfMessage: "Please enter valid phone number")
//            return false
//        }
        else if textFieldDOB.text == "" {
            Utilities.ShowAlert(OfMessage: "Please select date of birth")
            return false
        }else if textViewDescription.text.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter description")
            return false
        }
        else  if(!checkPassword.0) && !isSocialLogin
        {
            Utilities.ShowAlert(OfMessage: checkPassword.1)
            return checkPassword.0
        }else if(!confirmPassword.0){
            Utilities.ShowAlert(OfMessage: confirmPassword.1)
            return confirmPassword.0
        }else if textFieldPassword.text?.lowercased() != textFieldConfirmPassoword.text?.lowercased(){
            Utilities .ShowAlert(OfMessage: "Password and confirm password must be same")
            return false
        }
        return true
    }
    //MARK: - API Calls
    func webserviceForRegisterOtp() {
        let register = RegisterReqModel()
        
        register.full_name = textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.email = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.phone = (TextFieldSelectCountryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") + (textFieldPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
        let showDate = inputFormatter.date(from: textFieldDOB.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? Date()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let resultString = inputFormatter.string(from: showDate)
        register.dob = resultString
        register.password = textFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.confirm_password = textFieldConfirmPassoword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.nick_name = textFieldNickName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.advisor_description = textViewDescription.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        register.device_token = SingletonClass.sharedInstance.DeviceToken
        register.device_type = ReqDeviceType
        register.role = SingletonClass.sharedInstance.usertype
        register.social_id = ""
        register.social_type = ""
        Utilities.showHud()
        WebServiceSubClass.registerOtp(registerOtpModel: register, showHud: false, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                let otpModel = RegisterOtpResModel.init(fromJson: json)
                let vc = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: "AdviserEnterOTPViewController")as! AdviserEnterOTPViewController
                vc.objectRegister = register
                vc.strOtp = otpModel.otp
                self.navigationController?.pushViewController(vc, animated: true)
                //appDel.navigateToHomeAdviser()
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
}
extension AdviserRegisterViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldPhone {
            
            let charsLimit = 20

               let startingLength = textField.text?.count ?? 0
               let lengthToAdd = string.count
               let lengthToReplace =  range.length
               let newLength = startingLength + lengthToAdd - lengthToReplace

               return newLength <= charsLimit
            
            
        } else if textField == textFieldFullName || textField == textFieldNickName {
            let charsLimit = NameTotalCount
            
            let startingLength = textField.text?.count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace =  range.length
            let newLength = startingLength + lengthToAdd - lengthToReplace
            
            return newLength <= charsLimit
        } else {
            let newText = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            if textField.tag == 3 || textField.tag == 4{
                return numberOfChars < 15
            }else{
                return numberOfChars < 50
            }
        }
       
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
  
}
extension AdviserRegisterViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        let item = SingletonClass.sharedInstance.CountryNameWithCode[pickerViewForAll.selectedRow(inComponent: 0)]
        self.TextFieldSelectCountryCode.text = item.dialCode
        self.TextFieldSelectCountryCode.resignFirstResponder()
       
        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension AdviserRegisterViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return SingletonClass.sharedInstance.CountryNameWithCode.count
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return SingletonClass.sharedInstance.CountryNameWithCode[row].name
        
        
    }
    
}
