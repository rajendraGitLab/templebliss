//
//  AdviserEnterNewPasswordViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserEnterNewPasswordViewController: BaseViewController {
    
    //MARK: - Properties
    
    //MARK: - IBOutlets
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
  
    @IBAction func btnsaveClick(_ sender: Any)
   {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: AdviserLoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
      
    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
    
}
