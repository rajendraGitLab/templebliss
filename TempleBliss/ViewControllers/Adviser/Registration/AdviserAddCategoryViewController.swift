//
//  AdviserAddCategoryViewController.swift
//  TempleBliss
//
//  Created by Apple on 28/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
var categoryDataForAdviser : [addCategoryData] = []
class AdviserAddCategoryViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    
    //var priceStartFrom = SingletonClass.sharedInstance.categoryDataForAdviser?.category[0].priceRange[0].from ?? ""
    //MARK: -  Properties
    
    
    var categoryRequestModel : [CategoryAddRequestModel] = []
    
    //MARK: -  IBOutlets
    
    @IBOutlet weak var tblAddCategory: UITableView!
    //MARK: -  View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        categoryDataForAdviser.removeAll()
        
        self.navigationController?.navigationBar.isHidden = false
       // self.navigationController?.navigationBar.backgroundColor = .yellow
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Add Categories", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
      
        if SingletonClass.sharedInstance.initDataForAdviser?.categoryCount  != "0" {
            let tempInt:Int = Int(SingletonClass.sharedInstance.initDataForAdviser?.categoryCount ?? "3") ?? 3
            for i in 0...tempInt - 1 {
                
                categoryDataForAdviser.append(addCategoryData(id: "0", adviserSubCategory: [adviserAddSubCategory(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: ""),adviserAddSubCategory(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: ""),adviserAddSubCategory(name: indexColorForCommunicationType.video.CommunicationTypeName, price: "")], expand: false, name: "Select Category"))

            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: -  other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    
    //MARK: -  tableview Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if categoryDataForAdviser[section].isExpanded {
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category?[section].priceRange.count ?? 0
            
        }
        
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblAddCategory.dequeueReusableCell(withIdentifier: AdviserAddCategoryCell.reuseIdentifier, for: indexPath) as! AdviserAddCategoryCell
        cell.textFieldCategoryPrice.tag = indexPath.row
        
        cell.lblCategoryName.text = categoryDataForAdviser[indexPath.section].subCategory[indexPath.row].categoryName?.uppercased()
        cell.textFieldCategoryPrice.superview?.layer.borderWidth = 1
        
        
        cell.indexPathForTextField = indexPath
        
        switch categoryDataForAdviser[indexPath.section].subCategory[indexPath.row].categoryName
        //SingletonClass.sharedInstance.categoryDataForAdviser?.category?[indexPath.section].priceRange?[indexPath.row].type
        {
        case indexColorForCommunicationType.chat.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#2AD916")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#2AD916").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#2AD916")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_chat")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#2AD916")
            
        case indexColorForCommunicationType.audio.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#F3A791")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#F3A791").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#F3A791")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_audioCall")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#F3A791")
            
        case indexColorForCommunicationType.video.CommunicationTypeName:
            cell.textFieldCategoryPrice.textColor = UIColor(hexString: "#22A9E5")
            cell.textFieldCategoryPrice.superview?.layer.borderColor = UIColor(hexString: "#22A9E5").cgColor
            cell.textFieldCategoryPrice.attributedPlaceholder = NSAttributedString(string: "$0.00",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexString: "#22A9E5")])
            cell.categoryIcon.image = UIImage(named: "ic_AdviserDetails_videoCall")
            
            (cell.textFieldCategoryPrice.rightView?.subviews.first as? UILabel)?.textColor = UIColor(hexString: "#22A9E5")
            
        default:
            break
        }
        
        cell.textFieldCategoryPrice.text = categoryDataForAdviser[indexPath.section].subCategory[indexPath.row].categoryPrice
      
        if let amountString = cell.textFieldCategoryPrice.text?.currencyInputFormatting() {
            cell.textFieldCategoryPrice.text = amountString
        }
//        cell.isOneMonethDone = false
//        NotificationCenter.default.addObserver(forName: UITextField.textDidBeginEditingNotification, object: cell.textFieldCategoryPrice, queue: OperationQueue.main, using: { (notification) in
//            if cell.isOneMonethDone {
//                cell.textFieldCategoryPrice.resignFirstResponder()
//                let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
//
//                controller.StringButton = "Okay"
//                controller.isShowLeftIcon = false
//                controller.isShowRightIcon = false
//                controller.isShowNoteTextView = false
//                controller.stringDescription = "You are not allowed to change the price more than one time in a month."
//
//                controller.btnSubmitClosour = {
//                    self.dismiss(animated: true, completion: nil)
//                }
//
//                let navigationController = UINavigationController(rootViewController: controller)
//                navigationController.modalPresentationStyle = .overCurrentContext
//                navigationController.modalTransitionStyle = .crossDissolve
//                self.present(navigationController, animated: true, completion: nil)
//            }
//
//        })
        //if indexPath.section == 3 {
           
       // }
        
        
        return cell
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return Int(SingletonClass.sharedInstance.initDataForAdviser?.categoryCount ?? "") ?? 0
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var minimumRange = ""
        var maximumRange = ""
        for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1 {
            if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviser[indexPath.section].category_name{
                minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].from ?? "1.00"
                maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPath.row].to ?? "11.00"
            }
        }
        
        
        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserCommonPopupViewController.storyboardID) as! AdviserCommonPopupViewController
        
        controller.StringButton = "Okay"
        controller.isShowLeftIcon = false
        controller.isShowRightIcon = false
        controller.isShowNoteTextView = false
        controller.stringDescription = "You can set your price between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min."
        
        controller.btnSubmitClosour = {
            self.dismiss(animated: true, completion: nil)
        }
        
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .crossDissolve
        self.present(navigationController, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 76
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tblAddCategory.dequeueReusableCell(withIdentifier: adviserHeaderCell.reuseIdentifier) as! adviserHeaderCell
        cell.indexPathForName = section
        cell.tblView = tableView
        cell.TextFieldCategoryName.text = categoryDataForAdviser[section].category_name
        if categoryDataForAdviser[section].isExpanded {
            
            cell.btncategoryDelete.setImage(UIImage(named: "ic_removeAdviserCategory"), for: .normal)
            
        } else {
            
            cell.btncategoryDelete.setImage(UIImage(named: "ic_addAdviserCategory"), for: .normal)
            
        }
       
        cell.deleteButton.superview?.isHidden = true
        if categoryDataForAdviser[section].category_name != "Select Category" {
            cell.deleteButton.superview?.isHidden = false
        }
        cell.backgroundColor = .red
        
        cell.deleteCategoryClosure = {
            
            categoryDataForAdviser[section] = addCategoryData(id: "0", adviserSubCategory: [adviserAddSubCategory(name: indexColorForCommunicationType.chat.CommunicationTypeName, price: ""),adviserAddSubCategory(name: indexColorForCommunicationType.audio.CommunicationTypeName, price: ""),adviserAddSubCategory(name: indexColorForCommunicationType.video.CommunicationTypeName, price: "")], expand: false, name: "Select Category")
            
            
            self.tblAddCategory.reloadData()
        }
     
        cell.btnCategoryAddClosure = { [self] in
            if categoryDataForAdviser[section].category_id != "0" {
                if categoryDataForAdviser[section].isExpanded {
                    categoryDataForAdviser[section].isExpanded = false
                    
                } else {
                    categoryDataForAdviser[section].isExpanded = true
                }
                
                DispatchQueue.main.async {
                    tblAddCategory.reloadData()
                }
                DispatchQueue.main.async {
                    self.tblAddCategory.scrollToRow(at: IndexPath(item: NSNotFound, section: section), at: .top, animated: true)
                    self.tblAddCategory.layoutIfNeeded()
                }
            }
            else {
//                let alert = UIAlertController(title: AppInfo.appName, message: "Please select Category", preferredStyle: UIAlertController.Style.alert)
//                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
//               // present(alert, animated: true, completion: nil)
//                self.present(alert, animated: true, completion: nil)
                //appDel.window?.rootViewController?.children[0].present(alert, animated: true, completion: nil)
                //Utilities.showAlert(AppInfo.appName, message: "Please select Category", vc: self)
                
                Utilities.ShowAlert(OfMessage: "Please select Category")
                DispatchQueue.main.async {
                    tblAddCategory.reloadData()
                }
            }
        }
        
        cell.selectedCategoryData = {
            
            categoryDataForAdviser[section].category_name = cell.selectedCategoryName
            categoryDataForAdviser[section].category_id = cell.selectedCategoryID
            self.tblAddCategory.reloadData()
        }
        let yourSection = section
        let lastRow = tblAddCategory.numberOfRows(inSection: yourSection) - 1

        
        print(lastRow)
        
        return cell
        
        
    }
    
    
    //MARK: -  IBActions
    
    @IBAction func btnNextClick(_ sender: Any) {
        categoryRequestModel.removeAll()
        
            let validationTbl = validationForTbl()
            if validationTbl {
                
                if categoryRequestModel.count < 3 {
                    Utilities.ShowAlert(OfMessage: "Please select atleast one category")
                    
                } else {
                    
                    let productsDict = CategoryAddRequestModel.convertDataCartArrayToProductsDictionary(arrayDataCart: categoryRequestModel);
                    
                    let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
                    let jsonString:String = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
                    
                    let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserAddPaypalBusinessIDViewController.storyboardID) as! AdviserAddPaypalBusinessIDViewController
                    controller.adviserAddedCategory = jsonString as! String
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                }
                
                

            } else {
                Utilities.ShowAlert(OfMessage: "")
            }
        
        
//
//        if validation().1 {
//
//            let tempArrayForValues = categoryRequestModel.map{$0.category_id}.flatMap{$0}
//            let dups = Dictionary(grouping: tempArrayForValues, by: {$0}).filter { $1.count > 3 }.keys
//
//            if categoryRequestModel.count < 3 {
//                Utilities.ShowAlert(OfMessage: "Please select atleast one category")
//
//            } else if dups.count != 0 {
//                Utilities.ShowAlert(OfMessage: "You cannot enter same category")
//            } else {
//
//
//            }
//
//        }
//
//        else {
//            Utilities.ShowAlert(OfMessage: validation().0)
//        }
      
    }
//
//    func validation() -> (String,Bool) {
//        var validateMessage = ("",true)
//
//        let checkValidationForPrice = checkPriceIsinRangeOrNot()
//        if checkValidationForPrice.1 {
//            for i in 0...categoryDataForAdviser.count - 1 {
//                for j in 0...categoryDataForAdviser[i].subCategory.count - 1 {
//
//                    if categoryDataForAdviser[i].category_name != "Select Category" {
//                        let newData =  CategoryAddRequestModel.init(id: categoryDataForAdviser[i].category_id, CategoryType: categoryDataForAdviser[i].subCategory[j].categoryName ?? "", CategoryPrice: categoryDataForAdviser[i].subCategory[j].categoryPrice?.replacingOccurrences(of: Currency, with: "") ?? "")
//                        print(newData)
//                        categoryRequestModel.append(newData)
//                        print(categoryRequestModel)
//                    }
//
//
//                }
//            }
//        } else {
//            Utilities.ShowAlert(OfMessage: checkValidationForPrice.0)
//        }
//        return validateMessage
//
//    }
    func CheckCategoryDuplicate() -> (String,Bool) {
        var validateMessage = ("",false)
        print(#function)
        var categoryNameArray : [String] = []
        categoryDataForAdviser.forEach { (element) in
            if element.category_name != "" && element.category_name != "Select Category" {
                categoryNameArray.append(element.category_name )
            }
        }
        let hasDuplicates = categoryNameArray.count != Set(categoryNameArray).count
        
        if hasDuplicates {
            validateMessage = ("Sorry! You can not select same category multiple time",false)
            
        } else {
            validateMessage = ("",true)
        }
        
        return validateMessage
    }
    
    
    
    func validationForTbl() -> Bool {
        let checkDuplication = CheckCategoryDuplicate()
        if checkDuplication.1 {
            
            let checkPrice = checkPriceIsinRangeOrNot()
            if checkPrice.1 {
                
                for i in 0...categoryDataForAdviser.count - 1 {

                    if categoryDataForAdviser[i].category_name != "Select Category" {
                        for j in 0...(categoryDataForAdviser[i].subCategory.count ) - 1 {

                        print(j)

                            let element = categoryDataForAdviser[i]
                            let subElement = categoryDataForAdviser[i].subCategory[j]
                            
                            switch subElement.categoryName {
                            case indexColorForCommunicationType.chat.CommunicationTypeName:
                                let newData =  CategoryAddRequestModel.init(id: categoryDataForAdviser[i].category_id, CategoryType: "chat", CategoryPrice: categoryDataForAdviser[i].subCategory[j].categoryPrice?.replacingOccurrences(of: Currency, with: "") ?? "")
                               
                                categoryRequestModel.append(newData)
                            case indexColorForCommunicationType.audio.CommunicationTypeName:
                                
                                let newData =  CategoryAddRequestModel.init(id: categoryDataForAdviser[i].category_id, CategoryType: "audio", CategoryPrice: categoryDataForAdviser[i].subCategory[j].categoryPrice?.replacingOccurrences(of: Currency, with: "") ?? "")
                                
                                categoryRequestModel.append(newData)
                            case indexColorForCommunicationType.video.CommunicationTypeName:
                                
                                let newData =  CategoryAddRequestModel.init(id: categoryDataForAdviser[i].category_id, CategoryType: "video", CategoryPrice: categoryDataForAdviser[i].subCategory[j].categoryPrice?.replacingOccurrences(of: Currency, with: "") ?? "")
                                
                                categoryRequestModel.append(newData)
                            default:
                                break
                            }
                            
                        }

                    }
                }
                
            } else {
                Utilities.ShowAlert(OfMessage: checkPrice.0)
                return false
            }
            
            
        } else {
            Utilities.ShowAlert(OfMessage: checkDuplication.0)
            return false
        }
        
        
        
//        let checkPrice = checkPriceIsinRangeOrNot()
//        if checkPrice.1 {
//
//            let checkDuplication = CheckCategoryDuplicate()
//            if checkDuplication.1  else {
//
//            }
//
//
////
////
////            let tempArrayForValues = categoryRequestModel.map{$0.new_category_id}.flatMap{$0}
////            let dups = Dictionary(grouping: tempArrayForValues, by: {$0}).filter { $1.count > 3 }.keys
////            print(dups)
////            if dups.count != 0 {
////                validateMessage = ("You cannot enter same category",false)
////            } else {
////                validateMessage = ("",true)
////            }
//        }
//        else {
//
//        }
        return true
        
    }
    /*
    func validatbliionForCategory() -> (String,Bool) {
        var validateMessage = ("",false)
        var minimumRange = ""
        var maximumRange = ""
        
        outerLoop: for element in categoryDataForAdviser {
            element.subCategoryData?.forEach({ (subElement) in
                if element.categoryName != "Select Category" {
                    let foundItems = SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter( {$0.id == element.NewCategoryID})
                    
                    if foundItems?.count != 0 {
                        
                        foundItems?.forEach({ (foundItemsForeach) in
                            if foundItemsForeach.priceRange.count != 0 {
                                 foundItemsForeach.priceRange.forEach({ (priceRangeElement) in
                                    minimumRange = priceRangeElement.from
                                    maximumRange = priceRangeElement.to
                                    
                                    let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
                                    
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
                                    
                                    let dateForCompare:Date = dateFormatter.date(from: subElement.subLastUpdatedDate ?? "") ?? Date()
                                    
                                    let dateCompare = dateNow.months(from: dateForCompare)
                                    
                                    
                                    if dateCompare > 1 {
                                        if element.OldCategoryID == "0" && element.NewCategoryID != "0" {
                                            
                                            if foundItemsForeach.priceRange.count == element.subCategoryData?.count {
                                                
                                                if subElement.subCategoryPriceNew == "" {
                                                    validateMessage = ("Please enter proper price",false)
                                                 
                                                   // return validateMessage
                                                } else {
                                                    if let amountString = subElement.subCategoryPriceNew?.currencyInputFormatting() {
                                                        var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                                        repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                                        
                                                        let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                                        let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                                        
                                                        let textInDouble:Double = Double(repalacedText) ?? 0.0
                                                        
                                                        if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                            validateMessage = ("Please enter proper price",false)
                                                           
                                                           // return validateMessage
                                                            
                                                        } else {
                                                            validateMessage = ("Please enter proper price",true)
                                                        }
                                                    }
                                                }
                                                
                                            }
                                        }
                                    } else {
                                        if let amountString = subElement.subCategoryPriceNew?.currencyInputFormatting() {
                                            var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                            repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                                            
                                            let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                            let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                                            
                                            let textInDouble:Double = Double(repalacedText) ?? 0.0
                                            
                                            if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                                validateMessage = ("Please enter proper price",false)
                                                
                                               // return validateMessage
                                                
                                            } else {
                                                validateMessage = ("Please enter proper price",true)
                                            }
                                        }
                                    }
                                })
                            }
                        })
                    }
                }
            })
        }
//         categoryDataForAdviser.forEach { (element) in
//
//        }
        return validateMessage
    }
    
    */
    
    
//    func checkPriceIsinRangeOrNot() -> (String,Bool) {
//        var validateMessage = ("",true)
//        var minimumRange = ""
//        var maximumRange = ""
//
//
//        for i in 0...categoryDataForAdviser.count - 1 {
//            if categoryDataForAdviser[i].category_name != "Select Category" {
//
//                let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviser[i].category_id }
//
//
//
//                if foundItems?.count != 0 {
//                    for jj in 0...(foundItems?.count ?? 0) - 1 {
//
//                        for kk in 0...(foundItems?[jj].priceRange.count ?? 0) - 1 {
//                            minimumRange = foundItems?[jj].priceRange[kk].from ?? ""
//                            maximumRange = foundItems?[jj].priceRange[kk].to ?? ""
//
//                            let dateNow = Calendar.current.date(bySettingHour: 0, minute: 0, second: 0, of: Date())!
//
//                            let dateFormatter = DateFormatter()
//                            dateFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
//
//                            let dateForCompare:Date = dateFormatter.date(from: categoryDataForAdviser[i].subCategory[kk].subLastUpdatedDate ?? "") ?? Date()
//
//                            let dateCompare = dateNow.months(from: dateForCompare)
//
//                            if dateCompare < 1  {
//                                if categoryDataForAdviser[i].category_id == "0" {
//                                   if foundItems?[jj].priceRange.count == categoryDataForAdviser[i].subCategoryData?.count {
//                                       if categoryDataForAdviser[i].subCategoryData?[kk].subCategoryPriceNew == "" {
//                                           validateMessage = ("Please enter proper price",false)
//                                           return validateMessage
//                                       } else {
//                                           if let amountString = categoryDataForAdviser[i].subCategoryData?[kk].subCategoryPriceNew?.currencyInputFormatting() {
//
//                                               var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
//                                               repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
//
//                                               let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
//                                               let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
//
//                                               let textInDouble:Double = Double(repalacedText) ?? 0.0
//
//                                               print("the category \(categoryDataForAdviser[i].categoryName ?? "") is and the type is \(categoryDataForAdviser[i].subCategoryData?[kk].subCategoryName ?? "") and the price for is: \(textInDouble) in this the minimum price range is \(minimumRangeDouble) and maximum price range is \(maximumRangeDouble)")
//
//
//                                               if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
//                                                   validateMessage = ("Please enter proper price",false)
//                                                   return validateMessage
//
//                                               } else {
//                                                   validateMessage = ("Please enter proper price",true)
//                                               }
//                                           }
//                                       }
//
//                                   }
//                               }
//                            } else {
//                                if foundItems?[jj].priceRange.count == categoryDataForAdviser[i].subCategoryData?.count {
//                                    if categoryDataForAdviser[i].subCategoryData?[kk].subCategoryPriceNew == "" {
//                                        validateMessage = ("Please enter proper price",false)
//                                        return validateMessage
//                                    } else {
//                                        if let amountString = categoryDataForAdviser[i].subCategoryData?[kk].subCategoryPriceNew?.currencyInputFormatting() {
//
//                                            var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
//                                            repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
//
//                                            let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
//                                            let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
//
//                                            let textInDouble:Double = Double(repalacedText) ?? 0.0
//
//                                            print("the category \(categoryDataForAdviser[i].categoryName ?? "") is and the type is \(categoryDataForAdviser[i].subCategoryData?[kk].subCategoryName ?? "") and the price for is: \(textInDouble) in this the minimum price range is \(minimumRangeDouble) and maximum price range is \(maximumRangeDouble)")
//
//
//                                            if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
//                                                validateMessage = ("Please enter proper price",false)
//                                                return validateMessage
//
//                                            } else {
//                                                validateMessage = ("Please enter proper price",true)
//                                            }
//                                        }
//                                    }
//
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return validateMessage
//    }
    func checkPriceIsinRangeOrNot() -> (String,Bool) {
        var validateMessage = ("",false)
        var minimumRange = ""
        var maximumRange = ""

        categoryDataForAdviser.forEach { (element) in
            if element.category_name == "Select Category" {
                validateMessage = ("Please select atleast one category",false)
            }
        }

        for i in 0...categoryDataForAdviser.count - 1 {
            if categoryDataForAdviser[i].category_name != "Select Category" {

                let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviser[i].category_id }

                if foundItems?.count != 0 {
                    for jj in 0...(foundItems?.count ?? 0) - 1 {

                        for kk in 0...(foundItems?[jj].priceRange.count ?? 0) - 1 {
                            minimumRange = foundItems?[jj].priceRange[kk].from ?? ""
                            maximumRange = foundItems?[jj].priceRange[kk].to ?? ""


                            print(minimumRange)
                            print(maximumRange)


                            if foundItems?[jj].priceRange.count == categoryDataForAdviser[i].subCategory.count {
                                if let amountString = categoryDataForAdviser[i].subCategory[kk].categoryPrice?.currencyInputFormatting() {

                                    var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                                    repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)

                                    let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                                    let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0

                                    let textInDouble:Double = Double(repalacedText) ?? 0.0

                                    if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                                        //Please add price of  chat for category
                                       
                                            let msgString = "Please add price of \(categoryDataForAdviser[i].subCategory[kk].categoryName ?? "") for \(categoryDataForAdviser[i].category_name)"
                                       
                                        
                                        validateMessage = (msgString,false)
                                       // validateMessage = ("Please enter proper price",false)
                                        return validateMessage

                                    } else {
                                        validateMessage = ("Please enter proper price",true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    return validateMessage
    }
    //MARK: -  API Calls
    
}
class AdviserAddCategoryCell : UITableViewCell, UITextFieldDelegate {
    var indexPathForTextField = IndexPath()
    
    var sectionID = Int()
    var IndexID = Int()
    var isOneMonethDone = false
    @IBOutlet weak var textFieldCategoryPrice: addCategoryTextField!
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var lblCategoryName: AdviserAddCategoryLabel!
    
    override func awakeFromNib() {
        textFieldCategoryPrice.delegate = self
        textFieldCategoryPrice.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)

    }
    @objc func textFieldDidChange() {
       

            if textFieldCategoryPrice.text != "" {
                if let amountString = textFieldCategoryPrice.text?.currencyInputFormatting() {
                    textFieldCategoryPrice.text = amountString
                    categoryDataForAdviser[self.indexPathForTextField.section].subCategory[self.indexPathForTextField.row].categoryPrice = textFieldCategoryPrice.text
                    
                }
            }
        
    }
//    func textFieldDidChangeSelection(_ textField: UITextField) {
//
//
//            if textField.text != "" {
//                if let amountString = textField.text?.currencyInputFormatting() {
//                    textField.text = amountString
//                    categoryDataForAdviser[self.indexPathForTextField.section].subCategory[self.indexPathForTextField.row].categoryPrice = textField.text
//
//                }
//            }
//
////        let startPosition = textField.position(from: textField.beginningOfDocument, offset: 1)
////        let endPosition = textField.position(from: textField.beginningOfDocument, offset: 4)
////
////        if startPosition != nil && endPosition != nil {
////            textField.selectedTextRange = textField.textRange(from: startPosition!, to: endPosition!)
////        }
//
//    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textFieldCategoryPrice {
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92) {
                    textField.text = ""
                }
            }
            let charsLimit = 8
           
            
            let startingLength = textField.text?.replacingOccurrences(of: Currency, with: "").replacingOccurrences(of: ".", with: "").count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace =  range.length
            let newLength = startingLength + lengthToAdd - lengthToReplace
            
            return newLength <= charsLimit
            
        }
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        var minimumRange = ""
        var maximumRange = ""
        for i in 0...(SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 1) - 1  {
            
            if SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].name == categoryDataForAdviser[indexPathForTextField.section].category_name{
                
                minimumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].from ?? "1.00"
                maximumRange = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].to ?? "11.00"
                
                if let amountString = textField.text?.currencyInputFormatting() {
                    
                    var repalacedText = amountString.replacingOccurrences(of: Currency, with: "")
                    repalacedText = repalacedText.trimmingCharacters(in: .whitespacesAndNewlines)
                    
                    let minimumRangeDouble : Double = Double(minimumRange) ?? 0.0
                    let maximumRangeDouble : Double = Double(maximumRange) ?? 0.0
                    
                    let textInDouble:Double = Double(repalacedText) ?? 0.0
                    
                    if textInDouble < minimumRangeDouble ||  textInDouble > maximumRangeDouble {
                        
                        
                        
                        Utilities.ShowAlert(OfMessage: "Please enter text between \(Currency)\(minimumRange) to \(Currency)\(maximumRange)/min")
                        textField.text = ""
                       // textField.text = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[i].priceRange?[indexPathForTextField.row].from.currencyInputFormatting()
                        categoryDataForAdviser[indexPathForTextField.section].subCategory[indexPathForTextField.row].categoryPrice = textField.text
                        //categoryDataForAdviser[indexPathForTextField.section].subCategoryData?[indexPathForTextField.row].subCategoryPriceOld?.currencyInputFormatting()
                    } else {
                        categoryDataForAdviser[indexPathForTextField.section].subCategory[indexPathForTextField.row].categoryPrice = textField.text
                        
                    }
                }
            }
        }
    }
}
class adviserAddCategory {
    var categoryName : String?
    var isExpanded : Bool?
    var subCategoryData : [adviserAddSubCategory]?
    
    init(name:String,expanded:Bool,subCategory:[adviserAddSubCategory]) {
        self.categoryName = name
        self.isExpanded = expanded
        self.subCategoryData = subCategory
    }
}

class adviserHeaderCell : UITableViewCell {
    
    var indexPathForName = Int()
    let pickerViewForAll = GeneralPickerView()
    
    var tblView = UITableView()
    
    @IBOutlet weak var TextFieldCategoryName: AddCategoryTextField!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBAction func deleteButtonClick(_ sender: Any) {
        if let click = self.deleteCategoryClosure {
            click()
        }
    }
    var deleteCategoryClosure : (() -> ())?
    var btnCategoryAddClosure : (() -> ())?
    var selectedCategoryData : (() -> ())?
    
    var selectedCategoryID = ""
    var selectedCategoryName = ""
    
    
    @IBOutlet weak var showCategoryView: BorderView!
    @IBOutlet weak var btncategoryDelete: UIButton!
    
    
    @IBAction func btnCategoryAddClick(_ sender: UIButton) {
        if let click = self.btnCategoryAddClosure {
            click()
        }
    }
   // var arrayForCategoryName : [(name: String, id: String)] = []
    override func awakeFromNib() {
        TextFieldCategoryName.delegate = self
     
        TextFieldCategoryName.dropDownClick = { [self] in
            TextFieldCategoryName.inputView = pickerViewForAll
            TextFieldCategoryName.inputAccessoryView = pickerViewForAll.toolbar
            
            self.pickerViewForAll.reloadAllComponents()
        }

        setupDelegateForPickerView()
    }
    
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        pickerViewForAll.generalPickerDelegate = self
    }
}

extension adviserHeaderCell: GeneralPickerViewDelegate {
    
    func didTapDone() {
        let row = pickerViewForAll.selectedRow(inComponent: 0)
        
        if TextFieldCategoryName.isFirstResponder {
            
            self.selectedCategoryName = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name ?? ""
            self.selectedCategoryID = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].id ?? ""
            TextFieldCategoryName.text = self.selectedCategoryName
            
            categoryDataForAdviser[indexPathForName].category_name = self.selectedCategoryName
            categoryDataForAdviser[indexPathForName].category_id = self.selectedCategoryID
            let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviser[indexPathForName].category_id }
            if foundItems?.count != 0 {
                for i in 0...(foundItems?.count ?? 0) - 1 {
                    for j in 0...(foundItems?[i].priceRange.count ?? 0) - 1 {
                        categoryDataForAdviser[indexPathForName].subCategory[j].categoryPrice = ""

                    }
                }
            }

            
            print(categoryDataForAdviser)
            tblView.reloadData()
            
            
            
            
            
            
        }
        DispatchQueue.main.async {
            self.tblView.scrollToRow(at: IndexPath(item: NSNotFound, section: self.indexPathForName), at: .top, animated: true)
            self.tblView.layoutIfNeeded()
        }
//        if indexPathForName == 2 {
//            DispatchQueue.main.async {
//                let indexPath =  IndexPath(row:0, section: 2)
//                self.tblView.scrollToRow(at:indexPath, at: .bottom, animated: true)
//                self.tblView.layoutIfNeeded()
//            }
//        }
//        DispatchQueue.main.async {
//            self.tblView.scrollToRow(at:indexPath, at: .bottom, animated: true)
//        }

                   

     
            
//            DispatchQueue.main.async {
//
//
//
//
//
//
//
//                if self.indexPathForName == categoryDataForAdviser.count - 1 {
//                    self.tblView.scrollToBottom()
//                }
//             //   self.tblView.scrollToRow(at: IndexPath(item: 0, section: categoryDataForAdviser.count - 1), at: .bottom, animated: true)
//               // self.tblView.scrollToBottom()
//               // let indexPath = IndexPath(
//////                                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
//////                                section: self.numberOfSections - 1)
////                self.tblView.scrollToRow(at: IndexPath(row: -1, section: self.indexPathForName), at: .top, animated: true)
////                //scrollToItem(at: IndexPath(item: self.selectedCategoryType, section: 0), at: , animated: true)
//               // self.tblView.layoutIfNeeded()
//            }
      //  }
        
        
    }
    
    func didTapCancel() {
       // self.endEditing(true)
    }
}
extension adviserHeaderCell : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == TextFieldCategoryName {
           // self.tblView.reloadData()
            TextFieldCategoryName.inputView = pickerViewForAll
            TextFieldCategoryName.inputAccessoryView = pickerViewForAll.toolbar
            if let DummyFirst = (SingletonClass.sharedInstance.categoryDataForAdviser?.category?.first(where: {$0.name == TextFieldCategoryName.text ?? ""})) {
                
                let indexOfA = SingletonClass.sharedInstance.categoryDataForAdviser?.category?.firstIndex(of: DummyFirst) ?? 0
                pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
               
                self.pickerViewForAll.reloadAllComponents()
            }
            
            
             
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
       // DispatchQueue.main.async {
            self.tblView.reloadData()
      //  }
    }
    
}
extension adviserHeaderCell : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if TextFieldCategoryName.isFirstResponder {
//            
//            self.selectedCategoryName = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name ?? ""
//            self.selectedCategoryID = SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].id ?? ""
//            TextFieldCategoryName.text = self.selectedCategoryName
//            
//            categoryDataForAdviser[indexPathForName].category_name = self.selectedCategoryName
//            categoryDataForAdviser[indexPathForName].category_id = self.selectedCategoryID
//            
//            let foundItems =  SingletonClass.sharedInstance.categoryDataForAdviser?.category.filter { $0.id == categoryDataForAdviser[indexPathForName].category_id }
//            if foundItems?.count != 0 {
//                for i in 0...(foundItems?.count ?? 0) - 1 {
//                    for j in 0...(foundItems?[i].priceRange.count ?? 0) - 1 {
//                        categoryDataForAdviser[indexPathForName].subCategory[j].categoryPrice = ""
//
//                    }
//                }
//            }
//            
//            
//            print(categoryDataForAdviser)
//            tblView.reloadData()
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if TextFieldCategoryName.isFirstResponder {
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category.count ?? 0
            
        }
        return 0
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if TextFieldCategoryName.isFirstResponder {
            return SingletonClass.sharedInstance.categoryDataForAdviser?.category?[row].name
            
        }
        return ""
        
    }
    
}
extension Dictionary where Value: Equatable {
    func someKey(forValue val: Value) -> Key? {
        return first(where: { $1 == val })?.key
    }
}
class addCategoryData {
    var category_id = ""
    var category_name = ""
    var subCategory : [adviserAddSubCategory] = []
    var isExpanded : Bool
    init(id:String,adviserSubCategory:[adviserAddSubCategory],expand:Bool,name:String) {
        self.subCategory = adviserSubCategory
        self.category_id = id
        self.isExpanded = expand
        self.category_name = name
    }
}
class adviserAddSubCategory {
    var categoryPrice : String?
    var categoryName : String?
    
    init(name:String,price:String) {
        self.categoryName = name
        self.categoryPrice = price
        
    }
}
extension String {
    
    // formatting text for currency textField
    func currencyInputFormatting() -> String {
        
        var number: NSNumber!
        let formatter = NumberFormatter()
        formatter.numberStyle = .currencyAccounting
        formatter.currencySymbol = Currency
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2 
        
        var amountWithPrefix = self
        
        // remove from String: "$", ".", ","
        let regex = try! NSRegularExpression(pattern: "[^0-9]", options: .caseInsensitive)
        amountWithPrefix = regex.stringByReplacingMatches(in: amountWithPrefix, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count), withTemplate: "")
        
        let double = (amountWithPrefix as NSString).doubleValue
        number = NSNumber(value: (double / 100))
        
        // if first number is 0 or all numbers were deleted
        guard number != 0 as NSNumber else {
            return ""
        }
        return formatter.string(from: number) ?? ""
       // return "\(formatter.string(from: number)!)/min"
    }

}
