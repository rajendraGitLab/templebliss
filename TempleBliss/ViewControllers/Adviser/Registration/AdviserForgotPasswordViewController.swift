//
//  AdviserForgotPasswordViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class AdviserForgotPasswordViewController: BaseViewController {
    
    //MARK: - Properties
    
    //MARK: - IBOutlets
    @IBOutlet weak var textFieldEmail: ForgotPasswordTextField!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //MARK: - IBActions
    @IBAction func btnSendClick(_ sender: Any) {
      //  webserviceForForgotPassword()
        let checkEmail = textFieldEmail.validatedText(validationType: ValidatorType.email)

        if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return
        }else{
            webserviceForForgotPassword()
        }
    }
    
    //MARK: - API Calls
    func webserviceForForgotPassword(){
        let forgot = ForgotPasswordReqModel()
        forgot.email = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        forgot.role = SingletonClass.sharedInstance.usertype
        Utilities.showHud()
        WebServiceSubClass.ForgotPassword(forgotPassword: forgot, showHud: false, completion: { (response, status, error) in
            Utilities.hideHud()
            if (status){
                self.showAlertWithTwoButtonCompletion(title: AppName, Message: response["message"].stringValue, defaultButtonTitle: "OK", cancelButtonTitle: "") { (index) in
                    if index == 0{
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
    func showAlertWithTwoButtonCompletion(title:String, Message:String, defaultButtonTitle:String, cancelButtonTitle:String ,  Completion:@escaping ((Int) -> ())) {
        
        let alertController = UIAlertController(title: title , message:Message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: defaultButtonTitle, style: .default) { (UIAlertAction) in
            Completion(0)
        }
        if cancelButtonTitle != ""{
            let CancelAction = UIAlertAction(title: cancelButtonTitle, style: .cancel) { (UIAlertAction) in
                Completion(1)
            }
            alertController.addAction(OKAction)
            alertController.addAction(CancelAction)
        }else{
            alertController.addAction(OKAction)
        }
        self.present(alertController, animated: true, completion: nil)
    }
}
