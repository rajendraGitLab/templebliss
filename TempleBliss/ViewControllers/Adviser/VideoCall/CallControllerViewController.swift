/*

//
//  CallControllerViewController.swift
//  VirtuWoof Vet
//
//  Created by EWW071 on 21/04/20.
//  Copyright © 2020 EWW077. All rights reserved.
//

import UIKit
import TwilioVideo
import CallKit
import AVFoundation
import SwiftyJSON


class CallControllerViewController: BaseViewController {
    
    // MARK:- IBOutlets
    var soundTimer: Timer?
    var vibrationTimer: Timer?
    var playCount = 0
    static let soundId: SystemSoundID? = {
        var soundID: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &soundID)
            return soundID
        }
        return soundID
    }()
    //AudioServicesPlaySystemSound(_engineSound);

    static let timerInterval: TimeInterval = 5
//
    
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var lblTimer: UILabel?
    @IBOutlet weak var previewView: VideoView?
    @IBOutlet weak var disconnectButton: UIButton?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblPhoneNo: UILabel?
    @IBOutlet weak var viewDecline: UIView?
    @IBOutlet weak var viewAccept: UIView?
    @IBOutlet weak var viewContainerAccept: UIView?
    @IBOutlet weak var stackViewCall: UIStackView?
    @IBOutlet weak var btnFlipCamera: UIButton?
   
    var player: AVAudioPlayer?
    
    @IBOutlet weak var lblDecline: UILabel?
    @IBOutlet weak var lblAccept: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint?
    @IBOutlet weak var WidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ProfileUserView: UIView!
    
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnVideoOff: UIButton!
    @IBOutlet weak var btnPreivew: UIButton!
    
    // MARK:- Video call related custom variables
    
    var vetID : String?
    var strRoomName : String?
    var deviceToken : String?
    var is_rejected: Int?
    var isPickedUp: Int?
    //    var senderId : String?
    //    var roomName : String?
    //    var senderName : String?
    var CallingTo : String?

    var message : String?
    var visit_id : String?

    var senderID = ""
    var reciverID = ""
    var bookingID = ""

    var senderName = ""

//    var token : String?
        var boolIsFromSocket = false

    // MARK:- View Controller Members

    // Configure access token manually for testing, if desired! Create one manually in the console
    // at https://www.twilio.com/console/video/runtime/testing-tools
    var accessToken = ""

    // Video SDK components
  
    /**
     * We will create an audio device and manage it's lifecycle in response to CallKit events.
     */
 
    var camera: CameraSource?
    var localVideoTrack: LocalVideoTrack?
    var localAudioTrack: LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var remoteView: VideoView?


    
    
    @IBOutlet weak var imgvDeclinePhone: UIImageView!
    

    
//    required init?(coder aDecoder: NSCoder) {
//        let configuration = CXProviderConfiguration(localizedName: "TempleBliss")
//
//        configuration.maximumCallGroups = 1
//        configuration.maximumCallsPerCallGroup = 1
//        configuration.supportsVideo = true
//        configuration.includesCallsInRecents = false;
//        configuration.supportedHandleTypes = [.generic]
//        if let callKitIcon = UIImage(named: "iconMask80") {
//            configuration.iconTemplateImageData = callKitIcon.pngData()
//        }
//
//        self.callKitProvider = CXProvider(configuration: configuration)
//        self.callKitCallController = CXCallController()
//
//        super.init(coder: aDecoder)
//
//        self.callKitProvider?.setDelegate(self, queue: nil)
//    }
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        
         callKitProvider?.invalidate()
        
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
    }
    
    //MARK:- View Controller Life Cycle
    func getSoundID() -> SystemSoundID {
        var soundID: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &soundID)
            return soundID
        }
        return soundID
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
       // playSoundAndVibrateInLoop()
        // Do any additional setup after loading the view.
        
        //        self.title = "QuickStart"
        //1.
        self.lblName?.text = self.CallingTo
        self.navigationController?.navigationBar.isHidden = true
        
        //2.
        /*
         * The important thing to remember when providing a AudioDevice is that the device must be set
         * before performing any other actions with the SDK (such as creating Tracks, or connecting to a Room).
         * In this case we've already initialized our own `DefaultAudioDevice` instance which we will now set.
         */
        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall = DefaultAudioDevice()

        TwilioVideoSDK.audioDevice = AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall ?? DefaultAudioDevice()
        
        //3.
        NotificationCenter.default.removeObserver(self, name: .endCall, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.goBack), name: .endCall, object: nil)

        //4.
        if PlatformUtils.isSimulator {
            self.previewView?.removeFromSuperview()
        } else {
            // Preview our local camera track in the local video preview view.
            self.startPreview()
        }
        
        //5.
        // Disconnect and mic button will be displayed when the Client is connected to a Room.
        self.disconnectButton?.isHidden = true
        playSound()
        
        self.onSocketCallRejectBySender()
        
        /// For call button to come white, not blue
        self.imgvDeclinePhone.tintColor = nil
        self.imgvDeclinePhone.tintColor = .white
        
        self.setFontsOfLabels()
        
        DispatchQueue.main.async {
            self.invalidateTimers()
            //MARK:- Firebase Analytics Log
    //        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.AcceptButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
    ////        self.ProfileUserView.isHidden = false
    //        UtilityClass.showCircleInAnimatingLoader()
            self.player?.stop()
            self.webServiceForAccept()
        }
    }
    @IBAction func btnMuteClick(_ sender: UIButton) {
        
        
        if  self.btnMute.isSelected {
            print("Mic is enable now")
            
            if let localAudioTrack = self.localAudioTrack, let localParticipant =  room?.localParticipant  {

                localParticipant.publishAudioTrack(localAudioTrack)
           
                self.btnMute.isSelected = false
            }

        }else {
            print("Mic is mute now")

            if let localAudioTrack = self.localAudioTrack, let localParticipant =  room?.localParticipant  {

                localParticipant.unpublishAudioTrack(localAudioTrack)

                self.btnMute.isSelected = true
            }
            
           
            
        }
        
        
//
//
//
//
//        if let room = room, let uuid = room.uuid, let localAudioTrack = self.localAudioTrack {
//            let isMuted = localAudioTrack.isEnabled
//            let muteAction = CXSetMutedCallAction(call: uuid, muted: isMuted)
//            let transaction = CXTransaction(action: muteAction)
//
//            self.callKitCallController.request(transaction)  { error in
//                DispatchQueue.main.async {
//                    if let error = error {
//                        self.logMessage(messageText: "ATDebug :: Video Advisor Video CallSetMutedCallAction transaction request failed: \(error.localizedDescription)")
//                        return
//                    }
//                    self.logMessage(messageText: "ATDebug :: Video Advisor Video CallSetMutedCallAction transaction request successful")
//                }
//            }
//        }
    }
    func muteAudio(isMuted: Bool) {
        if let localAudioTrack = self.localAudioTrack {
            localAudioTrack.isEnabled = !isMuted

            // Update the button title
            if (!isMuted) {
                self.btnMute.setImage(#imageLiteral(resourceName: "mic_unmuted"), for: .normal)
            } else {
                self.btnMute.setImage(#imageLiteral(resourceName: "mic_muted"), for: .normal)
            }
        }
    }
    @IBAction func btnVideoOffClick(_ sender: UIButton) {
    }
    
    
    @IBAction func btnFlipCamera(_ sender: Any) {
        self.flipCamera()
    }
 
     @objc func flipCamera() {
        var newDevice: AVCaptureDevice?

        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }

            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "ATDebug :: Video Advisor Video CallError selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView?.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
            
            UIView.transition(with: self.previewView ?? UIView(), duration: 1.0, options: [.transitionFlipFromLeft], animations: {
            })
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
       
        
       // room?.disconnect()
        if let room =  room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Video Advisor Video CallAttempting to disconnect from room \(room.name)")
             userInitiatedDisconnect = true
            self.performEndCallAction(uuid: uuid)
        }
        self.goBack()
        AudioServicesDisposeSystemSoundID(getSoundID())
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setLocalisation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setLocalisation() {
        self.lblPhoneNo?.text = "Incoming Video Call"
        self.lblDecline?.text = "Decline"
        self.lblAccept.text = "Accept"
    
    }
    
    func setFontsOfLabels() {
        self.lblName?.font = UIFont.boldSystemFont(ofSize: 30.0)
        self.lblPhoneNo?.font = UIFont.systemFont(ofSize: 20.0)
        self.lblAccept.font = UIFont.systemFont(ofSize: 15.0)
        self.lblDecline?.font = UIFont.systemFont(ofSize: 15.0)
    }
    func askPermissionIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                
            })
        case .denied:
            let alert = UIAlertController(title: "Error", message: "Please allow microphone usage from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .granted:
            break
       
    }
    }
    func playSound() {

        guard let url = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }
            player.numberOfLoops = -1

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewDecline?.layer.cornerRadius = (viewDecline?.frame.size.width ?? 0) / 2
        self.viewDecline?.layer.masksToBounds = true
        
        self.viewAccept?.layer.cornerRadius = (viewAccept?.frame.size.width ?? 0) / 2
        self.viewAccept?.layer.masksToBounds = true
    }
    
    
    // MARK:- IBActions
    @IBAction func connect(sender: AnyObject) {
        self.performStartCallAction(uuid: UUID(), roomName: self.strRoomName)
    }
    
    func registerForLocalNotifications() {
        // Define the custom actions.
        let inviteAction = UNNotificationAction(identifier: "INVITE_ACTION",
                                                title: "Simulate VoIP Push",
                                                options: UNNotificationActionOptions(rawValue: 0))
        let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
                                                 title: "Decline",
                                                 options: .destructive)
        let notificationCenter = UNUserNotificationCenter.current()
        
        // Define the notification type
        let meetingInviteCategory = UNNotificationCategory(identifier: "ROOM_INVITATION",
                                                           actions: [inviteAction, declineAction],
                                                           intentIdentifiers: [],
                                                           options: .customDismissAction)
        notificationCenter.setNotificationCategories([meetingInviteCategory])
        
        // Register for notification callbacks.
        notificationCenter.delegate = self
        
        // Request permission to display alerts and play sounds.
        notificationCenter.requestAuthorization(options: [.alert])
        { (granted, error) in
            // Enable or disable features based on authorization.
        }
    }
    
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        self.viewContainerAccept?.isHidden = inRoom
        self.lblPhoneNo?.isHidden = inRoom
        self.lblTimer?.isHidden = !inRoom
        self.lblDecline?.isHidden = inRoom
        UIApplication.shared.isIdleTimerDisabled = inRoom
        self.setNeedsUpdateOfHomeIndicatorAutoHidden()
    }
    
    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.
        
        // Create an audio track.
        if (self.localAudioTrack == nil) {
            self.localAudioTrack = LocalAudioTrack()
            
            if (self.localAudioTrack == nil) {
                logMessage(messageText: "ATDebug :: Video Advisor Video CallFailed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (self.localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // MARK:- Private
    /**
     * Set up camera.
     * Add camera to local video track: represents local video produced by a `TVIVideoSource`.
     * Add preview view to local video track.
     * Tap gesture for front or back preview.
     * Start capturing for video by camera.
     *
     */
    func startPreview() {
        if PlatformUtils.isSimulator {
            return
        }
        
        let frontCamera = CameraSource.captureDevice(position: .front)
        let backCamera = CameraSource.captureDevice(position: .back)
        
        if (frontCamera != nil || backCamera != nil) {
            // Preview our local camera track in the local video preview view.
            self.camera = CameraSource(delegate: self)
            self.localVideoTrack = LocalVideoTrack(source: camera!, enabled: true, name: "Camera")
            
            // Add renderer to video track for local preview
            self.localVideoTrack!.addRenderer(self.previewView ?? VideoView())
            logMessage(messageText: "ATDebug :: Video Advisor Video CallVideo track created")
            
            if (frontCamera != nil && backCamera != nil) {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(CallControllerViewController.flipCamera))
                self.previewView?.addGestureRecognizer(tap)
            }
//            let format = VideoFormat()
//            format.dimensions = CMVideoDimensions(width:1280, height: 720)
//            format.frameRate = 30

            logMessage(messageText: "ATDebug :: Video Advisor Video Callcamera?.startCapture")
            self.camera?.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
             
                print("The error is \(error?.localizedDescription ?? "-")")
                print("The capture device is \(captureDevice.localizedName)")
                if let error = error {
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video CallCapture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                } else {
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video Callbefore shouldMirror")
                    self.previewView?.shouldMirror = (captureDevice.position == .front)
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video Callafter shouldMirror")
                    
                }
            }
            logMessage(messageText: "ATDebug :: Video Advisor Video Callafter camera?.startCapture")
        }
        else {
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    
    func cleanupRemoteParticipant() {
        if self.remoteParticipant != nil {
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
            self.remoteParticipant = nil
        }
    }
    
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
         callKitCallController.request(transaction, completion: { (error) in
            if let error = error {
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
            DispatchQueue.main.async {
                let newConstraint =  self.heightConstraint?.constraintWithMultiplier(0.251)
                self.view.removeConstraint(self.heightConstraint ?? NSLayoutConstraint())
                self.view.addConstraint(newConstraint ?? NSLayoutConstraint())
                self.heightConstraint = newConstraint
                
                let newWidthConstraint = self.WidthConstraint?.constraintWithMultiplier(0.30)
                self.view.removeConstraint(self.WidthConstraint ?? NSLayoutConstraint())
                self.view.addConstraint(newWidthConstraint ?? NSLayoutConstraint())
                self.WidthConstraint = newWidthConstraint
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    let BottomPadding = window?.safeAreaInsets.bottom
                    let standardSpacing: CGFloat = 40.0
                    self.BottomConstraint.constant = (BottomPadding ?? 0) + standardSpacing
                    self.LeadingConstraint.constant = 20
                }
                
                /*
                 if #available(iOS 11.0, *) {
                     let window = UIApplication.shared.keyWindow
                     let topPadding = window?.safeAreaInsets.top
                     let standardSpacing: CGFloat = 40.0
                     self.topConstraint.constant = (topPadding ?? 0) + standardSpacing
                     self.trailingConstraint.constant = -20
                 }
                 */
                
                UIViewPropertyAnimator(duration: 1.0, curve: .easeIn) {
                    self.previewView?.layer.cornerRadius = 10
                    self.previewView?.layer.masksToBounds = true
                }.startAnimation()
                
                UIView.animate(withDuration: 1.0) {
                    self.viewContainerAccept?.isHidden = true
                    self.stackViewCall?.layoutIfNeeded()
                    self.view.layoutIfNeeded()
                }
            }
            
        })
        
    }
    
    func reportIncomingCall(uuid: UUID, roomName: String?, completion: ((NSError?) -> Void)? = nil) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        
        let callUpdate = CXCallUpdate()
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = false
        callUpdate.supportsHolding = false
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false
        
         callKitProvider?.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if error == nil {
                NSLog("Incoming call successfully reported.")
            } else {
                NSLog("Failed to report incoming call successfully: \(String(describing: error?.localizedDescription)).")
            }
            completion?(error as NSError?)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //    @objc func callAccepted(_ notification: Notification) {
    //        if let response = notification.object as? [String:String] {
    //
    //            strRoomName = JSON(response["room_id"] as Any).stringValue //  response["room_id"] ?? ""
    //            senderName = JSON(response["sender_name"] as Any).stringValue // response["sender_name"] ?? ""
    //            receiverId = JSON(response["receiver_id"] as Any).stringValue // response["receiver_id"] ?? ""
    //            senderId = JSON(response["sender_id"] as Any).stringValue // response["sender_id"] ?? ""
    //            let newConstraint = self.heightConstraint.constraintWithMultiplier(0.281)
    //            view.removeConstraint(self.heightConstraint)
    //            view.addConstraint(newConstraint)
    //            view.layoutIfNeeded()
    //            self.heightConstraint = newConstraint
    //
    //            self.connect(sender: UIButton())
    //
    //        }
    //    }
    
    @IBAction func btnactionDecline(_ sender: Any) {
        self.invalidateTimers()
        //MARK:- Firebase Analytics Log
//        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.DeclineButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
        self.webserviceforRejectCall(reject: 1)
    }
    
    @IBAction func btnActionAccept(_ sender: UIButton) {
        self.invalidateTimers()
        //MARK:- Firebase Analytics Log
//        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.AcceptButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
////        self.ProfileUserView.isHidden = false
//        UtilityClass.showCircleInAnimatingLoader()
        self.player?.stop()
        self.webServiceForAccept()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        //MARK:- Firebase Analytics Log
      //  UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.BackButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .BackButton, methodName: #function)
        self.goBack()
    }
    
    /**
      * It will refresh my appointments screen, and refresh the timer screen if open.
      * Dismisses the call controller screen.
     */
    @objc func goBack()
    {
        self.invalidateTimers()
        print(#function)
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        
        self.refreshMyAppointmentsVCWhenCallScreenIsOpen()
                           
        self.refreshTimerScreenWhenCallScreenIsOpen()
 
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        self.player?.stop()
        SingletonClass.sharedInstance.isCommingCall = false
         callKitProvider?.invalidate()
        //         self.audioDevice.isEnabled = true
//        SocketIOManager.shared.socket.off(SocketKeys.VideoCall.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedByReceiver.rawValue)
        
        self.localVideoTrack = nil
        self.localAudioTrack = nil
        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall = DefaultAudioDevice()
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
//        if(self.presentingViewController?.isKind(of: SplashVC.self) ?? false)
//        {
//            let vc = self.presentingViewController as? SplashVC
//            vc?.webserviceOfInit()
//        }

        ///When alert screen is open, then added code, to dismiss cal controller
        if let alertVC = UIApplication.topViewController() as? UIAlertController {
            print("Alert controller is open, so dimissing it first, then dismissing alert controller")
            alertVC.dismiss(animated: false, completion: {
                self.dismiss(animated: true, completion: {
            //                    Utilities.ShowAlert(OfMessage: "Customer disconnect the call")
                    
                })//navigationController?.popViewController(animated: true)
            })
        }else{
           self.dismiss(animated: true, completion: {
          //  Utilities.ShowAlert(OfMessage: "Customer disconnect the call")
           })//navigationController?.popViewController(animated: true)
        }
    
    }
    
    func logMessage(messageText: String) {
        print(messageText)
        messageLabel?.text = messageText
    }
    
    
    
    func holdCall(onHold: Bool) {
        self.localAudioTrack?.isEnabled = !onHold
        self.localVideoTrack?.isEnabled = !onHold
    }
    
    func performRoomConnect(uuid: UUID, roomName: String? , completionHandler: @escaping (Bool) -> Swift.Void) {
        // Configure access token either from server or manually.
        // If the default wasn't changed, try fetching from server.
        //        if (accessToken == accessToken) {
//        do {
        self.accessToken =  token ?? ""//try TokenUtils.fetchToken(url: tokenUrl)
//        } catch {
//            let message = "Failed to fetch access token"
//            logMessage(messageText: message)
//            return
//        }
        //        }
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = ConnectOptions(token: self.accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [LocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [LocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            
            // Use the preferred signaling region
            if let signalingRegion = Settings.shared.signalingRegion {
                builder.region = signalingRegion
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
            
            // The CallKit UUID to assoicate with this Room.
            builder.uuid = uuid
        }
        
        // Connect to the Room using the options we provided.
         room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        
        logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  CallAttempting to connect to room \(String(describing: roomName))")
        
        self.showRoomUI(inRoom: true)
        
         callKitCompletionHandler = completionHandler
    }
}

extension CallControllerViewController
{
    func webserviceforRejectCall(reject: Int?) {

        is_rejected = reject
        var param = [String: Any]()
        
        param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "before_receive"]
        
        self.makeAvailableToggle()

        if is_rejected != nil {

            if isPickedUp != nil {
                param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "after_receive"]
               
            } else {
                param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "before_receive"]
                
            }
        }

        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
        
        if let room =  room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Video Advisor Video CallAttempting to disconnect from room \(room.name)")
             userInitiatedDisconnect = true
            self.performEndCallAction(uuid: uuid)
        }
        self.goBack()
        
        if self.remoteParticipant?.delegate != nil {
        
        } else {

        }
  
    }
    
    public func refreshMyAppointmentsVCWhenCallScreenIsOpen() {
        print(#function)
       
        
    }
    
    public func refreshTimerScreenWhenCallScreenIsOpen() {
        print(#function)
       
    }
    
    
    func webServiceForAccept()
    {
        
     
        let param = ["user_id" : Int(senderID ) ?? 0 ,
                     "advisor_id": Int(reciverID ) ?? 0,
                     "advisor_name" : senderName,
                     "booking_id" : Int(bookingID ) ?? 0] as [String: Any]
        
        self.emitSocketPickupCall(param: param)
       // param = ["user_id":Int(senderId ?? "") ?? 0,"user_name":senderName ?? "","advisor_id":Int(receiverId ?? "0") ?? 0,"booking_id": Int(visitID ?? "0") ?? 0, "call_status" : "before_receive"]
        self.connect(sender: UIButton())
    }
    
    
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
         callKitCallController.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            
            NSLog("EndCallAction transaction request successful")
        }
    }
}

//MARK:- CXProviderDelegate


// MARK:- RoomDelegate
extension CallControllerViewController : RoomDelegate {
    func roomDidConnect(room: Room) {
        // At the moment, this example only supports rendering one Participant at a time.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallConnected to room \(room.name) as \(room.localParticipant?.identity ?? "")")
        isPickedUp = 1
        // This example only renders 1 RemoteVideoTrack at a time. Listen for all events to decide which track to render.
        for remoteParticipant in room.remoteParticipants {
            remoteParticipant.delegate = self
        }
        
        let cxObserver =  callKitCallController.callObserver
        let calls = cxObserver.calls
        
        // Let the call provider know that the outgoing call has connected
        if let uuid = room.uuid, let call = calls.first(where:{$0.uuid == uuid}) {
            if call.isOutgoing {
                 callKitProvider?.reportOutgoingCall(with: uuid, connectedAt: nil)
            }
        }
        
         callKitCompletionHandler!(true)
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallDisconnected from room \(room.name), error = \(String(describing: error))")
        
        if ! userInitiatedDisconnect, let uuid = room.uuid, let error = error {
            var reason = CXCallEndedReason.remoteEnded
            
            if (error as NSError).code != TwilioVideoSDK.Error.roomRoomCompletedError.rawValue {
                reason = .failed
            }
            
             callKitProvider?.reportCall(with: uuid, endedAt: nil, reason: reason)
            //            self.callKitProvider.reportCall(with: uuid, endedAt: nil, reason: reason)
        }
        else
        {
            if room.uuid != nil
            {
                //                self.callKitProvider?.reportCall(with: uuid, endedAt: nil, reason: .remoteEnded)
            }
        }
        
        self.cleanupRemoteParticipant()
         room = nil
        self.showRoomUI(inRoom: false)
         callKitCompletionHandler = nil
         userInitiatedDisconnect = false
        
        self.invalidateTimers()
        //self.webserviceforRejectCall(reject: 1)
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailed to connect to room with error: \(error.localizedDescription)")
        
         callKitCompletionHandler!(false)
         room = nil
        self.showRoomUI(inRoom: false)
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallReconnecting to room \(room.name), error = \(String(describing: error))")
    }
    
    func roomDidReconnect(room: Room) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallReconnected to room \(room.name)")
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        // Listen for events from all Participants to decide which RemoteVideoTrack to render.
        participant.delegate = self
        isPickedUp = 1
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallRoom \(room.name), Participant \(participant.identity) disconnected")
        SingletonClass.sharedInstance.isCommingCall = false
        self.goBack()
        // Nothing to do in this example. Subscription events are used to add/remove renderers.
    }
}



// MARK:- RemoteParticipantDelegate
extension CallControllerViewController : RemoteParticipantDelegate {
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has offered to share the video Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) published video track")
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has stopped sharing the video Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) unpublished video track")
    }
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        // Remote Participant has offered to share the audio Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) published audio track")
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) unpublished audio track")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // The LocalParticipant is subscribed to the RemoteParticipant's video Track. Frames will begin to arrive now.
        logMessage(messageText: "ATDebug :: Video Advisor Video CallSubscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == nil) {
            _ = renderRemoteParticipant(participant: participant)
        }
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallUnsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        
        if(self.boolIsFromSocket)
        {
            if self.remoteParticipant == participant {
                
                self.performEndCallAction(uuid:  room?.uuid ?? UUID())
                cleanupRemoteParticipant()
                
                // Find another Participant video to render, if possible.
                if var remainingParticipants =  room?.remoteParticipants,
                    let index = remainingParticipants.firstIndex(of: participant) {
                    remainingParticipants.remove(at: index)
                    renderRemoteParticipants(participants: remainingParticipants)
                }
            }
        }
        else
        {
            self.remoteParticipant = nil
            
        }
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallSubscribed to audio track for Participant \(participant.identity)")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallUnsubscribed from audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) enabled video track")
    }
    
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) disabled video track")
    }
    
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) enabled audio track")
    }
    
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) disabled audio track")
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
    
    
    func renderRemoteParticipant(participant : RemoteParticipant) -> Bool {
        
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callremote render video")
        // This example renders the first subscribed RemoteVideoTrack from the RemoteParticipant.
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack,
                publication.isTrackSubscribed {
                setupRemoteVideoView()
                subscribedVideoTrack.addRenderer(self.remoteView!)
                self.remoteParticipant = participant
                return true
            }
        }
        return false
    }
    
    func renderRemoteParticipants(participants : Array<RemoteParticipant>) {
        for participant in participants {
            // Find the first renderable track.
            if participant.remoteVideoTracks.count > 0,
                renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }
    
    func setupRemoteVideoView() {
        
        
        // Creating `VideoView` programmatically
        self.remoteView = VideoView(frame: CGRect.zero, delegate: self)
        
        self.view.insertSubview(self.remoteView!, at: 0)
        // `VideoView` supports scaleToFill, scaleAspectFill and scaleAspectFit
        // scaleAspectFit is the default mode when you create `VideoView` programmatically.
        self.remoteView!.contentMode = .scaleAspectFill;
        
        let centerX = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerX)
        let centerY = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerY)
        let width = NSLayoutConstraint(item: self.remoteView!,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                       toItem: self.view,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       multiplier: 1,
                                       constant: 0);
        self.view.addConstraint(width)
        let height = NSLayoutConstraint(item: self.remoteView!,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        relatedBy: NSLayoutConstraint.Relation.equal,
                                        toItem: self.view,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        multiplier: 1,
                                        constant: 0);
        self.view.addConstraint(height)
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callstarted other video")
        self.perform(#selector(self.setupForLoader), with: nil, afterDelay: 3.0)
        
    }
    
    @objc func setupForLoader(){
        
     //   UtilityClass.hideCircleInAnimatingLoader()
//        self.ProfileUserView.isHidden = true
    }
    
}


// MARK:- VideoViewDelegate
extension CallControllerViewController : VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callstarted other video")
        self.view.setNeedsLayout()
    }
}

// MARK:- CameraSourceDelegate
extension CallControllerViewController : CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallCamera source failed with error: \(error.localizedDescription)")
    }
}

// MARK:- UITextFieldDelegate
extension CallControllerViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.connect(sender: textField)
        return true
    }
}


extension CallControllerViewController : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Will present notification \(notification)")
        
        
        
        self.reportIncomingCall(uuid: UUID(), roomName: CallControllerViewController.parseNotification(notification: notification)) { _ in
            // Always call the completion handler when done.
            completionHandler(UNNotificationPresentationOptions())
        }
    }
    
    static func parseNotification(notification: UNNotification) -> String {
        var roomName = ""
        if let requestedName = notification.request.content.userInfo["roomName"] as? String {
            roomName = requestedName
        }
        return roomName
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Received notification response in \(UIApplication.shared.applicationState.rawValue) \(response)")
        let roomName = CallControllerViewController.parseNotification(notification: response.notification)
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            self.performStartCallAction(uuid: UUID(), roomName: roomName)
            completionHandler()
            break
        case "INVITE_ACTION":
            self.reportIncomingCall(uuid: UUID(), roomName: roomName) { _ in
                // Always call the completion handler when done.
                completionHandler()
            }
            break
        case "DECLINE_ACTION":
            completionHandler()
            break
        case UNNotificationDismissActionIdentifier:
            completionHandler()
            break
        // Handle other actions…
        default:
            break
        }
    }
}

//enum SocketAPiKeys: String {
//
//    case ConnectClient                            = "connect_client"
//    case CheckTyping                              = "check_typing"
//    case SendMessage                              = "send_message"
//    case ReceiverMessage                          = "receiver_message"
//    case AllMessageRead                           = "all_message_read"
//    case VideoCall                                = "video_call"
//    case VideoCallRejectedByReceiver              = "call_reject_by_advisor"
//    case VideoCallRejectedBySender                = "call_reject_by_user"
//    case PickupCall                               = "pick_up_call"
//    //
//    ///Doctor - Nurse Meeting call
//    case ConCall = "con_call"
//    case MeetingCode = "meeting_code"
//    case AddJoinee = "add_joinee" // Join as a Doctor/Nurse
//    case AddJoinee2 = "add_joinee2" // Join as a family member
//    case ConferenceCall = "conference_call"
//    case EndCallLog = "end_call_log"
//
//
//}

//MARK: - extra

extension Notification.Name {
//    static let internetConnected = NSNotification.Name("internetConnected")
//    static let internetDisconnected = NSNotification.Name("internetDisconnected")
    static let networkStatusChanged = NSNotification.Name("connectivityStatusChanged")

//    static let reloadMainCategories = NSNotification.Name("reloadMainCategories")
    static let openCovidFlow = NSNotification.Name("openCovidFlow")
    static let endCall = NSNotification.Name("endCall")
    static let refreshCallHistory = NSNotification.Name("refreshCallHistory")

    static let logOutPushHandle = NSNotification.Name("logOutPushHandle")
    static let openMyAppointmentsPageFromPush = NSNotification.Name("openMyAppointmentsPageFromPush")
    static let listNotificationPushHandle = NSNotification.Name("listNotificationPushHandle")
//    static let openContactCovidFromPush = NSNotification.Name("openContactCovidFromPush")
//    static let openSymptomsCovidFromPush = NSNotification.Name("openSymptomsCovidFromPush")
    static let openCallHistoryFromPush = NSNotification.Name("openCallHistoryFromPush")



//    static let homeVCRefrehData = NSNotification.Name("homeVCRefrehData")


//    static let closeParkingSlotSocket = NSNotification.Name("CloseParkingSlotSocket")

}
class Settings: NSObject {

    // ISDK-2644: Resolving a conflict with AudioToolbox in iOS 13
    let supportedAudioCodecs: [TwilioVideo.AudioCodec] = [IsacCodec(),
                                                          OpusCodec(),
                                                          PcmaCodec(),
                                                          PcmuCodec(),
                                                          G722Codec()]
    
    let supportedVideoCodecs: [VideoCodec] = [Vp8Codec(),
                                              Vp8Codec(simulcast: true),
                                              H264Codec(),
                                              Vp9Codec()]

    // Valid signaling Regions are listed here:
    // https://www.twilio.com/docs/video/ip-address-whitelisting#signaling-communication
    let supportedSignalingRegions: [String] = ["gll",
                                               "au1",
                                               "br1",
                                               "de1",
                                               "ie1",
                                               "in1",
                                               "jp1",
                                               "sg1",
                                               "us1",
                                               "us2"]


    let supportedSignalingRegionDisplayString: [String : String] = ["gll": "Global Low Latency",
                                                                    "au1": "Australia",
                                                                    "br1": "Brazil",
                                                                    "de1": "Germany",
                                                                    "ie1": "Ireland",
                                                                    "in1": "India",
                                                                    "jp1": "Japan",
                                                                    "sg1": "Singapore",
                                                                    "us1": "US East Coast (Virginia)",
                                                                    "us2": "US West Coast (Oregon)"]
    
    var audioCodec: TwilioVideo.AudioCodec?
    var videoCodec: VideoCodec?

    var maxAudioBitrate = UInt()
    var maxVideoBitrate = UInt()

    var signalingRegion: String?

    func getEncodingParameters() -> EncodingParameters?  {
        if maxAudioBitrate == 0 && maxVideoBitrate == 0 {
            return nil;
        } else {
            return EncodingParameters(audioBitrate: maxAudioBitrate,
                                      videoBitrate: maxVideoBitrate)
        }
    }
    
    private override init() {
        // Can't initialize a singleton
    }
    
    // MARK:- Shared Instance
    static let shared = Settings()
}
struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}
extension CallControllerViewController
{
    
    func emitSocketReject(param: [String : Any]) {
        print(#function)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
    }
    
    
    func onSocketCallRejectBySender() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCallRejectedBySender.rawValue) { (json) in
            print(#function, "\n ", json)
            ///If call ended, then need to refresh status of call controller
            self.boolIsFromSocket = true
            
            self.goBack()
            
            self.makeAvailableToggle()
            
        }
    }
    
    func emitSocketPickupCall(param: [String : Any]) {
        print(#function)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.PickupCall.rawValue, with: param)
        
//        let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
//
//        controller.TotalTimecounter = 0
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//            controller.startTimer(withInterval: 1.0)
//        })
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StartTimer"), object: nil, userInfo: nil)
        
    }
    func makeAvailableToggle() {
        if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
            SingletonClass.sharedInstance.StartTimerForAdvisor()
            var ToggleModel : [AvailabilityToggleModel] = []
            
            if SingletonClass.sharedInstance.adviserAudioChatOn == true {
                ToggleModel.append(AvailabilityToggleModel(type: "audio", status: "1"))
            } else {
                ToggleModel.append(AvailabilityToggleModel(type: "audio", status: "0"))
            }
            
            if SingletonClass.sharedInstance.adviserVideoChatOn == true {
                ToggleModel.append(AvailabilityToggleModel(type: "video", status: "1"))
            } else {
                ToggleModel.append(AvailabilityToggleModel(type: "video", status: "0"))
            }
            
            if SingletonClass.sharedInstance.adviserTextChatOn == true {
                ToggleModel.append(AvailabilityToggleModel(type: "chat", status: "1"))
            } else {
                ToggleModel.append(AvailabilityToggleModel(type: "chat", status: "0"))
            }
            
            let productsDict = AvailabilityToggleModel.convertDataCartArrayToProductsDictionary(arrayDataCart: ToggleModel);

            let jsonData = try! JSONSerialization.data(withJSONObject: productsDict, options: [])
            let jsonString:String = String(data: jsonData, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) ?? ""
            
            
            let reqModel = changeAvailibalityStatusReqModelWithAllToggle()
            reqModel.user_id = SingletonClass.sharedInstance.UserId
            reqModel.status = jsonString
            
            webServiceCallForToggleWithAllToggle(Model: reqModel)
        }
    }
}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}
extension CallControllerViewController {
    func webServiceCallForToggleWithAllToggle(Model:changeAvailibalityStatusReqModelWithAllToggle) {
        
        WebServiceSubClass.webServiceCallForToggleWithAllToggle(CategoryModel: Model, completion: { (response, status, error) in
            
            if status{
               
                
            }else{
                Utilities.showAlertOfAPIResponse(param: error, vc: self)
            }
        })
    }
}
extension CallControllerViewController {

    func invalidateTimers() {
//        if let soundId = CallControllerViewController.soundId {
//            AudioServicesDisposeSystemSoundID(soundId)
//            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
//            playCount += 1
//        }
        let soundId: SystemSoundID = getSoundID()
        AudioServicesDisposeSystemSoundID(soundId)
        AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
        
        if let vibrationTimer = vibrationTimer {
            vibrationTimer.invalidate()
        }
        if let soundTimer = soundTimer {
            soundTimer.invalidate()
        }
    }
//    func playSoundAndVibrateInLoop() {
//        invalidateTimers()
////        if let soundId = CallControllerViewController.soundId {
////            AudioServicesPlaySystemSound(soundId)
////           // AudioServicesPlaySystemSound()
////            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
////            playCount += 1
////        }
//        let soundId: SystemSoundID = getSoundID()
//        AudioServicesPlaySystemSound(soundId)
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//         soundTimer = Timer.scheduledTimer(timeInterval: CallControllerViewController.timerInterval, target: self, selector: #selector(playSound(_:)), userInfo: nil, repeats: true)
//    }

    @IBAction func playSound(_ sender: Any) {
        let soundId: SystemSoundID = getSoundID()
        AudioServicesPlaySystemSound(soundId)
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//        if let soundId = CallControllerViewController.soundId {
//            AudioServicesPlaySystemSound(soundId)
//            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//            playCount += 1
//        }
    }
   

    
    @IBAction func cancelAll(_ sender: Any) {
        invalidateTimers()
    }

}
*/

//
//  CallControllerViewController.swift
//  VirtuWoof Vet
//
//  Created by EWW071 on 21/04/20.
//  Copyright © 2020 EWW077. All rights reserved.
//

import UIKit
import TwilioVideo
import CallKit
import AVFoundation
import SwiftyJSON
//MARK: Start

class CallControllerViewController: BaseViewController {
    let notificationCenter = UNUserNotificationCenter.current()

    // MARK:- IBOutlets
    var soundTimer: Timer?
    var vibrationTimer: Timer?
    var playCount = 0
    static let soundId: SystemSoundID? = {
        var soundID: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &soundID)
            return soundID
        }
        return soundID
    }()
    //AudioServicesPlaySystemSound(_engineSound);

    static let timerInterval: TimeInterval = 5
//
    
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var lblTimer: UILabel?
    @IBOutlet weak var previewView: VideoView?
    @IBOutlet weak var disconnectButton: UIButton?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblPhoneNo: UILabel?
    @IBOutlet weak var viewDecline: UIView?
    @IBOutlet weak var viewAccept: UIView?
    @IBOutlet weak var viewContainerAccept: UIView?
    @IBOutlet weak var stackViewCall: UIStackView?
    @IBOutlet weak var btnFlipCamera: UIButton?
    var token: String?
    var player: AVAudioPlayer?

    var room: Room?
    var callKitProvider: CXProvider? = nil
    var callKitCallController: CXCallController? = nil
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil
    var userInitiatedDisconnect: Bool = false
    
    
    
    @IBOutlet weak var lblDecline: UILabel?
    @IBOutlet weak var lblAccept: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint?
    @IBOutlet weak var WidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var ProfileUserView: UIView!
    
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnVideoOff: UIButton!
    @IBOutlet weak var btnPreivew: UIButton!
    
    // MARK:- Video call related custom variables
    
    var vetID : String?
    
    var deviceToken : String?
    var is_rejected: Int?
    var isPickedUp: Int?
    //    var senderId : String?
    var roomName : String?
    //    var senderName : String?
    var CallingTo : String?

    var message : String?
    var visit_id : String?

    var senderID = ""
    var reciverID = ""
    var bookingID = ""

    var senderName = ""

//    var token : String?
        var boolIsFromSocket = false

    // MARK:- View Controller Members

    // Configure access token manually for testing, if desired! Create one manually in the console
    // at https://www.twilio.com/console/video/runtime/testing-tools
    var accessToken = ""

    // Video SDK components
    
    /**
     * We will create an audio device and manage it's lifecycle in response to CallKit events.
     */
   // var audioDevice: DefaultAudioDevice?
    var camera: CameraSource?
    var audioDevice: DefaultAudioDevice = DefaultAudioDevice()
    var localVideoTrack: LocalVideoTrack?
    var localAudioTrack: LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var remoteView: VideoView?

    
    
    @IBOutlet weak var imgvDeclinePhone: UIImageView!
    
    
    required init?(coder aDecoder: NSCoder) {
        let configuration = CXProviderConfiguration(localizedName: "TempleBliss")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.supportsVideo = true
        configuration.supportedHandleTypes = [.generic]
        if let callKitIcon = UIImage(named: "iconMask80") {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }

        callKitProvider = CXProvider(configuration: configuration)
        callKitCallController = CXCallController()

        super.init(coder: aDecoder)

        callKitProvider?.setDelegate(self, queue: nil)
//        let update = CXCallUpdate()
//                update.remoteHandle = CXHandle(type: .generic, value: roomName ?? "")
//        callKitProvider!.reportNewIncomingCall(with: UUID(), update: update, completion: { error in })
    }
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        callKitProvider?.invalidate()
        
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
    }
        
    
    //MARK:- View Controller Life Cycle
    func getSoundID() -> SystemSoundID {
        var soundID: SystemSoundID = 0
        if let soundURL = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &soundID)
            return soundID
        }
        return soundID
    }
    override func viewDidLoad() {
        super.viewDidLoad()
     
       // playSoundAndVibrateInLoop()
        // Do any additional setup after loading the view.
        
        //        self.title = "QuickStart"
        //1.
        
       
        
        self.lblName?.text = self.CallingTo
        self.navigationController?.navigationBar.isHidden = true
        
        //2.
        /*
         * The important thing to remember when providing a AudioDevice is that the device must be set
         * before performing any other actions with the SDK (such as creating Tracks, or connecting to a Room).
         * In this case we've already initialized our own `DefaultAudioDevice` instance which we will now set.
         */
        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall = DefaultAudioDevice()

        TwilioVideoSDK.audioDevice = AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall ?? DefaultAudioDevice()
        
        //3.
        NotificationCenter.default.removeObserver(self, name: .endCall, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.goBack), name: .endCall, object: nil)

        //4.
        if PlatformUtils.isSimulator {
            self.previewView?.removeFromSuperview()
        } else {
            // Preview our local camera track in the local video preview view.
            self.startPreview()
        }
        
        //5.
        // Disconnect and mic button will be displayed when the Client is connected to a Room.
        self.disconnectButton?.isHidden = true
       // playSound()
        
//        self.onSocketCallRejectBySender()
        
        /// For call button to come white, not blue
        self.imgvDeclinePhone.tintColor = nil
        self.imgvDeclinePhone.tintColor = .white
        
        self.setFontsOfLabels()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            
//            self.invalidateTimers()
            //MARK:- Firebase Analytics Log
    //        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.AcceptButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
    ////        self.ProfileUserView.isHidden = false
    //        UtilityClass.showCircleInAnimatingLoader()
//            self.player?.stop()
//            self.webServiceForAccept()
            
        })
        self.registerForLocalNotifications()
        // Sandeep Suthar (ToDo Check)
        self.btnActionAccept(nil)
    }
    @IBAction func btnMuteClick(_ sender: UIButton) {
        
        
        if  self.btnMute.isSelected {
            print("Mic is enable now")
            
            if let localAudioTrack = self.localAudioTrack, let localParticipant =  room?.localParticipant  {

                localParticipant.publishAudioTrack(localAudioTrack)
           
                self.btnMute.isSelected = false
            }

        }else {
            print("Mic is mute now")

            if let localAudioTrack = self.localAudioTrack, let localParticipant =  room?.localParticipant  {

                localParticipant.unpublishAudioTrack(localAudioTrack)

                self.btnMute.isSelected = true
            }
        }
        
        
//
//
//
//
//        if let room = room, let uuid = room.uuid, let localAudioTrack = self.localAudioTrack {
//            let isMuted = localAudioTrack.isEnabled
//            let muteAction = CXSetMutedCallAction(call: uuid, muted: isMuted)
//            let transaction = CXTransaction(action: muteAction)
//
//            self.callKitCallController.request(transaction)  { error in
//                DispatchQueue.main.async {
//                    if let error = error {
//                        self.logMessage(messageText: "ATDebug :: Video Advisor Video CallSetMutedCallAction transaction request failed: \(error.localizedDescription)")
//                        return
//                    }
//                    self.logMessage(messageText: "ATDebug :: Video Advisor Video CallSetMutedCallAction transaction request successful")
//                }
//            }
//        }
    }
    func muteAudio(isMuted: Bool) {
        if let localAudioTrack = self.localAudioTrack {
            localAudioTrack.isEnabled = !isMuted

            // Update the button title
            if (!isMuted) {
                self.btnMute.setImage(#imageLiteral(resourceName: "mic_unmuted"), for: .normal)
            } else {
                self.btnMute.setImage(#imageLiteral(resourceName: "mic_muted"), for: .normal)
            }
        }
    }
    @IBAction func btnVideoOffClick(_ sender: UIButton) {
    }
    
    
    @IBAction func btnFlipCamera(_ sender: Any) {
        self.flipCamera()
    }
 
     @objc func flipCamera() {
        var newDevice: AVCaptureDevice?

        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }

            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "ATDebug :: Video Advisor Video CallError selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView?.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
            
            UIView.transition(with: self.previewView ?? UIView(), duration: 1.0, options: [.transitionFlipFromLeft], animations: {
            })
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
//         callKitCallController = nil
//        self.callKitProvider?.invalidate()
//        if let provider =  callKitProvider {
//            provider.invalidate()
//        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopTimer"), object: nil, userInfo: nil)
        notificationCenter.delegate = nil
        if let soundTimer = soundTimer {
            soundTimer.invalidate()
        }
        if let vibrationTimer = vibrationTimer {
            vibrationTimer.invalidate()
        }
        appDel.VideoCallScreenOpen = false
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
       
        
       // room?.disconnect()
        if let room =  room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Video Advisor Video CallAttempting to disconnect from room \(room.name)")
             userInitiatedDisconnect = true
            print("Ravi debug, view will dissappe")
            self.performEndCallAction(uuid: uuid)
        }
        self.goBack()
        AudioServicesDisposeSystemSoundID(getSoundID())
        self.navigationController?.navigationBar.isHidden = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setLocalisation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setLocalisation() {
        self.lblPhoneNo?.text = "Incoming Video Call"
        self.lblDecline?.text = "Decline"
        self.lblAccept.text = "Accept"
    
    }
    
    func setFontsOfLabels() {
        self.lblName?.font = UIFont.boldSystemFont(ofSize: 30.0)
        self.lblPhoneNo?.font = UIFont.systemFont(ofSize: 20.0)
        self.lblAccept.font = UIFont.systemFont(ofSize: 15.0)
        self.lblDecline?.font = UIFont.systemFont(ofSize: 15.0)
    }
    func askPermissionIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                
            })
        case .denied:
            let alert = UIAlertController(title: "Error", message: "Please allow microphone usage from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .granted:
            break
       
    }
    }
    func playSound() {

        guard let url = Bundle.main.url(forResource: "\(RingtoneSoundName)", withExtension: "mp3") else { return }

        do {
            try AVAudioSession.sharedInstance().setCategory(.ambient, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)

            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)

            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */

            guard let player = player else { return }
            player.numberOfLoops = -1

            player.play()

        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewDecline?.layer.cornerRadius = (viewDecline?.frame.size.width ?? 0) / 2
        self.viewDecline?.layer.masksToBounds = true
        
        self.viewAccept?.layer.cornerRadius = (viewAccept?.frame.size.width ?? 0) / 2
        self.viewAccept?.layer.masksToBounds = true
    }
    
    
    // MARK:- IBActions
    @IBAction func connect(sender: AnyObject) {
       // self.performStartCallAction(uuid: UUID(), roomName:  strRoomName)
    }
    
    func registerForLocalNotifications() {
        // Define the custom actions.
        let inviteAction = UNNotificationAction(identifier: "INVITE_ACTION",
                                                title: "Simulate VoIP Push",
                                                options: UNNotificationActionOptions(rawValue: 0))
        let declineAction = UNNotificationAction(identifier: "DECLINE_ACTION",
                                                 title: "Decline",
                                                 options: .destructive)
        
        // Define the notification type
        let meetingInviteCategory = UNNotificationCategory(identifier: "ROOM_INVITATION",
                                                           actions: [inviteAction, declineAction],
                                                           intentIdentifiers: [],
                                                           options: .customDismissAction)
        notificationCenter.setNotificationCategories([meetingInviteCategory])
        
        // Register for notification callbacks.
        notificationCenter.delegate = self
        
        // Request permission to display alerts and play sounds.
        notificationCenter.requestAuthorization(options: [.alert])
        { (granted, error) in
            // Enable or disable features based on authorization.
        }
    }
    
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        self.viewContainerAccept?.isHidden = inRoom
        self.lblPhoneNo?.isHidden = inRoom
        self.lblTimer?.isHidden = !inRoom
        self.lblDecline?.isHidden = inRoom
        UIApplication.shared.isIdleTimerDisabled = inRoom
        self.setNeedsUpdateOfHomeIndicatorAutoHidden()
    }
    
    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.
        
        // Create an audio track.
        if (self.localAudioTrack == nil) {
            self.localAudioTrack = LocalAudioTrack()
            
            if (self.localAudioTrack == nil) {
                logMessage(messageText: "ATDebug :: Video Advisor Video CallFailed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (self.localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // MARK:- Private
    /**
     * Set up camera.
     * Add camera to local video track: represents local video produced by a `TVIVideoSource`.
     * Add preview view to local video track.
     * Tap gesture for front or back preview.
     * Start capturing for video by camera.
     *
     */
    func startPreview() {
        if PlatformUtils.isSimulator {
            return
        }
        
        let frontCamera = CameraSource.captureDevice(position: .front)
        let backCamera = CameraSource.captureDevice(position: .back)
        
        if (frontCamera != nil || backCamera != nil) {
            // Preview our local camera track in the local video preview view.
            self.camera = CameraSource(delegate: self)
            self.localVideoTrack = LocalVideoTrack(source: camera!, enabled: true, name: "Camera")
            
            // Add renderer to video track for local preview
            self.localVideoTrack!.addRenderer(self.previewView ?? VideoView())
            logMessage(messageText: "ATDebug :: Video Advisor Video CallVideo track created")
            
            if (frontCamera != nil && backCamera != nil) {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(CallControllerViewController.flipCamera))
                self.previewView?.addGestureRecognizer(tap)
            }
//            let format = VideoFormat()
//            format.dimensions = CMVideoDimensions(width:1280, height: 720)
//            format.frameRate = 30

            logMessage(messageText: "ATDebug :: Video Advisor Video Callcamera?.startCapture")
            self.camera?.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
             
                print("The error is \(error?.localizedDescription ?? "-")")
                print("The capture device is \(captureDevice.localizedName)")
                if let error = error {
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video CallCapture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                } else {
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video Callbefore shouldMirror")
                    self.previewView?.shouldMirror = (captureDevice.position == .front)
                    self.logMessage(messageText: "ATDebug :: Video Advisor Video Callafter shouldMirror")
                    
                }
            }
            logMessage(messageText: "ATDebug :: Video Advisor Video Callafter camera?.startCapture")
        }
        else {
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    
    func cleanupRemoteParticipant() {
        if self.remoteParticipant != nil {
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
            self.remoteParticipant = nil
        }
    }
    
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
//        sleep(2)
        
        
         callKitCallController?.request(transaction, completion: { (error) in
            if let error = error {
                print("1Error")
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
            print("1Started")
        })
        
    }
    
    func reportIncomingCall(uuid: UUID, roomName: String?, completion: ((NSError?) -> Void)? = nil) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        
        let callUpdate = CXCallUpdate()
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = false
        callUpdate.supportsHolding = false
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false
        
        callKitProvider?.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if error == nil {
                NSLog("Incoming call successfully reported.")
            } else {
                NSLog("Failed to report incoming call successfully: \(String(describing: error?.localizedDescription)).")
            }
            completion?(error as NSError?)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    //    @objc func callAccepted(_ notification: Notification) {
    //        if let response = notification.object as? [String:String] {
    //
    //            strRoomName = JSON(response["room_id"] as Any).stringValue //  response["room_id"] ?? ""
    //            senderName = JSON(response["sender_name"] as Any).stringValue // response["sender_name"] ?? ""
    //            receiverId = JSON(response["receiver_id"] as Any).stringValue // response["receiver_id"] ?? ""
    //            senderId = JSON(response["sender_id"] as Any).stringValue // response["sender_id"] ?? ""
    //            let newConstraint = self.heightConstraint.constraintWithMultiplier(0.281)
    //            view.removeConstraint(self.heightConstraint)
    //            view.addConstraint(newConstraint)
    //            view.layoutIfNeeded()
    //            self.heightConstraint = newConstraint
    //
    //            self.connect(sender: UIButton())
    //
    //        }
    //    }
    
    @IBAction func btnactionDecline(_ sender: Any) {
        self.invalidateTimers()
        //MARK:- Firebase Analytics Log
//        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.DeclineButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
        self.webserviceforRejectCall(reject: 1)
    }
    
    @IBAction func btnActionAccept(_ sender: UIButton?) {
        self.invalidateTimers()
        //MARK:- Firebase Analytics Log
//        UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.AcceptButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .DeclineButton, methodName: #function)
////        self.ProfileUserView.isHidden = false
//        UtilityClass.showCircleInAnimatingLoader()
//        self.player?.stop()
        self.webServiceForAccept()
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        //MARK:- Firebase Analytics Log
      //  UtilityClass.addFirebaseLogEvent(eventName: VideoCallScreen.BackButtonPressedInVideoCall.rawValue, screenName: self.getClassName(), buttonName: .BackButton, methodName: #function)
        self.goBack()
    }
    
    /**
      * It will refresh my appointments screen, and refresh the timer screen if open.
      * Dismisses the call controller screen.
     */
    @objc func goBack()
    {
        self.invalidateTimers()
        print(#function)
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        
        self.refreshMyAppointmentsVCWhenCallScreenIsOpen()
                           
        self.refreshTimerScreenWhenCallScreenIsOpen()
 
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        self.player?.stop()
        SingletonClass.sharedInstance.isCommingCall = false
        callKitProvider?.invalidate()
        appDel.callKitProvider?.invalidate()
        //         self.audioDevice.isEnabled = true
//        SocketIOManager.shared.socket.off(SocketKeys.VideoCall.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedByReceiver.rawValue)
        
        self.localVideoTrack = nil
        self.localAudioTrack = nil
        AdvisorVideoCall.sharedInstance.audioDeviceForVideoCall = DefaultAudioDevice()
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
//        if(self.presentingViewController?.isKind(of: SplashVC.self) ?? false)
//        {
//            let vc = self.presentingViewController as? SplashVC
//            vc?.webserviceOfInit()
//        }

        ///When alert screen is open, then added code, to dismiss cal controller
        if let alertVC = UIApplication.topViewController() as? UIAlertController {
            print("Alert controller is open, so dimissing it first, then dismissing alert controller")
            alertVC.dismiss(animated: false, completion: {
                self.dismiss(animated: true, completion: {
            //                    Utilities.ShowAlert(OfMessage: "Customer disconnect the call")
                    
                })//navigationController?.popViewController(animated: true)
            })
        }else{
           self.dismiss(animated: true, completion: {
          //  Utilities.ShowAlert(OfMessage: "Customer disconnect the call")
           })//navigationController?.popViewController(animated: true)
        }
    
    }
    
    
}

extension CallControllerViewController {
    func webserviceforRejectCall(reject: Int?) {

        is_rejected = reject
        var param = [String: Any]()
        
        param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "before_receive"]
        
     

        if is_rejected != nil {

            if isPickedUp != nil {
                param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "after_receive"]
               print("@check1 not nil \(param)")
            } else {
                param = ["user_id":Int(senderID) ?? 0,"user_name":SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "","advisor_id":Int(reciverID) ?? 0,"booking_id": Int(bookingID) ?? 0, "call_status" : "before_receive"]
                print("@check1 nil \(param)")
            }
        }
        print("@check1 call Socket \(param)")
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
        
        if let room = room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Video Advisor Video CallAttempting to disconnect from room \(room.name)")
            userInitiatedDisconnect = true
            print("orifinal uuid Rejected by :  \(uuid)")
            print("ravi debug123 webserviceforRejectCall")
            self.performEndCallAction(uuid: uuid)
        } else {
            print("Sandeep Video call else case")
        }
        self.goBack()
        
        if self.remoteParticipant?.delegate != nil {
        
        } else {

        }
  
    }
    
    public func refreshMyAppointmentsVCWhenCallScreenIsOpen() {
        print(#function)
       
    }
    
    public func refreshTimerScreenWhenCallScreenIsOpen() {
        print(#function)
       
    }
    
    
    func webServiceForAccept()
    {
        
     
        let param = ["user_id" : Int(senderID ) ?? 0 ,
                     "advisor_id": Int(reciverID ) ?? 0,
                     "advisor_name" : senderName,
                     "booking_id" : Int(bookingID ) ?? 0] as [String: Any]
        
        self.emitSocketPickupCall(param: param)
       // param = ["user_id":Int(senderId ?? "") ?? 0,"user_name":senderName ?? "","advisor_id":Int(receiverId ?? "0") ?? 0,"booking_id": Int(visitID ?? "0") ?? 0, "call_status" : "before_receive"]
//        self.connect(sender: UIButton())
        self.performStartCallAction(uuid: UUID(), roomName: appDel.strRoomName)
    }
    
    
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        callKitCallController?.request(transaction) { error in
            if let error = error {
                
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            NSLog("Ravi debug123,Call controller")
            NSLog("EndCallAction transaction request successful")
        }
    }
    func holdCall(onHold: Bool) {
        self.localAudioTrack?.isEnabled = !onHold
        self.localVideoTrack?.isEnabled = !onHold
    }
    
    func performRoomConnect(uuid: UUID, roomName: String? , completionHandler: @escaping (Bool) -> Swift.Void) {
        print("performRoomConnect")
        // Configure access token either from server or manually.
        // If the default wasn't changed, try fetching from server.
        //        if (accessToken == accessToken) {
//        do {
        self.accessToken = token ?? ""//try TokenUtils.fetchToken(url: tokenUrl)
//        } catch {
//            let message = "Failed to fetch access token"
//            logMessage(messageText: message)
//            return
//        }
        //        }
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = ConnectOptions(token: self.accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [LocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [LocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            
            // Use the preferred signaling region
            if let signalingRegion = Settings.shared.signalingRegion {
                builder.region = signalingRegion
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
            
            // The CallKit UUID to assoicate with this Room.
            builder.uuid = uuid
        }
        
        // Connect to the Room using the options we provided.
        room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        
        logMessage(messageText: "ATDebug :: Video Advisor Video form CXProviderDelegate  CallAttempting to connect to room \(String(describing: roomName))")
        
        self.showRoomUI(inRoom: true)
        print("video timer start")
        
        callKitCompletionHandler = completionHandler
    }
    func logMessage(messageText: String) {
        print(messageText)
        messageLabel?.text = messageText
    }
}

//MARK:- CXProviderDelegate


// MARK:- RoomDelegate
extension CallControllerViewController : RoomDelegate {
    func roomDidConnect(room: Room) {
        // At the moment, this example only supports rendering one Participant at a time.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallConnected to room \(room.name) as \(room.localParticipant?.identity ?? "")")
        isPickedUp = 1
        // This example only renders 1 RemoteVideoTrack at a time. Listen for all events to decide which track to render.
        for remoteParticipant in room.remoteParticipants {
            remoteParticipant.delegate = self
        }
        
        let cxObserver = callKitCallController?.callObserver
        let calls = cxObserver?.calls
        
        // Let the call provider know that the outgoing call has connected
        if let uuid = room.uuid, let call = calls?.first(where:{$0.uuid == uuid}) {
            if call.isOutgoing {
                 callKitProvider?.reportOutgoingCall(with: uuid, connectedAt: nil)
            }
        }
        
         callKitCompletionHandler!(true)
        DispatchQueue.main.async {
            let newConstraint =  self.heightConstraint?.constraintWithMultiplier(0.251)
            self.view.removeConstraint(self.heightConstraint ?? NSLayoutConstraint())
            self.view.addConstraint(newConstraint ?? NSLayoutConstraint())
            self.heightConstraint = newConstraint
            
            let newWidthConstraint = self.WidthConstraint?.constraintWithMultiplier(0.30)
            self.view.removeConstraint(self.WidthConstraint ?? NSLayoutConstraint())
            self.view.addConstraint(newWidthConstraint ?? NSLayoutConstraint())
            self.WidthConstraint = newWidthConstraint
            if #available(iOS 11.0, *) {
                let window = UIApplication.shared.keyWindow
                let BottomPadding = window?.safeAreaInsets.bottom
                let standardSpacing: CGFloat = 40.0
                self.BottomConstraint.constant = (BottomPadding ?? 0) + standardSpacing
                self.LeadingConstraint.constant = 20
            }
            
            /*
             if #available(iOS 11.0, *) {
                 let window = UIApplication.shared.keyWindow
                 let topPadding = window?.safeAreaInsets.top
                 let standardSpacing: CGFloat = 40.0
                 self.topConstraint.constant = (topPadding ?? 0) + standardSpacing
                 self.trailingConstraint.constant = -20
             }
             */
            
            UIViewPropertyAnimator(duration: 1.0, curve: .easeIn) {
                self.previewView?.layer.cornerRadius = 10
                self.previewView?.layer.masksToBounds = true
            }.startAnimation()
            
            UIView.animate(withDuration: 1.0) {
                self.viewContainerAccept?.isHidden = true
                self.stackViewCall?.layoutIfNeeded()
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallDisconnected from room \(room.name), error = \(String(describing: error))")
        
        if !appDel.userInitiatedDisconnect, let uuid = room.uuid, let error = error {
            var reason = CXCallEndedReason.remoteEnded
            
            if (error as NSError).code != TwilioVideoSDK.Error.roomRoomCompletedError.rawValue {
                reason = .failed
            }
            
             callKitProvider?.reportCall(with: uuid, endedAt: nil, reason: reason)
            //            self.callKitProvider.reportCall(with: uuid, endedAt: nil, reason: reason)
        }
        else
        {
            if room.uuid != nil
            {
                //                self.callKitProvider?.reportCall(with: uuid, endedAt: nil, reason: .remoteEnded)
            }
        }
        
        self.cleanupRemoteParticipant()
        self.room = nil
        self.showRoomUI(inRoom: false)
         callKitCompletionHandler = nil
         userInitiatedDisconnect = false
        
        self.invalidateTimers()
        //self.webserviceforRejectCall(reject: 1)
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailed to connect to room with error: \(error.localizedDescription)")
        
         callKitCompletionHandler!(false)
        self.room = nil
        self.showRoomUI(inRoom: false)
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallReconnecting to room \(room.name), error = \(String(describing: error))")
    }
    
    func roomDidReconnect(room: Room) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallReconnected to room \(room.name)")
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        // Listen for events from all Participants to decide which RemoteVideoTrack to render.
        participant.delegate = self
        isPickedUp = 1
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallRoom \(room.name), Participant \(participant.identity) disconnected")
        SingletonClass.sharedInstance.isCommingCall = false
        self.goBack()
        // Nothing to do in this example. Subscription events are used to add/remove renderers.
    }
}



// MARK:- RemoteParticipantDelegate
extension CallControllerViewController : RemoteParticipantDelegate {
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has offered to share the video Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) published video track")
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has stopped sharing the video Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) unpublished video track")
    }
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        // Remote Participant has offered to share the audio Track.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) published audio track")
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) unpublished audio track")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // The LocalParticipant is subscribed to the RemoteParticipant's video Track. Frames will begin to arrive now.
        logMessage(messageText: "ATDebug :: Video Advisor Video CallSubscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == nil) {
            _ = renderRemoteParticipant(participant: participant)
        }
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallUnsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        
        if(self.boolIsFromSocket)
        {
            if self.remoteParticipant == participant {
                NSLog("Ravi debug123,didUnsubscribeFromVideoTrack")
                self.performEndCallAction(uuid:  room?.uuid ?? UUID())
                cleanupRemoteParticipant()
                
                // Find another Participant video to render, if possible.
                if var remainingParticipants =  room?.remoteParticipants,
                    let index = remainingParticipants.firstIndex(of: participant) {
                    remainingParticipants.remove(at: index)
                    renderRemoteParticipants(participants: remainingParticipants)
                }
            }
        }
        else
        {
            self.remoteParticipant = nil
            
        }
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallSubscribed to audio track for Participant \(participant.identity)")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        logMessage(messageText: "ATDebug :: Video Advisor Video CallUnsubscribed from audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) enabled video track")
    }
    
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) disabled video track")
    }
    
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) enabled audio track")
    }
    
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallParticipant \(participant.identity) disabled audio track")
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallFailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
    
    
    func renderRemoteParticipant(participant : RemoteParticipant) -> Bool {
        
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callremote render video")
        // This example renders the first subscribed RemoteVideoTrack from the RemoteParticipant.
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack,
                publication.isTrackSubscribed {
                setupRemoteVideoView()
                subscribedVideoTrack.addRenderer(self.remoteView!)
                self.remoteParticipant = participant
                return true
            }
        }
        return false
    }
    
    func renderRemoteParticipants(participants : Array<RemoteParticipant>) {
        for participant in participants {
            // Find the first renderable track.
            if participant.remoteVideoTracks.count > 0,
                renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }
    
    func setupRemoteVideoView() {
        
        
        // Creating `VideoView` programmatically
        self.remoteView = VideoView(frame: CGRect.zero, delegate: self)
        
        self.view.insertSubview(self.remoteView!, at: 0)
        // `VideoView` supports scaleToFill, scaleAspectFill and scaleAspectFit
        // scaleAspectFit is the default mode when you create `VideoView` programmatically.
        self.remoteView!.contentMode = .scaleAspectFill;
        
        let centerX = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerX)
        let centerY = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerY)
        let width = NSLayoutConstraint(item: self.remoteView!,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                       toItem: self.view,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       multiplier: 1,
                                       constant: 0);
        self.view.addConstraint(width)
        let height = NSLayoutConstraint(item: self.remoteView!,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        relatedBy: NSLayoutConstraint.Relation.equal,
                                        toItem: self.view,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        multiplier: 1,
                                        constant: 0);
        self.view.addConstraint(height)
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callstarted other video")
        self.perform(#selector(self.setupForLoader), with: nil, afterDelay: 3.0)
        
    }
    
    @objc func setupForLoader(){
        
     //   UtilityClass.hideCircleInAnimatingLoader()
//        self.ProfileUserView.isHidden = true
    }
    
}


// MARK:- VideoViewDelegate
extension CallControllerViewController : VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.logMessage(messageText: "ATDebug :: Video Advisor Video Callstarted other video")
        self.view.setNeedsLayout()
    }
}

// MARK:- CameraSourceDelegate
extension CallControllerViewController : CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        logMessage(messageText: "ATDebug :: Video Advisor Video CallCamera source failed with error: \(error.localizedDescription)")
    }
}

// MARK:- UITextFieldDelegate
extension CallControllerViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.connect(sender: textField)
        return true
    }
}


extension CallControllerViewController : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("Will present notification \(notification)")
        self.reportIncomingCall(uuid: UUID(), roomName: CallControllerViewController.parseNotification(notification: notification)) { _ in
            // Always call the completion handler when done.
            completionHandler(UNNotificationPresentationOptions())
        }
    }
    
    static func parseNotification(notification: UNNotification) -> String {
        var roomName = ""
        if let requestedName = notification.request.content.userInfo["roomName"] as? String {
            roomName = requestedName
        }
        return roomName
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        print("Received notification response in \(UIApplication.shared.applicationState.rawValue) \(response)")
        let roomName = CallControllerViewController.parseNotification(notification: response.notification)
        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            self.performStartCallAction(uuid: UUID(), roomName: roomName)
            completionHandler()
            break
        case "INVITE_ACTION":
            self.reportIncomingCall(uuid: UUID(), roomName: roomName) { _ in
                // Always call the completion handler when done.
                completionHandler()
            }
            break
        case "DECLINE_ACTION":
            completionHandler()
            break
        case UNNotificationDismissActionIdentifier:
            completionHandler()
            break
        // Handle other actions…
        default:
            break
        }
    }
}

//enum SocketAPiKeys: String {
//
//    case ConnectClient                            = "connect_client"
//    case CheckTyping                              = "check_typing"
//    case SendMessage                              = "send_message"
//    case ReceiverMessage                          = "receiver_message"
//    case AllMessageRead                           = "all_message_read"
//    case VideoCall                                = "video_call"
//    case VideoCallRejectedByReceiver              = "call_reject_by_advisor"
//    case VideoCallRejectedBySender                = "call_reject_by_user"
//    case PickupCall                               = "pick_up_call"
//    //
//    ///Doctor - Nurse Meeting call
//    case ConCall = "con_call"
//    case MeetingCode = "meeting_code"
//    case AddJoinee = "add_joinee" // Join as a Doctor/Nurse
//    case AddJoinee2 = "add_joinee2" // Join as a family member
//    case ConferenceCall = "conference_call"
//    case EndCallLog = "end_call_log"
//
//
//}

//MARK: - extra

extension Notification.Name {
//    static let internetConnected = NSNotification.Name("internetConnected")
//    static let internetDisconnected = NSNotification.Name("internetDisconnected")
    static let networkStatusChanged = NSNotification.Name("connectivityStatusChanged")

//    static let reloadMainCategories = NSNotification.Name("reloadMainCategories")
    static let openCovidFlow = NSNotification.Name("openCovidFlow")
    static let endCall = NSNotification.Name("endCall")
    static let refreshCallHistory = NSNotification.Name("refreshCallHistory")

    static let logOutPushHandle = NSNotification.Name("logOutPushHandle")
    static let openMyAppointmentsPageFromPush = NSNotification.Name("openMyAppointmentsPageFromPush")
    static let listNotificationPushHandle = NSNotification.Name("listNotificationPushHandle")
//    static let openContactCovidFromPush = NSNotification.Name("openContactCovidFromPush")
//    static let openSymptomsCovidFromPush = NSNotification.Name("openSymptomsCovidFromPush")
    static let openCallHistoryFromPush = NSNotification.Name("openCallHistoryFromPush")



//    static let homeVCRefrehData = NSNotification.Name("homeVCRefrehData")


//    static let closeParkingSlotSocket = NSNotification.Name("CloseParkingSlotSocket")

}
class Settings: NSObject {

    // ISDK-2644: Resolving a conflict with AudioToolbox in iOS 13
    let supportedAudioCodecs: [TwilioVideo.AudioCodec] = [IsacCodec(),
                                                          OpusCodec(),
                                                          PcmaCodec(),
                                                          PcmuCodec(),
                                                          G722Codec()]
    
    let supportedVideoCodecs: [VideoCodec] = [Vp8Codec(),
                                              Vp8Codec(simulcast: true),
                                              H264Codec(),
                                              Vp9Codec()]

    // Valid signaling Regions are listed here:
    // https://www.twilio.com/docs/video/ip-address-whitelisting#signaling-communication
    let supportedSignalingRegions: [String] = ["gll",
                                               "au1",
                                               "br1",
                                               "de1",
                                               "ie1",
                                               "in1",
                                               "jp1",
                                               "sg1",
                                               "us1",
                                               "us2"]


    let supportedSignalingRegionDisplayString: [String : String] = ["gll": "Global Low Latency",
                                                                    "au1": "Australia",
                                                                    "br1": "Brazil",
                                                                    "de1": "Germany",
                                                                    "ie1": "Ireland",
                                                                    "in1": "India",
                                                                    "jp1": "Japan",
                                                                    "sg1": "Singapore",
                                                                    "us1": "US East Coast (Virginia)",
                                                                    "us2": "US West Coast (Oregon)"]
    
    var audioCodec: TwilioVideo.AudioCodec?
    var videoCodec: VideoCodec?

    var maxAudioBitrate = UInt()
    var maxVideoBitrate = UInt()

    var signalingRegion: String?

    func getEncodingParameters() -> EncodingParameters?  {
        if maxAudioBitrate == 0 && maxVideoBitrate == 0 {
            return nil;
        } else {
            return EncodingParameters(audioBitrate: maxAudioBitrate,
                                      videoBitrate: maxVideoBitrate)
        }
    }
    
    private override init() {
        // Can't initialize a singleton
    }
    
    // MARK:- Shared Instance
    static let shared = Settings()
}
struct PlatformUtils {
    static let isSimulator: Bool = {
        var isSim = false
        #if arch(i386) || arch(x86_64)
            isSim = true
        #endif
        return isSim
    }()
}
extension CallControllerViewController
{
    
    func emitSocketReject(param: [String : Any]) {
        print(#function)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
    }
    
    
    func onSocketCallRejectBySender() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCallRejectedBySender.rawValue) { (json) in
            print(#function, "\n ", json)
            ///If call ended, then need to refresh status of call controller
            self.boolIsFromSocket = true
            
            self.goBack()
            
          
            
        }
    }
    
    func emitSocketPickupCall(param: [String : Any]) {
        print(#function)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.PickupCall.rawValue, with: param)
        self.OnPickupCall()
//        let controller:AdviserHomeViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserHomeViewController.storyboardID) as! AdviserHomeViewController
//
//        controller.TotalTimecounter = 0
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//            controller.startTimer(withInterval: 1.0)
//        })
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StartTimer"), object: nil, userInfo: nil)
        
    }
    func OnPickupCall()
    {
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.PickupCall.rawValue) { (response) in
 
            print(#function)
            print(response)
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["status"].string?.lowercased() == "accepted" {
                        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
                    } else if singleResponse["status"].string?.lowercased() == "rejected" {
                        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
                        if let topVC = UIApplication.topViewController() {
                            
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                            }
                            
                            
                        }
                    }
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
            
            
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
        }
    }

}

extension NSLayoutConstraint {
    func constraintWithMultiplier(_ multiplier: CGFloat) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: self.firstItem!, attribute: self.firstAttribute, relatedBy: self.relation, toItem: self.secondItem, attribute: self.secondAttribute, multiplier: multiplier, constant: self.constant)
    }
}

extension CallControllerViewController {

    func invalidateTimers() {
//        if let soundId = CallControllerViewController.soundId {
//            AudioServicesDisposeSystemSoundID(soundId)
//            AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
//            playCount += 1
//        }
        let soundId: SystemSoundID = getSoundID()
        AudioServicesDisposeSystemSoundID(soundId)
        AudioServicesDisposeSystemSoundID(kSystemSoundID_Vibrate)
        
        if let vibrationTimer = vibrationTimer {
            vibrationTimer.invalidate()
        }
        if let soundTimer = soundTimer {
            soundTimer.invalidate()
        }
    }
//    func playSoundAndVibrateInLoop() {
//        invalidateTimers()
////        if let soundId = CallControllerViewController.soundId {
////            AudioServicesPlaySystemSound(soundId)
////           // AudioServicesPlaySystemSound()
////            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
////            playCount += 1
////        }
//        let soundId: SystemSoundID = getSoundID()
//        AudioServicesPlaySystemSound(soundId)
//        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//         soundTimer = Timer.scheduledTimer(timeInterval: CallControllerViewController.timerInterval, target: self, selector: #selector(playSound(_:)), userInfo: nil, repeats: true)
//    }

    @IBAction func playSound(_ sender: Any) {
        let soundId: SystemSoundID = getSoundID()
        AudioServicesPlaySystemSound(soundId)
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//        if let soundId = CallControllerViewController.soundId {
//            AudioServicesPlaySystemSound(soundId)
//            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
//            playCount += 1
//        }
    }
   

    
    @IBAction func cancelAll(_ sender: Any) {
        invalidateTimers()
    }

}






















extension CallControllerViewController : CXProviderDelegate {

    func providerDidReset(_ provider: CXProvider) {
        logMessage(messageText: "providerDidReset:")

        // AudioDevice is enabled by default
        self.audioDevice.isEnabled = false
        
        room?.disconnect()
    }

    func providerDidBegin(_ provider: CXProvider) {
        logMessage(messageText: "providerDidBegin")
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didActivateAudioSession:")

        self.audioDevice.isEnabled = true
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        logMessage(messageText: "provider:didDeactivateAudioSession:")
        
        audioDevice.isEnabled = false
    }

    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        logMessage(messageText: "provider:timedOutPerformingAction:")
    }

    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        logMessage(messageText: "provider:performStartCallAction:")

        callKitProvider?.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        
        performRoomConnect(uuid: action.callUUID, roomName: action.handle.value) { (success) in
            if (success) {
                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        logMessage(messageText: "provider:performAnswerCallAction:")
//        self.webServiceForAccept()
        performRoomConnect(uuid: action.callUUID, roomName: roomName ?? "") { (success) in
            if (success) {
                action.fulfill(withDateConnected: Date())
            } else {
                action.fail()
            }
        }
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("provider:performEndCallAction:")
        self.webserviceforRejectCall(reject: 1)
        room?.disconnect()

        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        NSLog("provier:performSetMutedCallAction:")
        
        muteAudio(isMuted: action.isMuted)
        
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        NSLog("provier:performSetHeldCallAction:")

        let cxObserver = callKitCallController?.callObserver
        let calls = cxObserver?.calls

        guard let call = calls?.first(where:{$0.uuid == action.callUUID}) else {
            action.fail()
            return
        }

        if call.isOnHold {
            holdCall(onHold: false)
        } else {
            holdCall(onHold: true)
        }
        action.fulfill()
    }
}

