//
//  AdvisorAudioCall.swift
//  TempleBliss
//
//  Created by Apple on 25/06/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import Foundation
import TwilioVoice

class AdvisorAudioCall: NSObject {
    static let sharedInstance = AdvisorAudioCall()
    var audioDevice = DefaultAudioDevice()
//    var room: Room?
//    /**
//     * We will create an audio device and manage it's lifecycle in response to CallKit events.
//     */
//    var camera: CameraSource?
//    var localVideoTrack: LocalVideoTrack?
//    var localAudioTrack: LocalAudioTrack?
//    var remoteParticipant: RemoteParticipant?
//    var remoteView: VideoView?

    func toggleAudioRoute(toSpeaker: Bool) {
        print("ATDebug New :: \(#function)")
        // The mode set by the Voice SDK is "VoiceChat" so the default audio route is the built-in receiver. Use port override to switch the route.
        AdvisorAudioCall.sharedInstance.audioDevice.block = {
            DefaultAudioDevice.DefaultAVAudioSessionConfigurationBlock()
            
            //DefaultAudioDevice.DefaultAVAudioSessionConfigurationBlock()
            // DefaultAudioDevice.DefaultAVAudioSessionConfigurationBlock()
            //            TVODefaultAudioDevice.
            do {
                if toSpeaker {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                } else {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
                }
            } catch {
                NSLog(error.localizedDescription)
            }
        }
        
        AdvisorAudioCall.sharedInstance.audioDevice.block()
    }
}



