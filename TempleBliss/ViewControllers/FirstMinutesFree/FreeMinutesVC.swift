//
//  FreeMinutesVC.swift
//  TempleBliss
//
//  Created by iMac on 15/03/22.
//  Copyright © 2022 EWW071. All rights reserved.
//

import UIKit

class FreeMinutesVC: BaseViewController {
    
    
    // MARK: - Outlets
    @IBOutlet private weak var btnLetsGo: theamButtonWithBorder!
    @IBOutlet private weak var lblTitle: UILabel!
    @IBOutlet private weak var lblFreeMinutes: UILabel!

    
    // MARK: - Properties
    var freeMinutes = ""
    
    var didPressLetsStartBtn: (() -> ())?
    
    // MARK: - VC Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        isFreePopUpShown = true
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - SetupUI
    func setupUI() {
        lblTitle.font = CustomFont.bold.returnFont(22)
        lblFreeMinutes.font = CustomFont.bold.returnFont(36)
        
        lblTitle.text = "Enjoy \(freeMinutes) free minutes"
        lblFreeMinutes.text = "\(freeMinutes)"

        btnLetsGo.backgroundColor = colors.submitButtonTextColor.value
        btnLetsGo.titleLabel?.font = CustomFont.medium.returnFont(18)
        
        btnLetsGo.layer.cornerRadius = 8

    }
    
    // MARK: - Api call
    

    // MARK: - Action
    @IBAction private func onBtnLetsGo(_ sender: UIButton) {
        didPressLetsStartBtn?()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SelectAdvisorTab"), object: nil, userInfo: nil)

        self.dismiss(animated: true, completion: nil)
    }
    
    
}
