//
//  AudioCallViewController.swift
//  TempleBliss
//
//  Created by Apple on 05/04/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit
import AVFoundation
import PushKit
import CallKit
import TwilioVoice
import SDWebImage


class AudioCallViewController: BaseViewController {
    
    //MARK: - Properties
    var IsEndSessionDone = false
    var StartTimeOfRinging = Date()
    var EndTimeOfRinging = Date()
    
    var CallRejectByAdvisor : Bool = false
    var CallCutByCustomer : Bool = false
    
    var activeCall: Call? = nil
    var is_rejected: Int?
    var isPickedUp: Int?
    
    var SecondsForReminderScreen = 15 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var CommunicationScreenRemiderScreen = 15
    var TimerForReminderScreen = Timer()

    var totalMinutesTimer = 0
    var totalMinutes = "1"
    
    var internalTimer: Timer?
    var TotalTimecounter = 0
    
    var customerID : String?
    var AdvisorID : String?
    var bookingID : String?
    
    
    var advisorUserImageURl = ""
    var advisorName = ""
    
    var myIdentity = ""
    var CallToAdvisorName = ""
    
    let twimlParamTo = "To"
    
    let kRegistrationTTLInDays = 365
    
    let kCachedDeviceToken = "CachedDeviceToken" //"CachedDeviceToken"
    let kCachedBindingDate = "CachedBindingDate"
    
    
    var incomingPushCompletionCallback: (() -> Void)?
    var incomingAlertController: UIAlertController?
    
    var callKitCompletionCallback: ((Bool) -> Void)? = nil
    var audioDevice = DefaultAudioDevice()
    //DefaultAudioDevice()
    var activeCallInvites: [String: CallInvite]! = [:]
    var activeCalls: [String: Call]! = [:]
        
    // activeCall represents the last connected call
  //  var activeCall: Call? = nil
    
    var callKitProvider: CXProvider?
    var callKitCallController : CXCallController? = nil
    var userInitiatedDisconnect: Bool = false
    
    /*
     Custom ringback will be played when this flag is enabled.
     When [answerOnBridge](https://www.twilio.com/docs/voice/twiml/dial#answeronbridge) is enabled in
     the <Dial> TwiML verb, the caller will not hear the ringback while the call is ringing and awaiting
     to be accepted on the callee's side. Configure this flag based on the TwiML application.
     */
    
    var playCustomRingback = false
    var ringtonePlayer: AVAudioPlayer? = nil
    
    
    func removeAll() {
        
        if let provider = self.callKitProvider {
            provider.invalidate()
        }
        activeCall?.disconnect()
        activeCall = nil
        activeCalls = [:]
        activeCallInvites = [:]
        callKitCompletionCallback = nil
    }
    
//    required init?(coder aDecoder: NSCoder) {
//        // isSpinning = false
//
//        super.init(coder: aDecoder)
//    }
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        if let provider = self.callKitProvider {
            provider.invalidate()
        }
    }
    
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var BtnSpeaker: UIButton!
    @IBOutlet weak var BtnMuteCall: UIButton!
    @IBOutlet weak var BtnEndcall: UIButton!
    @IBOutlet weak var AdvisorImageView: UIImageView!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblAdvisorName: UILabel!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BtnEndcall.alpha = 1.0
        BtnEndcall.isEnabled = true
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        setLocalization()
        setValue()
        pickupCall()
        
        /* Please note that the designated initializer `CXProviderConfiguration(localizedName: String)` has been deprecated on iOS 14. */
        lblTimer.text = "Connecting"
        
                
        
        // Do any additional setup after loading the view.
    }
    func CallToAdvisorWithBookingID() {
        let configuration = CXProviderConfiguration(localizedName: "Voice Quickstart")
        configuration.ringtoneSound = "\(RingtoneSoundName).mp3"

        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        configuration.includesCallsInRecents = false
        configuration.supportsVideo = false
        if let callKitIcon = UIImage(named: CallKitIconName) {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }
        self.callKitProvider = CXProvider(configuration: configuration)
        if let provider = self.callKitProvider {

            provider.setDelegate(self, queue: nil)
        }
        
        self.totalMinutesTimer = (Int(self.totalMinutes) ?? 1)
        callToAdvisor()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        /*
         * The important thing to remember when providing a TVOAudioDevice is that the device must be set
         * before performing any other actions with the SDK (such as connecting a Call, or accepting an incoming Call).
         * In this case we've already initialized our own `TVODefaultAudioDevice` instance which we will now set.
         */
    
        TwilioVoiceSDK.audioDevice = self.audioDevice
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        if CallRejectByAdvisor {
            if isPickedUp == nil {
                DispatchQueue.main.async {
                    Utilities.ShowAlert(OfMessage: RequestRejectMessage)
                }
            }
           
        }
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCall.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedByReceiver.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
        
        
        self.stopTimerOnEndSession()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        if let provider = self.callKitProvider {
            provider.invalidate()
        }
        activeCall?.disconnect()
        activeCall = nil
        activeCalls = [:]
        activeCallInvites = [:]
        callKitCompletionCallback = nil
//        guard activeCall == nil else {
//        self.userInitiatedDisconnect = true
//            //performEndCallAction(uuid: activeCall!.uuid)
//            activeCall = nil
//            return
//        }
        
        self.TimerForReminderScreen.invalidate()
        self.internalTimer?.invalidate()
    }
    //MARK: - Microphone permission
    func showMicrophoneAccessRequest(_ uuid: UUID, _ handle: String) {
        let alertController = UIAlertController(title: "Voice Quick Start",
                                                message: "Microphone permission not granted",
                                                preferredStyle: .alert)
        
        let continueWithoutMic = UIAlertAction(title: "Continue without microphone", style: .default) { [weak self] _ in
            self?.performStartCallAction(uuid: uuid, handle: handle)
        }
        
        let goToSettings = UIAlertAction(title: "Settings", style: .default) { _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!,
                                      options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly: false],
                                      completionHandler: nil)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            //            self?.toggleUIState(isEnabled: true, showCallControl: false)
            //            self?.stopSpin()
        }
        
        [continueWithoutMic, goToSettings, cancel].forEach { alertController.addAction($0) }
        
        present(alertController, animated: true, completion: nil)
    }
    func checkRecordPermission(completion: @escaping (_ permissionGranted: Bool) -> Void) {
        let permissionStatus = AVAudioSession.sharedInstance().recordPermission
        
        switch permissionStatus {
        case .granted:
            // Record permission already granted.
            completion(true)
        case .denied:
            // Record permission denied.
            completion(false)
        case .undetermined:
            // Requesting record permission.
            // Optional: pop up app dialog to let the users know if they want to request.
            AVAudioSession.sharedInstance().requestRecordPermission { granted in completion(granted) }
        default:
            completion(false)
        }
    }
    //MARK: - call function
    func callToAdvisor() {
        guard self.activeCall == nil else {
            
            self.userInitiatedDisconnect = true
            performEndCallAction(uuid: self.activeCall!.uuid ?? UUID())
            
            // performEndCallAction(uuid: activeCall!.uuid!)
            // toggleUIState(isEnabled: false, showCallControl: false)
            
            return
        }
        
        checkRecordPermission { [weak self] permissionGranted in
            let uuid = UUID()
            let handle = self?.myIdentity ?? ""
            print(self?.myIdentity ?? "")
            guard !permissionGranted else {
                sleep(1)
                if self?.callKitCallController == nil {
                    self?.callKitCallController = CXCallController()
                }
                self?.performStartCallAction(uuid: uuid, handle: handle)
                return
            }
            
            self?.showMicrophoneAccessRequest(uuid, handle)
        }
    }
    //MARK: - other methods
    func SocketEmitForCutCall(reject: Int?,user_id:Int,user_name:String,advisor_id:Int,booking_id:Int) {
        print("Customer side audio call New :: \(#function)")
        is_rejected = reject
        var param = [String: Any]()
        
        param = ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "before_receive"]
        
        if is_rejected != nil {
            
            if isPickedUp != nil {
                
                param = ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "after_receive"]
                
            } else {
                param =  ["user_id":user_id,"user_name":user_name,"advisor_id":advisor_id,"booking_id": booking_id, "call_status" : "before_receive"]
                
            }
        }
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue, with: param)
    }
    func SocketForReject(reject: Int?) {

        is_rejected = reject
        var param = [String: Any]()

        param = ["user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                 "advisor_id" : Int(AdvisorID ?? "") ?? 0,
                 "booking_id" : Int(bookingID ?? "") ?? 0,
                 "user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","call_status" : "before_receive"
        ]

        if is_rejected != nil {
            if isPickedUp != nil {
                param = ["user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                         "advisor_id" : Int(AdvisorID ?? "") ?? 0,
                         "booking_id" : Int(bookingID ?? "") ?? 0,
                         "user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","call_status" : "after_receive"
                ]
            } else {
                param = ["user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                         "advisor_id" : Int(AdvisorID ?? "") ?? 0,
                         "booking_id" : Int(bookingID ?? "") ?? 0,
                         "user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","call_status" : "before_receive"
                ]
            }
        }
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedBySender.rawValue, with: param)
    }
    func setLocalization() {
        
    }
    func setValue() {
        
        AdvisorImageView.contentMode = .scaleToFill
        let imgURL:String = "\(self.advisorUserImageURl)"
        if imgURL != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(imgURL)")
            
            AdvisorImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            AdvisorImageView.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
        } else {
            AdvisorImageView.image = UIImage(named: "user_dummy_profile")
        }
        lblAdvisorName.text = self.advisorName
       
        lblTimer.text = "Calling"
        
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
        
    }
    
    func fetchAccessToken() -> String? {
        print("My Identity New :: \(self.myIdentity)")
//        print(":::::::: \(socketApiKeys.SocketBaseURL.rawValue)/token/\(self.myIdentity)")
        guard let accessTokenURL = URL(string: "\(SOCKET_URL)/token/\(self.myIdentity)") else { return nil }
       
        let jsonString = try? String(contentsOf: accessTokenURL, encoding: .utf8)
        
        let jsonDictonary = convertToDictionary(text: jsonString ?? "")
        let identityFromJson:String = jsonDictonary?["identity"] as? String ?? ""
        let accessToken:String = jsonDictonary?["token"] as? String ?? ""
        
        print("identityFromJson from json is :: \(identityFromJson)")
        print("accessToken from json is :: \(accessToken)")
        
//        return accessToken
        return AccessTokenForVOIP
    }
    
    func toggleAudioRoute(toSpeaker: Bool) {
        // The mode set by the Voice SDK is "VoiceChat" so the default audio route is the built-in receiver. Use port override to switch the route.
        self.audioDevice.block = {
            DefaultAudioDevice.DefaultAVAudioSessionConfigurationBlock()

            do {
                if toSpeaker {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.speaker)
                } else {
                    try AVAudioSession.sharedInstance().overrideOutputAudioPort(.none)
                }
            } catch {
                NSLog(error.localizedDescription)
            }
        }
        
        self.audioDevice.block()
    }
    
    //MARK: - IBActions
    @IBAction func BtnSpeakerClick(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            toggleAudioRoute(toSpeaker: false)
        } else {
            sender.isSelected = true
            toggleAudioRoute(toSpeaker: true)
        }
        
    }
    
    @IBAction func BtnEndCallClick(_ sender: Any) {
        CallCutByCustomer = true
        SocketForReject(reject: 1)
        callToAdvisor()
    }
    
    @IBAction func BtnMuteCallClick(_ sender: UIButton) {
        
        guard let activeCall = self.activeCall else { return }
        if sender.isSelected {
            sender.isSelected = false
            activeCall.isMuted = false
        } else {
            sender.isSelected = true
            activeCall.isMuted = true
        }
        
    }
}
// MARK: - PushKitEventDelegate

extension AudioCallViewController : PushKitEventDelegate {
    
    
    func credentialsUpdated(credentials: PKPushCredentials) {
       
      //  Ankur's Change
//                guard
//                    (registrationRequired() || UserDefaults.standard.data(forKey: kCachedDeviceToken) != credentials.token),let accessToken = fetchAccessToken()
//                else {
//                    return
//                }
        
        
        //Ankur's Change
        
        let accessToken = fetchAccessToken() ?? ""
        print("Assess Token New :: \(accessToken)")
        //        let accessToken = fetchAccessToken() ?? ""
        let cachedDeviceToken = credentials.token
        /*
         * Perform registration if a new device token is detected.
         */
        print("Ravi Debug register tocken : \(accessToken)")
        guard accessToken.count > 0, cachedDeviceToken.count > 0 else {
            return
        }
        TwilioVoiceSDK.register(accessToken: accessToken, deviceToken: cachedDeviceToken,completion: { error in
            if let error = error {
                print("An error occurred while registering: \(error.localizedDescription)")
            } else {
                print("Successfully registered for VoIP push notifications.")
                
                // Save the device token after successfully registered.
                
                UserDefaults.standard.setValue(cachedDeviceToken, forKey: "VOPIDeviceToken")
                UserDefaults.standard.setValue(accessToken, forKey: "VOIPAccessToken")
                
                UserDefaults.standard.synchronize()
                UserDefaults.standard.set(cachedDeviceToken, forKey: self.kCachedDeviceToken)
                
                /**
                 * The TTL of a registration is 1 year. The TTL for registration for this device/identity
                 * pair is reset to 1 year whenever a new registration occurs or a push notification is
                 * sent to this device/identity pair.
                 */
                UserDefaults.standard.set(Date(), forKey: self.kCachedBindingDate)
                //UserDefaults.standard.string(forKey: "")
                let AccessToken = UserDefaults.standard.data(forKey: "VOPIDeviceToken")
                let DeviceToken = UserDefaults.standard.value(forKey: "VOIPAccessToken")
                print("access token : \(String(describing: AccessToken)) and device token is :\(String(describing: DeviceToken))")
            }
        })
    }
    
    /**
     * The TTL of a registration is 1 year. The TTL for registration for this device/identity pair is reset to
     * 1 year whenever a new registration occurs or a push notification is sent to this device/identity pair.
     * This method checks if binding exists in UserDefaults, and if half of TTL has been passed then the method
     * will return true, else false.
     */
    func registrationRequired() -> Bool {
        guard
            let lastBindingCreated = UserDefaults.standard.object(forKey: self.kCachedBindingDate)
        else { return true }
        
        let date = Date()
        var components = DateComponents()
        components.setValue(self.kRegistrationTTLInDays/2, for: .day)
        let expirationDate = Calendar.current.date(byAdding: components, to: lastBindingCreated as! Date)!
        
        if expirationDate.compare(date) == ComparisonResult.orderedDescending {
            return false
        }
        return true;
    }
    func UnregisterVOIP() {
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                print("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                print("Successfully unregistered from VoIP push notifications.")
            }
        })
        
        
        //
        ////        let AccessToken = UserDefaults.standard.value(forKey: "VOPIDeviceToken")
        ////        let DeviceToken = UserDefaults.standard.value(forKey: "VOPIDeviceToken")
        //        print("access token : \(AccessToken) and device token is :\(DeviceToken)")
        //        guard let deviceToken = UserDefaults.standard.data(forKey: kCachedDeviceToken),
        //            let accessToken = fetchAccessToken() else { return }
        //        TwilioVoice.unregister(withAccessToken: accessToken, deviceToken: deviceToken, completion: { error in
        //            if let error = error {
        //                print("An error occurred while unregistering: \(error.localizedDescription)")
        //            } else {
        //                print("Successfully unregistered from VoIP push notifications.")
        //            }
        //        })
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
    func credentialsInvalidated() {
        
        let deviceVOIPToken = pushTokenForVOIP.count > 0 ? pushTokenForVOIP : UserDefaults.standard.data(forKey: self.kCachedDeviceToken) ?? Data()
        print("Device VOIP Token: ", deviceVOIPToken)
        TwilioVoiceSDK.unregister(accessToken: AccessTokenForVOIP, deviceToken: deviceVOIPToken, completion: { error in
            if let error = error {
                print("An error occurred while unregistering: \(error.localizedDescription)")
            } else {
                print("Successfully unregistered from VoIP push notifications.")
            }
        })
        
        
        UserDefaults.standard.removeObject(forKey: self.kCachedDeviceToken)
        
        // Remove the cached binding as credentials are invalidated
        UserDefaults.standard.removeObject(forKey: self.kCachedBindingDate)
    }
    
    func incomingPushReceived(payload: PKPushPayload) {
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
        // TwilioVoiceSDK.handleNotification(, delegate: self, delegateQueue: nil)
    }
    
    func incomingPushReceived(payload: PKPushPayload, completion: @escaping () -> Void) {
        // The Voice SDK will use main queue to invoke `cancelledCallInviteReceived:error:` when delegate queue is not passed
        TwilioVoiceSDK.handleNotification(payload.dictionaryPayload, delegate: self, delegateQueue: nil)
        
        if let version = Float(UIDevice.current.systemVersion), version < 13.0 {
            // Save for later when the notification is properly handled.
            self.incomingPushCompletionCallback = completion
        }
    }
    
    func incomingPushHandled() {
        guard let completion = self.incomingPushCompletionCallback else { return }
        
        self.incomingPushCompletionCallback = nil
        completion()
    }
    
    
    
}
//MARK: - SocketOn For Start Timer
extension AudioCallViewController {
    func OnForStartTimer() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.advisor_to_customer.rawValue) { (response) in
            
            print(#function)
            print(response)
            
        }
    }
    func pickupCall()
    {
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.PickupCall.rawValue) { (response) in
 
            print(#function)
            print(response)
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["status"].string?.lowercased() == "accepted" {
                        SingletonClass.sharedInstance.FreeMinutes = "0"
                        if let topvc = UIApplication.topViewController() {
                            if topvc.isKind(of: AudioCallViewController.self) {
                                self.isPickedUp = 1
                                self.startTimer(withInterval: 1.0)
                            } else {
                                self.SocketForReject(reject: 1)
                            }
                        }
                    } else if singleResponse["status"].string?.lowercased() == "rejected" {
                        self.lblTimer.text = "Call ended"
                        if let topVC = UIApplication.topViewController() {
                            
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                    }
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
            
            
        }
    }
}

// MARK: - TVONotificaitonDelegate

extension AudioCallViewController: NotificationDelegate {
    func callInviteReceived(callInvite: CallInvite) {
        print("callInviteReceived:")
        
        /**
         * The TTL of a registration is 1 year. The TTL for registration for this device/identity
         * pair is reset to 1 year whenever a new registration occurs or a push notification is
         * sent to this device/identity pair.
         */
        UserDefaults.standard.set(Date(), forKey: self.kCachedBindingDate)
        //        let callInf
        //        let callerInfo:CallerInfo = callInvite.callerInfo
        //        if let verified: NSNumber = callerInfo.verified {
        //            if verified.boolValue {
        //                print("Call invite received from verified caller number!")
        //            }
        //        }
        
        let from = (callInvite.from ?? "Voice Bot").replacingOccurrences(of: "client:", with: "")
        
        // Always report to CallKit
        reportIncomingCall(from: from, uuid: callInvite.uuid)
        self.activeCallInvites[callInvite.uuid.uuidString] = callInvite
    }
    
    func cancelledCallInviteReceived(cancelledCallInvite: CancelledCallInvite, error: Error) {
        print("cancelledCallInviteCanceled:error:, error: \(error.localizedDescription)")
        
        guard let activeCallInvites = self.activeCallInvites, !activeCallInvites.isEmpty else {
            print("No pending call invite")
            return
        }
        
        let callInvite = activeCallInvites.values.first { invite in invite.callSid == cancelledCallInvite.callSid }
        
        if let callInvite = callInvite {
            performEndCallAction(uuid: callInvite.uuid)
            self.activeCallInvites.removeValue(forKey: callInvite.uuid.uuidString)
        }
    }
    
}



// MARK: - CallDelegate

extension AudioCallViewController: CallDelegate {
    
    
    func callDidFailToConnect(call: Call, error: Error) {
        print("DEBUG11 :: \(#function)")
        print("Customer side audio call AUDIOCALL :: \(#function)")
        print(error)
        print("call:isReconnectingWithError:")
        lblTimer.text = "Call Ended"
        
        var reason = CXCallEndedReason.remoteEnded

      
            reason = .failed
     
        if let provider = self.callKitProvider {
            provider.reportCall(with: call.uuid ?? UUID(), endedAt: Date(), reason: reason)
            userInitiatedDisconnect = true
            provider.invalidate()
            activeCall?.disconnect()
             activeCall = nil
            activeCalls = [:]
            activeCallInvites = [:]
            callKitCompletionCallback = nil
        }
        DispatchQueue.main.async {
            self.dismissCallControllerScreen()
        }
       
        
        callDisconnected(call: call)
    }
   
    func callDidStartRinging(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Customer side audio call AUDIOCALL :: \(#function)")
        print("callDidStartRinging:")
        StartTimeOfRinging = Date()
        lblTimer.text = "Ringing"
        BtnEndcall.alpha = 1.0
        BtnEndcall.isEnabled = true
        //        placeCallButton.setTitle("Ringing", for: .normal)
        
        /*
         When [answerOnBridge](https://www.twilio.com/docs/voice/twiml/dial#answeronbridge) is enabled in the
         <Dial> TwiML verb, the caller will not hear the ringback while the call is ringing and awaiting to be
         accepted on the callee's side. The application can use the `AVAudioPlayer` to play custom audio files
         between the `[CallDelegate callDidStartRinging:]` and the `[CallDelegate callDidConnect:]` callbacks.
         */
        if self.playCustomRingback {
            playRingback()
        }
    }
    
    func callDidConnect(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Customer side audio call AUDIOCALL :: \(#function)")
        print("callDidConnect:")
        
        if self.playCustomRingback {
            stopRingback()
        }
        
        if let callKitCompletionCallback = self.callKitCompletionCallback {
            callKitCompletionCallback(true)
        }
        
        //        placeCallButton.setTitle("Hang Up", for: .normal)
        
        //        toggleUIState(isEnabled: true, showCallControl: true)
        //        stopSpin()
                toggleAudioRoute(toSpeaker: false)
    }
    
    //    func call(_ call: Call, didFailToConnectWithError error: Error?) {
    
    //    }
    
    func callDidReconnect(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Customer side audio call AUDIOCALL :: \(#function)")
        print("callDidReconnect:")
        
        //        placeCallButton.setTitle("Hang Up", for: .normal)
        //
        //        toggleUIState(isEnabled: true, showCallControl: true)
    }
    
    
    func callDidDisconnect(call: Call, error: Error?) {
        print(call.state)
        
        if CallCutByCustomer == false {
            print("user_id :: \(Int(customerID ?? "") ?? 0)")
            print("advisor_id :: \(Int(AdvisorID ?? "") ?? 0)")
            print("booking_id :: \(Int(bookingID ?? "") ?? 0)")
            CallRejectByAdvisor = true
            SocketEmitForCutCall(reject: 1, user_id: Int(customerID ?? "") ?? 0, user_name: SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "", advisor_id: Int(AdvisorID ?? "") ?? 0, booking_id: Int(bookingID ?? "") ?? 0)
        }
        print("Customer side audio call AUDIOCALL :: \(#function)")
        call.disconnect()
       lblTimer.text = "Call Ended"
      

        if let provider = self.callKitProvider {
            
            userInitiatedDisconnect = true
            provider.invalidate()
            activeCall?.disconnect()
             activeCall = nil
            activeCalls = [:]
            activeCallInvites = [:]
            callKitCompletionCallback = nil
        }
        DispatchQueue.main.async {
            self.dismissCallControllerScreen()
        }
        
        
        
        if !self.userInitiatedDisconnect {
            var reason = CXCallEndedReason.remoteEnded
            
            if error != nil {
                reason = .failed
            }
            
            if let provider = self.callKitProvider {
                provider.reportCall(with: call.uuid ?? UUID(), endedAt: Date(), reason: reason)
            }
        }
        
        callDisconnected(call: call)
    }
    
    func callDisconnected(call: Call) {
        print("DEBUG11 :: \(#function)")
        print("Customer side audio call AUDIOCALL :: \(#function)")
        if call == self.activeCall {
            self.activeCall = nil
        }
        
        self.activeCalls.removeValue(forKey: call.uuid?.uuidString ?? UUID().uuidString)
        
        self.userInitiatedDisconnect = false
        
        if self.playCustomRingback {
            stopRingback()
        }
        
        //        stopSpin()
        //        toggleUIState(isEnabled: true, showCallControl: false)
        //        placeCallButton.setTitle("Call", for: .normal)
    }
    
    func call(call: Call, didReceiveQualityWarnings currentWarnings: Set<NSNumber>, previousWarnings: Set<NSNumber>) {
        print("Customer side audio call AUDIOCALL :: \(#function)")
        /**
         * currentWarnings: existing quality warnings that have not been cleared yet
         * previousWarnings: last set of warnings prior to receiving this callback
         *
         * Example:
         *   - currentWarnings: { A, B }
         *   - previousWarnings: { B, C }
         *   - intersection: { B }
         *
         * Newly raised warnings = currentWarnings - intersection = { A }
         * Newly cleared warnings = previousWarnings - intersection = { C }
         */
        var warningsIntersection: Set<NSNumber> = currentWarnings
        warningsIntersection = warningsIntersection.intersection(previousWarnings)
        
        var newWarnings: Set<NSNumber> = currentWarnings
        newWarnings.subtract(warningsIntersection)
        if newWarnings.count > 0 {
            qualityWarningsUpdatePopup(newWarnings, isCleared: false)
        }
        
        var clearedWarnings: Set<NSNumber> = previousWarnings
        clearedWarnings.subtract(warningsIntersection)
        if clearedWarnings.count > 0 {
            qualityWarningsUpdatePopup(clearedWarnings, isCleared: true)
        }
    }
    
    func qualityWarningsUpdatePopup(_ warnings: Set<NSNumber>, isCleared: Bool) {
        print("Customer side audio call AUDIOCALL :: \(#function)")
        //        var popupMessage: String = "Warnings detected: "
        //        if isCleared {
        //            popupMessage = "Warnings cleared: "
        //        }
        //        //let mappedWarnings: [String] = warnings.map({number in warningString(.)})
        //        let mappedWarnings: [String] = warnings.map { number in warningString(Call.QualityWarning(rawValue: number.uintValue)!)}
        //        popupMessage += mappedWarnings.joined(separator: ", ")
        //
        //        qualityWarningsToaster.alpha = 0.0
        //        qualityWarningsToaster.text = popupMessage
        //        UIView.animate(withDuration: 1.0, animations: {
        //            self.qualityWarningsToaster.isHidden = false
        //            self.qualityWarningsToaster.alpha = 1.0
        //        }) { [weak self] finish in
        //            guard let strongSelf = self else { return }
        //            let deadlineTime = DispatchTime.now() + .seconds(5)
        //            DispatchQueue.main.asyncAfter(deadline: deadlineTime, execute: {
        //                UIView.animate(withDuration: 1.0, animations: {
        //                    strongSelf.qualityWarningsToaster.alpha = 0.0
        //                }) { (finished) in
        //                    strongSelf.qualityWarningsToaster.isHidden = true
        //                }
        //            })
        //        }
    }
    
    func warningString(_ warning: Call) -> String {
        print("Customer side audio call AUDIOCALL :: \(#function)")
        //        switch warning {
        //        case .highRtt: return "high-rtt"
        //        case .highJitter: return "high-jitter"
        //        case .highPacketsLostFraction: return "high-packets-lost-fraction"
        //        case .lowMos: return "low-mos"
        //        case .constantAudioInputLevel: return "constant-audio-input-level"
        //        default: return "Unknown warning"
        //        }
        return ""
    }
    
    
    // MARK: - Ringtone
    
    func playRingback() {
        let ringtonePath = URL(fileURLWithPath: Bundle.main.path(forResource: "ringtone", ofType: "wav")!)
        
        do {
            self.ringtonePlayer = try AVAudioPlayer(contentsOf: ringtonePath)
            self.ringtonePlayer?.delegate = self
            self.ringtonePlayer?.numberOfLoops = -1
            
            self.ringtonePlayer?.volume = 1.0
            self.ringtonePlayer?.play()
        } catch {
            print("Failed to initialize audio player")
        }
    }
    
    func stopRingback() {
        guard let ringtonePlayer = self.ringtonePlayer, ringtonePlayer.isPlaying else { return }
        
        ringtonePlayer.stop()
    }
}


// MARK: - CXProviderDelegate

extension AudioCallViewController: CXProviderDelegate {
    func providerDidReset(_ provider: CXProvider) {
        
        print("providerDidReset:")
        self.audioDevice.isEnabled = false
    }
    
    func providerDidBegin(_ provider: CXProvider) {
        print("providerDidBegin")
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print("provider:didActivateAudioSession:")
        self.audioDevice.isEnabled = true
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        print("provider:didDeactivateAudioSession:")
        self.audioDevice.isEnabled = false
        if let provider = self.callKitProvider {
            provider.invalidate()
        }
        DispatchQueue.main.async {
            self.dismissCallControllerScreen()
        }
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print("provider:timedOutPerformingAction:")
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        print("provider:performStartCallAction:")
        
        //        toggleUIState(isEnabled: false, showCallControl: false)
        //        startSpin()
        
        provider.reportOutgoingCall(with: action.callUUID, startedConnectingAt: Date())
        
        performVoiceCall(uuid: action.callUUID, client: "") { success in
            if success {
                print("performVoiceCall() successful")
                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
            } else {
                print("performVoiceCall() failed")
            }
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        print("provider:performAnswerCallAction:")
        
        self.performAnswerVoiceCall(uuid: action.callUUID)
        { (success) in
            if (success)
            {
                action.fulfill()
                
            }
            else
            {
                action.fail()
            }
        }
        action.fulfill()
        
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        
     
        print("provider:performEndCallAction:")

        lblTimer.text = "Call Ended"
        if let invite = self.activeCallInvites[action.callUUID.uuidString] {
            invite.reject()
            self.activeCallInvites.removeValue(forKey: action.callUUID.uuidString)
        } else if let call = self.activeCalls[action.callUUID.uuidString] {
            call.disconnect()
            endSession()
        } else {
            print("Unknown UUID to perform end-call action with")
        }
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        print("provider:performSetHeldAction:")
        
        if let call = self.activeCalls[action.callUUID.uuidString] {
            call.isOnHold = action.isOnHold
            action.fulfill()
        } else {
            action.fail()
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        print("provider:performSetMutedAction:")
        
        if let call = self.activeCalls[action.callUUID.uuidString] {
            call.isMuted = action.isMuted
            action.fulfill()
        } else {
            action.fail()
        }
    }
    
    
    // MARK: - Call Kit Actions
    
    func performStartCallAction(uuid: UUID, handle: String) {
        print("Ankur's Debug :: \(#function)")
        print("ATDebug New :: \(#function)")
        guard let provider = self.callKitProvider else {
            print("SJDebug CallKit provider not available")
            return
        }
        
        //SJ_Change :
        let callHandle = CXHandle(type: .generic, value: handle)
        
        print("handle : \(handle)")
        
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        let transaction = CXTransaction(action: startCallAction)
        
        self.callKitCallController?.request(transaction) { error in
            if let error = error {
                print("SJDebug StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            
            print("SJDebug StartCallAction transaction request successful")
            
            let callUpdate = CXCallUpdate()
            
            callUpdate.remoteHandle = callHandle
            callUpdate.supportsDTMF = true
            callUpdate.supportsHolding = false
            callUpdate.supportsGrouping = false
            callUpdate.supportsUngrouping = false
            callUpdate.hasVideo = false
            
            provider.reportCall(with: uuid, updated: callUpdate)
        }
    }
    
    
//    func performStartCallAction(uuid: UUID, handle: String) {
//        print("Customer side audio call AUDIOCALL :: \(#function)")
//        guard let provider = self.callKitProvider else {
//            print("CallKit provider not available")
//            return
//        }
//
//        //SJ_Change :
//        //let callHandle = CXHandle(type: .generic, value: "111122223333")
//        let callHandle = CXHandle(type: .generic, value: handle)
//
//        print("handle : \(handle)")
//
//        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
//        let transaction = CXTransaction(action: startCallAction)
//        DispatchQueue.main.async {
//            if self.callKitCallController == nil {
//                self.callKitCallController = CXCallController()
//            }
//            self.callKitCallController?.request(transaction) { error in
//                if let error = error {
//                    DispatchQueue.main.async {
//                        self.SocketForReject(reject: 1)
//                        self.dismissCallControllerScreen()
//                    }
//                    print("StartCallAction transaction request failed: \(error.localizedDescription)")
//                    return
//                }
//
//                print("StartCallAction transaction request successful")
//
//                let callUpdate = CXCallUpdate()
//
//                callUpdate.remoteHandle = callHandle
//                callUpdate.supportsDTMF = true
//                callUpdate.localizedCallerName = ""
//                callUpdate.supportsHolding = false
//                callUpdate.supportsGrouping = false
//                callUpdate.supportsUngrouping = false
//                callUpdate.hasVideo = false
//
//                provider.reportCall(with: uuid, updated: callUpdate)
//            }
//        }
//
//    }
    
    func reportIncomingCall(from: String, uuid: UUID) {
        print("Customer side audio call Audio :: \(#function)")
        guard let provider = self.callKitProvider else {
            print("CallKit provider not available")
            return
        }
        
        let callHandle = CXHandle(type: .generic, value: from)
        let callUpdate = CXCallUpdate()
        
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = true
        callUpdate.supportsHolding = false
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false
        
        provider.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
            if let error = error {
                print("Failed to report incoming call successfully: \(error.localizedDescription).")
            } else {
                print("Incoming call successfully reported.")
            }
        }
    }
    
    func performEndCallAction(uuid: UUID) {
        print("Customer side audio call Audio :: \(#function)")
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        DispatchQueue.main.async {
            self.dismissCallControllerScreen()
        }
        if self.callKitCallController == nil {
            self.callKitCallController = CXCallController()
        }
        self.callKitCallController?.request(transaction) { error in
            if let error = error {
                print("EndCallAction transaction request failed: \(error.localizedDescription).")
            } else {
                print("Ravi debug123,audiocallview")
                print("EndCallAction transaction request successful")
            }
        }
    }
    
    func performVoiceCall(uuid: UUID, client: String?, completionHandler: @escaping (Bool) -> Void) {
        print("Customer side audio call Audio :: \(#function)")
        guard let accessToken = fetchAccessToken() else {
            completionHandler(false)
            return
        }
//        let connectOptions = ConnectOptions(accessToken: accessToken, block: { builder in
//            builder.params = [self.twimlParamTo: self.CallToAdvisorName,"fname": "111000","lname":"New Value","from":"dvhgdhgvcsdvcjsadbkjsdb"]
//            builder.uuid = uuid
//        })
       
        let connectOptions = ConnectOptions(accessToken: accessToken) { builder in
            builder.params = [self.twimlParamTo: self.CallToAdvisorName,"customerId": "111000","lname":"New Value","From":"dvhgdhgvcsdvcjsadbkjsdb"]
            builder.uuid = uuid
            
        }
        
        let call = TwilioVoiceSDK.connect(options: connectOptions, delegate: self)
        self.activeCall = call
        self.activeCalls[call.uuid?.uuidString ?? UUID().uuidString] = call
        self.callKitCompletionCallback = completionHandler
    }
    
    func performAnswerVoiceCall(uuid: UUID, completionHandler: @escaping (Bool) -> Void) {
        print("Customer side audio call Audio :: \(#function)")
        guard let callInvite = activeCallInvites[uuid.uuidString] else {
            print("No CallInvite matches the UUID")
            return
        }
        
        let acceptOptions = AcceptOptions(callInvite: callInvite) { builder in
            builder.uuid = callInvite.uuid
        }
        
        let call = callInvite.accept(options: acceptOptions, delegate: self)
        self.activeCall = call
        self.activeCalls[call.uuid?.uuidString ?? UUID().uuidString] = call
        self.callKitCompletionCallback = completionHandler
        
        self.activeCallInvites.removeValue(forKey: uuid.uuidString)
        
        guard #available(iOS 13, *) else {
            incomingPushHandled()
            return
        }
    }
    func dismissCallControllerScreen() {
        if let provider = self.callKitProvider {
            provider.invalidate()
        }
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: AudioCallViewController.self) {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: nil)
                } else {
                    if isPickedUp != nil {
                        endSession()
                    }
                   
                    topVC.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}
// MARK: - AVAudioPlayerDelegate

extension AudioCallViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if flag {
            print("Audio player finished playing successfully");
        } else {
            print("Audio player finished playing with some error");
        }
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        if let error = error {
            print("Decode error occurred: \(error.localizedDescription)")
        }
    }
}
//MARK: - Timer for session
extension AudioCallViewController {
    
    func startTimer(withInterval interval: Double) {
        if internalTimer != nil {
            internalTimer?.invalidate()
        }
        // jobs.append(job)
        internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
        
    }
    
    func stopTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : Int(AdvisorID ?? "") ?? 0,
                           "booking_id" : Int(bookingID ?? "") ?? 0]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.stop_timer.rawValue , with: joinAdvisor)
        // jobs = [()->()]()
        internalTimer?.invalidate()
    }
    func stopReminderTimer() {
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: CommunicationReminerViewController.self) {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        
                    })
                    ResumeInternalTimer()
                }
            }
        }
        
        CommunicationScreenRemiderScreen = SecondsForReminderScreen
        TimerForReminderScreen.invalidate()
        print("Total Timer count is dont :: \(TotalTimecounter)")
    }
    @objc func updateTimerForReminderScreen() {
        if CommunicationScreenRemiderScreen != 0 {
            print("CommunicationScreenRemiderScreen :: \(CommunicationScreenRemiderScreen)")
            CommunicationScreenRemiderScreen -= 1     //This will decrement(count down)the seconds.
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommunicationReminerViewController.self) {
                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
                    let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
                    Controller.lblTimeLeft.text = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
                    Controller.lblChatReminderDescription.text = "Your audio session will be resume after \(String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)) minute!"
                }
            }
        } else {
            stopReminderTimer()
        }
    }
    func GoToReminderScreen() {
        
        if let topVC = UIApplication.topViewController() {
            
            let controllerForCommunication = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommunicationReminerViewController.storyboardID) as! CommunicationReminerViewController
            let navigationController = UINavigationController(rootViewController: controllerForCommunication)
            controllerForCommunication.SessionType = "voice session"
            let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
            controllerForCommunication.timePending = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
            controllerForCommunication.closourForResume = { [self] in
                stopReminderTimer()
                // print("Total Timer count is dont :: \(TotalTimecounter)")
            }
            controllerForCommunication.closourForBuyMinutes = { [self] in
                CommunicationScreenRemiderScreen = SecondsForReminderScreen
                TimerForReminderScreen.invalidate()
                
                if SingletonClass.sharedInstance.freeMinutesAdded.0 == true {
                    let RemoveFreeMinutes = totalMinutesTimer - (Int(SingletonClass.sharedInstance.freeMinutesAdded.1) ?? 0)
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(RemoveFreeMinutes) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                } else {
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                }
                //SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                controller.isBuyNow = true
                controller.closourForClickOnBack = {
                    ResumeInternalTimer()
                }
                controller.closourForBookRequest = {
                    self.dismiss(animated: true, completion: nil)
                    
                    self.totalMinutesTimer = self.totalMinutesTimer + (Int("\(controller.textFieldSelectMinutes.text ?? "")") ?? 1)
                    
                    ResumeInternalTimer()
                }
                
                controller.CommunicationType = SingletonClass.sharedInstance.dataForShareForBuyMinutes["typeName"] as? String ?? ""
                
                controller.AdvisorID = SingletonClass.sharedInstance.dataForShareForBuyMinutes["AdvisorID"] as? String ?? ""
                controller.CategoryID =  SingletonClass.sharedInstance.dataForShareForBuyMinutes["CategoryID"] as? String ?? ""
                controller.PriceOfSelectedCategory = SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? ""
                controller.BoookingID = bookingID ?? ""
                //                    controller.CommunicationType = DataForBuyAgain["typeName"] as? String ?? ""
                //
                //                    controller.AdvisorID = DataForBuyAgain["AdvisorID"] as? String ?? ""
                //
                //                    controller.PriceOfSelectedCategory = DataForBuyAgain["PriceOfSelectedCategory"] as? String ?? ""
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
            }
            
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            topVC.present(navigationController, animated: true, completion: nil)
            
        }
    }
    @objc func doJob() {
        let CheckTime = (totalMinutesTimer * 60) - 30
        if TotalTimecounter == CheckTime {
            self.stopTimer()
            CommunicationScreenRemiderScreen = SecondsForReminderScreen
            TimerForReminderScreen = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimerForReminderScreen)), userInfo: nil, repeats: true)
            
            GoToReminderScreen()
           // self.playSystemSound(systemSoundID: kSystemSoundID_Vibrate, count: 2)
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            })
            TotalTimecounter += 1
        } else if TotalTimecounter == (CheckTime + 30) {
            //   Utilities.ShowAlert(OfMessage: "Your session is complete")
            if isPickedUp != nil {
                endSession()
            }
//            endSession()
            
            print("you need to dismiss message screen")
            
            
        } else {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommunicationReminerViewController.self) {
                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
                    let timeLeft = secondsToHoursMinutesSeconds(seconds: (totalMinutesTimer * 60) - TotalTimecounter - 1)
                    Controller.lblTimeLeft.text = "\(timeLeft.1):\(timeLeft.2)"
                    Controller.lblChatReminderDescription.text = "Your audio session will be resume after \(timeLeft.1):\(timeLeft.2) minute!"
                } else if topVC.isKind(of: AudioCallViewController.self) {
                    
                    
                    let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter + 1)
                    lblTimer?.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                    
                }
                
            }
            print("go forward")
            
            TotalTimecounter += 1
        }
        
        print(totalMinutesTimer)
        print(TotalTimecounter)
        
        
    }
    func playSystemSound(systemSoundID: SystemSoundID, count: Int){
        
        AudioServicesPlaySystemSoundWithCompletion(systemSoundID) {
            if count > 0 {
                self.playSystemSound(systemSoundID: systemSoundID, count: count - 1)
            }
        }
        
    }
    func ResumeInternalTimer() {
        self.internalTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.doJob), userInfo: nil, repeats: true)
        
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : Int(AdvisorID ?? "") ?? 0,
                           "booking_id" : Int(bookingID ?? "") ?? 0]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.resume_timer.rawValue , with: joinAdvisor)
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (0, (seconds / 60), (seconds % 60))
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func endSession() {
        if IsEndSessionDone {
            
        } else {
            callToAdvisor()
            SingletonClass.sharedInstance.WalletBalanceForMaintain = ""
            if let topVC = UIApplication.topViewController() {
                
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        
                    })
                } else {
                    topVC.navigationController?.popViewController(animated: true)
                }
                
            }
            let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
            
            
            let DOBOfCustomer = convertDateFormater(SingletonClass.sharedInstance.loginForCustomer?.profile.dob ?? "")
            let param = [
                "user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                "advisor_id" : Int(AdvisorID ?? "") ?? 0,
                "booking_id" : Int(bookingID ?? "") ?? 0,
                "minute" :  String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2),
                "amount" : SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "",
                "dob" : DOBOfCustomer
                
            ] as [String : Any]
            
            print(param)
            
            SocketIOManager.shared.socketEmit(for: socketApiKeys.end_session.rawValue, with: param)
            self.stopTimerOnEndSession()
            IsEndSessionDone = true
        }
        
        
    }
    func stopTimerOnEndSession() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        internalTimer?.invalidate()
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: date)
        
    }
}
