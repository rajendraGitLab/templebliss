//
//  EditProfileViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage

protocol EditProfileDelegate {
    func refereshProfileScreen()
}

class EditProfileViewController: BaseViewController {

    //MARK: - Properties
    var delegateEdit : EditProfileDelegate!
    private var imagePicker : ImagePicker!
    var selectedImage : UIImage?
    var customTabBarController : CustomTabBarVC?
    var isRemovePhoto = false
    
    var selectedOldImage : UIImage?
    //MARK: - IBOutlets
    
    @IBOutlet weak var btnUpdatePicture: UIButton!
    @IBOutlet weak var btnSave: theamSubmitButton!
    
    @IBOutlet weak var userImageView: myProfileImageView!{ didSet{ userImageView.layer.cornerRadius = userImageView.frame.size.height / 2}}
    @IBOutlet weak var textFieldFullName: EditProfileTextField!
    @IBOutlet weak var textFieldEmailAddress: EditProfileTextField!
    @IBOutlet weak var TextFieldPhoneNumber: EditProfileTextField!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
       
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Edit Profile", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        self.imagePicker = ImagePicker(presentationController: self, delegate: self, allowsEditing: true)
        
        textFieldEmailAddress.isUserInteractionEnabled = false
        textFieldEmailAddress.superview?.alpha = 0.6
        
        TextFieldPhoneNumber.isUserInteractionEnabled = false
        TextFieldPhoneNumber.superview?.alpha = 0.6
        
        setValue()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        textFieldFullName.placeholder = "Nick Name"
        if let userdata = SingletonClass.sharedInstance.loginForCustomer{
            textFieldFullName.text = userdata.profile.fullName ?? ""
            textFieldEmailAddress.text = userdata.profile.email ?? ""
            TextFieldPhoneNumber.text = userdata.profile.phone
            userImageView.image = UIImage(named: "user_dummy_profile")
            if SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(userdata.profile.profilePicture ?? "")"
                userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
                userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
                selectedImage = userImageView.image ?? UIImage()
                selectedOldImage = selectedImage
            }
        }
    }
    //MARK: - IBActions
   
    @IBAction func btnSaveClick(_ sender: theamSubmitButton) {
//        self.navigationController?.popViewController(animated: true)
        if !textFieldFullName.isEnabled{
            (sender as AnyObject).setImage(#imageLiteral(resourceName: "imgUpdateDone"), for: .normal)
        }else{
            if validation(){
        
                if SingletonClass.sharedInstance.loginForCustomer?.profile.fullName != textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" {
                    
                    
                    webserviceForEditprofile()
                } else {
                    if selectedOldImage == nil && isRemovePhoto {
                         self.navigationController?.popViewController(animated: true)
                    } else {
                        if selectedOldImage != nil && isRemovePhoto {
                            webserviceForEditprofile()
                        } else if selectedOldImage == selectedImage {
                            self.navigationController?.popViewController(animated: true)
                        } else if (selectedOldImage == selectedImage) == false {
                            webserviceForEditprofile()
                        } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
                            webserviceForEditprofile()
                        } else {
                             self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
          
            }
        }
    }
    @IBAction func btnChangeUserProfileClick(_ sender: UIButton) {
        resignFirstResponder()
        if (self.userImageView.image != nil || self.selectedImage != nil) && ((self.userImageView.image?.isEqualToImage(UIImage.init(named: "user_dummy_profile")!)) != nil){
            self.imagePicker.present(from: self.userImageView, viewPresented: self.view, isRemove: true)
        } else {
            self.imagePicker.present(from: self.userImageView, viewPresented: self.view, isRemove: false)
        }
        
     
    }
    //MARK: - API Calls
    func validation()->Bool{
        let FullName = textFieldFullName.validatedText(validationType: ValidatorType.username(field: "nick name") )//ValidatorType.requiredField(field: "first name"))
        let Email = textFieldEmailAddress.validatedText(validationType: ValidatorType.email)
        let phone = TextFieldPhoneNumber.validatedText(validationType: ValidatorType.requiredField(field: "contact number"))
        
        if (!FullName.0){
            Utilities.ShowAlert(OfMessage: FullName.1)
            return FullName.0
        }else if (!Email.0){
            Utilities.ShowAlert(OfMessage: Email.1)
            return Email.0
        }
        else if (!phone.0){
            Utilities.ShowAlert(OfMessage: phone.1)
            return phone.0
        }
        
        return true
    }
    
    
    
    func webserviceForEditprofile(){
        let updateModel = EditProfileReqModel()
        updateModel.full_name = textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        updateModel.email = textFieldEmailAddress.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        updateModel.phone = TextFieldPhoneNumber.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        if self.isRemovePhoto{
            updateModel.remove_image = "1"
        } else {
            updateModel.remove_image = "0"
        }
        var updestring:[String] = []
        if SingletonClass.sharedInstance.loginForCustomer?.profile.firstName != textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "" {
            updestring.append("full_name")
        }
        
        
//
//        if selectedOldImage == nil && isRemovePhoto {
//           // self.navigationController?.popViewController(animated: true)
//        } else {
        if selectedOldImage == nil && isRemovePhoto {
            // self.navigationController?.popViewController(animated: true)
        } else {
            if selectedOldImage != nil && isRemovePhoto {
                updestring.append("profile_picture")
            } else if selectedOldImage == selectedImage {
                
            } else if (selectedOldImage == selectedImage) == false {
                updestring.append("profile_picture")
            } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
                updestring.append("profile_picture")
            } else {
            }
        }
//            if selectedOldImage == nil && isRemovePhoto {
//                
//            } else {
//                if selectedOldImage != nil && isRemovePhoto {
//                   // setRequestMdoel()
//                    updestring.append("profile_picture")
//                } else if selectedOldImage == selectedImage {
//                    
//                } else if (selectedOldImage == selectedImage) == false {
//                   // setRequestMdoel()
//                    updestring.append("profile_picture")
//                } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
//                    //setRequestMdoel()
//                    updestring.append("profile_picture")
//                } else {
//                    
//                }
//            }
            
            
            
//            if selectedOldImage != nil && isRemovePhoto {
//                updestring.append("profile_picture")
//            } else if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
//                updestring.append("profile_picture")
//            } else {
//               // self.navigationController?.popViewController(animated: true)
//            }
            
   //     }
        
        
        
//        if ((selectedOldImage?.isEqualToImage(selectedImage ?? UIImage())) != nil) == false {
//            updestring.append("profile_picture")
//        }
        updateModel.is_change = updestring.map{String($0)}.joined(separator: ",")
        updateModel.user_id = SingletonClass.sharedInstance.UserId
        Utilities.showHud()
        
        WebServiceSubClass.UpdateProfileCustomer(editProfileModel: updateModel, img: selectedImage ?? UIImage(), isRemoveImage: self.isRemovePhoto, showHud: true, completion: { (response, status, error) in
            Utilities.hideHud()
             if status{
                
                let loginModel = userInforForCustomer.init(fromJson: response)
                let loginModelDetails = loginModel
                SingletonClass.sharedInstance.loginForCustomer = loginModelDetails
                 userDefault.setUserDataForCustomer(objProfile: loginModelDetails)
                self.isRemovePhoto = false
                 Utilities.displayAlert("", message: response["message"].string ?? "", completion: {_ in
                    
                    self.navigationController?.popViewController(animated: true)
                    self.delegateEdit.refereshProfileScreen()
                 }, otherTitles: nil)
             }else{
                 Utilities.showAlertOfAPIResponse(param: error, vc: self)
             }
         })
    }
}

extension EditProfileViewController:ImagePickerDelegate {
    
    func didSelect(image: UIImage?, SelectedTag:Int) {

            if(image == nil && SelectedTag == 101){
                self.selectedImage = UIImage()
                self.isRemovePhoto = true
                self.userImageView.image = UIImage.init(named: "user_dummy_profile")
            }else if image != nil{
                DispatchQueue.main.async {
                    let fixedOrientedImage = image?.fixOrientation()
                   self.userImageView.image = fixedOrientedImage
                    self.userImageView.setNeedsDisplay()
                    self.selectedImage = fixedOrientedImage
                }
                self.isRemovePhoto = false
                self.userImageView.layoutSubviews()
                self.userImageView.layoutIfNeeded()
                
                
                
            }else{
                return
            }
    }
}
