//
//  addPaymentVC.swift
//  Adelante
//
//  Created by Apple on 07/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import FormTextField
import PassKit

class addPaymentVC: BaseViewController ,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    
    //MARK: - IBOutlets
    @IBOutlet weak var AddMoneyTextField: RegisterTextField!
    @IBOutlet weak var tblPaymentMethod: UITableView!
    @IBOutlet weak var btnAddCard: theamSubmitButton!
    @IBOutlet weak var applePayView: UIView!
    
    //MARK: - Properties
    let paymentHandler = ApplePaymentHandler()
    var MoneyToAdded = ""
    var bookingID = ""
    
    var isFromBuyMinutes = false
    var bookingData : [String:Any] = [:]
    var isFromBuyMoreMinutes = false
    // var cardDetails : [String] = []
    var cardDetailsData : MysavedCardsResModel?
    var selectedPaymentMethods = 0
    //var selectedIndexOfPayment = -1
    var direction:CGFloat = 1
    var shakes = 0
    
    //MARK: - View Life Cycle Methods
    @objc func GetCardData(){
        GetMyCardDetails()
       }
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        backButtonClick = {
            
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "GetCardData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetCardData), name: NSNotification.Name(rawValue: "GetCardData"), object: nil)
        GetMyCardDetails()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Select Payment Method", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        if isFromBuyMinutes {
            AddMoneyTextField.isUserInteractionEnabled = false
            AddMoneyTextField.superview?.alpha = 0.6
        } else {
            AddMoneyTextField.isUserInteractionEnabled = true
            AddMoneyTextField.superview?.alpha = 1.0
        }
        AddMoneyTextField.delegate = self
        AddMoneyTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        tblPaymentMethod.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
        setAfter {
            self.setupApplePay()
        }
        
    }
    
    @objc func textFieldDidChange() {
       

        if AddMoneyTextField.text != "" {
            if let amountString = AddMoneyTextField.text?.currencyInputFormatting() {
                AddMoneyTextField.text = amountString
                
            }
        }
        
    }
//    func textFieldDidChangeSelection(_ textField: UITextField) {
//        if textField.text != "" {
//            if let amountString = textField.text?.currencyInputFormatting() {
//                textField.text = amountString
//
//            }
//        }
//    }
    override func viewWillAppear(_ animated: Bool) {
        
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        // lblTitle.text = "AddCardVC_lblPaymentMethod".Localized()
        AddMoneyTextField.placeholder = "\(Currency) Enter an amount"
        btnAddCard.setTitle("ADD CARD", for: .normal)
        
        AddMoneyTextField.text = String(format: "%.2f", ((MoneyToAdded as NSString?)?.floatValue) ?? 0.00).currencyInputFormatting()
    }
   
    func shake(_ theOneYouWannaShake: UIView?) {
        UIView.animate(withDuration: 0.06, animations: {
            theOneYouWannaShake?.transform = CGAffineTransform(translationX: 5 * self.direction, y: 0)
        }) { [self] finished in
            
            if shakes >= 10 {
                theOneYouWannaShake?.transform = CGAffineTransform.identity
                return
            }
            shakes += 1
            direction = direction * -1
            shake(theOneYouWannaShake)
        }
    }
    //MARK: - btnAction
    @IBAction func placeOrderBtn(_ sender: theamSubmitButton) {
        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_addCard)
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AddCardVC.storyboardID)
        self.navigationController?.pushViewController(controller, animated: true)
      
    }
    
    //MARK: - tblViewMethods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        switch section {
        case 0:
            if cardDetailsData?.cards.count != 0 {
                return cardDetailsData?.cards.count ?? 0
            } else {
                return 1
            }
           
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if cardDetailsData?.cards.count != 0 {
        switch indexPath.section {
        case 0:
            var cell = UITableViewCell()
            
            let cell2 = tblPaymentMethod.dequeueReusableCell(withIdentifier: paymentMethodCell2.reuseIdentifier, for: indexPath) as! paymentMethodCell2
            cell2.vWMain.layer.borderColor = colors.white.value.cgColor
            if indexPath.row == selectedPaymentMethods {
                cell2.vWMain.layer.borderWidth = 0
                cell2.selectPaymentMethodButton.isHidden = true
                cell2.viewForEnterCVV.isHidden = false
                cell2.labelInValidCVVLabel.text = ""
            } else {
                cell2.vWMain.layer.borderWidth = 0
                cell2.selectPaymentMethodButton.isHidden = true
                cell2.viewForEnterCVV.isHidden = true
                cell2.labelInValidCVVLabel.text = ""
            }
            
            cell2.lblExpiresDate.text = "Expires \(cardDetailsData?.cards?[indexPath.row].displayExpiryDate ?? "")"
            
            switch cardDetailsData?.cards?[indexPath.row].cardType {
            case "1":
                cell2.paymentMethodImageView.image = UIImage(named: "ic_dummyVisa")
            case "2":
                cell2.paymentMethodImageView.image = UIImage(named: "ic_dummyMasterCard")
            case "3":
                cell2.paymentMethodImageView.image = UIImage(named: "ic_dummyAExpress")
            case "4":
                cell2.paymentMethodImageView.image = UIImage(named: "")
            case "5":
                cell2.paymentMethodImageView.image = UIImage(named: "")
            case "6":
                cell2.paymentMethodImageView.image = UIImage(named: "ic_dummyDiscover")
                
            default:
                break
            }
            
            cell2.closourForPay = {
                if self.AddMoneyTextField.text == "" {
                    Utilities.ShowAlert(OfMessage: "Please enter amount")
                } else  if cell2.textFieldEnterCVV.text?.trim() != self.cardDetailsData?.cards?[indexPath.row].originalCvv {
                    self.direction = 1
                    self.shakes = 0
                    cell2.textFieldEnterCVV.text = ""
                    self.shake(cell2.textFieldEnterCVV)
                    cell2.labelInValidCVVLabel.text = ""
                }
                else {
                    self.AddAmountDetails(cardID: self.cardDetailsData?.cards?[indexPath.row].id ?? "")
                }
            }
            cell2.lblcardDetails.text = cardDetailsData?.cards?[indexPath.row].displayNumber
            cell = cell2
            
            return cell
        default:
            return UITableViewCell()
        }
    } else {
        let NoDatacell = tblPaymentMethod.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
        
        NoDatacell.imgNoData.image = UIImage(named: "CreditCardIcon")
        NoDatacell.lblNoDataTitle.text = "No card found"
        
        return NoDatacell
    }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if cardDetailsData?.cards.count != 0 {
                return UITableView.automaticDimension
            } else {
                return tableView.frame.size.height
            }
           
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if cardDetailsData?.cards.count != 0 {
            return 50
        } else {
            return 0
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tblPaymentMethod.dequeueReusableCell(withIdentifier: WalletHistoryHeaderCell.reuseIdentifier) as! WalletHistoryHeaderCell
        cell.lblheader.text = ""
        if cardDetailsData == nil {
            cell.lblheader.text = ""
        } else {
            if cardDetailsData?.cards.count == 0 {
                cell.lblheader.text = ""
            } else {
                cell.lblheader.text = "Choose desired payment method"
            }
        }
        
        
      
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            if cardDetailsData?.cards.count != 0 {
                selectedPaymentMethods = indexPath.row
                tblPaymentMethod.reloadData()
            }
           
            
        default:
            break
        }
        
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            DeleteDardDetails(cardID: cardDetailsData?.cards?[indexPath.row].id ?? "")
            // handle delete (by removing the data from your array and updating the tableview)
        }
    }
    //MARK: - API Calls
   
}
class WalletHistoryHeaderCell : UITableViewCell {
    @IBOutlet weak var lblheader : walletHistoryLabel!
   
    override func awakeFromNib() {
        lblheader.text = ""
    }
}
class paymentMethodCell1 : UITableViewCell {
    @IBOutlet weak var vWMain: PaymentView!
    @IBOutlet weak var lblWallet: addPaymentlable!
    @IBOutlet weak var lblwalletBalance: addPaymentlable!
    @IBOutlet weak var paymentImageView: UIImageView!
}
class paymentMethodCell2 : UITableViewCell {
    
    var closourForPay : (() -> ())?
    var validation = Validation()
    var inputValidator = InputValidator()
    
    @IBOutlet weak var textFieldEnterCVV: EnterCVVTextField!
    @IBOutlet weak var viewForEnterCVV: viewViewClearBG!
    
    @IBOutlet weak var btnPay: theamSubmitButton!
    
    @IBOutlet weak var labelInValidCVVLabel: InValidCVVLabel!
    
    
    @IBOutlet weak var vWMain: PaymentView!
    @IBOutlet weak var selectPaymentMethodButton: UIButton!
    @IBOutlet weak var paymentMethodImageView: UIImageView!
    @IBOutlet weak var lblExpiresDate: addPaymentlable!
    @IBOutlet weak var lblcardDetails: addPaymentlable!
    
    @IBAction func btnPayClick(_ sender: theamSubmitButton) {
        if let click = self.closourForPay {
            click()
        }
        
    }
    override func awakeFromNib() {
        textFieldEnterCVV.placeholder = "Enter CVV"
        textFieldEnterCVV.text = ""
        CvvValidation()
    }
    func CvvValidation() {
        textFieldEnterCVV.isSecureTextEntry = true
        textFieldEnterCVV.inputType = .integer
        
        self.validation.maximumLength = 4
        self.validation.minimumLength = 3
        
        validation.characterSet = NSCharacterSet.decimalDigits
        let inputValidator = InputValidator(validation: validation)
        textFieldEnterCVV.inputValidator = inputValidator
    }
}
class addPaymentlable : UILabel {
    @IBInspectable var isWallet : Bool = false
    @IBInspectable var isWalletBalance : Bool = false
    @IBInspectable var isCardNumber : Bool = false
    @IBInspectable var isExpires : Bool = false
    override func awakeFromNib() {
        if isWallet {
            self.font = CustomFont.bold.returnFont(18)
            self.textColor = colors.white.value
            self.textAlignment = .left
        } else if isWalletBalance {
            self.font = CustomFont.medium.returnFont(20)
            self.textColor = colors.white.value
            self.textAlignment = .left
        }  else if isCardNumber {
            self.font = CustomFont.regular.returnFont(18)
            self.textColor = colors.white.value
            self.textAlignment = .left
        } else if isExpires {
            self.font = CustomFont.regular.returnFont(13)
            self.textColor = colors.white.value
            self.textAlignment = .left
        }
        
    }
}
class PaymentView : UIView {
    override func awakeFromNib() {
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
        
        self.layer.masksToBounds = false
        
        self.layer.shadowColor = colors.black.value.cgColor
        self.layer.shadowOffset = CGSize(width: -1, height: 1.0)
        self.layer.shadowOpacity = 0.16
        self.layer.shadowRadius = 3.0
    }
}
extension addPaymentVC {
    func GetMyCardDetails() {
        let getMyCardsReqModel = GetMyCardsReqModel()
        getMyCardsReqModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        webserviceForGetMycards(reqModel: getMyCardsReqModel)
    }
    func webserviceForGetMycards(reqModel:GetMyCardsReqModel) {
        Utilities.showHud()
        WebServiceSubClass.GetMyCards(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                let ResponseModel = MysavedCardsResModel.init(fromJson: json)
                self.cardDetailsData = ResponseModel
                
                self.tblPaymentMethod.reloadData()
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
        
    }
    
    func DeleteDardDetails(cardID : String) {
        Utilities.showHud()
        let getMyCardsReqModel = DeleteCardsReqModel()
        getMyCardsReqModel.card_id = cardID
        webserviceForDeleteMycards(reqModel: getMyCardsReqModel)
    }
    func webserviceForDeleteMycards(reqModel:DeleteCardsReqModel) {
        WebServiceSubClass.DeleteCards(addCategory: reqModel, completion: {(json, status, response) in
            
            if status {
                self.GetMyCardDetails()
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
    func AddAmountDetails(cardID : String) {
        let amount:Double = Double(getAmountFromTextField()) ?? 0.00
        
        if amount < 1.00 {
            AddMoneyTextField.isUserInteractionEnabled = true
            AddMoneyTextField.superview?.alpha = 1.0
            
         
            let alert = UIAlertController(title: AppInfo.appName , message: "You need to add minimum $1.00", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let addAmountModel = AddAmountReqModel()
            addAmountModel.card_id = cardID
            addAmountModel.amount = getAmountFromTextField()
            if self.isFromBuyMoreMinutes {
                
            }
            webserviceForAddAmount(reqModel: addAmountModel)
        }
        
        
    }
    
    private func getAmountFromTextField() -> String {
        return (AddMoneyTextField.text?.replacingOccurrences(of: "\(Currency)", with: "") ?? "").replacingOccurrences(of: ",", with: "")
    }
    
    fileprivate func paymentSuccessFunc(_ message: String) {
        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_addMoney)
        let alert = UIAlertController(title: AppInfo.appName , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            
            
            self.navigationController?.popViewController(animated: true)
            if let topVC = UIApplication.topViewController() {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        
                        if self.isFromBuyMinutes {
                            if self.isFromBuyMoreMinutes {
                                SocketIOManager.shared.socketEmit(for: socketApiKeys.buy_more_minute.rawValue, with: self.bookingData)
                                if let topVC = UIApplication.topViewController() {
                                    if topVC.isKind(of: CustomerVideoCallViewController.self) {
                                        let vc = topVC as! CustomerVideoCallViewController
                                        vc.totalMinutesTimer = vc.totalMinutesTimer + (Int("\(self.bookingData["minute"] ?? "")") ?? 1)
                                        
                                        vc.ResumeInternalTimer()
                                    } else if topVC.isKind(of: AudioCallViewController.self) {
                                        let vc = topVC as! AudioCallViewController
                                        vc.totalMinutesTimer = vc.totalMinutesTimer + (Int("\(self.bookingData["minute"] ?? "")") ?? 1)
                                        
                                        vc.ResumeInternalTimer()
                                    } else {
                                        if let navControllers = topVC.navigationController?.children {
                                            for controllers in navControllers {
                                                if controllers.isKind(of: AdviserDetailsViewController.self) {
                                                    let vc = controllers as! AdviserDetailsViewController
                                                    vc.totalMinutesTimer = vc.totalMinutesTimer + (Int("\(self.bookingData["minute"] ?? "")") ?? 1)
                                                    
                                                    vc.ResumeInternalTimer()
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            }  else {
                                
                                SocketIOManager.shared.socketEmit(for: socketApiKeys.booking_request.rawValue, with: self.bookingData)
                                if self.bookingData["type"] as? String ?? "" == "Chat" || self.bookingData["type"] as? String ?? "" == "chat" {
                                    let CommonLabelController:CommonLabelPopupViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonLabelPopupViewController.storyboardID) as! CommonLabelPopupViewController
                                    
                                    CommonLabelController.textForShow = "Please wait for advisor approval"
                                    let navigationController = UINavigationController(rootViewController: CommonLabelController)
                                    navigationController.modalPresentationStyle = .overCurrentContext
                                    navigationController.modalTransitionStyle = .crossDissolve
                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                } else if self.bookingData["type"] as? String ?? "" == "Audio" || self.bookingData["type"] as? String ?? "" == "audio" {
                                    
                                    self.GoToAudioCallScreen(DataForRequestPush: SingletonClass.sharedInstance.DataForAudioVideo)
                                } else if self.bookingData["type"] as? String ?? "" == "Video" || self.bookingData["type"] as? String ?? "" == "video" {
                                    
                                    self.GoToVideoCallScreen(DataForRequestPush: SingletonClass.sharedInstance.DataForAudioVideo)
                                }
                                
                            }
                        }else {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DataRefresh"), object: nil, userInfo: nil)
                            self.navigationController?.popViewController(animated: true)
                        }
                    })
                } else {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DataRefresh"), object: nil, userInfo: nil)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func webserviceForAddAmount(reqModel:AddAmountReqModel) {
        Utilities.showHud()
        WebServiceSubClass.AddAmount(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                let resModel = WalletHistoryResModel.init(fromJson: json)
                print("After add money to wallet current balance is \(resModel.walletAmount ?? "0")")
                SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = resModel.walletAmount ?? "0"
                userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DataRefresh"), object: nil, userInfo: nil)
                self.paymentSuccessFunc(json["message"].string ?? "")
                
            } else {
                if let strMessage = json["message"].string {
                    Utilities.showAlertOfAPIResponse(param: strMessage, vc: self)
                }else {
                    Utilities.showAlertOfAPIResponse(param: "Something went wrong", vc: self)
                }
            }
            
            
        })
        
    }
    //MARK: -  Navigation to Audio video
    func GoToVideoCallScreen(DataForRequestPush:[String:Any]) {

        
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {

                    print("NEW" + #function)


                    if let topController = UIApplication.topViewController() {
                        let controller:CustomerVideoCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomerVideoCallViewController.storyboardID) as! CustomerVideoCallViewController
                        
                        print("Data For Request Push :: \(DataForRequestPush["CallingTo"] as? String ?? "")")
                        print("Data For Request Push :: \(DataForRequestPush["receiverId"] as? String ?? "")")
                        controller.CallingTo = DataForRequestPush["CallingTo"] as? String ?? ""
                        //self.selectedAdviserDetails?.nickName ?? ""
                        controller.senderName = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? ""
                        controller.strRoomName = ""
                        //               vc.visit_id = visit_id
                        controller.token = ""
                        controller.visitID = ""
                        controller.receiverId = DataForRequestPush["receiverId"] as? String ?? ""
                        //self.selectedAdviserDetails?.id ?? ""
                        controller.senderId = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                        controller.totalMinutes = "0"
                        controller.navigationController?.isNavigationBarHidden = true
                
                        topController.navigationController?.pushViewController(controller, animated: true)
                    }


                })
            } else {
                if let topController = UIApplication.topViewController() {
                    let controller:CustomerVideoCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomerVideoCallViewController.storyboardID) as! CustomerVideoCallViewController
                    
                    print("Data For Request Push :: \(DataForRequestPush["CallingTo"] as? String ?? "")")
                    print("Data For Request Push :: \(DataForRequestPush["receiverId"] as? String ?? "")")
                    controller.CallingTo = DataForRequestPush["CallingTo"] as? String ?? ""
                    //self.selectedAdviserDetails?.nickName ?? ""
                    controller.senderName = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? ""
                    controller.strRoomName = ""
                    //               vc.visit_id = visit_id
                    controller.token = ""
                    controller.visitID = ""
                    controller.receiverId = DataForRequestPush["receiverId"] as? String ?? ""
                    //self.selectedAdviserDetails?.id ?? ""
                    controller.senderId = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                    controller.totalMinutes = "0"
                    controller.navigationController?.isNavigationBarHidden = true
            
                    topController.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    func GoToAudioCallScreen(DataForRequestPush:[String:Any]) {

        if let topVC = UIApplication.topViewController() {
            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {

                    print("NEW" + #function)
                    if let topController = UIApplication.topViewController() {
                        let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
                        
                        controller.CallToAdvisorName = "\((DataForRequestPush["advisor_name"] as? String ?? "").replacingOccurrences(of: " ", with: "_"))_\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                        
                        controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"

                        controller.advisorName = DataForRequestPush["advisor_name"] as? String ?? ""
                        controller.advisorUserImageURl = DataForRequestPush["advisor_profile_picture"] as? String ?? ""

                        controller.bookingID = "\(DataForRequestPush["booking_id"] as? Int ?? 0)"
                        controller.AdvisorID = "\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                        controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                        controller.totalMinutes = DataForRequestPush["minute"] as? String ?? ""
                        
                        topController.navigationController?.pushViewController(controller, animated: true)
                    }
                })
            } else {
                if let topController = UIApplication.topViewController() {
                    let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController

                    controller.CallToAdvisorName = "\((DataForRequestPush["advisor_name"] as? String ?? "").replacingOccurrences(of: " ", with: "_"))_\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                    controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"

                    controller.advisorName = DataForRequestPush["advisor_name"] as? String ?? ""
                    controller.advisorUserImageURl = DataForRequestPush["advisor_profile_picture"] as? String ?? ""

                    controller.bookingID = "\(DataForRequestPush["booking_id"] as? Int ?? 0)"
                    controller.AdvisorID = "\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                    controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                    controller.totalMinutes = DataForRequestPush["minute"] as? String ?? ""
                    topController.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
}

// MARK: - Apple Pay
extension addPaymentVC {
    func setupApplePay() {
        let result = ApplePaymentHandler.applePayStatus()
        var button: UIButton?

        if result.canMakePayments {
            if #available(iOS 14.0, *) {
                button = PKPaymentButton(paymentButtonType: .addMoney , paymentButtonStyle: .white)
            } else {
                // Fallback on earlier versions
                button = PKPaymentButton(paymentButtonType: .plain , paymentButtonStyle: .white)
            }
            button?.addTarget(self, action: #selector(addPaymentVC.payPressed), for: .touchUpInside)
        } else if result.canSetupCards {
            button = PKPaymentButton(paymentButtonType: .setUp, paymentButtonStyle: .white)
            button?.addTarget(self, action: #selector(addPaymentVC.setupPressed), for: .touchUpInside)
        }

        if let applePayButton = button {
            let constraints = [
                applePayButton.centerXAnchor.constraint(equalTo: applePayView.centerXAnchor),
                applePayButton.centerYAnchor.constraint(equalTo: applePayView.centerYAnchor),
                applePayButton.widthAnchor.constraint(equalTo: applePayView.widthAnchor),
                applePayButton.heightAnchor.constraint(equalTo: applePayView.heightAnchor)
            ]
            applePayButton.translatesAutoresizingMaskIntoConstraints = false
            applePayView.addSubview(applePayButton)
            NSLayoutConstraint.activate(constraints)
            applePayButton.cornerRadius = 12
        }
    }
    
    @objc func payPressed(sender: AnyObject) {
        if self.AddMoneyTextField.text == "" {
            Utilities.ShowAlert(OfMessage: "Please enter amount")
            return
        }
        self.AddMoneyTextField.resignFirstResponder()
        let amount = Double(getAmountFromTextField()) ?? 0.0
        if amount < 1.00 {
            AddMoneyTextField.isUserInteractionEnabled = true
            AddMoneyTextField.superview?.alpha = 1.0
            
            let alert = UIAlertController(title: AppInfo.appName , message: "You need to add minimum $1.00", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            Utilities.showHud()
            paymentHandler.startPayment(amount: getAmountFromTextField()) { (success) in
                Utilities.hideHud()
                if success {
                    self.paymentSuccessFunc("Payment completed")
                } else {
                    Utilities.showAlertOfAPIResponse(param: "Payment failed", vc: self)
                }
            }
        }
    }

    @objc func setupPressed(sender: AnyObject) {
        let passLibrary = PKPassLibrary()
        passLibrary.openPaymentSetup()
    }
}
