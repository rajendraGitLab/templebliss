//
//  AddCardVC.swift
//  Adelante
//
//  Created by Apple on 07/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import FormTextField

class AddCardVC: BaseViewController, FormTextFieldDelegate{
    
    //MARK: - Properties
    var isCreditCardValid = Bool()
    var pickerView = UIPickerView()
    var aryMonth = [String]()
    var aryYear = [String]()
    var strMonth = ""
    var strYear = ""
    var cardTypeLabel = String()
    var validation = Validation()
    var inputValidator = InputValidator()
    
    var creditCardValidator: CreditCardValidator?
    
    var SelectedCardType = "0"
    var zipCode_str = String()
    //MARK: - IBOutlets
    
   
    @IBOutlet weak var LblName : addCardLabel!
    @IBOutlet weak var LblCreditCardNumber : addCardLabel!
    @IBOutlet weak var LblExpires : addCardLabel!
    @IBOutlet weak var LblCVV : addCardLabel!
    @IBOutlet weak var LblCountry : addCardLabel!
    @IBOutlet weak var LblzipCode : addCardLabel!
    @IBOutlet weak var LblDebitCardAreAcceptedDescription : addCardLabel!
    
    @IBOutlet weak var TextFieldName : addCarddetailsTextField!
    @IBOutlet weak var TextFieldCreditCardNumber : addCarddetailsTextField!
    @IBOutlet weak var TextFieldZipCode : addCarddetailsTextField!
    @IBOutlet weak var TextFieldExpires : addCarddetailsTextField!
    @IBOutlet weak var TextFieldCVV : addCarddetailsTextField!
    @IBOutlet weak var TextFieldCountry : addCarddetailsTextField!
    @IBOutlet weak var btnSave : theamSubmitButton!

    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Add Card", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        
        TextFieldExpires.delegate = self
        pickerView.delegate = self
        TextFieldZipCode.delegate = self
        pickerSetup()
        
        creditCardValidator = CreditCardValidator()
        cardNum()
        SetValidation()
        
        setLocalization()
        
        TextFieldName.validTextColor = colors.white.value
        //    NavbarrightButton()
     
        
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func pickerSetup() {
        let calendar = Calendar.current
        let currentYear = calendar.component(.year, from: Date())
        aryYear = (currentYear...(currentYear + 15)).map { String($0) }
        aryMonth = ["01","02","03","04","05","06","07","08","09","10","11","12"]
        
    }
    func setLocalization() {
        
        TextFieldName.text = ""
        TextFieldCreditCardNumber.text = ""
        TextFieldZipCode.text = ""
        TextFieldExpires.text = ""
        TextFieldCVV.text = ""
        TextFieldCountry.text = ""
        
        LblName.text = "NAME"
        LblCreditCardNumber.text = "CREDIT CARD NUMBER"
        LblExpires.text = "EXPIRES"
        LblCVV.text = "CVV"
        LblCountry.text = "COUNTRY"
        LblzipCode.text = "ZIPCODE"
        LblDebitCardAreAcceptedDescription.text = "Debit cards are accepted at some locations and for some categories."
        TextFieldName.placeholder = "Enter card holder name"
        TextFieldCreditCardNumber.placeholder = "Enter credit card number"
        TextFieldZipCode.placeholder = "Zip code"
        TextFieldExpires.placeholder = "Expiry date"
        TextFieldCVV.placeholder = "Enter CVV"
        TextFieldCountry.placeholder = "Enter country"
        btnSave.setTitle("SAVE", for: .normal)
        
    }

    //MARK: - IBActions
    
    func validate() -> Bool {
      
            let cardHolder = TextFieldName.validatedText(validationType: ValidatorType.cardHolder)
            let cardNumber = TextFieldCreditCardNumber.validatedText(validationType: ValidatorType.cardNumber)
            let expDate =  TextFieldExpires.validatedText(validationType: ValidatorType.expDate)
            let cvv = TextFieldCVV.validatedText(validationType: ValidatorType.cvv)
            
         
        if(!cardHolder.0)
        {
            showAlert(message: cardHolder.1)
           
            return cardHolder.0
        }
        else  if(!cardNumber.0)
        {
            showAlert(message: cardNumber.1)
            //Utilities.ShowAlert(OfMessage: cardNumber.1)
            return cardNumber.0
        }
        else  if(!expDate.0)
        {
            showAlert(message: expDate.1)
            //Utilities.ShowAlert(OfMessage: expDate.1)
            return expDate.0
        }
        else  if(!cvv.0)
        {
            showAlert(message: cvv.1)
            //Utilities.ShowAlert(OfMessage: cvv.1)
            return cvv.0
        }
        return true
        
    }
    @IBAction func placeOrderBtn(_ sender: theamSubmitButton) {
        
        if validate() {
            Utilities.showHud()
            SaveCard()
        }
        
//        if isValidatePaymentDetail().0 {
//          
//            
////            webservices
//        }else {
////            utility
//        }
     

           }
    @IBAction func txtCardNumberEditingChange(_ sender: UITextField) {
        if let number = sender.text {
                  if number.isEmpty {
                      isCreditCardValid = false
                      //                   self.btnVisa.isSelected = false
                      //                   self.btnMasterCard.isSelected = false
                      //                   self.btnAmerican.isSelected = false
                      //                   self.btnJCB.isSelected = false
                      //                   self.btnDiscover.isSelected = false
                      //                   self.btnDiner.isSelected = false
                      self.TextFieldCreditCardNumber.textColor = colors.white.value
                  } else {
                      validateCardNumber(number: number)
                      detectCardNumberType(number: number)
                  }
              }
    }
    //MARK: - Validation
    
    func detectCardNumberType(number: String) {
        if let type = creditCardValidator?.type(from: number) {
            isCreditCardValid = true
            self.cardTypeLabel = type.name.lowercased()
            print(type.name.lowercased())
                        
            self.TextFieldCreditCardNumber.textColor = colors.white.value
            self.CvvValidation()
        } else {
            isCreditCardValid = false
            self.cardTypeLabel = "Undefined"
        }
    }
    func validateCardNumber(number: String) {
        if ((creditCardValidator?.validate(string: number)) != nil) {
               isCreditCardValid = true
           } else {
               isCreditCardValid = false
           }
       }
    
    func cardNum() {
        TextFieldCreditCardNumber.inputType = .integer
       TextFieldCreditCardNumber.textFieldDelegate = self
        TextFieldCreditCardNumber.formatter = CardNumberFormatter()
        TextFieldCreditCardNumber.setValue(colors.white.value , forKeyPath: "placeholderLabel.textColor")
        TextFieldCreditCardNumber.placeholder = "Card Number"
//        TextFieldCreditCardNumber.font = UIFont.regular(ofSize: 13.0)
        TextFieldCreditCardNumber.textColor = colors.white.value
        TextFieldCreditCardNumber.leftMargin = 0
        TextFieldCreditCardNumber.layer.cornerRadius = 5
        validation.maximumLength = 19
        validation.minimumLength = 14
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        validation.characterSet = characterSet as CharacterSet
//        inputValidator = InputValidator(validation: validation)
//        TextFieldCreditCardNumber.inputValidator = inputValidator
    }
    
    func SetValidation () {
        //card number
        TextFieldCreditCardNumber.inputType = .integer
        TextFieldCreditCardNumber.formatter = CardNumberFormatter()
        validation.maximumLength = 19
        validation.minimumLength = 14
        let characterSet = NSMutableCharacterSet.decimalDigit()
        characterSet.addCharacters(in: " ")
        validation.characterSet = characterSet as CharacterSet
        inputValidator = InputValidator(validation: validation)
        TextFieldCreditCardNumber.inputValidator = inputValidator
//        txtExpiryDate.inputView = monthPicker
        // Expiry Date
        
        //        txtExpiryDate.inputType = .integer
        //        txtExpiryDate.formatter = CardExpirationDateFormatter()
        validation.minimumLength = 1
        
        //        let inputValidators = CardExpirationDateInputValidator(validation: validation)
        
        //        txtExpiryDate.inputValidator = inputValidators
        
        // CVV
        self.CvvValidation()
    }
    
    func CvvValidation() {
        TextFieldCVV.isSecureTextEntry = true
        TextFieldCVV.inputType = .integer
         
             self.validation.maximumLength = 4
             self.validation.minimumLength = 3
         
         validation.characterSet = NSCharacterSet.decimalDigits
         let inputValidator = InputValidator(validation: validation)
         TextFieldCVV.inputValidator = inputValidator
     }
    
    func isValidatePaymentDetail() -> (Bool,String) {
        var isValidate:Bool = true
        var ValidatorMessage:String = ""
        let holder = TextFieldName.validatedText(validationType: ValidatorType.username(field: "card holder name") )//ValidatorType.requiredField(field: "first name"))
        
        if (!holder.0) {
            isValidate = false
            ValidatorMessage = holder.1
            
        }else if (TextFieldCreditCardNumber.text!.isEmptyOrWhitespace()) {
            isValidate = false
            ValidatorMessage = "Please enter card number"
            
        }else if !isCreditCardValid {
            isValidate = false
            ValidatorMessage = "Please enter a valid card number"
        }
        //        else if TextFieldCreditCardNumber.text?.count != 19 {
        //            isValidate = false
        //            ValidatorMessage = "Please enter valid card number."
        //        }
        else if TextFieldExpires.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count == 0 {
            isValidate = false
            ValidatorMessage = "Please enter expiry date"
            
        } else if TextFieldCVV.text!.isEmptyOrWhitespace() {
            isValidate = false
            ValidatorMessage = "Please enter cvv"
        }
        
        return (isValidate,ValidatorMessage)
    }
    func showAlert(message:String) {
        let alert = UIAlertController(title: AppInfo.appName , message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - API Calls
    
    
}
extension AddCardVC : UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == TextFieldExpires {
            TextFieldExpires.inputView = pickerView
//            return false
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == TextFieldExpires {
            strMonth = aryMonth[pickerView.selectedRow(inComponent: 0)]
            strYear = aryYear[pickerView.selectedRow(inComponent: 1)]
            
            if strYear.isEmpty {
                strYear = aryYear[0]
            }

            if strMonth.isEmpty {
                strMonth = aryMonth[0]
            }

            TextFieldExpires.text = "\(strMonth) / \(strYear)"
            
        }
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.TextFieldZipCode {
            let maxLength = 12
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
                return newString.length <= maxLength
        }else{
            return true
        }
    }
}
extension AddCardVC : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return aryMonth.count
        }
        else {
            return aryYear.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return aryMonth[row]
        }
        else {
            return aryYear[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0 {
             strMonth = aryMonth[row]
        }else {
             strYear = aryYear[row]
           
        }
        if strYear.isEmpty {
            strYear = aryYear[0]
        }
        
        if strMonth.isEmpty {
            strMonth = aryMonth[0]
        }
        
        TextFieldExpires.text = "\(strMonth) / \(strYear)"
    }
}


public class CreditCardValidator {
    
    public lazy var types: [CreditCardValidationType] = {
        var types = [CreditCardValidationType]()
        for object in CreditCardValidator.types {
            types.append(CreditCardValidationType(dict: object))
        }
        return types
        }()
    
    public init() { }
    
    /**
    Get card type from string
    
    - parameter string: card number string
    
    - returns: CreditCardValidationType structure
    */
    public func type(from string: String) -> CreditCardValidationType? {
        for type in types {
            let predicate = NSPredicate(format: "SELF MATCHES %@", type.regex)
            let numbersString = self.onlyNumbers(string: string)
            if predicate.evaluate(with: numbersString) {
                return type
            }
        }
        return nil
    }
    
    /**
    Validate card number
    
    - parameter string: card number string
    
    - returns: true or false
    */
    public func validate(string: String) -> Bool {
        let numbers = self.onlyNumbers(string: string)
        if numbers.count < 9 {
            return false
        }
        
        var reversedString = ""
        let range: Range<String.Index> = numbers.startIndex..<numbers.endIndex
        
        numbers.enumerateSubstrings(in: range, options: [.reverse, .byComposedCharacterSequences]) { (substring, substringRange, enclosingRange, stop) -> () in
            reversedString += substring!
        }
        
        var oddSum = 0, evenSum = 0
        let reversedArray = reversedString
        
        for (i, s) in reversedArray.enumerated() {
            
            let digit = Int(String(s))!
            
            if i % 2 == 0 {
                evenSum += digit
            } else {
                oddSum += digit / 5 + (2 * digit) % 10
            }
        }
        return (oddSum + evenSum) % 10 == 0
    }
    
    /**
    Validate card number string for type
    
    - parameter string: card number string
    - parameter type:   CreditCardValidationType structure
    
    - returns: true or false
    */
    public func validate(string: String, forType type: CreditCardValidationType) -> Bool {
        return self.type(from: string) == type
    }
    
    public func onlyNumbers(string: String) -> String {
        let set = CharacterSet.decimalDigits.inverted
        let numbers = string.components(separatedBy: set)
        return numbers.joined(separator: "")
    }
    
    // MARK: - Loading data
    
    private static let types = [
        [
            "name": "Amex",
            "regex": "^3[47][0-9]{5,}$"
        ], [
            "name": "Visa",
            "regex": "^4\\d{0,}$"
        ], [
            "name": "MasterCard",
            "regex": "^5[1-5]\\d{0,14}$"
        ], [
            "name": "Maestro",
            "regex": "^(?:5[0678]\\d\\d|6304|6390|67\\d\\d)\\d{8,15}$"
        ], [
            "name": "Diners Club",
            "regex": "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$"
        ], [
            "name": "JCB",
            "regex": "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        ], [
            "name": "Discover",
            "regex": "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        ], [
            "name": "UnionPay",
            "regex": "^62[0-5]\\d{13,16}$"
        ], [
            "name": "Mir",
            "regex": "^22[0-9]{1,14}$"
        ]
    ]
    
}

public func ==(lhs: CreditCardValidationType, rhs: CreditCardValidationType) -> Bool {
    return lhs.name == rhs.name
}

public struct CreditCardValidationType: Equatable {
    
    public var name: String
    
    public var regex: String

    public init(dict: [String: Any]) {
        if let name = dict["name"] as? String {
            self.name = name
        } else {
            self.name = ""
        }
        
        if let regex = dict["regex"] as? String {
            self.regex = regex
        } else {
            self.regex = ""
        }
    }
    
}


extension AddCardVC {
    func SaveCard() {
        
        let AddCardModel = AddCardReqModel()
        
        AddCardModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        AddCardModel.card_holder_name = self.TextFieldName.text ?? ""
        AddCardModel.card_number = self.TextFieldCreditCardNumber.text?.replacingOccurrences(of: " ", with: "") ?? ""
        AddCardModel.expiry_date = self.TextFieldExpires.text?.replacingOccurrences(of: " ", with: "") ?? ""
        AddCardModel.cvv = self.TextFieldCVV.text ?? ""
        if self.TextFieldZipCode.text?.count == 0 || self.TextFieldZipCode.text?.count ?? 0 <= 3 {
            showAlert(message: "Please enter valid zipcode")
            Utilities.hideHud()
            return
        }else{
            AddCardModel.zipcode = self.TextFieldZipCode.text ?? ""
        }
       
       // array('1' => 'visa', '2' => 'mastercard', '3' => 'amex', '4' => 'jcb', '5' => 'dinnerclub', '6' => 'discover');
        switch self.cardTypeLabel {
        case "Visa".lowercased():
            AddCardModel.card_type = "1"
        case "MasterCard".lowercased():
            AddCardModel.card_type = "2"
        case "Amex".lowercased():
            AddCardModel.card_type = "3"
        case "JCB".lowercased():
            AddCardModel.card_type = "4"
        case "Diners Club".lowercased():
            AddCardModel.card_type = "5"
        case "Discover".lowercased():
            AddCardModel.card_type = "6"
        case "Mir".lowercased():
            break
        case "UnionPay".lowercased():
            break
        case "Maestro".lowercased():
            break
            
        default:
            break
        }
        
            webserviceForAddCard(reqModel: AddCardModel)
    }
    func webserviceForAddCard(reqModel:AddCardReqModel) {
    
     WebServiceSubClass.AddCard(addCategory: reqModel, completion: {(json, status, response) in
       
            if status {
                AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_saveCard)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "GetCardData"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            } else {
                Utilities.hideHud()
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}
