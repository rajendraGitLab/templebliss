//
//  MyFavouriteViewController.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import SkeletonView
class MyFavouriteViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {

    //MARK: - Properties
    var pageNumber = 1
    var pagelimit = 10
    var isNeedToReload = false
    var FirstTimeLoad = true
    
    var customTabBarController : CustomTabBarVC?
    var advisorList : [Advisor] = []
//    var advisorList : adviserListResModel?
    var totalPagesLoad = 1
    var isLoadingList : Bool = false
  
    //MARK: - IBOutlets
    @IBOutlet weak var myFavouriteCollectionView: UICollectionView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        self.myFavouriteCollectionView.register(UINib(nibName:"NoDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoDataCollectionViewCell")
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        super.viewDidLoad()
        setLocalization()
        setValue()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Favorite Advisors", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        self.myFavouriteCollectionView.allowsMultipleSelection = false
      //  GetAdvisorsDataWithPaginations()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        
        customTabBarController?.hideTabBar()
    }
    override func viewDidAppear(_ animated: Bool) {
        self.latestReload()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }

    //MARK: -  collectionViewMethods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if FirstTimeLoad == true {
            
            return 10
            
        } else {
            if advisorList.count == 0 {
                return 1
            } else {
                
                return advisorList.count ?? 0
            }
            
        }
        
        //return advisorList.advisors.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if FirstTimeLoad == true {
            let cell = myFavouriteCollectionView.dequeueReusableCell(withReuseIdentifier: myFavouriteCell.reuseIdentifier, for: indexPath) as! myFavouriteCell
            
            
            cell.userProfileImage.image = UIImage(named: "")
            cell.adviserName.text = ""
            cell.categoryName.text = ""
            cell.lblRatting.text = ""
            cell.btnFavourite.isSelected = false
            cell.btnChat.isHidden = true
            
            
            
            cell.btnVideo.isHidden = true
            cell.btnAudio.isHidden = true
            cell.btnFavourite.isHidden = true
            cell.adviserActiveStatusImageView.isHidden = true
            
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            
            return cell
        } else {
            if advisorList.count == 0 {
                let NoDatacell = myFavouriteCollectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCollectionViewCell", for: indexPath) as! NoDataCollectionViewCell
                NoDatacell.img.image = UIImage(named: "ic_noAdvisorsFound")
                NoDatacell.lblNodataTitle.text = "No favorite advisor found"
                //NodataTitleText.ItemList

                return NoDatacell
            } else {
                
                let cell = myFavouriteCollectionView.dequeueReusableCell(withReuseIdentifier: myFavouriteCell.reuseIdentifier, for: indexPath) as! myFavouriteCell
                cell.btnFavourite.isHidden = false
                cell.adviserActiveStatusImageView.isHidden = false
                cell.userProfileImage.hideSkeleton()
                cell.adviserName.hideSkeleton()
                
                cell.categoryName.hideSkeleton()
                cell.adviserName.text = advisorList[indexPath.row].nickName
                
                if advisorList[indexPath.row].isFavourite == "0"{
                    cell.btnFavourite.isSelected = false
                } else {
                    cell.btnFavourite.isSelected = true
                }
                switch advisorList[indexPath.row].isAvailabile {
                case 0:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserInActive.value
                case 1:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserActive.value
                case 2:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserBusy.value
                default:
                    break
                }
                
                
                
                //        var arrayForCategoryName : [String] = []
                //        let arrayOfAdviserCategory = advisorList[indexPath.row].categories.split(separator: ",").map { String($0) }
                //        SingletonClass.sharedInstance.categoryListCustomer?.category.forEach({ (element) in
                //            arrayOfAdviserCategory?.forEach({ (subCategory) in
                //                if subCategory == element.id {
                //                    arrayForCategoryName.append(element.name)
                //                }
                //            })
                //        })
                cell.lblCategoryName.text = advisorList[indexPath.row].categoriesName
                //arrayForCategoryName.joined(separator: ", ").capitalized
                
                let num = NSNumber(value: advisorList[indexPath.row].ratings ?? 0)
                let formatter : NumberFormatter = NumberFormatter()
                formatter.numberStyle = .decimal
                let str = formatter.string(from: num)!
                cell.lblRatting.text = "- ⭐️ \(str)"
                
                cell.lblRatting.text = "- ⭐️ \(advisorList[indexPath.row].avgRating ?? "")"
                
                cell.btnChat.isHidden = true
                cell.btnVideo.isHidden = true
                cell.btnAudio.isHidden = true
                
                let pointsArr = advisorList[indexPath.row].availabileFor.split(separator: ",")
                if pointsArr.count != 0 {
                    for i in 0...(pointsArr.count ?? 0 ) - 1 {
                        
                        if pointsArr[i].lowercased() == typeOfCommunication.Audio.returnString().lowercased() {
                            cell.btnAudio.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Chat.returnString().lowercased() {
                            cell.btnChat.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Video.returnString().lowercased() {
                            cell.btnVideo.isHidden = false
                        }
                        
                    }
                } else {
                    cell.btnChat.isHidden = true
                    cell.btnVideo.isHidden = true
                    cell.btnAudio.isHidden = true
                }
                
                
                if (advisorList[indexPath.row].profilePicture ?? "") == "" {
                    cell.userProfileImage.image = UIImage(named: "user_dummy_profile")
                } else {
                    //                    let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
                    //                    cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
                    let imgURL = advisorList[indexPath.row].profilePicture ?? ""
                    
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    
                    cell.userProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                    //                    cell.userProfileImage.sd_setImage(with: strURl, completed: {_,_,_,_ in
                    //                        cell.userProfileImage.hideSkeleton()
                    //                    })
                    cell.userProfileImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                
                
                
                
                cell.closourForFavourite = {
                    if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                        //                SingletonClass.sharedInstance.selectedCategoryIndex = -1
                        //                SingletonClass.sharedInstance.category_id = ""
                        //                SingletonClass.sharedInstance.descriptionForSelectedCategories = ""
                        //                SingletonClass.sharedInstance.selectedTypeAdvisor = ""
                        appDel.navigateToUserLogin()
                        userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                    }else {
                        Utilities.showHud()
                        let favouriteModel = FavouriteReqModel()
                        favouriteModel.advisor_id = self.advisorList[indexPath.row].id ?? ""
                        favouriteModel.user_id = SingletonClass.sharedInstance.UserId
                        //                        if cell.isFavourite == true {
                        //                            favouriteModel.status = "1"
                        //                        } else {
                        //                            favouriteModel.status = "0"
                        //                        }
                        if cell.btnFavourite.isSelected {
                            favouriteModel.status = "1"
                        } else {
                            favouriteModel.status = "0"
                        }
                        cell.isUserInteractionEnabled = false
                        //cell.btnFavourite.isUserInteractionEnabled = false
                        //Utilities.showHud()
                        WebServiceSubClass.AddToFavourite(CategoryModel: favouriteModel, completion: {(json, status, response) in
                            cell.isUserInteractionEnabled = true
                            //cell.btnFavourite.isUserInteractionEnabled = true
                            if status {
                                self.GetAllListedData()
                                
                                // self.getAdviserData(searchString: self.textfieldSearchAdviser.text ?? "")
                            } else {
                                if self.advisorList[indexPath.row].isFavourite == "0"{
                                    cell.btnFavourite.isSelected = false
                                } else {
                                    cell.btnFavourite.isSelected = true
                                }
                                // Utilities.hideHud()
                            }
                        })
                    }
                    
                    
                }
                
                return cell
            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FirstTimeLoad == true {
            var heightOfCell:CGFloat = 0.0
            let widthOfCell  = (myFavouriteCollectionView.frame.size.width - 14) / 2
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                heightOfCell = layout.itemSize.height
                
            }
            heightOfCell = widthOfCell + 56
            return CGSize(width: widthOfCell, height: heightOfCell)
        } else {
            if advisorList.count == 0 {
                return CGSize(width: self.myFavouriteCollectionView.frame.width, height: self.myFavouriteCollectionView.frame.height)
            } else {
                
                var heightOfCell:CGFloat = 0.0
                let widthOfCell  = (myFavouriteCollectionView.frame.size.width - 14) / 2
                if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    heightOfCell = layout.itemSize.height
                    
                }
                heightOfCell = widthOfCell + 56
                return CGSize(width: widthOfCell, height: heightOfCell)
            }
            
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if advisorList.count == 0 {
            
        } else {
            
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
            controller.selectedAdviserDetails = advisorList[indexPath.row]
            //controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if advisorList.count != 0 {
            if indexPath.row == advisorList.count - 4 && isNeedToReload == true
            {
                ReloadMore()
            }
        }
       
    }
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
//    {
//        if advisorList.count == 0 {
//
//        } else {
//            if (advisorList.count ?? 0) > 10 {
//                if indexPath.row == advisorList.count
//                {
//                    self.isLoadingList = true
//                    LoadMoreData()
//                }
//
//            }
//
//
//        }
//
//    }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
    
}
class myFavouriteCell : UICollectionViewCell {
    
    var closourForFavourite : (() -> ())?
    @IBOutlet weak var adviserActiveStatusImageView: UIImageView!
    
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblRatting: AdviserPageLabel!
    @IBAction func btnFavouriteClick(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        if let click = self.closourForFavourite {
            click()
        }
    }
    
    @IBOutlet weak var categoryName: AdviserPageLabel!
    @IBOutlet weak var adviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblAdviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblCategoryName: AdviserPageLabel!
    @IBOutlet weak var icLiveOn: UIImageView!
    
    
    @IBAction func btnChatClick(_ sender: Any) {
    }
    
    @IBAction func btnAudioClick(_ sender: Any) {
    }
    
    
    @IBAction func btnVideoClick(_ sender: Any) {
    }
    override func awakeFromNib() {
        lblCategoryName.numberOfLines = 2
        
        
        userProfileImage.isSkeletonable = true
        
        lblAdviserName.isSkeletonable = true
        
        lblCategoryName.isSkeletonable = true
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        //cell.ca.numberOfLines = 1
       
        
        userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
//        adviserName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
//
//        categoryName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        
    }
}
//extension MyFavouriteViewController {
////    func GetAdvisorsDataWithPaginations() {
////        self.advisorList = []
////        ReloadAdvisorData(pageNumber: "1")
////    }
////    func ReloadAdvisorData(pageNumber:String) {
////        print(#function)
////        if Int(pageNumber) == self.totalPagesLoad + 1 {
////            Utilities.hideHud()
////
////            self.FirstTimeLoad = false
////            self.myFavouriteCollectionView.reloadData()
////            Utilities.hideHud()
////        } else {
////            let requestModelForadvisorList = AdviserListRequestModel()
////            requestModelForadvisorList.category_id = ""
////            requestModelForadvisorList.search_string = ""
////            requestModelForadvisorList.type = ""
////            //requestModelForadvisorList.type = search_filter.lowercased()
////            requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
////            requestModelForadvisorList.page = "\(pageNumber)"
////            requestModelForadvisorList.is_favourite = "1"
////            //Utilities.showHud()
////            webServiceCallForAdvisorDetailsWithPagination(RequestModel: requestModelForadvisorList)
////
////        }
////    }
////    func webServiceCallForAdvisorDetailsWithPagination(RequestModel:AdviserListRequestModel) {
////        WebServiceSubClass.adviserList1(CategoryModel: RequestModel, completion: {(json, status, response) in
////            if status {
////
////                let categoryResModel = adviserListResModel.init(fromJson: json)
////                self.advisorList.append(contentsOf: categoryResModel.advisors)
////                let AdvisorListFormJSON:[Advisor] = categoryResModel.advisors
////
////                let pageNumberLoaded = Int(RequestModel.page) ?? 0
////                print(pageNumberLoaded)
////
////                if AdvisorListFormJSON.count == 0 {
////                    self.FirstTimeLoad = false
////                    self.myFavouriteCollectionView.reloadData()
////                    Utilities.hideHud()
////                } else {
////                    self.ReloadAdvisorData(pageNumber: "\(pageNumberLoaded + 1)")
////                }
////
////            } else {
////                Utilities.hideHud()
////                if self.advisorList.count == 0 {
////
////
////                    self.FirstTimeLoad = false
////                    self.myFavouriteCollectionView.reloadData()
////                }
////                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
////            }
////
////        })
////    }
////
////
////
////    func LoadMoreData() {
////        print(#function)
////        let requestModelForadvisorList = AdviserListRequestModel()
////        requestModelForadvisorList.category_id = ""
////        requestModelForadvisorList.search_string = ""
////        requestModelForadvisorList.type = ""
////        //requestModelForadvisorList.type = search_filter.lowercased()
////        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
////        requestModelForadvisorList.page = "\(totalPagesLoad + 1)"
////        requestModelForadvisorList.is_favourite = "1"
////
////            webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
////    }
////    func webserviceForLoadMoreData(reqModel:AdviserListRequestModel) {
////        WebServiceSubClass.adviserList1(CategoryModel: reqModel, completion: {(json, status, response) in
////            self.isLoadingList = false
////            if status {
////
////                let categoryResModel = adviserListResModel.init(fromJson: json)
////                self.advisorList.append(contentsOf: categoryResModel.advisors)
////
////
////                if categoryResModel.advisors.count != 0 {
////                    self.totalPagesLoad  = self.totalPagesLoad + 1
////                }
////
////                self.myFavouriteCollectionView.reloadData()
////
////            } else {
////                Utilities.hideHud()
////                if self.advisorList.count == 0 {
////
////                    self.FirstTimeLoad = false
////                    self.myFavouriteCollectionView.reloadData()
////                }
////            }
////
////        })
////    }
//
//    func getAdviserData() {
//        let requestModelForAdviserList = AdviserListRequestModel()
//        requestModelForAdviserList.category_id = ""
//        requestModelForAdviserList.search_string = ""
//        requestModelForAdviserList.type = ""
//        //requestModelForAdviserList.type = search_filter.lowercased()
//        requestModelForAdviserList.user_id = SingletonClass.sharedInstance.UserId
//        requestModelForAdviserList.is_favourite = "1"
//        //Utilities.showHud()
//        WebServiceSubClass.adviserList(CategoryModel: requestModelForAdviserList, completion: {(json, status, response) in
//            Utilities.hideHud()
//
//            if status {
//
//                let categoryResModel = adviserListResModel.init(fromJson: json)
//                self.advisorList = categoryResModel
//                self.FirstTimeLoad = false
//                    self.myFavouriteCollectionView.reloadData()
//
//            } else {
//                Utilities.displayAlert("", message: json["message"].string ?? "")
//            }
//        })
//    }
//}


extension MyFavouriteViewController {
    func latestReload(){
        pageNumber = 1
        isNeedToReload = false
        
        let requestModelForadvisorList = AdviserListRequestModel()
        requestModelForadvisorList.category_id = ""
        requestModelForadvisorList.search_string = ""
        requestModelForadvisorList.type = ""
        //requestModelForadvisorList.type = search_filter.lowercased()
        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
        requestModelForadvisorList.page = "\(pageNumber)"
        requestModelForadvisorList.is_favourite = "1"
        
        webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
        
    }
    func ReloadMore(){
        print(#function)
        pageNumber += 1
        isNeedToReload = false
       
        let requestModelForadvisorList = AdviserListRequestModel()
        requestModelForadvisorList.category_id = ""
        requestModelForadvisorList.search_string = ""
        requestModelForadvisorList.type = ""
        
        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
        requestModelForadvisorList.page = "\(pageNumber)"
        requestModelForadvisorList.is_favourite = "1"
        
        webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
       
    }
    
}
extension MyFavouriteViewController {
    
    func webserviceForLoadMoreData(reqModel:AdviserListRequestModel){
       
        WebServiceSubClass.adviserList1(CategoryModel: reqModel, completion: { (json, status, response) in
           
            if status {
                self.FirstTimeLoad = false
                self.myFavouriteCollectionView.isHidden = false
                print(response)
                
                
                let objresponse = adviserListResModel.init(fromJson: json)
               
                if objresponse.advisors.count == self.pagelimit {
                    self.isNeedToReload = true
                }
                else {
                    self.isNeedToReload = false
                }
                if self.pageNumber == 1 {
                    //let sortedtemp = objresponse.items.sorted(by: { $0.id > $1.id })
                    self.advisorList = objresponse.advisors
                 }
                 else{
                   // let sortedtemp = objresponse.items.sorted(by: { $0.id > $1.id })
                   self.advisorList.append(contentsOf: objresponse.advisors)
                 }
                self.myFavouriteCollectionView.reloadData()
                
               
            }
            else{
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    self.FirstTimeLoad = false
                    self.myFavouriteCollectionView.reloadData()
                }
                
               
            }
        })
    }
    func GetAllListedData() {
        self.advisorList = []
        ReloadAdvisorData(pageNumberTemp: "1")
    }
    func ReloadAdvisorData(pageNumberTemp:String) {
        print(#function)
        if Int(pageNumberTemp) == self.pageNumber + 1 {
            Utilities.hideHud()
            
            self.FirstTimeLoad = false
            sleep(1)
            self.myFavouriteCollectionView.reloadData()
            Utilities.hideHud()
        } else {
            let requestModelForadvisorList = AdviserListRequestModel()
            requestModelForadvisorList.category_id = ""
            requestModelForadvisorList.search_string = ""
            requestModelForadvisorList.type = ""
            //requestModelForadvisorList.type = search_filter.lowercased()
            requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
            requestModelForadvisorList.page = "\(pageNumberTemp)"
            requestModelForadvisorList.is_favourite = "1"
            //Utilities.showHud()
            webServiceCallForAdvisorDetailsWithPagination(RequestModel: requestModelForadvisorList)
            
        }
    }
    func webServiceCallForAdvisorDetailsWithPagination(RequestModel:AdviserListRequestModel) {
        WebServiceSubClass.adviserList1(CategoryModel: RequestModel, completion: {(json, status, response) in
            self.myFavouriteCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
           
            if status {
                
                let categoryResModel = adviserListResModel.init(fromJson: json)
                
                let AdvisorListFormJSON:[Advisor] = categoryResModel.advisors
//
                let pageNumberLoaded = Int(RequestModel.page) ?? 0
//
                self.advisorList.append(contentsOf: categoryResModel.advisors)

                if AdvisorListFormJSON.count == 0 {
                    Utilities.hideHud()
                } else {
                    sleep(1)
                    self.ReloadAdvisorData(pageNumberTemp: "\(pageNumberLoaded + 1)")
                }
                
            } else {
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    
                    
                    self.FirstTimeLoad = false
                    self.myFavouriteCollectionView.reloadData()
                }
                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}
