//
//  RateAdviserViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage
class RateAdviserViewController: BaseViewController,UITextViewDelegate {

    
    
    //MARK: - Properties
    var isDismiss : (() -> ())?
    var advisorID : String = ""
    var bookingID : String = ""
    var advisorProfileImaage : String = ""
    var advisorName : String = ""
    var TotalMinutes : String = ""
    
    
    //MARK: - IBOutlets
    @IBOutlet weak var rattingView: CosmosView!
    @IBOutlet weak var TextVIewRattingDescription: themeTextView!
    @IBOutlet weak var adviserImage: UIImageView!
    @IBOutlet weak var lblAdviserName: AdviserRattingLabel!
    @IBOutlet weak var lblTotalMinutes: AdviserRattingLabel!
    @IBOutlet weak var lblRateAdviser: AdviserRattingLabel!
    @IBOutlet weak var btnSubmit: theamSubmitButton!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        adviserImage.contentMode = .scaleAspectFill
        adviserImage.clipsToBounds = true
        
        TextVIewRattingDescription.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
   
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           let maxAllowedCharactersPerLine = 1000
           let lines = (textView.text as NSString).replacingCharacters(in: range, with: text).components(separatedBy: .newlines)
           for line in lines {
               if line.count > maxAllowedCharactersPerLine {
                   return false
               }
           }
           return true
       }
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 1000 { textView.undoManager?.removeAllActions() }
        textView.text = String(textView.text.prefix(1000))
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        if (advisorProfileImaage) == "" {
            adviserImage.image = UIImage(named: "user_dummy_profile")
        } else {

            let imgURL = advisorProfileImaage
            
            let strURl = URL(string: APIEnvironment.profileBu + imgURL)
           
            adviserImage.sd_imageIndicator = SDWebImageActivityIndicator.white

            adviserImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
        }
        
        TextVIewRattingDescription.text = ""
        
        lblAdviserName.text = advisorName.capitalized
        
        lblRateAdviser.text = "Rate " + advisorName.lowercased()
        if TotalMinutes == "1" {
            lblTotalMinutes.text = TotalMinutes + " Minute"
        } else {
            lblTotalMinutes.text = TotalMinutes + " Minutes"
        }
        
    }
    //MARK: - IBActions
    @IBAction func btnSubmitClick(_ sender: UIButton) {
        
        let reqModel = CustomerGiveRattingReqModel()
        reqModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        reqModel.note = TextVIewRattingDescription.text.trim()
        reqModel.rating = "\(rattingView.rating)"
        reqModel.advisor_id = advisorID
        reqModel.booking_id = bookingID
        WebseviceCallForGiveRattings(ReqModel: reqModel)
        
        //self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - API Calls
    
    
    
    
    func WebseviceCallForGiveRattings(ReqModel : CustomerGiveRattingReqModel) {
        Utilities.showHud()
        WebServiceSubClass.CustomerGiveRatting(addCategory: ReqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
              
                if let click = self.isDismiss {
                    click()
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
    
    
    
    
    
    
 
}
