//
//  MyCreditViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class MyCreditViewController: BaseViewController {

    //MARK: - Properties
    var isLoadingList : Bool = false
    var totalPagesLoad = 1
    
    var customTabBarController : CustomTabBarVC?
    
    var walletHistoryData : [WalletHistory] = []
    
    //MARK: - IBOutlets
    @IBOutlet weak var TotalAmount: MyCreditLabel!
    @IBOutlet weak var tblWalletHistory: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "DataRefresh"), object: nil)
                 NotificationCenter.default.addObserver(self, selector: #selector(DataRefresh), name: NSNotification.Name(rawValue: "DataRefresh"), object: nil)
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        tblWalletHistory.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
        
        setLocalization()
        setValue()
        GetWalletHistory()
        self.tblWalletHistory.isHidden = true
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "My Wallet", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        

    }
    @objc func DataRefresh(){
        totalPagesLoad = 1
        self.tblWalletHistory.isHidden = true
        GetWalletHistory()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        customTabBarController?.hideTabBar()
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        TotalAmount.text = ""
    }
    func convertDateFormater(_ date: String) -> String
        {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd MMM YYYY h:mm a"
        return  dateFormatter.string(from: date!)

        }
    //MARK: - IBActions
    
    @IBAction func btnAddMoneyClick(_ sender: Any) {
        let controller:addPaymentVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: addPaymentVC.storyboardID) as! addPaymentVC
        controller.isFromBuyMinutes = false
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
        print(myProfileCategory.MyCredit.value)
        
    }
    
    //MARK: - API Calls

}
    //MARK: - TableView Methods
extension MyCreditViewController : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if walletHistoryData.count != 0 {
            return walletHistoryData.count
        } else {
            return 1
        }
        
    }
   
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
               if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                   
                   if walletHistoryData.count != 0 {
                       self.isLoadingList = true
                       let spinner = UIActivityIndicatorView(style: .white)
                       spinner.startAnimating()
                       spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblWalletHistory.bounds.width, height: CGFloat(44))
                       
                       self.tblWalletHistory.tableFooterView = spinner
                       self.tblWalletHistory.tableFooterView?.isHidden = false
                       LoadMoreData()
                   }
                      
                   
               }
       
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let lastItem = walletHistoryData.count - 1
//        if(indexPath.row) == lastItem {
//            let spinner = UIActivityIndicatorView(style: .white)
//            spinner.startAnimating()
//            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//            self.tblWalletHistory.tableFooterView = spinner
//            self.tblWalletHistory.tableFooterView?.isHidden = false
//            LoadMoreData()
//        }
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if walletHistoryData.count != 0 {
            let cell = tblWalletHistory.dequeueReusableCell(withIdentifier: WalletHistoryCell.reuseIdentifier, for: indexPath) as! WalletHistoryCell
            
            if walletHistoryData[indexPath.row].amountType == "plus" {
                cell.lblmoneyFrom.text = walletHistoryData[indexPath.row].descriptionField ?? ""
                
                cell.lblPrice.text = "+ \(Currency) \(String(format: "%.2f", ((walletHistoryData[indexPath.row].amount as NSString?)?.floatValue) ?? 0.00))"
                cell.lblDate.text = convertDateFormater(walletHistoryData[indexPath.row].createdAt)
                cell.lblPrice.textColor = UIColor(hexString: "#FFFFFF")
            } else {
                cell.lblmoneyFrom.text = walletHistoryData[indexPath.row].descriptionField ?? ""
                cell.lblPrice.text = "- \(Currency) \(String(format: "%.2f", ((walletHistoryData[indexPath.row].amount as NSString?)?.floatValue) ?? 0.00))"
                cell.lblDate.text = convertDateFormater(walletHistoryData[indexPath.row].createdAt)
                cell.lblPrice.textColor = UIColor(hexString: "#E24444")
            }
            //            cell.lblmoneyFrom.text = "Refund from Order #15421"
            //            cell.lblDate.text = "12/10/2020  01:55 PM"
            //            cell.lblPrice.text = "+52.00"
            //            cell.selectionStyle = .none
            //            if indexPath.row == 1
            //            {
            //                cell.lblmoneyFrom.text = "Payment Done"
            //                cell.lblPrice.text = "- 30.00"
            //                cell.lblPrice.textColor = UIColor(hexString: "#E24444")
            //            }
            
            return cell
        } else {
            let NoDatacell = tblWalletHistory.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
            
            NoDatacell.imgNoData.image = UIImage(named: "transaction")
            NoDatacell.lblNoDataTitle.text = "No recent history found"
            
            return NoDatacell
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if walletHistoryData.count == 0 {
            return 0
        } else {
            return 50
        }
            
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if walletHistoryData.count != 0 {
            return UITableView.automaticDimension
        } else {
            return tblWalletHistory.frame.size.height
            
        }
        //return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let cell = tblWalletHistory.dequeueReusableCell(withIdentifier: WalletHistoryHeaderCell.reuseIdentifier) as! WalletHistoryHeaderCell
        cell.lblheader.text = "Recent History"
       
            return cell
       
    }
  
    func numberOfSections(in tableView: UITableView) -> Int {
        if walletHistoryData.count != 0 {
            return 1
        } else {
            return 1
        }
        
    }
}
class WalletHistoryCell : UITableViewCell {
    @IBOutlet weak var lblmoneyFrom : walletHistoryLabel!
    @IBOutlet weak var lblDate : walletHistoryLabel!
    @IBOutlet weak var lblPrice : walletHistoryLabel!
}

class walletHistoryLabel : UILabel {
   
    @IBInspectable var ismoneyFrom : Bool = false
    @IBInspectable var istime : Bool = false
    @IBInspectable var ismoney : Bool = false
    override func awakeFromNib() {
        if ismoneyFrom {
            self.textColor = colors.white.value
            self.font = CustomFont.regular.returnFont(14)
                   self.textAlignment = .left
        } else if istime {
            self.textColor = colors.white.value.withAlphaComponent(0.5)
            self.font = CustomFont.regular.returnFont(13)
                   self.textAlignment = .left
        } else if ismoney {
            self.textColor = colors.white.value
            self.font = CustomFont.bold.returnFont(15)
                   self.textAlignment = .center
        }
    }
}
extension MyCreditViewController {
    func GetWalletHistory() {
        self.walletHistoryData = []
        let getMyCardsReqModel = GetMyCardsReqModel()
        getMyCardsReqModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        getMyCardsReqModel.page = "\(totalPagesLoad)"
        webserviceForGetWallletHistory(reqModel: getMyCardsReqModel)
    }
    func webserviceForGetWallletHistory(reqModel:GetMyCardsReqModel) {
        Utilities.showHud()
        WebServiceSubClass.GetWalletHistory(addCategory: reqModel, completion: {(json, status, response) in
           
            Utilities.hideHud()
            if status {
                let ResponseModel = WalletHistoryResModel.init(fromJson: json)
                SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = ResponseModel.walletAmount ?? "0"
                userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)
                
                self.TotalAmount.text = "\(Currency) \(String(format: "%.2f", ((ResponseModel.walletAmount as? NSString)?.floatValue) ?? 0.00))"
                if ResponseModel.walletHistory.count != 0 {
                    self.walletHistoryData.append(contentsOf: ResponseModel.walletHistory)
//                    self.walletHistoryData =
                    
                }
                if ResponseModel.walletHistory.count != 0 {
                    self.totalPagesLoad  = self.totalPagesLoad + 1
                }
                    
                    
                self.tblWalletHistory.isHidden = false
                self.tblWalletHistory.reloadData()
                
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
        
    }
}
extension MyCreditViewController {
    func LoadMoreData() {
        let getMyCardsReqModel = GetMyCardsReqModel()
        getMyCardsReqModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        getMyCardsReqModel.page = "\(totalPagesLoad)"
        WebServiceForLoadMore(reqModel: getMyCardsReqModel)
    }
    func WebServiceForLoadMore(reqModel:GetMyCardsReqModel) {
        
        WebServiceSubClass.GetWalletHistory(addCategory: reqModel, completion: {(json, status, response) in
          
            self.tblWalletHistory.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                let ResponseModel = WalletHistoryResModel.init(fromJson: json)
                SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = ResponseModel.walletAmount ?? "0"
                userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)
                
                self.TotalAmount.text = "\(Currency) \(String(format: "%.2f", ((ResponseModel.walletAmount as? NSString)?.floatValue) ?? 0.00))"
                if ResponseModel.walletHistory.count != 0 {
                    self.totalPagesLoad  = self.totalPagesLoad + 1
                    self.walletHistoryData.append(contentsOf: ResponseModel.walletHistory)
 
                }
                self.tblWalletHistory.reloadData()
            } else {
               
            }
        })
    }
}
