//
//  DeleteAccountViewController.swift
//  TempleBliss
//
//  Created by iMac on 28/02/22.
//  Copyright © 2022 EWW071. All rights reserved.
//

import UIKit

class DeleteAccountViewController: BaseViewController {
    
    // MARK: - Outlets
    @IBOutlet private weak var btnDelete: UIButton!
    @IBOutlet private weak var tvDescription: UITextView!
    
    // MARK: - Properties
    
    
    // MARK: - VC Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    func setupUI() {
        addNavBarImage(isLeft: true, isRight: true)
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Delete Account", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        
        tvDescription.isEditable = false
        tvDescription.font = CustomFont.regular.returnFont(14.0)
        
        tvDescription.text = "Delete account will delete your account permently. You can not further access Temple Bliss. Your money will refunded to your PayPal account.\n\nNote:- PayPal payout fee will apply in withdrawal"
    }
    
    func openEmailAlert() {
        let alert = UIAlertController(title: AppInfo.appName , message: "Please enter the email address associated with your PayPal account. We will add your wallet money to your PayPal account.", preferredStyle: .alert)
        
        alert.addTextField { (textField) in
            textField.placeholder = "Enter email id..."
            textField.keyboardType = .emailAddress
            textField.font = self.tvDescription.font
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            let tfEmail = alert.textFields![0] as UITextField
            let checkEmail = tfEmail.validatedText(validationType: ValidatorType.email)
            
            if checkEmail.0 {
                self.deleteAPICall(emailId: tfEmail.text!)
            } else {
                Utilities.ShowAlert(OfMessage: checkEmail.1)
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Api call
    private func deleteAPICall(emailId: String? = nil) {
        
        let del = DeleteAccountData()
        del.user_id = Int(SingletonClass.sharedInstance.UserId) ?? 0
        if let emailId = emailId {
            del.email_id = emailId
        }
        
        
        WebServiceSubClass.DeleteAccount(Data: del) { json, status, response in
            if status {
                SingletonClass.sharedInstance.StopCustomerTimer()
                appDel.performLogout()
            } else {
                Utilities.displayAlert(AppName, message: (json["message"].stringValue))
            }
        }
    }
    
    // MARK: - Action
    @IBAction private func onBtnDelete(_ sender: UIButton) {
        let alert = UIAlertController(title: AppInfo.appName , message: "Are you sure you want to delete your account permanently? (You can’t undo this action).", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            print("Wallet Amount: ", SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount ?? "")
            if (SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount ?? "0") != "0" {
                self.openEmailAlert()
            } else {
                self.deleteAPICall()
            }

        }))
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in

        }))
        self.present(alert, animated: true, completion: nil)
    }
    
}

