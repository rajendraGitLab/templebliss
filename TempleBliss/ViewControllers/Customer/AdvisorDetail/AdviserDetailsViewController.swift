//
//  AdviserDetailsViewController.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import Cosmos
import AVFoundation
class AdviserDetailsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ExpandableLabelDelegate  {
    
    
    //MARK: -  Properties
    var timer : Timer?
    var count = 1
    
    var refreshControl = UIRefreshControl()
    var states : Array<Bool>?
    var DataForBuyAgain : [String:Any] = [:]
    var SecondsForReminderScreen = 15 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var CommunicationScreenRemiderScreen = 15
    var TimerForReminderScreen = Timer()
    
    var TotalTimecounter = 0
    
    
    var totalMinutesTimer = 0
    
    var internalTimer: Timer?
    
    var categoriesData = [CategoriesArray]()
    var customTabBarController : CustomTabBarVC?
    var selectedCategoryType = 0
    var statesForColleps : Bool = true
    
    var AcceptedDataOfUser : [String:Any] = [:]
    
    var categoryColorArray : [String] = [CategoryTypeColor.Category1.colorHexString(),
                                         CategoryTypeColor.Category2.colorHexString(),
                                         CategoryTypeColor.Category3.colorHexString()]
    var selectedAdviserDetails : Advisor?
    var SelectedCategoryName = ""
    
    var RequestAcceptData : [String : Any] = [:]
    
    var callNowBtnColor: UIColor?
    
    //MARK: -  IBOutlets
    @IBOutlet weak var MainScrollView: UIScrollView!
    @IBOutlet weak var MainViewForNotifyMe: viewViewClearBG!
    @IBOutlet weak var btnNotifyMe: BtnNotifyMe!
    @IBOutlet weak var AverageRatiningView: CosmosView!
    @IBOutlet weak var adviserActiveStatusImageView: UIImageView!
    @IBOutlet weak var lblAdviserName: adviserDetailsLabel!
    @IBOutlet weak var lblRatingAndCount: adviserDetailsLabel!
    @IBOutlet weak var lblDescriptionText: adviserDetailsDescriptionLabel!
    @IBOutlet weak var adviserProfileImage: UIImageView!
    @IBOutlet weak var tblRateAndReviewHeight: NSLayoutConstraint!
    @IBOutlet weak var tblCommunicationTypeHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionCategoryHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet weak var tblRateAndReview: UITableView!
    @IBOutlet weak var tblCommunicationType: UITableView!
    //MARK: -  Observer method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblCommunicationType.layer.removeAllAnimations()
        tblCommunicationTypeHeight.constant = tblCommunicationType.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }
    }
    //MARK: -  View Life Cycle Methods
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        GetAdvisorDetailsForUpdate(userId: SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "", advisorID: selectedAdviserDetails?.id ?? ""){ (success) in
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        self.MainScrollView.refreshControl = refreshControl
        
        self.tblCommunicationType.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        AverageRatiningView.isUserInteractionEnabled = false
        categoryColorArray = Array(repeating: categoryColorArray, count: 10)
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        lblDescriptionText.collapsed = true
        lblDescriptionText.text = nil
        
        setLocalization()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Advisor Details", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.like.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        setValue()
        increaseOpenCount()
        //
        
        // Do any additional setup after loading the view.
    }
    
    func increaseOpenCount() {
        var actionCount = userDefault.integer(forKey: .advisorDetailScreenOpenCount)
        actionCount += 1
        userDefault.set(actionCount, forKey: .advisorDetailScreenOpenCount)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetAdvisorDetailsForUpdate(userId: SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "", advisorID: selectedAdviserDetails?.id ?? ""){ (success) in
            
        }
        customTabBarController?.hideTabBar()
    }
    
    //MARK: -  other methods
    func goToChatScreen() {
        totalMinutesTimer = (Int(RequestAcceptData["minute"] as? String ?? "") ?? 1)
        
        TotalTimecounter = 0
        
        startTimer(withInterval: 1)
        
    }
    
    func willExpandLabel(_ label: ExpandableLabel) {
        if label == lblDescriptionText {
            lblDescriptionText.numberOfLines = 0
            lblDescriptionText.collapsed = false
        } else {
            tblRateAndReview.beginUpdates()
        }
        
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        if label == lblDescriptionText {
            
        } else {
            let point = label.convert(CGPoint.zero, to: tblRateAndReview)
            if let indexPath = tblRateAndReview.indexPathForRow(at: point) as IndexPath? {
                states?[indexPath.row] = false
                DispatchQueue.main.async { [weak self] in
                    self?.tblRateAndReview.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
            tblRateAndReview.endUpdates()
        }
        
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        if label == lblDescriptionText {
            lblDescriptionText.numberOfLines = 6
            lblDescriptionText.collapsed = true
        } else {
            tblRateAndReview.beginUpdates()
        }
        
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        if label == lblDescriptionText {
            
        } else {
            let point = label.convert(CGPoint.zero, to: tblRateAndReview)
            if let indexPath = tblRateAndReview.indexPathForRow(at: point) as IndexPath? {
                states?[indexPath.row] = true
                DispatchQueue.main.async { [weak self] in
                    self?.tblRateAndReview.scrollToRow(at: indexPath, at: .top, animated: true)
                }
            }
            tblRateAndReview.endUpdates()
        }
        
    }
    
    func setLocalization() {
        
    }
    func alertToEncourageCameraAccessInitially() {
        let alert = UIAlertController(
            title: AppName,
            message: "Camera access required for capturing photos!",
            preferredStyle: UIAlertController.Style.alert
        )
        alert.addAction(UIAlertAction(title: "Open settings", style: .cancel, handler: { (alert) -> Void in
            
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: nil))
        
        self.present(alert, animated: true)
    }
    func ShowMicroPhonePermissionAlert() {
        
        let alert = UIAlertController(title: AppName, message: "Please allow microphone usage from settings", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
        
    }
    func checkCameraAccess() -> Bool {
        var permissionCheck: Bool = false
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            permissionCheck = false
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            
            permissionCheck = true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    DispatchQueue.main.async {
                        
                        permissionCheck = true
                    }
                    
                } else {
                    permissionCheck = false
                }
            }
        @unknown default:
            print("Dafaukt casr < Image Picker class")
        }
        return permissionCheck
    }
    func checkMicPermission() -> Bool {
        
        var permissionCheck: Bool = false
        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            permissionCheck = true
        case AVAudioSession.RecordPermission.denied:
            permissionCheck = false
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                if granted {
                    permissionCheck = true
                } else {
                    permissionCheck = false
                }
            })
        default:
            break
        }
        
        return permissionCheck
    }
    func setValue() {
        
        print("ATDebug Detail Screen :: \(Double(selectedAdviserDetails?.avgRating ?? "0") ?? 0.0)")
        print("ATDebug Detail Screen :: \(selectedAdviserDetails?.ratings ?? 0)")
        print("ATDebug Detail Screen :: \(String(describing: selectedAdviserDetails?.nickName))")
        
        AverageRatiningView.rating = Double(selectedAdviserDetails?.avgRating ?? "0") ?? 0.0
        lblRatingAndCount.text = "\(selectedAdviserDetails?.ratings ?? 0)"
        lblAdviserName.text = selectedAdviserDetails?.nickName
        
        
        lblDescriptionText.delegate = self
        lblDescriptionText.setLessLinkWith(lessLink: "show less", attributes: [.foregroundColor:colors.white.value,.font: CustomFont.medium.returnFont(15)], position: .left)
        
        lblDescriptionText.shouldCollapse = true
        lblDescriptionText.textReplacementType = .word
        lblDescriptionText.numberOfLines = 6
        lblDescriptionText.collapsed = statesForColleps
        
        lblDescriptionText.text = selectedAdviserDetails?.advisorDescription.trimmingCharacters(in: .whitespacesAndNewlines)
        
        lblRatingAndCount.text = " \(selectedAdviserDetails?.avgRating ?? "0") (\(selectedAdviserDetails?.totalRating ?? "0"))"
        lblRatingAndCount.sizeToFit()
        
        
        let NewCategoryID = selectedAdviserDetails?.categories.split(separator: ",")
        
        categoriesData.removeAll()
        print(NewCategoryID ?? "")
        
        
        NewCategoryID?.forEach({ (NewCategoryIDElement) in
            let foundItems = selectedAdviserDetails?.availability.filter { $0.categoryId == NewCategoryIDElement}
            
            
            
            if foundItems?.count != 0 {
                
                var communicationTypeArray = [CommunicationType]()
                
                let matches1 = foundItems?.filter( {$0.type == "chat"})
                
                
                
                communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_chat") ?? UIImage(), name: indexColorForCommunicationType.chat.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.chat.setColorForCommunicationType()), price: String(format: "%.2f", ((matches1?.last?.price as NSString?)?.floatValue) ?? 0.00), discountPrice: String(format: "%.2f", ((matches1?.last?.discountPrice as NSString?)?.floatValue) ?? 0.00)))
                
                
                let matches2 = foundItems?.filter( {$0.type == "audio"})
                
                communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), name: indexColorForCommunicationType.audio.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.audio.setColorForCommunicationType()), price: String(format: "%.2f", ((matches2?.last?.price as NSString?)?.floatValue) ?? 0.00), discountPrice: String(format: "%.2f", ((matches2?.last?.discountPrice as NSString?)?.floatValue) ?? 0.00)))
                
                
                
                let matches3 = foundItems?.filter( {$0.type == "video"})
                communicationTypeArray.append(CommunicationType(image: UIImage(named: "ic_AdviserDetails_videoCall") ?? UIImage(), name: indexColorForCommunicationType.video.CommunicationTypeName, color: UIColor(hexString: indexColorForCommunicationType.video.setColorForCommunicationType()), price: String(format: "%.2f", ((matches3?.last?.price as NSString?)?.floatValue) ?? 0.00), discountPrice: String(format: "%.2f", ((matches3?.last?.discountPrice as NSString?)?.floatValue) ?? 0.00)))
                
                
                if SingletonClass.sharedInstance.categoryDataForAdviser?.category.count != 0 {
                    SingletonClass.sharedInstance.categoryDataForAdviser?.category.forEach({ (elementCategory) in
                        if elementCategory.id == NewCategoryIDElement
                        {
                            categoriesData.append(CategoriesArray(id: elementCategory.id, name: elementCategory.name, color: UIColor(hexString: categoryColorArray[categoriesData.count]).withAlphaComponent(0.25) , communicationType: communicationTypeArray))
                            
                        }
                    })
                }
                
            }
        })
        
        adviserProfileImage.contentMode = .scaleAspectFill
        let imgURL:String = "\(selectedAdviserDetails?.profilePicture ?? "")"
        if imgURL != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(imgURL)")
            
            adviserProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
            adviserProfileImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
            
            
        } else {
            adviserProfileImage.image = UIImage(named: "user_dummy_profile")
        }
        
        if let barbuttonItem = (self.navigationItem.rightBarButtonItems?.first) {
            if let customView = barbuttonItem.customView {
                if customView.isKind(of: UIView.self) {
                    let viewFromNavigation : UIView = customView
                    
                    if let CheckSubViews = viewFromNavigation.subviews.first {
                        if  CheckSubViews.isKind(of: UIButton.self) {
                            let btnRight = CheckSubViews as! UIButton
                            if selectedAdviserDetails?.isFavourite == "0" {
                                
                                btnRight.isSelected = false
                            } else {
                                
                                btnRight.isSelected = true
                            }
                            
                        }
                    }
                }
            }
        }
        
        tblRateAndReview.reloadDataWithAutoSizingCellWorkAround()
        tblRateAndReviewHeight.constant = tblRateAndReview.contentSize.height
        
        switch selectedAdviserDetails?.isAvailabile {
        case 0:
            adviserActiveStatusImageView.backgroundColor = colors.AdviserInActive.value
        case 1:
            adviserActiveStatusImageView.backgroundColor = colors.AdviserActive.value
        case 2:
            adviserActiveStatusImageView.backgroundColor = colors.AdviserBusy.value
        default:
            break
        }
        
        if selectedAdviserDetails?.isAvailabile == 0 {
            MainViewForNotifyMe.isHidden = false
        } else {
            MainViewForNotifyMe.isHidden = true
        }
        
        if categoriesData.count != 0 {
            if SelectedCategoryName != "All" {
                
                for i in 0...categoriesData.count - 1 {
                    if categoriesData[i].categoryName == SelectedCategoryName {
                        selectedCategoryType = i
                        break
                    }
                }
            } else if SelectedCategoryName == "" {
                selectedCategoryType = 0
            } else {
                selectedCategoryType = 0
            }
        }
        collectionViewCategories.reloadData()
        tblCommunicationType.reloadData()
        
        
        likeButtonClick = {
            if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                appDel.navigateToUserLogin()
                userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
            } else {
                let favouriteModel = FavouriteReqModel()
                favouriteModel.advisor_id = self.selectedAdviserDetails?.id ?? ""
                favouriteModel.user_id = SingletonClass.sharedInstance.UserId
                if self.rightSideButtonIsSelected == true{
                    favouriteModel.status = "1"
                } else {
                    favouriteModel.status = "0"
                }
                //Utilities.showHud()
                WebServiceSubClass.AddToFavourite(CategoryModel: favouriteModel, completion: {(json, status, response) in
                    //Utilities.hideHud()
                    if status {
                        
                    }
                })
            }
            
        }
        
        self.tblCommunicationType.reloadData()
        self.tblRateAndReview.reloadDataWithAutoSizingCellWorkAround()
        
        self.tblCommunicationType.layoutIfNeeded()
        self.tblRateAndReview.layoutIfNeeded()
        
        DispatchQueue.main.async {
            
            self.tblRateAndReviewHeight.constant = self.tblRateAndReview.contentSize.height
            self.tblRateAndReview.layoutIfNeeded()
            
            self.collectionViewCategories.scrollToItem(at: IndexPath(item: self.selectedCategoryType, section: 0), at: .right, animated: true)
            self.collectionViewCategories.layoutIfNeeded()
        }
        
    }
    
    //MARK: -  tableViewMethods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case tblCommunicationType:
            if categoriesData.count != 0 {
                return categoriesData[selectedCategoryType].communicationTypeArray.count
            }
            else {
                return 0
            }
        case tblRateAndReview:
            return selectedAdviserDetails?.reviewHistory.count ?? 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case tblCommunicationType:
            let cell = tblCommunicationType.dequeueReusableCell(withIdentifier: CommunicationTypeCell.reuseIdentifier, for: indexPath) as! CommunicationTypeCell
            
            if categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typePrice ?? "" == categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? ""
            {
                cell.lblPriceOffer.isHidden = true
                cell.lblPrice.text = "\(Currency)\(categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typePrice ?? "")/min"
            } else {
                cell.lblPriceOffer.isHidden = false
                cell.lblPriceOffer.attributedText = "\(Currency)\(categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typePrice ?? "")/min".strikeThrough(strickText: "\(Currency)\(categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typePrice ?? "")/min")
                
                cell.lblPrice.text = "\(Currency)\(categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? "")/min"
                
            }
            
            
            let cellType = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typeName!
            
            cell.nextBtn.cornerRadius = 8
            if cellType == "Text Chat" {
                cell.nextBtn.setTitle("CHAT NOW", for: .normal)
                cell.nextBtn.backgroundColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
                cell.lblPriceOffer.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
                cell.lblPrice.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
                callNowBtnColor = cell.nextBtn.backgroundColor
            }else if cellType == "Audio Call" {
                cell.nextBtn.setTitle("CALL NOW", for: .normal)
                cell.nextBtn.backgroundColor = UIColor.init(hexString: "ED1B23")
                cell.lblPriceOffer.textColor = UIColor.init(hexString: "ED1B23")
                cell.lblPrice.textColor = UIColor.init(hexString: "ED1B23")
            }else if cellType == "Video Call"{
                cell.nextBtn.setTitle("TALK NOW", for: .normal)
                cell.nextBtn.backgroundColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
                cell.lblPriceOffer.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
                cell.lblPrice.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
            }else {
                cell.nextBtn.setTitle("", for: .normal)
            }
            
            cell.lblTypeTitle.text = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typeName?.uppercased()
//            cell.lblPrice.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
//            cell.lblPrice.textColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
            cell.BGView.layer.borderColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor?.cgColor
            cell.typeImage.image = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].typeImage
            cell.nextImage.image = UIImage(named:"ic_AdviserDetails_forward")
            cell.nextImage.image = cell.nextImage.image?.withRenderingMode(.alwaysTemplate)
            
            cell.nextImage.tintColor = categoriesData[selectedCategoryType].communicationTypeArray[indexPath.row].priceColor
            
            return cell
        case tblRateAndReview:
            
            let cell = tblRateAndReview.dequeueReusableCell(withIdentifier: RateAndReviewCell.reuseIdentifier, for: indexPath) as! RateAndReviewCell
            
            
            cell.lblRattingDate.text = convertDateFormater(selectedAdviserDetails?.reviewHistory?[indexPath.row].createdAt ?? "")
            
            
            cell.lbluserName.text = selectedAdviserDetails?.reviewHistory?[indexPath.row].fullName
            
            if (selectedAdviserDetails?.reviewHistory?[indexPath.row].profilePicture ?? "") == "" {
                cell.UserImage.image = UIImage(named: "user_dummy_profile")
            } else {
                
                let imgURL = selectedAdviserDetails?.reviewHistory?[indexPath.row].profilePicture ?? ""
                
                let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                
                cell.UserImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                
                cell.UserImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
            }
            cell.UserImage.contentMode = .scaleAspectFill
            
            cell.lblRattingDescription.delegate = self
            
            if selectedAdviserDetails?.reviewHistory?[indexPath.row].rating ?? "" == "" {
                cell.lblRattingDescription.numberOfLines = 0
            } else {
                cell.lblRattingDescription.numberOfLines = 3
            }
            
             
            cell.lblRattingDescription.collapsed = states?[indexPath.row] ?? true
            
            //            cell.lblTotalRatting.text = selectedAdviserDetails?.reviewHistory?[indexPath.row].rating ?? ""
            cell.lblTotalRatting.text = selectedAdviserDetails?.reviewHistory?[indexPath.row].rating ?? ""
            cell.RatiningView.rating = Double(selectedAdviserDetails?.reviewHistory?[indexPath.row].rating ?? "") ?? 0.0
            cell.lblRattingDescription.text = self.selectedAdviserDetails?.reviewHistory?[indexPath.row].note ?? ""
            
            cell.layoutIfNeeded()
            cell.layoutSubviews()
            return cell
            
        default:
            return UITableViewCell()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Utilities.showHud()
        GetAdvisorDetailsForUpdate(userId: SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "", advisorID: selectedAdviserDetails?.id ?? ""){ (success) in
            if success == true{
                Utilities.hideHud()
                if let barbuttonItem = (self.navigationItem.rightBarButtonItems?.first) {
                    if let customView = barbuttonItem.customView {
                        if customView.isKind(of: UIView.self) {
                            let viewFromNavigation : UIView = customView
                            
                            if let CheckSubViews = viewFromNavigation.subviews.first {
                                if  CheckSubViews.isKind(of: UIButton.self) {
                                    print("This is an label view from Detail screen")
                                }
                            }
                        }
                    }
                }
                
                switch tableView {
                case self.tblCommunicationType:
                    if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                        appDel.navigateToUserLogin()
                        userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                    } else {
                        
                        switch self.selectedAdviserDetails?.isAvailabile ?? -1 {
                        case 0:
                            Utilities.ShowAlert(OfMessage: "Advisor is not available")
                        case 1:
                            WebServiceSubClass.initApi(strParams: SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "", showHud: true, completion: { (json, status, error) in
                                
                                if status {
                                    SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = json["wallet_amount"].stringValue
                                    SingletonClass.sharedInstance.WalletBalanceForMaintain = ""
        //                            SingletonClass.sharedInstance.WalletBalanceForMaintain = SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount ?? ""
                                    let arrayForAvailable = (self.selectedAdviserDetails?.availabileFor ?? "").components(separatedBy: ",")
        //                            arrayForAvailable.append("Audio")
                                    print()
                                    switch self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeName {
                                    case indexColorForCommunicationType.audio.CommunicationTypeName:
                                        //
                                        
                                        
                                        if arrayForAvailable.contains("audio")  || arrayForAvailable.contains("Audio") {
                                            if self.checkMicPermission() {
                                                
                                                if self.checkMicPermission() {
                                                    
                                                    let DataForShare:[String:Any] = ["advisor_name":self.selectedAdviserDetails?.nickName ?? "","advisor_profile_picture":self.selectedAdviserDetails?.profilePicture ?? "","booking_id":0,"advisor_id":Int(self.selectedAdviserDetails?.id ?? "") ?? 0,"minute":"0",
                                                    ]
                                                    SingletonClass.sharedInstance.DataForAudioVideo = DataForShare
                                                    let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                                                    controller.isBuyNow = false
                                                    controller.appBorderColor = self.callNowBtnColor
                                                    
                                                    controller.closourForBookRequest = {
                                                        self.dismiss(animated: true, completion: nil)
                                                        
                                                        self.GoToAudioCallScreen(DataForRequestPush: DataForShare)
                                                        
                                                        
                                                    }
                                                    controller.CommunicationType = "audio"
                                                    
                                                    controller.AdvisorID = self.selectedAdviserDetails?.id ?? ""
                                                    
                                                    controller.PriceOfSelectedCategory = self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? ""
                                                    controller.CategoryID = self.categoriesData[self.selectedCategoryType].categoryID ?? ""
                                                    AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_audioCall)

                                                    let navigationController = UINavigationController(rootViewController: controller)
                                                    navigationController.modalPresentationStyle = .overCurrentContext
                                                    navigationController.modalTransitionStyle = .crossDissolve
                                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                                    
                                                    SingletonClass.sharedInstance.dataForShareForBuyMinutes = ["typeName":controller.CommunicationType,"AdvisorID":self.selectedAdviserDetails?.id ?? "", "PriceOfSelectedCategory":self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? "","CategoryID":self.categoriesData[self.selectedCategoryType].categoryID ?? ""]
                                                    
                                                } else {
                                                    self.ShowMicroPhonePermissionAlert()
                                                }
                                                
                                            }else {
                                                self.ShowMicroPhonePermissionAlert()
                                            }
                                        } else {
                                            Utilities.ShowAlert(OfMessage: "Advisor is not available for \(indexColorForCommunicationType.audio.CommunicationTypeName.lowercased())")
                                        }
                                        
                                    case indexColorForCommunicationType.video.CommunicationTypeName:
                                        
                                        if arrayForAvailable.contains("video")  || arrayForAvailable.contains("Video") {
                                            if self.checkMicPermission() {
                                                if self.checkCameraAccess() {
                                                    let DataForShare = ["CallingTo":self.selectedAdviserDetails?.nickName ?? "",
                                                                        "senderName":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "",
                                                                        "strRoomName":"",
                                                                        "token":"",
                                                                        "visitID":"",
                                                                        "receiverId":self.selectedAdviserDetails?.id ?? "",
                                                                        "senderId":SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "",
                                                                        "totalMinutes":"0"]
                                                    SingletonClass.sharedInstance.DataForAudioVideo = DataForShare
                                                    let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                                                    controller.isBuyNow = false
                                                    controller.appBorderColor = self.callNowBtnColor
                                                    
                                                    controller.closourForBookRequest = {
                                                        self.dismiss(animated: true, completion: nil)
                                                        
                                                        self.GoToVideoCallScreen()
                                                        
                                                    }
                                                    controller.CommunicationType = "video"
                                                    
                                                    controller.AdvisorID = self.selectedAdviserDetails?.id ?? ""
                                                    
                                                    controller.PriceOfSelectedCategory = self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? ""
                                                    controller.CategoryID = self.categoriesData[self.selectedCategoryType].categoryID ?? ""
                                                    AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_videoCall)
                                                    let navigationController = UINavigationController(rootViewController: controller)
                                                    navigationController.modalPresentationStyle = .overCurrentContext
                                                    navigationController.modalTransitionStyle = .crossDissolve
                                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                                    
                                                    SingletonClass.sharedInstance.dataForShareForBuyMinutes = ["typeName":controller.CommunicationType,"AdvisorID":self.selectedAdviserDetails?.id ?? "", "PriceOfSelectedCategory":self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? "","CategoryID":self.categoriesData[self.selectedCategoryType].categoryID ?? ""]
                                                } else {
                                                    self.alertToEncourageCameraAccessInitially()
                                                }
                                            } else {
                                                self.ShowMicroPhonePermissionAlert()
                                            }
                                            
                                        } else {
                                            Utilities.ShowAlert(OfMessage: "Advisor is not available for \(indexColorForCommunicationType.video.CommunicationTypeName.lowercased())")
                                        }
                                        
                                    case indexColorForCommunicationType.chat.CommunicationTypeName:
                                        
                                        if arrayForAvailable.contains("chat")  || arrayForAvailable.contains("Chat") {
                                            
                                            let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                                            controller.isBuyNow = false
                                            controller.appBorderColor = self.callNowBtnColor
                                            
                                            controller.closourForBookRequest = {
                                                self.dismiss(animated: true, completion: nil)
                                                let CommonLabelController:CommonLabelPopupViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonLabelPopupViewController.storyboardID) as! CommonLabelPopupViewController
                                                
                                                CommonLabelController.textForShow = "Please wait for advisor approval"
                                                //  let navigationController = UINavigationController(rootViewController: CommonLabelController)
                                                CommonLabelController.modalPresentationStyle = .overCurrentContext
                                                CommonLabelController.modalTransitionStyle = .crossDissolve
                                                appDel.window?.rootViewController?.present(CommonLabelController, animated: true, completion: nil)
                                                
                                            }
                                            controller.CommunicationType = "chat"
                                            
                                            controller.AdvisorID = self.selectedAdviserDetails?.id ?? ""
                                            
                                            controller.PriceOfSelectedCategory = self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? ""
                                            controller.CategoryID = self.categoriesData[self.selectedCategoryType].categoryID ?? ""
                                            AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_textChatAdvisor)
                                            let navigationController = UINavigationController(rootViewController: controller)
                                            navigationController.modalPresentationStyle = .overCurrentContext
                                            navigationController.modalTransitionStyle = .crossDissolve
                                            appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                            
                                            SingletonClass.sharedInstance.dataForShareForBuyMinutes = ["typeName":controller.CommunicationType,"AdvisorID":self.selectedAdviserDetails?.id ?? "", "PriceOfSelectedCategory":self.categoriesData[self.selectedCategoryType].communicationTypeArray[indexPath.row].typeDiscountPrice ?? "","CategoryID":self.categoriesData[self.selectedCategoryType].categoryID ?? ""]
                                            
                                            
                                        } else {
                                            Utilities.ShowAlert(OfMessage: "Advisor is not available for \(indexColorForCommunicationType.chat.CommunicationTypeName.lowercased())")
                                        }
                                        
                                    default:
                                        Utilities.ShowAlert(OfMessage: "")
                                    }
                                    
                                } else {
                                    
                                }
                            })
                            
                        case 2:
                            Utilities.ShowAlert(OfMessage: "Advisor is busy on another call")
                            
                        default:
                            break
                        }
                    }
                    
                default:
                    break
                }
            }else{
                Utilities.hideHud()
                Utilities.ShowAlert(OfMessage: "Something went wrong!")
            }
        }
       
    }
    
    func GoToVideoCallScreen() {
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {
                    
                    print("NEW" + #function)
                    
                    
                    if let topController = UIApplication.topViewController() {
                        let controller:CustomerVideoCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomerVideoCallViewController.storyboardID) as! CustomerVideoCallViewController
                        
                        
                        controller.CallingTo = self.selectedAdviserDetails?.nickName ?? ""
                        controller.senderName = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? ""
                        controller.strRoomName = ""
                        //               vc.visit_id = visit_id
                        controller.token = ""
                        controller.visitID = ""
                        controller.receiverId = self.selectedAdviserDetails?.id ?? ""
                        controller.senderId = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                        controller.totalMinutes = "0"
                        controller.navigationController?.isNavigationBarHidden = true
                        
                        topController.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                    
                })
            } else {
                if let topController = UIApplication.topViewController() {
                    let controller:CustomerVideoCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CustomerVideoCallViewController.storyboardID) as! CustomerVideoCallViewController
                    
                    
                    controller.CallingTo = self.selectedAdviserDetails?.nickName ?? ""
                    controller.senderName = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? ""
                    controller.strRoomName = ""
                    //               vc.visit_id = visit_id
                    controller.token = ""
                    controller.visitID = ""
                    controller.receiverId = self.selectedAdviserDetails?.id ?? ""
                    controller.senderId = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                    controller.totalMinutes = "0"
                    controller.navigationController?.isNavigationBarHidden = true
                    
                    topController.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    func GoToAudioCallScreen(DataForRequestPush:[String:Any]) {
        
        
        
        if let topVC = UIApplication.topViewController() {
            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {
                    
                    print("NEW" + #function)
                    
                    
                    if let topController = UIApplication.topViewController() {
                        let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
                        
                        controller.CallToAdvisorName = "\((DataForRequestPush["advisor_name"] as? String ?? "").replacingOccurrences(of: " ", with: "_"))_\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                        
                        controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"
                        
                        controller.advisorName = DataForRequestPush["advisor_name"] as? String ?? ""
                        controller.advisorUserImageURl = DataForRequestPush["advisor_profile_picture"] as? String ?? ""
                        
                        controller.bookingID = "\(DataForRequestPush["booking_id"] as? Int ?? 0)"
                        controller.AdvisorID = "\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                        controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                        controller.totalMinutes = DataForRequestPush["minute"] as? String ?? ""
                        
                        topController.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                    
                })
            } else {
                if let topController = UIApplication.topViewController() {
                    let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
                    
                    controller.CallToAdvisorName = "\((DataForRequestPush["advisor_name"] as? String ?? "").replacingOccurrences(of: " ", with: "_"))_\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                    controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"
                    
                    controller.advisorName = DataForRequestPush["advisor_name"] as? String ?? ""
                    controller.advisorUserImageURl = DataForRequestPush["advisor_profile_picture"] as? String ?? ""
                    
                    controller.bookingID = "\(DataForRequestPush["booking_id"] as? Int ?? 0)"
                    controller.AdvisorID = "\(DataForRequestPush["advisor_id"] as? Int ?? 0)"
                    controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
                    controller.totalMinutes = DataForRequestPush["minute"] as? String ?? ""
                    topController.navigationController?.pushViewController(controller, animated: true)
                }
            }
        }
    }
    
    //MARK: -  CollectionViewMethods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categoriesData.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ((categoriesData[indexPath.row].categoryName?.uppercased() ?? "").sizeOfString(usingFont: CustomFont.medium.returnFont(12)).width) + 30
                      , height: collectionViewCategories.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionViewCategories.dequeueReusableCell(withReuseIdentifier: categoriesCell.reuseIdentifier, for: indexPath) as! categoriesCell
        cell.BGView.backgroundColor = categoriesData[indexPath.row].categoryBgColor
        cell.categoryName.text = categoriesData[indexPath.row].categoryName?.uppercased()
        
        cell.categoryName.layer.cornerRadius = 15
        cell.BGView.layer.cornerRadius = 15
        cell.layer.cornerRadius = 15
        cell.clipsToBounds = true
        cell.BGView.clipsToBounds = true
        cell.categoryName.clipsToBounds = true
        
        cell.BGView.layer.borderColor = colors.white.value.cgColor
        if indexPath.row == selectedCategoryType {
            cell.BGView.layer.borderWidth = 1
        } else {
            cell.BGView.layer.borderWidth = 0
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCategoryType = indexPath.row
        collectionViewCategories.reloadData()
        tblCommunicationType.reloadData()
        
        DispatchQueue.main.async {
            self.collectionViewCategories.scrollToItem(at: IndexPath(item: self.selectedCategoryType, section: 0), at: .right, animated: true)
            self.collectionViewCategories.layoutIfNeeded()
        }
        
        
        
        
    }
    //MARK: - Emit Video Call
    func emitVideoCall(bookingID:Int) {
        
        let joinAdvisor = ["customer_id": Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,"customer_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","advisor_id":Int(selectedAdviserDetails?.id ?? "") ?? 0,"advisor_name":selectedAdviserDetails?.nickName ?? "","booking_id":bookingID] as [String : Any]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCall.rawValue , with: joinAdvisor)
        
    }
    
    //MARK: -  IBActions
    
    @IBAction func btnNotifyMeClick(_ sender: Any) {
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            appDel.navigateToUserLogin()
            userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
        } else {
            WebserviceForButtonNotifyMe()
        }
        //self.btnNotifyMe.setTitle("Notified", for: .normal)
        
    }
    @IBAction func btnShowAllRatings(_ sender: Any) {
        let controller = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdviserRatingsReviewsViewController.storyboardID) as! AdviserRatingsReviewsViewController
        //
        controller.isFromCustomer = true
        controller.AdvisorID = selectedAdviserDetails?.id ?? ""
        controller.hidesBottomBarWhenPushed = true
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnBuyMinutesClick(_ sender: Any) {
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            appDel.navigateToUserLogin()
            userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
            //            SingletonClass.sharedInstance.selectedCategoryIndex = -1
            //            SingletonClass.sharedInstance.category_id = ""
            //            SingletonClass.sharedInstance.descriptionForSelectedCategories = ""
            //            SingletonClass.sharedInstance.selectedTypeAdvisor = ""
        } else {
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
            controller.appBorderColor = self.callNowBtnColor
            let navigationController = UINavigationController(rootViewController: controller)
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            appDel.window?.rootViewController?.present(controller, animated: true, completion: nil)
        }
        
        
        //self.navigationController?.pushViewController(controller, animated: true)
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "dd MMM"
        return  dateFormatter.string(from: date)
        
    }
    //MARK: -  API Calls
    
    
    
    
}
class CommunicationTypeCell : UITableViewCell {
    @IBOutlet weak var BGView: AdviserDetailsView!
    @IBOutlet weak var nextImage: UIImageView!
    @IBOutlet weak var lblPrice: adviserDetailsLabel!
    @IBOutlet weak var lblPriceOffer: adviserDetailsLabel!
    @IBOutlet weak var lblTypeTitle: adviserDetailsLabel!
    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var nextBtn: UIButton!
}
class RateAndReviewCell : UITableViewCell {
    @IBOutlet weak var RatiningView: CosmosView!
    @IBOutlet weak var lblTotalRatting: adviserDetailsLabel!
    @IBOutlet weak var lblRattingDescription: AdvisorHomeScreenDescriptionLabel!
    @IBOutlet weak var lblRattingDate: adviserDetailsLabel!
    @IBOutlet weak var lbluserName: adviserRattingLabel!
    @IBOutlet weak var UserImage: UIImageView!
    
    override func awakeFromNib() {
        RatiningView.isUserInteractionEnabled = false
        
        lblRattingDescription.setLessLinkWith(lessLink: "show less", attributes: [.foregroundColor:colors.white.value,.font: CustomFont.medium.returnFont(15)], position: .left)
        
        
        
        lblRattingDescription.shouldCollapse = true
        lblRattingDescription.textReplacementType = .word
    }
}
class categoriesCell : UICollectionViewCell {
    @IBOutlet weak var BGView: UIView!
    @IBOutlet weak var categoryName: adviserDetailsLabel!
    
    override func awakeFromNib() {
        BGView.layer.cornerRadius = 15
    }
    
}
class CategoriesArray {
    var categoryID : String?
    var categoryName : String?
    var categoryBgColor : UIColor?
    var communicationTypeArray:[CommunicationType]
    init(id:String,name:String,color:UIColor,communicationType:[CommunicationType]) {
        self.categoryID = id
        self.categoryName = name
        self.categoryBgColor = color
        self.communicationTypeArray = communicationType
    }
}
class rateAndReview {
    var userImage : UIImage?
    var userName : String?
    var userReview : String?
    var userReviewDate : String?
    var userRatting : String?
    
    init(image:UIImage,name:String,review:String,reviewDate:String,ratting:String) {
        self.userImage = image
        self.userName = name
        self.userReview =  review
        self.userReviewDate = reviewDate
        self.userRatting = ratting
    }
}

class CommunicationType {
    var typeImage : UIImage?
    var typeName : String?
    var priceColor : UIColor?
    var typePrice : String?
    var typeDiscountPrice : String?
    
    init(image: UIImage,name:String,color:UIColor,price : String,discountPrice : String) {
        self.typeImage = image
        self.typeName = name
        self.priceColor = color
        self.typePrice = price
        self.typeDiscountPrice = discountPrice
    }
}
enum indexColorForCommunicationType {
    case chat,audio,video
    func setColorForCommunicationType() -> String {
        switch self {
        case .chat:
            return "#2AD916"
        case .audio:
            return "#F3A791"
        case .video:
            return "#22A9E5"
        }
    }
    
    var CommunicationTypeName : String {
        switch self {
        case .chat:
            return "Text Chat"
        case .audio:
            return "Audio Call"
        case .video:
            return "Video Call"
        }
    }
    
}
enum CategoryTypeColor {
    case Category1,Category2,Category3
    func colorHexString() -> String {
        switch self {
        case .Category1:
            return "#88FF00"
        case .Category2:
            return "#CF6B6B"
        case .Category3:
            return "#6B86CF"
        }
    }
}

extension String {
    func strikeThrough(strickText:String) -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        let rangeToUnderLine = (self as NSString).range(of: strickText)
        print(rangeToUnderLine)
        attributeString.addAttribute(
            .strikethroughStyle,
            value:  NSUnderlineStyle.single.rawValue, range: rangeToUnderLine)
        print(attributeString)
        return attributeString
    }
}


extension String {
    func strThr(strickText:String) -> NSAttributedString {
        let attributeString =  NSMutableAttributedString(string: self)
        let range = attributeString.mutableString.range(of: strickText)
        
        attributeString.replaceCharacters(
            in: range,
            with: NSAttributedString(
                string: strickText, attributes: [
                    .strikethroughStyle: NSUnderlineStyle.single.rawValue,
                    .strikethroughColor: UIColor.systemRed,
                    .foregroundColor: UIColor.systemRed,
                ]
            )
        )
        return attributeString
    }
}
class PaddingLabel: UILabel {
    
    var topInset: CGFloat = 0.0
    var bottomInset: CGFloat = 0.0
    var leftInset: CGFloat = 10.0
    var rightInset: CGFloat = 10.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
}
extension AdviserDetailsViewController {
    
    func startTimer(withInterval interval: Double) {
        if internalTimer != nil {
            internalTimer?.invalidate()
        }
        
        // jobs.append(job)
        internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute:  { [self] in
            let controller:MessageViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MessageViewController.storyboardID) as! MessageViewController
            controller.sessionType = "session"
            controller.RequestAcceptDataOfUser = self.RequestAcceptData
            controller.timerSecond = TotalTimecounter
            SingletonClass.sharedInstance.dataForShareForBuyMinutes = ["typeName": SingletonClass.sharedInstance.dataForShareForBuyMinutes["typeName"] as? String ?? "","AdvisorID": SingletonClass.sharedInstance.dataForShareForBuyMinutes["AdvisorID"] as? String ?? "", "PriceOfSelectedCategory":SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "","booking_id":RequestAcceptData["booking_id"] as? Int ?? 0]
            
            
            //  DataForBuyAgain = ["typeName": DataForBuyAgain["typeName"] as? String ?? "","AdvisorID": DataForBuyAgain["AdvisorID"] as? String ?? "", "PriceOfSelectedCategory":DataForBuyAgain["PriceOfSelectedCategory"] as? String ?? "","booking_id":RequestAcceptData["booking_id"] as? Int ?? 0]
            controller.bookingID = "\(self.RequestAcceptData["booking_id"] ?? 0)"
            self.navigationController?.pushViewController(controller, animated: true)
        })
        
    }
    func stopReminderTimer() {
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: CommunicationReminerViewController.self) {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        
                    })
                    ResumeInternalTimer()
                }
            } else if topVC.isKind(of: MessageViewController.self) {
                ResumeInternalTimer()
            }
        }
        
        CommunicationScreenRemiderScreen = SecondsForReminderScreen
        TimerForReminderScreen.invalidate()
        print("Total Timer count is dont :: \(TotalTimecounter)")
    }
    
    func stopTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        // jobs = [()->()]()
        
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : self.RequestAcceptData["advisor_id"] as? Int ?? 0,
                           "booking_id" : self.RequestAcceptData["booking_id"] as? Int ?? 0] as [String : Any]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.stop_timer.rawValue , with: joinAdvisor)
        
        internalTimer?.invalidate()
    }
    func stopTimerOnEndSession() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        // jobs = [()->()]()
        
        
        
        internalTimer?.invalidate()
    }
    @objc func updateTimerForReminderScreen() {
        if CommunicationScreenRemiderScreen != 0 {
            print("CommunicationScreenRemiderScreen :: \(CommunicationScreenRemiderScreen)")
            CommunicationScreenRemiderScreen -= 1     //This will decrement(count down)the seconds.
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommunicationReminerViewController.self) {
                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
                    let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
                    Controller.lblTimeLeft.text = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
                    Controller.lblChatReminderDescription.text = "Your chat session will be resume after \(String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)) minute!"
                }
            }
        } else {
            stopReminderTimer()
        }
    }
    func GoToReminderScreen() {
        if let topVC = UIApplication.topViewController() {
            let controllerForCommunication = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommunicationReminerViewController.storyboardID) as! CommunicationReminerViewController
            //let navigationController = UINavigationController(rootViewController: controllerForCommunication)
            controllerForCommunication.SessionType = "chat session"
            let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
            controllerForCommunication.timePending = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
            controllerForCommunication.closourForResume = { [self] in
                stopReminderTimer()
                // print("Total Timer count is dont :: \(TotalTimecounter)")
            }
            controllerForCommunication.closourForBuyMinutes = { [self] in
                CommunicationScreenRemiderScreen = SecondsForReminderScreen
                TimerForReminderScreen.invalidate()
                if SingletonClass.sharedInstance.freeMinutesAdded.0 == true {
                    let RemoveFreeMinutes = totalMinutesTimer - (Int(SingletonClass.sharedInstance.freeMinutesAdded.1) ?? 0)
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(RemoveFreeMinutes) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                } else {
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                }
                
                // SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                controller.isBuyNow = true
                controller.appBorderColor = self.callNowBtnColor
                controller.closourForClickOnBack = {
                    ResumeInternalTimer()
                    
                }
                
                controller.closourForBookRequest = {
                    self.dismiss(animated: true, completion: nil)
                    
                    self.totalMinutesTimer = self.totalMinutesTimer + (Int("\(controller.textFieldSelectMinutes.text ?? "")") ?? 1)
                    
                    ResumeInternalTimer()
                    
                }
                
                controller.CommunicationType = SingletonClass.sharedInstance.dataForShareForBuyMinutes["typeName"] as? String ?? ""
                
                controller.AdvisorID = SingletonClass.sharedInstance.dataForShareForBuyMinutes["AdvisorID"] as? String ?? ""
                
                controller.PriceOfSelectedCategory = SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? ""
                controller.CategoryID = SingletonClass.sharedInstance.dataForShareForBuyMinutes["CategoryID"] as? String ?? ""
                controller.BoookingID = "\(self.RequestAcceptData["booking_id"] as? Int ?? 0)"
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
            }
            
            controllerForCommunication.modalPresentationStyle = .overCurrentContext
            controllerForCommunication.modalTransitionStyle = .crossDissolve
            topVC.present(controllerForCommunication, animated: true, completion: nil)
        }
    }
    @objc func doJob() {
        let CheckTime = (totalMinutesTimer * 60) - 30
        if TotalTimecounter == CheckTime {
            stopTimer()
            CommunicationScreenRemiderScreen = SecondsForReminderScreen
            TimerForReminderScreen = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimerForReminderScreen)), userInfo: nil, repeats: true)
            
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: UIAlertController.self) {
                    topVC.dismiss(animated: true, completion: {
                        self.GoToReminderScreen()
                    })
                } else if topVC.isKind(of: MessageViewController.self) {
                    GoToReminderScreen()
                } else {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            self.GoToReminderScreen()
                        })
                    } else {
                        topVC.navigationController?.popViewController(animated: true)
                        GoToReminderScreen()
                    }
                }
            }
            TotalTimecounter += 1
        } else if TotalTimecounter == (CheckTime + 30) {
            endSession()
            
            if let topVC = UIApplication.topViewController() {
                
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        
                        if let MsgVC = UIApplication.topViewController() {
                            if MsgVC.isKind(of: MessageViewController.self) {
                                MsgVC.navigationController?.popViewController(animated: true)
                            }
                        }
                    })
                } else {
                    topVC.navigationController?.popViewController(animated: true)
                }
            }
            print("you need to dismiss message screen")
        } else {
            if let topVC = UIApplication.topViewController() {
                
                if topVC.isKind(of: MessageViewController.self) {
                    let Controller:MessageViewController = topVC as! MessageViewController
                    
                    Controller.timerSecond = TotalTimecounter + 1
                    Controller.setTimeLabel()
                }
            }
            print("go forward")
            TotalTimecounter += 1
        }
        print(totalMinutesTimer)
        print(TotalTimecounter)
    }
    func ResumeInternalTimer() {
        self.internalTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.doJob), userInfo: nil, repeats: true)
        
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : self.RequestAcceptData["advisor_id"] as? Int ?? 0,
                           "booking_id" : self.RequestAcceptData["booking_id"] as? Int ?? 0] as [String : Any]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.resume_timer.rawValue , with: joinAdvisor)
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (0, (seconds / 60), (seconds % 60))
//        return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func endSession() {
        SingletonClass.sharedInstance.WalletBalanceForMaintain = ""
        let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
        let param = [
            "user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
            "advisor_id" : self.RequestAcceptData["advisor_id"] as? Int ?? 0,
            "booking_id" : self.RequestAcceptData["booking_id"] as? Int ?? 0,
            "minute" : String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2),
            "amount" : SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "",
            "dob" : SingletonClass.sharedInstance.loginForCustomer?.profile.dob ?? ""
            
        ] as [String : Any]
        
        print(param)
        SocketIOManager.shared.socketEmit(for: socketApiKeys.end_session.rawValue, with: param)
        stopTimerOnEndSession()
        
    }
    
    
}
extension AdviserDetailsViewController {
    func WebserviceForButtonNotifyMe() {
        let NotifyMeModel = NotifyMeReqModel()
        NotifyMeModel.advisor_id = self.selectedAdviserDetails?.id ?? ""
        NotifyMeModel.user_id = SingletonClass.sharedInstance.UserId
        
        WebServiceSubClass.NotifyMe(CategoryModel: NotifyMeModel, completion: {(json, status, response) in
            
            if status {
                self.MainViewForNotifyMe.isHidden = true
                //self.btnNotifyMe.setTitle("Notified", for: .normal)
            } else {
                self.MainViewForNotifyMe.isHidden = false
                //self.btnNotifyMe.setTitle("Notify Me", for: .normal)
            }
        })
    }
    func GetAdvisorDetailsForUpdate(userId:String,advisorID:String , completionHandler: @escaping (Bool) -> Swift.Void) {
        
        print(#function)
        let ReqModelData = AdvisorDetailsReqModel()
        ReqModelData.user_id = userId
        ReqModelData.advisor_id = advisorID
        webserviceForGetAdvisorDetailsForUpdate(reqModel: ReqModelData){ (success) in
            completionHandler(success)
        }
        
    }
    func webserviceForGetAdvisorDetailsForUpdate(reqModel:AdvisorDetailsReqModel , completionHandler: @escaping (Bool) -> Swift.Void) {
        
        print(#function)
        WebServiceSubClass.AdvisorDetails(addCategory: reqModel, completion: {(json, status, response) in
            self.MainScrollView.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            self.refreshControl.endRefreshing()
            if status {
                
                let AdvisorData = AdvisorDetailResModel.init(fromJson: json)
                
                self.selectedAdviserDetails = AdvisorData.advisorDetails
                self.setValue()
                completionHandler(true)
            } else {
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
                completionHandler(false)
            }
        })
    }
    
}
