//
//  MessageViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import NVActivityIndicatorView
import SwiftyJSON
import IQKeyboardManagerSwift
class MessageViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate, UITextViewDelegate {

    //MARK:- ======== Properties ========
    @IBOutlet weak var txtMessage: themeTextView!
    @IBOutlet weak var textHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnSendMessage: UIButton!
    
    var timerSecond : Int = -1
    
    var customTabBarController : CustomTabBarVC?
   
    let NotificationSocketTypingGet = NSNotification.Name(rawValue: "NotificationSocketTypingGet")
    var RequestAcceptDataOfUser : [String : Any] = [:]
    var timer: Timer?
    var timeLeft = 5
    var senderImage = UIImage()
    var reciverImage = UIImage()
    var userName = "Lina Ryan"
    var MessageArray = [ChatHistory]()
    
    var sessionType = ""
    var bookingID = ""
    //MARK:- ======== IBOutlets ========
    
    @IBOutlet weak var lblShowTimer: customerMessageTimeLabel!
   // @IBOutlet weak var SendMessageHeight: NSLayoutConstraint!
    @IBOutlet weak var viewIndicator: NVActivityIndicatorView!
    
    @IBOutlet weak var viewTyping: viewViewClearBG!
    @IBOutlet weak var communicationTypeImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var conBottomChatBox: NSLayoutConstraint!
    @IBOutlet weak var advisorImageView: UIImageView!
    @IBOutlet weak var tblShowMessageTop: NSLayoutConstraint!
    @IBOutlet weak var tblShowMessage: UITableView!
    @IBOutlet weak var textFieldSendMessage: SendMessageTextField!
    @IBOutlet weak var lblTypingStatus: RegistrationPageLabel!
    @IBOutlet weak var costomerNameLbl: UILabel!
    @IBOutlet weak var advisorNameLbl: UILabel!
    
   // @IBOutlet weak var tblShowMessageHeight: NSLayoutConstraint!
    //MARK:- ======== View Life Cycle Methods ========
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        costomerNameLbl.text = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? ""
//        advisorNameLbl.text =  "\(RequestAcceptDataOfUser["advisor_name"] ?? "")"
        
        costomerNameLbl.text = ""
        advisorNameLbl.text =  ""
        
        txtMessage.delegate = self
        userImageView.contentMode = .scaleAspectFill
        advisorImageView.contentMode = .scaleAspectFill
        adjustTextViewHeight()
        lblShowTimer.text = "00:00"
        if sessionType == "session" {
            self.txtMessage.resignFirstResponder()
            self.txtMessage.superview?.isHidden = false
            timerSecond = -1
            self.lblShowTimer.isHidden = false
        } else {
            self.txtMessage.resignFirstResponder()
            self.txtMessage.superview?.isHidden = true
            timerSecond = 0
            self.lblShowTimer.isHidden = true
        }
             
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        tblShowMessage.keyboardDismissMode = .onDrag

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        if sessionType == "session" || sessionType == "Session" {
            setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Chat", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture ?? "")
        } else {
            setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "Message", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture ?? "")
        }
       
        
        //tblShowMessage.scrollToBottom()
        
//       cx
        textFieldSendMessage.sendMessageClick = { [self] in
            (textFieldSendMessage.rightView?.subviews[0] as? UIButton)?.isUserInteractionEnabled = false
            if textFieldSendMessage.text?.trim() != "" {

                
                if SocketIOManager.shared.socket.status == .connected {
                    emitSocketSendMesage(message: textFieldSendMessage.text?.trim() ?? "")
                }
                
            }
            else{
                (textFieldSendMessage.rightView?.subviews[0] as? UIButton)?.isUserInteractionEnabled = true
            }
            
           
        }
        
        backButtonClick = {
            //self.NavBackButton.isUserInteractionEnabled = false
            if self.timerSecond != 0 {
                let alert = UIAlertController(title: AppInfo.appName , message: "Are you sure to end the session?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in


                    for controller in self.navigationController!.viewControllers as Array {
                        if controller.isKind(of: AdviserDetailsViewController.self) {
                            let MessageVC:AdviserDetailsViewController = controller as! AdviserDetailsViewController
                            MessageVC.endSession()
                            break
                        }
                    }

                    self.navigationController?.popViewController(animated: true)
                    //self.NavBackButton.isUserInteractionEnabled = true

                }))
                alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in

                    //self.NavBackButton.isUserInteractionEnabled = true
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
                //self.NavBackButton.isUserInteractionEnabled = true
            }
//            if self.timerSecond != -1 {
//
//
//
//            }
//            else {
//
//                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
//                    if self.timerSecond != 0 {
//                        let alert = UIAlertController(title: AppInfo.appName , message: "Are you sure to end the session?", preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
//
//
//                            for controller in self.navigationController!.viewControllers as Array {
//                                if controller.isKind(of: AdviserDetailsViewController.self) {
//                                    let MessageVC:AdviserDetailsViewController = controller as! AdviserDetailsViewController
//                                    MessageVC.endSession()
//
//                                    break
//                                }
//                            }
//
//                            self.navigationController?.popViewController(animated: true)
//                            self.NavBackButton.isUserInteractionEnabled = true
//                        }))
//                        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { action in
//                            self.NavBackButton.isUserInteractionEnabled = true
//
//                        }))
//                        self.present(alert, animated: true, completion: nil)
//                    } else {
//                        self.NavBackButton.isUserInteractionEnabled = true
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                })
//
//
//            }
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getClicIsTyping(notification:)), name: NotificationSocketTypingGet, object: nil)
        setLocalization()
        setValue()
        textFieldSendMessage.delegate = self
        //tblShowMessage.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)

        
        textFieldSendMessage.attributedPlaceholder = NSAttributedString(string: textFieldSendMessage.placeholder ?? "",
                                                        attributes: [NSAttributedString.Key.foregroundColor: colors.white.value])
        resetALL()
        AllSocketOn()
        GetHistoryOfChat()
        //initUIForNavigation("\(self.RequestAcceptDataOfUser["advisor_name"] ?? "")", andImage: "\(self.RequestAcceptDataOfUser["advisor_profile_picture"] ?? "")")
        // Do any additional setup after loading the view.
    }
    func initUIForNavigation(_ title: String, andImage image: String) {

        let rect:CGRect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.navigationController?.navigationBar.frame.size.width ?? 0.0, height: 44))

        let titleView:UIView = UIView.init(frame: rect)
       
        let image_view:UIImageView = UIImageView()
       
        if image != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(image)")

            image_view.sd_imageIndicator = SDWebImageActivityIndicator.white
            image_view.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))

        } else {
            image_view.image = UIImage(named: "user_dummy_profile")
        }
        
        image_view.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        image_view.center.y = titleView.center.y
        image_view.layer.cornerRadius = image_view.bounds.size.width / 2.0
        image_view.layer.masksToBounds = true
        titleView.addSubview(image_view)
        
        /* label */
        let label:UILabel = UILabel.init(frame: CGRect(x: 45, y: 0, width: rect.width - 170, height: 40))

        label.text = title
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        label.center.y = titleView.center.y
        label.textAlignment = .left
        titleView.addSubview(label)

        self.navigationItem.titleView = titleView

    }
    
    func resetALL() {
        self.viewTyping.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        setupKeyboard(true)
        self.deregisterFromKeyboardNotifications()
       
        SocketIOManager.shared.socket.off(socketApiKeys.all_message_read.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.send_message.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.check_typing.rawValue)
       
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
       
        //        if self.isMovingFromParent {
        //          SocketIOManager.shared.closeConnection()
        //            allSocketOffMethods()
        //        }
    }
    override func viewWillAppear(_ animated: Bool) {
      //  self.SendMessageHeight.constant = 0
       
        if sessionType == "session" {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
        
        setupKeyboard(false)
        customTabBarController?.hideTabBar()
        self.registerForKeyboardNotifications()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        EmitAllmsgRead()
    }
    func GetHistoryOfChat() {
        let chatHistoryReqModel = getChatHistoryReqModel()
        chatHistoryReqModel.booking_id = bookingID
        chatHistoryReqModel.chat_type = sessionType
        
        getHistory(ReqModel: chatHistoryReqModel)
        
        AllSocketOn()
    }
    //MARK: - notification center for Keyboard Hide and Show
    func deregisterFromKeyboardNotifications(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func registerForKeyboardNotifications(){
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    func animateConstraintWithDuration(duration: TimeInterval = 0.5) {
        UIView.animate(withDuration: duration, animations: { [weak self] in
            self?.loadViewIfNeeded() ?? ()
        })
    }
    
    @objc func keyboardWasShown(notification: NSNotification){
        
         let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) + 15
        
        if #available(iOS 11.0, *) {
            conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) - view.safeAreaInsets.bottom + 35
        } else {
            conBottomChatBox.constant = (keyboardSize?.height ?? 0.0) + 35
        }
        print(conBottomChatBox.constant)
        self.animateConstraintWithDuration()
        if MessageArray.count != 0 {
            self.tblShowMessage.scrollToBottom()
//            let indexPath = IndexPath(row: self.MessageArray.count - 1, section: 0)
//            self.tblShowMessage.insertRows(at: [indexPath], with: .bottom)
//            let path = IndexPath.init(row: self.MessageArray.count - 1, section: 0)
//            self.tblShowMessage.scrollToRow(at: path, at: .bottom, animated: true)
        }
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        
         conBottomChatBox.constant = 35
        self.animateConstraintWithDuration()
        if MessageArray.count != 0 {
            self.tblShowMessage.scrollToBottom()
//            let indexPath = IndexPath(row: self.MessageArray.count - 1, section: 0)
//            self.tblShowMessage.insertRows(at: [indexPath], with: .bottom)
//            let path = IndexPath.init(row: self.MessageArray.count - 1, section: 0)
//            self.tblShowMessage.scrollToRow(at: path, at: .bottom, animated: true)
        }
        
    }
    func setupKeyboard(_ enable: Bool) {
        IQKeyboardManager.shared.enable = enable
        IQKeyboardManager.shared.enableAutoToolbar = enable
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = !enable
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
    }
    //MARK:- ======== other methods ========
    func textViewDidChange(_ textView: UITextView) {
        EmitSocketTyping()
        adjustTextViewHeight()
    }
    
    
    func adjustTextViewHeight()
    {
        
        let fixedWidth = txtMessage.frame.size.width
        
        let newSize = txtMessage.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))

        let numLines = Int(txtMessage.contentSize.height / (txtMessage.font?.lineHeight)! )
        print(numLines)
        print("textFieldHeight: \(newSize.height)")
        self.textHeightConstraint?.constant = newSize.height
        
        if numLines <= 4 {
            self.txtMessage.isScrollEnabled = false
            let difference = newSize.height - 44 //new line
            self.textHeightConstraint?.constant = newSize.height
            self.containerHeightConstraint?.constant = 56 + difference //new line
        }
        else {
            self.txtMessage.isScrollEnabled = true
            
        }
        self.view.layoutIfNeeded()
        
        
    }
    
    func setLocalization() {
        
    }
    
    func setTimeLabel() {
        
        print(timerSecond)
        let TimeTotal = secondsToHoursMinutesSeconds(seconds: timerSecond)
        lblShowTimer.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
        print("Total time is \(String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2))")
        
        if let barbuttonItem = (self.navigationItem.rightBarButtonItems?.first) {
            if let customView = barbuttonItem.customView {
                if customView.isKind(of: UIView.self) {
                    let viewFromNavigation : UIView = customView
                    
                    if let CheckSubViews = viewFromNavigation.subviews.last {
                        if  CheckSubViews.isKind(of: UILabel.self) {
                            let lbl = CheckSubViews as! UILabel
                            lbl.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                            print("This is an label view from Message screen")
                        }
                    }
                }
            }
        }
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (0, (seconds / 60), (seconds % 60))
      //return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    func setValue() {
        if RequestAcceptDataOfUser["customer_profile_picture"] as? String != ""{
            let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["customer_profile_picture"] ?? "")"
            userImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            userImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
        }else{
            userImageView.image = UIImage.init(named: "user_dummy_profile")
        }
        
        if RequestAcceptDataOfUser["advisor_profile_picture"] as? String != ""{
            let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["advisor_profile_picture"] ?? "")"
            advisorImageView.sd_imageIndicator = SDWebImageActivityIndicator.white
            advisorImageView.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
           
          
        }else{
            advisorImageView.image = UIImage.init(named: "user_dummy_profile")
           
        }
        senderImage = userImageView.image ?? UIImage()
        reciverImage = advisorImageView.image ?? UIImage()
//        switch RequestAcceptDataOfUser["type"] as? String {
//        case "chat":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_chat")
//        case "audio":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_audioCall")
//        case "video":
//            communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_videoCall")
//        default:
//            break
//        }
        communicationTypeImageView.image = UIImage(named: "ic_AdviserDetails_chat")
        
    }
    //MARK:- ======== tableViewMethods ========
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MessageArray.count
      //  return MessageArray[section].MessageData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        
        if MessageArray[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "Advisor" || MessageArray[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "advisor" {
            let SenderCell = tblShowMessage.dequeueReusableCell(withIdentifier: MessageSenderCell.reuseIdentifier, for: indexPath) as! MessageSenderCell
           
           
            SenderCell.viewBG.backgroundColor = UIColor(hexString: "#273F4D")
            SenderCell.chatMessage.text = MessageArray[indexPath.row].ChatMessage["message"] as? String ?? ""
            SenderCell.profileImage.image = senderImage
            
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: MessageArray[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
            inputFormatter.dateFormat = "h:mm a"
            let resultString = inputFormatter.string(from: showDate)
            print(resultString)
            
            
            SenderCell.lblTime.text = resultString
            //SenderCell.profileImage.image = senderImage
            // Create Date Formatter
            
            if RequestAcceptDataOfUser["advisor_profile_picture"] as? String != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["advisor_profile_picture"] ?? "")"
                SenderCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                SenderCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
            }else{
                SenderCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
            }
            
//            SenderCell.isHidden = true
            cell = SenderCell
        } else {
            let ReciverCell = tblShowMessage.dequeueReusableCell(withIdentifier: MessageReciverCell.reuseIdentifier, for: indexPath) as! MessageReciverCell
            ReciverCell.viewBG.backgroundColor = UIColor(hexString: "#5A7685")
            ReciverCell.chatMessage.text = MessageArray[indexPath.row].ChatMessage["message"] as? String ?? ""
            let inputFormatter = DateFormatter()
            inputFormatter.dateFormat = "yyyy-mm-dd HH:mm:ss"
            let showDate = inputFormatter.date(from: MessageArray[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
            inputFormatter.dateFormat = "h:mm a"
            let resultString = inputFormatter.string(from: showDate)
            print(resultString)
            ReciverCell.chatMessageReadStatus?.text = MessageArray[indexPath.row].readStatus == 1 ? "Read" : "Sent"
            
            ReciverCell.lblTime.text = resultString
            if RequestAcceptDataOfUser["customer_profile_picture"] as? String != ""{
                let strUrl = "\(APIEnvironment.profileBu)\(RequestAcceptDataOfUser["customer_profile_picture"] ?? "")"
                ReciverCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                ReciverCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
               
              
            }else{
                ReciverCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
               
            }
//            ReciverCell.isHidden = true
            cell = ReciverCell
        }
        
        return cell
       
    }
    
    //MARK:- ======== IBActions ========
    @IBAction func btnSendMessageClick(_ sender: Any) {
      btnSendMessage.isUserInteractionEnabled = false
        if txtMessage.text?.trim() != "" {

            
            if SocketIOManager.shared.socket.status == .connected {
                emitSocketSendMesage(message: txtMessage.text?.trim() ?? "")
            }
            
        }
        else{
            btnSendMessage.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- ======== API Calls ========
    func getHistory(ReqModel : getChatHistoryReqModel) {
        WebServiceSubClass.GetChatHistory(addCategory: ReqModel, completion: {(json, status, response) in
            
            if status {
                self.MessageArray = []
                let chatHistoryRes = ChatHistoryResModel.init(fromJson: json)
                if self.sessionType == "free" {
                    if Int(chatHistoryRes.remaining_msg ?? "") ?? 0 > 0 {
                       // self.SendMessageHeight.constant = 56
                        self.txtMessage.superview?.isHidden = false
                    } else {
                      //  self.SendMessageHeight.constant = 0
                        self.txtMessage.resignFirstResponder()
                        self.txtMessage.superview?.isHidden = true
                    }
                } else {
                   // self.SendMessageHeight.constant = 56
                    self.txtMessage.superview?.isHidden = false
                }
               
                chatHistoryRes.chatHistory.forEach { (element) in
                    let messageData : [String:Any] = ["sender_type": element.senderType ?? "","receiver_type":element.receiverType ?? "","current_date":element.bookingDate ?? "","message_send_date":element.messageDate ?? "","message":element.message ?? "","booking_id":Int(element.bookingId) ?? 0,"sender_id":Int(element.senderId) ?? 0,"receiver_id":Int(element.receiverId) ?? 0]
                    
                    self.MessageArray.append(ChatHistory(ReadStatus: Int(element.readStatus) ?? 0, FullName: element.fullName, NickName:element.nickName, Profile: element.profilePicture, message: messageData))
                }
                DispatchQueue.main.async {
                    
                    self.MessageArray = self.MessageArray.reversed()
                    
                    if self.MessageArray.count != 0 {
                        self.tblShowMessage.reloadData()
                        DispatchQueue.main.async {
                            self.tblShowMessage.scrollToBottom()
                        }
                    }

                }
                
                self.EmitAllmsgRead()
                
                
            }else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
            
            
        })
    }
    
    //MARK:- ======== SocketsMethods ========
    
    
    //MARK:- ======== TextViewc Delegate Methods  ==========
    
    //MARK:- ========== Emit Socket All msg Read =======
    func EmitAllmsgRead(){
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
            "sender_type" : "Customer",
            "receiver_id" : RequestAcceptDataOfUser["advisor_id"] as? Int ?? 0,
            "receiver_type" : "Advisor",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,
            ] as [String : Any]
        SocketIOManager.shared.socketEmit(for: socketApiKeys.all_message_read.rawValue, with: param)
    }
    //MARK:- ====== Emit socket Check Typing =======
    func EmitSocketTyping(){
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
            "sender_type" : "Customer",
            "receiver_id" : RequestAcceptDataOfUser["advisor_id"] as? Int ?? 0,
            "receiver_type" : "Advisor",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0
            ] as [String : Any]
        
        print(param)
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.check_typing.rawValue, with: param)
    }
    func onSocketTyping() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.check_typing.rawValue) { (json) in
            print(#function)
            print(json)
            self.viewTyping?.isHidden = false
            print(json)
            print("Get on for Typing")
            let dict = json[0]
            if dict["booking_id"].int ?? 0 == self.RequestAcceptDataOfUser["booking_id"] as? Int ?? 0 {
                self.viewTyping.isHidden = false
                NotificationCenter.default.post(name: self.NotificationSocketTypingGet, object: json)
                if self.MessageArray.count != 0 {
                    self.tblShowMessage.scrollToBottom()
                }
                
            }
            else{
                self.viewTyping.isHidden = true
                return
            }
           
        }
    }
    @objc func getClicIsTyping(notification:Notification) -> Void {
        
        let message = JSON(notification.object as Any).array?.first?.dictionary?["message"]?.string ?? ""
        lblTypingStatus.text = "\(message)"

        viewTyping.isHidden = false
        timeLeft = 2
        if timer == nil {
            loadTyping()
        }
    }
    func loadTyping() {
        
        self.viewIndicator.type = .ballPulseSync
        self.viewIndicator.color = colors.white.value
        self.viewIndicator.startAnimating()
        
        if timer == nil {
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { timer in
                
                self.timeLeft -= 1
                if(self.timeLeft <= 0) {
                    self.timer?.invalidate()
                    timer.invalidate()
                    self.timer = nil
                    self.viewIndicator.stopAnimating()
                    self.viewTyping.isHidden = true
                }
            }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
//        EmitSocketTyping()
    }
    
    func AllSocketOn() {
        OnSocketToReciveMesage()
        onSocketAllmsgRead()
        onSocketTyping()
       
    }
    func emitSocketSendMesage(message : String){
        
        let param = [
            "sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
            "sender_type" : "Customer",
            "receiver_id" : RequestAcceptDataOfUser["advisor_id"] as? Int ?? 0,
            "receiver_type" : "Advisor",
            "booking_id" : RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,
            "message" : message
            ] as [String : Any]
        
        print(param)
        
        btnSendMessage.isUserInteractionEnabled = true
        SocketIOManager.shared.socketEmit(for: socketApiKeys.send_message.rawValue, with: param)
        
        
        
        let filter = MessageArray.filter({$0.ChatMessage["sender_type"] as? String == "Customer"})
        print(filter.count)
        
        
        
//        let filter = MessageArray.filter({$0.ChatMessage["sender_type"] as? String ?? "" == "Customer"})
//
        let count = filter.count + 1
        if (count) == 3 {
            
            if self.sessionType == "free" {
              //  self.SendMessageHeight.constant = 0
                self.txtMessage.superview?.isHidden = true
                self.txtMessage.resignFirstResponder()
            } else {
                //self.SendMessageHeight.constant = 56
                self.txtMessage.superview?.isHidden = false
            }
            
        }else {
            //self.SendMessageHeight.constant = 56
            self.txtMessage.superview?.isHidden = false
        }
        

//
//        let messageData : [String:Any] = ["booking_id":RequestAcceptDataOfUser["booking_id"] as? Int ?? 0,"sender_id":Int(SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.id ?? "") ?? 0,"message":textFieldSendMessage.text ?? "","message_send_date":"","receiver_id":RequestAcceptDataOfUser["user_id"] as? Int ?? 0,"current_date":"","sender_type":"Advisor","receiver_type":"Customer"]
//
//        self.MessageArray.append(ChatHistory(FullName: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.fullName ?? "", NickName: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.nickName ?? "", Profile: SingletonClass.sharedInstance.LoginRegisterUpdateData?.profile.profilePicture ?? "", message: messageData))
        
        txtMessage.text = ""
//        let indexPath = IndexPath(row: self.MessageArray.count - 1, section: 0)
//        self.tblShowMessage.insertRows(at: [indexPath], with: .bottom)
//        let path = IndexPath.init(row: self.MessageArray.count - 1, section: 0)
//        self.tblShowMessage.scrollToRow(at: path, at: .bottom, animated: true)
    }

    
    
    func OnSocketToReciveMesage(){
       
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.send_message.rawValue) { (json) in
            print(#function)
            print("Got on for send message")
            print(json)
            
            if let arrResponse = json.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["booking_id"].int == self.RequestAcceptDataOfUser["booking_id"] as? Int ?? 0 {
                        let userInfo = singleResponse["user"].array?.first
                        
                        let messageData : [String:Any] = ["booking_id":singleResponse["booking_id"].int ?? 0,"sender_id":singleResponse["sender_id"].int ?? 0,"message":singleResponse["message"].string ?? "","message_send_date":singleResponse["message_send_date"].string ?? "","receiver_id":singleResponse["receiver_id"].int ?? 0,"current_date":singleResponse["current_date"].string ?? "","sender_type":singleResponse["sender_type"].string ?? "","receiver_type":singleResponse["receiver_type"].string ?? ""]
                        
                        self.MessageArray.append(ChatHistory(ReadStatus: 0, FullName: userInfo?["full_name"] as? String ?? "", NickName: userInfo?["nick_name"] as? String ?? "", Profile: userInfo?["profile_picture"] as? String ?? "", message: messageData))
                        
                        self.tblShowMessage.reloadData()
                        DispatchQueue.main.async {
                          
                            self.tblShowMessage.scrollToBottom()
                        }
                        
//                        let indexPath = IndexPath(row: self.MessageArray.count - 1, section: 0)
//                        self.tblShowMessage.insertRows(at: [indexPath], with: .bottom)
//                        let path = IndexPath.init(row: self.MessageArray.count - 1, section: 0)
//                        self.tblShowMessage.scrollToRow(at: path, at: .bottom, animated: true)
                        self.EmitAllmsgRead()
                    //
//                        self.apicallMsgSeen()
//                        self.EmitAllmsgRead()
//

                    //    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotificationDataRefresh"), object: nil, userInfo: VisitIdDict)
                        
                        
                        /*
                         
                         
                        
                         
                        
                         
                         */
                        
                    }
                    else {
                        
                        return
                    }
                    
                }
            }
            
        }
    }
    

    //MARK:- ====== On socket Read all message =======
    func onSocketAllmsgRead(){
        SocketIOManager.shared.socketCall(for: socketApiKeys.all_message_read.rawValue) { (json) in
            print(#function)
            print(json)
            for i in 0..<self.MessageArray.count{
                if self.MessageArray[i].readStatus == 0{
                    self.MessageArray[i].readStatus = 1
                    let indexpath = IndexPath.init(row: i, section: 0)
                    self.tblShowMessage?.reloadRows(at: [indexpath], with: .none)
                }
            }
        }
    }
    
}
class MessageSenderCell : UITableViewCell {
  
    @IBOutlet weak var chatMessage: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        
    }
}
class MessageReciverCell : UITableViewCell {
    @IBOutlet weak var chatMessageReadStatus: UILabel!
    @IBOutlet weak var chatMessage: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTime: UILabel!
    override func awakeFromNib() {
        chatMessageReadStatus.font = CustomFont.regular.returnFont(13)
        chatMessageReadStatus.textColor = colors.white.value.withAlphaComponent(0.5)
    }
    
}
class MessageChat {
    var MessageDate : String?
    var MessageData : [MessageAllData]?
    init(date:String,Data:[MessageAllData]) {
        self.MessageDate = date
        self.MessageData = Data
    }
   
}
class MessageAllData {
    var isFromSender : Bool?
    var chatTime : String?
    var chatMessage : String?

    init(fromSender:Bool,time:String,message:String) {
        self.isFromSender = fromSender
        self.chatTime = time
        self.chatMessage = message
    }
}
