//
//  ShowConversationViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
class ShowConversationViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK: -  Properties
    var navigationName = ""
    var navigationImage = ""
    
    
    var sessionType = ""
    var isFormAdvisor : Bool = false
    
    var AdvisorImage = ""
 
    var userName = ""
    var customTabBarController : CustomTabBarVC?
    
    var bookingID = ""
    
    var conversationArray = [ChatHistoryMain]()
    //var MessageArray = [ChatHistory]()
    //MARK: -  IBOutlets
    
    @IBOutlet weak var tblShowConversation: UITableView!
    
    //MARK: -  View Life Cycle Methods
    override func viewDidLayoutSubviews() {
       
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        

        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        tblShowConversation.isHidden = true
        let chatHistoryReqModel = getChatHistoryReqModel()
        chatHistoryReqModel.booking_id = bookingID
        chatHistoryReqModel.chat_type = sessionType
        getHistory(ReqModel: chatHistoryReqModel)
        tblShowConversation.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
       
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
        setValue()
        
        initUIForNavigation("\(navigationName)", andImage: "\(navigationImage)")
        // Do any additional setup after loading the view.
    }
    func initUIForNavigation(_ title: String, andImage image: String) {

        let rect:CGRect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: self.navigationController?.navigationBar.frame.size.width ?? 0.0, height: 44))

        let titleView:UIView = UIView.init(frame: rect)
       
        let image_view:UIImageView = UIImageView()
       
        if image != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(image)")

            image_view.sd_imageIndicator = SDWebImageActivityIndicator.white
            image_view.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))

        } else {
            image_view.image = UIImage(named: "user_dummy_profile")
        }
        image_view.contentMode = .scaleAspectFill
        
        image_view.frame = CGRect.init(x: 0, y: 0, width: 40, height: 40)
        image_view.center.y = titleView.center.y
        image_view.layer.cornerRadius = image_view.bounds.size.width / 2.0
        image_view.layer.masksToBounds = true
        titleView.addSubview(image_view)
        
        /* label */
        let label:UILabel = UILabel.init(frame: CGRect(x: 45, y: 0, width: rect.width - 170, height: 40))

        label.text = title
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 20.0, weight: .bold)
        label.center.y = titleView.center.y
        label.textAlignment = .left
        titleView.addSubview(label)

        self.navigationItem.titleView = titleView

    }
    override func viewWillAppear(_ animated: Bool) {
        customTabBarController?.hideTabBar()
    }
    
    //MARK: -  other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: date) ?? Date()
            dateFormatter.dateFormat = "dd MMM yyyy"
            return  dateFormatter.string(from: date)

        }
    //MARK: -  tableViewMethods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if conversationArray.count == 0 {
            return 1
        } else {
            return conversationArray[section].History.count
        }
       
        
      
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if conversationArray.count == 0 {
            return tableView.frame.size.height
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if conversationArray.count == 0 {
            let NoDatacell = tblShowConversation.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
            let myImage = UIImage(named: "no_chat")
            NoDatacell.imgNoData.image = myImage
            NoDatacell.imgNoData.contentMode = .scaleAspectFit
          
            NoDatacell.lblNoDataTitle.text = "No chat history found"
            //NodataTitleText.ItemList
            
            return NoDatacell
        } else {
            if isFormAdvisor {
               
                var cell = UITableViewCell()
               
                if conversationArray[indexPath.section].History[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "Customer" || conversationArray[indexPath.section].History[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "customer" {
                    let SenderCell = tblShowConversation.dequeueReusableCell(withIdentifier: conversationSenderCell.reuseIdentifier, for: indexPath) as! conversationSenderCell
                   
                   
                    SenderCell.viewBG.backgroundColor = UIColor(hexString: "#273F4D")
                    SenderCell.chatMessage.text = conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message"] as? String ?? ""
                   
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy/mm/dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
                    inputFormatter.dateFormat = "HH:mm"
                    let resultString = inputFormatter.string(from: showDate)
                    print(resultString)
                    
                    
                    SenderCell.lblTime.text = resultString
                    //SenderCell.profileImage.image = senderImage
                    // Create Date Formatter
                    if conversationArray[indexPath.section].History[indexPath.row].ProfileImage != ""{
                        let strUrl = "\(APIEnvironment.profileBu)\(conversationArray[indexPath.section].History[indexPath.row].ProfileImage ?? "")"
                        SenderCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                        SenderCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
                    }else{
                        SenderCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
                    }
                    
        //            SenderCell.isHidden = true
                    cell = SenderCell
                } else {
                    let ReciverCell = tblShowConversation.dequeueReusableCell(withIdentifier: conversationReciverCell.reuseIdentifier, for: indexPath) as! conversationReciverCell
                    ReciverCell.viewBG.backgroundColor = UIColor(hexString: "#5A7685")
                    ReciverCell.chatMessage.text = conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message"] as? String ?? ""
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy/mm/dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
                    inputFormatter.dateFormat = "HH:mm"
                    let resultString = inputFormatter.string(from: showDate)
                    print(resultString)
                   // ReciverCell.chatMessageReadStatus?.text = conversationArray[indexPath.section].History[indexPath.row].readStatus == 1 ? "Read" : "Sent"
                    
                    ReciverCell.lblTime.text = resultString
                    if conversationArray[indexPath.section].History[indexPath.row].ProfileImage != ""{
                        let strUrl = "\(APIEnvironment.profileBu)\(conversationArray[indexPath.section].History[indexPath.row].ProfileImage ?? "")"
                        ReciverCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                        ReciverCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())


                    }else{
                        ReciverCell.profileImage.image = UIImage.init(named: "user_dummy_profile")

                    }
                    cell = ReciverCell
                }
                return cell
                
            } else {
                var cell = UITableViewCell()
                if conversationArray[indexPath.section].History[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "Customer" || conversationArray[indexPath.section].History[indexPath.row].ChatMessage["sender_type"] as? String ?? "" == "customer" {
                    
                    let ReciverCell = tblShowConversation.dequeueReusableCell(withIdentifier: conversationReciverCell.reuseIdentifier, for: indexPath) as! conversationReciverCell
                    ReciverCell.viewBG.backgroundColor = UIColor(hexString: "#5A7685")
                    ReciverCell.chatMessage.text = conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message"] as? String ?? ""
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy/mm/dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
                    inputFormatter.dateFormat = "HH:mm"
                    let resultString = inputFormatter.string(from: showDate)
                    print(resultString)
                   // ReciverCell.chatMessageReadStatus?.text = conversationArray[indexPath.section].History[indexPath.row].readStatus == 1 ? "Read" : "Sent"
                    
                    ReciverCell.lblTime.text = resultString
                    if conversationArray[indexPath.section].History[indexPath.row].ProfileImage != ""{
                        let strUrl = "\(APIEnvironment.profileBu)\(conversationArray[indexPath.section].History[indexPath.row].ProfileImage ?? "")"
                        ReciverCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                        ReciverCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())


                    }else{
                        ReciverCell.profileImage.image = UIImage.init(named: "user_dummy_profile")

                    }
                    cell = ReciverCell
                } else {
                    let SenderCell = tblShowConversation.dequeueReusableCell(withIdentifier: conversationSenderCell.reuseIdentifier, for: indexPath) as! conversationSenderCell
                   
                   
                    SenderCell.viewBG.backgroundColor = UIColor(hexString: "#273F4D")
                    SenderCell.chatMessage.text = conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message"] as? String ?? ""
                   
                    
                    let inputFormatter = DateFormatter()
                    inputFormatter.dateFormat = "yyyy/mm/dd HH:mm:ss"
                    let showDate = inputFormatter.date(from: conversationArray[indexPath.section].History[indexPath.row].ChatMessage["message_send_date"] as? String ?? "") ?? Date()
                    inputFormatter.dateFormat = "HH:mm"
                    let resultString = inputFormatter.string(from: showDate)
                    print(resultString)
                    
                    
                    SenderCell.lblTime.text = resultString
                    //SenderCell.profileImage.image = senderImage
                    // Create Date Formatter
                    if conversationArray[indexPath.section].History[indexPath.row].ProfileImage != ""{
                        let strUrl = "\(APIEnvironment.profileBu)\(conversationArray[indexPath.section].History[indexPath.row].ProfileImage ?? "")"
                        SenderCell.profileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                        SenderCell.profileImage.sd_setImage(with: URL(string: strUrl),  placeholderImage: UIImage())
                    }else{
                        SenderCell.profileImage.image = UIImage.init(named: "user_dummy_profile")
                    }
                    
        //            SenderCell.isHidden = true
                    cell = SenderCell
                    
                }
                return cell
                
            }

        }
       
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if conversationArray.count == 0 {
            return 0
        } else {
            return 25
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if conversationArray.count == 0 {
            return 1
        } else {
            return conversationArray.count
        }
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView  = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: tblShowConversation.frame.size.width, height: 25)
        headerView.backgroundColor = UIColor(hexString: "#334D5C").withAlphaComponent(0.05)
        let labelDate = UILabel()
        labelDate.text = convertDateFormater(conversationArray[section].ConversationDate ?? "") 
        
        labelDate.font = CustomFont.medium.returnFont(12)
        labelDate.textColor = colors.white.value
        labelDate.textAlignment = .center
        labelDate.numberOfLines = 0
        labelDate.lineBreakMode = .byWordWrapping


        var rect: CGRect = labelDate.frame //get frame of label
        rect.size = (labelDate.text?.size(withAttributes: [NSAttributedString.Key.font: CustomFont.medium.returnFont(12)]))!
        
        labelDate.frame.size.width = rect.width + 20
        labelDate.frame.size.height = 25
       
        labelDate.layer.cornerRadius = labelDate.frame.size.height / 2
        labelDate.layer.borderWidth = 1
        labelDate.layer.borderColor = colors.white.value.cgColor
        labelDate.center.x = headerView.frame.size.width / 2
        
        //headerView.backgroundColor = UIColor(hexString: "#334D5C")
        
        headerView.addSubview(labelDate)
        print(labelDate.frame.size.height)
        return headerView
    }
   
    //MARK: -  IBActions
    
    
    //MARK: -  API Calls

}
class conversationSenderCell : UITableViewCell {
    
    @IBOutlet weak var chatMessage: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTime: UILabel!
}
class conversationReciverCell : UITableViewCell {
    @IBOutlet weak var chatMessage: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var lblTime: UILabel!
}
class ConversationChat {
    var ConversationDate : String?
    var ConversationData : [converSationAllData]?
    init(date:String,Data:[converSationAllData]) {
        self.ConversationDate = date
        self.ConversationData = Data
    }
   
}
class converSationAllData {
    var isFromSender : Bool?
    var chatTime : String?
    var chatMessage : String?

    init(fromSender:Bool,time:String,message:String) {
        self.isFromSender = fromSender
        self.chatTime = time
        self.chatMessage = message
    }
}
extension ShowConversationViewController {
    func getHistory(ReqModel : getChatHistoryReqModel) {
        Utilities.showHud()
        WebServiceSubClass.GetChatHistory(addCategory: ReqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
                let chatHistoryRes = ChatHistoryResModel.init(fromJson: json)
                if chatHistoryRes.chatHistory.count != 0 {
                    let SortedData = chatHistoryRes.chatHistory.filter({$0.bookingId == chatHistoryRes.chatHistory?[0].bookingId})
                    var tempHistoryData : [ChatHistoryForShowConversation] = []
                    SortedData.forEach { (element) in
                        
                        let messageData : [String:Any] = ["sender_type": element.senderType ?? "","receiver_type":element.receiverType ?? "","current_date":element.bookingDate ?? "","message_send_date":element.messageDate ?? "","message":element.message ?? "","booking_id":Int("57") ?? 0,"sender_id":Int(element.senderId) ?? 0,"receiver_id":Int(element.receiverId) ?? 0]
                        
                        tempHistoryData.append(ChatHistoryForShowConversation(ReadStatus: Int(element.readStatus) ?? 0, FullName: element.fullName, NickName:element.nickName, Profile: element.profilePicture, message: messageData))
                        
                    }
                    tempHistoryData = tempHistoryData.reversed()
                    self.conversationArray.append(ChatHistoryMain(Date: chatHistoryRes.chatHistory?[0].bookingDate ?? "", historyData: tempHistoryData))
                }
                DispatchQueue.main.async {
                  //  if self.conversationArray.count != 0 {
                        self.tblShowConversation.reloadData()
                    self.tblShowConversation.isHidden = false
                  //  }
                }
            }else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
              //  Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
            
            
        })
    }
}

class ChatHistoryMain {
    var ConversationDate : String?
    var History : [ChatHistoryForShowConversation]
    init(Date:String,historyData:[ChatHistoryForShowConversation]) {
        self.ConversationDate = Date
        self.History = historyData
    }
}

class ChatHistoryForShowConversation {
    
    var readStatus : Int?
    var ChatMessage : [String:Any] = [:]
    var ProfileImage : String?
    var full_name : String?
    var nick_name : String?
    
    init(ReadStatus:Int,FullName:String,NickName:String,Profile:String,message:[String:Any]) {
        
        self.readStatus = ReadStatus
        self.full_name = FullName
        self.nick_name = NickName
        self.ProfileImage = Profile
        self.ChatMessage = message
    }
}
