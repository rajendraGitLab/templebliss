//
//  IntroScreenViewController.swift
//  TempleBliss
//
//  Created by Apple on 10/05/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class IntroScreenViewController: BaseViewController {

    //MARK: - Properties
    var IntroImageArray : [String] = ["intro_1","intro_2","intro_3","intro_4"]
    //MARK: - IBOutlets
    
    @IBOutlet weak var CollectionViewPageControl: UICollectionView!
    @IBOutlet weak var IntroPageControl: UIPageControl!
    @IBOutlet weak var btnNext: theamSubmitButton!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        self.IntroPageControl.numberOfPages = IntroImageArray.count
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.skipTour.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        backButtonClick = {
            self.CollectionViewPageControl.contentOffset = CGPoint(x: (self.IntroPageControl.currentPage - 1) * Int(self.CollectionViewPageControl.frame.size.width), y: 0)
        }

    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //MARK: - IBActions
    
//    @IBAction func IntroPageControllAction(_ sender: UIPageControl) {
//        
//        print(sender.currentPage)
//        print(IntroPageControl.currentPage)
//        
//        CollectionViewPageControl.contentOffset = CGPoint(x: (self.IntroPageControl.currentPage + 1) * Int(CollectionViewPageControl.frame.size.width), y: 0)
//        
//    }
    @IBAction func btnNextClick(_ sender: Any) {
        if self.IntroPageControl.currentPage == IntroImageArray.count - 1 {
            AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_nextBtnClick)
            appDel.navigateToHomeCustomer()
        } else {
            
            CollectionViewPageControl.contentOffset = CGPoint(x: (self.IntroPageControl.currentPage + 1) * Int(CollectionViewPageControl.frame.size.width), y: 0)
            
        }
       
    }
    @IBAction func pageControlSelectionAction(_ sender: UIPageControl) {
        let page: Int? = sender.currentPage
        var frame: CGRect = self.CollectionViewPageControl.frame
        frame.origin.x = frame.size.width * CGFloat(page ?? 0)
        frame.origin.y = 0
        self.CollectionViewPageControl.scrollRectToVisible(frame, animated: true)
    }
    
}
//MARK: -  collectionViewMethods
extension IntroScreenViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return IntroImageArray.count
      
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = CollectionViewPageControl.dequeueReusableCell(withReuseIdentifier: IntroCell.reuseIdentifier, for: indexPath) as! IntroCell
        
        cell.IntroImageView.image = UIImage(named: IntroImageArray[indexPath.row])
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: CollectionViewPageControl.frame.size.width, height: CollectionViewPageControl.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        self.IntroPageControl.currentPage = indexPath.row
        if self.IntroPageControl.currentPage == IntroImageArray.count - 1 {
            setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
            self.btnNext.setTitle("Get started".uppercased(), for: .normal)
        } else {
            if indexPath.row != 0 {
                setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.skipTour.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
            } else {
                setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.skipTour.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
            }
            self.btnNext.setTitle("Next".uppercased(), for: .normal)
        }
       
    }
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let pageSide = self.CollectionViewPageControl.frame.width
//        let offset = scrollView.contentOffset.x
//        let currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
//        self.IntroPageControl.currentPage = currentPage
//        if self.IntroPageControl.currentPage == IntroImageArray.count - 1 {
//            setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
//            self.btnNext.setTitle("Get started".uppercased(), for: .normal)
//        } else {
//            if currentPage != 0 {
//                setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.skipTour.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
//            } else {
//                setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.skipTour.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
//            }
//            self.btnNext.setTitle("Next".uppercased(), for: .normal)
//        }
//    }
   
    
}
class IntroCell : UICollectionViewCell {
    
    @IBOutlet weak var IntroImageView: UIImageView!
    
}
