//
//  HomeViewController.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import PullToRefreshKit
import SkeletonView
import SwiftyJSON
class HomeViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate, UITabBarControllerDelegate {
   

      //MARK: - -------------  Properties -------------
    
    var MiniutesForVideo = ""
    var customTabBarController : CustomTabBarVC?
    var CategoryList : [categoryForCustomer]?
    var refreshControl = UIRefreshControl()
      //MARK: - -------------  IBOutlets -------------
    
    @IBOutlet weak var textfieldSearchCategories: HomePageTextField!
    @IBOutlet weak var lblCategories: HomePageLabel!
    @IBOutlet weak var LblFreeMinutes: HomePageLabel?
    @IBOutlet weak var CategoryCollectionView: UICollectionView!
     //MARK: - -------------  View Life Cycle Methods -------------
    func OnSocket() {
        if !SocketIOManager.shared.isSocketOn {
            SocketIOManager.shared.establishConnection()
            print("ATDebug : HomeVC establishConnection")
        } else {
            CallOnAllSocketToListen()
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let controller:AdvisorAudioCallViewController = AppStoryboard.AdviserMain.instance.instantiateViewController(withIdentifier: AdvisorAudioCallViewController.storyboardID) as! AdvisorAudioCallViewController
        appDel.pushKitEventDelegate = controller
        appDel.initializePushKit()
        self.navigationController?.navigationBar.isHidden = false
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        setLocalization()
        
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            
        } else {
            OnSocket()
            RegisterForVOIP()
            self.getMinutesData()
            self.FirstTimeMinutesData()
        }
        
        
        
        self.CategoryCollectionView.register(UINib(nibName:"NoDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoDataCollectionViewCell")
        
        textfieldSearchCategories.delegate = self
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
            self.customTabBarController?.delegate = self
        }
        
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        self.CategoryCollectionView.refreshControl = refreshControl
        textfieldSearchCategories.returnKeyType = .search
  
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {

        PullwebserviceForCategory(searchedString: self.textfieldSearchCategories.text ?? "")

    }
    override func viewDidAppear(_ animated: Bool) {
       
    }
    override func viewWillAppear(_ animated: Bool) {
      
        if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
            if SingletonClass.sharedInstance.FreeMinutes == "1" {
                LblFreeMinutes?.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minute first reading"
            } else {
                LblFreeMinutes?.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minutes first reading"
            }
        } else {
            LblFreeMinutes?.text = ""
        }
        
        CategoryList = nil
        CategoryCollectionView.reloadData()
        textfieldSearchCategories.text = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.20, execute: {
            self.webserviceForCategory(searchedString: "")
        })
        
        customTabBarController?.showTabBar()
       
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    func gotoRatingReviewScreen(singleResponse: JSON) {
        // Komal
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: RateAdviserViewController.storyboardID) as! RateAdviserViewController
        //self.customTabBarController?.hideTabBar()
        controller.isDismiss = {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerMySessionReload"), object: nil, userInfo: nil)
            //self.customTabBarController?.showTabBar()
            // self.ReloadAllLoadedSessionData()
        }
        controller.advisorID = singleResponse["advisor_id"].string ?? ""
        controller.advisorProfileImaage = singleResponse["profilePicture"].string ?? ""
        controller.advisorName = singleResponse["nickName"].string ?? ""
        controller.TotalMinutes = singleResponse["total_minute"].string ?? ""

        controller.bookingID = singleResponse["bookingId"].string ?? ""
        //self.customTabBarController?.hideTabBar()
        
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.modalPresentationStyle = .overCurrentContext
        navigationController.modalTransitionStyle = .crossDissolve
        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
    }
    
     //MARK: - -------------  Register for VOIP -------------
    func RegisterForVOIP() {
        
        let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
        appDel.pushKitEventDelegate = controller
        appDel.initializePushKit()
        
    }
     //MARK: - -------------  textfield methods -------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == textfieldSearchCategories {
            self.webserviceForCategory(searchedString: textField.text ?? "")
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        CategoryList?.removeAll()
            webserviceForCategory(searchedString:textField.text ?? "")
    }
     //MARK: - -------------  other methods -------------
    func setLocalization() {
        
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if tabBarController.selectedIndex == 1 {
            DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                for navcontroller in self.parent?.parent?.children ?? [UINavigationController()]
                {
                    if let advisorVC = navcontroller.children.first as? AdvisorsViewController
                    {
                        advisorVC.setValue()
                    }
                }
            }
        }
        SingletonClass.sharedInstance.selectedCategoryIndex = -1
    }

      //MARK: - -------------  collectionViewMethods -------------
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        if CategoryList?.count == nil {
            return 6
        } else {
            return (CategoryList?.count ?? 0) != 0 ? (CategoryList?.count ?? 0) : 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if CategoryList?.count == nil {
            let cell = CategoryCollectionView.dequeueReusableCell(withReuseIdentifier: HomePageCategoryCell.reuseIdentifier, for: indexPath) as! HomePageCategoryCell
            cell.categoryName.text = ""
            cell.categoryImage.image = nil
            
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.categoryName.numberOfLines = 1
            
            cell.categoryImageMainView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            return cell
        } else {
            if CategoryList?.count != 0 {
            
                let cell = CategoryCollectionView.dequeueReusableCell(withReuseIdentifier: HomePageCategoryCell.reuseIdentifier, for: indexPath) as! HomePageCategoryCell

                cell.categoryName.hideSkeleton()
                cell.categoryImageMainView.hideSkeleton()
                cell.categoryName.text = CategoryList?[indexPath.row].name
                
                if (CategoryList?[indexPath.row].image ?? "") == "" {
                    cell.categoryImage.image = UIImage(named: "")
                } else {
                    let imgURL = CategoryList?[indexPath.row].image ?? ""
                    
                    print("Index \(indexPath.row) and ImageURL is \(imgURL)")
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    print(URL(string: APIEnvironment.profileBu + imgURL) ?? "")
                    
                    cell.categoryImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                    cell.categoryImage.sd_setImage(with: strURl, completed: {_,_,_,_ in
                       
                    })
                    
                }
                 
                return cell
            } else {
                let NoDatacell = CategoryCollectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCollectionViewCell", for: indexPath) as! NoDataCollectionViewCell
                NoDatacell.img.image = UIImage(named: "ic_noCategoryFound")
                NoDatacell.lblNodataTitle.text = "No category found"
               
                return NoDatacell
            }
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if CategoryList?.count == nil {
            
            var heightOfCell:CGFloat = 0.0
            let widthOfCell  = (CategoryCollectionView.frame.size.width - 26) / 2
            
            heightOfCell = (widthOfCell * 216) / 159
            
            return CGSize(width: widthOfCell, height: heightOfCell)
        }
        else {
            if CategoryList?.count != 0 {
                
                var heightOfCell:CGFloat = 0.0
                let widthOfCell  = (CategoryCollectionView.frame.size.width - 26) / 2
                
                
                heightOfCell = (widthOfCell * 216) / 159
                
                
                return CGSize(width: widthOfCell, height: heightOfCell)
            }
            else {
                return CGSize(width: self.CategoryCollectionView.frame.width, height: self.CategoryCollectionView.frame.height)
            }
        }
       

    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if CategoryList?.count == nil {
            
        }
        else {
            if CategoryList?.count != 0 {
                let catName = (CategoryList?[indexPath.row].name ?? "").replacingOccurrences(of: " ", with: "_")
                AppDelegate.firebaseLogEvent(name: "Category_\(catName)")
                let imageDataDict:[String: String] = ["category_id": CategoryList?[indexPath.row].id ?? "","descriptionForSelectedCategories": CategoryList?[indexPath.row].descriptionField ?? "","selectedTypeAdvisor": CategoryList?[indexPath.row].name ?? ""]
                
                self.customTabBarController?.selectedIndex = 1

                DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                    for navcontroller in self.parent?.parent?.children ?? [UINavigationController()]
                    {
                        if let advisorVC = navcontroller.children.first as? AdvisorsViewController
                        {
                            
                            advisorVC.GetData(imageDataDict)
                        }
                    }
                }
            }
        }
    }
      //MARK: - -------------  IBActions -------------
    
    @IBAction func textfieldSearchCategoryAction(_ sender: HomePageTextField) {
        
    }
    
    
      //MARK: - -------------  API Calls -------------
    @objc func webserviceForCategory(searchedString:String) {
        let CategoryModel = CategoryReqModel()
        CategoryModel.search_string = searchedString
        
        WebServiceSubClass.CategoryForCustomer(CategoryModel: CategoryModel, completion: {(json, status, response) in
            self.CategoryCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));

            if status {
               
                let categoryResModel = CategoryListForCustomer.init(fromJson: json)

                SingletonClass.sharedInstance.categoryListCustomer = categoryResModel
                self.CategoryList = categoryResModel.category
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.20, execute: {
                    self.CategoryCollectionView.reloadData()
                })
                
            }else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    @objc func PullwebserviceForCategory(searchedString:String) {
        let CategoryModel = CategoryReqModel()
        CategoryModel.search_string = searchedString
       // Utilities.showHud()
        WebServiceSubClass.CategoryForCustomer(CategoryModel: CategoryModel, completion: {(json, status, response) in
            self.CategoryCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            //Utilities.hideHud()
            if status {
               
                let categoryResModel = CategoryListForCustomer.init(fromJson: json)

                SingletonClass.sharedInstance.categoryListCustomer = categoryResModel
                self.CategoryList = categoryResModel.category
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.20, execute: {
                    self.CategoryCollectionView.reloadData()
                })
            }else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }


        })
    }
    func getMinutesData() {
        WebServiceSubClass.minutes(strParams: "", showHud: false, completion: { (json, status, error) in
            if status {
                
                let minutesData = minutesResModel.init(fromJson: json)
                if minutesData != nil {
                    SingletonClass.sharedInstance.minutesArray = minutesData.minutes
                }
                
            } else {
              Utilities.displayAlert(AppName, message: error as? String ?? "Something went wrong")
            }
        })
    }
    func FirstTimeMinutesData() {
        let ReqModel = GetFirstTimeData()
        ReqModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        
        WebServiceSubClass.GetFirstTime(Data: ReqModel, completion: {(json, status, response) in
            // Utilities.hideHud()
             
             if status {
                SingletonClass.sharedInstance.FreeMinutes = json["free_minute"].string ?? "0"
                if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
                    // plural
                    if SingletonClass.sharedInstance.FreeMinutes == "1" {
                        self.LblFreeMinutes?.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minute first reading"
                    } else {
                        self.LblFreeMinutes?.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minutes first reading"
                    }
                    if !isFreePopUpShown {
                        let vc = FreeMinutesVC(nibName: "FreeMinutesVC", bundle: nil)
                        vc.freeMinutes = SingletonClass.sharedInstance.FreeMinutes
                        vc.modalPresentationStyle = .overCurrentContext
                        if self.tabBarController != nil {
                            self.tabBarController?.present(vc, animated: true)
                        }
                    }
                } else {
                    self.LblFreeMinutes?.text = ""
                }
                 
             } else {
                 Utilities.displayAlert("", message: json["message"].string ?? "")
             }
             
         })
        
        
        WebServiceSubClass.minutes(strParams: "", showHud: false, completion: { (json, status, error) in
            if status {
                
                let minutesData = minutesResModel.init(fromJson: json)
                if minutesData != nil {
                    SingletonClass.sharedInstance.minutesArray = minutesData.minutes
                }
                
            } else {
              Utilities.displayAlert(AppName, message: error as? String ?? "Something went wrong")
            }
        })
    }
}




extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
}
class HomePageCategoryCell : UICollectionViewCell {
    
    @IBOutlet weak var categoryName: HomePageLabel!
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryImageMainView: UIView!
    
    override func awakeFromNib() {
        
        isSkeletonable = true
        categoryName.numberOfLines = 2
        
        categoryName.isSkeletonable = true
        categoryImage.isSkeletonable = true
        
        categoryImageMainView.isSkeletonable = true
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        categoryName.numberOfLines = 1
        
        categoryImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)

    }
   
}
