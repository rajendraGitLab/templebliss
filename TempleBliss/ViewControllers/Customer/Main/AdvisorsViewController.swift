/*

//
//  AdvisorsViewController.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import DropDown
import SDWebImage
import PullToRefreshKit
import SkeletonView

class AdvisorsViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITabBarControllerDelegate {
    
    //MARK: -  Properties
    var selectedIndexOfAdvisors : Int = -1
    
    
    
    var refreshControl = UIRefreshControl()
    
    var fromSelectedCategory : Bool = false
    var category_id : String = ""
    var user_id : String = ""
    var type : String = ""
    var search_string : String = ""
    
    var descriptionForSelectedCategories = ""
    var selectedTypeAdvisor = ""
    
    var selectedFilter = ""
    var searchedText = ""
    var customTabBarController : CustomTabBarVC?
    
    
    var advisorList : [Advisor] = []
    
    var totalPagesLoad = 1
    var isLoadingList : Bool = false
    var FirstTimeLoad = true
    
    let pickerViewForAll = GeneralPickerView()
    var filterArray : [String] = [
        "All",
        "Top rating",
        "Chat",
        "Audio",
        "Video"
    ]
    let chooseFilterDropDown = DropDown()
    
    @IBOutlet weak var viewForShowFilterDropDown: viewViewClearBG!
    var searchTask: DispatchWorkItem?
    var GoToDetailsPage : Bool = false
    //MARK: -  IBOutlets
    @IBOutlet weak var btnSelectAdviser: UIButton!
    @IBOutlet weak var lblSelectedAdviser: AdviserPageLabel!
    @IBOutlet weak var lblDescription: AdviserPageLabel!
    @IBOutlet weak var textFieldSelectFilter: AdviserPageTextField!
    @IBOutlet weak var textfieldSearchAdviser: AdviserPageTextField!
    @IBOutlet weak var lblAdviser: AdviserPageLabel!
    @IBOutlet weak var advisorsCollectionView: UICollectionView!
    //MARK: -  View Life Cycle Methods
    func GetData(_ notification: [String : String]) {
        category_id = notification["category_id"] ?? ""
        
        descriptionForSelectedCategories = notification["descriptionForSelectedCategories"] ?? ""
        
        selectedTypeAdvisor = notification["selectedTypeAdvisor"] ?? ""
        
        textfieldSearchAdviser?.text = ""
        
        textFieldSelectFilter?.text = filterArray[0]
        lblSelectedAdviser?.text = filterArray[0]
        selectedFilter = filterArray[0]
        searchedText = ""
        
        setValue()
        
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.GetAdvisorsDataWithPaginations()
        // PullToRefresh(searchString: textfieldSearchAdviser.text ?? "")
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.totalPagesLoad = 1
        GetAdvisorsDataWithPaginations()
        self.advisorsCollectionView.register(UINib(nibName:"NoDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoDataCollectionViewCell")
        textfieldSearchAdviser?.delegate = self
        textfieldSearchAdviser.returnKeyType = .search
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
            customTabBarController?.delegate = self
        }
        
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
        self.advisorsCollectionView.refreshControl = refreshControl
        
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        setupDelegateForPickerView()
        
        
        
        category_id = ""
        
        descriptionForSelectedCategories = ""
        
        selectedTypeAdvisor = ""
        
        textfieldSearchAdviser?.text = ""
        textFieldSelectFilter?.text = filterArray[0]
        lblSelectedAdviser?.text = filterArray[0]
        selectedFilter = filterArray[0]
        searchedText = ""
        
        
        // self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        //
        
        
        // Do any additional setup after loading the view.
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        if textField == textfieldSearchAdviser {
            self.totalPagesLoad = 1
            self.GetAdvisorsDataWithPaginations()
            // getAdviserData(searchString:textField.text ?? "")
        }
        return true
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 1 {
            category_id = ""
            
            descriptionForSelectedCategories = ""
            
            selectedTypeAdvisor = ""
            
            textfieldSearchAdviser?.text = ""
            textFieldSelectFilter?.text = filterArray[0]
            lblSelectedAdviser?.text = filterArray[0]
            selectedFilter = filterArray[0]
            searchedText = ""
            
            // advisorList.advisors = []
            //  advisorList.advisors.removeAll()
            ////            //advisorList.advisors = nil
            self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.advisorsCollectionView.reloadData()
            setValue()
            
        } else if tabBarController.selectedIndex == 0 || tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3{
            SingletonClass.sharedInstance.selectedCategoryIndex = -1
            category_id = ""
            
            descriptionForSelectedCategories = ""
            
            selectedTypeAdvisor = ""
            
            advisorList = []
            
            
            // advisorList.advisors.removeAll()
            //            //advisorList.advisors = nil
            self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.advisorsCollectionView.reloadData()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if category_id != "" {
            self.GetAdvisorsDataWithPaginations()
            // self.getAdviserData(searchString:  self.textfieldSearchAdviser.text ?? "")
        } else {
            if GoToDetailsPage {
                GoToDetailsPage = false
                setValue()
            }
            
        }
        self.customTabBarController?.showTabBar()
        //        self.customTabBarController?.showTabBar()
    }
    override func viewWillAppear(_ animated: Bool) {
        
        // self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        
        
    }
    
    //MARK: -  other methods
    func setValue() {
        
        
        if selectedTypeAdvisor == "" {
            lblAdviser.text = "All Advisors"
        } else {
            lblAdviser.text = "\(selectedTypeAdvisor) Advisors"
        }
        //self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
        lblDescription.text = descriptionForSelectedCategories
        self.GetAdvisorsDataWithPaginations()
        //  getAdviserData(searchString: textfieldSearchAdviser.text ?? "")
        // getAdviserDataWithEmptySearch()
    }
    //    @objc func getAdviserData(searchString : String) {
    //        let requestModelForadvisorList = advisorListRequestModel()
    //        requestModelForadvisorList.category_id = category_id
    //        requestModelForadvisorList.search_string = searchString
    //        requestModelForadvisorList.type = selectedFilter.lowercased()
    //        //requestModelForadvisorList.type = search_filter.lowercased()
    //        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
    //        requestModelForadvisorList.is_favourite = "0"
    //        //Utilities.showHud()
    //        WebServiceSubClass.advisorList(CategoryModel: requestModelForadvisorList, completion: {(json, status, response) in
    //           // Utilities.hideHud()
    //
    //            if status {
    //
    //                let categoryResModel = advisorListResModel.init(fromJson: json)
    //                self.advisorList = categoryResModel
    //
    //                    self.advisorsCollectionView.reloadData()
    //
    //            } else {
    //              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
    //            }
    //
    //        })
    //    }
    //    @objc func PullToRefresh(searchString : String) {
    //        let requestModelForadvisorList = advisorListRequestModel()
    //        requestModelForadvisorList.category_id = category_id
    //        requestModelForadvisorList.search_string = searchString
    //        requestModelForadvisorList.type = selectedFilter.lowercased()
    //        //requestModelForadvisorList.type = search_filter.lowercased()
    //        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
    //        requestModelForadvisorList.is_favourite = "0"
    ////        Utilities.showHud()
    //        WebServiceSubClass.advisorList(CategoryModel: requestModelForadvisorList, completion: {(json, status, response) in
    //            self.refreshControl.endRefreshing()
    ////            Utilities.hideHud()
    //            if status {
    //
    //                let categoryResModel = advisorListResModel.init(fromJson: json)
    //                self.advisorList = categoryResModel
    //
    //                self.advisorsCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
    //                self.advisorsCollectionView.reloadData()
    //            } else {
    //              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
    //            }
    //
    //        })
    //    }
    func setupChooseDropDown() {
        chooseFilterDropDown.anchorView = viewForShowFilterDropDown
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseFilterDropDown.bottomOffset = CGPoint(x: 0, y: viewForShowFilterDropDown.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseFilterDropDown.dataSource = filterArray
        chooseFilterDropDown.textFont = CustomFont.regular.returnFont(12)
        chooseFilterDropDown.animationEntranceOptions = .beginFromCurrentState
        // Action triggered on selection
        chooseFilterDropDown.selectionAction = { [self] (index, item) in
            
            
            self.lblSelectedAdviser.text = item
        }
    }
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        
        pickerViewForAll.generalPickerDelegate = self
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldSelectFilter {
            textFieldSelectFilter.inputView = pickerViewForAll
            textFieldSelectFilter.inputAccessoryView = pickerViewForAll.toolbar
            let indexOfA = filterArray.firstIndex(of: textFieldSelectFilter.text ?? "") ?? 0 // 0
            pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
            //pickerViewForAll.selectedRow(inComponent: indexOfA)
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == textfieldSearchAdviser {
            self.totalPagesLoad = 1
            self.GetAdvisorsDataWithPaginations()
            // getAdviserData(searchString:textField.text ?? "")
        }
    }
    //MARK: -  IBActions
    @IBAction func btnDropDownClick(_ sender: Any) {
        
        
        // chooseFilterDropDown.show()
    }
    @IBAction func textfieldSearchAdviserAction(_ sender: HomePageTextField) {
        
        
        
        
    }
    //MARK: -  collectionViewMethods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if FirstTimeLoad == true {
            
            return 10
            
        } else {
            if advisorList.count == 0 {
                return 1
            } else {
                
                return advisorList.count
            }
            
        }
        
        //return advisorList.advisors.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if FirstTimeLoad == true {
            let cell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: advisorsCell.reuseIdentifier, for: indexPath) as! advisorsCell
            
            
            cell.userProfileImage.image = UIImage(named: "")
            cell.adviserName.text = ""
            cell.categoryName.text = ""
            cell.lblRatting.text = ""
            cell.btnFavourite.isSelected = false
            cell.btnChat.isHidden = true
            
            
            
            cell.btnVideo.isHidden = true
            cell.btnAudio.isHidden = true
            cell.btnFavourite.isHidden = true
            cell.adviserActiveStatusImageView.isHidden = true
            
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            
            return cell
        } else {
            if advisorList.count == 0 {
                let NoDatacell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCollectionViewCell", for: indexPath) as! NoDataCollectionViewCell
                NoDatacell.img.image = UIImage(named: "ic_noAdvisorsFound")
                NoDatacell.lblNodataTitle.text = "No advisor found"
                //NodataTitleText.ItemList
                
                return NoDatacell
            } else {
                selectedIndexOfAdvisors = -1
                let cell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: advisorsCell.reuseIdentifier, for: indexPath) as! advisorsCell
                cell.btnFavourite.isHidden = false
                cell.adviserActiveStatusImageView.isHidden = false
                cell.userProfileImage.hideSkeleton()
                cell.adviserName.hideSkeleton()
                
                cell.categoryName.hideSkeleton()
                cell.adviserName.text = advisorList[indexPath.row].nickName
                
                if advisorList[indexPath.row].isFavourite == "0"{
                    cell.btnFavourite.isSelected = false
                } else {
                    cell.btnFavourite.isSelected = true
                }
                switch advisorList[indexPath.row].isAvailabile {
                case 0:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserInActive.value
                case 1:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserActive.value
                case 2:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserBusy.value
                default:
                    break
                }
                
                
                
                //        var arrayForCategoryName : [String] = []
                //        let arrayOfAdviserCategory = advisorList[indexPath.row].categories.split(separator: ",").map { String($0) }
                //        SingletonClass.sharedInstance.categoryListCustomer?.category.forEach({ (element) in
                //            arrayOfAdviserCategory?.forEach({ (subCategory) in
                //                if subCategory == element.id {
                //                    arrayForCategoryName.append(element.name)
                //                }
                //            })
                //        })
                cell.lblCategoryName.text = advisorList[indexPath.row].categoriesName
                //arrayForCategoryName.joined(separator: ", ").capitalized
                
                let num = NSNumber(value: advisorList[indexPath.row].ratings ?? 0)
                let formatter : NumberFormatter = NumberFormatter()
                formatter.numberStyle = .decimal
                let str = formatter.string(from: num)!
                cell.lblRatting.text = "- ⭐️ \(str)"
                
                cell.lblRatting.text = "- ⭐️ \(advisorList[indexPath.row].avgRating ?? "")"
                
                cell.btnChat.isHidden = true
                cell.btnVideo.isHidden = true
                cell.btnAudio.isHidden = true
                
                let pointsArr = advisorList[indexPath.row].availabileFor.split(separator: ",")
                if pointsArr.count != 0 {
                    for i in 0...(pointsArr.count ) - 1 {
                        
                        if pointsArr[i].lowercased() == typeOfCommunication.Audio.returnString().lowercased() {
                            cell.btnAudio.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Chat.returnString().lowercased() {
                            cell.btnChat.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Video.returnString().lowercased() {
                            cell.btnVideo.isHidden = false
                        }
                        
                    }
                } else {
                    cell.btnChat.isHidden = true
                    cell.btnVideo.isHidden = true
                    cell.btnAudio.isHidden = true
                }
                
                
                if (advisorList[indexPath.row].profilePicture ?? "") == "" {
                    cell.userProfileImage.image = UIImage(named: "user_dummy_profile")
                } else {
                    //                    let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
                    //                    cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
                    let imgURL = advisorList[indexPath.row].profilePicture ?? ""
                    
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    
                    cell.userProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                    //                    cell.userProfileImage.sd_setImage(with: strURl, completed: {_,_,_,_ in
                    //                        cell.userProfileImage.hideSkeleton()
                    //                    })
                    cell.userProfileImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                
                
                
                
                cell.closourForFavourite = {
                    if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                        //                SingletonClass.sharedInstance.selectedCategoryIndex = -1
                        //                SingletonClass.sharedInstance.category_id = ""
                        //                SingletonClass.sharedInstance.descriptionForSelectedCategories = ""
                        //                SingletonClass.sharedInstance.selectedTypeAdvisor = ""
                        appDel.navigateToUserLogin()
                        userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                    }else {
                        let favouriteModel = FavouriteReqModel()
                        favouriteModel.advisor_id = self.advisorList[indexPath.row].id ?? ""
                        favouriteModel.user_id = SingletonClass.sharedInstance.UserId
                        //                        if cell.isFavourite == true {
                        //                            favouriteModel.status = "1"
                        //                        } else {
                        //                            favouriteModel.status = "0"
                        //                        }
                        if cell.btnFavourite.isSelected {
                            favouriteModel.status = "1"
                        } else {
                            favouriteModel.status = "0"
                        }
                        cell.isUserInteractionEnabled = false
                        //cell.btnFavourite.isUserInteractionEnabled = false
                        //Utilities.showHud()
                        WebServiceSubClass.AddToFavourite(CategoryModel: favouriteModel, completion: {(json, status, response) in
                            cell.isUserInteractionEnabled = true
                            //cell.btnFavourite.isUserInteractionEnabled = true
                            if status {
                                self.GetAdvisorsDataWithPaginations()
                                // self.getAdviserData(searchString: self.textfieldSearchAdviser.text ?? "")
                            } else {
                                if self.advisorList[indexPath.row].isFavourite == "0"{
                                    cell.btnFavourite.isSelected = false
                                } else {
                                    cell.btnFavourite.isSelected = true
                                }
                                // Utilities.hideHud()
                            }
                        })
                    }
                    
                    
                }
                
                return cell
            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FirstTimeLoad == true {
            var heightOfCell:CGFloat = 0.0
            let widthOfCell  = (advisorsCollectionView.frame.size.width - 14) / 2
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                heightOfCell = layout.itemSize.height
                
            }
            heightOfCell = widthOfCell + 56
            return CGSize(width: widthOfCell, height: heightOfCell)
        } else {
            if advisorList.count == 0 {
                return CGSize(width: self.advisorsCollectionView.frame.width, height: self.advisorsCollectionView.frame.height)
            } else {
                
                var heightOfCell:CGFloat = 0.0
                let widthOfCell  = (advisorsCollectionView.frame.size.width - 14) / 2
                if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    heightOfCell = layout.itemSize.height
                    
                }
                heightOfCell = widthOfCell + 56
                return CGSize(width: widthOfCell, height: heightOfCell)
            }
            
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if advisorList.count == 0 {
            
        } else {
            
            let controller:AdviserDetailsViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
            
            selectedIndexOfAdvisors = indexPath.row
            GoToDetailsPage = true
            controller.SelectedCategoryName = selectedTypeAdvisor
            controller.selectedAdviserDetails = advisorList[indexPath.row]
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if indexPath.row == advisorList.count - 1 && isLoadingList == false
        {
            self.isLoadingList = true
            LoadMoreData()
        }
    }
    //MARK: -  API Calls
}
class advisorsCell : UICollectionViewCell {
    
    var closourForFavourite : (() -> ())?
    //var isFavourite = false
    @IBOutlet weak var adviserActiveStatusImageView: UIImageView!
    
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblRatting: AdviserPageLabel!
    @IBAction func btnFavouriteClick(_ sender: UIButton) {
        if sender.isSelected {
            //isFavourite = false
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
        if let click = self.closourForFavourite {
            click()
        }
    }
    override func awakeFromNib() {
        lblCategoryName.numberOfLines = 2
        
        userProfileImage.contentMode = .scaleAspectFill
        userProfileImage.isSkeletonable = true
        
        lblAdviserName.isSkeletonable = true
        
        lblCategoryName.isSkeletonable = true
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        //cell.ca.numberOfLines = 1
        
        
        userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        //        adviserName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        //
        //        categoryName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        
    }
    @IBOutlet weak var categoryName: AdviserPageLabel!
    @IBOutlet weak var adviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblAdviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblCategoryName: AdviserPageLabel!
    @IBOutlet weak var icLiveOn: UIImageView!
    
    
    @IBAction func btnChatClick(_ sender: Any) {
    }
    
    @IBAction func btnAudioClick(_ sender: Any) {
    }
    
    
    @IBAction func btnVideoClick(_ sender: Any) {
    }
    
}
enum typeOfCommunication {
    case Chat,Audio,Video
    
    
    func returnString()->String{
        switch self {
        case .Chat:
            return "Chat"
        case .Audio:
            return "Audio"
        case .Video:
            return "Video"
            
        }
    }
    
    
}
extension AdvisorsViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        advisorList.removeAll()
        
        let item = filterArray[pickerViewForAll.selectedRow(inComponent: 0)]
        
        if item == "Top rating" {
            self.selectedFilter = "ratings"
        }
        
        else {
            self.selectedFilter = item
        }
        
        self.textFieldSelectFilter.text = item
        
        self.lblSelectedAdviser.text = item
        self.textFieldSelectFilter.resignFirstResponder()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.20, execute: {
            self.totalPagesLoad = 1
            self.GetAdvisorsDataWithPaginations()
            //            self.getAdviserData(searchString: self.textfieldSearchAdviser.text ?? "")
        })
        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension AdvisorsViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return filterArray.count
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return filterArray[row]
        
        
    }
    
}
extension AdvisorsViewController {
    func GetAdvisorsDataWithPaginations() {
        self.advisorList = []
        ReloadAdvisorData(pageNumber: "1")
    }
    func ReloadAdvisorData(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            Utilities.hideHud()
            
            self.FirstTimeLoad = false
            self.advisorsCollectionView.reloadData()
            Utilities.hideHud()
        } else {
            let requestModelForadvisorList = AdviserListRequestModel()
            requestModelForadvisorList.category_id = category_id
            requestModelForadvisorList.search_string = self.textfieldSearchAdviser.text ?? ""
            requestModelForadvisorList.type = selectedFilter.lowercased()
            //requestModelForadvisorList.type = search_filter.lowercased()
            requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
            requestModelForadvisorList.page = "\(pageNumber)"
            requestModelForadvisorList.is_favourite = "0"
            //Utilities.showHud()
            webServiceCallForAdvisorDetailsWithPagination(RequestModel: requestModelForadvisorList)
            
        }
    }
    func webServiceCallForAdvisorDetailsWithPagination(RequestModel:AdviserListRequestModel) {
        WebServiceSubClass.adviserList1(CategoryModel: RequestModel, completion: {(json, status, response) in
            self.advisorsCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            if status {
                
                let categoryResModel = adviserListResModel.init(fromJson: json)
                
                categoryResModel.advisors.forEach { element in
                    if self.advisorList.contains(where: {$0.id == element.id}) {
                        
                    } else {
                        self.advisorList.append(element)
                    }
                
                }
                
//                self.advisorList.append(contentsOf: categoryResModel.advisors)
                let AdvisorListFormJSON:[Advisor] = categoryResModel.advisors
//
                let pageNumberLoaded = Int(RequestModel.page) ?? 0
//                print(pageNumberLoaded)
                
                if AdvisorListFormJSON.count == 0 {
                    Utilities.hideHud()
                } else {
                    self.ReloadAdvisorData(pageNumber: "\(pageNumberLoaded + 1)")
                }
                
            } else {
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    
                    
                    self.FirstTimeLoad = false
                    self.advisorsCollectionView.reloadData()
                }
                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
    
    func LoadMoreData() {
        print(#function)
        let requestModelForadvisorList = AdviserListRequestModel()
        requestModelForadvisorList.category_id = category_id
        requestModelForadvisorList.search_string = self.textfieldSearchAdviser.text ?? ""
        requestModelForadvisorList.type = selectedFilter.lowercased()
        //requestModelForadvisorList.type = search_filter.lowercased()
        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
        requestModelForadvisorList.page = "\(totalPagesLoad + 1)"
        requestModelForadvisorList.is_favourite = "0"
        
        webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
    }
    func webserviceForLoadMoreData(reqModel:AdviserListRequestModel) {
        WebServiceSubClass.adviserList1(CategoryModel: reqModel, completion: {(json, status, response) in
            self.isLoadingList = false
            self.refreshControl.endRefreshing()
            self.advisorsCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
            if status {
                
                let categoryResModel = adviserListResModel.init(fromJson: json)
                
                categoryResModel.advisors.forEach { element in
                    if self.advisorList.contains(where: {$0.id == element.id}) {
                        
                    } else {
                        self.advisorList.append(element)
                    }
                
                }
                
                //self.advisorList.append(contentsOf: categoryResModel.advisors)
                DispatchQueue.main.async {
                    if categoryResModel.advisors.count != 0 {
                        self.totalPagesLoad  = self.totalPagesLoad + 1
                    }
                    
                    self.advisorsCollectionView.reloadData()
                }
                
            } else {
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    self.FirstTimeLoad = false
                    self.advisorsCollectionView.reloadData()
                }
            }
        })
    }
}

*/


//
//  AdvisorsViewController.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//




import UIKit
import DropDown
import SDWebImage
import PullToRefreshKit
import SkeletonView
import Firebase

class AdvisorsViewController: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITabBarControllerDelegate {
    
    //MARK: -  Properties
    var pageNumber = 1
    var pagelimit = 10
    var isNeedToReload = false
    var FirstTimeLoad = true
    
    var isFirstTime = false
    var selectedIndexOfAdvisors : Int = -1
    
    var advisorList : [Advisor] = []
    
    
    var refreshControl = UIRefreshControl()
    
    var fromSelectedCategory : Bool = false
    var category_id : String = ""
    var user_id : String = ""
    var type : String = ""
    var search_string : String = ""
    
    var descriptionForSelectedCategories = ""
    var selectedTypeAdvisor = ""
    
    var selectedFilter = ""
    var searchedText = ""
    var customTabBarController : CustomTabBarVC?
    
    //var advisorList : [Advisor] = []
    //var adviserList : adviserListResModel?

    let pickerViewForAll = GeneralPickerView()
    var filterArray : [String] = [
        "All",
        "Top rating",
        "Chat",
        "Audio",
        "Video"
    ]
    let chooseFilterDropDown = DropDown()
    
    @IBOutlet weak var viewForShowFilterDropDown: viewViewClearBG!
    var searchTask: DispatchWorkItem?
    var GoToDetailsPage : Bool = false
    //MARK: -  IBOutlets
    @IBOutlet weak var btnSelectAdviser: UIButton!
    @IBOutlet weak var LblFreeMinutes: AdviserPageLabel!
    @IBOutlet weak var lblSelectedAdviser: AdviserPageLabel!
    @IBOutlet weak var lblDescription: AdviserPageLabel!
    @IBOutlet weak var textFieldSelectFilter: AdviserPageTextField!
    @IBOutlet weak var textfieldSearchAdviser: AdviserPageTextField!
    @IBOutlet weak var lblAdviser: AdviserPageLabel!
    @IBOutlet weak var advisorsCollectionView: UICollectionView!
    //MARK: -  View Life Cycle Methods
    func GetData(_ notification: [String : String]) {
        //print("Advisor List :: \(#function)")
        category_id = notification["category_id"] ?? ""
        
        descriptionForSelectedCategories = notification["descriptionForSelectedCategories"] ?? ""
        
        selectedTypeAdvisor = notification["selectedTypeAdvisor"] ?? ""

        textfieldSearchAdviser?.text = ""

        textFieldSelectFilter?.text = filterArray[0]
        lblSelectedAdviser?.text = filterArray[0]
        selectedFilter = filterArray[0]
        searchedText = ""
        
        setValue()
        
     }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.latestReload()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MessageScreenNotification"), object: nil, userInfo: nil)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NotifyMeNotification"), object: nil, userInfo: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(selectAdvisorTab(_:)), name: NSNotification.Name.init(rawValue: "SelectAdvisorTab"), object: nil)
        
//        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
//          AnalyticsParameterItemID: "id-viewDidLoad",
//            AnalyticsParameterItemName: viewDidLoad,
//          AnalyticsParameterContentType: "cont",
//        ])
        
       
        
        //print("Advisor List :: \(#function)")
//        if !isFirstTime {
//            isFirstTime = true
//            GoToDetailsPage = true
//        }
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                if let NavigationController = appDel.window?.rootViewController as? UINavigationController {
                    if let topVC = (NavigationController.children.first?.children.first as? UINavigationController)?.viewControllers {
                        for controller in topVC as Array {
                            if controller.isKind(of: HomeViewController.self) {
                                let HomeVC = controller as! HomeViewController
                                HomeVC.OnSocket()
                                HomeVC.RegisterForVOIP()
                                HomeVC.getMinutesData()
                                HomeVC.FirstTimeMinutesData()
                                
                                break
                            }
                        }
                    }
                    
                }
            })
            
        }
        self.advisorsCollectionView.register(UINib(nibName:"NoDataCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NoDataCollectionViewCell")
        textfieldSearchAdviser?.delegate = self
        textfieldSearchAdviser.returnKeyType = .search
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
            customTabBarController?.delegate = self
        }
        
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
         self.advisorsCollectionView.refreshControl = refreshControl
       
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        setupDelegateForPickerView()
        
        
//        let elasticHeader = ElasticRefreshHeader()
//        elasticHeader.backgroundColor = .clear
//        self.advisorsCollectionView.configRefreshHeader(with: elasticHeader,container:self)  { [weak self] in
//
 //           self?.PullToRefresh(searchString: self?.textfieldSearchAdviser.text ?? "")
//
//        }
//
//        self.advisorsCollectionView.footerAlwaysAtBottom = true
        
        category_id = ""
        
        descriptionForSelectedCategories = ""
        
        selectedTypeAdvisor = ""
       
        textfieldSearchAdviser?.text = ""
        textFieldSelectFilter?.text = filterArray[0]
        lblSelectedAdviser?.text = filterArray[0]
        selectedFilter = filterArray[0]
        searchedText = ""
        
        self.advisorsCollectionView.decelerationRate = .normal;
       // self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        // Do any additional setup after loading the view.kk
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        //print("Advisor List :: \(#function)")
        textField.resignFirstResponder()
        if textField == textfieldSearchAdviser {
            self.latestReload()
        }
        return true
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if tabBarController.selectedIndex == 1 {
            category_id = ""
            
            descriptionForSelectedCategories = ""
            
            selectedTypeAdvisor = ""
            
            textfieldSearchAdviser?.text = ""
            textFieldSelectFilter?.text = filterArray[0]
            lblSelectedAdviser?.text = filterArray[0]
            selectedFilter = filterArray[0]
            searchedText = ""
            
            // advisorList.advisors = []
            //  advisorList.advisors.removeAll()
            ////            //advisorList.advisors = nil
            self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.advisorsCollectionView.reloadData()
            setValue()
            
        } else if tabBarController.selectedIndex == 0 || tabBarController.selectedIndex == 2 || tabBarController.selectedIndex == 3 {
            SingletonClass.sharedInstance.selectedCategoryIndex = -1
            category_id = ""
            
            descriptionForSelectedCategories = ""
            
            selectedTypeAdvisor = ""
            
            advisorList = []
            
            
            // advisorList.advisors.removeAll()
            //            //advisorList.advisors = nil
            self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.advisorsCollectionView.reloadData()
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if category_id != "" {
            GetAllListedData()
//            self.latestReload()
            
            // self.getAdviserData(searchString:  self.textfieldSearchAdviser.text ?? "")
        } else {
            if GoToDetailsPage {
                GoToDetailsPage = false
                setValue()
            }
            
        }
        self.customTabBarController?.showTabBar()
        //        self.customTabBarController?.showTabBar()
        
        // Open review popup when user back from this screen 5 or more time
        if !userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            if userDefault.integer(forKey: .advisorDetailScreenOpenCount) > 5 {
                AppStoreReviewManager.requestReviewIfAppropriate()
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if !SingletonClass.sharedInstance.OpenFirstTimeListing {
            SingletonClass.sharedInstance.OpenFirstTimeListing = true
            category_id = ""
            
            descriptionForSelectedCategories = ""
            
            selectedTypeAdvisor = ""
           
            textfieldSearchAdviser?.text = ""
            textFieldSelectFilter?.text = filterArray[0]
            lblSelectedAdviser?.text = filterArray[0]
            selectedFilter = filterArray[0]
            searchedText = ""
            
           // adviserList?.advisors = []
            advisorList = []
          //  adviserList?.advisors.removeAll()
////            //adviserList?.advisors = nil
            self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.advisorsCollectionView.reloadData()
            setValue()
        }
        //print("Advisor List :: \(#function)")
       // self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
       
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        isFirstTime = false
        SingletonClass.sharedInstance.selectedCategoryIndex = -1
        category_id = ""
        
        descriptionForSelectedCategories = ""
        
        selectedTypeAdvisor = ""
        
      advisorList = []
        
     
       // adviserList?.advisors.removeAll()
//            //adviserList?.advisors = nil
//        self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
//        self.advisorsCollectionView.reloadData()
        //print("Advisor List :: \(#function)")
    }
    
    //MARK: -  other methods
    
    @objc func selectAdvisorTab(_ notification: Notification) {
        self.tabBarController?.selectedIndex = 1
        self.advisorsCollectionView.reloadData()
        setValue()
    }
    
    func setValue() {
        //print("Advisor List :: \(#function)")
        if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
            if SingletonClass.sharedInstance.FreeMinutes == "1" {
                LblFreeMinutes.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minute first reading"
            } else {
                LblFreeMinutes.text = "\(SingletonClass.sharedInstance.FreeMinutes) Free minutes first reading"
            }
            
        } else {
            LblFreeMinutes.text = ""
        }
        DispatchQueue.main.async { [self] in
            if selectedTypeAdvisor == "" {
                lblAdviser.text = "All Advisors"
            } else {
                lblAdviser.text = "\(selectedTypeAdvisor)"
            }
            //self.advisorsCollectionView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            lblDescription.text = descriptionForSelectedCategories
        }
        
        self.latestReload()
       // getAdviserDataWithEmptySearch()
    }
//    @objc func getAdviserData(searchString : String) {
//        //print("Advisor List :: \(#function)")
//        let requestModelForAdviserList = AdviserListRequestModel()
//        requestModelForAdviserList.category_id = category_id
//        requestModelForAdviserList.search_string = searchString
//        requestModelForAdviserList.type = selectedFilter.lowercased()
//        //requestModelForAdviserList.type = search_filter.lowercased()
//        requestModelForAdviserList.user_id = SingletonClass.sharedInstance.UserId
//        requestModelForAdviserList.is_favourite = "0"
//        //Utilities.showHud()
//        WebServiceSubClass.adviserList(CategoryModel: requestModelForAdviserList, completion: {(json, status, response) in
//           // Utilities.hideHud()
//
//            if status {
//
//                let categoryResModel = adviserListResModel.init(fromJson: json)
//                self.adviserList = categoryResModel
//
//                    self.advisorsCollectionView.reloadData()
//
//            } else {
//                Utilities.displayAlert("", message: json["message"].string ?? "")
//            }
//        })
//    }
//    @objc func PullToRefresh(searchString : String) {
//        //print("Advisor List :: \(#function)")
//        let requestModelForAdviserList = AdviserListRequestModel()
//        requestModelForAdviserList.category_id = category_id
//        requestModelForAdviserList.search_string = searchString
//        requestModelForAdviserList.type = selectedFilter.lowercased()
//        //requestModelForAdviserList.type = search_filter.lowercased()
//        requestModelForAdviserList.user_id = SingletonClass.sharedInstance.UserId
//        requestModelForAdviserList.is_favourite = "0"
////        Utilities.showHud()
//        WebServiceSubClass.adviserList(CategoryModel: requestModelForAdviserList, completion: {(json, status, response) in
//            self.refreshControl.endRefreshing()
////            Utilities.hideHud()
//            if status {
//
//                let categoryResModel = adviserListResModel.init(fromJson: json)
//                self.adviserList = categoryResModel
//
//                self.advisorsCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
//                self.advisorsCollectionView.reloadData()
//            } else {
//                Utilities.displayAlert("", message: json["message"].string ?? "")
//            }
//
//        })
//    }
    func setupChooseDropDown() {
        //print("Advisor List :: \(#function)")
        chooseFilterDropDown.anchorView = viewForShowFilterDropDown
        
        // By default, the dropdown will have its origin on the top left corner of its anchor view
        // So it will come over the anchor view and hide it completely
        // If you want to have the dropdown underneath your anchor view, you can do this:
        chooseFilterDropDown.bottomOffset = CGPoint(x: 0, y: viewForShowFilterDropDown.bounds.height)
        
        // You can also use localizationKeysDataSource instead. Check the docs.
        chooseFilterDropDown.dataSource = filterArray
        chooseFilterDropDown.textFont = CustomFont.regular.returnFont(12)
        chooseFilterDropDown.animationEntranceOptions = .beginFromCurrentState
        // Action triggered on selection
        chooseFilterDropDown.selectionAction = { [self] (index, item) in
            
          
            self.lblSelectedAdviser.text = item
        }
    }
    func setupDelegateForPickerView() {
        //print("Advisor List :: \(#function)")
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        
        pickerViewForAll.generalPickerDelegate = self
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        //print("Advisor List :: \(#function)")
        if textField == textFieldSelectFilter {
            textFieldSelectFilter.inputView = pickerViewForAll
            textFieldSelectFilter.inputAccessoryView = pickerViewForAll.toolbar
            let indexOfA = filterArray.firstIndex(of: textFieldSelectFilter.text ?? "") ?? 0 // 0
            pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
            //pickerViewForAll.selectedRow(inComponent: indexOfA)
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //print("Advisor List :: \(#function)")
        if textField == textfieldSearchAdviser {
            self.latestReload()
        }
    }
    //MARK: -  IBActions
    @IBAction func btnDropDownClick(_ sender: Any) {
       
      
       // chooseFilterDropDown.show()
    }
    @IBAction func textfieldSearchAdviserAction(_ sender: HomePageTextField) {
  
        //print("Advisor List :: \(#function)")

        
    }
    //MARK: -  collectionViewMethods
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if FirstTimeLoad == true {
            
            return 10
            
        } else {
            if advisorList.count == 0 {
                return 1
            } else {
                
                return advisorList.count
            }
            
        }
        
        //return advisorList.advisors.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if FirstTimeLoad == true {
            let cell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: advisorsCell.reuseIdentifier, for: indexPath) as! advisorsCell
            
            
            cell.userProfileImage.image = UIImage(named: "")
            cell.adviserName.text = ""
            cell.categoryName.text = ""
            cell.lblRatting.text = ""
            cell.btnFavourite.isSelected = false
            cell.btnChat.isHidden = true
            
            
            
            cell.btnVideo.isHidden = true
            cell.btnAudio.isHidden = true
            cell.btnFavourite.isHidden = true
            cell.adviserActiveStatusImageView.isHidden = true
            
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
            cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
            
            return cell
        } else {
            if advisorList.count == 0 {
                let NoDatacell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: "NoDataCollectionViewCell", for: indexPath) as! NoDataCollectionViewCell
                NoDatacell.img.image = UIImage(named: "ic_noAdvisorsFound")
                NoDatacell.lblNodataTitle.text = "No advisor found"
                //NodataTitleText.ItemList
                
                return NoDatacell
            } else {
                selectedIndexOfAdvisors = -1
                let cell = advisorsCollectionView.dequeueReusableCell(withReuseIdentifier: advisorsCell.reuseIdentifier, for: indexPath) as! advisorsCell
                cell.btnFavourite.isHidden = false
                cell.adviserActiveStatusImageView.isHidden = false
                cell.userProfileImage.hideSkeleton()
                cell.adviserName.hideSkeleton()
                
                cell.categoryName.hideSkeleton()
                cell.adviserName.text = advisorList[indexPath.row].nickName
                
                if advisorList[indexPath.row].isFavourite == "0"{
                    cell.btnFavourite.isSelected = false
                } else {
                    cell.btnFavourite.isSelected = true
                }
                switch advisorList[indexPath.row].isAvailabile {
                case 0:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserInActive.value
                case 1:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserActive.value
                case 2:
                    cell.adviserActiveStatusImageView.backgroundColor = colors.AdviserBusy.value
                default:
                    break
                }
                
                
                
                //        var arrayForCategoryName : [String] = []
                //        let arrayOfAdviserCategory = advisorList[indexPath.row].categories.split(separator: ",").map { String($0) }
                //        SingletonClass.sharedInstance.categoryListCustomer?.category.forEach({ (element) in
                //            arrayOfAdviserCategory?.forEach({ (subCategory) in
                //                if subCategory == element.id {
                //                    arrayForCategoryName.append(element.name)
                //                }
                //            })
                //        })
                cell.lblCategoryName.text = advisorList[indexPath.row].categoriesName
                //arrayForCategoryName.joined(separator: ", ").capitalized
                
                let num = NSNumber(value: advisorList[indexPath.row].ratings ?? 0)
                let formatter : NumberFormatter = NumberFormatter()
                formatter.numberStyle = .decimal
                let str = formatter.string(from: num)!
                cell.lblRatting.text = "- ⭐️ \(str)"
                
                cell.lblRatting.text = "- ⭐️ \(advisorList[indexPath.row].avgRating ?? "")"
                
                cell.btnChat.isHidden = true
                cell.btnVideo.isHidden = true
                cell.btnAudio.isHidden = true
                
                let pointsArr = advisorList[indexPath.row].availabileFor.split(separator: ",")
                if pointsArr.count != 0 {
                    for i in 0...(pointsArr.count ) - 1 {
                        
                        if pointsArr[i].lowercased() == typeOfCommunication.Audio.returnString().lowercased() {
                            cell.btnAudio.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Chat.returnString().lowercased() {
                            cell.btnChat.isHidden = false
                        } else if pointsArr[i].lowercased() == typeOfCommunication.Video.returnString().lowercased() {
                            cell.btnVideo.isHidden = false
                        }
                        
                    }
                } else {
                    cell.btnChat.isHidden = true
                    cell.btnVideo.isHidden = true
                    cell.btnAudio.isHidden = true
                }
                cell.userProfileImage.image = UIImage(named: "user_dummy_profile")
                
                if (advisorList[indexPath.row].profilePicture ?? "") == "" {
                    cell.userProfileImage.image = UIImage(named: "user_dummy_profile")
                } else {
                    //                    let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
                    //                    cell.userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
                    let imgURL = advisorList[indexPath.row].profilePicture ?? ""
                    
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                    
                    
                    cell.userProfileImage.sd_imageIndicator = SDWebImageActivityIndicator.white
                    //                    cell.userProfileImage.sd_setImage(with: strURl, completed: {_,_,_,_ in
                    //                        cell.userProfileImage.hideSkeleton()
                    //                    })
                    cell.userProfileImage.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                
                
                
                
                cell.closourForFavourite = {
                    if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                        //                SingletonClass.sharedInstance.selectedCategoryIndex = -1
                        //                SingletonClass.sharedInstance.category_id = ""
                        //                SingletonClass.sharedInstance.descriptionForSelectedCategories = ""
                        //                SingletonClass.sharedInstance.selectedTypeAdvisor = ""
                        appDel.navigateToUserLogin()
                        userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                    }else {
                        Utilities.showHud()
                        let favouriteModel = FavouriteReqModel()
                        favouriteModel.advisor_id = self.advisorList[indexPath.row].id ?? ""
                        favouriteModel.user_id = SingletonClass.sharedInstance.UserId
                        //                        if cell.isFavourite == true {
                        //                            favouriteModel.status = "1"
                        //                        } else {
                        //                            favouriteModel.status = "0"
                        //                        }
                        if cell.btnFavourite.isSelected {
                            favouriteModel.status = "1"
                        } else {
                            favouriteModel.status = "0"
                        }
                        cell.isUserInteractionEnabled = false
                        //cell.btnFavourite.isUserInteractionEnabled = false
                        //Utilities.showHud()
                        WebServiceSubClass.AddToFavourite(CategoryModel: favouriteModel, completion: {(json, status, response) in
                            cell.isUserInteractionEnabled = true
                            //cell.btnFavourite.isUserInteractionEnabled = true
                            if status {
                                self.GetAllListedData()
                                
                                // self.getAdviserData(searchString: self.textfieldSearchAdviser.text ?? "")
                            } else {
                                if self.advisorList[indexPath.row].isFavourite == "0"{
                                    cell.btnFavourite.isSelected = false
                                } else {
                                    cell.btnFavourite.isSelected = true
                                }
                                // Utilities.hideHud()
                            }
                        })
                    }
                    
                    
                }
                
                return cell
            }
            
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if FirstTimeLoad == true {
            var heightOfCell:CGFloat = 0.0
            let widthOfCell  = (advisorsCollectionView.frame.size.width - 14) / 2
            if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                heightOfCell = layout.itemSize.height
                
            }
            heightOfCell = widthOfCell + 56
            return CGSize(width: widthOfCell, height: heightOfCell)
        } else {
            if advisorList.count == 0 {
                return CGSize(width: self.advisorsCollectionView.frame.width, height: self.advisorsCollectionView.frame.height)
            } else {
                
                var heightOfCell:CGFloat = 0.0
                let widthOfCell  = (advisorsCollectionView.frame.size.width - 14) / 2
                if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                    heightOfCell = layout.itemSize.height
                    
                }
                heightOfCell = widthOfCell + 56
                return CGSize(width: widthOfCell, height: heightOfCell)
            }
            
        }
        
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if advisorList.count == 0 {
            
        } else {
            
            let controller:AdviserDetailsViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AdviserDetailsViewController.storyboardID) as! AdviserDetailsViewController
            
            selectedIndexOfAdvisors = indexPath.row
            GoToDetailsPage = true
            controller.SelectedCategoryName = selectedTypeAdvisor
            controller.selectedAdviserDetails = advisorList[indexPath.row]
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath)
    {
        if advisorList.count != 0 {
            if indexPath.row == advisorList.count - 4 && isNeedToReload == true
            {
                ReloadMore()
            } else {
                self.advisorsCollectionView.switchRefreshFooter(to: .removed)
            }
        } else {
//
        }
       
    }
    //MARK: -  API Calls
}
class advisorsCell : UICollectionViewCell {
    
    var closourForFavourite : (() -> ())?
    //var isFavourite = false
    @IBOutlet weak var adviserActiveStatusImageView: UIImageView!
    
    @IBOutlet weak var btnVideo: UIButton!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var btnChat: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var lblRatting: AdviserPageLabel!
    @IBAction func btnFavouriteClick(_ sender: UIButton) {
        if sender.isSelected {
            //isFavourite = false
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
        
        if let click = self.closourForFavourite {
            click()
        }
    }
    override func awakeFromNib() {
        lblCategoryName.numberOfLines = 2
        
        userProfileImage.contentMode = .scaleAspectFill
        userProfileImage.isSkeletonable = true
        
        lblAdviserName.isSkeletonable = true
        
        lblCategoryName.isSkeletonable = true
        
        let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
        //cell.ca.numberOfLines = 1
       
        
        userProfileImage.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
//        adviserName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
//
//        categoryName.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        
    }
    @IBOutlet weak var categoryName: AdviserPageLabel!
    @IBOutlet weak var adviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblAdviserName: AdviserPageLabel!
    
    @IBOutlet weak var lblCategoryName: AdviserPageLabel!
    @IBOutlet weak var icLiveOn: UIImageView!
    
    
    @IBAction func btnChatClick(_ sender: Any) {
    }
    
    @IBAction func btnAudioClick(_ sender: Any) {
    }
    
    
    @IBAction func btnVideoClick(_ sender: Any) {
    }
    
}
enum typeOfCommunication {
    case Chat,Audio,Video
    
    
    func returnString()->String{
        switch self {
        case .Chat:
            return "Chat"
        case .Audio:
            return "Audio"
        case .Video:
            return "Video"
        
        }
    }
    

}
extension AdvisorsViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        advisorList.removeAll()
        
        let item = filterArray[pickerViewForAll.selectedRow(inComponent: 0)]
        
        if item == "Top rating" {
            self.selectedFilter = "ratings"
        }
        
        else {
            self.selectedFilter = item
        }
        
        self.textFieldSelectFilter.text = item
        
        self.lblSelectedAdviser.text = item
        self.textFieldSelectFilter.resignFirstResponder()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.20, execute: {
            
            self.latestReload()
            
            //            self.getAdviserData(searchString: self.textfieldSearchAdviser.text ?? "")
        })
        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension AdvisorsViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return filterArray.count
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return filterArray[row]
        
        
    }
    
}
extension AdvisorsViewController {
    func latestReload(){
        pageNumber = 1
        isNeedToReload = false
        
        let requestModelForadvisorList = AdviserListRequestModel()
        requestModelForadvisorList.category_id = category_id
        requestModelForadvisorList.search_string = self.textfieldSearchAdviser.text ?? ""
        requestModelForadvisorList.type = selectedFilter.lowercased()
        //requestModelForadvisorList.type = search_filter.lowercased()
        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
        requestModelForadvisorList.page = "\(pageNumber)"
        requestModelForadvisorList.is_favourite = "0"
        
        webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
        
    }
    func ReloadMore(){
        print(#function)
        pageNumber += 1
        isNeedToReload = false
       
        let requestModelForadvisorList = AdviserListRequestModel()
        requestModelForadvisorList.category_id = category_id
        requestModelForadvisorList.search_string = self.textfieldSearchAdviser.text ?? ""
        requestModelForadvisorList.type = selectedFilter.lowercased()
        //requestModelForadvisorList.type = search_filter.lowercased()
        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
        requestModelForadvisorList.page = "\(pageNumber)"
        requestModelForadvisorList.is_favourite = "0"
        webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
       
    }
    
}
extension AdvisorsViewController {
    
    func webserviceForLoadMoreData(reqModel:AdviserListRequestModel){
        Utilities.showHud()
        WebServiceSubClass.adviserList1(CategoryModel: reqModel, completion: { (json, status, response) in
            Utilities.hideHud()
            self.refreshControl.endRefreshing()
            if status {
                self.FirstTimeLoad = false
                self.advisorsCollectionView.isHidden = false
                print(response)
                
                
                let objresponse = adviserListResModel.init(fromJson: json)
               
                if objresponse.advisors.count == self.pagelimit {
                    self.isNeedToReload = true
                }
                else {
                    self.isNeedToReload = false
                }
                if self.pageNumber == 1 {
                    //let sortedtemp = objresponse.items.sorted(by: { $0.id > $1.id })
                    self.advisorList = objresponse.advisors
                 }
                 else{
                   // let sortedtemp = objresponse.items.sorted(by: { $0.id > $1.id })
                   self.advisorList.append(contentsOf: objresponse.advisors)
                 }
                self.advisorsCollectionView.reloadData()
                self.refreshControl.endRefreshing()
               
            }
            else{
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    self.FirstTimeLoad = false
                    self.advisorsCollectionView.reloadData()
                }
                
               
            }
        })
    }
    func GetAllListedData() {
        self.advisorList = []
        ReloadAdvisorData(pageNumberTemp: "1")
    }
    func ReloadAdvisorData(pageNumberTemp:String) {
        print(#function)
        if Int(pageNumberTemp) == self.pageNumber + 1 {
            Utilities.hideHud()
            
            self.FirstTimeLoad = false
            sleep(1)
            self.advisorsCollectionView.reloadData()
            Utilities.hideHud()
        } else {
            let requestModelForadvisorList = AdviserListRequestModel()
            requestModelForadvisorList.category_id = category_id
            requestModelForadvisorList.search_string = self.textfieldSearchAdviser.text ?? ""
            requestModelForadvisorList.type = selectedFilter.lowercased()
            //requestModelForadvisorList.type = search_filter.lowercased()
            requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
            requestModelForadvisorList.page = "\(pageNumberTemp)"
            requestModelForadvisorList.is_favourite = "0"
            //Utilities.showHud()
            webServiceCallForAdvisorDetailsWithPagination(RequestModel: requestModelForadvisorList)
            
        }
    }
    func webServiceCallForAdvisorDetailsWithPagination(RequestModel:AdviserListRequestModel) {
        WebServiceSubClass.adviserList1(CategoryModel: RequestModel, completion: {(json, status, response) in
            self.advisorsCollectionView.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            if status {
                
                let categoryResModel = adviserListResModel.init(fromJson: json)
                
                let AdvisorListFormJSON:[Advisor] = categoryResModel.advisors
//
                let pageNumberLoaded = Int(RequestModel.page) ?? 0
//
                self.advisorList.append(contentsOf: categoryResModel.advisors)

                if AdvisorListFormJSON.count == 0 {
                    Utilities.hideHud()
                } else {
                    sleep(1)
                    self.ReloadAdvisorData(pageNumberTemp: "\(pageNumberLoaded + 1)")
                }
                
            } else {
                Utilities.hideHud()
                if self.advisorList.count == 0 {
                    
                    
                    self.FirstTimeLoad = false
                    self.advisorsCollectionView.reloadData()
                }
                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}

//extension AdvisorsViewController {
//    func GetAdvisorsDataWithPaginations() {
//        self.advisorList = []
//        ReloadAdvisorData(pageNumber: "1")
//    }
//    func ReloadAdvisorData(pageNumber:String) {
//        print(#function)
//        if Int(pageNumber) == self.totalPagesLoad + 1 {
//            Utilities.hideHud()
//
//            self.FirstTimeLoad = false
//            self.myFavouriteCollectionView.reloadData()
//            Utilities.hideHud()
//        } else {
//            let requestModelForadvisorList = AdviserListRequestModel()
//            requestModelForadvisorList.category_id = ""
//            requestModelForadvisorList.search_string = ""
//            requestModelForadvisorList.type = ""
//            //requestModelForadvisorList.type = search_filter.lowercased()
//            requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
//            requestModelForadvisorList.page = "\(pageNumber)"
//            requestModelForadvisorList.is_favourite = "1"
//            //Utilities.showHud()
//            webServiceCallForAdvisorDetailsWithPagination(RequestModel: requestModelForadvisorList)
//
//        }
//    }
//    func webServiceCallForAdvisorDetailsWithPagination(RequestModel:AdviserListRequestModel) {
//        WebServiceSubClass.adviserList1(CategoryModel: RequestModel, completion: {(json, status, response) in
//            if status {
//
//                let categoryResModel = adviserListResModel.init(fromJson: json)
//                self.advisorList.append(contentsOf: categoryResModel.advisors)
//                let AdvisorListFormJSON:[Advisor] = categoryResModel.advisors
//
//                let pageNumberLoaded = Int(RequestModel.page) ?? 0
//                print(pageNumberLoaded)
//
//                if AdvisorListFormJSON.count == 0 {
//                    self.FirstTimeLoad = false
//                    self.myFavouriteCollectionView.reloadData()
//                    Utilities.hideHud()
//                } else {
//                    self.ReloadAdvisorData(pageNumber: "\(pageNumberLoaded + 1)")
//                }
//
//            } else {
//                Utilities.hideHud()
//                if self.advisorList.count == 0 {
//
//
//                    self.FirstTimeLoad = false
//                    self.myFavouriteCollectionView.reloadData()
//                }
//                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
//            }
//
//        })
//    }
//
//
//
//    func LoadMoreData() {
//        print(#function)
//        let requestModelForadvisorList = AdviserListRequestModel()
//        requestModelForadvisorList.category_id = ""
//        requestModelForadvisorList.search_string = ""
//        requestModelForadvisorList.type = ""
//        //requestModelForadvisorList.type = search_filter.lowercased()
//        requestModelForadvisorList.user_id = SingletonClass.sharedInstance.UserId
//        requestModelForadvisorList.page = "\(totalPagesLoad + 1)"
//        requestModelForadvisorList.is_favourite = "1"
//
//            webserviceForLoadMoreData(reqModel: requestModelForadvisorList)
//    }
//    func webserviceForLoadMoreData(reqModel:AdviserListRequestModel) {
//        WebServiceSubClass.adviserList1(CategoryModel: reqModel, completion: {(json, status, response) in
//            self.isLoadingList = false
//            if status {
//
//                let categoryResModel = adviserListResModel.init(fromJson: json)
//                self.advisorList.append(contentsOf: categoryResModel.advisors)
//
//
//                if categoryResModel.advisors.count != 0 {
//                    self.totalPagesLoad  = self.totalPagesLoad + 1
//                }
//
//                self.myFavouriteCollectionView.reloadData()
//
//            } else {
//                Utilities.hideHud()
//                if self.advisorList.count == 0 {
//
//                    self.FirstTimeLoad = false
//                    self.myFavouriteCollectionView.reloadData()
//                }
//            }
//
//        })
//    }
//
//
//}
//
//
//

