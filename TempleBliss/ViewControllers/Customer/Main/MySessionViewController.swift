//
//  MySessionViewController.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SkeletonView
import SDWebImage
class MySessionViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    //MARK: - Properties
    var isLoadingList : Bool = false
    var FirstTimeLoad = true
    var firstTimeTotalCount = 0
    var totalPagesLoad = 1
    var customTabBarController : CustomTabBarVC?
    var MainSessionListArray : [SessionList] = []
    var refreshControl = UIRefreshControl()
    var MysessionData : [[SessionList]] = []
    //MARK: - IBOutlets
    @IBOutlet weak var tblMySession: UITableView!
    //MARK: - View Life Cycle Methods
    @objc func GetMySessionDataOnNotification(){
        self.ReloadAllLoadedSessionData()
       }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "CustomerMySessionReload"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GetMySessionDataOnNotification), name: NSNotification.Name(rawValue: "CustomerMySessionReload"), object: nil)
        setLocalization()
     
        //Ankur's Change
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: true, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
      
        tblMySession.register(UINib(nibName:"NoDataTableViewCell", bundle: nil), forCellReuseIdentifier: "NoDataTableViewCell")
        //Ankur's Change
       
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = .white
         self.tblMySession.refreshControl = refreshControl
    
        // Do any additional setup after loading the view.
    }
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        totalPagesLoad = 1
        self.PullToRefreshGetData()

    }
    override func viewWillAppear(_ animated: Bool) {
      //  self.MysessionData = []
    //    self.tblMySession.reloadData()
       
      
    }
    override func viewDidAppear(_ animated: Bool) {
        self.customTabBarController?.showTabBar()
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
            appDel.navigateToUserLogin()
            userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
        } else {
            
            self.ReloadAllLoadedSessionData()
        }
    }
    
    func AttributedTextwithImageSuffix(AttributeImage : UIImage , AttributedText : String , buttonBound : UIButton) -> NSAttributedString
    {
        let fullString = NSMutableAttributedString(string: AttributedText + "  ")
        let image1Attachment = NSTextAttachment()
        image1Attachment.bounds = CGRect(x: 0, y: ((buttonBound.titleLabel?.font.capHeight ?? 0.0) - AttributeImage.size.height).rounded() / 2, width: AttributeImage.size.width, height: AttributeImage.size.height)
        image1Attachment.image = AttributeImage
        let image1String = NSAttributedString(attachment: image1Attachment)
        fullString.append(image1String)
        fullString.append(NSAttributedString(string: ""))
        return fullString
    }
    //MARK: - tableViewMethods
    func numberOfSections(in tableView: UITableView) -> Int {
        if FirstTimeLoad == true {
            return 1
        } else {
            if MysessionData.count == 0 {
                return 1
            } else {
                return MysessionData.count
            }
            
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if FirstTimeLoad == true {
            let headerView  = UIView()
            return headerView
        } else {
            if MysessionData.count == 0 {
                let headerView  = UIView()
                return headerView
            } else {
                
                let cell = tblMySession.dequeueReusableCell(withIdentifier: AdvisorHeaderCell.reuseIdentifier) as! AdvisorHeaderCell
                cell.SessionDate.text = convertDateFormater(MysessionData[section][0].bookingDate ?? "")
                return cell
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if FirstTimeLoad == true {
            return 7
        } else {
            if MysessionData.count == 0 {
                return 1
            } else {
                return MysessionData[section].count
            }
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if FirstTimeLoad == true {
            return 100
        } else {
            if MysessionData.count == 0 {
                return tableView.frame.size.height
            } else {
                return UITableView.automaticDimension
            }
            
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     
            if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !isLoadingList){
                
                if MysessionData.count != 0 {
                    self.isLoadingList = true
                    let spinner = UIActivityIndicatorView(style: .white)
                    spinner.startAnimating()
                    spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tblMySession.bounds.width, height: CGFloat(44))
                    
                    self.tblMySession.tableFooterView = spinner
                    self.tblMySession.tableFooterView?.isHidden = false
                    LoadMoreData()
                }
                   
                
            }
        
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if MysessionData.count != 0 {
            GetSessionSummaryDetails(bookingID: Int(MysessionData[indexPath.section][indexPath.row].bookingId) ?? 0)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if FirstTimeLoad == true {
            let cell = tblMySession.dequeueReusableCell(withIdentifier: mySessionCell.reuseIdentifier, for: indexPath) as! mySessionCell
            cell.mySessionAdvisorData.isHidden = true
            cell.mySessionGiveRattingView.isHidden = true
            cell.skeletonImageView.isHidden = false
           
//
            let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
                        cell.skeletonImageView.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
           // cell.lblPdfDescription.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)

            return cell
        } else {
            if MysessionData.count != 0 {
                let cell = tblMySession.dequeueReusableCell(withIdentifier: mySessionCell.reuseIdentifier, for: indexPath) as! mySessionCell
                cell.mySessionAdvisorData.isHidden = false
                cell.mySessionGiveRattingView.isHidden = false
                cell.skeletonImageView.isHidden = true
                
                if MysessionData[indexPath.section][indexPath.row].postUnreadMessageCount == "0" {
                    cell.btnPostChat.setImage(UIImage(named: "ic_messageMySession"), for: .normal)
                    cell.btnPostChat.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

                } else {
                    cell.btnPostChat.setImage(UIImage(named: "ic_PostChatWithMessage"), for: .normal)
                    cell.btnPostChat.imageEdgeInsets = UIEdgeInsets(top: -10, left: 0, bottom: 0, right: -12)
                }
                cell.advisorName.text = MysessionData[indexPath.section][indexPath.row].nickName
                
                if (MysessionData[indexPath.section][indexPath.row].profilePicture ?? "") == "" {
                    cell.advisorImageView.image = UIImage(named: "user_dummy_profile")
                } else {

                    let imgURL = MysessionData[indexPath.section][indexPath.row].profilePicture ?? ""
                    
                    let strURl = URL(string: APIEnvironment.profileBu + imgURL)
                   
                    cell.advisorImageView.sd_imageIndicator = SDWebImageActivityIndicator.white

                    cell.advisorImageView.sd_setImage(with: strURl,  placeholderImage: UIImage(named: "user_dummy_profile"))
                }
                
                
                switch MysessionData[indexPath.section][indexPath.row].type {
                case "video":
                    cell.communicationType.setImage(UIImage(named: "ic_videoMySession") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = false
                    cell.communicationType.alpha = 0.7
                   // cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_videoMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                    
                case "audio":
                    cell.communicationType.setImage(UIImage(named: "ic_AdviserDetails_audioCall") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = false
                    cell.communicationType.alpha = 0.7
                    
                   // cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_audioMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                    
                case "chat":
                    cell.communicationType.setImage(UIImage(named: "ic_chatMySession") ?? UIImage(), for: .normal)
                    cell.communicationType.setTitle((MysessionData[indexPath.section][indexPath.row].type ?? "").capitalized, for: .normal)
                    cell.communicationType.isUserInteractionEnabled = true
                    cell.communicationType.alpha = 1.0
                //    cell.communicationType.setAttributedTitle(AttributedTextwithImageSuffix(AttributeImage: UIImage(named: "ic_chatMySession") ?? UIImage(), AttributedText: MysessionData[indexPath.section][indexPath.row].type ?? "", buttonBound: cell.communicationType), for: .normal)
                    
                default:
                    break
                }
                
               
                cell.lblSessionTime.text = ConvertToTime(MysessionData[indexPath.section][indexPath.row].bookingDate ?? "")
              
                //convertDateFormater(MysessionData[indexPath.section][indexPath.row].bookingDate ?? "")
                if MysessionData[indexPath.section][indexPath.row].total_minutes ?? "" == "01.00" || MysessionData[indexPath.section][indexPath.row].total_minutes.components(separatedBy: ":")[0] == "00" {
                    cell.TotalTime.text = "\(MysessionData[indexPath.section][indexPath.row].total_minutes ?? "") Minute"
                } else {
                    cell.TotalTime.text = "\(MysessionData[indexPath.section][indexPath.row].total_minutes ?? "") Minutes"
                }
                cell.TotalMoney.text = "$ \(MysessionData[indexPath.section][indexPath.row].Amount ?? "")"
               // cell.TotalTime.text = ""
                cell.btnChat = {
                    let controller:ShowConversationViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowConversationViewController.storyboardID) as! ShowConversationViewController
                    controller.sessionType = "session"
                    controller.navigationImage = self.MysessionData[indexPath.section][indexPath.row].profilePicture ?? ""
                    controller.navigationName = self.MysessionData[indexPath.section][indexPath.row].nickName ?? ""
                    controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                   controller.userName = self.MysessionData[indexPath.section][indexPath.row].nickName ?? ""
                    controller.isFormAdvisor = false
                    controller.AdvisorImage = self.MysessionData[indexPath.section][indexPath.row].profilePicture
                        ?? ""
                    controller.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(controller, animated: true)
                 
                }
                cell.btnReadMessage = {
                    let controller : MessageViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MessageViewController.storyboardID) as! MessageViewController
                    
                    let dataForShare : [String:Any] = ["advisor_profile_picture":self.MysessionData[indexPath.section][indexPath.row].profilePicture ?? "","customer_profile_picture":SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture ?? "","type":self.MysessionData[indexPath.section][indexPath.row].type ?? "", "advisor_id":Int(self.MysessionData[indexPath.section][indexPath.row].advisor_id ?? "") ?? 0,"booking_id":Int(self.MysessionData[indexPath.section][indexPath.row].bookingId ?? "") ?? 0]
                    
                    controller.sessionType = "free"
                    controller.RequestAcceptDataOfUser = dataForShare
                    controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                    controller.hidesBottomBarWhenPushed = true
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                if MysessionData[indexPath.section][indexPath.row].rating_availible == "1" {
//                    cell.giveRattingsButton.isHidden = true
                    cell.giveRattingsButton.isEnabled = true
                    cell.giveRattingsButton.alpha = 1.0
                } else {
//                    cell.giveRattingsButton.isHidden = false
                    cell.giveRattingsButton.isEnabled = false
                    cell.giveRattingsButton.alpha = 0.5
                }
                
                cell.btnGiveRattingReviews = {
                    
                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: RateAdviserViewController.storyboardID) as! RateAdviserViewController
                    //self.customTabBarController?.hideTabBar()
                    controller.isDismiss = {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerMySessionReload"), object: nil, userInfo: nil)
                        //self.customTabBarController?.showTabBar()
                       // self.ReloadAllLoadedSessionData()
                    }
                    controller.advisorID = self.MysessionData[indexPath.section][indexPath.row].advisor_id ?? ""
                    controller.advisorProfileImaage = self.MysessionData[indexPath.section][indexPath.row].profilePicture ?? ""
                    controller.advisorName = self.MysessionData[indexPath.section][indexPath.row].nickName ?? ""
                    controller.TotalMinutes = self.MysessionData[indexPath.section][indexPath.row].total_minutes ?? ""

                    
                    controller.bookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                    //self.customTabBarController?.hideTabBar()
                    
                    let navigationController = UINavigationController(rootViewController: controller)
                    navigationController.modalPresentationStyle = .overCurrentContext
                    navigationController.modalTransitionStyle = .crossDissolve
                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                }
                cell.btnDeleteSession = {
                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: DeleteSessionViewController.storyboardID) as! DeleteSessionViewController
                    controller.BookingID = self.MysessionData[indexPath.section][indexPath.row].bookingId ?? ""
                  //  self.customTabBarController?.hideTabBar()
                    controller.isDismiss = {
                       // self.customTabBarController?.showTabBar()
                    }
                    controller.isCancle = {
                      //  self.customTabBarController?.showTabBar()
                    }
                    controller.isDelete = {
                        let alert = UIAlertController(title: AppInfo.appName , message: "Session Deleted Successfully", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.ReloadAllLoadedSessionData()
                        }))
                        self.present(alert, animated: true, completion: nil)
                       // self.customTabBarController?.showTabBar()
                      
                       
                    }
                    let navigationController = UINavigationController(rootViewController: controller)
                    navigationController.modalPresentationStyle = .overCurrentContext
                    navigationController.modalTransitionStyle = .crossDissolve
                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                }
                
                return cell
            }else {
                let NoDatacell = tblMySession.dequeueReusableCell(withIdentifier: "NoDataTableViewCell", for: indexPath) as! NoDataTableViewCell
                
                NoDatacell.imgNoData.image = UIImage(named: "ic_noSessionFound")
                NoDatacell.lblNoDataTitle.text = "No Session found"
            
                return NoDatacell
            }
            
        }
       
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "dd MMM YYYY"
        return  dateFormatter.string(from: date)
    }
    func ConvertToTime(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:s"
        let date = dateFormatter.date(from: date) ?? Date()
        dateFormatter.dateFormat = "HH:mm a"
        return  dateFormatter.string(from: date)
    }
   
}

//MARK: - API Calls
extension MySessionViewController {
    func LoadMoreData() {
        let MySessionModel = CustomerMySessionReqModel()
        MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        MySessionModel.page = "\(totalPagesLoad + 1)"
        webserviceForLoadMoreData(reqModel: MySessionModel)
    }
    func webserviceForLoadMoreData(reqModel:CustomerMySessionReqModel) {
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                
                
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                if MySessionResModel.sessionList.count != 0 {
                    self.totalPagesLoad  = self.totalPagesLoad + 1
                }
                let MySessionListDummy = self.MainSessionListArray
                self.MysessionData = []
                var datesArray = self.MainSessionListArray.compactMap({self.convertDateFormater($0.bookingDate)})
                datesArray = datesArray.uniqued()
                var dic = [[SessionList]]() // Your required result
                
                datesArray.forEach { (element) in
                    
                    let NewDictonary =  MySessionListDummy.filter({self.convertDateFormater($0.bookingDate) == element})
                    
                    
                    dic.append(NewDictonary)
                }
                self.MysessionData = dic
                self.tblMySession.reloadData()
            } else {
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
                // Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
    }
}

extension MySessionViewController {
    func ReloadAllLoadedSessionData() {
        
        self.MainSessionListArray = []
        
        ReloadSessionData(pageNumber: "1")
        
    }
    func ReloadSessionData(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            Utilities.hideHud()
            self.FirstTimeLoad = false
            //self.totalPagesLoad = self.totalPagesLoad + 1
            
            
            let MySessionListDummy = self.MainSessionListArray
            self.MysessionData = []
            var datesArray = self.MainSessionListArray.compactMap({convertDateFormater($0.bookingDate)})
            datesArray = datesArray.uniqued()
            var dic = [[SessionList]]() // Your required result
            
            datesArray.forEach { (element) in
                
                let NewDictonary =  MySessionListDummy.filter({convertDateFormater($0.bookingDate) == element})
                
                
                dic.append(NewDictonary)
            }
            self.MysessionData = dic
            print(dic)
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
            Utilities.hideHud()
            self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
        } else {
            if pageNumber == "1" {
                self.MainSessionListArray = []
                
            }
            let MySessionModel = CustomerMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionData(reqModel: MySessionModel)
        }
    }
    func webserviceForReloadSessionData(reqModel:CustomerMySessionReqModel) {
        if self.FirstTimeLoad == false {
            Utilities.showHud()
        }
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
            
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                
                
                
                
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                if MySessionResModel.sessionList.count == 0 {
                    Utilities.hideHud()
                }
            } else {
                self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
                self.refreshControl.endRefreshing()
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
}
extension MySessionViewController {
    func PullToRefreshGetData() {
        
        
        ReloadSessionDataByPullToRefresh(pageNumber: "1")
        
    }
    func ReloadSessionDataByPullToRefresh(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            
            self.FirstTimeLoad = false
            
            self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
            self.refreshControl.endRefreshing()
            let MySessionListDummy = self.MainSessionListArray
            self.MysessionData = []
            var datesArray = self.MainSessionListArray.compactMap({convertDateFormater($0.bookingDate)})
            datesArray = datesArray.uniqued()
            // let datesArray = self.MainSessionListArray.compactMap { $0.bookingDate} // return array of date
            var dic = [[SessionList]]() // Your required result
            
            datesArray.forEach { (element) in
                
                let NewDictonary =  MySessionListDummy.filter({convertDateFormater($0.bookingDate) == element})
                
                
                dic.append(NewDictonary)
            }
            self.MysessionData = dic
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
            
        } else {
            
            if pageNumber == "1" {
                self.MainSessionListArray = []
                
            }
            let MySessionModel = CustomerMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionDataByPullToRefresh(reqModel: MySessionModel)
        }
    }
    func webserviceForReloadSessionDataByPullToRefresh(reqModel:CustomerMySessionReqModel) {
        
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
            
            
            if status {
                
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                self.MainSessionListArray.append(contentsOf: MySessionResModel.sessionList)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                
                
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                
            } else {
                self.tblMySession.switchRefreshHeader(to: .normal(.none, 0.5));
                self.refreshControl.endRefreshing()
                if self.MysessionData.count == 0 {
                    
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
    
}
/*
extension MySessionViewController {
    func getSessionData() {
        MysessionData = []
        print(#function)
        let MySessionModel = CustomerMySessionReqModel()
        MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        MySessionModel.page = "\(totalPagesLoad)"
        webserviceForMySession(reqModel: MySessionModel)
    }
    func webserviceForMySession(reqModel:CustomerMySessionReqModel) {
        
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            self.FirstTimeLoad = false
            if status {
    
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                let sessionListFromJSON:[SessionList] = MySessionResModel.sessionList
                self.MysessionData.append(contentsOf: sessionListFromJSON)
      
                DispatchQueue.main.async {
                    self.tblMySession.reloadData()
                    print("after append total count is \(self.MysessionData.count)")
                }
            } else {
              Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
            }
        })
        
    }
}
extension MySessionViewController {
    func LoadMoreData() {
        print(#function)
            let MySessionModel = CustomerMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
            MySessionModel.page = "\(totalPagesLoad + 1)"
            webserviceForLoadMoreData(reqModel: MySessionModel)
    }
    func webserviceForLoadMoreData(reqModel:CustomerMySessionReqModel) {
       
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
          
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
                
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                let sessionListFromJSON:[SessionList] = MySessionResModel.sessionList
                self.MysessionData.append(contentsOf: sessionListFromJSON)
                if MySessionResModel.sessionList.count != 0 {
                    self.totalPagesLoad  = self.totalPagesLoad + 1
                }
                self.tblMySession.reloadData()
               
            } else {
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                   
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
    
}
extension MySessionViewController {
    func ReloadAllLoadedSessionData() {
        self.MysessionData = []
        ReloadSessionData(pageNumber: "1")
    }
    func ReloadSessionData(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            Utilities.hideHud()
           
            self.FirstTimeLoad = false
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
            Utilities.hideHud()
        } else {
            let MySessionModel = CustomerMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionData(reqModel: MySessionModel)
        }
        
        
    }
    func webserviceForReloadSessionData(reqModel:CustomerMySessionReqModel) {
        if self.FirstTimeLoad == false {
            Utilities.showHud()
        }
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
           
            self.tblMySession.tableFooterView?.isHidden = true
            self.isLoadingList = false
            if status {
              
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                let sessionListFromJSON:[SessionList] = MySessionResModel.sessionList
                self.MysessionData.append(contentsOf: sessionListFromJSON)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                if sessionListFromJSON.count == 0 {
                    Utilities.hideHud()
                }
            } else {
                Utilities.hideHud()
                if self.MysessionData.count == 0 {
                    
                   
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
    
}
extension MySessionViewController {
    func PullToRefreshGetData() {
     
        ReloadSessionDataByPullToRefresh(pageNumber: "1")
    }
    func ReloadSessionDataByPullToRefresh(pageNumber:String) {
        print(#function)
        if Int(pageNumber) == self.totalPagesLoad + 1 {
            
           
            self.FirstTimeLoad = false
            let MySessionListDummy = self.MainSessionListArray
            self.MysessionData = []
            var datesArray = self.MainSessionListArray.compactMap({self.convertDateFormater($0.bookingDate)})
            datesArray = datesArray.uniqued()
            var dic = [[SessionListForAdvisor]]() // Your required result
            
            datesArray.forEach { (element) in
                
                let NewDictonary =  MySessionListDummy.filter({self.convertDateFormater($0.bookingDate) == element})
                
                
                dic.append(NewDictonary)
            }
            self.MysessionData = dic
            self.tblMySession.reloadData()
            print("after append total count is \(self.MysessionData.count)")
           
        } else {
            if pageNumber == "1" {
                self.MysessionData = []
            }
            let MySessionModel = CustomerMySessionReqModel()
            MySessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
            MySessionModel.page = "\(pageNumber)"
            webserviceForReloadSessionDataByPullToRefresh(reqModel: MySessionModel)
        }
        
        
    }
    func webserviceForReloadSessionDataByPullToRefresh(reqModel:CustomerMySessionReqModel) {
       
        print(#function)
        WebServiceSubClass.CustomerMySession(addCategory: reqModel, completion: {(json, status, response) in
           
            self.tblMySession.tableFooterView?.isHidden = true
           
            if status {
              
                let MySessionResModel = MySessionCustomerResModel.init(fromJson: json)
                let sessionListFromJSON:[SessionList] = MySessionResModel.sessionList
                self.MysessionData.append(contentsOf: sessionListFromJSON)
                let pageNumberLoaded = Int(reqModel.page) ?? 0
                print(pageNumberLoaded)
                self.ReloadSessionData(pageNumber: "\(pageNumberLoaded + 1)")
                
            } else {
               
                if self.MysessionData.count == 0 {
                    
                   
                    self.FirstTimeLoad = false
                    self.tblMySession.reloadData()
                }
            }
        })
    }
    
}
 */
extension MySessionViewController {
    
    func GetSessionSummaryDetails(bookingID:Int) {
        print(#function)
        let GetSummaryReqModel = GetSessionSummary()
        GetSummaryReqModel.booking_id = bookingID
        GetSummaryReqModel.type = "customer"
        webserviceForGetSessionSummatyDetails(reqModel: GetSummaryReqModel)
        
        
    }
    func webserviceForGetSessionSummatyDetails(reqModel:GetSessionSummary) {
        Utilities.showHud()
        print(#function)
        WebServiceSubClass.SessionSummary(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if status {
    
                let SummaryModel = ShowSummaryResModel.init(fromJson: json)
                let dataForShare:[String:String] = ["total_minute":SummaryModel.data.totalMinute ?? "","free_minute":SummaryModel.data.freeMinute ?? "","booking_amount":SummaryModel.data.bookingAmount ?? "","total_chargeable_amount":SummaryModel.data.totalChargeableAmount ?? "","discount_type":SummaryModel.data.discountType ?? "","discount_value":SummaryModel.data.discountValue ?? "","category_name":SummaryModel.data.category_name ?? ""]
                
                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                controller.isFromCustomer = true
                controller.dataForTable = dataForShare
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                
            } else {
                
                
            }
        })
    }
    
}

//MARK: - MySession TableView Cell
class mySessionCell : UITableViewCell {
    
    @IBOutlet weak var btnPostChat: UIButton!
    @IBOutlet weak var lblSessionTime: MySessionLabel!
    @IBOutlet weak var skeletonImageView: UIImageView!
    
    @IBOutlet weak var mySessionAdvisorData: MySessionPageView!
    @IBOutlet weak var mySessionGiveRattingView: viewViewClearBG!
    
    @IBOutlet weak var giveRattingsButton: MySessionButton!
    
    var btnChat : (() -> ())?
    var btnReadMessage : (() -> ())?
    var btnGiveRattingReviews : (() -> ())?
    var btnDeleteSession : (() -> ())?
    
    @IBOutlet weak var communicationType: MySessionButton!
    @IBOutlet weak var advisorImageView: UIImageView!
    
    @IBOutlet weak var TotalMoney: MySessionLabel!
    @IBOutlet weak var TotalTime: MySessionLabel!
    @IBOutlet weak var advisorName: MySessionLabel!
    @IBAction func btnReadMessageClick(_ sender: Any) {
        if let click = self.btnReadMessage {
            click()
        }
        
    }
    
    @IBAction func btnChatClick(_ sender: Any) {
        if let click = self.btnChat {
            click()
        }
    }
    
    @IBAction func btnGiveRattingClick(_ sender: Any) {
        if let click = self.btnGiveRattingReviews {
            click()
        }
    }
    
    @IBAction func btnDeleteSessionClick(_ sender: Any) {
        if let click = self.btnDeleteSession {
            click()
        }
    }
}
