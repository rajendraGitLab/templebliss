//
//  MyProfileViewController.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import SDWebImage
import SkeletonView
class MyProfileViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource ,EditProfileDelegate{
    //MARK: - Properties
    var loginModelDetails : Profile?
    var myProfileData = [myProfile]()
    var customTabBarController : CustomTabBarVC?
    //MARK: - IBOutlets
    @IBOutlet weak var lblUserName: MyProfileLabel!
    @IBOutlet weak var lblUserEmail: MyProfileLabel!
    @IBOutlet weak var imgProfile: UIImageView!{ didSet{ imgProfile.layer.cornerRadius = imgProfile.frame.size.height / 2}}
    
    @IBOutlet weak var lblUserContactNo: MyProfileLabel!
    @IBOutlet weak var tblMyProfile: UITableView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        
        setLocalization()
       
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "My Profile", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
      
       
       
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
       
        customTabBarController?.showTabBar()
        setValue()
        self.view.layoutIfNeeded()
    }
    override func viewWillAppear(_ animated: Bool) {
        if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
//            SingletonClass.sharedInstance.selectedCategoryIndex = -1
//            SingletonClass.sharedInstance.category_id = ""
//            SingletonClass.sharedInstance.descriptionForSelectedCategories = ""
//            SingletonClass.sharedInstance.selectedTypeAdvisor = ""
            appDel.navigateToUserLogin()
            userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
        }
        
        myProfileData.append(myProfile(img: myProfileCategory.ViewFavourites.imageValue, name: myProfileCategory.ViewFavourites.value))
        myProfileData.append(myProfile(img: myProfileCategory.MyCredit.imageValue, name: myProfileCategory.MyCredit.value))
        
        if SingletonClass.sharedInstance.loginForCustomer?.profile.socialType == "0" || SingletonClass.sharedInstance.loginForCustomer?.profile.socialType == ""  {
            myProfileData.append(myProfile(img: myProfileCategory.ChangePassword.imageValue, name: myProfileCategory.ChangePassword.value))
        }
        
        
        myProfileData.append(myProfile(img: myProfileCategory.settings.imageValue, name: myProfileCategory.settings.value))
        
//        setValue()
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
       // imgProfile.isSkeletonable = true
      //  imgProfile.image = nil
       // imgProfile.showAnimatedSkeleton()
        //let animation = SkeletonAnimationBuilder().makeSlidingAnimation(withDirection: .leftRight)
       // imgProfile.showAnimatedSkeleton()
       
       // imgProfile.showAnimatedGradientSkeleton(usingGradient: SkeletonGradient(baseColor: colors.white.value.withAlphaComponent(0.25)), animation: animation)
        
        lblUserName.text = SingletonClass.sharedInstance.loginForCustomer?.profile.fullName
        lblUserEmail.text = SingletonClass.sharedInstance.loginForCustomer?.profile.email
        lblUserContactNo.text = SingletonClass.sharedInstance.loginForCustomer?.profile.phone
        

        let imgURL:String = "\(SingletonClass.sharedInstance.loginForCustomer?.profile.profilePicture ?? "")"
        if imgURL != "" {
            let strURl = URL(string: "\(APIEnvironment.profileBu)\(imgURL)")
          //  imgProfile.sd_imageTransition = .fade
            //imgProfile.sd_se

            imgProfile.sd_imageIndicator = SDWebImageActivityIndicator.white
//            DispatchQueue.main.async {
                self.imgProfile.sd_setImage(with: strURl,  placeholderImage: UIImage(named: ""))
       //     }
            
//            imgProfile.sd_setImage(with: strURl, completed: {_,_,_,_ in
//                //DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//                //self.imgProfile.stopSkeletonAnimation()
//                    self.imgProfile.hideSkeleton()
//                //})
//
//            })
        } else {
            imgProfile.image = UIImage(named: "user_dummy_profile")
        }
            
    }
    
    //MARK: - Table View Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myProfileData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMyProfile.dequeueReusableCell(withIdentifier: myProfileCell.reuseIdentifier, for: indexPath) as! myProfileCell
        cell.categoryImage.image = myProfileData[indexPath.row].profileCategoryImage
        cell.categoryName.text = myProfileData[indexPath.row].profileCategoryName
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch  myProfileData[indexPath.row].profileCategoryName{
        case myProfileCategory.ViewFavourites.value:
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MyFavouriteViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            
            break
        case myProfileCategory.MyCredit.value:
          //
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: MyCreditViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            print(myProfileCategory.MyCredit.value)
            break
        case myProfileCategory.ChangePassword.value:
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ChangePasswordViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            break
        case myProfileCategory.settings.value:
            let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: settingsViewController.storyboardID)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
            break
        default:
            break
        }
    }
    //MARK: - IBActions
    
    @IBAction func btnEditProfileCLick(_ sender: Any) {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: EditProfileViewController.storyboardID)as! EditProfileViewController
        controller.delegateEdit = self
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnLogoutClick(_ sender: theamSubmitButton) {
        showLogout()
    }
    //MARK: - API Calls
    func showLogout() {
        
        let alertController = UIAlertController(title: AppName,
                                                message: GlobalStrings.Alert_logout.rawValue,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertController.addAction(UIAlertAction(title: "Yes", style: .default){ _ in
            SingletonClass.sharedInstance.StopCustomerTimer()
            appDel.performLogout()
        })
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true)
        }
    }
    func refereshProfileScreen() {
        setValue()
    }
}
class myProfileCell : UITableViewCell {
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: MyProfileLabel!
    
}



class myProfile {
    var profileCategoryImage : UIImage?
    var profileCategoryName : String?
    init(img:UIImage,name:String) {
        self.profileCategoryImage = img
        self.profileCategoryName = name
    }
}
enum myProfileCategory {
    case ViewFavourites, MyCredit, ChangePassword , settings
    
    var value:String{
        switch self {
        case .ViewFavourites:
            return "View Favorites"
        case .MyCredit:
            return "My Wallet"
        case .ChangePassword:
            return "Change Password"
        case .settings:
            return "Settings"
        
        }
    }
    var imageValue : UIImage {
        switch self {
        case .ViewFavourites:
            return UIImage(named: "ic_viewFavourites")!
        case .MyCredit:
            return UIImage(named: "ic_myCredit")!
        case .ChangePassword:
            return UIImage(named: "ic_changePassword")!
        case .settings:
            return UIImage(named: "ic_Settings")!
    }
    }
}
