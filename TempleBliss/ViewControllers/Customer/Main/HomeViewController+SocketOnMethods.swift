//
//  HomeViewController+SocketOnMethods.swift
//  TempleBliss
//
//  Created by Apple on 07/07/21.
//  Copyright © 2021 EWW071. All rights reserved.
//


import Foundation
import UIKit
import SDWebImage
import PullToRefreshKit
import SkeletonView
import SwiftyJSON
 //MARK: - ------------- AllSocketOnMethods -------------
extension HomeViewController {
    func CallOnAllSocketToListen() {
        print("ATDebug :: Home \(#function)")
        AdvisorInactive()
        RejectRequest()
        AcceptRequest()
        AdvisorInactiveOnBooking()
        buy_more_minute()
        advisor_busy()
        OnForVideoCallCustomer()
        OnEndSession()
        voice_call_customer()
        RequestRejectDisconnect()
        connect_customer_interval()
        
    }
    
     //MARK: - ------------- connect_customer_interval -------------
    func connect_customer_interval() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.connect_customer_interval.rawValue) { (response) in
            
//            print("ATDebug :: Home \(#function)")
//            print(#function)
//            print(response)
            
        }
    }
     //MARK: - ------------- AdvisorInactive -------------
    func AdvisorInactive() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.advisor_inactive.rawValue) { (response) in
            
            print("ATDebug :: Home \(#function)")
            print(#function)
            print(response)
           
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdvisorsViewController.self) {
                    let controller : AdvisorsViewController = topVC as! AdvisorsViewController
//                    controller.GetAdvisorsDataWithPaginations()
                    controller.latestReload()
//                    controller.getAdviserData(searchString: controller.textfieldSearchAdviser.text ?? "")
//                    controller.totalPagesLoad = 1
//                    controller.GetAdvisorsDataWithPaginations()
                }
            }
        }
    }
     //MARK: - ------------- AdvisorInactiveOnBooking -------------
    func AdvisorInactiveOnBooking() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.booking_request.rawValue) { (response) in
            
            print("ATDebug :: Home \(#function)")
            print(#function)
            print(response)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
                
                if let topVC = UIApplication.topViewController() {
                    if topVC.isKind(of: CustomerVideoCallViewController.self) || topVC.isKind(of: AudioCallViewController.self) {
                        if topVC.isModal {
                            topVC.dismiss(animated: true, completion: {
                                if let arrResponse = response.array {
                                    if let singleResponse = arrResponse.first {
                                        Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                    }
                                }
                            })
                        } else {
                            topVC.navigationController?.popViewController(animated: true)
                            DispatchQueue.main.async {
                                if let arrResponse = response.array {
                                    if let singleResponse = arrResponse.first {
                                        Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                    }
                                }
                            }
                        }
                    } else {
                        if topVC.isModal {
                            topVC.dismiss(animated: true, completion: {
                                            if let arrResponse = response.array {
                                                if let singleResponse = arrResponse.first {
                                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                                }
                                            }})
                        } else {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                }
                            }
                        }
                    }
                    
                }
            })
        }
    }
     //MARK: - -------------  RequestRejectDisconnect() -------------
    
    func RequestRejectDisconnect() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.request_reject_disconnect.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Advisor Home \(#function)")
           
            if let topVC = UIApplication.topViewController() {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                            }
                        }
                    })
                } else if topVC.isKind(of: CustomerVideoCallViewController.self) {
                    topVC.navigationController?.popViewController(animated: true)
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                        }
                    }
                } else if topVC.isKind(of: AudioCallViewController.self) {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        topVC.navigationController?.popViewController(animated: true)
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                            }
                        }
                    })
                }
            }
        }
    }
     //MARK: - ------------- RejectRequest -------------
    func RejectRequest() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.request_reject.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if let topVC = UIApplication.topViewController() {
                        if topVC.isKind(of: MessageViewController.self) || topVC.isKind(of: CustomerVideoCallViewController.self) || topVC.isKind(of: AudioCallViewController.self) {
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                })
                            }
                        } else {
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                                    Utilities.ShowAlert(OfMessage: singleResponse["message"].string ?? "")
                                })
                            }
                        }
                    }
                }
            }
        }
    }
            

     //MARK: - ------------- buy_more_minute -------------
    func buy_more_minute() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.buy_more_minute.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
        }
    }
     //MARK: - ------------- voice_call_customer -------------
    func voice_call_customer() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.voiceCallCustomer.rawValue) { (response) in

            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
        }
    }
     //MARK: - ------------- advisor_busy -------------
    func advisor_busy() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.advisor_buzy.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: AdvisorsViewController.self) {
                    let controller : AdvisorsViewController = topVC as! AdvisorsViewController
//                    controller.GetAdvisorsDataWithPaginations()
                    controller.latestReload()
//                                        controller.getAdviserData(searchString: controller.textfieldSearchAdviser.text ?? "")
//                    controller.totalPagesLoad = 1
//                    controller.GetAdvisorsDataWithPaginations()
                }
            }
        }
    }
    
   
     //MARK: - ------------- AcceptRequest -------------
   
    func AcceptRequest() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.request_accept.rawValue) { (response) in
            
            print(#function)
            print("Home Screen",response)
            print("ATDebug :: Home \(#function)")
            self.MiniutesForVideo = ""
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
                if let topVC = UIApplication.topViewController() {
                    if topVC.isModal {
                        topVC.dismiss(animated: true, completion: {
                            if let arrResponse = response.array {
                                if let singleResponse = arrResponse.first {
                                    print("NEW" + #function)
                                    if String(singleResponse["type"].string ?? "") == "chat" {
                                        
                                        if String(singleResponse["status"].string ?? "") == "accepted" {
                                            
                                            SingletonClass.sharedInstance.FreeMinutes = "0"
                                            if let topController = UIApplication.topViewController() {
                                              
                                                if topController.isKind(of: AdviserDetailsViewController.self) {
                                                    
                                                    let topVC = topController as? AdviserDetailsViewController
                                                    print("NEW" + #function)
                                                    let DataForShare : [String:Any] = ["advisor_id":singleResponse["advisor_id"].int ?? 0,"type":singleResponse["type"].string ?? "","start_time":singleResponse["start_time"].string ?? "","customer_profile_picture":singleResponse["customer_profile_picture"].string ?? "","message":singleResponse["message"],"default_message":singleResponse["default_message"].string ?? "","advisor_profile_picture":singleResponse["advisor_profile_picture"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"minute":singleResponse["minute"].string ?? "","advisor_name":singleResponse["advisor_name"].string ?? ""]
                                                    topVC?.RequestAcceptData = DataForShare
                                                    topVC?.goToChatScreen()
                                                }
                                            }
                                        } else if String(singleResponse["status"].string ?? "") == "rejected" {
                                            
                                        }
                                        
                                        
                                    } else if String(singleResponse["type"].string ?? "") == "video" {

                                        if let topController = UIApplication.topViewController() {
                                            if topController.isKind(of: CustomerVideoCallViewController.self) {
                                                let vc = topController as! CustomerVideoCallViewController
                                                
                                                
                                                
                                                let senderID = singleResponse["user_id"].int ?? 0
                                                _ = singleResponse["message"].string ?? ""
                                                let roomName = singleResponse["room"].string ?? ""
                                                let senderName = singleResponse["name"].string ?? ""
                                                let bookingID = "\(singleResponse["booking_id"].int ?? 0)"
                                                print("NEW ::::: \(singleResponse["token"].string ?? "")")
                                                let token = singleResponse["sender_token"].string ?? ""
                                                let reciverID = singleResponse["receiver_id"].int ?? 0
                                                let receiver_name = singleResponse["receiver_name"].string ?? ""
                                                
                                                
                                              
                                                vc.strRoomName = roomName
                                                //               vc.visit_id = visit_id
                                                vc.token = token
                                                vc.visitID = bookingID
                                              
                                                vc.totalMinutes = singleResponse["minute"].string ?? ""
                                                
                                                print("Here come with data 2")
                                                
                                                // topController.navigationController?.pushViewController(vc,
                                                
                                                
                                                
                                                
                                            }
                                        }
                                        
                                        
                                       
//                                        let joinAdvisor = ["customer_id": Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,"customer_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","advisor_id":singleResponse["advisor_id"].int ?? 0,"advisor_name":singleResponse["advisor_name"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0] as [String : Any]
//
//                                        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCall.rawValue , with: joinAdvisor)
//
//                                        print("video call accept")
                                        
                                    } else if String(singleResponse["type"].string ?? "") == "audio" {
                                        print("NEW" + #function)
//                                        sleep(2)
                                        if let topController = UIApplication.topViewController() {
                                            if topController.isKind(of: AudioCallViewController.self) {
                                                let AudioVC = topController as! AudioCallViewController
                                                AudioVC.totalMinutes = singleResponse["minute"].string ?? ""
                                                AudioVC.bookingID = "\(singleResponse["booking_id"].int ?? 0)"
                                                AudioVC.CallToAdvisorWithBookingID()
                                            }
                                        }
                                        
                                        
                                        
//
//                                        if let topController = UIApplication.topViewController() {
//                                            let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
//
//                                            controller.CallToAdvisorName = "\((singleResponse["advisor_name"].string ?? "").replacingOccurrences(of: " ", with: "_"))_\(singleResponse["advisor_id"].int ?? 0)"
//                                            controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"
//
//                                            controller.advisorName = singleResponse["advisor_name"].string ?? ""
//                                            controller.advisorUserImageURl = singleResponse["advisor_profile_picture"].string ?? ""
//
//                                            controller.bookingID = "\(singleResponse["booking_id"].int ?? 0)"
//                                            controller.AdvisorID = "\(singleResponse["advisor_id"].int ?? 0)"
//                                            controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
//                                            controller.totalMinutes = singleResponse["minute"].string ?? ""
//                                            topController.navigationController?.pushViewController(controller, animated: true)
//                                        }
                                        
                                    }
                                    
                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                if String(singleResponse["type"].string ?? "") == "chat" {
                                    
                                    if String(singleResponse["status"].string ?? "") == "accepted" {
                                        
                                        SingletonClass.sharedInstance.FreeMinutes = "0"
                                        if let topController = UIApplication.topViewController() {
                                            if topController.isKind(of: AdviserDetailsViewController.self) {
                                                
                                                let topVC = topController as? AdviserDetailsViewController
                                                
                                                let DataForShare : [String:Any] = ["advisor_id":singleResponse["advisor_id"].int ?? 0,"type":singleResponse["type"].string ?? "","start_time":singleResponse["start_time"].string ?? "","customer_profile_picture":singleResponse["customer_profile_picture"].string ?? "","message":singleResponse["message"],"default_message":singleResponse["default_message"].string ?? "","advisor_profile_picture":singleResponse["advisor_profile_picture"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0,"minute":singleResponse["minute"].string ?? "","advisor_name":singleResponse["advisor_name"].string ?? ""]
                                                topVC?.RequestAcceptData = DataForShare
                                                topVC?.goToChatScreen()
                                            }
                                        }
                                        
                                    } else if String(singleResponse["status"].string ?? "") == "rejected" {
                                        
                                    }
                                    
                                    
                                } else if String(singleResponse["type"].string ?? "") == "video" {
                                    print("NEW" + #function)
                                    if let topController = UIApplication.topViewController() {
                                        if topController.isKind(of: CustomerVideoCallViewController.self) {
                                            let vc = topController as! CustomerVideoCallViewController
                                            
                                            
                                            let senderID = singleResponse["user_id"].int ?? 0
                                            _ = singleResponse["message"].string ?? ""
                                            let roomName = singleResponse["room"].string ?? ""
                                            let senderName = singleResponse["name"].string ?? ""
                                            let bookingID = "\(singleResponse["booking_id"].int ?? 0)"
                                            let token = singleResponse["sender_token"].string ?? ""
                                            let reciverID = singleResponse["receiver_id"].int ?? 0
                                            let receiver_name = singleResponse["receiver_name"].string ?? ""
                                            
                                            vc.strRoomName = roomName
                                            //               vc.visit_id = visit_id
                                            vc.token = token
                                            vc.visitID = bookingID
                                          
                                            vc.totalMinutes = singleResponse["minute"].string ?? ""
                                            
                                            print("Here come with data 2")
                                            
                                            // topController.navigationController?.pushViewController(vc,
                                            
                                            
                                            
                                            
                                        }
                                    }
                                    
                                    
                                    
//                                    self.MiniutesForVideo = singleResponse["minute"].string ?? ""
//                                    let joinAdvisor = ["customer_id": Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,"customer_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","advisor_id":singleResponse["advisor_id"].int ?? 0,"advisor_name":singleResponse["advisor_name"].string ?? "","booking_id":singleResponse["booking_id"].int ?? 0] as [String : Any]
//
//                                    SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCall.rawValue , with: joinAdvisor)
//
//                                    print("video call accept")
                                    
                                } else if String(singleResponse["type"].string ?? "") == "audio" {
                                    print("NEW" + #function)
                                    if let topController = UIApplication.topViewController() {
                                        if topController.isKind(of: AudioCallViewController.self) {
                                            let AudioVC = topController as! AudioCallViewController
                                            AudioVC.totalMinutes = singleResponse["minute"].string ?? ""
                                            AudioVC.bookingID = "\(singleResponse["booking_id"].int ?? 0)"
                                            AudioVC.CallToAdvisorWithBookingID()
                                        }
                                    }
//                                    if let topController = UIApplication.topViewController() {
//                                        let controller:AudioCallViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: AudioCallViewController.storyboardID) as! AudioCallViewController
//
//                                        controller.CallToAdvisorName = "\((singleResponse["advisor_name"].string ?? "").replacingOccurrences(of: " ", with: "_"))_\(singleResponse["advisor_id"].int ?? 0)"
//                                        controller.myIdentity = "\((SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "").replacingOccurrences(of: " ", with: "_"))_\(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "")"
//
//                                        controller.advisorName = singleResponse["advisor_name"].string ?? ""
//                                        controller.advisorUserImageURl = singleResponse["advisor_profile_picture"].string ?? ""
//
//                                        controller.bookingID = "\(singleResponse["booking_id"].int ?? 0)"
//                                        controller.AdvisorID = "\(singleResponse["advisor_id"].int ?? 0)"
//                                        controller.customerID = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
//                                        controller.totalMinutes = singleResponse["minute"].string ?? ""
//                                        topController.navigationController?.pushViewController(controller, animated: true)
//                                    }
                                }
                            }
                        }
                    }
                }
            })

        }
    }
     //MARK: - ------------- OnEndSession -------------
    func goBackFromMessageOrVideoScreen(ArrayOfJson:JSON) {
        let singleResponse = ArrayOfJson
        if let topVC = UIApplication.topViewController() {

            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {

                    SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = singleResponse["wallet_amount"].string ?? ""
                    userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)

                    let dataForShare:[String:String] = ["total_minute":singleResponse["total_minute"].string ?? "","free_minute":singleResponse["free_minute"].string ?? "","booking_amount":singleResponse["booking_amount"].string ?? "","total_chargeable_amount":singleResponse["total_chargeable_amount"].string ?? "","discount_type":singleResponse["discount_type"].string ?? "","discount_value":singleResponse["discount_value"].string ?? "","message":singleResponse["message"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                    let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                        let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                        controller.isFromCustomer = true
                        controller.dataForTable = dataForShare
                        let navigationController = UINavigationController(rootViewController: controller)
                        navigationController.modalPresentationStyle = .overCurrentContext
                        navigationController.modalTransitionStyle = .crossDissolve
                        appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                        
                        controller.didBack = {
                            self.gotoRatingReviewScreen(singleResponse: singleResponse)
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                })

            } else {
                topVC.navigationController?.popViewController(animated: true)
                let dataForShare:[String:String] = ["total_minute":singleResponse["total_minute"].string ?? "","free_minute":singleResponse["free_minute"].string ?? "","booking_amount":singleResponse["booking_amount"].string ?? "","total_chargeable_amount":singleResponse["total_chargeable_amount"].string ?? "","discount_type":singleResponse["discount_type"].string ?? "","discount_value":singleResponse["discount_value"].string ?? "","message":singleResponse["message"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                    let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                    controller.isFromCustomer = true
                    controller.dataForTable = dataForShare
                    
                    controller.didBack = {
                        self.gotoRatingReviewScreen(singleResponse: singleResponse)
                    }
                    
                    let navigationController = UINavigationController(rootViewController: controller)
                    navigationController.modalPresentationStyle = .overCurrentContext
                    navigationController.modalTransitionStyle = .crossDissolve
                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                }))
                self.present(alert, animated: true, completion: nil)

            }
        }
    }
    
    
    func OnEndSession() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.end_session.rawValue) { (response) in

            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
            if let newTopVC = UIApplication.topViewController() {
                if newTopVC.isKind(of: MessageViewController.self) || newTopVC.isKind(of: CustomerVideoCallViewController.self) || newTopVC.isKind(of: AudioCallViewController.self) {
                    if newTopVC.isKind(of: MessageViewController.self) {
                        if let navControllers = newTopVC.navigationController?.children {
                            for controllers in navControllers {
                                if controllers.isKind(of: AdviserDetailsViewController.self) {
                                    let vc = controllers as! AdviserDetailsViewController
                                    vc.stopTimer()
                                    if let arrResponse = response.array {
                                        if let singleResponse = arrResponse.first {
                                            self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                        }
                                    }
                                }
                            }
                        }

                    } else if newTopVC.isKind(of: CustomerVideoCallViewController.self) {
                        let vc = newTopVC as! CustomerVideoCallViewController
                        vc.stopTimer()
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                            }
                        }
                    }
                    else if newTopVC.isKind(of: AudioCallViewController.self) {
                       let VC = newTopVC as! AudioCallViewController
                        VC.isPickedUp = nil
                        VC.stopTimerOnEndSession()
                        VC.navigationController?.popViewController(animated: true)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                
                                SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = singleResponse["wallet_amount"].string ?? ""
                                userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)

                                let dataForShare:[String:String] = ["total_minute":singleResponse["total_minute"].string ?? "","free_minute":singleResponse["free_minute"].string ?? "","booking_amount":singleResponse["booking_amount"].string ?? "","total_chargeable_amount":singleResponse["total_chargeable_amount"].string ?? "","discount_type":singleResponse["discount_type"].string ?? "","discount_value":singleResponse["discount_value"].string ?? "","message":singleResponse["message"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                                    let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                    controller.isFromCustomer = true
                                    controller.dataForTable = dataForShare
                                    controller.didBack = {
                                        self.gotoRatingReviewScreen(singleResponse: singleResponse)
                                    }
                                    let navigationController = UINavigationController(rootViewController: controller)
                                    navigationController.modalPresentationStyle = .overCurrentContext
                                    navigationController.modalTransitionStyle = .crossDissolve
                                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                }))
                               
                                appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                            }
                        }})
                    }
                } else {
                    if newTopVC.isModal {
                        newTopVC.dismiss(animated: true, completion: {
                            if let LatestTopVC = UIApplication.topViewController() {
                                if LatestTopVC.isKind(of: AudioCallViewController.self) {
                                    let VC = LatestTopVC as! AudioCallViewController
                                     VC.isPickedUp = nil
                                    VC.stopTimerOnEndSession()
                                     VC.navigationController?.popViewController(animated: true)
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {                        if let arrResponse = response.array {
                                        if let singleResponse = arrResponse.first {
                                            
                                            SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = singleResponse["wallet_amount"].string ?? ""
                                            userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)

                                            let dataForShare:[String:String] = ["total_minute":singleResponse["total_minute"].string ?? "","free_minute":singleResponse["free_minute"].string ?? "","booking_amount":singleResponse["booking_amount"].string ?? "","total_chargeable_amount":singleResponse["total_chargeable_amount"].string ?? "","discount_type":singleResponse["discount_type"].string ?? "","discount_value":singleResponse["discount_value"].string ?? "","message":singleResponse["message"].string ?? "","category_name":singleResponse["category_name"].string ?? ""]
                                            let alert = UIAlertController(title: AppInfo.appName, message: singleResponse["message"].string ?? "", preferredStyle: .alert)
                                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in

                                                let controller:ShowSummaryViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: ShowSummaryViewController.storyboardID) as! ShowSummaryViewController
                                                controller.isFromCustomer = true
                                                controller.dataForTable = dataForShare
                                                controller.didBack = {
                                                    self.gotoRatingReviewScreen(singleResponse: singleResponse)
                                                }
                                                let navigationController = UINavigationController(rootViewController: controller)
                                                navigationController.modalPresentationStyle = .overCurrentContext
                                                navigationController.modalTransitionStyle = .crossDissolve
                                                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
                                            }))
                                            appDel.window?.rootViewController?.present(alert, animated: true, completion: nil)
                                        }
                                    }})
                                    
                                } else {
                                    if let arrResponse = response.array {
                                        if let singleResponse = arrResponse.first {
                                            self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                                        }
                                    }
                                }
                            }
                        })
                    } else {
                        if let arrResponse = response.array {
                            if let singleResponse = arrResponse.first {
                                self.goBackFromMessageOrVideoScreen(ArrayOfJson: singleResponse)
                            }
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    if let arrResponse = response.array {
                        if let singleResponse = arrResponse.first {
                            SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount = singleResponse["wallet_amount"].string ?? ""
                            userDefault.setUserDataForCustomer(objProfile: SingletonClass.sharedInstance.loginForCustomer!)

                        }
                    }
                })
            }
        }
    }
     //MARK: - ------------- OnForVideoCallCustomer -------------
    func OnForVideoCallCustomer() {
       
        SocketIOManager.shared.socketCall(for: socketApiKeys.videoCallCustomer.rawValue) { (response) in
            
            print(#function)
            print(response)
            print("ATDebug :: Home \(#function)")
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    
                    let senderID = singleResponse["user_id"].int ?? 0
                    _ = singleResponse["message"].string ?? ""
                    let roomName = singleResponse["room"].string ?? ""
                    let senderName = singleResponse["name"].string ?? ""
                    let bookingID = "\(singleResponse["booking_id"].int ?? 0)"
                    let token = singleResponse["token"].string ?? ""
                    let reciverID = singleResponse["receiver_id"].int ?? 0
                    let receiver_name = singleResponse["receiver_name"].string ?? ""
                    
                    
                    
                    //                accessToken = token
                    //                print("AccessToken: \n \(accessToken)")
                    
                    let story = UIStoryboard(name: "Main", bundle: nil)
                    guard let vc = story.instantiateViewController(withIdentifier: "CustomerVideoCallViewController") as? CustomerVideoCallViewController else { return }
                    
                    if let topVC = UIApplication.topViewController() {
                        if topVC.isKind(of: CustomerVideoCallViewController.self) {
                            return
                        }
                    }
                    print("Here come with data")
                    if let topController = UIApplication.topViewController() {
                        vc.CallingTo = receiver_name
                        vc.senderName = senderName
                        vc.strRoomName = roomName
                        //               vc.visit_id = visit_id
                        vc.token = token
                        vc.visitID = bookingID
                        vc.receiverId = "\(reciverID)"
                        vc.senderId = "\(senderID)"
                        vc.totalMinutes = self.MiniutesForVideo
                        vc.navigationController?.isNavigationBarHidden = true
                        topController.navigationController?.pushViewController(vc, animated: true)
                        print("Here come with data 2")
                        
                        // topController.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
            }
        }
    }

}
