//
//  DeleteSessionViewController.swift
//  TempleBliss
//
//  Created by Apple on 25/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class DeleteSessionViewController: BaseViewController {

    //MARK: - Properties
    var isDismiss : (() -> ())?
    var isCancle : (() -> ())?
    var isDelete : (() -> ())?
    
    var BookingID = String()
    //MARK: - IBOutlets
    
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let click = self.isDismiss {
            click()
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //MARK: - IBActions
    
    @IBAction func btnCancle(_ sender: Any) {
        if let click = self.isCancle {
            click()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        let DeleteSessionModel = deleteSessionCustomerReqModel()
        DeleteSessionModel.user_id = SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? ""
        DeleteSessionModel.booking_id = BookingID
        webserviceCallForDeleteMySession(reqModel: DeleteSessionModel)
       
    }
    //MARK: - API Calls
    
    
    
    

}
extension DeleteSessionViewController {
    func webserviceCallForDeleteMySession(reqModel:deleteSessionCustomerReqModel) {
        self.dismiss(animated: true, completion: nil)
        Utilities.showHud()
        WebServiceSubClass.delete_session(addCategory: reqModel, completion: {(json, status, response) in
            Utilities.hideHud()
            if let click = self.isDelete {
                click()
                
            }
            if status {
                
            } else {
               
                Utilities.displayAlert(AppName, message: response as? String ?? "Something went wrong")
                
            }
        })
    }
}
