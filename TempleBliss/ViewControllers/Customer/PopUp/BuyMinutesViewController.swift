//
//  BuyMinutesViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import DropDown

class BuyMinutesViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource, UITextFieldDelegate {
    
    //MARK: -  Properties
    var BoookingID = ""
    
    var DataForBookAgain : [String:Any] = [:]
    var isBuyNow : Bool = false
    var appBorderColor: UIColor?
    
    var closourForBookRequest : (() -> ())?
    var closourForClickOnBack : (() -> ())?
    
    var PriceOfSelectedCategory : String = ""
    var AdvisorID : String = ""
    var CategoryID : String = ""
    var CommunicationType : String = ""
    
    var minutesArray : [String] = []
    var AvailableCredit : String = ""
    let pickerViewForAll = GeneralPickerView()
    var buyMinutesData = [buyMinutesCategory]()
    let dropDown = DropDown()
    
    //MARK: -  IBOutlets
    @IBOutlet weak var tblMinutePriceHeight: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: buyMinutesLabel!
    @IBOutlet weak var textFieldSelectMinutes: BuyMinutesTextField!
    @IBOutlet weak var tblMinutesprice: UITableView!
    @IBOutlet weak var btnPurchase: UIButton!
    @IBOutlet weak var borderView1: UIView!
    @IBOutlet weak var borderView2: UIView!
    @IBOutlet weak var btnDropDown: UIButton!
    @IBOutlet weak var carousel: iCarousel!


    //MARK: -  View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        SingletonClass.sharedInstance.FreeMinutes = "0"
        print(PriceOfSelectedCategory)
        
        setLocalization()
        setValue()
        // self.tblMinutesprice.removeObserver(self, forKeyPath: "contentSize")
        self.tblMinutesprice.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        AvailableCredit = SingletonClass.sharedInstance.loginForCustomer?.profile.walletAmount ?? ""
        //  AvailableCredit = "1"
        textFieldSelectMinutes.delegate = self
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        minutesArray = SingletonClass.sharedInstance.minutesArray
        if minutesArray.count != 0 {
            textFieldSelectMinutes?.text = minutesArray[0]
        }
        carousel.type = .rotary
        //        setupDelegateForPickerView()
        if SingletonClass.sharedInstance.WalletBalanceForMaintain != "" {
            
            let newCredit : Float = Float(String(format: "%.2f", ((SingletonClass.sharedInstance.WalletBalanceForMaintain as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
            print("New credit value for minutes is \(newCredit)")
            let availCredit : Float = Float(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
            print("Available credit value for minutes is \(availCredit)")
            AvailableCredit = "\(availCredit - newCredit)"
//            AvailableCredit = "\(newCredit)"
            print("Difference of credit is \(AvailableCredit)")
        }
         
        buyMinutesData.removeAll()
        
        if textFieldSelectMinutes?.text ?? "" == "1" {
            
            buyMinutesData.append(buyMinutesCategory(name: "Minute Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
        } else {
            buyMinutesData.append(buyMinutesCategory(name: "Minutes Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
        }
        
        buyMinutesData.append(buyMinutesCategory(name: "Price", price: "\(Currency) \( String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00))"))
        
        let TotalPriceInDouble : Float = Float(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let TotalMinutesInDouble : Float = Float(String(format: "%.2f", ((textFieldSelectMinutes?.text as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let AvailableCreditInDouble : Float = Float(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        
        buyMinutesData.append(buyMinutesCategory(name: "Total Price", price: "\(Currency) \( String(format: "%.2f", (("\(TotalPriceInDouble * TotalMinutesInDouble)" as NSString?)?.floatValue) ?? 0.00))"))
        
        
        if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
            buyMinutesData.append(buyMinutesCategory(name: "Free Minutes", price: "\(SingletonClass.sharedInstance.FreeMinutes)"))
            let TotalMinutes = (Int(textFieldSelectMinutes?.text ?? "") ?? 0) + (Int(SingletonClass.sharedInstance.FreeMinutes) ?? 0)
            
            buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(TotalMinutes)"))
            
        } else {
            if textFieldSelectMinutes?.text ?? "" == "1" {
                
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            } else {
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            }
        }
        
        
     
        let CheckBalance = (TotalPriceInDouble * TotalMinutesInDouble) - AvailableCreditInDouble
        //(((Double(PriceOfSelectedCategory) ?? 0.00) * (Double(textFieldSelectMinutes?.text ?? "1" ) ?? 0.00)) - Double("\(AvailableCredit)") ?? 0.00)
        print(CheckBalance)
        
        buyMinutesData.append(buyMinutesCategory(name: "Available Credit", price: "\(Currency) \(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00))"))
        if CheckBalance > 0 {
            buyMinutesData.append(buyMinutesCategory(name: "Amount to Pay", price: "\(Currency) \(String(format: "%.2f", (("\(CheckBalance)" as NSString?)?.floatValue) ?? 0.00))"))
        }
        tblMinutesprice.reloadData()
        
        
        
        /*
        
        if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
            buyMinutesData.append(buyMinutesCategory(name: "Free Minutes", price: "\(SingletonClass.sharedInstance.FreeMinutes)"))
            let TotalMinutes = (Int(textFieldSelectMinutes?.text ?? "") ?? 0) + (Int(SingletonClass.sharedInstance.FreeMinutes) ?? 0)
            
            buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(TotalMinutes)"))
            
        } else {
            if textFieldSelectMinutes?.text ?? "" == "1" {
                
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            } else {
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            }
        }
        
        buyMinutesData.append(buyMinutesCategory(name: "Price", price: "\(Currency) \( String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00))"))
        
        
        let TotalPriceInDouble : Double = Double(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let TotalMinutesInDouble : Double = Double(String(format: "%.2f", ((textFieldSelectMinutes?.text as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let AvailableCreditInDouble : Double = Double(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        
        buyMinutesData.append(buyMinutesCategory(name: "Total Price", price: "\(Currency) \( String(format: "%.2f", (("\(TotalPriceInDouble * TotalMinutesInDouble)" as NSString?)?.floatValue) ?? 0.00))"))
        let CheckBalance = (TotalPriceInDouble * TotalMinutesInDouble) - AvailableCreditInDouble
        //(((Double(PriceOfSelectedCategory) ?? 0.00) * (Double(textFieldSelectMinutes?.text ?? "1" ) ?? 0.00)) - Double("\(AvailableCredit)") ?? 0.00)
        print(CheckBalance)
        
        buyMinutesData.append(buyMinutesCategory(name: "Available Credit", price: "\(Currency) \(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00))"))
        if CheckBalance > 0 {
            buyMinutesData.append(buyMinutesCategory(name: "Amount to Pay", price: "\(Currency) \(String(format: "%.2f", (("\(CheckBalance)" as NSString?)?.floatValue) ?? 0.00))"))
        }
        tblMinutesprice.reloadData()
        
        */
        
        backButtonClick = {
            if let click = self.closourForClickOnBack {
                click()
            }
        }
        
        
        //        if textFieldSelectMinutes?.text ?? "" == "1" {
        //            buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "") minute"))
        //        } else {
        //            buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "") minutes"))
        //        }
        //
        //        buyMinutesData.append(buyMinutesCategory(name: "Price", price: "\(Currency) \(PriceOfSelectedCategory)"))
        //
        //
        //        let TotalPriceInDouble : Double = Double(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        //        let TotalMinutesInDouble : Double = Double(String(format: "%.2f", ((textFieldSelectMinutes?.text as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        //        let AvailableCreditInDouble : Double = Double(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        //        buyMinutesData.append(buyMinutesCategory(name: "Total Price", price: "\(TotalPriceInDouble * TotalMinutesInDouble)"))
        //        let CheckBalance = (TotalPriceInDouble * TotalMinutesInDouble) - AvailableCreditInDouble
        //        //(((Double(PriceOfSelectedCategory) ?? 0.00) * (Double(textFieldSelectMinutes?.text ?? "1" ) ?? 0.00)) - Double("\(AvailableCredit)") ?? 0.00)
        //        print(CheckBalance)
        //        buyMinutesData.append(buyMinutesCategory(name: "Available Credit", price: "\(Currency) \(AvailableCredit)"))
        //        if CheckBalance > 0 {
        //            buyMinutesData.append(buyMinutesCategory(name: "Amount to Pay", price: "\(Currency) \(String(format: "%.2f", (("\(CheckBalance)" as NSString?)?.floatValue) ?? 0.00))"))
        //        }
        //
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        tblMinutesprice.cornerRadius = 8
        tblMinutesprice.layer.borderColor = appBorderColor?.cgColor
        tblMinutesprice.layer.borderWidth = 1
        tblMinutesprice.backgroundColor = .clear
        
        borderView1.cornerRadius = 8
        borderView1.layer.borderColor = appBorderColor?.cgColor
        borderView1.layer.borderWidth = 1
        borderView1.backgroundColor = .clear
        
        borderView2.cornerRadius = 8
        borderView2.layer.borderColor = appBorderColor?.cgColor
        borderView2.layer.borderWidth = 1
        borderView2.backgroundColor = .clear

        btnPurchase.cornerRadius = 8
        btnPurchase.backgroundColor = appBorderColor
        btnPurchase.setTitleColor(.white, for: .normal)
        
        tblMinutesprice.separatorColor = appBorderColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(SingletonClass.sharedInstance.minutesArray)
        
        setUpDropDown()
        
        carousel.reloadData()
    }
    
    func setUpDropDown() {
        dropDown.anchorView = btnDropDown
        dropDown.dataSource = minutesArray
        dropDown.width = btnDropDown.frame.size.width
        dropDown.textFont = textFieldSelectMinutes.font!
        DropDown.appearance().setupCornerRadius(8)

        // Top of drop down will be below the anchorView
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)

        // Action triggered on selection
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.textFieldSelectMinutes.text = item
            buyMinutesData.removeAll()
            if textFieldSelectMinutes?.text ?? "" == "1" {
                
                buyMinutesData.append(buyMinutesCategory(name: "Minute Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
            } else {
                buyMinutesData.append(buyMinutesCategory(name: "Minutes Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
            }
            
            buyMinutesData.append(buyMinutesCategory(name: "Price", price: "\(Currency) \( String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00))"))
            
            let TotalPriceInDouble : Float = Float(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
            let TotalMinutesInDouble : Float = Float(String(format: "%.2f", ((textFieldSelectMinutes?.text as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
            let AvailableCreditInDouble : Float = Float(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
            
            buyMinutesData.append(buyMinutesCategory(name: "Total Price", price: "\(Currency) \( String(format: "%.2f", (("\(TotalPriceInDouble * TotalMinutesInDouble)" as NSString?)?.floatValue) ?? 0.00))"))
            
            
            
            if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
                buyMinutesData.append(buyMinutesCategory(name: "Free Minutes", price: "\(SingletonClass.sharedInstance.FreeMinutes)"))
                let TotalMinutes = (Int(textFieldSelectMinutes?.text ?? "") ?? 0) + (Int(SingletonClass.sharedInstance.FreeMinutes) ?? 0)
                
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(TotalMinutes)"))
                
            } else {
                if textFieldSelectMinutes?.text ?? "" == "1" {
                    
                    buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
                } else {
                    buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
                }
            }
            
            let CheckBalance = (TotalPriceInDouble * TotalMinutesInDouble) - AvailableCreditInDouble
            //(((Double(PriceOfSelectedCategory) ?? 0.00) * (Double(textFieldSelectMinutes?.text ?? "1" ) ?? 0.00)) - Double("\(AvailableCredit)") ?? 0.00)
            print(CheckBalance)
            
            buyMinutesData.append(buyMinutesCategory(name: "Available Credit", price: "\(Currency) \(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00))"))
            if CheckBalance > 0 {
                buyMinutesData.append(buyMinutesCategory(name: "Amount to Pay", price: "\(Currency) \(String(format: "%.2f", (("\(CheckBalance)" as NSString?)?.floatValue) ?? 0.00))"))
            }
            tblMinutesprice.reloadData()
            
            
        }

        

    }
    
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        
        pickerViewForAll.generalPickerDelegate = self
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == textFieldSelectMinutes {
            textFieldSelectMinutes.inputView = pickerViewForAll
            textFieldSelectMinutes.inputAccessoryView = pickerViewForAll.toolbar
            let indexOfA = minutesArray.firstIndex(of: textFieldSelectMinutes.text ?? "") ?? 0 // 0
            pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
            //pickerViewForAll.selectedRow(inComponent: indexOfA)
            self.pickerViewForAll.reloadAllComponents()
        }
        
    }
    //MARK: -  Observer method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblMinutesprice.layer.removeAllAnimations()
        tblMinutePriceHeight.constant = tblMinutesprice.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }
    }
    //MARK: -  other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //        self.dismiss(animated: true, completion: nil)
    //    }
    
    //MARK: -  tbl view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buyMinutesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMinutesprice.dequeueReusableCell(withIdentifier: minutesPriceCell.reuseIdentifier, for: indexPath) as! minutesPriceCell
        cell.lblMinutesCategoryName.text = buyMinutesData[indexPath.row].categoryName
        cell.lblMinutesCategoryPrice.text = buyMinutesData[indexPath.row].categoryPrice
        return cell
    }
    //MARK: -  IBActions
    @IBAction func btnDropDownClick(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func btnPurchaseClick(_ sender: Any) {
        
        print("ATDebug ::::: Payment")
        
        var TotalMiniutes : String = ""
        if let TempBuyMore = buyMinutesData.filter({$0.categoryName == "Total Minutes"}).first {
            
            TotalMiniutes = TempBuyMore.categoryPrice ?? ""
            
        }
        
        
        if self.buyMinutesData.contains(where: {$0.categoryName == "Amount to Pay"}) {
            if isBuyNow {
                
                let BookRequestParameters:[String:Any] = ["user_id": Int(SingletonClass.sharedInstance.UserId) ?? 0 , "advisor_id":Int(AdvisorID) ?? 0,"booking_id":Int(BoookingID) ?? 0,"minute":TotalMiniutes,"category_id":Int(CategoryID) ?? 0,"free_minute":SingletonClass.sharedInstance.FreeMinutes]
                
                let controller:addPaymentVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: addPaymentVC.storyboardID) as! addPaymentVC
                
                controller.bookingData = BookRequestParameters
                controller.isFromBuyMinutes = true
                controller.bookingID = BoookingID
                controller.isFromBuyMoreMinutes = true
                if let TempMoneyToAdd = buyMinutesData.filter({$0.categoryName == "Amount to Pay"}).first {
                    controller.MoneyToAdded = TempMoneyToAdd.categoryPrice?.replacingOccurrences(of: "\(Currency) ", with: "") ?? ""
                   
                }
                
                controller.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(controller, animated: true)
                print(myProfileCategory.MyCredit.value)
                
                
            } else {
                if self.buyMinutesData.contains(where: {$0.categoryName == "Free Minutes"}) {
                    SingletonClass.sharedInstance.freeMinutesAdded = (true,SingletonClass.sharedInstance.FreeMinutes)
                } else {
                    SingletonClass.sharedInstance.freeMinutesAdded = (false,"0")
                }
                let BookRequestParameters:[String:Any] = ["user_id": Int(SingletonClass.sharedInstance.UserId) ?? 0 , "username":"\(SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "")", "advisor_id":Int(AdvisorID) ?? 0,"type":CommunicationType,"minute":TotalMiniutes,"category_id":Int(CategoryID) ?? 0,"free_minute":SingletonClass.sharedInstance.FreeMinutes]
                
                let controller:addPaymentVC = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: addPaymentVC.storyboardID) as! addPaymentVC
                
                controller.bookingData = BookRequestParameters
                controller.isFromBuyMinutes = true
                controller.isFromBuyMoreMinutes = false
                controller.bookingID = BoookingID
                if let TempMoneyToAdd = buyMinutesData.filter({$0.categoryName == "Amount to Pay"}).first {
                    controller.MoneyToAdded = TempMoneyToAdd.categoryPrice?.replacingOccurrences(of: "\(Currency) ", with: "") ?? ""
                   
                }
                controller.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(controller, animated: true)
                print(myProfileCategory.MyCredit.value)
            }
        } else {
            if isBuyNow {
                if SocketIOManager.shared.socket.status == .connected {
                    
                    let BookRequestParameters:[String:Any] = ["user_id": Int(SingletonClass.sharedInstance.UserId) ?? 0, "advisor_id":Int(AdvisorID) ?? 0,"booking_id":Int(BoookingID) ?? 0,"minute":TotalMiniutes,"free_minute":SingletonClass.sharedInstance.FreeMinutes]
                    
                    
                    SocketIOManager.shared.socketEmit(for: socketApiKeys.buy_more_minute.rawValue, with: BookRequestParameters)
                   
                    if let click = self.closourForBookRequest {
                        click()
                    }
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            } else {
                if SocketIOManager.shared.socket.status == .connected {
                    
                    let BookRequestParameters:[String:Any] = ["user_id": Int(SingletonClass.sharedInstance.UserId) ?? 0 , "username":"\(SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "")", "advisor_id":Int(AdvisorID) ?? 0,"type":CommunicationType,"minute":TotalMiniutes,"category_id":Int(CategoryID) ?? 0,"free_minute":SingletonClass.sharedInstance.FreeMinutes]
                    
                    
                    SocketIOManager.shared.socketEmit(for: socketApiKeys.booking_request.rawValue, with: BookRequestParameters)
                    if self.buyMinutesData.contains(where: {$0.categoryName == "Free Minutes"}) {
                        SingletonClass.sharedInstance.freeMinutesAdded = (true,SingletonClass.sharedInstance.FreeMinutes)
                    } else {
                        SingletonClass.sharedInstance.freeMinutesAdded = (false,"0")
                    }
                    if let click = self.closourForBookRequest {
                        click()
                    }
                } else {
//                    Utilities.displayAlert(AppName, message: "Soket not connected")
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
  
    
}
class minutesPriceCell : UITableViewCell {
    
    @IBOutlet weak var lblMinutesCategoryName: buyMinutesLabel!
    @IBOutlet weak var lblMinutesCategoryPrice: buyMinutesLabel!
}
class buyMinutesCategory {
    var categoryName : String?
    var categoryPrice : String?
    init(name:String,price:String) {
        self.categoryName = name
        self.categoryPrice = price
    }
}
extension BuyMinutesViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        if carousel.currentItemIndex < 0 { return }
//        let item = minutesArray[pickerViewForAll.selectedRow(inComponent: 0)]
        let item = minutesArray[carousel.currentItemIndex]

        self.textFieldSelectMinutes.text = item
        textFieldSelectMinutes.resignFirstResponder()
        buyMinutesData.removeAll()
        if textFieldSelectMinutes?.text ?? "" == "1" {
            
            buyMinutesData.append(buyMinutesCategory(name: "Minute Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
        } else {
            buyMinutesData.append(buyMinutesCategory(name: "Minutes Purchased", price: "\(textFieldSelectMinutes?.text ?? "")"))
        }
        
        buyMinutesData.append(buyMinutesCategory(name: "Price", price: "\(Currency) \( String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00))"))
        
        let TotalPriceInDouble : Float = Float(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let TotalMinutesInDouble : Float = Float(String(format: "%.2f", ((textFieldSelectMinutes?.text as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let AvailableCreditInDouble : Float = Float(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        
        buyMinutesData.append(buyMinutesCategory(name: "Total Price", price: "\(Currency) \( String(format: "%.2f", (("\(TotalPriceInDouble * TotalMinutesInDouble)" as NSString?)?.floatValue) ?? 0.00))"))
        
        
        
        if SingletonClass.sharedInstance.FreeMinutes != "" && SingletonClass.sharedInstance.FreeMinutes != "0" {
            buyMinutesData.append(buyMinutesCategory(name: "Free Minutes", price: "\(SingletonClass.sharedInstance.FreeMinutes)"))
            let TotalMinutes = (Int(textFieldSelectMinutes?.text ?? "") ?? 0) + (Int(SingletonClass.sharedInstance.FreeMinutes) ?? 0)
            
            buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(TotalMinutes)"))
            
        } else {
            if textFieldSelectMinutes?.text ?? "" == "1" {
                
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            } else {
                buyMinutesData.append(buyMinutesCategory(name: "Total Minutes", price: "\(textFieldSelectMinutes?.text ?? "")"))
            }
        }
        
        
        
        
        
        
     
        let CheckBalance = (TotalPriceInDouble * TotalMinutesInDouble) - AvailableCreditInDouble
        //(((Double(PriceOfSelectedCategory) ?? 0.00) * (Double(textFieldSelectMinutes?.text ?? "1" ) ?? 0.00)) - Double("\(AvailableCredit)") ?? 0.00)
        print(CheckBalance)
        
        buyMinutesData.append(buyMinutesCategory(name: "Available Credit", price: "\(Currency) \(String(format: "%.2f", ((AvailableCredit as NSString?)?.floatValue) ?? 0.00))"))
        if CheckBalance > 0 {
            buyMinutesData.append(buyMinutesCategory(name: "Amount to Pay", price: "\(Currency) \(String(format: "%.2f", (("\(CheckBalance)" as NSString?)?.floatValue) ?? 0.00))"))
        }
        tblMinutesprice.reloadData()
        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension BuyMinutesViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return minutesArray.count
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return minutesArray[row]
        
    }
    
    
}

extension BuyMinutesViewController: iCarouselDelegate, iCarouselDataSource {
    func numberOfItems(in carousel: iCarousel) -> Int {
        return minutesArray.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var minutesView: MinutesView
        if view == nil {
            minutesView = UINib(nibName: "MinutesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! MinutesView
        } else {
            minutesView = view as! MinutesView
        }
        
        minutesView.frame.size = CGSize(width: carousel.frame.size.height, height: carousel.frame.size.height)
//        if index == carousel.currentItemIndex {
//            minutesView.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
//        } else {
//            minutesView.transform = CGAffineTransform.identity
//        }
        minutesView.contentView.backgroundColor = appBorderColor
        minutesView.lblMinutes.text = minutesArray[index]
        
        let TotalPriceInDouble : Float = Float(String(format: "%.2f", ((PriceOfSelectedCategory as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        let TotalMinutesInDouble : Float = Float(String(format: "%.2f", ((minutesArray[index] as NSString?)?.floatValue) ?? 0.00)) ?? 0.00
        
        minutesView.lblPrice.text = "\(Currency) \( String(format: "%.2f", (("\(TotalPriceInDouble * TotalMinutesInDouble)" as NSString?)?.floatValue) ?? 0.00))"
        return minutesView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.2
        }
        if (option == .wrap) {
            return 1
        }
//        if (option == .showBackfaces) {
//            return false
//        }
        if (option == .visibleItems) {
            return 3
        }
        return value
    }
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        let view = carousel.currentItemView
//        view?.transform = CGAffineTransform.identity.scaledBy(x: 1.1, y: 1.1)
        
        didTapDone()
    }
    
//    func carouselItemWidth(_ carousel: iCarousel) -> CGFloat {
//        return carousel.frame.size.height - 20
//    }
    
    func carouselDidScroll(_ carousel: iCarousel) {
//        carousel.visibleItemViews.forEach { view in
//            (view as! UIView).transform = CGAffineTransform.identity
//        }
    }
    
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        
    }
}
