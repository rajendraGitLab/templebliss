//
//  MinutesView.swift
//  TempleBliss
//
//  Created by iMac on 28/12/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class MinutesView: UIView {
    
    // MARK: - Outlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblMinutes: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    
    // MARK: - Methods
    override func awakeFromNib() {
        contentView.cornerRadius = 8
    }

}
