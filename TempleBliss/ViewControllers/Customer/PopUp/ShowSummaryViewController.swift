//
//  ShowSummaryViewController.swift
//  TempleBliss
//
//  Created by Apple on 17/03/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class ShowSummaryViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //MARK: - Properties
    var isFromCustomer = false
    var dataForTable : [String:String] = [:]
    var showSummaryData = [showSummarayModel]()
    var didBack: (() -> ())?
    //MARK: - IBOutlets
    @IBOutlet weak var tblMinutePriceHeight: NSLayoutConstraint!
    @IBOutlet weak var tblShowSummary: UITableView!
    @IBOutlet weak var btnPurchase: theamSubmitButton!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        self.tblShowSummary.removeObserver
        //   self.tblShowSummary.removeObserver(self, forKeyPath: "contentSize")
        //   self.tblShowSummary.removeObserver(self, forKeyPath: , options: NSKeyValueObservingOptions.new, context: nil)
        self.tblShowSummary.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        
        setLocalization()
        setValue()
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        if isFromCustomer {
            if dataForTable[nameForTitleInSummary.category_name.ParamName] ?? "" != "" && dataForTable[nameForTitleInSummary.category_name.ParamName]  ?? "" != "" {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.category_name.Name, valueOfTitle: dataForTable[nameForTitleInSummary.category_name.ParamName]  ?? ""))
                
            }
            
            if dataForTable[nameForTitleInSummary.total_minute.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.total_minute.ParamName]  ?? "" != "0.00" {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.total_minute.Name, valueOfTitle: dataForTable[nameForTitleInSummary.total_minute.ParamName]  ?? ""))
                
            }
            
            if dataForTable[nameForTitleInSummary.free_minute.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.free_minute.ParamName]  ?? "" != "0" && dataForTable[nameForTitleInSummary.free_minute.ParamName]  ?? "" != "0.00" && dataForTable[nameForTitleInSummary.free_minute.ParamName]  ?? "" != "00:00"  {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.free_minute.Name, valueOfTitle: dataForTable[nameForTitleInSummary.free_minute.ParamName]  ?? ""))
                
            }
            
            if dataForTable[nameForTitleInSummary.booking_amount.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.booking_amount.ParamName]  ?? "" != "0.00" {
                if (dataForTable[nameForTitleInSummary.booking_amount.ParamName]  ?? "").contains("\(Currency)"){
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.booking_amount.Name, valueOfTitle: dataForTable[nameForTitleInSummary.booking_amount.ParamName]  ?? ""))
                } else {
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.booking_amount.Name, valueOfTitle: "\(Currency) \(dataForTable[nameForTitleInSummary.booking_amount.ParamName]  ?? "")"))
                }
            }
            
            if dataForTable[nameForTitleInSummary.total_chargeable_amount.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.total_chargeable_amount.ParamName]  ?? "" != "0" {
                if (dataForTable[nameForTitleInSummary.total_chargeable_amount.ParamName]  ?? "").contains("\(Currency)"){
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.total_chargeable_amount.Name, valueOfTitle: dataForTable[nameForTitleInSummary.total_chargeable_amount.ParamName]  ?? ""))
                } else {
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.total_chargeable_amount.Name, valueOfTitle: "\(Currency) \(dataForTable[nameForTitleInSummary.total_chargeable_amount.ParamName]  ?? "")"))
                }
            }
            
            if dataForTable[nameForTitleInSummary.discount_type.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.discount_type.ParamName]  ?? "" != "0" {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_type.Name, valueOfTitle: dataForTable[nameForTitleInSummary.discount_type.ParamName]  ?? ""))
                
            }
            
            if (dataForTable[nameForTitleInSummary.discount_type.ParamName] )?.lowercased() ?? "" == "percentage" {
                if dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "" != "0" {
                    
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_value.Name, valueOfTitle: "\(dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "")%"))
                }
            } else {
                if dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "" != "0" {
                    if (dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "").contains("\(Currency)"){
                        showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_value.Name, valueOfTitle: dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? ""))
                    } else {
                        showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_value.Name, valueOfTitle: "\(Currency) \(dataForTable[nameForTitleInSummary.discount_value.ParamName]  ?? "")"))
                    }
                }
            }

            
            //            if dataForTable[nameForTitleInSummary.discount_value.ParamName] as? String ?? "" != "" && dataForTable[nameForTitleInSummary.discount_value.ParamName] as? String ?? "" != "0" {
//                if (dataForTable[nameForTitleInSummary.discount_value.ParamName] as? String ?? "").contains("\(Currency)"){
//                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_value.Name, valueOfTitle: dataForTable[nameForTitleInSummary.discount_value.ParamName] as? String ?? ""))
//                } else {
//                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.discount_value.Name, valueOfTitle: "\(Currency) \(dataForTable[nameForTitleInSummary.discount_value.ParamName] as? String ?? "")"))
//                }
//            }
            
            
        } else {
            if dataForTable[nameForTitleInSummary.category_name.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.category_name.ParamName]  ?? "" != "" {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.category_name.Name, valueOfTitle: dataForTable[nameForTitleInSummary.category_name.ParamName]  ?? ""))
                
                
            }
            if dataForTable[nameForTitleInSummary.total_minute.ParamName]  ?? "" != "" {
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.total_minute.Name, valueOfTitle: dataForTable[nameForTitleInSummary.total_minute.ParamName]  ?? ""))
            }
            if dataForTable[nameForTitleInSummary.commission_rate.ParamName]  ?? "" != "" {
                
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.commission_rate.Name, valueOfTitle: "\(dataForTable[nameForTitleInSummary.commission_rate.ParamName]  ?? "")%"))
                
            }
            if dataForTable[nameForTitleInSummary.advisor_special_day_commission.ParamName]  ?? "" != "" && dataForTable[nameForTitleInSummary.advisor_special_day_commission.ParamName]  ?? "" != "0" {
                showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.advisor_special_day_commission.Name, valueOfTitle: "\(dataForTable[nameForTitleInSummary.advisor_special_day_commission.ParamName]  ?? "")%"))
            }
            if dataForTable[nameForTitleInSummary.session_earning.ParamName]  ?? "" != "" {
                if (dataForTable[nameForTitleInSummary.session_earning.ParamName]  ?? "").contains("\(Currency)"){
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.session_earning.Name, valueOfTitle: dataForTable[nameForTitleInSummary.session_earning.ParamName]  ?? ""))
                } else {
                    showSummaryData.append(showSummarayModel(name: nameForTitleInSummary.session_earning.Name, valueOfTitle: "\(Currency) \(dataForTable[nameForTitleInSummary.session_earning.ParamName]  ?? "")"))
                }
            }
            
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    //MARK: -  Observer method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tblShowSummary.layer.removeAllAnimations()
        tblMinutePriceHeight.constant = tblShowSummary.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.updateViewConstraints()
        }
    }
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    
    //MARK: -  tbl view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showSummaryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblShowSummary.dequeueReusableCell(withIdentifier: showSummaryCell.reuseIdentifier, for: indexPath) as! showSummaryCell
        cell.lblTitle.text = showSummaryData[indexPath.row].title
        cell.lblValue.text = showSummaryData[indexPath.row].value
        return cell
    }
    //MARK: - IBActions
    @IBAction func btnPurchaseClick(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.didBack?()
    }
    
    //MARK: - API Calls
    
    
    
}
class showSummaryCell : UITableViewCell {
    
    @IBOutlet weak var lblTitle: buyMinutesLabel!
    @IBOutlet weak var lblValue: buyMinutesLabel!
}
class showSummarayModel {
    var title : String?
    var value : String?
    init(name:String,valueOfTitle:String) {
        self.title = name
        self.value = valueOfTitle
    }
}
enum nameForTitleInSummary {
    case total_minute,free_minute,booking_amount,total_chargeable_amount,discount_type,discount_value,message,commission_rate,session_earning,advisor_special_day_commission,category_name
    
    var Name:String {
        switch self {
        case .total_minute:
            return "total_minute".replacingOccurrences(of: "_", with: " ").capitalized
        case .free_minute:
            return "free_minute".replacingOccurrences(of: "_", with: " ").capitalized
        case .booking_amount:
            return "total_amount".replacingOccurrences(of: "_", with: " ").capitalized
        case .total_chargeable_amount:
            return "total_charged_amount".replacingOccurrences(of: "_", with: " ").capitalized
        case .discount_type:
            return "discount_type".replacingOccurrences(of: "_", with: " ").capitalized
        case .discount_value:
            return "discount_value".replacingOccurrences(of: "_", with: " ").capitalized
        case .message:
            return "message".replacingOccurrences(of: "_", with: " ").capitalized
        case .session_earning:
            return "session_earning".replacingOccurrences(of: "_", with: " ").capitalized
        case .commission_rate:
            return "commission_rate".replacingOccurrences(of: "_", with: " ").capitalized
        case .advisor_special_day_commission:
            return "special_day_commission".replacingOccurrences(of: "_", with: " ").capitalized
        case .category_name:
            return "category_name".replacingOccurrences(of: "_", with: " ").capitalized
        }
    }
    var ParamName:String {
        switch self {
        case .total_minute:
            return "total_minute"
        case .free_minute:
            return "free_minute"
        case .booking_amount:
            return "booking_amount"
        case .total_chargeable_amount:
            return "total_chargeable_amount"
        case .discount_type:
            return "discount_type"
        case .discount_value:
            return "discount_value"
        case .message:
            return "message"
        case .commission_rate:
            return "commission_rate"
        case .session_earning:
            return "session_earning"
        case .advisor_special_day_commission:
            return "advisor_special_day_commission"
        case .category_name:
            return "category_name"
            
            
            
        }
    }
}

