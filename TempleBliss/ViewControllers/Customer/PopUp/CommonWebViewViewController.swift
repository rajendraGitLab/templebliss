//
//  CommonWebViewViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import WebKit

class CommonWebViewViewController: BaseViewController, WKNavigationDelegate {
    
    //MARK: -  Properties
    var customTabBarController: CustomTabBarVC?
    var strUrl = "https://www.google.com"
    private let webView = WKWebView(frame: .zero)
    var strNavTitle = ""
    
    // MARK: - IBOutlets
    @IBOutlet weak var vwWebMain: viewViewClearBG!
    
    // MARK: - ViewController Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    // MARK: - Other Methods
    func setUp() {
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        addNavBarImage(isLeft: true, isRight: true)
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: strNavTitle, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        webView.backgroundColor = .clear
     //   webView.isOpaque = false
//        [webView setBackgroundColor:[UIColor clearColor]];
//        [webView setOpaque:NO];
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView)
        NSLayoutConstraint.activate([
            self.webView.leftAnchor.constraint(equalTo: self.vwWebMain.leftAnchor),
            self.webView.bottomAnchor.constraint(equalTo: self.vwWebMain.bottomAnchor),
            self.webView.rightAnchor.constraint(equalTo: self.vwWebMain.rightAnchor),
            self.webView.topAnchor.constraint(equalTo: self.vwWebMain.topAnchor),
        ])
        self.webView.backgroundColor = .clear
        self.view.setNeedsLayout()
        let request = URLRequest(url: URL.init(string: "\(APIEnvironment.profileBu)\(strUrl)")!)
        self.webView.navigationDelegate = self
        //self.webView.loadHTMLString("\(APIEnvironment.profileBu)\(strUrl)", baseURL: nil)
       

        self.webView.load(request)
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.customTabBarController?.hideTabBar()
        setUp()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        Utilities.showHud()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        Utilities.hideHud()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        Utilities.hideHud()
    }
    
    // MARK: - IBActions
    
    // MARK: - Api Calls
}
