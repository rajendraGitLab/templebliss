//
//  CommonTextViewViewController.swift
//  TempleBliss
//
//  Created by Apple on 02/02/21.
//  Copyright © 2021 EWW071. All rights reserved.
//

import UIKit

class CommonTextViewViewController: BaseViewController {

    
    //MARK: - Properties
    var strUrl = "https://www.google.com"
    var customTabBarController: CustomTabBarVC?
    var strNavTitle = ""
    //MARK: - IBOutlets
    
    @IBOutlet weak var commonTextview: UITextView!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        if self.tabBarController != nil {
            self.customTabBarController = (self.tabBarController as! CustomTabBarVC)
        }
        addNavBarImage(isLeft: true, isRight: true)
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: strNavTitle, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "", isChatScreen: false, userImage: "")
        
       
//        let attributes: [NSAttributedString.Key: Any] = [
//            .foregroundColor: UIColor.white,
//            .backgroundColor: UIColor.clear,
//            .font: CustomFont.regular.returnFont(15)
//        ]
//        var attrStr = try! NSAttributedString(
//                    data: strUrl.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
//                    options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
//
//                commonTextview.attributedText = attrStr
        commonTextview.text = stringFromHTML(string: strUrl)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
              self.customTabBarController?.hideTabBar()
          }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    func stringFromHTML( string: String?) -> String
        {
            do{
                let str = try NSAttributedString(data:string!.data(using: String.Encoding.utf8, allowLossyConversion: true
                    )!, options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                return str.string
            } catch
            {
                print("html error\n",error)
            }
            return ""
        }
    //MARK: - IBActions
    
    
    //MARK: - API Calls
    
    
    
    

}
