//
//  CommunicationReminerViewController.swift
//  TempleBliss
//
//  Created by Apple on 24/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class CommunicationReminerViewController: BaseViewController {

    //MARK: - Properties
    var closourForBuyMinutes : (() -> ())?
    var closourForResume : (() -> ())?
    var timePending : String = ""
    var SessionType = ""
    //MARK: - IBOutlets
    
    @IBOutlet weak var lblTimeLeft: chatReminederLabel!
    @IBOutlet weak var lblChatReminderDescription: chatReminederLabel!
    @IBOutlet weak var btnBuyMoreMinutes: theamSubmitButton!
    @IBOutlet weak var btnResumeChat: theamButtonWithBorder!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        backButtonClick = {
            if let click = self.closourForResume {
                click()
            }
        }
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: "", leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
        lblTimeLeft.text = timePending
        lblChatReminderDescription.text = "Your \(SessionType) will be resume after \(timePending) minute!"
    }
    //MARK: - IBActions
    
    @IBAction func btnBuyMoreClick(_ sender: Any) {
        if let click = self.closourForBuyMinutes {
            self.dismiss(animated: true, completion: nil)
            click()
        }
//        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID)
//        let navigationController = UINavigationController(rootViewController: controller)
//        navigationController.modalPresentationStyle = .overCurrentContext
//        navigationController.modalTransitionStyle = .crossDissolve
//        self.present(navigationController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnResumeClick(_ sender: Any) {
        if let click = self.closourForResume {
            self.dismiss(animated: true, completion: nil)
            click()
        }
        
      
    }
    //MARK: - API Calls
    
    
    
    

}
