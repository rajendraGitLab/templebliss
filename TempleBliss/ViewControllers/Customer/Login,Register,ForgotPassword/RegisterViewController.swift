//
//  RegisterViewController.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import GoogleSignIn
import FBSDKLoginKit
import AuthenticationServices

class RegisterViewController: BaseViewController {

    //MARK: -  Properties
    var isSocialLogin : Bool = false
    var strSelectedDate = ""
    var socialID = ""
    var socialType = ""
    
    var FullNameString : String = ""
    var emailString : String = ""
    
    //var CountryNameWithCode : [CountryCode] = []
    let pickerViewForAll = GeneralPickerView()
    //MARK: -  IBOutlets
    
    @IBOutlet weak var TextFieldSelectCountryCode: CountryCodeTextField!
    
    @IBOutlet weak var lblRegisterToContinue: RegistrationPageLabel!
    @IBOutlet weak var lblDescription: RegistrationPageLabel!
    
    @IBOutlet weak var textFieldFullName: RegisterTextField!
    
    @IBOutlet weak var DtPicker: UIDatePicker!
    @IBOutlet weak var textFieldEmail: RegisterTextField!
    @IBOutlet weak var textFieldPhone: RegisterTextField!
    @IBOutlet weak var textFieldDOB: RegisterTextField!
    @IBOutlet weak var textFieldPassword: RegisterTextField!
    @IBOutlet weak var textFieldConfirmPassoword: RegisterTextField!
    @IBOutlet weak var lblByCLicking: RegistrationPageLabel!
    @IBOutlet weak var btnTerms: RegisterScreenButon!
    @IBOutlet weak var btnRegisterNow: theamSubmitButton!
    
    @IBOutlet weak var lblAlreadyUser: RegistrationPageLabel!
    @IBOutlet weak var btnClickHereToLogin: RegisterScreenButon!
    //MARK: -  View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setLocalization()
        setValue()
        if SingletonClass.sharedInstance.CountryNameWithCode.count != 0 {
            self.TextFieldSelectCountryCode.text = SingletonClass.sharedInstance.CountryNameWithCode[0].dialCode
            self.setupDelegateForPickerView()
        } else {
            self.TextFieldSelectCountryCode.text = "+91"
        }
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        textFieldPhone.delegate = self
        textFieldFullName.delegate = self
        
        NotificationCenter.default.addObserver(forName: .AccessTokenDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            
            // Print out access token
            print("FB Access Token: \(String(describing: AccessToken.current?.tokenString))")
        }
        
        if FullNameString != "" && emailString != "" {
            textFieldPassword.text = "12345678"
            textFieldConfirmPassoword.text = "12345678"
            textFieldPassword.superview?.isHidden = true
            textFieldConfirmPassoword.superview?.isHidden = true
            
            
            textFieldFullName.isUserInteractionEnabled = false
            textFieldFullName.superview?.alpha = 0.6
            
            textFieldEmail.isUserInteractionEnabled = false
            textFieldEmail.superview?.alpha = 0.6
            
        } else {
            textFieldPassword.superview?.isHidden = false
            textFieldConfirmPassoword.superview?.isHidden = false
            
            textFieldFullName.isUserInteractionEnabled = true
            textFieldFullName.superview?.alpha = 1.0
            
            textFieldEmail.isUserInteractionEnabled = true
            textFieldEmail.superview?.alpha = 1.0
        }
        
        textFieldEmail.text = emailString
        textFieldFullName.text = FullNameString
        TextFieldSelectCountryCode.delegate = self
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
  
        
    }
    
    //MARK: -  other methods
    func setLocalization() {
        
    }
    func setValue() {
        lblRegisterToContinue.text = "Register To Continue"
        lblDescription.text = "Enter your details to continue with TempleBliss"
        
        textFieldFullName.placeholder = "Nick Name"
        
        textFieldEmail.placeholder = "Email Address"
        textFieldPhone.placeholder = "Phone Number"
        textFieldDOB.placeholder = "DOB"
        textFieldPassword.placeholder = "Password"
        textFieldConfirmPassoword.placeholder = "Confirm Password"
        lblByCLicking.text = "By clicking register, I am agree with"
        btnTerms.setTitle("T&C", for: .normal)
        btnRegisterNow.setTitle("REGISTER NOW", for: .normal)
        textFieldDOB.inputView = DtPicker
        lblAlreadyUser.text = "Already User?"
        btnClickHereToLogin.setTitle("Click here to Login", for: .normal)
        
        textFieldDOB.setInputViewDatePicker(target: self, selector: #selector(self.btnDoneDatePickerClicked(_:)))
        
    }
    func setUpDatePicker() {
        if "\(userDefault.value(forKey: UserDefaultsKey.selLanguage.rawValue) ?? "")" == "ar" {
            DtPicker.calendar = Calendar(identifier: .islamicTabular)
        } else {
            DtPicker.calendar = Calendar(identifier: .gregorian)
        }
        DtPicker.maximumDate = Date()
        DtPicker.datePickerMode = .date
        DtPicker.date = Date()
    }
    func setupDelegateForPickerView() {
        pickerViewForAll.dataSource = self
        pickerViewForAll.delegate = self
        
        pickerViewForAll.generalPickerDelegate = self
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == TextFieldSelectCountryCode {
            TextFieldSelectCountryCode.inputView = pickerViewForAll
            TextFieldSelectCountryCode.inputAccessoryView = pickerViewForAll.toolbar
           
            if let DummyFirst = SingletonClass.sharedInstance.CountryNameWithCode.first(where: {$0.code == TextFieldSelectCountryCode.text ?? ""}) {
                
                let indexOfA = SingletonClass.sharedInstance.CountryNameWithCode.firstIndex(of: DummyFirst) ?? 0
                pickerViewForAll.selectRow(indexOfA, inComponent: 0, animated: false)
               
                self.pickerViewForAll.reloadAllComponents()
            }
            
        }
        
    }
    //MARK: -  IBActions
    
    @IBAction func btnRegisterNowClick(_ sender: Any) {
        if validation(){
           
            if socialID != "" && socialType != "" {
                WebserviceForSocialLogin()
            } else {
                webserviceForRegister()
            }
            
        }

    }
    @IBAction func btnFBClicked(_ sender: UIButton)
    {
        
        let loginManager = LoginManager()
        
        if let _ = AccessToken.current {
            // Access token available -- user already logged in
            // Perform log out
            
            loginManager.logOut()
          
            
        } else {
            // Access token not available -- user already logged out
            // Perform log in
            
            loginManager.logIn(permissions: [], from: self) { [weak self] (result, error) in
                
                // Check for error
                guard error == nil else {
                    // Error occurred
                    print(error!.localizedDescription)
                    return
                }
                
                // Check for cancel
                guard let result = result, !result.isCancelled else {
                    print("User cancelled login")
                    return
                }
                
                self?.getFBUserData()
            }
        }
    }

    @IBAction func btnTermsClick(_ sender: Any) {
        let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommonWebViewViewController.storyboardID) as! CommonWebViewViewController
        controller.strNavTitle = "Terms & Conditions"
        controller.strUrl = SingletonClass.sharedInstance.settingsModel?.customerTermsCondition ?? ""
        self.navigationController?.pushViewController(controller, animated: true)
    }
    @IBAction func btnLoginClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionHandleAppleSignin(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    @IBAction func btnDoneDatePickerClicked(_ sender: UIButton) {
        if let datePicker = self.textFieldDOB.inputView as? UIDatePicker {
            
                let formatter = DateFormatter()
                formatter.dateFormat = DateFormatterString.onlyDate.rawValue
                
                textFieldDOB.text = formatter.string(from: datePicker.date)
          
        }
        self.textFieldDOB.resignFirstResponder() // 2-5
        
        
//        let dt = DtPicker.date
//        let inputFormatter = DateFormatter()
//        inputFormatter.dateFormat =
//        let resultString = inputFormatter.string(from: dt)
//        textFieldDOB.text = resultString
//        strSelectedDate = resultString
//        self.textFieldDOB.resignFirstResponder()
//        vwDtPicker.isHidden = true
    }
    @IBAction func btnGoogleSignClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let signInConfig = GIDConfiguration.init(clientID: kGoogle_Client_ID)
        
//        GIDSignIn.sharedInstance.presentingViewController = self
//
//        GIDSignIn.sharedInstance.scopes.append("https://www.googleapis.com/auth/plus.login")
//
//        GIDSignIn.sharedInstance.scopes.append("https://www.googleapis.com/auth/user.birthday.read")
//        GIDSignIn.sharedInstance.scopes.append("https://www.googleapis.com/auth/userinfo.profile")
        
        GIDSignIn.sharedInstance.addScopes(["https://www.googleapis.com/auth/plus.login",
                                            "https://www.googleapis.com/auth/user.birthday.read",
                                            "https://www.googleapis.com/auth/userinfo.profile"], presenting: self)
        
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            guard let user = user else { return }
            
            if let profiledata = user.profile {
                let userId : String = user.userID ?? "" // For client-side use only!
                let firstName : String  = profiledata.givenName ?? ""
                let lastName : String  = profiledata.familyName ?? ""
                let email : String = profiledata.email
                
                var dictUserData = [String: AnyObject]()
                
                
                dictUserData["Firstname"] = firstName as AnyObject
                dictUserData["Lastname"] = lastName as AnyObject
                dictUserData["Email"] = email as AnyObject
                dictUserData["MobileNo"] = "" as AnyObject
                //dictUserData["Lat"] = "\(SingletonClass.sharedInstance.latitude)" as AnyObject
                // dictUserData["Lng"] = "\(SingletonClass.sharedInstance.longitude)" as AnyObject
                dictUserData["SocialId"] = "\(userId)" as AnyObject
                dictUserData["SocialType"] = "Google" as AnyObject
                dictUserData["Token"] = SingletonClass.sharedInstance.DeviceToken as AnyObject
                dictUserData["DeviceType"] = "1" as AnyObject
                self.socialID = userId
                self.socialType = "Google"
                
                
                self.textFieldFullName.text = "\(dictUserData["Firstname"] as! String) \(dictUserData["Lastname"] as! String)"
                print("\(dictUserData["Firstname"] as! String) \(dictUserData["Lastname"] as! String)")
                self.textFieldEmail.text = email
                self.textFieldPhone.text = dictUserData["MobileNo"] as? String
                self.textFieldPassword.text = "12345678"
                self.textFieldConfirmPassoword.text = "12345678"
                self.textFieldDOB.text = ""
                
                self.textFieldPassword.superview?.isHidden = true
                self.textFieldConfirmPassoword.superview?.isHidden = true
                
                print(dictUserData)
                
            }
        }
        
    }
    
    //MARK: -  Validation
    func validation()->Bool{
        let firstName = textFieldFullName.validatedText(validationType: ValidatorType.username(field: "nick name"))
        let checkEmail = textFieldEmail.validatedText(validationType: ValidatorType.email)
        let checkPassword = textFieldPassword.validatedText(validationType: ValidatorType.password)
        let confirmPassword = textFieldConfirmPassoword.validatedText(validationType: ValidatorType.requiredField(field: "confirm Password"))
       
        
        if (!firstName.0){
            Utilities.ShowAlert(OfMessage: firstName.1)
            return firstName.0
        }else if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
        else if textFieldPhone.text?.count == 0 {
            Utilities.ShowAlert(OfMessage: "Please enter phone number")
            return false
        }
//        else if (textFieldPhone.text?.count ?? 0) < 10 {
//            Utilities.ShowAlert(OfMessage: "Please enter valid phone number")
//            return false
//        }
        else if textFieldDOB.text == "" {
            Utilities.ShowAlert(OfMessage: "Please select date of birth")
            return false
        }
        else  if(!checkPassword.0) && !isSocialLogin
        {
            Utilities.ShowAlert(OfMessage: checkPassword.1)
            return checkPassword.0
        }else if(!confirmPassword.0){
            Utilities.ShowAlert(OfMessage: confirmPassword.1)
            return confirmPassword.0
        }else if textFieldPassword.text?.lowercased() != textFieldConfirmPassoword.text?.lowercased(){
            Utilities .ShowAlert(OfMessage: "Password and confirm password must be same")
            return false
        }
        return true
    }
    //MARK: -  API Calls
    func webserviceForRegister(){
        let register = RegisterReqModel()
        register.full_name = textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.email = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.phone = (TextFieldSelectCountryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") + (textFieldPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
        let showDate = inputFormatter.date(from: textFieldDOB.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? Date()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let resultString = inputFormatter.string(from: showDate)
        register.dob = resultString
        register.password = textFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.confirm_password = textFieldConfirmPassoword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        register.device_token = SingletonClass.sharedInstance.DeviceToken
        register.device_type = ReqDeviceType
        //register.category_id = "4"
        register.role = SingletonClass.sharedInstance.usertype
        register.social_id = socialID
        register.social_type = socialType
        Utilities.showHud()
        WebServiceSubClass.registerOtp(registerOtpModel: register, showHud: true, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_registerNow)
                let otpModel = RegisterOtpResModel.init(fromJson: json)
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "EnterOTPViewController")as! EnterOTPViewController
                vc.objectRegister = register
                vc.strOtp = otpModel.otp
                self.navigationController?.pushViewController(vc, animated: true)
                //appDel.navigateToHomeCustomer()
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    
    func WebserviceForSocialLogin(){
        let SocialRegister = RegisterReqModel()
        SocialRegister.full_name = textFieldFullName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        SocialRegister.email = textFieldEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        SocialRegister.phone = (TextFieldSelectCountryCode.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") + (textFieldPhone.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        let inputFormatter = DateFormatter()
        inputFormatter.dateFormat = DateFormatterString.onlyDate.rawValue
        let showDate = inputFormatter.date(from: textFieldDOB.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? Date()
        inputFormatter.dateFormat = "yyyy-MM-dd"
        let resultString = inputFormatter.string(from: showDate)
        SocialRegister.dob = resultString
        SocialRegister.password = textFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        SocialRegister.confirm_password = textFieldConfirmPassoword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        SocialRegister.device_token = SingletonClass.sharedInstance.DeviceToken
        SocialRegister.device_type = ReqDeviceType
        //register.category_id = "4"
        SocialRegister.role = SingletonClass.sharedInstance.usertype
        SocialRegister.social_id = socialID
        SocialRegister.social_type = socialType
        Utilities.showHud()
        WebServiceSubClass.register(registerModel:SocialRegister, showHud: true, completion: { (json, status, response) in
            //Utilities.hideHud()
            if(status)
            {
                if SocialRegister.social_id != "" && SocialRegister.social_type != "" {
                 //   Utilities.displayAlert(AppName, message: json["message"].string ?? "", completion: {_ in
                        let socialModel = userSocialData()
                        
                        socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                        socialModel.device_type = ReqDeviceType
                        socialModel.social_id = SocialRegister.social_id
                        socialModel.user_name = SocialRegister.email
                        socialModel.social_type = SocialRegister.social_type
                        Utilities.showHud()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute:  {
                        WebServiceSubClass.socialLogin(socialloginModel: socialModel, showHud: true, completion: { (json, status, response,isUserExist) in
                            Utilities.hideHud()
                            if(status)
                            {
                                AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_registerNow)
                                let loginModel = userInforForCustomer.init(fromJson: json)
                                let loginModelDetails = loginModel
                                SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                                SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                                SingletonClass.sharedInstance.loginForCustomer = loginModelDetails
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                                userDefault.setValue(loginModelDetails.profile.apiKey , forKey: UserDefaultsKey.X_API_KEY.rawValue)
                                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                                userDefault.setUserDataForCustomer(objProfile: loginModelDetails)
                                appDel.navigateToHomeCustomer()
                                
                            } else if (isUserExist) {
                                if json["message"] == nil {
                                    Utilities.displayErrorAlert(response as? String ?? "")
                                } else {
                                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                                }
                                
                            }
                            
                        })
                    })
                       
                        //appDel.navigateToHomeCustomer()
                        //appDel.navigateToUserLogin()
                        
                  //  }, acceptTitle: "OK")
                } else {
                    Utilities.hideHud()
                    Utilities.displayAlert(AppName, message: json["message"].string ?? "", completion: {_ in
                        appDel.navigateToUserLogin()
                        
                    }, acceptTitle: "OK")
                }
                
                
                //ShowAlert(OfMessage: json["message"].string ?? "")
               
               
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    
    
}
extension RegisterViewController:UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if textField == textFieldPhone {
            
            let charsLimit = 20

               let startingLength = textField.text?.count ?? 0
               let lengthToAdd = string.count
               let lengthToReplace =  range.length
               let newLength = startingLength + lengthToAdd - lengthToReplace

               return newLength <= charsLimit
            

        } else if textField == textFieldFullName {
            let charsLimit = NameTotalCount
            
            let startingLength = textField.text?.count ?? 0
            let lengthToAdd = string.count
            let lengthToReplace =  range.length
            let newLength = startingLength + lengthToAdd - lengthToReplace
            
            return newLength <= charsLimit
        } else {
            let newText = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
            let numberOfChars = newText.count
            if textField.tag == 3 || textField.tag == 4{
                return numberOfChars < 15
            }else{
                return numberOfChars < 50
            }
        }
       
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
//extension RegisterViewController : GIDSignInDelegate {
//   func signInWillDispatch(signIn: GIDSignIn!, error: Error!)
//   {
//       // myActivityIndicator.stopAnimating()
//   }
//
//   // Present a view that prompts the user to sign in with Google
//   func sign(_ signIn: GIDSignIn!,
//             present viewController: UIViewController!) {
//       UIApplication.shared.statusBarStyle = .default
//       self.present(viewController, animated: true, completion: nil)
//   }
//
//   // Dismiss the "Sign in with Google" view
//   func sign(_ signIn: GIDSignIn!,
//             dismiss viewController: UIViewController!)
//   {
//       UIApplication.shared.statusBarStyle = .lightContent
//       self.dismiss(animated: true, completion: nil)
//   }
//
//   func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
//   {
//
//       if (error == nil)
//       {
//           // Perform any operations on signed in user here.
//           let userId : String = user.userID // For client-side use only!
//           let firstName : String  = user.profile.givenName
//           let lastName : String  = user.profile.familyName
//           let email : String = user.profile.email
//
//
//
//           var dictUserData = [String: AnyObject]()
//
//
//           dictUserData["Firstname"] = firstName as AnyObject
//           dictUserData["Lastname"] = lastName as AnyObject
//           dictUserData["Email"] = email as AnyObject
//           dictUserData["MobileNo"] = "" as AnyObject
//           //dictUserData["Lat"] = "\(SingletonClass.sharedInstance.latitude)" as AnyObject
//          // dictUserData["Lng"] = "\(SingletonClass.sharedInstance.longitude)" as AnyObject
//           dictUserData["SocialId"] = "\(userId)" as AnyObject
//           dictUserData["SocialType"] = "Google" as AnyObject
//           dictUserData["Token"] = SingletonClass.sharedInstance.DeviceToken as AnyObject
//           dictUserData["DeviceType"] = "1" as AnyObject
//        socialID = userId
//        socialType = "Google"
//
//
//        textFieldFullName.text = "\(dictUserData["Firstname"] as! String) \(dictUserData["Lastname"] as! String)"
//        print("\(dictUserData["Firstname"] as! String) \(dictUserData["Lastname"] as! String)")
//        textFieldEmail.text = email
//        textFieldPhone.text = dictUserData["MobileNo"] as? String
//        textFieldPassword.text = "12345678"
//        textFieldConfirmPassoword.text = "12345678"
//        textFieldDOB.text = ""
//
//        textFieldPassword.superview?.isHidden = true
//        textFieldConfirmPassoword.superview?.isHidden = true
//
//           print(dictUserData)
//
//
//       }
//       else
//       {
//           print("\(error.localizedDescription)")
//       }
//
//   }
//}
extension RegisterViewController {
    func getFBUserData()
    {
        
        //        Utilities.showActivityIndicator()
        
        var parameters = [AnyHashable: Any]()
        parameters["fields"] = "first_name, last_name, picture, email,id"
        
        GraphRequest.init(graphPath: "me", parameters: parameters as! [String : Any]).start { [self] (connection, result, error) in
            if error == nil
            {
                let dictData = result as! [String : AnyObject]
                let strFirstName : String = dictData["first_name"] as? String ?? ""
                 let strLastName : String = dictData["last_name"] as? String ?? ""
                 let strEmail : String = dictData["email"] as? String ?? ""
                 let strUserId = String(describing: dictData["id"])
             
                let imgUrl = ((dictData["picture"] as! [String:AnyObject])["data"]  as! [String:AnyObject])["url"] as? String
            
                var strFullName = ""
                
                var dictUserData = [String: AnyObject]()
                
                dictUserData["Firstname"] = strFirstName as AnyObject
                dictUserData["Lastname"] = strLastName as AnyObject
                dictUserData["Email"] = strEmail as AnyObject
                dictUserData["MobileNo"] = "" as AnyObject
              
                dictUserData["SocialId"] = strUserId as AnyObject
                dictUserData["SocialType"] = "Facebook" as AnyObject
                
                dictUserData["DeviceType"] = "1" as AnyObject
                
                socialID = strUserId
                socialType = "Facebook"
                   
                
                textFieldFullName.text = "\(strFirstName) \(strLastName)"

                textFieldEmail.text = strEmail
                textFieldPhone.text = dictUserData["MobileNo"] as? String
                textFieldPassword.text = "12345678"
                textFieldConfirmPassoword.text = "12345678"
                textFieldDOB.text = ""
                
                textFieldPassword.superview?.isHidden = true
                textFieldConfirmPassoword.superview?.isHidden = true
            }
        }
    }
}
extension RegisterViewController : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    //For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    // ASAuthorizationControllerDelegate function for authorization failed
  @available(iOS 13.0, *)
     func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
         print(error.localizedDescription)
     }
     
     @available(iOS 13.0, *)
     // ASAuthorizationControllerDelegate function for successful authorization
     func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
         if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let appleId : String = appleIDCredential.user 
            let appleUserFirstName : String = appleIDCredential.fullName?.givenName ?? ""
            let appleUserLastName : String = appleIDCredential.fullName?.familyName ?? ""
             let appleUserEmail : String = appleIDCredential.email ?? ""
           
            
            if appleUserEmail != "" && appleUserFirstName != "" && appleUserLastName != "" {
                let appleReqModel = appleDetailsReqModel()
                appleReqModel.apple_id = appleId
                appleReqModel.first_name = appleUserFirstName
                appleReqModel.last_name = appleUserLastName
                appleReqModel.email = appleUserEmail
                Utilities.showHud()
                WebServiceSubClass.appleLogin(CategoryModel: appleReqModel, completion: { (json, status, response) in
                    Utilities.hideHud()
                    if(status)
                    {
                        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_registerNow)
                        let loginModel = AppleDetailsResModel.init(fromJson: json)
                        self.socialID = loginModel.appleDetail.appleId
                        self.socialType = "Apple"
                        
                        self.textFieldFullName.text = "\(loginModel.appleDetail.firstName ?? "") \(loginModel.appleDetail.lastName ?? "")"
                        
                        self.textFieldEmail.text = loginModel.appleDetail.email
                        self.textFieldPhone.text = ""
                        self.textFieldPassword.text = "12345678"
                        self.textFieldConfirmPassoword.text = "12345678"
                        self.textFieldDOB.text = ""
                         
                        self.textFieldPassword.superview?.isHidden = true
                        self.textFieldConfirmPassoword.superview?.isHidden = true
                        
                    }
                    else
                    {
                        if json["message"] == nil {
                            Utilities.displayErrorAlert(response as? String ?? "")
                        } else {
                            Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                        }
                    }
                })
            } else {
                let appleReqModel = appleDetailsReqModel()
                appleReqModel.apple_id = appleId
                appleReqModel.first_name = appleUserFirstName
                appleReqModel.last_name = appleUserLastName
                appleReqModel.email = appleUserEmail
                Utilities.showHud()
                WebServiceSubClass.appleLogin(CategoryModel: appleReqModel, completion: { (json, status, response) in
                    Utilities.hideHud()
                    if(status)
                    {
                        let loginModel = AppleDetailsResModel.init(fromJson: json)
                        self.socialID = loginModel.appleDetail.appleId
                        self.socialType = "Apple"
                        
                        self.textFieldFullName.text = "\(loginModel.appleDetail.firstName ?? "") \(loginModel.appleDetail.lastName ?? "")"
                        
                        self.textFieldEmail.text = loginModel.appleDetail.email
                        self.textFieldPhone.text = ""
                        self.textFieldPassword.text = "12345678"
                        self.textFieldConfirmPassoword.text = "12345678"
                        self.textFieldDOB.text = ""
                         
                        self.textFieldPassword.superview?.isHidden = true
                        self.textFieldConfirmPassoword.superview?.isHidden = true
                    }
                    else
                    {
                        if json["message"] == nil {
                            Utilities.displayErrorAlert(response as? String ?? "")
                        } else {
                            Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                        }
                    }
                })
            }
            
//             self.checkAppleId(credentials: appleIDCredential)
         }
         else if let passwordCredential = authorization.credential as? ASPasswordCredential {
             let appleUsername = passwordCredential.user
             let applePassword = passwordCredential.password
             
             print(appleUsername, applePassword)
         }
     }
}

extension RegisterViewController: GeneralPickerViewDelegate {
    
    func didTapDone() {
        let item = SingletonClass.sharedInstance.CountryNameWithCode[pickerViewForAll.selectedRow(inComponent: 0)]
        self.TextFieldSelectCountryCode.text = item.dialCode
        self.TextFieldSelectCountryCode.resignFirstResponder()
       
        
    }
    
    func didTapCancel() {
        //self.endEditing(true)
    }
}
extension RegisterViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return SingletonClass.sharedInstance.CountryNameWithCode.count
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return SingletonClass.sharedInstance.CountryNameWithCode[row].name
        
        
    }
    
}
