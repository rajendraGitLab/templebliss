//
//  EnterOTPViewController.swift
//  TempleBliss
//
//  Created by Apple on 23/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class EnterOTPViewController: BaseViewController {

    //MARK: - Properties
    var objectRegister = RegisterReqModel()
    var strOtp = ""
    //MARK: - IBOutlets
    @IBOutlet weak var btnOTP: EnterOTPButton!
    @IBOutlet weak var textFieldOtp: EnterOTPTextField!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLocalization()
        setValue()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        //ToCreateAAccount(Count: 50)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if strOtp != "" {
            var secondsRemaining = 30
            self.btnOTP.isEnabled = false
            Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                
                if secondsRemaining > 1 {
                    UIView.performWithoutAnimation {
                        //self.btnOTP.setTitle("Resend after \(secondsRemaining) seconds", for: .normal)
                        self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) seconds", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        secondsRemaining -= 1
                        self.btnOTP.layoutIfNeeded()
                    }
                
                } else if secondsRemaining == 1 {
                    UIView.performWithoutAnimation {
                       // self.btnOTP.setTitle("Resend after \(secondsRemaining) second", for: .normal)
                        self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) second", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        secondsRemaining -= 1
                        self.btnOTP.layoutIfNeeded()
                    }
                } else {
                    UIView.performWithoutAnimation {
                        
                        self.btnOTP.setunderline(title: "Resend OTP", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                        self.btnOTP.isEnabled = true
                        Timer.invalidate()
                    }
                    
                }
            }
        }
    }
    
    //MARK: - other methods
    func setLocalization() {
        
    }
    func setValue() {
    }
    func validation() -> Bool{
        if textFieldOtp.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            Utilities.ShowAlert(OfMessage: "Please enter OTP")
            
                return false
          
        } else {
            if strOtp != textFieldOtp.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""{
                Utilities.ShowAlert(OfMessage: ErrorMessages.Invelid_Otp.rawValue)
                return false
            }
        }
        return true
    }
    //MARK: - IBActions
    @IBAction func btnResendOtp(_ sender: Any) {
        
        webserviceForResendOtp()
    }
    
    @IBAction func btnVerifyClick(_ sender: Any) {
//        for controller in self.navigationController!.viewControllers as Array {
//            if controller.isKind(of: LoginViewController.self) {
//                self.navigationController!.popToViewController(controller, animated: true)
//                break
//            }
//        }
        if validation(){
            webserviceForRegisterOtp()
        }
    }
    //MARK: - API Calls
    func webserviceForRegisterOtp(){
        Utilities.showHud()
        WebServiceSubClass.register(registerModel:objectRegister, showHud: true, completion: { (json, status, response) in
            //Utilities.hideHud()
            if(status)
            {
                if self.objectRegister.social_id != "" && self.objectRegister.social_type != "" {
                 //   Utilities.displayAlert(AppName, message: json["message"].string ?? "", completion: {_ in
                        let socialModel = userSocialData()
                        
                        socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                        socialModel.device_type = ReqDeviceType
                        socialModel.social_id = self.objectRegister.social_id
                        socialModel.user_name = self.objectRegister.email
                        socialModel.social_type = self.objectRegister.social_type
                        Utilities.showHud()
                        WebServiceSubClass.socialLogin(socialloginModel: socialModel, showHud: true, completion: { (json, status, response,isUserExist) in
                            Utilities.hideHud()
                            if(status)
                            {
                                let loginModel = userInforForCustomer.init(fromJson: json)
                                let loginModelDetails = loginModel
                                SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                                SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                                SingletonClass.sharedInstance.loginForCustomer = loginModelDetails
                                userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                                userDefault.setValue(loginModelDetails.profile.apiKey , forKey: UserDefaultsKey.X_API_KEY.rawValue)
                                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                                userDefault.setUserDataForCustomer(objProfile: loginModelDetails)
                                appDel.navigateToHomeCustomer()
                                
                            } else if (isUserExist) {
                                if json["message"] == nil {
                                    Utilities.displayErrorAlert(response as? String ?? "")
                                } else {
                                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                                }
                                
                            }
                            
                        })
                        //appDel.navigateToHomeCustomer()
                        //appDel.navigateToUserLogin()
                        
                  //  }, acceptTitle: "OK")
                } else {
                    Utilities.hideHud()
                    Utilities.displayAlert(AppName, message: json["message"].string ?? "", completion: {_ in
                        appDel.navigateToUserLogin()
                        
                    }, acceptTitle: "OK")
                }
                
                
                //ShowAlert(OfMessage: json["message"].string ?? "")
               
               
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    func webserviceForResendOtp(){
        Utilities.showHud()
        WebServiceSubClass.registerOtp(registerOtpModel: objectRegister, showHud: false, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                
                let otpModel = RegisterOtpResModel.init(fromJson: json)
                self.strOtp = otpModel.otp
                self.textFieldOtp.text = ""
                
                Utilities.ShowAlert(OfMessage: "OTP sent successfully")
                
                var secondsRemaining = 30
                self.btnOTP.isEnabled = false
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                    if secondsRemaining > 1 {
                        UIView.performWithoutAnimation {
                            self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) seconds", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            secondsRemaining -= 1
                            self.btnOTP.layoutIfNeeded()
                        }
                    
                    } else if secondsRemaining == 1 {
                        UIView.performWithoutAnimation {
                            self.btnOTP.setWithOutunderline(title: "Resend after \(secondsRemaining) second", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            secondsRemaining -= 1
                            self.btnOTP.layoutIfNeeded()
                        }
                    } else {
                        UIView.performWithoutAnimation {
                            
                            self.btnOTP.setunderline(title: "Resend OTP", color: colors.white.value, font: CustomFont.regular.returnFont(11))
                            self.btnOTP.isEnabled = true
                            Timer.invalidate()
                        }
                        
                    }
                   }
                //appDel.navigateToHomeAdviser()
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
    func ToCreateAAccount(Count:Int){
        objectRegister.full_name = "Dummy\(Count)"
        objectRegister.email = "dummy\(Count)@yopmail.com"
        objectRegister.phone = "+911111\(Count)222233"
        Utilities.showHud()
        WebServiceSubClass.register(registerModel:objectRegister, showHud: true, completion: { (json, status, response) in
            //Utilities.hideHud()
            if(status)
            {
                if Count == 100 {
                    
                } else {
                    self.ToCreateAAccount(Count: Count + 1)
                }
                
                
                //ShowAlert(OfMessage: json["message"].string ?? "")
               
               
            }
            else
            {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
            }
        })
    }
}
