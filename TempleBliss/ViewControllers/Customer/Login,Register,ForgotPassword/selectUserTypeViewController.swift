//
//  selectUserTypeViewController.swift
//  TempleBliss
//
//  Created by Apple on 22/12/20.
//  Copyright © 2020 EWW071. All rights reserved.
//

import UIKit

class selectUserTypeViewController: BaseViewController,UINavigationControllerDelegate {

    //MARK: - Properties
    //MARK: - IBOutlets
    @IBOutlet weak var lblTempleBliss: SplashScreenLabel!
    @IBOutlet weak var imgSplashScreen: UIImageView!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stactViewBottom: NSLayoutConstraint!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //lblTempleBliss.animationType =
        imgSplashScreen.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
       
            UIView.animate(withDuration: 1.50, animations: {[self] in
                imgSplashScreen.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            }, completion: {finished in
                
                if userDefault.bool(forKey: UserDefaultsKey.isUserSkip.rawValue) {
                    
//                    self.stackView.isHidden = true
//                    self.stactViewBottom.constant = 35
//                    UIView.animate(withDuration: 0.6, animations: {
//                        self.view.layoutIfNeeded()
//                        //stackView.layoutIfNeeded()
//                    })
//                    let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
//
//                    let nav = UINavigationController(rootViewController: controller)
//                    nav.navigationBar.isHidden = true
//                    self.window?.rootViewController = nav
                    //self.setUpNavigation()
                } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsAdviser.rawValue) == true) {
//                    self.stackView.isHidden = true
//                    self.stactViewBottom.constant = 35
//                    UIView.animate(withDuration: 0.6, animations: {
//                        self.view.layoutIfNeeded()
//                        //stackView.layoutIfNeeded()
//                    })
                    
//                    let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
//
//                    let nav = UINavigationController(rootViewController: controller)
//                    nav.navigationBar.isHidden = true
//                    self.window?.rootViewController = nav

                } else if (userDefault.bool(forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue) == true) {
//                    self.stackView.isHidden = true
//                    self.stactViewBottom.constant = 35
//                    UIView.animate(withDuration: 0.6, animations: {
//                        self.view.layoutIfNeeded()
//                        //stackView.layoutIfNeeded()
//                    })
//                    let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: SplashVC.storyboardID) as! SplashVC
//
//                    let nav = UINavigationController(rootViewController: controller)
//                    nav.navigationBar.isHidden = true
//                    self.window?.rootViewController = nav
                } else {
                    self.stackView.isHidden = false
                    self.stactViewBottom.constant = 35
                    UIView.animate(withDuration: 0.6, animations: {
                        self.view.layoutIfNeeded()
                        //stackView.layoutIfNeeded()
                    })
                }
            })
     
        
        //self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        setLocalization()
        self.navigationController?.navigationBar.isHidden = false
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        //self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
            return true
        }
    //MARK: - other methods
    func setLocalization() {
        
    }
    //MARK: - IBActions
    
    @IBAction func newCustomerClick(_ sender: Any) {
        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.imCustomer)
        let controller:LoginViewController = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: LoginViewController.storyboardID) as! LoginViewController
        SingletonClass.sharedInstance.usertype = "3"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func IAmAdviserClick(_ sender: Any) {
//        AppDelegate.firebaseLogEvent(name: .imAdvisor)
        let controller = AppStoryboard.AdviserLogin.instance.instantiateViewController(withIdentifier: AdviserLoginViewController.storyboardID)
        SingletonClass.sharedInstance.usertype = "2"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    //MARK: - API Calls
    
    

}
