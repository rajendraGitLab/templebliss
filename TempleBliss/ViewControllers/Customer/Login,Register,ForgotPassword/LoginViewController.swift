 //
 //  LoginViewController.swift
 //  ApiStructureModule
 //
 //  Created by EWW071 on 14/03/20.
 //  Copyright © 2020 EWW071. All rights reserved.
 //
 
 import UIKit
 import FBSDKLoginKit
 import FBSDKCoreKit
 import GoogleSignIn
 import AuthenticationServices
 class LoginViewController: BaseViewController {
    
    
    //MARK: - Properties
    var iscustomer : Bool = false
    var user_SocialData = userSocialData()
    var socialID = ""
    var socialType = ""
    //MARK: - IBOutlets
    @IBOutlet weak var mainScrollView: viewViewClearBG!
    @IBOutlet weak var lblLogin: loginPageLabel!
    @IBOutlet weak var lblEnterCredentials: loginPageLabel!
    @IBOutlet weak var textFieldEMail: loginPageTextField!
    @IBOutlet weak var textFieldPassword: loginPageTextField!
    @IBOutlet weak var btnForgotPassword: LoginScreenButon!
    @IBOutlet weak var btnLoginNow: theamSubmitButton!
    @IBOutlet weak var lblNewUser: loginPageLabel!
    @IBOutlet weak var btnSignUp: LoginScreenButon!
    //MARK: - View Life Cycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        GetCountryCodeList()
        setLocalization()
        self.navigationController?.navigationBar.isHidden = false
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.back.value, rightImages: [NavItemsRight.skip.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")
        
        backButtonClick = {
            appDel.navigateToLogin()
        }
        
        //        if UIDevice.current.name == "iPhone 013" || UIDevice.current.name == "iPhone 016" {
        //
        //            textFieldEMail.text = "vansh@yopmail.com"
        //            textFieldPassword.text = "12345678"
        //        } else {
        //            textFieldEMail.text = ""
        //            textFieldPassword.text = ""
        //        }
        //
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        SingletonClass.sharedInstance.usertype = "3"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            let topPadding = window?.safeAreaInsets.top ?? 0.0
            let bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
            
            if mainScrollView.frame.size.height > UIScreen.main.bounds.height - topPadding - bottomPadding {
                (mainScrollView.superview as! UIScrollView).isScrollEnabled = true
                // mainScrollView.isScrollEnabled = true
            } else {
                (mainScrollView.superview as! UIScrollView).isScrollEnabled = false
            }
        }
    }
    
    //MARK: - other methods
    func setLocalization() {
        lblLogin.text = "Login To Continue"
        lblEnterCredentials.text = "Enter your credentials to continue with the app"
        textFieldEMail.placeholder = "Email Address"
        textFieldPassword.placeholder = "Password"
        btnForgotPassword.setTitle("Forgot Password?", for:.normal)
        btnLoginNow.setTitle("login now".uppercased(), for: .normal)
        lblNewUser.text = "New User? Click Here to"
        btnSignUp.setTitle("Sign Up", for: .normal)
        
        //textFieldEMail.text = "johndoe@gmail.com"
        
    }
    //MARK: - IBActions
    @IBAction func btnSignUpClick(_ sender: Any) {
        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_signUp)
        textFieldEMail.text = ""
        textFieldPassword.text = ""
        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: RegisterViewController.storyboardID) as! RegisterViewController
        controller.FullNameString = ""
        controller.emailString = ""
        controller.socialID = ""
        controller.socialType = ""
        SingletonClass.sharedInstance.usertype = "3"
        userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func btnFBClicked(_ sender: UIButton)
    {
        if !WebService.shared.isConnected {
            
            Utilities.ShowAlert(OfMessage: "Please check your internet")
            
            return
        }
        let login = LoginManager()
        
        login.logIn(permissions: ["public_profile","email"], from: self) { (result, error) in
            
            if error != nil {
                //                UIApplication.shared.statusBarStyle = .lightContent
            }
            else if (result?.isCancelled)! {
                //                UIApplication.shared.statusBarStyle = .lightContent
            }else {
                if (result?.grantedPermissions.contains("email"))! {
                    //                    UIApplication.shared.statusBarStyle = .lightContent
                    self.getFBUserData()
                }else {
                    print("you don't have permission")
                }
            }
        }
    }
    @IBAction func btnForgotPasswordClick(_ sender: Any) {
        textFieldEMail.text = ""
        textFieldPassword.text = ""
        let controller = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: ForgotPasswordViewController.storyboardID)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func btnLoginClick(_ sender: Any) {
        
        if validation(){
            webserviceForlogin()
        }
        
    }
    @IBAction func btnGoogleSignClick(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let signInConfig = GIDConfiguration.init(clientID: kGoogle_Client_ID)

        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            guard let user = user else { return }
            
            if let profiledata = user.profile {
                // Perform any operations on signed in user here.
                let userId : String = user.userID ?? "" // For client-side use only!
                let firstName : String  = profiledata.givenName ?? ""
                let lastName : String  = profiledata.familyName ?? ""
                let email : String = profiledata.email
                
                var strFullName = ""
                //let phoneNo : String = user
                strFullName = "\(firstName) \(lastName)"
                
                var dictUserData = [String: AnyObject]()
                
                dictUserData["Firstname"] = firstName as AnyObject
                dictUserData["Lastname"] = lastName as AnyObject
                dictUserData["Email"] = email as AnyObject
                dictUserData["MobileNo"] = "" as AnyObject
                //dictUserData["Lat"] = "\(SingletonClass.sharedInstance.latitude)" as AnyObject
                // dictUserData["Lng"] = "\(SingletonClass.sharedInstance.longitude)" as AnyObject
                dictUserData["SocialId"] = "\(userId)" as AnyObject
                dictUserData["SocialType"] = "Google" as AnyObject
                dictUserData["Token"] = SingletonClass.sharedInstance.DeviceToken as AnyObject
                dictUserData["DeviceType"] = "1" as AnyObject
                self.socialID = userId
                self.socialType = "Google"
                
                let socialModel = userSocialData()
                socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                socialModel.device_type = ReqDeviceType
                socialModel.social_id = self.socialID
                socialModel.user_name = email
                socialModel.social_type = self.socialType
                
                self.user_SocialData = socialModel
                
                self.webservice_SocialLoginCheck(email: email, FullName: strFullName)
                print(dictUserData)
            }
          }
        
    }
    
    @IBAction func actionHandleAppleSignin(_ sender: Any) {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        }
    }
    
    
    //MARK: - API Calls
    func webserviceForlogin()
    {
        let login = LoginReqModel()
        login.user_name = textFieldEMail.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        login.password = textFieldPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        login.device_token = SingletonClass.sharedInstance.DeviceToken
        login.device_type = ReqDeviceType
        login.role = SingletonClass.sharedInstance.usertype
        //        login.add
        Utilities.showHud()
        WebServiceSubClass.login(loginModel: login, showHud: true, completion: { (json, status, response) in
            Utilities.hideHud()
            if(status)
            {
                AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_login)
                let loginModel = userInforForCustomer.init(fromJson: json)
                let loginModelDetails = loginModel
                //loginModelDetails.profile.QuickTour = "1"
                SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                SingletonClass.sharedInstance.loginForCustomer = loginModelDetails
                userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                userDefault.setValue(loginModelDetails.profile.apiKey , forKey: UserDefaultsKey.X_API_KEY.rawValue)
                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                userDefault.setUserDataForCustomer(objProfile: loginModelDetails)
                appDel.navigateToHomeCustomer()
                
            }
            else
            {
                
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
                
            }
        })
    }
    func webservice_SocialLoginCheck(email:String,FullName : String){
        
        Utilities.showHud()
        WebServiceSubClass.socialLogin(socialloginModel: user_SocialData, showHud: true, completion: { (json, status, response,isUserExist) in
            Utilities.hideHud()
            if(status)
            {
                AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_login)
                let loginModel = userInforForCustomer.init(fromJson: json)
                let loginModelDetails = loginModel
                SingletonClass.sharedInstance.UserId = loginModelDetails.profile.id ?? ""
                SingletonClass.sharedInstance.Api_Key = loginModelDetails.profile.apiKey ?? ""
                SingletonClass.sharedInstance.loginForCustomer = loginModelDetails
                userDefault.setValue(false, forKey: UserDefaultsKey.isUserSkip.rawValue)
                userDefault.setValue(loginModelDetails.profile.apiKey , forKey: UserDefaultsKey.X_API_KEY.rawValue)
                userDefault.setValue(true, forKey: UserDefaultsKey.isUserLoginAsCustomer.rawValue)
                userDefault.setUserDataForCustomer(objProfile: loginModelDetails)
                appDel.navigateToHomeCustomer()
                
            } else if (isUserExist) {
                if json["message"] == nil {
                    Utilities.displayErrorAlert(response as? String ?? "")
                } else {
                    Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                }
                
            }
            else
            {
                let controller : RegisterViewController  = AppStoryboard.Login.instance.instantiateViewController(withIdentifier: RegisterViewController.storyboardID) as! RegisterViewController
                controller.emailString = email
                controller.FullNameString = FullName
                controller.socialID = self.socialID
                controller.socialType = self.socialType
                SingletonClass.sharedInstance.usertype = "3"
                userDefault.setValue(SingletonClass.sharedInstance.usertype, forKey: UserDefaultsKey.selectedUserType.rawValue)
                self.navigationController?.pushViewController(controller, animated: true)
            }
        })
    }
    func validation() -> Bool
    {
        if textFieldEMail.text?.count == 0 {
            
            Utilities.ShowAlert(OfMessage: "Please enter email")
            return false
        }
        
        let checkEmail = textFieldEMail.validatedText(validationType: ValidatorType.email)
        let checkPassword = textFieldPassword.validatedText(validationType: ValidatorType.password)
        if(!checkEmail.0)
        {
            Utilities.ShowAlert(OfMessage: checkEmail.1)
            return checkEmail.0
        }
        else  if(!checkPassword.0)
        {
            Utilities.ShowAlert(OfMessage: checkPassword.1)
            return checkPassword.0
        }
        return true
    }
    
 }
 class userSocialData : RequestModel {
    var user_name : String = ""
    var device_token : String = ""
    var device_type : String = ReqDeviceType
    var social_type : String = ""
    var social_id : String = ""
    
 }
// extension LoginViewController : GIDSignInDelegate {
//    func signInWillDispatch(signIn: GIDSignIn!, error: Error!)
//    {
//        // myActivityIndicator.stopAnimating()
//    }
//    
//    // Present a view that prompts the user to sign in with Google
//    func sign(_ signIn: GIDSignIn!,
//              present viewController: UIViewController!) {
//        UIApplication.shared.statusBarStyle = .default
//        self.present(viewController, animated: true, completion: nil)
//    }
//    
//    // Dismiss the "Sign in with Google" view
//    func sign(_ signIn: GIDSignIn!,
//              dismiss viewController: UIViewController!)
//    {
//        UIApplication.shared.statusBarStyle = .lightContent
//        self.dismiss(animated: true, completion: nil)
//    }
//    
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!)
//    {
//        
//        if (error == nil)
//        {
//            guard let user = user else { return }
//            if let profiledata = user.profile {
//                // Perform any operations on signed in user here.
//                let userId : String = user.userID // For client-side use only!
//                let firstName : String  = profiledata.givenName
//                let lastName : String  = profiledata.familyName
//                let email : String = user.profile.email
//                
//                var strFullName = ""
//                //let phoneNo : String = user
//                strFullName = "\(firstName) \(lastName)"
//                
//                var dictUserData = [String: AnyObject]()
//                
//                
//                dictUserData["Firstname"] = firstName as AnyObject
//                dictUserData["Lastname"] = lastName as AnyObject
//                dictUserData["Email"] = email as AnyObject
//                dictUserData["MobileNo"] = "" as AnyObject
//                //dictUserData["Lat"] = "\(SingletonClass.sharedInstance.latitude)" as AnyObject
//                // dictUserData["Lng"] = "\(SingletonClass.sharedInstance.longitude)" as AnyObject
//                dictUserData["SocialId"] = "\(userId)" as AnyObject
//                dictUserData["SocialType"] = "Google" as AnyObject
//                dictUserData["Token"] = SingletonClass.sharedInstance.DeviceToken as AnyObject
//                dictUserData["DeviceType"] = "1" as AnyObject
//                socialID = userId
//                socialType = "Google"
//                
//                let socialModel = userSocialData()
//                socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
//                socialModel.device_type = ReqDeviceType
//                socialModel.social_id = socialID
//                socialModel.user_name = email
//                socialModel.social_type = socialType
//                
//                user_SocialData = socialModel
//                
//                webservice_SocialLoginCheck(email: email, FullName: strFullName)
//                print(dictUserData)
//            }
//            
//        }
//        else
//        {
//            print("\(error.localizedDescription)")
//        }
//        
//    }
// }
 extension LoginViewController {
    func getFBUserData()
    {
        
        var parameters = [AnyHashable: Any]()
        parameters["fields"] = "first_name, last_name, picture, email,id"
        
        GraphRequest.init(graphPath: "me", parameters: parameters as! [String : Any]).start { [self] (connection, result, error) in
            if error == nil
            {
                let dictData = result as! [String : AnyObject]
                
                let strFirstName : String = dictData["first_name"] as? String ?? ""
                let strLastName : String = dictData["last_name"] as? String ?? ""
                let strEmail : String = dictData["email"] as? String ?? ""
                let strUserId = String(describing: dictData["id"])
                
                let strFullName = ""
                
                var dictUserData = [String: AnyObject]()
                
                dictUserData["Firstname"] = strFirstName as AnyObject
                dictUserData["Lastname"] = strLastName as AnyObject
                dictUserData["Email"] = strEmail as AnyObject
                dictUserData["MobileNo"] = "" as AnyObject
                
                dictUserData["SocialId"] = strUserId as AnyObject
                dictUserData["SocialType"] = "Facebook" as AnyObject
                
                dictUserData["DeviceType"] = "1" as AnyObject
                
                socialID = strUserId
                socialType = "Facebook"
                
                let socialModel = userSocialData()
                socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                socialModel.device_type = ReqDeviceType
                socialModel.social_id = socialID
                socialModel.user_name = strEmail
                socialModel.social_type = socialType
                
                user_SocialData = socialModel
                
                webservice_SocialLoginCheck(email: strEmail, FullName: "\(strFirstName) \(strLastName)")
                print(dictUserData)
                
                
            }
        }
    }
 }
 
 extension LoginViewController : ASAuthorizationControllerDelegate, ASAuthorizationControllerPresentationContextProviding {
    
    //For present window
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
    
    // ASAuthorizationControllerDelegate function for authorization failed
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
    }
    
    @available(iOS 13.0, *)
    // ASAuthorizationControllerDelegate function for successful authorization
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            let appleId:String = appleIDCredential.user
            let appleUserFirstName:String = appleIDCredential.fullName?.givenName ?? ""
            let appleUserLastName:String = appleIDCredential.fullName?.familyName ?? ""
            let appleUserEmail:String = appleIDCredential.email ?? ""
            
            socialID = appleId
            socialType = "Apple"
            
            
            if appleUserEmail != "" && appleUserFirstName != "" && appleUserLastName != "" {
                let appleReqModel = appleDetailsReqModel()
                appleReqModel.apple_id = appleId
                appleReqModel.first_name = appleUserFirstName
                appleReqModel.last_name = appleUserLastName
                appleReqModel.email = appleUserEmail
                Utilities.showHud()
                WebServiceSubClass.appleLogin(CategoryModel: appleReqModel, completion: { (json, status, response) in
                    if(status)
                    {
                        AppDelegate.firebaseLogEvent(name: AnalyticsEvents.C_login)
                        let loginModel = AppleDetailsResModel.init(fromJson: json)
                        self.socialID = loginModel.appleDetail.appleId
                        self.socialType = "Apple"
                        
                        let socialModel = userSocialData()
                        socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                        socialModel.device_type = ReqDeviceType
                        socialModel.social_id = self.socialID
                        socialModel.user_name = loginModel.appleDetail.email
                        socialModel.social_type = self.socialType
                        self.user_SocialData = socialModel
                        self.webservice_SocialLoginCheck(email: appleUserEmail, FullName: "\(loginModel.appleDetail.firstName ?? "") \(loginModel.appleDetail.lastName ?? "")" )
                    }
                    else
                    {
                        Utilities.hideHud()
                        if json["message"] == nil {
                            Utilities.displayErrorAlert(response as? String ?? "")
                        } else {
                            Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                        }
                    }
                })
            } else {
                let appleReqModel = appleDetailsReqModel()
                appleReqModel.apple_id = appleId
                appleReqModel.first_name = appleUserFirstName
                appleReqModel.last_name = appleUserLastName
                appleReqModel.email = appleUserEmail
                Utilities.showHud()
                WebServiceSubClass.appleLogin(CategoryModel: appleReqModel, completion: { (json, status, response) in
                    if(status)
                    {
                        let loginModel = AppleDetailsResModel.init(fromJson: json)
                        
                        self.socialID = loginModel.appleDetail.appleId
                        self.socialType = "Apple"
                        
                        let socialModel = userSocialData()
                        socialModel.device_token = SingletonClass.sharedInstance.DeviceToken
                        socialModel.device_type = ReqDeviceType
                        socialModel.social_id = self.socialID
                        socialModel.user_name = loginModel.appleDetail.email
                        socialModel.social_type = self.socialType
                        self.user_SocialData = socialModel
                        self.webservice_SocialLoginCheck(email: loginModel.appleDetail.email, FullName: "\(loginModel.appleDetail.firstName ?? "") \(loginModel.appleDetail.lastName ?? "")" )
                        
                    }
                    else
                    {
                        Utilities.hideHud()
                        if json["message"] == nil {
                            Utilities.displayErrorAlert(response as? String ?? "")
                        } else {
                            Utilities.displayErrorAlert(json["message"].string ?? "Something went wrong")
                        }
                    }
                })
            }
            
        }
        else if let passwordCredential = authorization.credential as? ASPasswordCredential {
            let appleUsername = passwordCredential.user
            let applePassword = passwordCredential.password
            
            print(appleUsername, applePassword)
        }
    }
 }
 extension LoginViewController {
    func GetCountryCodeList() {
        WebServiceSubClass.GetCountryCode(strParams: "", showHud: false, completion: { (json, status, error) in
            if status {
                
                let CodeData = CountryListWithCode.init(fromJson: json)
                SingletonClass.sharedInstance.CountryNameWithCode = CodeData.countryCode
                
            } else {
                Utilities.displayAlert(AppName, message: error as? String ?? "Something went wrong")
            }
            
            
        })
    }
 }
