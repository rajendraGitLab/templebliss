//
//  CustomerVideoCallViewController.swift
//  twilioDemo
//
//  Created by Apple on 15/03/21.
//


import UIKit
import TwilioVideo
import CallKit
import AVFoundation
import SwiftyJSON
import os.log

class CustomerVideoCallViewController: BaseViewController {
    
    // MARK:- IBOutlets
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var messageLabel: UILabel?
    @IBOutlet weak var roomTextField: UITextField?
    @IBOutlet weak var previewView: VideoView?
    @IBOutlet weak var disconnectButton: UIButton?
    @IBOutlet weak var lblName: UILabel?
    @IBOutlet weak var lblPhoneNo: UILabel?
    @IBOutlet weak var lblTimer: UILabel?
    @IBOutlet weak var viewDecline: UIView?
    
    @IBOutlet weak var lblDeclineText: UILabel?
    @IBOutlet weak var WidthConstraint: NSLayoutConstraint?
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var trailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var BottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var LeadingConstraint: NSLayoutConstraint!
    
    var CallRejectByAdvisor = false
    var isFromAppDelegate = Bool()
    var count = 1

    // MARK:- Video call related custom variables
    var SecondsForReminderScreen = 15 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var CommunicationScreenRemiderScreen = 15
    var TimerForReminderScreen = Timer()

    var totalMinutesTimer = 0

    var internalTimer: Timer?

    var totalMinutes = "1"


    var TotalTimecounter = 0


  


    var receiverId : String?
    var strRoomName : String?
    var deviceToken : String?
    var is_rejected: Int?
    var isPickedUp: Int?
    var senderId : String?
    //    var roomName : String?
    var senderName : String?
    var CallingTo : String?
    var token: String?
    var visitID : String?

    var timer : Timer?
    // MARK:- View Controller Members

    // Configure access token manually for testing, if desired! Create one manually in the console
    // at https://www.twilio.com/console/video/runtime/testing-tools
    var accessToken = ""

    // Video SDK components
    var room: Room?
    /**
     * We will create an audio device and manage it's lifecycle in response to CallKit events.
     */
    var audioDevice: DefaultAudioDevice = DefaultAudioDevice()
    var camera: CameraSource?
    var localVideoTrack: LocalVideoTrack?
    var localAudioTrack: LocalAudioTrack?
    var remoteParticipant: RemoteParticipant?
    var remoteView: VideoView?


    // CallKit components
    var callKitProvider: CXProvider? = nil
    var callKitCallController: CXCallController? = nil
    var callKitCompletionHandler: ((Bool)->Swift.Void?)? = nil
    var userInitiatedDisconnect: Bool = false
    
    required init?(coder aDecoder: NSCoder) {
        let configuration = CXProviderConfiguration(localizedName: "TempleBliss")
        configuration.maximumCallGroups = 1
        configuration.maximumCallsPerCallGroup = 1
        
        configuration.supportsVideo = true
        configuration.includesCallsInRecents = false
        configuration.supportedHandleTypes = [.generic]
        if let callKitIcon = UIImage(named: CallKitIconName) {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }
        if let callKitIcon = UIImage(named: "iconMask80") {
            configuration.iconTemplateImageData = callKitIcon.pngData()
        }
        
        self.callKitProvider = CXProvider(configuration: configuration)
        self.callKitCallController = CXCallController()
        
        super.init(coder: aDecoder)
        
        self.callKitProvider?.setDelegate(self, queue: nil)
    }
    
    deinit {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        self.callKitProvider?.invalidate()
        //         self.audioDevice.isEnabled = true
        self.localVideoTrack = nil
        self.localAudioTrack = nil
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
    }
    
    func askPermissionIfNeeded() {
        switch AVAudioSession.sharedInstance().recordPermission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                
            })
        case .denied:
            let alert = UIAlertController(title: "Error", message: "Please allow microphone usage from settings", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Open settings", style: .default, handler: { action in
                UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alert, animated: true, completion: nil)
        case .granted:
            break
            
        }
    } 
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblName?.text = self.CallingTo
        askPermissionIfNeeded()
        
        setNavigationBarInViewController(controller: self, naviColor: .clear, naviTitle: NavTitles.none.value, leftImage: NavItemsLeft.none.value, rightImages: [NavItemsRight.none.value], isTranslucent: true, isShowTitleOnTop: false, TopTitleTExt: "TempleBliss", isChatScreen: false, userImage: "")

        TwilioVideoSDK.audioDevice = self.audioDevice;
        if PlatformUtils.isSimulator {
            self.previewView?.removeFromSuperview()
        } else {
            self.startPreview()
        }
        self.disconnectButton?.isHidden = true
        self.roomTextField?.autocapitalizationType = .none
        self.roomTextField?.delegate = self
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(CustomerVideoCallViewController.callRejection(_:)), userInfo: nil, repeats: true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(CustomerVideoCallViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        ONVideoCall()
        ONForRejectVideoCall()
        pickupCall()
    }
    override func viewDidDisappear(_ animated: Bool) {
        if CallRejectByAdvisor {
            DispatchQueue.main.async {
                Utilities.ShowAlert(OfMessage: RequestRejectMessage)
            }
        }
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCall.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedByReceiver.rawValue)
        SocketIOManager.shared.socket.off(socketApiKeys.PickupCall.rawValue)
        
        self.timer?.invalidate()
        self.stopTimerOnEndSession()
        self.room?.disconnect()
        if let room = self.room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Customer Video Call Attempting to disconnect from room \(room.name)")
            self.userInitiatedDisconnect = true
            self.performEndCallAction(uuid: uuid)
        }
        print("Video call screen go back")
        self.goBack()
       // self.navigationController?.navigationBar.isHidden = false
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        viewDecline?.layer.cornerRadius = (viewDecline?.frame.size.width ?? 0) / 2
        viewDecline?.layer.masksToBounds = true
    }
    
    @objc func flipCamera1() {
        var newDevice: AVCaptureDevice?
        
        if let camera = self.camera, let captureDevice = camera.device {
            if captureDevice.position == .front {
                newDevice = CameraSource.captureDevice(position: .back)
            } else {
                newDevice = CameraSource.captureDevice(position: .front)
            }
            
            if let newDevice = newDevice {
                camera.selectCaptureDevice(newDevice) { (captureDevice, videoFormat, error) in
                    if let error = error {
                        self.logMessage(messageText: "ATDebug :: Customer Video Call Error selecting capture device.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                    } else {
                        self.previewView?.shouldMirror = (captureDevice.position == .front)
                    }
                }
            }
            
            UIView.transition(with: self.previewView ?? UIView(), duration: 1.0, options: [.transitionFlipFromLeft], animations: {
            })
        }
        
    }
    
    // MARK:- IBActions
    @IBAction func connect(sender: AnyObject) {
        performStartCallAction(uuid: UUID(), roomName: self.strRoomName)
        self.dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        if (self.roomTextField?.isFirstResponder ?? false) {
            self.roomTextField?.resignFirstResponder()
        }
    }
    
    
    // Update our UI based upon if we are in a Room or not
    func showRoomUI(inRoom: Bool) {
        
        UIApplication.shared.isIdleTimerDisabled = inRoom
        
        // Show / hide the automatic home indicator on modern iPhones.
        self.lblPhoneNo?.isHidden = inRoom
        self.lblTimer?.isHidden = !inRoom
        self.lblDeclineText?.isHidden = inRoom
        self.setNeedsUpdateOfHomeIndicatorAutoHidden()
    }
    
    func prepareLocalMedia() {
        // We will share local audio and video when we connect to the Room.
        
        // Create an audio track.
        if (self.localAudioTrack == nil) {
            self.localAudioTrack = LocalAudioTrack()
            
            if (self.localAudioTrack == nil) {
                logMessage(messageText: "ATDebug :: Customer Video Call Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (self.localVideoTrack == nil) {
            self.startPreview()
        }
    }
    
    // MARK:- Private
    func startPreview() {
        if PlatformUtils.isSimulator {
            return
        }
        
        let frontCamera = CameraSource.captureDevice(position: .front)
        let backCamera = CameraSource.captureDevice(position: .back)
        
        if (frontCamera != nil || backCamera != nil) {
            // Preview our local camera track in the local video preview view.
            self.camera = CameraSource(delegate: self)
            self.localVideoTrack = LocalVideoTrack(source: camera!, enabled: true, name: "Camera")
            
            // Add renderer to video track for local preview
            self.localVideoTrack!.addRenderer(self.previewView ?? VideoView())
            logMessage(messageText: "ATDebug :: Customer Video Call Video track created")
            
            if (frontCamera != nil && backCamera != nil) {
                // We will flip camera on tap.
                let tap = UITapGestureRecognizer(target: self, action: #selector(self.flipCamera1))
                self.previewView?.addGestureRecognizer(tap)
            }
            
            let format = VideoFormat()
            format.dimensions = CMVideoDimensions(width:1280, height: 720)
            format.frameRate = 30
            
            self.camera!.startCapture(device: frontCamera != nil ? frontCamera! : backCamera!) { (captureDevice, videoFormat, error) in
                if let error = error {
                    self.logMessage(messageText: "ATDebug :: Customer Video Call Capture failed with error.\ncode = \((error as NSError).code) error = \(error.localizedDescription)")
                } else {
                    self.previewView?.shouldMirror = (captureDevice.position == .front)
                }
            }
        }
        else {
            self.logMessage(messageText:"No front or back capture device found!")
        }
    }
    
    func cleanupRemoteParticipant() {
        if self.remoteParticipant != nil {
            self.remoteView?.removeFromSuperview()
            self.remoteView = nil
            self.remoteParticipant = nil
        }
    }
    
    
    func performStartCallAction(uuid: UUID, roomName: String?) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        let startCallAction = CXStartCallAction(call: uuid, handle: callHandle)
        
        startCallAction.isVideo = true
        
        let transaction = CXTransaction(action: startCallAction)
        
        self.callKitCallController?.request(transaction, completion: { (error) in
            if let error = error {
                NSLog("StartCallAction transaction request failed: \(error.localizedDescription)")
                return
            }
            NSLog("StartCallAction transaction request successful")
        })
        
    }
    
    func reportIncomingCall(uuid: UUID, roomName: String?, completion: ((NSError?) -> Void)? = nil) {
        let callHandle = CXHandle(type: .generic, value: roomName ?? "")
        
        let callUpdate = CXCallUpdate()
        callUpdate.remoteHandle = callHandle
        callUpdate.supportsDTMF = false
        callUpdate.supportsHolding = false
        callUpdate.supportsGrouping = false
        callUpdate.supportsUngrouping = false
        callUpdate.hasVideo = false
        
        self.callKitProvider?.reportCall(with: uuid, updated: callUpdate)
        
        // callKitProvider.reportNewIncomingCall(with: uuid, update: callUpdate) { error in
        //   if error == nil {
        NSLog("Incoming call successfully reported.")
        //                let storyboard = UIStoryboard(name: "Main", bundle: nil)
        //                let tempVC = storyboard.instantiateViewController(withIdentifier: "TempViewController") as? TempViewController
        //                self.present(tempVC ?? UIViewController(), animated: true, completion: nil)
        // } else {
        //   NSLog("Failed to report incoming call successfully: \(String(describing: error?.localizedDescription)).")
        //}
        //completion?(error as NSError?)
        //}
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    @objc func callAccepted(_ notification: Notification) {
        if let response = notification.object as? [String:String] {
            
            self.timer?.invalidate()
            self.timer = nil
            
            self.strRoomName = JSON(response["room_id"] as Any).stringValue //  response["room_id"] ?? ""
          //  senderName = JSON(response["sender_name"] as Any).stringValue // response["sender_name"] ?? ""
            self.receiverId = JSON(response["receiver_id"] as Any).stringValue // response["receiver_id"] ?? ""
            self.senderId = JSON(response["sender_id"] as Any).stringValue // response["sender_id"] ?? ""
            self.connect(sender: UIButton())
            
            let newConstraint =  self.heightConstraint?.constraintWithMultiplier(0.251)
            self.view.removeConstraint(self.heightConstraint ?? NSLayoutConstraint())
            self.view.addConstraint(newConstraint ?? NSLayoutConstraint())
            self.heightConstraint = newConstraint
            
            let newWidthConstraint = self.WidthConstraint?.constraintWithMultiplier(0.30)
            self.view.removeConstraint(self.WidthConstraint ?? NSLayoutConstraint())
            self.view.addConstraint(newWidthConstraint ?? NSLayoutConstraint())
            self.WidthConstraint = newWidthConstraint
            
//            let newConstraint = self.heightConstraint.constraintWithMultiplier(0.30)
//            view.removeConstraint(self.heightConstraint)
//            view.addConstraint(newConstraint)
//            self.heightConstraint = newConstraint
            
            
            if #available(iOS 11.0, *) {
//                let window = UIApplication.shared.keyWindow
//                let topPadding = window?.safeAreaInsets.top
//                let standardSpacing: CGFloat = 40.0
//                self.topConstraint.constant = (topPadding ?? 0) + standardSpacing
//                self.trailingConstraint.constant = 20
                
                let window = UIApplication.shared.keyWindow
                let BottomPadding = window?.safeAreaInsets.bottom
                let standardSpacing: CGFloat = 40.0
                self.BottomConstraint.constant = (BottomPadding ?? 0) + standardSpacing
                self.LeadingConstraint.constant = 20
            }
            UIView.animate(withDuration: 1.0) {
                self.view.layoutIfNeeded()
            }
            UIViewPropertyAnimator(duration: 1.0, curve: .easeIn) {
                self.previewView?.layer.cornerRadius = 10
                self.previewView?.layer.masksToBounds = true
            }.startAnimation()
            
        }
      
        
    }
    
    @objc func callRejection(_ notification: Timer) {
        print("The count is \(count)")
        if(count == 30)
        {
            //self.btnactionDecline(UIButton())
        }
        count = count + 1
        
    }
    @IBAction func btnMuteClick(_ sender: UIButton) {
        
        
        if  self.btnMute.isSelected {
            print("Mic is enable now")
            
            if let localAudioTrack = self.localAudioTrack, let localParticipant = room?.localParticipant {

                localParticipant.publishAudioTrack(localAudioTrack)
           
                self.btnMute.isSelected = false
            }

        }else {
            print("Mic is mute now")

            if let localAudioTrack = self.localAudioTrack, let localParticipant = room?.localParticipant  {

                localParticipant.unpublishAudioTrack(localAudioTrack)

                self.btnMute.isSelected = true
            }
            
        }
        
        

//        if let room = room, let uuid = room.uuid, let localAudioTrack = self.localAudioTrack {
//            let isMuted = localAudioTrack.isEnabled
//            let muteAction = CXSetMutedCallAction(call: uuid, muted: isMuted)
//            let transaction = CXTransaction(action: muteAction)
//
//            self.callKitCallController.request(transaction)  { error in
//                DispatchQueue.main.async {
//                    if let error = error {
//                        self.logMessage(messageText: "ATDebug :: Advisor Video CallSetMutedCallAction transaction request failed: \(error.localizedDescription)")
//                        return
//                    }
//                    self.logMessage(messageText: "ATDebug :: Advisor Video CallSetMutedCallAction transaction request successful")
//                }
//            }
//        }
    }
    @IBAction func btnactionDecline(_ sender: Any) {
        self.socketForDisconnect(reject: 1)
        
        
        self.timer?.invalidate()
        self.timer = nil
                count = 1
//
//        self.socketForDisconnect(reject: 1)//webserviceForDisconnect(reject: 1)
        //        self.goBack()
    }
    
    
    func goBack()
    {
        // CallKit has an odd API contract where the developer must call invalidate or the CXProvider is leaked.
        self.callKitProvider?.invalidate()
        //         self.audioDevice.isEnabled = true
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCall.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedByReceiver.rawValue)
//        SocketIOManager.shared.socket.off(socketApiKeys.VideoCallRejectedBySender.rawValue)
//        
        self.localVideoTrack = nil
        self.localAudioTrack = nil
        // We are done with camera
        if let camera = self.camera {
            camera.stopCapture()
            self.camera = nil
        }
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
}


//MARK:- ========== Socket for Video Call =======

extension CustomerVideoCallViewController
{
    
    func socketForDisconnect(reject: Int?) {
        //        let url = APIUrl.videoCall
        is_rejected = reject
        var param = [String: Any]()
        param = ["user_id":Int(senderId ?? "") ?? 0,"user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "","advisor_id":Int(receiverId ?? "0") ?? 0,"booking_id": Int(visitID ?? "0") ?? 0, "call_status" : "before_receive"]
      //  param = ["receiver_id" : Int(receiverId ?? "0") ?? 0 , "sender_id": Int(senderId ?? "") ?? 0, "visit_id" : Int(visitID ?? "0") ?? 0, "call_status" : "before_receive"]
        if is_rejected != nil {

            if isPickedUp != nil {
                param = ["user_id":Int(senderId ?? "") ?? 0,"user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "" ,"advisor_id":Int(receiverId ?? "0") ?? 0,"booking_id": Int(visitID ?? "0") ?? 0, "call_status" : "after_receive"]
                endSession()
               
            } else {
                param = ["user_id":Int(senderId ?? "") ?? 0,"user_name":SingletonClass.sharedInstance.loginForCustomer?.profile.fullName ?? "" ,"advisor_id":Int(receiverId ?? "0") ?? 0,"booking_id": Int(visitID ?? "0") ?? 0, "call_status" : "before_receive"]
                
            }
        }
        if param["call_status"] as? String ?? "" == "before_receive" {
            self.goBack()
        }
        
        print("\n\nParam : \n\(param)\n\n")
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCallRejectedBySender.rawValue, with: param)
        
        if let room = self.room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Customer Video Call Attempting to disconnect from room \(room.name)")
            self.userInitiatedDisconnect = true
            self.performEndCallAction(uuid: uuid)
        }
    }
    
    func EmitVideoCall() {
        
        let joinAdvisor = ["customer_id": Int(senderId ?? "") ?? 0,"customer_name":senderName ?? "","advisor_id":Int(receiverId ?? "") ?? 0,"advisor_name":senderName ?? "","booking_id":Int(visitID ?? "") ?? 0] as [String : Any]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.VideoCall.rawValue , with: joinAdvisor)
        
    }
    
    func ONForRejectVideoCall() {
        SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCallRejectedByReceiver.rawValue) { (response) in
            print(#function)
            print(response)
            self.CallRejectByAdvisor = true
            if let room = self.room, let uuid = room.uuid {
                self.logMessage(messageText: "ATDebug :: Customer Video Call Attempting to disconnect from room \(room.name)")
                self.userInitiatedDisconnect = true
                self.performEndCallAction(uuid: uuid)
            }
            
            self.is_rejected = 1
           
            if self.is_rejected != nil {

                if self.isPickedUp != nil {
                   
                    self.endSession()
                   
                } else {
                    
                    
                }
            }
            self.goBack()
        }
    }

    
    func ONVideoCall() {
        
        SocketIOManager.shared.socketCall(for: socketApiKeys.VideoCall.rawValue) { (json) in
            print(#function)
            print(json)
            let dict = json[0]
            self.token = dict["token"].stringValue
            if self.is_rejected != nil {
                if let room = self.room, let uuid = room.uuid {
                    self.logMessage(messageText: "ATDebug :: Customer Video Call Attempting to disconnect from room \(room.name)")
                    self.userInitiatedDisconnect = true
                    self.performEndCallAction(uuid: uuid)
                    
                }
                self.goBack()
            }
            
        }
    }
    
    func pickupCall()
    {
        //PickUpCall
        SocketIOManager.shared.socketCall(for: socketApiKeys.PickupCall.rawValue) { (response) in
            //            if let response = notification.object as? [String:String] {
            print(#function)
            print(response)
           
            
            if let arrResponse = response.array {
                if let singleResponse = arrResponse.first {
                    if singleResponse["status"].string?.lowercased() == "accepted" {
                        SingletonClass.sharedInstance.FreeMinutes = "0"
                        self.timer?.invalidate()
                        self.timer = nil

                        self.strRoomName = JSON(response[0]["room_id"] as Any).stringValue //  response["room_id"] ?? ""
                       // self.senderName = JSON(response[0]["sender_name"] as Any).stringValue // response["sender_name"] ?? ""
                        self.receiverId = "\(JSON(response[0]["advisor_id"] as Any).int ?? 0)"  // response["receiver_id"] ?? ""
                        self.senderId = "\(JSON(response[0]["user_id"] as Any).int ?? 0)" // response["sender_id"] ?? ""
                        self.visitID = "\(JSON(response[0]["booking_id"] as Any).int ?? 0)"
                        self.connect(sender: UIButton())
                        
                        
                        let newConstraint =  self.heightConstraint?.constraintWithMultiplier(0.251)
                        self.view.removeConstraint(self.heightConstraint ?? NSLayoutConstraint())
                        self.view.addConstraint(newConstraint ?? NSLayoutConstraint())
                        self.heightConstraint = newConstraint
                        
                        let newWidthConstraint = self.WidthConstraint?.constraintWithMultiplier(0.30)
                        self.view.removeConstraint(self.WidthConstraint ?? NSLayoutConstraint())
                        self.view.addConstraint(newWidthConstraint ?? NSLayoutConstraint())
                        self.WidthConstraint = newWidthConstraint
            //
            //            let newConstraint = self.heightConstraint.constraintWithMultiplier(0.30)
            //            self.view.removeConstraint(self.heightConstraint)
            //            self.view.addConstraint(newConstraint)
            //            self.heightConstraint = newConstraint
                        
                        
                        if #available(iOS 11.0, *) {
            //                let window = UIApplication.shared.keyWindow
            //                let topPadding = window?.safeAreaInsets.top
            //                let standardSpacing: CGFloat = 40.0
            //                self.topConstraint.constant = (topPadding ?? 0) + standardSpacing
            //                self.trailingConstraint.constant = 20
                            
                            let window = UIApplication.shared.keyWindow
                            let BottomPadding = window?.safeAreaInsets.bottom
                            let standardSpacing: CGFloat = 40.0
                            if self.BottomConstraint != nil {
                                self.BottomConstraint.constant = (BottomPadding ?? 0) + standardSpacing
                            }
                            if self.LeadingConstraint != nil {
                                self.LeadingConstraint.constant = 20
                            }
                            
                        }
                        UIView.animate(withDuration: 1.0) {
                            self.view.layoutIfNeeded()
                        }
                        UIViewPropertyAnimator(duration: 1.0, curve: .easeIn) {
                            self.previewView?.layer.cornerRadius = 10
                            self.previewView?.layer.masksToBounds = true
                        }.startAnimation()
                        
                        self.totalMinutesTimer = (Int(self.totalMinutes) ?? 1)
                       
                        self.TotalTimecounter = 0
                        
                        self.startTimer(withInterval: 1)
                    } else if singleResponse["status"].string?.lowercased() == "rejected" {
                        self.lblTimer?.text = "Call ended"
                        if let topVC = UIApplication.topViewController() {
                            
                            if topVC.isModal {
                                topVC.dismiss(animated: true, completion: {
                                    
                                })
                            } else {
                                topVC.navigationController?.popViewController(animated: true)
                            }
                            
                        }
                    }
                    print("Status of call :: \(singleResponse["status"])")
                }
            }
            
            
            
            //            }
        }
    }
    
}

extension CustomerVideoCallViewController
{
    func performEndCallAction(uuid: UUID) {
        let endCallAction = CXEndCallAction(call: uuid)
        let transaction = CXTransaction(action: endCallAction)
        
        self.callKitCallController?.request(transaction) { error in
            if let error = error {
                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
                return
            }
            self.goBack()
            NSLog("EndCallAction transaction request successful")
        }
    }
}


extension CustomerVideoCallViewController : CXProviderDelegate {
    
    
    func logMessage(messageText: String) {
        print(messageText)
        messageLabel?.text = messageText
        
        if #available(iOS 14.0, *) {
            let logger = Logger(subsystem: Bundle.main.bundleIdentifier!, category: "Komal")
            logger.log("\(messageText)")

        } else {
            // Fallback on earlier versions
        }
    }
    
    func providerDidReset(_ provider: CXProvider) {
        logMessage(messageText: "ATDebug :: Customer Video Call providerDidReset:")
        
        // AudioDevice is enabled by default
        self.audioDevice.isEnabled = true
        
        room?.disconnect()
    }
    
    func providerDidBegin(_ provider: CXProvider) {
        logMessage(messageText: "ATDebug :: Customer Video Call providerDidBegin")
    }
    
    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        logMessage(messageText: "ATDebug :: Customer Video Call provider:didActivateAudioSession:")
        
        self.audioDevice.isEnabled = true
    }
    
    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        logMessage(messageText: "ATDebug :: Customer Video Call provider:didDeactivateAudioSession:")
    }
    
    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        logMessage(messageText: "ATDebug :: Customer Video Call provider:timedOutPerformingAction:")
    }
    
    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        logMessage(messageText: "ATDebug :: Customer Video Call provider:performStartCallAction:")
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false;
        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()
        callKitProvider?.reportOutgoingCall(with: action.callUUID, startedConnectingAt: nil)
        performRoomConnect(uuid: action.callUUID, roomName: action.handle.value) { (success) in
            if (success) {
                provider.reportOutgoingCall(with: action.callUUID, connectedAt: Date())
                action.fulfill()
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        logMessage(messageText: "ATDebug :: Customer Video Call provider:performAnswerCallAction:")
        
        /*
         * Configure the audio session, but do not start call audio here, since it must be done once
         * the audio session has been activated by the system after having its priority elevated.
         */
        
        // Stop the audio unit by setting isEnabled to `false`.
        self.audioDevice.isEnabled = false;
        
        // Configure the AVAudioSession by executign the audio device's `block`.
        self.audioDevice.block()
        
        performRoomConnect(uuid: action.callUUID, roomName: self.strRoomName) { (success) in
            if (success) {
                action.fulfill(withDateConnected: Date())
            } else {
                action.fail()
            }
        }
    }
    
    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        NSLog("provider:performEndCallAction:")
        
        room?.disconnect()
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetMutedCallAction) {
        NSLog("provier:performSetMutedCallAction:")
        
        //        muteAudio(isMuted: action.isMuted)
        
        action.fulfill()
    }
    
    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        NSLog("provier:performSetHeldCallAction:")
        
        let cxObserver = self.callKitCallController?.callObserver
        let calls = cxObserver?.calls
        
        guard let call = calls?.first(where:{$0.uuid == action.callUUID}) else {
            action.fail()
            return
        }
        
        if call.isOnHold {
            holdCall(onHold: false)
        } else {
            holdCall(onHold: true)
        }
        action.fulfill()
    }
    
    func holdCall(onHold: Bool) {
        self.localAudioTrack?.isEnabled = !onHold
        self.localVideoTrack?.isEnabled = !onHold
    }
    
    func performRoomConnect(uuid: UUID, roomName: String? , completionHandler: @escaping (Bool) -> Swift.Void) {
        // Configure access token either from server or manually.
        // If the default wasn't changed, try fetching from server.
        //        if (accessToken == accessToken) {
        //        do {
        self.accessToken = self.token ?? ""//try TokenUtils.fetchToken(url: tokenUrl)
        //        } catch {
        //            let message = "Failed to fetch access token"
        //            logMessage(messageText: message)
        //            return
        //        }
        //        }
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = ConnectOptions(token: self.accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [LocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [LocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters//EncodingParameters(audioBitrate:16, videoBitrate:0)//
            }
            
            // Use the preferred signaling region
            if let signalingRegion = Settings.shared.signalingRegion {
                builder.region = signalingRegion
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = roomName
            
            // The CallKit UUID to assoicate with this Room.
            builder.uuid = uuid
        }
        
        // Connect to the Room using the options we provided.
        self.room = TwilioVideoSDK.connect(options: connectOptions, delegate: self)
        
        logMessage(messageText: "ATDebug :: Customer Video Call Attempting to connect to room \(String(describing: roomName))")
        
        self.showRoomUI(inRoom: true)
        
        self.callKitCompletionHandler = completionHandler
    }
}

// MARK:- RoomDelegate
extension CustomerVideoCallViewController : RoomDelegate {
    func roomDidConnect(room: Room) {
        // At the moment, this example only supports rendering one Participant at a time.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Connected to room \(room.name) as \(room.localParticipant?.identity ?? "")")
        isPickedUp = 1
        // This example only renders 1 RemoteVideoTrack at a time. Listen for all events to decide which track to render.
        for remoteParticipant in room.remoteParticipants {
            remoteParticipant.delegate = self
        }
        
        
        let cxObserver = self.callKitCallController?.callObserver
        let calls = cxObserver?.calls
        
        // Let the call provider know that the outgoing call has connected
        if let uuid = room.uuid, let call = calls?.first(where:{$0.uuid == uuid}) {
            if call.isOutgoing {
                self.callKitProvider?.reportOutgoingCall(with: uuid, connectedAt: nil)
            }
        }
        
        self.callKitCompletionHandler!(true)
    }
    
    func roomDidDisconnect(room: Room, error: Error?) {
        logMessage(messageText: "ATDebug :: Customer Video Call Disconnected from room \(room.name), error = \(String(describing: error))")
        
        if !self.userInitiatedDisconnect, let uuid = room.uuid, let error = error {
            var reason = CXCallEndedReason.remoteEnded
            
            if (error as NSError).code != TwilioVideoSDK.Error.roomRoomCompletedError.rawValue {
                reason = .failed
            }
            
            self.callKitProvider?.reportCall(with: uuid, endedAt: nil, reason: reason)
            //            self.callKitProvider.reportCall(with: uuid, endedAt: nil, reason: reason)
        }
        
        self.cleanupRemoteParticipant()
        self.room = nil
        self.showRoomUI(inRoom: false)
        self.callKitCompletionHandler = nil
        self.userInitiatedDisconnect = false
    }
    
    func roomDidFailToConnect(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Customer Video Call Failed to connect to room with error: \(error.localizedDescription)")
        
        self.callKitCompletionHandler!(false)
        self.room = nil
        self.showRoomUI(inRoom: false)
    }
    
    func roomIsReconnecting(room: Room, error: Error) {
        logMessage(messageText: "ATDebug :: Customer Video Call Reconnecting to room \(room.name), error = \(String(describing: error))")
    }
    
    func roomDidReconnect(room: Room) {
        logMessage(messageText: "ATDebug :: Customer Video Call Reconnected to room \(room.name)")
    }
    
    func participantDidConnect(room: Room, participant: RemoteParticipant) {
        // Listen for events from all Participants to decide which RemoteVideoTrack to render.
        participant.delegate = self
        isPickedUp = 1
        
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func participantDidDisconnect(room: Room, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Customer Video Call Room \(room.name), Participant \(participant.identity) disconnected")
        self.goBack()
        // Nothing to do in this example. Subscription events are used to add/remove renderers.
    }
}



// MARK:- RemoteParticipantDelegate
extension CustomerVideoCallViewController : RemoteParticipantDelegate {
    func remoteParticipantDidPublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has offered to share the video Track.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) published video track")
    }
    
    func remoteParticipantDidUnpublishVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        // Remote Participant has stopped sharing the video Track.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) unpublished video track")
    }
    
    func remoteParticipantDidPublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        // Remote Participant has offered to share the audio Track.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) published audio track")
    }
    
    func remoteParticipantDidUnpublishAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) unpublished audio track")
    }
    
    func didSubscribeToVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // The LocalParticipant is subscribed to the RemoteParticipant's video Track. Frames will begin to arrive now.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == nil) {
            _ = renderRemoteParticipant(participant: participant)
        }
    }
    
    func didUnsubscribeFromVideoTrack(videoTrack: RemoteVideoTrack, publication: RemoteVideoTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        
        if self.remoteParticipant == participant {
            cleanupRemoteParticipant()
            
            // Find another Participant video to render, if possible.
            if var remainingParticipants = room?.remoteParticipants,
               let index = remainingParticipants.firstIndex(of: participant) {
                remainingParticipants.remove(at: index)
                renderRemoteParticipants(participants: remainingParticipants)
            }
        }
    }
    
    func didSubscribeToAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Subscribed to audio track for Participant \(participant.identity)")
    }
    
    func didUnsubscribeFromAudioTrack(audioTrack: RemoteAudioTrack, publication: RemoteAudioTrackPublication, participant: RemoteParticipant) {
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        
        logMessage(messageText: "ATDebug :: Customer Video Call Unsubscribed from audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipantDidEnableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) enabled video track")
    }
    
    func remoteParticipantDidDisableVideoTrack(participant: RemoteParticipant, publication: RemoteVideoTrackPublication) {
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) disabled video track")
    }
    
    func remoteParticipantDidEnableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) enabled audio track")
    }
    
    func remoteParticipantDidDisableAudioTrack(participant: RemoteParticipant, publication: RemoteAudioTrackPublication) {
        logMessage(messageText: "ATDebug :: Customer Video Call Participant \(participant.identity) disabled audio track")
    }
    
    func didFailToSubscribeToAudioTrack(publication: RemoteAudioTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Customer Video Call FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func didFailToSubscribeToVideoTrack(publication: RemoteVideoTrackPublication, error: Error, participant: RemoteParticipant) {
        logMessage(messageText: "ATDebug :: Customer Video Call FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
    
    
    func renderRemoteParticipant(participant : RemoteParticipant) -> Bool {
        // This example renders the first subscribed RemoteVideoTrack from the RemoteParticipant.
        let videoPublications = participant.remoteVideoTracks
        for publication in videoPublications {
            if let subscribedVideoTrack = publication.remoteTrack,
               publication.isTrackSubscribed {
                setupRemoteVideoView()
                subscribedVideoTrack.addRenderer(self.remoteView!)
                self.remoteParticipant = participant
                return true
            }
        }
        return false
    }
    
    func renderRemoteParticipants(participants : Array<RemoteParticipant>) {
        for participant in participants {
            // Find the first renderable track.
            if participant.remoteVideoTracks.count > 0,
               renderRemoteParticipant(participant: participant) {
                break
            }
        }
    }
    
    func setupRemoteVideoView() {
        // Creating `VideoView` programmatically
        self.remoteView = VideoView(frame: CGRect.zero, delegate: self)
        
        self.view.insertSubview(self.remoteView!, at: 0)
        
        // `VideoView` supports scaleToFill, scaleAspectFill and scaleAspectFit
        // scaleAspectFit is the default mode when you create `VideoView` programmatically.
        self.remoteView!.contentMode = .scaleAspectFill;
        
        let centerX = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerX,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerX)
        let centerY = NSLayoutConstraint(item: self.remoteView!,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         relatedBy: NSLayoutConstraint.Relation.equal,
                                         toItem: self.view,
                                         attribute: NSLayoutConstraint.Attribute.centerY,
                                         multiplier: 1,
                                         constant: 0);
        self.view.addConstraint(centerY)
        let width = NSLayoutConstraint(item: self.remoteView!,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       relatedBy: NSLayoutConstraint.Relation.equal,
                                       toItem: self.view,
                                       attribute: NSLayoutConstraint.Attribute.width,
                                       multiplier: 1,
                                       constant: 0);
        self.view.addConstraint(width)
        let height = NSLayoutConstraint(item: self.remoteView!,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        relatedBy: NSLayoutConstraint.Relation.equal,
                                        toItem: self.view,
                                        attribute: NSLayoutConstraint.Attribute.height,
                                        multiplier: 1,
                                        constant: 0);
        self.view.addConstraint(height)
    }
    
}


// MARK:- VideoViewDelegate
extension CustomerVideoCallViewController : VideoViewDelegate {
    func videoViewDimensionsDidChange(view: VideoView, dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

// MARK:- CameraSourceDelegate
extension CustomerVideoCallViewController : CameraSourceDelegate {
    func cameraSourceDidFail(source: CameraSource, error: Error) {
        logMessage(messageText: "ATDebug :: Customer Video Call Camera source failed with error: \(error.localizedDescription)")
    }
}

// MARK:- UITextFieldDelegate
extension CustomerVideoCallViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.connect(sender: textField)
        return true
    }
}

//extension CustomerVideoCallViewController {
//
//    func startTimer(withInterval interval: Double) {
//        if internalTimer != nil {
//            internalTimer?.invalidate()
//        }
//       // jobs.append(job)
//        internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
//
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute:  { [self] in
//
//        })
//
//    }
//
//    func stopTimer() {
//        guard internalTimer != nil else {
//            print("No timer active, start the timer before you stop it.")
//            return
//        }
//       // jobs = [()->()]()
//        internalTimer?.invalidate()
//    }
//
//    @objc func doJob() {
//        let CheckTime = (totalMinutesTimer * 60) - 30
//        if TotalTimecounter == CheckTime {
//
//            if let topVC = UIApplication.topViewController() {
//                let controllerForCommunication = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommunicationReminerViewController.storyboardID) as! CommunicationReminerViewController
//                let navigationController = UINavigationController(rootViewController: controllerForCommunication)
//
//                let timeLeft = secondsToHoursMinutesSeconds(seconds: (totalMinutesTimer * 60) - TotalTimecounter - 1)
//                controllerForCommunication.timePending = "\(timeLeft.1):\(timeLeft.2)"
//
//                controllerForCommunication.closourForBuyMinutes = { [self] in
//                    let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
//                    controller.isBuyNow = true
//
//                    controller.closourForBookRequest = {
//                        self.dismiss(animated: true, completion: nil)
//
//                        self.totalMinutesTimer = self.totalMinutesTimer + (Int("\(controller.textFieldSelectMinutes.text ?? "")") ?? 1)
//                    }
//                    controller.CommunicationType = DataForBuyAgain["typeName"] as? String ?? ""
//
//                    controller.AdvisorID = DataForBuyAgain["AdvisorID"] as? String ?? ""
//
//                    controller.PriceOfSelectedCategory = DataForBuyAgain["PriceOfSelectedCategory"] as? String ?? ""
//                    let navigationController = UINavigationController(rootViewController: controller)
//                    navigationController.modalPresentationStyle = .overCurrentContext
//                    navigationController.modalTransitionStyle = .crossDissolve
//                    appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
//                }
//
//                navigationController.modalPresentationStyle = .overCurrentContext
//                navigationController.modalTransitionStyle = .crossDissolve
//                topVC.present(navigationController, animated: true, completion: nil)
//
//            }
//
//
//
//
//
//            TotalTimecounter += 1
//        } else if TotalTimecounter == (CheckTime + 30) {
//
//            endSession()
//
//
//            if let topVC = UIApplication.topViewController() {
//
//                if topVC.isModal {
//                    topVC.dismiss(animated: true, completion: {
//
//                        if let MsgVC = UIApplication.topViewController() {
//                            if MsgVC.isKind(of: MessageViewController.self) {
//                                MsgVC.navigationController?.popViewController(animated: true)
//                            }
//                        }
//
//                    })
//                } else {
//                    topVC.navigationController?.popViewController(animated: true)
//                }
//
//            }
//
//
////            DispatchQueue.main.async { [self] in
////                if let topVC = UIApplication.topViewController() {
////                    let controller = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: RateAdviserViewController.storyboardID) as! RateAdviserViewController
////
////                    controller.isDismiss = {
////
////                    }
////                    controller.advisorID = "\(RequestAcceptData["advisor_id"] as? Int ?? 0)"
////
////                    controller.advisorProfileImaage = RequestAcceptData["advisor_profile_picture"] as? String ?? ""
////                    controller.advisorName = ""
////                    controller.TotalMinutes = "\(totalMinutesTimer)"
////
////                    controller.bookingID = "\(RequestAcceptData["booking_id"] as? Int ?? 0)"
////
////
////                    let navigationController = UINavigationController(rootViewController: controller)
////                    navigationController.modalPresentationStyle = .overCurrentContext
////                    navigationController.modalTransitionStyle = .crossDissolve
////                    topVC.present(navigationController, animated: true, completion: nil)
////
////                }
////            }
//
//            print("you need to dismiss message screen")
//
//
//        } else {
//            if let topVC = UIApplication.topViewController() {
//                if topVC.isKind(of: CommunicationReminerViewController.self) {
//                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
//                    let timeLeft = secondsToHoursMinutesSeconds(seconds: (totalMinutesTimer * 60) - TotalTimecounter - 1)
//                    Controller.lblTimeLeft.text = "\(timeLeft.1):\(timeLeft.2)"
//                    Controller.lblChatReminderDescription.text = "Your video session will be resume after \(timeLeft.1):\(timeLeft.2) minute!"
//                } else if topVC.isKind(of: MessageViewController.self) {
//                    let Controller:MessageViewController = topVC as! MessageViewController
//
//                    Controller.timerSecond = TotalTimecounter
//
//                }
//
//
//            }
//            print("go forward")
//            TotalTimecounter += 1
//        }
//
////        if TotalTimecounter == 30 {
////            TotalTimecounter += -1
////
////        } else if TotalTimecounter == 0 {
////            self.navigationController?.popViewController(animated: true)
////            stopTimer()
////        } else {
////            TotalTimecounter += -1
////        }
//        print(totalMinutesTimer)
//        print(TotalTimecounter)
//
//
//    }
//    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
//      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
//    }
//
//    func endSession() {
//        let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
//        let param = [
//            "user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
//            "advisor_id" : self.RequestAcceptData["advisor_id"] as? Int ?? 0,
//            "booking_id" : self.RequestAcceptData["booking_id"] as? Int ?? 0,
//            "minute" : "\(TimeTotal.1):\(TimeTotal.2)"
//
//            ] as [String : Any]
//
//        print(param)
//        SocketIOManager.shared.socketEmit(for: socketApiKeys.end_session.rawValue, with: param)
//        stopTimer()
//
//
//
//    }
//
//
//}
extension CustomerVideoCallViewController {

    func startTimer(withInterval interval: Double) {
        if internalTimer != nil {
            internalTimer?.invalidate()
        }
       // jobs.append(job)
        internalTimer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: #selector(doJob), userInfo: nil, repeats: true)
        
    }

    func stopTimer() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : Int(receiverId ?? "") ?? 0,
                           "booking_id" : Int(visitID ?? "") ?? 0]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.stop_timer.rawValue , with: joinAdvisor)
       // jobs = [()->()]()
        internalTimer?.invalidate()
    }
    func stopReminderTimer() {
        if let topVC = UIApplication.topViewController() {
            if topVC.isKind(of: CommunicationReminerViewController.self) {
                if topVC.isModal {
                    topVC.dismiss(animated: true, completion: {
                       
                    })
                    ResumeInternalTimer()
                }
            }
        }
        
        CommunicationScreenRemiderScreen = SecondsForReminderScreen
        TimerForReminderScreen.invalidate()
        print("Total Timer count is dont :: \(TotalTimecounter)")
    }
    @objc func updateTimerForReminderScreen() {
        if CommunicationScreenRemiderScreen != 0 {
            print("CommunicationScreenRemiderScreen :: \(CommunicationScreenRemiderScreen)")
            CommunicationScreenRemiderScreen -= 1     //This will decrement(count down)the seconds.
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommunicationReminerViewController.self) {
                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
                    let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
                    Controller.lblTimeLeft.text = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
                    Controller.lblChatReminderDescription.text = "Your video session will be resume after \(String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)) minute!"
                }
            }
        } else {
            stopReminderTimer()
        }
    }
    func GoToReminderScreen() {
     
        if let topVC = UIApplication.topViewController() {
           
            let controllerForCommunication = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: CommunicationReminerViewController.storyboardID) as! CommunicationReminerViewController
            let navigationController = UINavigationController(rootViewController: controllerForCommunication)
            controllerForCommunication.SessionType = "video session"
            let timeLeft = secondsToHoursMinutesSeconds(seconds: CommunicationScreenRemiderScreen)
            controllerForCommunication.timePending = String(format: "%0.2d:%0.2d",timeLeft.1,timeLeft.2)
            controllerForCommunication.closourForResume = { [self] in
                stopReminderTimer()
               // print("Total Timer count is dont :: \(TotalTimecounter)")
            }
            controllerForCommunication.closourForBuyMinutes = { [self] in
                CommunicationScreenRemiderScreen = SecondsForReminderScreen
                TimerForReminderScreen.invalidate()
                
                if SingletonClass.sharedInstance.freeMinutesAdded.0 == true {
                    let RemoveFreeMinutes = totalMinutesTimer - (Int(SingletonClass.sharedInstance.freeMinutesAdded.1) ?? 0)
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(RemoveFreeMinutes) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                } else {
                    SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                }
               // SingletonClass.sharedInstance.WalletBalanceForMaintain = "\((Float(totalMinutesTimer) ) * (Float(SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "") ?? 0.0))"
                let controller:BuyMinutesViewController = AppStoryboard.Main.instance.instantiateViewController(withIdentifier: BuyMinutesViewController.storyboardID) as! BuyMinutesViewController
                controller.isBuyNow = true
                controller.closourForClickOnBack = {
                    ResumeInternalTimer()
                }
                controller.closourForBookRequest = {
                    self.dismiss(animated: true, completion: nil)
                    
                    self.totalMinutesTimer = self.totalMinutesTimer + (Int("\(controller.textFieldSelectMinutes.text ?? "")") ?? 1)
                    
                    ResumeInternalTimer()
                }
                
                controller.CommunicationType = SingletonClass.sharedInstance.dataForShareForBuyMinutes["typeName"] as? String ?? ""
               
                controller.AdvisorID = SingletonClass.sharedInstance.dataForShareForBuyMinutes["AdvisorID"] as? String ?? ""
                controller.CategoryID =  SingletonClass.sharedInstance.dataForShareForBuyMinutes["CategoryID"] as? String ?? ""
                controller.PriceOfSelectedCategory = SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? ""
                controller.BoookingID = visitID ?? ""
//                    controller.CommunicationType = DataForBuyAgain["typeName"] as? String ?? ""
//
//                    controller.AdvisorID = DataForBuyAgain["AdvisorID"] as? String ?? ""
//
//                    controller.PriceOfSelectedCategory = DataForBuyAgain["PriceOfSelectedCategory"] as? String ?? ""
                let navigationController = UINavigationController(rootViewController: controller)
                navigationController.modalPresentationStyle = .overCurrentContext
                navigationController.modalTransitionStyle = .crossDissolve
                appDel.window?.rootViewController?.present(navigationController, animated: true, completion: nil)
            }
            
            navigationController.modalPresentationStyle = .overCurrentContext
            navigationController.modalTransitionStyle = .crossDissolve
            topVC.present(navigationController, animated: true, completion: nil)
            
        }
    }
    @objc func doJob() {
        let CheckTime = (totalMinutesTimer * 60) - 30
        if TotalTimecounter == CheckTime {
            self.stopTimer()
            CommunicationScreenRemiderScreen = SecondsForReminderScreen
            TimerForReminderScreen = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimerForReminderScreen)), userInfo: nil, repeats: true)

            GoToReminderScreen()
            TotalTimecounter += 1
        } else if TotalTimecounter == (CheckTime + 30) {
         //   Utilities.ShowAlert(OfMessage: "Your session is complete")
            endSession()

            print("you need to dismiss message screen")
            
            
        } else {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: CommunicationReminerViewController.self) {
                    let Controller:CommunicationReminerViewController = topVC as! CommunicationReminerViewController
                    let timeLeft = secondsToHoursMinutesSeconds(seconds: (totalMinutesTimer * 60) - TotalTimecounter - 1)
                    Controller.lblTimeLeft.text = "\(timeLeft.1):\(timeLeft.2)"
                    Controller.lblChatReminderDescription.text = "Your video session will be resume after \(timeLeft.1):\(timeLeft.2) minute!"
                } else if topVC.isKind(of: CustomerVideoCallViewController.self) {
                    let Controller = topVC as! CustomerVideoCallViewController
                   
                    let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter + 1)
                    lblTimer?.text = String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2)
                    
                }
                
            }
            print("go forward")
            
            TotalTimecounter += 1
        }

        print(totalMinutesTimer)
        print(TotalTimecounter)
       
        
    }
    func ResumeInternalTimer() {
        self.internalTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.doJob), userInfo: nil, repeats: true)
        
        let joinAdvisor = ["sender_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
                           "receiver_id" : Int(receiverId ?? "") ?? 0,
                           "booking_id" : Int(visitID ?? "") ?? 0]
        
        SocketIOManager.shared.socketEmit(for: socketApiKeys.resume_timer.rawValue , with: joinAdvisor)
    }
    func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
        return (0, (seconds / 60), (seconds % 60))
//      return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func endSession() {
        
        SingletonClass.sharedInstance.WalletBalanceForMaintain = ""
        if let topVC = UIApplication.topViewController() {

            if topVC.isModal {
                topVC.dismiss(animated: true, completion: {

                })
            } else {
                topVC.navigationController?.popViewController(animated: true)
            }

        }
        let TimeTotal = secondsToHoursMinutesSeconds(seconds: TotalTimecounter)
                
        let DOBOfCustomer = convertDateFormater(SingletonClass.sharedInstance.loginForCustomer?.profile.dob ?? "")
        let param = [
            "user_id" : Int(SingletonClass.sharedInstance.loginForCustomer?.profile.id ?? "") ?? 0,
            "advisor_id" : Int(receiverId ?? "") ?? 0,
            "booking_id" : Int(visitID ?? "") ?? 0,
            "minute" : String(format: "%0.2d:%0.2d",TimeTotal.1,TimeTotal.2),
            "amount" : SingletonClass.sharedInstance.dataForShareForBuyMinutes["PriceOfSelectedCategory"] as? String ?? "",
            "dob" : DOBOfCustomer
          
            ] as [String : Any]
        
        print(param)
        self.timer?.invalidate()
        self.timer = nil
        //        count = 1
        
       
        SocketIOManager.shared.socketEmit(for: socketApiKeys.end_session.rawValue, with: param)
       // self.socketForDisconnect(reject: 1)
        self.stopTimerOnEndSession()
        if let room = self.room, let uuid = room.uuid {
            self.logMessage(messageText: "ATDebug :: Customer Video Call Attempting to disconnect from room \(room.name)")
            self.userInitiatedDisconnect = true
            self.performEndCallAction(uuid: uuid)
        }
        
    }
    func stopTimerOnEndSession() {
        guard internalTimer != nil else {
            print("No timer active, start the timer before you stop it.")
            return
        }
       // jobs = [()->()]()
        
    
        
        internalTimer?.invalidate()
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return  dateFormatter.string(from: date!)

        }

    
}
