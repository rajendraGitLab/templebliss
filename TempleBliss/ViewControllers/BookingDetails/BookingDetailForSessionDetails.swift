//
//  BookingDetail.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 24, 2021

import Foundation
import SwiftyJSON


class BookingDetailForSessionDetails : NSObject, NSCoding{

    var advisorId : String!
    var advisorName : String!
    var advisorProfilePicture : String!
    var bookingId : String!
    var customerName : String!
    var customerProfilePicture : String!
    var minute : String!
    var startTime : String!
    var type : String!
    var userId : String!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        advisorId = json["advisor_id"].stringValue
        advisorName = json["advisor_name"].stringValue
        advisorProfilePicture = json["advisor_profile_picture"].stringValue
        bookingId = json["booking_id"].stringValue
        customerName = json["customer_name"].stringValue
        customerProfilePicture = json["customer_profile_picture"].stringValue
        minute = json["minute"].stringValue
        startTime = json["start_time"].stringValue
        type = json["type"].stringValue
        userId = json["user_id"].stringValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if advisorId != nil{
        	dictionary["advisor_id"] = advisorId
        }
        if advisorName != nil{
        	dictionary["advisor_name"] = advisorName
        }
        if advisorProfilePicture != nil{
        	dictionary["advisor_profile_picture"] = advisorProfilePicture
        }
        if bookingId != nil{
        	dictionary["booking_id"] = bookingId
        }
        if customerName != nil{
        	dictionary["customer_name"] = customerName
        }
        if customerProfilePicture != nil{
        	dictionary["customer_profile_picture"] = customerProfilePicture
        }
        if minute != nil{
        	dictionary["minute"] = minute
        }
        if startTime != nil{
        	dictionary["start_time"] = startTime
        }
        if type != nil{
        	dictionary["type"] = type
        }
        if userId != nil{
        	dictionary["user_id"] = userId
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		advisorId = aDecoder.decodeObject(forKey: "advisor_id") as? String
		advisorName = aDecoder.decodeObject(forKey: "advisor_name") as? String
		advisorProfilePicture = aDecoder.decodeObject(forKey: "advisor_profile_picture") as? String
		bookingId = aDecoder.decodeObject(forKey: "booking_id") as? String
		customerName = aDecoder.decodeObject(forKey: "customer_name") as? String
		customerProfilePicture = aDecoder.decodeObject(forKey: "customer_profile_picture") as? String
		minute = aDecoder.decodeObject(forKey: "minute") as? String
		startTime = aDecoder.decodeObject(forKey: "start_time") as? String
		type = aDecoder.decodeObject(forKey: "type") as? String
		userId = aDecoder.decodeObject(forKey: "user_id") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if advisorId != nil{
			aCoder.encode(advisorId, forKey: "advisor_id")
		}
		if advisorName != nil{
			aCoder.encode(advisorName, forKey: "advisor_name")
		}
		if advisorProfilePicture != nil{
			aCoder.encode(advisorProfilePicture, forKey: "advisor_profile_picture")
		}
		if bookingId != nil{
			aCoder.encode(bookingId, forKey: "booking_id")
		}
		if customerName != nil{
			aCoder.encode(customerName, forKey: "customer_name")
		}
		if customerProfilePicture != nil{
			aCoder.encode(customerProfilePicture, forKey: "customer_profile_picture")
		}
		if minute != nil{
			aCoder.encode(minute, forKey: "minute")
		}
		if startTime != nil{
			aCoder.encode(startTime, forKey: "start_time")
		}
		if type != nil{
			aCoder.encode(type, forKey: "type")
		}
		if userId != nil{
			aCoder.encode(userId, forKey: "user_id")
		}

	}

}
