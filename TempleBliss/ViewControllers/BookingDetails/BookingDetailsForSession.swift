//
//  BookingDetails.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on June 24, 2021

import Foundation
import SwiftyJSON


class BookingDetailsForSession : NSObject, NSCoding{

    var bookingDetails : BookingDetailForSessionDetails!
    var status : Bool!

	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json.isEmpty{
			return
		}
        let bookingDetailsJson = json["booking_details"]
        if !bookingDetailsJson.isEmpty{
            bookingDetails = BookingDetailForSessionDetails(fromJson: bookingDetailsJson)
        }
        status = json["status"].boolValue
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
        if bookingDetails != nil{
        	dictionary["bookingDetails"] = bookingDetails.toDictionary()
        }
        if status != nil{
        	dictionary["status"] = status
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
		bookingDetails = aDecoder.decodeObject(forKey: "booking_details") as? BookingDetailForSessionDetails
		status = aDecoder.decodeObject(forKey: "status") as? Bool
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
	{
		if bookingDetails != nil{
			aCoder.encode(bookingDetails, forKey: "booking_details")
		}
		if status != nil{
			aCoder.encode(status, forKey: "status")
		}

	}

}
